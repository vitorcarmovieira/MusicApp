//
//  ModelHistoric.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 16/04/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation
//asdasd
class ModelHistoric{
    
    var _id        : String = ""
    var performDate : String = ""
    var hitPercentage : Float = 0.0
    var metronome : Int = 0
    var notes     : [ModelNote] = []
}
