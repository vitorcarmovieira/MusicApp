//
//  ModeloRepeat.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/18/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelRepeat {
    var id            = 0
    var idScore       = 0
    var initialBar    = 0
    var finalBar      = 0
    var voltaBrackets = 0
}
