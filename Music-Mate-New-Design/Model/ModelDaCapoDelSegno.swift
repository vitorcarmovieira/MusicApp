//
//  ModelRepetition.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelDaCapoDelSegno {
    var id         = 0
    var idScore    = 0
    var type       = -1
    
    var initialBar = -1
    var finalBar   = -1
    var toCoda     = -1
    var coda       = -1
    var fine       = -1
    var dsAlCoda   = -1
    var dcAlCoda   = -1
    var dsAlFine   = -1
    var dcAlFine   = -1
    
//    var finalBar: Int {
//        get { return _initialBar }
//        set (newValue) {
//            var change = true
//            if toCoda != -1 && newValue > toCoda {
//                change = false
//            }
//            if coda != -1 && newValue > coda {
//                change = false
//            }
//            if fine != -1 && newValue > fine {
//                change = false
//            }
//            if dsAlFine != -1 && newValue > dsAlFine {
//                change = false
//            }
//            if dcAlFine != -1 && newValue > dcAlFine {
//                change = false
//            }
//            if dsAlCoda != -1 && newValue > dsAlCoda {
//                change = false
//            }
//            if dcAlCoda != -1 && newValue > dcAlCoda {
//                change = false
//            }
//
//            if change && newValue != -1 {
//                _initialBar = newValue
//            }
//            if newValue == -1 {
//                _finalBar = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um segno
//    var initialBar: Int {
//        get { return _initialBar }
//        set (newValue) {
//            var change = true
//            if toCoda != -1 && newValue > toCoda {
//                change = false
//            }
//            if coda != -1 && newValue > coda {
//                change = false
//            }
//            if fine != -1 && newValue > fine {
//                change = false
//            }
//            if dsAlFine != -1 && newValue > dsAlFine {
//                change = false
//            }
//            if dcAlFine != -1 && newValue > dcAlFine {
//                change = false
//            }
//            if dsAlCoda != -1 && newValue > dsAlCoda {
//                change = false
//            }
//            if dcAlCoda != -1 && newValue > dcAlCoda {
//                change = false
//            }
//
//            if change && newValue != -1 {
//                _initialBar = newValue
//            }
//            if newValue == -1 {
//                _initialBar = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um toCoda
//    var toCoda: Int {
//        get { return _toCoda }
//        set (newValue) {
//            if newValue != _toCoda && dsAlFine == -1 && dcAlFine == -1 && fine == -1 {
//                if initialBar < newValue {
//                    if dsAlCoda != -1 && dsAlCoda > newValue {
//                        _toCoda = newValue
//                    }
//                    if dcAlCoda != -1 && dcAlCoda > newValue {
//                        _toCoda = newValue
//                    }
//
//                    if dsAlCoda == -1 && dcAlCoda == -1 {
//                        _toCoda = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _toCoda = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um coda
//    var coda: Int {
//        get { return _coda }
//        set (newValue) {
//            if newValue != _coda && dsAlFine == -1 && dcAlFine == -1 && fine == -1 {
//                if initialBar < newValue {
//                    if dsAlCoda != -1 && dsAlCoda < newValue {
//                        _coda = newValue
//                    }
//                    if dcAlCoda != -1 && dcAlCoda < newValue {
//                        _coda = newValue
//                    }
//
//                    if dsAlCoda == -1 && dcAlCoda == -1 {
//                        _coda = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _coda = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um fine
//    var fine: Int {
//        get { return _fine }
//        set (newValue) {
//            if newValue != _fine && dcAlCoda == -1 && dsAlCoda == -1 && toCoda == -1 && coda == -1 {
//                if initialBar < newValue {
//                    if dsAlFine != -1 && dsAlFine > newValue {
//                        _fine = newValue
//                    }
//                    if dcAlFine != -1 && dcAlFine > newValue {
//                        _fine = newValue
//                    }
//
//                    if dsAlFine == -1 && dcAlFine == -1 {
//                        _fine = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _fine = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um dcAlCoda
//    var dsAlCoda: Int {
//        get { return _dsAlCoda }
//        set (newValue) {
//            if newValue != _dsAlCoda && dcAlCoda == -1 && dcAlFine == -1 && dsAlFine == -1 && fine == -1 {
//                if self.toCoda == -1 && initialBar == -1  {
//                    _dsAlCoda = newValue
//                }
//                else {
//                    if self.toCoda < newValue && initialBar < newValue {
//                        _dsAlCoda = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _dsAlCoda = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um dcAlCoda
//    var dcAlCoda: Int {
//        get { return _dcAlCoda }
//        set (newValue) {
//            if newValue != _dcAlCoda && initialBar == -1 && dsAlCoda == -1 && dcAlFine == -1 && dsAlFine == -1 && fine == -1 {
//                if self.toCoda == -1 {
//                    _dcAlCoda = newValue
//                }
//                else {
//                    if self.toCoda < newValue {
//                        _dcAlCoda = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _dcAlCoda = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um dsAlFine
//    var dsAlFine: Int {
//        get { return _dsAlFine }
//        set (newValue) {
//            if newValue != _dsAlFine && coda == -1 && toCoda == -1 && dsAlCoda == -1 && dcAlCoda == -1 && dcAlFine == -1 {
//                if self.fine == -1 && initialBar == -1 {
//                    _dsAlFine = newValue
//                }
//                else {
//                    if self.fine < newValue && initialBar < newValue {
//                        _dsAlFine = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _dsAlFine = -1
//            }
//        }
//    }
//
//    //Condições para adicionar um dcAlFine
//    var dcAlFine: Int {
//        get { return _dcAlFine }
//        set (newValue) {
//            if newValue != _dcAlFine && initialBar == -1 && coda == -1 && toCoda == -1 && dsAlCoda == -1 && dcAlCoda == -1 && dsAlFine == -1 {
//                if self.fine == -1 {
//                    _dcAlFine = newValue
//                }
//                else {
//                    if self.fine < newValue {
//                        _dcAlFine = newValue
//                    }
//                }
//            }
//            if newValue == -1 {
//                _dcAlFine = -1
//            }
//        }
//    }
    
    func isValid() -> Bool {
        if initialBar == -1 && finalBar == -1 && coda == -1 && toCoda == -1 && fine == -1 && dsAlCoda == -1 && dcAlCoda == -1 && dsAlFine == -1 && dcAlFine == -1 {
            return true
        }
        if dsAlCoda != -1 {
            if initialBar != -1 && coda != -1 && toCoda != -1 && fine == -1 && dcAlCoda == -1 && dsAlFine == -1 && dcAlFine == -1 {
                return initialBar < toCoda && toCoda < coda && coda <= (dsAlCoda + 1)
            }
            return false
        }
        
        if dcAlCoda != -1 {
            if coda != -1 && toCoda != -1 && dsAlCoda == -1 && dsAlFine == -1 && dcAlFine == -1 && fine == -1 {
                return toCoda < coda && coda <= (dcAlCoda + 1)
            }
            return false
        }
        
        if dsAlFine != -1 {
            if initialBar != -1 && fine != -1 && coda == -1 && toCoda == -1 && dsAlCoda == -1 && dcAlCoda == -1 && dcAlFine == -1 {
                return initialBar < fine && fine < dsAlFine
            }
            return false
        }
        
        if dcAlFine != -1 {
            if fine != -1 && coda == -1 && toCoda == -1 && dsAlCoda == -1 && dcAlCoda == -1 && dsAlFine == -1 {
                return fine < dcAlFine
            }
            return false
        }
        
        return false
    }
    
//    private var _initialBar = -1
//    private var _finalBar   = -1
//    private var _toCoda     = -1
//    private var _coda       = -1
//    private var _fine       = -1
//    private var _dsAlCoda   = -1
//    private var _dcAlCoda   = -1
//    private var _dsAlFine   = -1
//    private var _dcAlFine   = -1
//
    
    
}
