//
//  ModelArtist.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 18/06/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ModelArtist: NSObject {

    var _id      :  String =  ""
    var idUser  :  String =  ""
    var type    :  Int    =  1
    var firstName : String = ""
    var lastName : String = ""
    
}
