//
//  ModelPause.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 4/16/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class ModelPause {
    private var channel   : Int
    private var startTick : Int
    private var endTick   : Int
    
    init(channel: Int, startTick:Int, endTick: Int) {
        self.channel   = channel
        self.startTick = startTick
        self.endTick   = endTick
    }
    
    func setChannel(channel: Int) {
        self.channel = channel
    }
    
    func setStartTick(startTick: Int) {
        self.startTick = startTick
    }
    
    func setEndTick(endTick: Int) {
        self.endTick = endTick
    }
    
    func getChannel() -> Int {
        return channel
    }
    
    func getStartTick() -> Int {
        return startTick
    }
    
    func getEndTick() -> Int {
        return endTick
    }
}
