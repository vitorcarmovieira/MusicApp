//
//  ModelScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelScore: NSCopying {
    
    var id             : Int                     = 0
    var idMidi         : String                  = ""
    var idStore        : String                  = ""
    var title          : String                  = ""
    var subtitle       : String                  = ""
    var synonymTitle   : String                  = ""
    var metronome      : Int                     = 120
    var baseNote       : Int                     = 4
    var quantization   : Int                     = 4
    var type           : Int                     = 2
    var status         : Int                     = 0
    var barStopped     : Int                     = 0
    var keySignature   : Int                     = 0
    var idPartOriginal : Int                     = -1
    var difficulty     : Int                     = 1
    var arranger         : String                  = ""
    var createdyear    : String                  = ""
    var changedDate    : String                  = ""
    var copyright      : ModelCopyright           = ModelCopyright()
    var user           : ModelUser               = ModelUser()
    var instrument     : ModelInstrument         = ModelInstrument()
    var typeInstrument : ModelTypeInstrument     = ModelTypeInstrument()
    var genre          : ModelGenre              = ModelGenre()
    var subgenre       : ModelSubGenre           = ModelSubGenre()
    var country        : ModelCountry            = ModelCountry()
    var author         : [ModelAuthor]             = []
    var artist          : [ModelArtist]           = []
    var tags           : [ModelTag]               = []
    var configurations : [ModelBarConfiguration] = []
    var repeats        : [ModelRepeat]           = []
    var dsDcs          : [ModelDaCapoDelSegno]   = []
    var notes          : [ModelNote]             = []
    var playedNotes    : [ModelNotePlayed]       = []
    var pauses : [ModelPause] = []
    var timeFinished   : Int                     = 0
    var quantizPerform : Int                     = 4
    var isPoliphonic   : Int                    = 0
    var suggestedQuantization = 4
    var inserted = false
    private var groupsOfNotes   : [[GroupChannelNotes]] = []
    
    func copy(with zone: NSZone? = nil) -> Any {
        let model = ModelScore()
        
        model.title        = title
        model.author       = author
        model.metronome    = metronome
        model.baseNote     = baseNote
        model.quantization = quantization
        model.type         = type
        model.status       = status
        model.barStopped   = barStopped
        model.arranger       = arranger
        model.createdyear  = createdyear
        model.changedDate  = changedDate
        model.user         = user
        model.instrument   = instrument
        model.genre        = genre
        model.country      = country
        
        for config in configurations {
            let conf = ModelBarConfiguration()
            conf.bar         = config.bar
            conf.clef        = config.clef
            conf.denominator = config.denominator
            conf.id          = config.id
            conf.key         = config.key
            conf.metronome   = config.metronome
            conf.numerator   = config.numerator
            
            model.configurations.append(conf)
        }
        
        for repet in repeats {
            let rep = ModelRepeat()
            rep.finalBar      = repet.finalBar
            rep.id            = repet.id
            rep.idScore       = repet.idScore
            rep.initialBar    = repet.initialBar
            rep.voltaBrackets = repet.voltaBrackets
            
            model.repeats.append(rep)
        }
        
        for dsDc in dsDcs {
            let d = ModelDaCapoDelSegno()
            d.coda = dsDc.coda
            d.dcAlCoda   = dsDc.dsAlCoda
            d.dcAlFine   = dsDc.dcAlFine
            d.dsAlCoda   = dsDc.dsAlCoda
            d.dsAlFine   = dsDc.dsAlFine
            d.finalBar   = dsDc.finalBar
            d.fine       = dsDc.fine
            d.id         = dsDc.id
            d.idScore    = dsDc.idScore
            d.initialBar = dsDc.initialBar
            d.toCoda     = dsDc.toCoda
            d.type       = dsDc.type
            
            model.dsDcs.append(d)
        }
        
        model.notes.removeAll()
        
        for note in notes {
            let n = ModelNote()
            n.bar               = note.bar
            n.channel           = note.channel
            n.duration          = note.duration
            n.durationQuantized = note.durationQuantized
            n.id                = note.id
            n.initialTime       = note.initialTime
            n.intensity         = note.intensity
            n.isToDelete        = note.isToDelete
            n.placeInBar        = note.placeInBar
            n.tIniQuantized     = note.tIniQuantized
            n.tone              = note.tone
            
            model.notes.append(n)
            
        }
        
        return model
    }
    
    func getTimeBar() -> Int {
        for i in 0..<configurations.count {
            if configurations[i].bar == 1 {
                return ( configurations[i].numerator * getTicksDenominator(denominator: configurations[i].denominator) )
            }
        }
        
        return 3840
    }
    
    private func getTicksDenominator(denominator: Int) -> Int {
        switch denominator {
        case 2:
            return 1920
        case 4:
            return 960
        case 8:
            return 480
        case 16:
            return 240
        default:
            break
        }
        
        return 960
    }
    
    func addNote(midi: Int, ti: Int, tiQ: Int, dur: Int, durQ: Int, chnl: Int) {
        let note  = ModelNote()
        note.initialTime       = ti
        note.tIniQuantized     = tiQ
        note.duration          = dur
        note.durationQuantized = durQ
        note.channel           = chnl
        note.tone              = midi
        
        notes.append(note)
    }
    
    func getBarLastNote() -> Int {
        var maxTime = 0
        for note in notes {
            maxTime = max(maxTime, note.tIniQuantized + note.durationQuantized)
        }
        
        return getBar(tick: maxTime)
    }
    
    func addRepeat(repetition: ModelRepeat, isToDelete: Bool) -> Bool {
        for repetitionAdded in repeats {
            if repetition.initialBar == repetitionAdded.initialBar && repetition.finalBar == repetitionAdded.finalBar {
                return false
            }
            if repetition.initialBar < repetitionAdded.initialBar && repetition.finalBar > repetitionAdded.initialBar {
                return false
            }
            if repetition.initialBar >= repetitionAdded.initialBar && repetition.initialBar <= repetitionAdded.finalBar {
                return false
            }
        }
        
        if repetition.finalBar + 1 > getBarLastNote() - 1 {
            return false
        }
        
        if isToDelete {
            if deleteBarAfterPutRepetition(repetition: repetition) {
                repeats.append(repetition)
                return true
            } else {
                return false
            }
        }
        
        repeats.append(repetition)
        
        return true
    }
    
    func deleteBarAfterPutRepetition(repetition: ModelRepeat) -> Bool {
        
        var newNotes: [ModelNote] = []
        
        for note in notes {
            let barNote = getBar(tick: note.tIniQuantized)
            
            let start = repetition.initialBar
            let end   = repetition.finalBar
            let chave = repetition.voltaBrackets
            
            if barNote <= end {
                let note1 = ModelNote()
                note1.channel      = note.channel
                note1.tone        = note.tone
                note1.initialTime   = note.initialTime
                note1.tIniQuantized = note.tIniQuantized
                note1.duration    = note.duration
                note1.durationQuantized = note.durationQuantized
                
                newNotes.append(note1)
                newNotes.append(note)
            }
            
//            if barNote > end && barNote < end + (end - chave) - chave {
//                  delete
//            }
            
            let timeBar = getTimeBar()
            
            let decrease = timeBar * (end - start + 1)
//            print(end + ((end - start) - chave), end + (end - start))

            if barNote > end + (end - (start-1)) {
                let note1 = ModelNote()
                note1.channel          = note.channel
                note1.tone             = note.tone
                note1.initialTime      = note.initialTime - decrease
                note1.tIniQuantized     = note.tIniQuantized - decrease
                note1.duration         = note.duration
                note1.durationQuantized = note.durationQuantized
                
                newNotes.append(note1)
            }
        }
        
        var maxT = 0
        for note in newNotes {
            maxT = max(maxT, note.tIniQuantized + note.durationQuantized)
        }
        
        print(repetition.finalBar + 1, getBar(tick: maxT))
        
        if repetition.finalBar + 1 > getBar(tick: maxT) - 1 {
            return false
        }
        
        notes = newNotes
        
        return true
    }
    
    private func getBar(tick: Int) -> Int {
        let timeBar = getTimeBar()
        
        if tick == 0 {
            return 1
        }
        
        return Int(tick / timeBar) + 1
    }
    
    func addDSDC(dsdc: ModelDaCapoDelSegno) -> Bool {
        var lowerBar   = 10000
        var greaterBar = -1
        
        if dsdc.coda > 0 {
            lowerBar   = min(lowerBar, dsdc.coda)
            greaterBar = max(greaterBar, dsdc.coda)
        }
        if dsdc.dcAlCoda > 0 {
            lowerBar   = min(lowerBar, dsdc.dcAlCoda)
            greaterBar = max(greaterBar, dsdc.dcAlCoda)
        }
        if dsdc.dcAlFine > 0 {
            lowerBar   = min(lowerBar, dsdc.dcAlFine)
            greaterBar = max(greaterBar, dsdc.dcAlFine)
        }
        if dsdc.dsAlCoda > 0 {
            lowerBar   = min(lowerBar, dsdc.dsAlCoda)
            greaterBar = max(greaterBar, dsdc.dsAlCoda)
        }
        if dsdc.dsAlFine > 0 {
            lowerBar   = min(lowerBar, dsdc.dsAlFine)
            greaterBar = max(greaterBar, dsdc.dsAlFine)
        }
        if dsdc.finalBar > 0 {
            lowerBar   = min(lowerBar, dsdc.finalBar)
            greaterBar = max(greaterBar, dsdc.finalBar)
        }
        if dsdc.fine > 0 {
            lowerBar   = min(lowerBar, dsdc.fine)
            greaterBar = max(greaterBar, dsdc.fine)
        }
        if dsdc.initialBar > 0 {
            lowerBar   = min(lowerBar, dsdc.initialBar)
            greaterBar = max(greaterBar, dsdc.initialBar)
        }
        if dsdc.toCoda > 0 {
            lowerBar   = min(lowerBar, dsdc.toCoda)
            greaterBar = max(greaterBar, dsdc.toCoda)
        }
        
        if hasRepetitionBetween(barIni: lowerBar, barEnd: greaterBar) {
            return false
        } else {
            dsDcs.append(dsdc)
        }
        
        return true
    }
    
    private func hasRepetitionBetween(barIni: Int, barEnd: Int) -> Bool {
        for repet in repeats {
            if barIni < repet.initialBar && barEnd > repet.initialBar {
                return true
            } else if barIni < repet.finalBar && barEnd > repet.finalBar {
                return true
            } else if barIni >= repet.initialBar && barEnd <= repet.finalBar {
                return true
            }
        }
        
        return false
    }
    
    func quantizeInitialTime(tick: Int, perform: Bool = false) -> Int {
        let ticksQuant = getTicksFromQuantization(perform: perform)
        
        var fator = 0
        let div = Double(tick) / Double(ticksQuant)
        fator = Int(round(div))
//        if  div - Double(Int(div)) < 0.5 {
//            fator = Int(div)
//        } else {
//            fator = Int(div) + 1
//        }
        
//        let newTick = ticksQuant * fator
        
//        let diference = tick - newTick
        
//        if diference > (ticksQuant / 2) {
//            return (fator + 1) * ticksQuant
//        } else {
            return fator * ticksQuant
//        }
    }
    
    func quantizeDuration(tick: Int, perform: Bool = false) -> Int {
        let ticksQuant = getTicksFromQuantization(perform: perform)
        
        let fator = Int(round(Double(tick / ticksQuant)))
        
        let newTick = ticksQuant * fator
        
        let diference = tick - newTick
        
        if (Double(diference) > (Double(ticksQuant) / 1.45)) && (((fator + 1) * ticksQuant) > 0) {
            return (fator + 1) * ticksQuant
        } else if (fator * ticksQuant) > 0 {
            return fator * ticksQuant
        } else {
            return ticksQuant
        }
    }
    
    private var tickInSeconds: Double {
        get {
            return (60.0 / Double(metronome)) / Double(960)
        }
    }
    
    func getTimeTicks(time: Double) -> Int {
        return Int(round(time/tickInSeconds))
    }
    
    func requantizePlayedNotes(newQuantization: Int) {
        self.quantizPerform = newQuantization
        
        for played in playedNotes {
            played.tIniQuantized     = quantizeInitialTime(tick: played.initialTime, perform: true)
            played.durationQuantized = quantizeDuration(tick: played.duration, perform: true)
        }
    }
    
    private func getTicksFromQuantization(perform: Bool = false) -> Int {
        var qt = quantization
        if perform {
            qt = quantizPerform
        }
        
        switch qt {
        case 4:
            return 960
        case 8:
            return 480
        case 16:
            return 240
        default:
            return 960
        }
    }

    func getBarTicks() -> Int {
        var ticksTime = 960
        
        switch denominator {
        case 2:
            ticksTime = 1920
        case 4:
            ticksTime = 960
        case 8:
            ticksTime = 480
        case 16:
            ticksTime = 240
        default:
            break
        }
        
        return ticksTime * numerator
    }

    func deleteHighsNotes() {
        var i = 0
        
        while i < (notes.count - 1) {
            var j = i + 1
            
            while j < notes.count {
                let startNotei = notes[i].tIniQuantized
                let startNotej = notes[j].tIniQuantized
                let endNotei   = notes[i].tIniQuantized + notes[i].durationQuantized
                let endNotej   = notes[j].tIniQuantized + notes[j].durationQuantized
                
                let p1 = startNotei <= startNotej && endNotei >= startNotej
                let p2 = startNotei >= startNotej && startNotei <= endNotej && endNotei >= endNotej
                let p3 = startNotei >= startNotej && endNotei <= endNotej
                
//                let equali = notes[i].tIniQuantized >= notes[j].tIniQuantized && notes[i].tIniQuantized < notes[j].tIniQuantized + notes[j].durationQuantized
//
//                let equalj = (notes[i].tIniQuantized + notes[i].durationQuantized) > notes[j].tIniQuantized && (notes[i].tIniQuantized + notes[i].durationQuantized) <= notes[j].tIniQuantized + notes[j].durationQuantized
//
//                let sameTime = notes[i].tIniQuantized == notes[j].tIniQuantized && notes[i].durationQuantized == notes[j].durationQuantized
                
                if (p1 || p2 || p3) {
                    if (notes[i].tone <= notes[j].tone) {
                        notes.remove(at: j)
                    } else {
                        notes.remove(at: i)
                        j = i + 1
                    }
                } else {
                    j += 1
                }
            }
            
            i += 1
        }
    }
    
    func generatePauses() {
        pauses.removeAll()
        
        var dict: [[Int:[ModelNote]]] = []
        
        dict.append([:])
        dict.append([:])
        dict.append([:])
        dict.append([:])
        
        var greaterTimeOfChannel = [0, 0, 0, 0]
        var maxTime = 0
        //verificar
        for i in 0..<notes.count {
            var notesDict = dict[notes[i].channel - 1][notes[i].tIniQuantized]
            if notesDict == nil {
                notesDict = [notes[i]]
            } else {
                notesDict?.append(notes[i])
            }
            dict[notes[i].channel - 1].updateValue(notesDict!, forKey: notes[i].tIniQuantized)
            greaterTimeOfChannel[notes[i].channel - 1] = max(greaterTimeOfChannel[notes[i].channel - 1], notes[i].tIniQuantized + notes[i].durationQuantized)
            maxTime = max(maxTime, notes[i].tIniQuantized + notes[i].durationQuantized)
        }
        
        for i in 0..<dict.count {
            let sortedDict = dict[i].sorted { $0.0 < $1.0 }
            
            var tickStart = 0
            var tickEnd   = 0
            
            for j in 0..<sortedDict.count {
                for k in 0..<sortedDict[j].value.count {
                    tickEnd = sortedDict[j].value[k].tIniQuantized
                    
                    if tickEnd - tickStart > 0 {
                        let pause = ModelPause(channel: i + 1, startTick: tickStart, endTick: tickEnd)
                        pauses.append(pause)
                    }
                    tickStart = sortedDict[j].value[k].tIniQuantized + sortedDict[j].value[k].durationQuantized
                }
            }
        }
        
        for i in 0..<greaterTimeOfChannel.count {
            if maxTime - greaterTimeOfChannel[i] > 0 && (i == 0 || i == 2) {
                let pause = ModelPause(channel: i + 1, startTick: greaterTimeOfChannel[i], endTick: maxTime)
                pauses.append(pause)
            }
        }
        
        dict.removeAll()
    }
    
    private var numerator   = 4
    private var denominator = 4
    
    func extendNotes(numerator: Int, denominator: Int) {
        self.numerator   = numerator
        self.denominator = denominator
        
        createGroups()
        
//        fillGroups()
//
//        for i in 0..<groupsOfNotes.count {
//            if groupsOfNotes[i].count > 0 {
//                for j in 0..<groupsOfNotes[i].count - 1 {
//                    for k in 0..<groupsOfNotes[i][j].notes.count {
//                        let initialTick    = groupsOfNotes[i][j].initialTime
//                        let bar            = getTickBar(tick: initialTick)
//                        let greaterBarTick = getGreaterBarTick(bar: bar)
//
//                        let duration = groupsOfNotes[i][j + 1].initialTime - groupsOfNotes[i][j].initialTime
//
//                        if (initialTick + duration) <= greaterBarTick {
//                            groupsOfNotes[i][j].notes[k].durationQuantized = duration
//                            groupsOfNotes[i][j].notes[k].lastGroup = false
//                        } else {
//                            let duration = greaterBarTick - groupsOfNotes[i][j].initialTime
//                            groupsOfNotes[i][j].notes[k].durationQuantized = duration
//                            groupsOfNotes[i][j].notes[k].lastGroup = false
//                        }
//                    }
//                }
//            }
//        }
//
//        for i in 0..<groupsOfNotes.count {
//            let j = groupsOfNotes[i].count - 1
//            if j >= 0 {
//                for k in 0..<groupsOfNotes[i][j].notes.count {
//                    let initialTick    = groupsOfNotes[i][j].initialTime
//                    let bar            = getTickBar(tick: initialTick)
//                    let greaterBarTick = getGreaterBarTick(bar: bar)
//                    let duration = greaterBarTick - groupsOfNotes[i][j].initialTime
//                    groupsOfNotes[i][j].notes[k].durationQuantized = duration
//                    groupsOfNotes[i][j].notes[k].lastGroup = true
//                }
//            }
//        }
//
//        fillNotesFromGroup()
    }
    
    private func fillNotesFromGroup() {
//        notes.removeAll()
        
        for i in 0..<groupsOfNotes.count {
            for j in 0..<groupsOfNotes[i].count {
                for k in 0..<groupsOfNotes[i][j].notes.count {
                    groupsOfNotes[i][j].notes[k].cantDraw = true
                    notes.append(groupsOfNotes[i][j].notes[k])
                }
            }
        }
    }
    
    private func getGreaterBarTick(bar: Int) -> Int {
        return bar * getBarTicks()
    }
    
    private func getTickBar(tick: Int) -> Int {
        return (tick / getBarTicks()) + 1
    }
    
    private func createGroups() {
        var dict: [[Int:[ModelNote]]] = []
        
        dict.append([:])
        dict.append([:])
        dict.append([:])
        dict.append([:])
        
        for i in 0..<notes.count {
            var notesDict = dict[notes[i].channel - 1][notes[i].tIniQuantized]
            if notesDict == nil {
                notesDict = [notes[i]]
            } else {
                notesDict?.append(notes[i])
            }
            dict[notes[i].channel - 1].updateValue(notesDict!, forKey: notes[i].tIniQuantized)
        }
        
        for i in 0..<dict.count {
            let sortedDict = dict[i].sorted { $0.0 < $1.0 }
            
            if sortedDict.count > 0 {
                for j in 0..<sortedDict.count - 1 {
                    let bar            = getTickBar(tick: sortedDict[j].key)
                    let greaterBarTick = getGreaterBarTick(bar: bar)
                    
                    let duration = sortedDict[j + 1].key - sortedDict[j].key
                    
                    for k in 0..<sortedDict[j].value.count {
                        if (sortedDict[j].key + duration) <= greaterBarTick {
                            sortedDict[j].value[k].durationQuantized = duration
                            sortedDict[j].value[k].lastGroup = false
                        } else {
                            let duration = greaterBarTick - sortedDict[j].key
                            sortedDict[j].value[k].durationQuantized = duration
                            sortedDict[j].value[k].lastGroup = false
                        }
                        sortedDict[j].value[k].cantDraw = true
                    }
                }
            }
        }
        
        
        dict.removeAll()
    }
    
    private func fillGroups() {
        groupsOfNotes.removeAll()
//        notes.sort(by: {$1.tIniQuantized > $0.tIniQuantized})
        groupsOfNotes.append([])
        groupsOfNotes.append([])
        groupsOfNotes.append([])
        groupsOfNotes.append([])
        
        
        for i in 0..<notes.count {
            var cont = 0

            var array = groupsOfNotes[notes[i].channel - 1].filter { $0.initialTime == notes[i].tIniQuantized }
            
            if array.count > 0 {
                array[0].notes.append(notes[i])
                cont += 1
            }
//            for (j,group) in groupsOfNotes[notes[i].channel - 1].enumerated() where notes[i].tIniQuantized == group.initialTime {
////                if notes[i].tIniQuantized == groupsOfNotes[notes[i].channel - 1][j].initialTime {
//                    groupsOfNotes[notes[i].channel - 1][j].notes.append(notes[i])
//
//                    cont += 1
//
//                    break
////                }
//            }

            if cont == 0 {
                groupsOfNotes[notes[i].channel - 1].append(GroupChannelNotes(channel: notes[i].channel, initialTime: notes[i].tIniQuantized, notes: [notes[i]]))
            }
        }

       
//        for (i, note) in notes.enumerated() {
//            var cont = 0
//
//            for (j, group) in groupsOfNotes[note.channel - 1].enumerated() {
//                if note.tIniQuantized == group.initialTime {
//                    groupsOfNotes[note.channel - 1][j].notes.append(notes[i])
//
//                    cont += 1
//
//                    break
//                }
//            }
//
//            if cont == 0 {
//                var group = GroupChannelNotes(channel: note.channel, initialTime: note.tIniQuantized, notes: [])
//                group.notes.append(notes[i])
//                groupsOfNotes[note.channel - 1].append(group)
//            }
//        }
//
         notes.removeAll()
    }

    func deleteRepetition(type: TypeRepetition, bar: Int) {
        if type == .start {
            for (i, rep) in repeats.enumerated() {
                if rep.initialBar == bar {
                    repeats.remove(at: i)
                    break
                }
            }
        }
        
        if type == .end {
            for (i, rep) in repeats.enumerated() {
                if rep.finalBar == bar {
                    repeats.remove(at: i)
                    break
                }
            }
        }
    }
    
    func deleteDsDc(type: TypeRepetition, bar: Int) {
        if type == .coda {
            for dsdc in dsDcs {
                if dsdc.coda == bar {
                    dsdc.coda = -1
                    break
                }
            }
        }
        
        if type == .toCoda {
            for dsdc in dsDcs {
                if dsdc.toCoda == bar {
                    dsdc.toCoda = -1
                    break
                }
            }
        }
        
        if type == .segno {
            for dsdc in dsDcs {
                if dsdc.finalBar == bar {
                    dsdc.finalBar   = -1
                    dsdc.initialBar = -1
                    break
                }
            }
        }
        
        if type == .fine {
            for dsdc in dsDcs {
                if dsdc.fine == bar {
                    dsdc.fine = -1
                    break
                }
            }
        }
        
        if type == .dsAlFine {
            for dsdc in dsDcs {
                if dsdc.dsAlFine == bar {
                    dsdc.dsAlFine = -1
                    break
                }
            }
        }
        
        if type == .dsAlCoda {
            for dsdc in dsDcs {
                if dsdc.dsAlCoda == bar {
                    dsdc.dsAlCoda = -1
                    break
                }
            }
        }
        
        if type == .dcAlFine {
            for dsdc in dsDcs {
                if dsdc.dcAlFine == bar {
                    dsdc.dcAlFine = -1
                    break
                }
            }
        }
        
        if type == .dcAlCoda {
            for dsdc in dsDcs {
                if dsdc.dcAlCoda == bar {
                    dsdc.dcAlCoda = -1
                    break
                }
            }
        }
    }
    
    func returnKeyValue() -> Int{
        switch self.keySignature {
        case 0:
            return 1
        case 1:
            return 10
        case 2:
            return 4
        case 3:
            return 12
        case 4:
            return 6
        case 5:
            return 14
        case 6:
            return 8
        case 7:
            return 3
        case -1:
            return 7
        case -2:
            return 13
        case -3:
            return 5
        case -4:
            return 11
        case -5:
            return 3
        case -6:
            return 8
        default:
            return 1
        }
    }

    func changeKeySignature(newTonality: KeyType){
        for config in configurations {
            if config.bar == 1 {
                config.key = newTonality.rawValue
                break
            }
        }
        
    }

    func changeTonality(newTonality: KeyType){
        var oldKey = 0
        var newKey = 0
        var canChange = true
        for config in configurations {
            if config.bar == 1 {
                oldKey = self.getNumberTonality(tonality: config.key)
                newKey = self.getNumberTonality(tonality: newTonality.rawValue)
                break
            }
        }
        
        let distance = newKey - oldKey
        for note in notes {
            if (note.tone + distance) > MidiCode.b8.rawValue || (note.tone + distance) < 12 {
                canChange = false
                break
            }
        }
        
        if(canChange){
            for config in configurations {
                if config.bar == 1 {
                    config.key = newTonality.rawValue
                    break
                }
            }
            for note in notes {
                if (note.tone + distance) > MidiCode.b8.rawValue{
                    let newTone = note.tone + distance - 12
                    note.tone = newTone
                } else {
                    note.tone += distance
                }
            }
        }
       
    }

    var octave = 0
    func backToDefaultNotes(){
   
        if(octave > 0){
        for note in notes {
                note.tone -= self.octave
            }
             octave = 0
        }
        else if(octave < 0){
            for note in notes {
                note.tone += self.octave*(-1)
            }
             octave = 0
        }
    }
    
    var editOctave : Bool = true
   
    func plusOctaveTonality() -> Bool {
        for note in notes {
            if (note.tone + 12) < MidiCode.b8.rawValue {
                editOctave = true
            }else {
                editOctave = false
                return false
            }
        }
        if editOctave {
            self.octave += 12
            for note in notes {
                note.tone += 12
            }
        }
        return true
    }
    
    func minusOctaveTonality() -> Bool {
     
        for note in notes {
            if (note.tone - 12) > MidiCode.c0.rawValue {
                editOctave = true
            }else {
                editOctave = false
                return false
            }
        }
        if editOctave {
            self.octave -= 12
            for note in notes {
                note.tone -= 12
            }
        }
        
        return true
    }
    
    
    func getNumberTonality(tonality: Int) -> Int {
        switch tonality {
        case 1:
            return 1
        case 2:
            return 2
        case 3:
            return 2
        case 4:
            return 3
        case 5:
            return 4
        case 6:
            return 5
        case 7:
            return 6
        case 8:
            return 7
        case 9:
            return 7
        case 10:
            return 8
        case 11:
            return 9
        case 12:
            return 10
        case 13:
            return 11
        case 14:
            return 12
        default:
            return 0
        }
    }
    
    /// Divide uma nota em duas notas de tempos iguais a metade da inicial
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func divideNote(initialTime: Int, tone: Int, channel: Int) {
        for (i, note) in notes.enumerated() {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                if (note.durationQuantized / 2) == Int(note.durationQuantized / 2) {
                    let note1 = ModelNote()
                    note1.tone              = note.tone
                    note1.channel           = note.channel
                    note1.duration          = Int(note.durationQuantized / 2)
                    note1.initialTime       = note.initialTime
                    note1.tIniQuantized     = note.tIniQuantized
                    note1.durationQuantized = Int(note.durationQuantized / 2)
                    
                    let note2 = ModelNote()
                    note2.tone              = note.tone
                    note2.channel           = note.channel
                    note2.duration          = Int(note.durationQuantized / 2)
                    note2.initialTime       = note.initialTime   + Int(note.durationQuantized / 2)
                    note2.tIniQuantized     = note.tIniQuantized + Int(note.durationQuantized / 2)
                    note2.durationQuantized = Int(note.durationQuantized / 2)
                    
                    notes.append(note1)
                    notes.append(note2)
                    
                    notes.remove(at: i)
                }
                
                break
            }
        }
    }
    
    /// Divide uma nota e substitui por uma com a metade do seu tempo mais uma pausa com mesma duração
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func divideNoteAndPutNoteMorePause(initialTime: Int, tone: Int, channel: Int) {
        for (i, note) in notes.enumerated() {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                if (note.durationQuantized / 2) == Int(note.durationQuantized / 2) {
                    let note1 = ModelNote()
                    note1.tone              = note.tone
                    note1.channel           = note.channel
                    note1.duration          = Int(note.durationQuantized / 2)
                    note1.initialTime       = note.initialTime
                    note1.tIniQuantized     = note.tIniQuantized
                    note1.durationQuantized = Int(note.durationQuantized / 2)
                    
                    notes.append(note1)
                    
                    notes.remove(at: i)
                }
                
                break
            }
        }
    }
    
    /// Divide uma nota e substitui por uma pausa com a metade da sua duração mais uma pausa de mesma duração
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func divideNoteAndPutPauseMoreNote(initialTime: Int, tone: Int, channel: Int) {
        for (i, note) in notes.enumerated() {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                if (note.durationQuantized / 2) == Int(note.durationQuantized / 2) {
                    let note2 = ModelNote()
                    note2.tone              = note.tone
                    note2.channel           = note.channel
                    note2.duration          = Int(note.durationQuantized / 2)
                    note2.initialTime       = note.initialTime   + Int(note.durationQuantized / 2)
                    note2.tIniQuantized     = note.tIniQuantized + Int(note.durationQuantized / 2)
                    note2.durationQuantized = Int(note.durationQuantized / 2)
                    
                    notes.append(note2)
                    notes.remove(at: i)
                }
                
                break
            }
        }
    }
    
    /// Diminui duração da nota pela metade e prolonga a nota anterior com o tempo reduzido
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func deleteNoteAndExtendAfter(initialTime: Int, tone: Int, channel: Int) {
        var durationNote = 0
        
        //Procura pela nota selecionada
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                durationNote = note.durationQuantized
                break
            }
        }

        var is1440 = false
        if durationNote == 1440 {
            is1440 = true
            durationNote = 960
        }
        
        if (durationNote / 2) >= 240 {
            //Muda tempo inicial de todas as notas que tem tempo inicial igual ao da nota selecionada
            for note in notes {
                if note.tIniQuantized == initialTime {
                    if is1440 {
                        note.durationQuantized = 960
                    } else {
                        note.durationQuantized = durationNote / 2
                    }
                    
                    note.duration          = note.durationQuantized
                    note.tIniQuantized    += durationNote / 2
                    note.initialTime      = note.tIniQuantized
                }
            }
            
            //Encontra notas próximas anteriores a nota selecionada
            var lowerTime = 0
            for note in notes {
                if note.tIniQuantized < initialTime {
                    lowerTime = max(lowerTime, note.tIniQuantized)
                }
            }
            
            //Prolonga notas próximas a nota selecionada
            for note in notes {
                if note.tIniQuantized == lowerTime {
                    note.duration          = note.durationQuantized + (durationNote / 2)
                    note.durationQuantized = note.durationQuantized + (durationNote / 2)
                }
            }
        }
    }
    
    /// Diminui duração da nota pela metade, diminui o tempo inicial da próxima nota com o mesmo tempo e prolonga essa nota
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func deleteNoteAndExtendBefore(initialTime: Int, tone: Int, channel: Int) {
        notes.sort(by: {$1.tIniQuantized > $0.tIniQuantized})
        var durationNote = 0
        
        //Procura pela nota selecionada
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                durationNote = note.durationQuantized
                break
            }
        }
        
        var parametro = durationNote
        
        if parametro == 1440 {
            parametro = 960
        }
        
        if (durationNote / 2) >= 240 {
            //Muda tempo inicial de todas as notas que tem tempo inicial igual ao da nota selecionada
            for note in notes {
                if note.tIniQuantized == initialTime && note.channel == channel {
                    note.durationQuantized = (parametro / 2)
                    note.duration          = (parametro / 2)
                }
            }
            
            //Encontra notas próximas anteriores a nota selecionada
            var greaterTime = -1
            for note in notes {
                if note.tIniQuantized > initialTime {
                    greaterTime = note.tIniQuantized
                    break
                }
            }
            
            //Prolonga notas próximas a nota selecionada
            for note in notes {
                if note.tIniQuantized == greaterTime {
                    note.tIniQuantized     -= (parametro / 2)
                    note.initialTime       = note.tIniQuantized
                    note.durationQuantized += (parametro / 2)
                    note.duration          = note.durationQuantized
                }
            }
        }
    }
    
    /// Deleta nota da partitura
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func deleteNoteAndReplaceWithPause(realInitTime: Int, initialTime: Int, tone: Int, channel: Int) {
        for (i, note) in notes.enumerated() {
            if realInitTime == initialTime {
                if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                    notes.remove(at: i)
                    
                    break
                }
            } else if note.tIniQuantized == realInitTime && note.tone == tone {
                let d = (note.tIniQuantized + note.durationQuantized) - initialTime
                note.durationQuantized -= d
            }
        }
    }
    
    /// Deleta nota e prolonga nota anterior
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    func deleteNoteAndExtendNoteBefore(initialTime: Int, tone: Int, channel: Int) {
        var lowerTime = 0
        for note in notes {
            if note.tIniQuantized < initialTime && note.channel == channel {
                lowerTime = max(lowerTime, note.tIniQuantized)
            }
        }
        
        var durationMore = 0
        
        for (i, note) in notes.enumerated() {
            if note.tIniQuantized == initialTime && note.tone == tone && note.channel == channel {
                durationMore = note.durationQuantized
                notes.remove(at: i)
                
                break
            }
        }
        
        for note in notes {
            if note.tIniQuantized == lowerTime && note.channel == channel {
                note.durationQuantized += durationMore
                note.duration = note.durationQuantized
            }
        }
        
    }
    
    /// Altera tom da nota
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    ///   - newTone: nova tom da nota
    func changeNoteTone(initialTime: Int, tone: Int, newTone: Int) {
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone {
                note.tone = newTone
            }
        }
    }
    
    /// Altera canal da nota
    ///
    /// - Parameters:
    ///   - initialTime: tempo inicial da nota que se deseja dividir
    ///   - tone: tom da nota que se deseja dividir
    ///   - newChannel: novo canal da nota
    func changeNoteChannel(initialTime: Int, tone: Int, newChannel: Int) {
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone {
                note.channel = newChannel
            }
        }
    }
    
    func addNote(tone: Int, initialTime: Int, duration: Int, channel: Int) {
        let note = ModelNote()
        note.tone              = tone
        note.initialTime       = initialTime
        note.tIniQuantized     = initialTime
        note.duration          = duration
        note.durationQuantized = duration
        note.channel           = channel
        
        notes.append(note)
    }
    
    func addBemol(tone: Int, initialTime: Int) {
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone {
                if note.tone - 1 >= 12 {
                    note.tone = note.tone - 1
                }
            }
        }
    }
    
    func addSustain(tone: Int, initialTime: Int) {
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone {
                if note.tone + 1 <= 119 {
                    note.tone = note.tone + 1
                }
            }
        }
    }
    
    func addNatural(tone: Int, initialTime: Int) {
        for note in notes {
            if note.tIniQuantized == initialTime && note.tone == tone {
                let tone = DrawTone()
                tone.midi = MidiCode(rawValue: note.tone)!
                
                if tone.hasFlat {
                    note.tone += 1
                }
                if tone.hasSharp {
                    note.tone -= 1
                }
            }
        }
    }
    
    func addPause(initialTime: Int, duration: Int) {
        for note in notes {
            if note.tIniQuantized >= initialTime {
                note.tIniQuantized += duration
                note.initialTime += duration
            }
        }
    }
    
    func getPercetagePlayed() -> Float {
        let numberOfNotes = Float(checkNumberOfNotesUntilFinishedTime())
        
        if numberOfNotes != 0.0 {
            return Float(verifyCorrectsNotes()) / numberOfNotes
        } else {
            return 0.0
        }
        
    }
    
    func getPercetagePartiture() -> Float {
        let numberOfNotes = Float(getTotalNumberOfNotes())
        
        if numberOfNotes != 0.0 {
            return Float(verifyCorrectsNotes()) / numberOfNotes
        } else {
            return 0.0
        }
    }
    
    private func getTotalNumberOfNotes() -> Int {
        return notes.count
    }
    
    private func checkNumberOfNotesUntilFinishedTime() -> Int {
        var correctsNotes = 0
        
        for note in notes {
            for played in playedNotes {
                if note.tIniQuantized == played.tIniQuantized && note.tone == played.tone && note.tIniQuantized <= timeFinished {
                    correctsNotes += 1
                }
            }
        }
        
        return correctsNotes
    }
    
    private func verifyCorrectsNotes() -> Int {
        var correctsNotes = 0
        
        for note in notes {
            for played in playedNotes {
                if note.tIniQuantized == played.tIniQuantized && note.tone == played.tone {
                    correctsNotes += 1
                }
            }
        }
        
        return correctsNotes
    }
    
    func calculateQuantization() {
        var lowerNote = 1000000
        for note in notes {
            lowerNote = min(lowerNote, note.duration)
//            print(note.durationQuantized)
        }
        
        if lowerNote <= 240 {
            suggestedQuantization = 16
            return
        } else if lowerNote <= 480 {
            suggestedQuantization = 8
            return
        } else if lowerNote <= 960 {
            suggestedQuantization = 4
            return
        }
    }
    
    func verifyCorrectsNotes() {
        for i in 0..<playedNotes.count {
            playedNotes[i].isCorrect = false
        }
        
        for i in 0..<notes.count {
            for j in 0..<playedNotes.count {
                if notes[i].tIniQuantized == playedNotes[j].tIniQuantized && notes[i].tone == playedNotes[j].tone {
                    playedNotes[j].isCorrect = true
                }
            }
        }
    }
    
    func printPauses() {
        for pause in pauses {
            print(pause.getChannel(), pause.getStartTick(), pause.getEndTick() - pause.getStartTick())
        }
    }
    
    func deleteBar(tStart: Int, tEnd: Int) {
        var i = 0
        
        while i < self.notes.count {
            if self.notes[i].tIniQuantized >= tStart && self.notes[i].tIniQuantized < tEnd {
                self.notes.remove(at: i)
            } else {
                if self.notes[i].tIniQuantized >= tEnd {
                    self.notes[i].tIniQuantized -= tEnd - tStart
                }
                i += 1
            }
        }
    }
}

struct GroupChannelNotes {
    var channel     = 0
    var initialTime = 0
    var notes       : [ModelNote] = []
}

struct NoteAfterRepetition {
    var timeToDecrease = 0
    var notes: [ModelNote] = []
}
