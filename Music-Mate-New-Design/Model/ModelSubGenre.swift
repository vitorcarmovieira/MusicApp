//
//  ModelSubGenre.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 2/23/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class ModelSubGenre {
    var id          = ""
    var name        = ""
    var description = ""
    var idGenre     = ""
}
