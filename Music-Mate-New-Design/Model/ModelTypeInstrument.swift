//
//  ModelTypeInstrument.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/10/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class ModelTypeInstrument {
    var id     = ""
    var status = 0
    var midiRangeInitial = 0
    var midiRangeFinal   = 0
    var name             = ""
    var tonality         = 1
    var clef              = 1
    var midiTransform    = 0
    var type             = 0
    var instrumentCategory       = ModelInstrument()

    func getPartitureTonalityForInstrument(_ transform : Int, _ scoreKey : Int) -> Int{
        var keyConv = 0
        switch scoreKey{
        case 1 :
            keyConv = 1
        case 2 :
            keyConv = 2
        case 4 :
            keyConv = 3
        case 5 :
            keyConv = 4
        case 6 :
            keyConv = 5
        case 7 :
            keyConv = 6
        case 8 :
            keyConv = 7
        case 10 :
            keyConv = 8
        case 11 :
            keyConv = 9
        case 12 :
            keyConv = 10
        case 13 :
            keyConv = 11
        case 14 :
            keyConv = 12
        default:
            keyConv = 1
        }
            if(transform > 11 || transform < -11 || transform == 0){
            return scoreKey
            }
            else {
            let newKey = keyConv+transform
                switch newKey {
                case 11:
                    return 13
                case 10:
                    return 12
                case 9:
                    return 11
                case 8:
                    return 10
                case 7:
                    return 9
                case 6:
                    return 7
                case 5:
                    return 6
                case 4:
                    return 5
                case 3:
                    return 4
                case 2:
                    return 2
                case 1:
                    return 1
                case 0:
                    return 14
                case -1:
                    return 13
                case -2:
                    return 12
                case -3:
                    return 11
                case -4:
                    return 10
                case -5:
                    return 9
                case -6:
                    return 7
                case -7:
                    return 6
                case -8:
                    return 5
                case -9:
                    return 4
                case -10:
                    return 2
                case -11:
                    return 1
                default:
                    return 1
                }
            }
        
       
        
    }

}
