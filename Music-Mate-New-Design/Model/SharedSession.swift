//
//  SharedSession.swift
//  Music-Mate-New-Design
//
//  Created by Thatielen Oliveira on 20/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

//
//  SharedSession.swift
//  Music Mate
//
//  Created by Mario Jorge Maciel on 01/08/17.
//  Copyright © 2017 Mario Jorge Maciel. All rights reserved.
//

class UserManager {
    
    // MARK: - Properties
    
    var id: Int
    var firstName: String
    var lastName: String
    var email: String
    var token: String
    var gender: Int
    var urlProfilePicture: String
    var tipo: Int
    var status: Bool
    var server: String
    var image: String
    
    static let sharedUserManager = UserManager()
    
    static let url = "http://10.0.1.100"
    
    // Initialization
    
    
    private static func sharedInstanceWith (id: Int, name: String, email: String, token: String, gender: Int, urlProfilePicture: String, tipo: Int, status: Bool, server: String, image: String, lastName: String) {
        let instance = UserManager.sharedUserManager
        
        instance.id = id
        instance.firstName = name
        instance.email = email
        instance.token = token
        instance.gender = gender
        instance.urlProfilePicture = urlProfilePicture
        instance.tipo = tipo
        instance.status = status
        instance.server = server
        instance.image = image
        instance.lastName = lastName
    }
    
    private init (id: Int=0, name: String="", email: String="", token: String="", gender: Int=0, urlProfilePicture: String="", tipo: Int=0, status: Bool=false, server: String = url, image: String="", lastName: String = "") {
        self.id = id
        self.firstName = name
        self.email = email
        self.token = token
        self.gender = gender
        self.urlProfilePicture = urlProfilePicture
        self.tipo = tipo
        self.status = status
        self.server = server
        self.image = image
        self.lastName = lastName
    }
    
}

//let user1 = UserSingle.sharedInstance
//let user2 = UserSingle.sharedInstanceWith("BJ", email: "xyz@gmail.com", userID: 12)
//user1.userID  //12

