//
//  ModelNotePlayed.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelNotePlayed {
    var id                = 0
    var channel           = 0
    var tone              = 0
    var initialTime       = 0
    var tIniQuantized     = 0
    var duration          = 0
    var durationQuantized = 0
    var intensity         = 0
    var bar               = 0
    var placeInBar        = 0
    var isToDelete        = false
    var isCorrect         = false
}
