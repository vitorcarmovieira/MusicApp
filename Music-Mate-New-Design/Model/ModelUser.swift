//
//  ModelUser.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelUser {
    var id         = ""
    var email      = ""
    var password   = ""
    var firstName   = ""
    var lastName   = ""
    var birthDate   = ""
    var image      = ""
    var status     = 0
    var interests  = ""
    var token      = ""
    var genre      = 0
    var country    = ModelCountry()
    var instrument = ModelInstrument()
    var typeInst   = ModelTypeInstrument()
}
