//
//  ModelBarConfiguration.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class ModelBarConfiguration {
    var id          = 0
    var bar         = 0
    var numerator   = 4
    var denominator = 4
    var clef        = 1
    var metronome   = 90
    var key         = 1
    var keyType     = 1
    var idPartitura = 1
}
