//
//  LeitorWAV.swift
//  TesteMic
//
//  Created by Gabriel Bruno on 10/5/16.
//  Copyright © 2016 LTMDEV. All rights reserved.
//

import AVFoundation

class WAVReader {
    
    private var arquivo = ""
    private var extensao = ""
    
    var TamanhoDaJanela : UInt32 = 8820
    
    func pegaAmplitudesWAV(arquivo : String) -> (taxaDeAmostragem: Int, amostras: [Float]){
        self.arquivo = arquivo
        self.extensao = "wav"
        return lerAquivoEmUmUnicoVetor()
    }
    
    func pegaJanelasWAV(arquivo : String) -> (taxaDeAmostragem: Int, amostras: [[Float]]){
        self.arquivo = arquivo
        self.extensao = "wav"
        return lerAquivoEmJanelas()
    }
    
    private func lerAquivoEmJanelas() -> (Int, [[Float]]) {
        
        let url = Bundle.main.url(forResource: arquivo, withExtension: extensao)
        
        let file = try! AVAudioFile(forReading: url!)
        
        let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: 1, interleaved: false)
        
        let buf = AVAudioPCMBuffer(pcmFormat: format!, frameCapacity: TamanhoDaJanela)
        
        var floatArray = [[Float]]()
        
        if let buffer = buf {
            while pegaJanela(file: file, buf: buffer){
                floatArray.append([Float](UnsafeBufferPointer(start: buffer.floatChannelData?[0], count:Int(buffer.frameLength))))
            }
            
            floatArray[floatArray.count - 1].append(contentsOf: [Float](repeating: 0.0, count: (Int(TamanhoDaJanela) - floatArray.last!.count)))
        }

        return (Int(file.fileFormat.sampleRate), floatArray)
    }
    
    
    
    func getCacheDirectory() -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        
        return paths[0]
        
    }
    
    func getFileURL(filePath: String) -> NSURL{
        
        let path  = getCacheDirectory().appending(filePath)
        
        let filePath = NSURL(fileURLWithPath: path)
        
        return filePath
    }
    
    
    private func lerAquivoEmUmUnicoVetor() -> (Int, [Float]) {
        
//        let url = getFileURL(filePath: "/" + self.arquivo)
        
        let file = try! AVAudioFile(forReading: URL(string: self.arquivo)!)
        
        
        var floatArray = [Float]()
        
        let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: 1, interleaved: false)
        
        
        if let formater = format {
            let buf = AVAudioPCMBuffer(pcmFormat: formater, frameCapacity: TamanhoDaJanela)

            floatArray.removeAll()
            
            if let buffer = buf {
                while pegaJanela(file: file, buf: buffer){
                    floatArray.append(contentsOf: ([Float](UnsafeBufferPointer(start: buffer.floatChannelData?[0], count:Int(buffer.frameLength)))))
                }
            }
        }
        
        return (Int(file.fileFormat.sampleRate), floatArray)
    }
    
    private func pegaJanela(file : AVAudioFile, buf : AVAudioPCMBuffer) -> Bool{
        do {
            try file.read(into: buf)
            return true
        } catch {
            return false
        }
    }
}
