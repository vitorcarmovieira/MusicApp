//
//  ExtString.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/15/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[Range(start ..< end)])
    }
}
