//
//  ServiceRemote.swift
//  Music-Mate-New-Design
//
//  Created by Thatielen Oliveira on 17/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class ServiceRemote{
    
    enum Method:String {
        case POST = "POST"
        case GET = "GET"
    }
    
    static func getServerRemote() -> String {
        return "http://10.0.1.100"
    }

    func service(urlToRequest: String, method: String) -> String{
        var json:String = ""
            let urluse = URL(string: urlToRequest)
            let session = URLSession.shared
        let request = NSMutableURLRequest(url: urluse!)
            request.httpMethod = method
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            let paramString = "data="
            request.httpBody = paramString.data(using: String.Encoding.utf8)
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let _: Data = data, let _: URLResponse = response, error == nil else {
                    print("*****error")
                    return
                }
                let dataString = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                //print("*****This is the data 4: \(dataString)") //JSONSerialization
                json = dataString! as String
                //return dataString
            }
            task.resume()
        print(json)
        return json
    }
}
