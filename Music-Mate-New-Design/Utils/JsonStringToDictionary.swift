//
//  JsonStringToDictionary.swift
//  Music-Mate-New-Design
//
//  Created by Thatielen Oliveira on 17/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation
class JsonStringToDictionary {
    
    func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
                print ("json:", json)
                return json
            } catch {
                print("Deu erro:",error.localizedDescription)
            }
        }
        return nil
    }
}
