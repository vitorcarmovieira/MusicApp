//
//  Validator.swift
//  Music-Mate-New-Design
//
//  Created by Thatielen Oliveira on 12/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation



class Validator {
    
    
    
    func validateDate( date : String ) -> Bool{
        
        let vls = date.components(separatedBy: "/")
        
        
        
        if vls.count == 3{
            
            
            
            if Int(vls[1]) == 02{
                
                return validateFebruary(day: vls[0], month: vls[1], year: vls[2])
                
                
                
            }
            
            
            
            if validateDayOfDate(day: vls[0]) && validateMonthOfDate(month: vls[1]) && validateYearOfDate(year: vls[2]){
                
                
                
                
                
                return true
                
            }
            
            
            
            
            
        }
        
        
        
        return false
        
    }
    
    
    
    func validateComposer( name : String ) -> Bool{
        
        
        
        
        
        //        if vls.count == 3{
        
        //
        
        //            if validateDayOfDate(day: vls[0]) && validateMonthOfDate(month: vls[1]) && validateYearOfDate(year: vls[2]){
        
        //
        
        //                return true
        
        //            }
        
        //
        
        //
        
        //        }
        
        
        
        return false
        
    }
    
    
    
    private func validateDayOfDate( day : String ) -> Bool{
        
        
        
        if let n = Int(day){
            
            if n > 0 && n < 32{
                
                return true
                
            }
            
            
            
        }
        
        
        
        return false
        
        
        
    }
    
    
    
    
    
    private func validateMonthOfDate( month : String ) -> Bool{
        
        
        
        if let n = Int(month){
            
            if n > 0 && n < 13{
                
                return true
                
            }
            
            
            
        }
        
        
        
        return false
        
        
        
    }
    
    
    
    
    
    private func validateYearOfDate( year : String ) -> Bool{
        
        
        
        if let n = Int(year){
            
            
            
            let date = Date()
            
            let calendar = Calendar.current
            
            let components = calendar.dateComponents([.year], from: date)
            
            
            
            if let year = components.year {
                
                if n <= year {
                    
                    return true
                    
                }
                
            }
            
            
            
        }
        
        
        
        return false
        
        
        
    }
    
    
    
    private func validateFebruary( day : String, month: String, year: String  ) -> Bool{
        
        
        
        if let n = Int(year){
            
            if n%4 == 0{
                
                if let n = Int (day){
                    
                    if n < 30{
                        
                        return true
                        
                    }
                    
                }
                
                
                
            }else{
                
                if let n = Int (day){
                    
                    if n < 29{
                        
                        return true
                        
                    }
                    
                }
                
            }
            
            
            
        }
        
        
        
        return false
        
        
        
    }
    
    
    
}
