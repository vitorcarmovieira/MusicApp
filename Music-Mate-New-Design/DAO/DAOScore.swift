
//
//  DAOScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//
import UIKit

/// Instancia estática da classe DAOScore
let instanceScore = DAOScore()

/// Classe de persistência da partitura
class DAOScore: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOScore
    ///
    /// - Returns: instância da classe DAOScore
    class func getInstance() -> DAOScore {
        
        if instanceScore.dataBase == nil {
            instanceScore.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        
        return instanceScore
    }
    
    /// Adiciona uma nova partitura no banco de dados
    ///
    /// - Parameter model: modelo de partitura
    func addScore(_ model: ModelScore) {
        instanceScore.dataBase!.open()
        
        /// String de inserção de partitura
        let sql = "INSERT INTO mm_partitura (" +
            "part_titulo, "          +
            "part_dificuldade, "      +
            "part_subtitulo, "       +
            "part_metronomo, "       +
            "part_notabase, "        +
            "part_quantizacao, "     +
            "part_Copyright,"        +
            "part_tipo, "           +
            "part_status, "         +
            "part_compassoParado, "  +
            "part_artista, "        +
            "part_dataCriacao, "    +
            "part_idMidi, "         +
            "part_idLoja, "         +
            "gen_idGenero, "        +
            "sub_idSubgenero, "     +
            "part_isPoliphonic, "   +
            "pais_idPais, "        +
            "user_email, "         +
            "part_dataAlteracao)"   +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [model.title,
                                                                     model.difficulty,
                                                                     model.subtitle,
                                                                     model.metronome,
                                                                     model.baseNote,
                                                                     model.quantization,
                                                                     model.copyright,
                                                                     model.type,
                                                                     model.status,
                                                                     model.barStopped,
                                                                     model.arranger,
                                                                     model.createdyear,
                                                                     model.idMidi,
                                                                     model.idStore,
                                                                     model.genre.id,
                                                                     model.subgenre.id,
                                                                     model.isPoliphonic,
                                                                     model.country.id,
                                                                     model.user.email,
                                                                     model.changedDate])
        
        let idScore = getGreaterCode()
        
        for tag in model.tags {
            DAOTag.getInstance().addTag(tag, idScore)
        }
        
        DAOPublisher.getInstance().addPublisherToScore(model.copyright, idScore: idScore)
        
        for art in model.artist{
            DAOAuthorArtist.getInstance().addArtistToScore(art, idScore: idScore)
        }
        for aut in model.author{
            DAOAuthorArtist.getInstance().addAuthorToScore(aut, idScore: idScore)
        }
        
        
        for config in model.configurations {
            DAOBarConfiguration.getInstance().addConfigurationToScore(config, idScore: idScore)
        }
        
        for repetition in model.repeats {
            DAORepeat.getInstance().addRepetitionToScore(repetition, idScore: idScore)
        }
        
        for dsdc in model.dsDcs {
            DAODaCapoDelSegno.getInstance().addDsDcToScore(dsdc, idScore: idScore)
        }
        
        let instance = DAONote.getInstance()
        if let db = instanceNote.dataBase {
            db.open()
        }
        
        var sqlNote = ""
        for note in model.notes {
            instance.addNoteFromScore(note, idScore: idScore, sql: &sqlNote)
            
            sqlNote += " "
        }
        if let db = instanceNote.dataBase {
            db.executeStatements(sqlNote)
            //            db.executeUpdate(sqlNote, withArgumentsIn: nil)
            db.close()
        }
        
        instanceScore.dataBase!.close()
    }
    
    func updateStatusScore(_ scoreId: Int, _ scoreType : Int){
        instanceScore.dataBase!.open()
        /// String de atualização de partitura
        let sql = "UPDATE mm_partitura SET part_status = ? WHERE part_id = ?"
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [scoreType, scoreId])
        instanceScore.dataBase!.close()
    }
    
    func createArrangement(_ model: ModelScore){
        instanceScore.dataBase!.open()
        let sql = "INSERT INTO mm_partitura (" +
            "part_titulo, "          +
            "part_dificuldade, "      +
            "part_subtitulo, "       +
            "part_metronomo, "       +
            "part_notabase, "        +
            "part_quantizacao, "     +
            "part_tipo, "            +
            "part_status, "          +
            "part_compassoParado, "  +
            "part_artista, "         +
            "part_dataCriacao, "     +
            "part_idMidi, "          +
            "part_idLoja, "          +
            "gen_idGenero, "         +
            "sub_idSubgenero, "      +
            "pais_idPais, "          +
            "part_dataAlteracao, "    +
            "user_email, "           +
            "part_isPoliphonic, "           +
            "part_idPartOriginal)"    +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [model.title,
                                                                     model.difficulty,
                                                                     model.subtitle,
                                                                     model.metronome,
                                                                     model.baseNote,
                                                                     model.quantization,
                                                                     model.type,
                                                                     model.status,
                                                                     model.barStopped,
                                                                     model.arranger,
                                                                     model.createdyear,
                                                                     model.idMidi,
                                                                     model.idStore,
                                                                     model.genre.id,
                                                                     model.subgenre.id,
                                                                     model.country.id,
                                                                     model.changedDate,
                                                                     model.user.email,
                                                                     model.isPoliphonic,
                                                                     model.idPartOriginal])
        
        let idScore = getGreaterCode()
        
        for config in model.configurations {
            DAOBarConfiguration.getInstance().addConfigurationToScore(config, idScore: idScore)
        }
        
        
        for repetition in model.repeats {
            DAORepeat.getInstance().addRepetitionToScore(repetition, idScore: idScore)
        }
        
        for dsdc in model.dsDcs {
            DAODaCapoDelSegno.getInstance().addDsDcToScore(dsdc, idScore: idScore)
        }
        
        let instance = DAONote.getInstance()
        if let db = instanceNote.dataBase {
            db.open()
        }
        
        var sqlNote = ""
        for note in model.notes {
            instance.addNoteFromScore(note, idScore: idScore, sql: &sqlNote)
            
            sqlNote += " "
        }
        if let db = instanceNote.dataBase {
            db.executeStatements(sqlNote)
            //            db.executeUpdate(sqlNote, withArgumentsIn: nil)
            db.close()
        }
        
        instanceScore.dataBase!.close()
    }
    
    func updateScore(_ model: ModelScore) {
        instanceScore.dataBase!.open()
        /// String de atualização de partitura
        let sql = "UPDATE mm_partitura SET " +
            "part_titulo         = ?,"       +
            "part_dificuldade    = ?,"             +
            "part_subtitulo      = ?,"       +
            "part_metronomo      = ?,"       +
            "part_notabase       = ?,"       +
            "part_quantizacao    = ?,"       +
            "part_Copyright    = ?,"       +
            "part_tipo           = ?,"       +
            "part_status         = ?,"       +
            "part_compassoParado = ?,"       +
            "part_artista        = ?,"       +
            "part_dataCriacao    = ?,"       +
            "part_dataAlteracao  = ?,"       +
            "part_idMidi         = ?,"       +
            "part_idLoja         = ?,"       +
            "gen_idGenero        = ?,"       +
            "sub_idSubgenero     = ?,"       +
            "part_isPoliphonic     = ?,"       +
            "pais_idPais         = ?"        +
            "WHERE part_id = ?"
        
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [model.title,
                                                                     model.difficulty,
                                                                     model.subtitle,
                                                                     model.metronome,
                                                                     model.baseNote,
                                                                     model.quantization,
                                                                     model.copyright,
                                                                     model.type,
                                                                     model.status,
                                                                     model.barStopped,
                                                                     model.arranger,
                                                                     model.createdyear,
                                                                     model.changedDate,
                                                                     model.idMidi,
                                                                     model.idStore,
                                                                     model.genre.id,
                                                                     model.subgenre.id,
                                                                     model.isPoliphonic,
                                                                     model.country.id,
                                                                     model.id])
        
        
        DAOBarConfiguration.getInstance().removeConfigurationsFromScore(idScore: model.id)
        DAORepeat.getInstance().removeRepeatsFromScore(idScore: model.id)
        DAODaCapoDelSegno.getInstance().removeDSDCsFromScore(idScore: model.id)
        DAONote.getInstance().removeNotesFromScore(idScore: model.id)
        DAOAuthorArtist.getInstance().removeAuthor(idScore: model.id)
        DAOPublisher.getInstance().removePublisher(idScore: model.id)
        
        DAOPublisher.getInstance().addPublisherToScore(model.copyright, idScore: model.id)
        
        for art in model.artist{
            DAOAuthorArtist.getInstance().addArtistToScore(art, idScore: model.id)
        }
        for aut in model.author{
            DAOAuthorArtist.getInstance().addAuthorToScore(aut, idScore: model.id)
        }
        
        for tag in model.tags {
            DAOTag.getInstance().addTag(tag, model.id)
        }
        
        for config in model.configurations {
            DAOBarConfiguration.getInstance().addConfigurationToScore(config, idScore: model.id)
        }
        
        for repetition in model.repeats {
            DAORepeat.getInstance().addRepetitionToScore(repetition, idScore: model.id)
        }
        
        for dsdc in model.dsDcs {
            DAODaCapoDelSegno.getInstance().addDsDcToScore(dsdc, idScore: model.id)
        }
        
        let instance = DAONote.getInstance()
        if let db = instanceNote.dataBase {
            db.open()
        }
        
        var sqlNote = ""
        for note in model.notes {
            instance.addNoteFromScore(note, idScore: model.id, sql: &sqlNote)
            
            sqlNote += " "
        }
        if let db = instanceNote.dataBase {
            db.executeStatements(sqlNote)
            //            db.executeUpdate(sqlNote, withArgumentsIn: nil)
            db.close()
        }
        //        for note in model.notes {
        //            DAONote.getInstance().addNoteToScore(note, idScore: model.id)
        //        }
        
        instanceScore.dataBase!.close()
    }
    
    func deleteScore(_ scoreId: Int){
         instanceScore.dataBase!.open()
        let sql = "DELETE FROM mm_partitura WHERE part_id = ?"
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [scoreId])
        instanceScore.dataBase!.close()
    }
    
    func getScores(_ local: String, email: String, busca : String) -> [ModelScore] {
        
        instanceScore.dataBase!.open()
        var consulta = " "
        switch local {
        case "Search All MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "All MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND user_email=?"

        case "Search Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=2 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=2 AND user_email=?"

        case "Search Arrangements MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=3 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Arrangements MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=3 AND user_email=?"

        case "Search Store MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=1 AND user_email=?  AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Store MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND part_tipo=1 AND user_email=?"

        case "Search Store and Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=1 OR part_tipo=2) AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Store and Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=1 OR part_tipo=2) AND user_email=?"

        case "Search Store and Arrangements MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=1 OR part_tipo=3) AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Store and Arrangements MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=1 OR part_tipo=3) AND user_email=?"

        case "Search Arrangements and Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=2 OR part_tipo=3) AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Arrangements and Compositions MyMusic":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=1 AND (part_tipo=2 OR part_tipo=3) AND user_email=?"

        case "Search All MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "All MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND user_email=?"

        case "Search Compositions MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND part_tipo=2 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Compositions MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND part_tipo=2 AND user_email=?"

        case "Search Arrangements MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND part_tipo=3 AND user_email=? AND part_titulo LIKE '%\(busca)%' ORDER BY part_titulo"
        case "Arrangements MyCompositions":
            consulta = "SELECT * FROM mm_partitura WHERE part_status=2 AND part_tipo=3 AND user_email=?"
        default:
            consulta = "SELECT * FROM mm_partitura AND user_email=?"
        }

        let sql = consulta
        let result: FMResultSet! = instanceScore.dataBase!.executeQuery(sql, withArgumentsIn: [email])

        var models: [ModelScore] = []

        if (result != nil) {
            while result.next() {
                let model = ModelScore()

                
                model.id             = Int(result.int(forColumn: "part_id"))
                model.title          = result.string(forColumn: "part_titulo")!
                model.difficulty    = Int(result.int(forColumn: "part_dificuldade"))
                model.subtitle       = result.string(forColumn: "part_subtitulo")!
                model.createdyear = result.string(forColumn: "part_dataCriacao")!
                model.quantization   = Int(result.int(forColumn: "part_quantizacao"))
                model.metronome      = Int(result.int(forColumn: "part_metronomo"))
                model.baseNote       = Int(result.int(forColumn: "part_notabase"))
                model.idMidi         = result.string(forColumn: "part_idMidi")!
                model.idStore        = result.string(forColumn: "part_idLoja")!
                model.status        = Int(result.int(forColumn: "part_status"))
                model.type          = Int(result.int(forColumn: "part_tipo"))
                model.keySignature = Int(result.int(forColumn: "part_tipo"))
                model.isPoliphonic = Int(result.int(forColumn: "part_isPoliphonic"))

                let idGenre = result.string(forColumn: "gen_idGenero")
                if idGenre != nil {
                    model.genre = DAOGenre.getInstance().getGender(with: idGenre!)
                }

                let idSubGenre = result.string(forColumn: "sub_idSubgenero")
                if idSubGenre != nil {
                    model.subgenre = DAOSubGenre.getInstance().getSubGender(with: idSubGenre!)
                }

                let idCountry = result.string(forColumn: "pais_idPais")
                if idCountry != nil {
                    model.country = DAOCountry.getInstance().getCountry(with: idCountry!)
                }

                model.copyright = DAOPublisher.getInstance().getPublisherFromScoreId(model.id)
                model.configurations = DAOBarConfiguration.getInstance().getConfigurationsFromScoreId(model.id)
                model.tags = DAOTag.getInstance().getTagById(model.id)
                model.notes          = DAONote.getInstance().getNotesFromScoreId(model.id)
                model.repeats        = DAORepeat.getInstance().getRepetitionsFromScoreId(model.id)
                model.dsDcs          = DAODaCapoDelSegno.getInstance().getDsDcsFromScoreId(model.id)
                model.author         = DAOAuthorArtist.getInstance().getAuthorFromScoreId(model.id)
                model.artist         = DAOAuthorArtist.getInstance().getArtistFromScoreId(model.id)
                model.inserted = true

                models.append(model)
            }
        }
        
        instanceScore.dataBase!.close()
        
        return models
    }
    
    func getGreaterCode() -> Int {
        instanceScore.dataBase!.open()
        let test = ""
        let resultSet: FMResultSet! = instanceScore.dataBase!.executeQuery("SELECT MAX(part_id) AS part_codigo FROM mm_partitura", withArgumentsIn: [test])
        
        if (resultSet != nil) {
            while resultSet.next() {
                let code = Int(resultSet.int(forColumn: "part_codigo"))
                instanceScore.dataBase!.close()
                return code
            }
        }
        instanceScore.dataBase!.close()
        
        return 0
    }
    
    /// Deleta uma nova partitura no banco de dados
    ///
    /// - Parameter model: modelo de partitura
    func deleteScore(_ model: ModelScore) {
        instanceScore.dataBase!.open()
        
        /// String de inserção de partitura
        let sql = "DELETE FROM mm_partitura WHERE part_id = ?"
        
        instanceScore.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id])
        
        instanceScore.dataBase!.close()
    }
}
