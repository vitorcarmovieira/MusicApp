//
//  DAOSubGenre.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 2/23/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOInstrument
let instanceSubGenre = DAOSubGenre()

/// Classe de persistência do instrumento
class DAOSubGenre: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOInstrument
    ///
    /// - Returns: instância da classe DAOInstrument
    class func getInstance() -> DAOSubGenre {
        
        if instanceSubGenre.dataBase == nil {
            instanceSubGenre.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
         instanceSubGenre.dataBase!.open()
        return instanceSubGenre
    }
    
    /// Adiciona um novo instrumento no banco de dados
    ///
    /// - Parameter model: modelo de instrumento
    func addSubGenre(_ model: ModelSubGenre) {
        instanceSubGenre.dataBase!.open()
        
        /// String de inserção de instrumento
        let sql = "INSERT INTO mm_subgenero (" +
                "sub_id, "                     +
                "sub_nome, "                   +
                "gen_id) "                     +
                "VALUES (?, ?, ?)"
        
       
        instanceSubGenre.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id,
                                                                          model.name,
                                                                          model.idGenre
                                                                          ])
        
        instanceSubGenre.dataBase!.close()
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getSubGenrs(with genreId: String) -> [ModelSubGenre] {
        instanceSubGenre.dataBase!.open()
        
        let sql = "SELECT * FROM mm_subgenero WHERE gen_id=?"
        let result: FMResultSet! = instanceSubGenre.dataBase!.executeQuery(sql, withArgumentsIn: [ genreId ])
        
        var models: [ModelSubGenre] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelSubGenre()
                model.id              = result.string(forColumn: "sub_id")!
                model.name            = result.string(forColumn: "sub_nome")!
                model.idGenre         = result.string(forColumn: "gen_id")!
                
                models.append(model)
            }
        }
        
        instanceSubGenre.dataBase!.close()
        
        return models
    }
    
    
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getSubGender(with id: String) -> ModelSubGenre {
      //  instanceSubGenre.dataBase!.open()
        
        let sql = "SELECT * FROM mm_subgenero WHERE sub_id=?"
        let result: FMResultSet! = instanceSubGenre.dataBase!.executeQuery(sql, withArgumentsIn: [ id ])
        
        let model = ModelSubGenre()
        
        if (result != nil) {
            while result.next() {
                model.id              = result.string(forColumn: "sub_id")!
                model.name            = result.string(forColumn: "sub_nome")!
                model.idGenre         = result.string(forColumn: "gen_id")!
            }
        }
      
     //   instanceSubGenre.dataBase!.close()
        
        return model
    }
}
