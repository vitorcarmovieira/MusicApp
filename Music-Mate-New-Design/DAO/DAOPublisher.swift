//
//  DAOPublisher.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 19/06/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

let instancePublisher = DAOPublisher()

/// Classe de persistência de configuracao de compasso
class DAOPublisher: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOBarConfiguration
    ///
    /// - Returns: instância da classe DAOBarConfiguration
    class func getInstance() -> DAOPublisher {
        
        if instancePublisher.dataBase == nil {
            instancePublisher.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instancePublisher.dataBase!.open()
        return instancePublisher
    }
    
    /// Adiciona uma nova configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func addPublisherToScore(_ model: ModelCopyright, idScore: Int) {
        instancePublisher.dataBase!.open()
        
        /// String de inserção de configuracao de compasso
        let sql = "INSERT INTO mm_produtora(" +
            "prod_nome, "                             +
            "prod_idLoja, "                            +
            "part_idPartitura)"                            +
        "VALUES (?, ?, ?)"
        
        instancePublisher.dataBase!.executeUpdate(sql, withArgumentsIn: [model.name,
                                                                            model.id,
                                                                            idScore,])
        
        instancePublisher.dataBase!.close()
    }
    
    /// Devolve configuracao de compasso pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: configuracoes com o id passado
    func getPublisherFromScoreId(_ id: Int) -> ModelCopyright {
        instancePublisher.dataBase!.open()
        
        let sql = "SELECT * FROM mm_produtora WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instancePublisher.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        let model = ModelCopyright()
        
        if (result != nil) {
            while result.next() {
                model.id          = result.string(forColumn: "prod_idLoja")!
                model.name         = result.string(forColumn: "prod_nome")!
            }
        }
        
        instancePublisher.dataBase!.close()
        
        return model
    }
    
 
    /// Remove uma configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removePublisher(idScore: Int) {
        instanceAuthorArtist.dataBase!.open()
        
        let sql = "DELETE FROM mm_produtora WHERE " + "part_idPartitura=" + "?"
        
        instanceAuthorArtist.dataBase!.executeUpdate(sql, withArgumentsIn: [ idScore ])
        
        instanceAuthorArtist.dataBase!.close()
    }
    
}
