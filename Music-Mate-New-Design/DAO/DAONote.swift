//
//  DAONote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAONote
let instanceNote = DAONote()

/// Classe de persistência de notas
class DAONote: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAONote
    ///
    /// - Returns: instância da classe DAONote
    class func getInstance() -> DAONote {
        
        if instanceNote.dataBase == nil {
            instanceNote.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
         instanceNote.dataBase!.open()
        return instanceNote
    }
    
    func addNoteFromScore(_ model: ModelNote, idScore: Int, sql: inout String) {
        sql += "INSERT INTO mm_nota ("    +
            "nota_canal, "                   +
            "nota_tom, "                     +
            "nota_tempoInicial, "            +
            "nota_tempoInicialQuantizado, "  +
            "nota_duracao, "                 +
            "nota_duracaoQuantizada, "       +
            "part_idPartitura) "             +
        "VALUES (\(model.channel), \(model.tone), \(model.initialTime), \(model.tIniQuantized), \(model.duration), \(model.durationQuantized), \(idScore));"
    }
    
    /// Adiciona uma nova nota no banco de dados
    ///
    /// - Parameter model: modelo de nota
    func addNoteToScore(_ model: ModelNote, idScore: Int) {
        
        
        /// String de inserção de note
        let sql = "INSERT INTO mm_nota ("    +
            "nota_canal, "                   +
            "nota_tom, "                     +
            "nota_tempoInicial, "            +
            "nota_tempoInicialQuantizado, "  +
            "nota_duracao, "                 +
            "nota_duracaoQuantizada, "       +
            "part_idPartitura) "             +
            "VALUES (?, ?, ?, ?, ?, ?, ?)"
        
        instanceNote.dataBase!.executeUpdate(sql, withArgumentsIn: [model.channel,
                                                                      model.tone,
                                                                      model.initialTime,
                                                                      model.tIniQuantized,
                                                                      model.duration,
                                                                      model.durationQuantized,
                                                                      idScore])
        
        
    }
    
    /// Devolve notas pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: notas com o id passado
    func getNotesFromScoreId(_ id: Int) -> [ModelNote] {
   //     instanceNote.dataBase!.open()
        
        let sql = "SELECT * FROM mm_nota WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instanceNote.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelNote] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelNote()
                
                model.id                = Int(result.int(forColumn: "nota_id"))
                model.channel           = Int(result.int(forColumn: "nota_canal"))
                model.tone              = Int(result.int(forColumn: "nota_tom"))
                model.initialTime       = Int(result.int(forColumn: "nota_tempoInicial"))
                model.tIniQuantized     = Int(result.int(forColumn: "nota_tempoInicialQuantizado"))
                model.duration          = Int(result.int(forColumn: "nota_duracao"))
                model.durationQuantized = Int(result.int(forColumn: "nota_duracaoQuantizada"))
                model.idScore           = Int(result.int(forColumn: "part_idPartitura"))
                
                models.append(model)
            }
        }
     
      //  instanceNote.dataBase!.close()
        
        return models
    }
    
    /// Remove notas de uma partitura no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removeNotesFromScore(idScore: Int) {
        instanceNote.dataBase!.open()
        
        let sql = "DELETE FROM mm_nota WHERE " + "part_idPartitura=\(idScore)"
        
        instanceNote.dataBase!.executeUpdate(sql, withArgumentsIn: [""])
        
        instanceNote.dataBase!.close()
    }
}
