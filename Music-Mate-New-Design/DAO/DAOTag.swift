//
//  DAOTag.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 02/06/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit


let instanceTag = DAOTag()

/// Classe de persistência do usuario
class DAOTag: NSObject {
    
    var dataBase: FMDatabase? = nil
    

    class func getInstance() -> DAOTag {
        
        if instanceTag.dataBase == nil {
            instanceTag.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        
        return instanceTag
    }
    
    /// Adiciona um novo usuario no banco de dados
    ///
    /// - Parameter model: modelo de usuario
    func addTag(_ model: ModelTag, _ idScore : Int) {
        instanceTag.dataBase!.open()
        
        /// String de inserção de usuario
        let sql = "INSERT INTO mm_tag (" +
            "tag_name, "                    +
            "part_idPartitura) "                    +
        "VALUES (?, ?)"
        
        print(model.name)
        print(idScore)
        instanceTag.dataBase!.executeUpdate(sql, withArgumentsIn: [model.name,
                                                             idScore,
                                                                   ])
        
        instanceTag.dataBase!.close()
    }
    
      func getTagById(_ id: Int) -> [ModelTag] {
        instanceTag.dataBase!.open()
        
        let sql = "SELECT * FROM mm_tag WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instanceTag.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var model :[ModelTag] = []
        
        if (result != nil) {
            while result.next() {
                var mod = ModelTag()
                mod.name        = result.string(forColumn: "tag_name")!
                mod.idPartitura        = Int(result.string(forColumn: "part_idPartitura")!)
                model.append(mod)
                instanceTag.dataBase!.close()
            }
        }
        
        return model
    }
}
