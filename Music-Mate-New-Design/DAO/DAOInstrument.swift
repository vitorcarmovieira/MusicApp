//
//  DAOInstrument.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOInstrument
let instanceInstrument = DAOInstrument()

/// Classe de persistência do instrumento
class DAOInstrument: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOInstrument
    ///
    /// - Returns: instância da classe DAOInstrument
    class func getInstance() -> DAOInstrument {
        
        if instanceInstrument.dataBase == nil {
            instanceInstrument.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        
        return instanceInstrument
    }
    
    /// Adiciona um novo instrumento no banco de dados
    ///
    /// - Parameter model: modelo de instrumento
    func addInstrument(_ model: ModelInstrument) {
        instanceInstrument.dataBase!.open()
        
        /// String de inserção de instrumento
        let sql = "INSERT INTO mm_instrumento (" +
            "inst_id, "                          +
            "inst_nome) "                        +
            "VALUES (?, ?)"
        
        instanceInstrument.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id,
                                                                          model.name])
        
        instanceInstrument.dataBase!.close()
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getInstrumentById(_ id: String) -> ModelInstrument {
        instanceInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_instrumento WHERE " + "inst_id=" + "?"
        let result: FMResultSet! = instanceInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        let model = ModelInstrument()
        
        if (result != nil) {
            while result.next() {
                model.id              = result.string(forColumn: "inst_id")!
                model.name            = result.string(forColumn: "inst_nome")!
 
                instanceInstrument.dataBase!.close()
            }
        }
        
        return model
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getAllInstruments() -> [ModelInstrument] {
        instanceInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_instrumento"
        let result: FMResultSet! = instanceInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [""])
        
        var models: [ModelInstrument] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelInstrument()
                model.id              = result.string(forColumn: "inst_id")!
                model.name            = result.string(forColumn: "inst_nome")!

                
                models.append(model)
            }
        }
        
        instanceInstrument.dataBase!.close()
        
        return models
    }
    
}
