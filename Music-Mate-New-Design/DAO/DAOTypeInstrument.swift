//
//  DAOTypeInstrument.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/28/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOInstrument
let instanceTypeInstrument = DAOTypeInstrument()

/// Classe de persistência do instrumento
class DAOTypeInstrument: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOInstrument
    ///
    /// - Returns: instância da classe DAOInstrument
    class func getInstance() -> DAOTypeInstrument {
        
        if instanceTypeInstrument.dataBase == nil {
            instanceTypeInstrument.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        
        return instanceTypeInstrument
    }
    
    /// Adiciona um novo instrumento no banco de dados
    ///
    /// - Parameter model: modelo de instrumento
    func addInstrument(_ model: ModelTypeInstrument) {
        instanceTypeInstrument.dataBase!.open()
        
        /// String de inserção de instrumento
        let sql = "INSERT INTO mm_tipoInstrumento (" +
            "tipoInst_id, "                          +
            "tipoInst_name, "                        +
            "tipoInst_midiRangeInicial, "            +
            "tipoInst_midiRangeFinal, "              +
            "tipoInst_tonality, "                    +
            "tipoInst_clef, "                         +
            "tipoInst_midiTransform, "               +
            "tipoInst_status, "                      +
            "tipoInst_type, "                      +
            "inst_idInstrumento) "                   +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        instanceTypeInstrument.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id,
                                                                          model.name,
                                                                          model.midiRangeInitial,
                                                                          model.midiRangeFinal,
                                                                          model.tonality,
                                                                          model.clef,
                                                                          model.midiTransform,
                                                                          model.status,
                                                                          model.type,
                                                                model.instrumentCategory.id])
        
        instanceTypeInstrument.dataBase!.close()
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
//    func addAtribute(){
//        instanceTypeInstrument.dataBase!.open()
//       
//        let sql = "ALTER TABLE mm_tipoInstrumento ADD COLUMN tipoInst_active INTEGER;"
//      //  instanceTypeInstrument.dataBase!.executeQuery(sql, withArgumentsIn: nil)
//        instanceTypeInstrument.dataBase!.executeUpdate(sql, withArgumentsIn: nil)
//        instanceTypeInstrument.dataBase!.close()
//    }
//    
    func getTypesInstrumentsById(_ id: String) -> [ModelTypeInstrument] {
        instanceTypeInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_tipoInstrumento WHERE " + "inst_idInstrumento=" + "?"
        let result: FMResultSet! = instanceTypeInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelTypeInstrument] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelTypeInstrument()

                model.id               = result.string(forColumn: "tipoInst_id")!
                model.name             = result.string(forColumn: "tipoInst_name")!
                model.status           = Int(result.int(forColumn: "tipoInst_status"))
                model.midiRangeInitial = Int(result.int(forColumn: "tipoInst_midiRangeInicial"))
                model.midiRangeFinal   = Int(result.int(forColumn: "tipoInst_midiRangeFinal"))
                model.tonality         = Int(result.int(forColumn: "tipoInst_tonality"))
                model.clef              = Int(result.int(forColumn: "tipoInst_clef"))
                model.midiTransform    = Int(result.int(forColumn: "tipoInst_midiTransform"))
                model.type            = Int(result.int(forColumn: "tipoInst_type"))
                model.instrumentCategory = DAOInstrument.getInstance().getInstrumentById(id)
                models.append(model)
            }
        }
        
        instanceTypeInstrument.dataBase!.close()
        
        return models
    }
    
    func getTypeInstrumentById(_ id: String) -> ModelTypeInstrument {
        instanceTypeInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_tipoInstrumento WHERE " + "tipoInst_id=" + "?"
        let result: FMResultSet! = instanceTypeInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        let model = ModelTypeInstrument()
        
        if (result != nil) {
            while result.next() {
                model.id               = result.string(forColumn: "tipoInst_id")!
                model.name             = result.string(forColumn: "tipoInst_name")!
                model.status           = Int(result.int(forColumn: "tipoInst_status"))
                model.midiRangeInitial = Int(result.int(forColumn: "tipoInst_midiRangeInicial"))
                model.midiRangeFinal   = Int(result.int(forColumn: "tipoInst_midiRangeFinal"))
                model.tonality         = Int(result.int(forColumn: "tipoInst_tonality"))
                model.clef              = Int(result.int(forColumn: "tipoInst_clef"))
                model.midiTransform    = Int(result.int(forColumn: "tipoInst_midiTransform"))
                model.type            = Int(result.int(forColumn: "tipoInst_type"))
               
                
                model.instrumentCategory = DAOInstrument.getInstance().getInstrumentById(id)
                
                instanceTypeInstrument.dataBase!.close()
            }
        }
        
        return model
    }
    
    func getTypeInstrumentActive() -> ModelTypeInstrument {
        instanceTypeInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_tipoInstrumento WHERE " + "tipoInst_status=2"
        let result: FMResultSet! = instanceTypeInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [""])
        
        let model = ModelTypeInstrument()
        
        if (result != nil) {
            while result.next() {
                print("result: ",result)
                model.id               = result.string(forColumn: "tipoInst_id")!
                model.name             = result.string(forColumn: "tipoInst_name")!
                model.status           = Int(result.int(forColumn: "tipoInst_status"))
                model.midiRangeInitial = Int(result.int(forColumn: "tipoInst_midiRangeInicial"))
                model.midiRangeFinal   = Int(result.int(forColumn: "tipoInst_midiRangeFinal"))
                model.tonality         = Int(result.int(forColumn: "tipoInst_tonality"))
                model.clef              = Int(result.int(forColumn: "tipoInst_clef"))
                model.midiTransform    = Int(result.int(forColumn: "tipoInst_midiTransform"))
                model.type            = Int(result.int(forColumn: "tipoInst_type"))
                model.instrumentCategory = DAOInstrument.getInstance().getInstrumentById(result.string(forColumn: "inst_idInstrumento")!)
                
                instanceTypeInstrument.dataBase!.close()
            }
        }
        
        return model
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getAllTypesInstruments() -> [ModelTypeInstrument] {
        instanceTypeInstrument.dataBase!.open()
        
        let sql = "SELECT * FROM mm_tipoInstrumento"
        let result: FMResultSet! = instanceTypeInstrument.dataBase!.executeQuery(sql, withArgumentsIn: [""])
        
        var models: [ModelTypeInstrument] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelTypeInstrument()
                model.id               = result.string(forColumn: "tipoInst_id")!
                model.name             = result.string(forColumn: "tipoInst_name")!
                model.status           = Int(result.int(forColumn: "tipoInst_status"))
                model.midiRangeInitial = Int(result.int(forColumn: "tipoInst_midiRangeInicial"))
                model.midiRangeFinal   = Int(result.int(forColumn: "tipoInst_midiRangeFinal"))
                model.tonality         = Int(result.int(forColumn: "tipoInst_tonality"))
                model.clef              = Int(result.int(forColumn: "tipoInst_clef"))
                model.midiTransform    = Int(result.int(forColumn: "tipoInst_midiTransform"))
                model.type            = Int(result.int(forColumn: "tipoInst_type"))
                
                let idInstrument = result.string(forColumn: "inst_idInstrumento")
               
                model.instrumentCategory = DAOInstrument.getInstance().getInstrumentById(idInstrument!)
                
                models.append(model)
            }
        }
        
        instanceTypeInstrument.dataBase!.close()
        
        return models
    }
    
    func setInstrumentActive(model: ModelTypeInstrument) {
        instanceTypeInstrument.dataBase!.open()
        /// String de atualização de partitura
        let sql = "UPDATE mm_tipoInstrumento SET " +
            "tipoInst_status        = ? "       +
            "WHERE tipoInst_status = ?"
        
        instanceTypeInstrument.dataBase!.executeUpdate(sql, withArgumentsIn: [1, 2])
        
        let sql2 = "UPDATE mm_tipoInstrumento SET " +
            "tipoInst_status        = ? "       +
        "WHERE tipoInst_id = ?"
        
        instanceTypeInstrument.dataBase!.executeUpdate(sql2, withArgumentsIn: [2, model.id])
        instanceTypeInstrument.dataBase!.close()
    }
    
    
}
