//
//  DAOGenre.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOInstrument
let instanceGenre = DAOGenre()

/// Classe de persistência do instrumento
class DAOGenre: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOInstrument
    ///
    /// - Returns: instância da classe DAOInstrument
    class func getInstance() -> DAOGenre {
        
        if instanceGenre.dataBase == nil {
            instanceGenre.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instanceGenre.dataBase!.open()
        return instanceGenre
    }
    
    /// Adiciona um novo instrumento no banco de dados
    ///
    /// - Parameter model: modelo de instrumento
    func addGenre(_ model: ModelGenre) {
        instanceGenre.dataBase!.open()
        
        /// String de inserção de instrumento
        let sql = "INSERT INTO mm_genero (" +
            "gen_id, "                      +
            "gen_titulo, "                  +
            "gen_status, "                  +
            "gen_descricao)"                +
        "VALUES (?, ?, ?, ?)"
        
        instanceGenre.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id,
                                                                     model.name,
                                                                     0,
                                                                     model.description ])
        
        instanceGenre.dataBase!.close()
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getAllGenrs() -> [ModelGenre] {
        instanceGenre.dataBase!.open()
        
        let sql = "SELECT * FROM mm_genero"
        let result: FMResultSet! = instanceGenre.dataBase!.executeQuery(sql, withArgumentsIn: [""])
        
        var models: [ModelGenre] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelGenre()
                model.id              = result.string(forColumn: "gen_id")!
                model.name            = result.string(forColumn: "gen_titulo")!
                
                
                models.append(model)
            }
        }
        
        instanceGenre.dataBase!.close()
        
        return models
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getGender(with id: String) -> ModelGenre {
//        instanceGenre.dataBase!.open()
        
        let sql = "SELECT * FROM mm_genero WHERE gen_id=?"
        let result: FMResultSet! = instanceGenre.dataBase!.executeQuery(sql, withArgumentsIn: [ id ])
        
        let model = ModelGenre()
        
        if (result != nil) {
            while result.next() {
                model.id              = result.string(forColumn: "gen_id")!
                model.name            = result.string(forColumn: "gen_titulo")!
            }
        }
     
//        instanceGenre.dataBase!.close()
        
        return model
    }
    
}

