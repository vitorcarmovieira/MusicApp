//
//  DAOUser.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOUser
let instanceUser = DAOUser()

/// Classe de persistência do usuario
class DAOUser: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOUser
    ///
    /// - Returns: instância da classe DAOUser
    class func getInstance() -> DAOUser {

        if instanceUser.dataBase == nil {
            instanceUser.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        
        return instanceUser
    }
    
    /// Adiciona um novo usuario no banco de dados
    ///
    /// - Parameter model: modelo de usuario
    func addUser(_ model: ModelUser) {
        instanceUser.dataBase!.open()
        
        /// String de inserção de usuario
        let sql = "INSERT INTO mm_user (" +
            "usu_email, "                    +
            "usu_password, "                    +
            "usu_firstName, "                     +
            "usu_lastName, "                +
            "usu_birthDate, "              +
            "usu_image, "                +
            "usu_status, "                   +
            "usu_interesses, "               +
            "usu_genre, "                   +
            "pais_idCountry, "                  +
            "inst_idInstrument, "           +
            "tipoInst_idTipo, "              +
            "usu_token) "                    +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        instanceUser.dataBase!.executeUpdate(sql, withArgumentsIn: [model.email,
                                                                    model.password,
                                                                    model.firstName,
                                                                    model.lastName,
                                                                    model.birthDate,
                                                                    model.image,
                                                                    model.status,
                                                                    model.interests,
                                                                    model.genre,
                                                                    model.country.id,
                                                                    model.instrument.id,
                                                                    model.typeInst.id,
                                                                    model.token])
        
        instanceUser.dataBase!.close()
    }
    
    func removeAllUsers() {
        instanceUser.dataBase!.open()
        
        let sql = "DELETE FROM mm_user"
    
        do {
            try instanceUser.dataBase!.executeUpdate(sql, values: nil)
        } catch {
            print("Erro")
        }
//        instanceUser.dataBase!.executeQuery(sql, withArgumentsIn: nil)
        
        instanceUser.dataBase!.close()
    }
    
    /// Devolve usuario pelo id
    ///
    /// - Parameter id: id do usuário
    /// - Returns: usuario com o id passado
    func getUserById(_ id: Int) -> ModelUser {
        instanceUser.dataBase!.open()
        
        let sql = "SELECT * FROM mm_user WHERE " + "usu_id=" + "?"
        let result: FMResultSet! = instanceUser.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        let model = ModelUser()
        
        if (result != nil) {
            while result.next() {
                model.id        = result.string(forColumn: "usu_id")!
                model.email     = result.string(forColumn: "usu_email")!
                model.password  = result.string(forColumn: "usu_password")!
                model.firstName      = result.string(forColumn: "usu_firstName")!
                model.lastName  = result.string(forColumn: "usu_lastName")!
                model.birthDate  = result.string(forColumn: "usu_birthDate")!
                model.image  = result.string(forColumn: "usu_image")!
                model.status    = Int(result.int(forColumn: "usu_status"))
                model.interests = result.string(forColumn: "usu_interesses")!
                model.genre    = Int(result.int(forColumn: "usu_genre"))
                model.token     = result.string(forColumn: "usu_token")!
                
                let idCountry = result.string(forColumn: "pais_idCountry")
                
                if let id = idCountry {
                    model.country = DAOCountry.getInstance().getCountry(with: id)
                }
                
                let idInstrument = result.string(forColumn: "inst_idInstrument")
                
                if let id = idInstrument {
                    model.instrument = DAOInstrument.getInstance().getInstrumentById(id)
                }
                
                let idType = result.string(forColumn: "tipoInst_idTipo")
                
                if let id = idType {
                    model.typeInst = DAOTypeInstrument.getInstance().getTypeInstrumentById(id)
                }
                
                instanceUser.dataBase!.close()
            }
        }
        
        return model
    }

    func getUserActive() -> ModelUser? {
        instanceUser.dataBase!.open()
        let sql = "SELECT * FROM mm_user"
        let result: FMResultSet! = instanceUser.dataBase!.executeQuery(sql, withArgumentsIn: [])
        
        let model = ModelUser()
        
        if (result != nil) {
            if result.next() {
                model.id       = result.string(forColumn: "usu_id")!
                model.email    = result.string(forColumn: "usu_email")!
                model.password = result.string(forColumn: "usu_password")!
                model.firstName     = result.string(forColumn: "usu_firstName")!
                model.lastName = result.string(forColumn: "usu_lastName")!
                model.birthDate = result.string(forColumn: "usu_birthDate")!
                model.image = result.string(forColumn: "usu_image")!
                model.status   = Int(result.int(forColumn: "usu_status"))
                model.interests = result.string(forColumn: "usu_interesses")!
                model.genre    = Int(result.int(forColumn: "usu_genre"))
                model.token    = result.string(forColumn: "usu_token")!
                
                let idCountry = result.string(forColumn: "pais_idCountry")
                
                if let id = idCountry {
                    model.country = DAOCountry.getInstance().getCountry(with: id)
                }
                
                let idInstrument = result.string(forColumn: "inst_idInstrument")
                
                if let id = idInstrument {
                    model.instrument = DAOInstrument.getInstance().getInstrumentById(id)
                }
                
                let idType = result.string(forColumn: "tipoInst_idTipo")
                
                if let id = idType {
                    model.typeInst = DAOTypeInstrument.getInstance().getTypeInstrumentById(id)
                }
                
                instanceUser.dataBase!.close()
                result.close()
                return model
            }
        }
        
        return nil
    }

    func changeUser(model: ModelUser) {
        instanceUser.dataBase!.open()
        /// String de atualização de partitura
        let sql = "UPDATE mm_user SET " +
            "usu_firstName        = ?,"       +
            "usu_lastName   = ?,"       +
            "usu_birthDate = ?,"       +
            "usu_image   = ?,"       +
            "usu_genre      = ?,"       +
            "pais_idCountry     = ?,"       +
            "inst_idInstrument = ?,"    +
            "tipoInst_idTipo = ?,"       +
            "usu_interesses  = ? "       +
            
        "WHERE usu_email = ?"
        
        instanceUser.dataBase!.executeUpdate(sql, withArgumentsIn: [model.firstName,
                                                                    model.lastName,
                                                                    model.birthDate,
                                                                    model.image,
                                                                    model.genre,
                                                                    model.country.id,
                                                                    model.instrument.id,
                                                                    model.typeInst.id,
                                                                    model.interests,
                                                                    model.email])
        instanceUser.dataBase!.close()
    }
}
