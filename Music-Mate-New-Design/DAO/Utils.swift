//
//  Utils.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    class func getPackage(_ name: String) -> String {
        
        let bundlePath = Bundle.main.path(forResource: "musicMate", ofType: ".sqlite")
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileManager = FileManager.default
        let fullDestPath = URL(fileURLWithPath: destPath).appendingPathComponent(name)
        if fileManager.fileExists(atPath: fullDestPath.path){
            print("Database file is exist")
           // print(fileManager.fileExists(atPath: bundlePath!))
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPath.path)
            }catch{
                print("\n",error)
            }
        }
        
        print(fullDestPath.path)
        return fullDestPath.path
        
//
//        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        let fileURL = documentsUrl.appendingPathComponent(name)
//
//        return fileURL.path
    }
    
    class func copyFile(_ name: NSString) {
        let package: String = getPackage(name as String)
        let manager = FileManager.default
        if !manager.fileExists(atPath: package) {
            
            let documentsUrl = Bundle.main.resourceURL
            let ofPackage = documentsUrl!.appendingPathComponent(name as String)
            
            do {
                try manager.copyItem(atPath: ofPackage.path, toPath: package)
            } catch _ as NSError {
            }
            
        }
    }
    
}
