//
//  DAOAuthorArtist.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 19/06/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOBarConfiguration
let instanceAuthorArtist = DAOAuthorArtist()

/// Classe de persistência de configuracao de compasso
class DAOAuthorArtist: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOBarConfiguration
    ///
    /// - Returns: instância da classe DAOBarConfiguration
    class func getInstance() -> DAOAuthorArtist {
        
        if instanceAuthorArtist.dataBase == nil {
            instanceAuthorArtist.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instanceAuthorArtist.dataBase!.open()
        return instanceAuthorArtist
    }
    
    /// Adiciona uma nova configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func addAuthorToScore(_ model: ModelAuthor, idScore: Int) {
        instanceAuthorArtist.dataBase!.open()
        
        /// String de inserção de configuracao de compasso
        let sql = "INSERT INTO mm_autorArtista(" +
            "aut_nome, "                             +
            "aut_sobrenome, "                            +
            "aut_tipo, "                              +
            "aut_idUsu, "                          +
            "part_idPartitura)"                            +
        "VALUES (?, ?, ?, ?, ?)"
        
        instanceAuthorArtist.dataBase!.executeUpdate(sql, withArgumentsIn: [model.firstName,
                                                                             model.lastName,
                                                                             1,
                                                                             model.idUser,
                                                                             idScore,])
        
        instanceAuthorArtist.dataBase!.close()
    }
    func addArtistToScore(_ model: ModelArtist, idScore: Int) {
        instanceAuthorArtist.dataBase!.open()
        
        /// String de inserção de configuracao de compasso
        let sql = "INSERT INTO mm_autorArtista(" +
            "aut_nome, "                             +
            "aut_sobrenome, "                            +
            "aut_tipo, "                              +
            "aut_idUsu, "                          +
            "part_idPartitura)"                            +
        "VALUES (?, ?, ?, ?, ?)"
        
        instanceAuthorArtist.dataBase!.executeUpdate(sql, withArgumentsIn: [model.firstName,
                                                                            model.lastName,
                                                                            3,
                                                                            model.idUser,
                                                                            idScore,])
        
        instanceAuthorArtist.dataBase!.close()
    }
    
    /// Devolve configuracao de compasso pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: configuracoes com o id passado
    func getAuthorFromScoreId(_ id: Int) -> [ModelAuthor] {
             instanceAuthorArtist.dataBase!.open()
        
        let sql = "SELECT * FROM mm_autorArtista WHERE " + "part_idPartitura=" + "?" + "AND aut_tipo=1"
        let result: FMResultSet! = instanceAuthorArtist.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelAuthor] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelAuthor()
                
                model.idUser          = result.string(forColumn: "aut_idUsu")!
                model.firstName         = result.string(forColumn: "aut_nome")!
                model.lastName   = result.string(forColumn: "aut_sobrenome")!
                models.append(model)
            }
        }
        
           instanceAuthorArtist.dataBase!.close()
        
        return models
    }
    
    func getArtistFromScoreId(_ id: Int) -> [ModelArtist] {
        instanceAuthorArtist.dataBase!.open()
        
        let sql = "SELECT * FROM mm_autorArtista WHERE " + "part_idPartitura=" + "?" + "AND aut_tipo=3"
        let result: FMResultSet! = instanceAuthorArtist.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelArtist] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelArtist()
                
                model.idUser          = result.string(forColumn: "aut_idUsu")!
                model.firstName         = result.string(forColumn: "aut_nome")!
                model.lastName   = result.string(forColumn: "aut_sobrenome")!
                models.append(model)
            }
        }
        
        instanceAuthorArtist.dataBase!.close()
        
        return models
    }
    
    /// Remove uma configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removeAuthor(idScore: Int) {
        instanceAuthorArtist.dataBase!.open()
        
        let sql = "DELETE FROM mm_autorArtista WHERE " + "part_idPartitura=" + "?"
        
        instanceAuthorArtist.dataBase!.executeUpdate(sql, withArgumentsIn: [ idScore ])
        
        instanceAuthorArtist.dataBase!.close()
    }
    
}
