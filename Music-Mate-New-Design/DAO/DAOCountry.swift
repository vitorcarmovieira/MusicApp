//
//  DAOCountry.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOInstrument
let instanceCountry = DAOCountry()

/// Classe de persistência do instrumento
class DAOCountry: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOInstrument
    ///
    /// - Returns: instância da classe DAOInstrument
    class func getInstance() -> DAOCountry {
        
        if instanceCountry.dataBase == nil {
            instanceCountry.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instanceCountry.dataBase!.open()
        return instanceCountry
    }
    
    /// Adiciona um novo instrumento no banco de dados
    ///
    /// - Parameter model: modelo de instrumento
    func addCountry(_ model: ModelCountry) {
        instanceCountry.dataBase!.open()
        
        /// String de inserção de instrumento
        let sql = "INSERT INTO mm_pais (" +
            "pais_id, "                   +
            "pais_nome) "                 +
        "VALUES (?, ?)"
        
     
        instanceCountry.dataBase!.executeUpdate(sql, withArgumentsIn: [model.id,
                                                                       model.name])
        
        instanceCountry.dataBase!.close()
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getAllCountries() -> [ModelCountry] {
        instanceCountry.dataBase!.open()
        
        let sql = "SELECT * FROM mm_pais"
        let result: FMResultSet! = instanceCountry.dataBase!.executeQuery(sql, withArgumentsIn: [""])
        
        var models: [ModelCountry] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelCountry()
                model.id              = result.string(forColumn: "pais_id")!
                model.name            = result.string(forColumn: "pais_nome")!
                
                
                models.append(model)
            }
        }
        
        instanceCountry.dataBase!.close()
        
        return models
    }
    
    /// Devolve instrumento pelo id
    ///
    /// - Parameter id: id do instrumento
    /// - Returns: instrumento com o id passado
    func getCountry(with id: String) -> ModelCountry {
      //  instanceCountry.dataBase!.open()
        
        let sql = "SELECT * FROM mm_pais WHERE pais_id=?"
        let result: FMResultSet! = instanceCountry.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        let model = ModelCountry()
        
        if (result != nil) {
            while result.next() {
                model.id              = result.string(forColumn: "pais_id")!
                model.name            = result.string(forColumn: "pais_nome")!
            }
        }
     
     //   instanceCountry.dataBase!.close()
        
        return model
    }
    
}
