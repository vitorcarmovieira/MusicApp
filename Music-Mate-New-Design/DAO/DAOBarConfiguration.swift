//
//  DAOBarConfiguration.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAOBarConfiguration
let instanceConfiguration = DAOBarConfiguration()

/// Classe de persistência de configuracao de compasso
class DAOBarConfiguration: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAOBarConfiguration
    ///
    /// - Returns: instância da classe DAOBarConfiguration
    class func getInstance() -> DAOBarConfiguration {
        
        if instanceConfiguration.dataBase == nil {
            instanceConfiguration.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
          instanceConfiguration.dataBase!.open()
        return instanceConfiguration
    }

    /// Adiciona uma nova configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func addConfigurationToScore(_ model: ModelBarConfiguration, idScore: Int) {
        instanceConfiguration.dataBase!.open()
        
        /// String de inserção de configuracao de compasso
        let sql = "INSERT INTO mm_configuracaoDeCompasso (" +
            "config_compasso, "                             +
            "config_numerador, "                            +
            "config_denominador, "                          +
            "config_clave, "                                +
            "config_metronomo, "                            +
            "part_idPartitura, "                            +
            "config_armaduraTipo, "                            +
            "config_armadura) "                             +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
        
        instanceConfiguration.dataBase!.executeUpdate(sql, withArgumentsIn: [model.bar,
                                                                          model.numerator,
                                                                          model.denominator,
                                                                          model.clef,
                                                                          model.metronome,
                                                                          idScore,
                                                                          model.keyType,
                                                                          model.key])
        
        instanceConfiguration.dataBase!.close()
    }

    /// Devolve configuracao de compasso pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: configuracoes com o id passado
    func getConfigurationsFromScoreId(_ id: Int) -> [ModelBarConfiguration] {
   //     instanceConfiguration.dataBase!.open()
        
        let sql = "SELECT * FROM mm_configuracaoDeCompasso WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instanceConfiguration.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelBarConfiguration] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelBarConfiguration()
                
                model.id          = Int(result.int(forColumn: "config_id"))
                model.bar         = Int(result.int(forColumn: "config_compasso"))
                model.numerator   = Int(result.int(forColumn: "config_numerador"))
                model.denominator = Int(result.int(forColumn: "config_denominador"))
                model.clef        = Int(result.int(forColumn: "config_clave"))
                model.metronome   = Int(result.int(forColumn: "config_metronomo"))
                model.key         = Int(result.int(forColumn: "config_armadura"))
                model.idPartitura = Int(result.int(forColumn: "part_idPartitura"))
                model.keyType = Int(result.int(forColumn: "config_armaduraTipo"))
                models.append(model)
            }
        }
       
     //   instanceConfiguration.dataBase!.close()
        
        return models
    }
    
    /// Remove uma configuracao de compasso no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removeConfigurationsFromScore(idScore: Int) {
        instanceConfiguration.dataBase!.open()
        
        let sql = "DELETE FROM mm_configuracaoDeCompasso WHERE " + "part_idPartitura=" + "?"
        
        instanceConfiguration.dataBase!.executeUpdate(sql, withArgumentsIn: [ idScore ])
        
        instanceConfiguration.dataBase!.close()
    }
    
}
