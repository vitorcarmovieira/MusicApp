//
//  DAORepeat.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAORepeat
let instanceRepeat = DAORepeat()

/// Classe de persistência de configuracao de compasso
class DAORepeat: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAORepeat
    ///
    /// - Returns: instância da classe DAORepeat
    class func getInstance() -> DAORepeat {
        
        if instanceRepeat.dataBase == nil {
            instanceRepeat.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instanceRepeat.dataBase!.open()
        return instanceRepeat
    }
    
    /// Adiciona uma nova repeticao no banco de dados
    ///
    /// - Parameter model: modelo de repeticao
    func addRepetitionToScore(_ model: ModelRepeat, idScore: Int) {
        instanceRepeat.dataBase!.open()
        
        /// String de inserção de repeticao
        let sql = "INSERT INTO mm_repeticao (" +
            "repet_compassoInicial, "          +
            "repet_compassoFinal, "            +
            "part_idPartitura, "               +
            "repet_chaveDeVolta) "             +
        "VALUES (?, ?, ?, ?)"
        
        instanceRepeat.dataBase!.executeUpdate(sql, withArgumentsIn: [model.initialBar,
                                                                             model.finalBar,
                                                                             idScore,
                                                                             model.voltaBrackets])
        
        instanceRepeat.dataBase!.close()
    }

    /// Devolve repeticao pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: repeticoes com o id passado
    func getRepetitionsFromScoreId(_ id: Int) -> [ModelRepeat] {
      //  instanceRepeat.dataBase!.open()
        
        let sql = "SELECT * FROM mm_repeticao WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instanceRepeat.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelRepeat] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelRepeat()
                
                model.id            = Int(result.int(forColumn: "repet_id"))
                model.initialBar    = Int(result.int(forColumn: "repet_compassoInicial"))
                model.finalBar      = Int(result.int(forColumn: "repet_compassoFinal"))
                model.voltaBrackets = Int(result.int(forColumn: "repet_chaveDeVolta"))
                model.idScore       = Int(result.int(forColumn: "part_idPartitura"))
                
                models.append(model)
            }
        }
        
     //   instanceRepeat.dataBase!.close()
    
        return models
    }
    
    /// Remove repeticoes de uma partitura no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removeRepeatsFromScore(idScore: Int) {
        instanceRepeat.dataBase!.open()
        
        let sql = "DELETE FROM mm_repeticao WHERE " + "part_idPartitura=" + "?"
        
        instanceRepeat.dataBase!.executeUpdate(sql, withArgumentsIn: [ idScore ])
        
        instanceRepeat.dataBase!.close()
    }
    
}
