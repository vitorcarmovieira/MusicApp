//
//  DAODaCapoDelSegno.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/3/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

/// Instancia estática da classe DAODaCapoDelSegno
let instanceDsdc = DAODaCapoDelSegno()

/// Classe de persistência de configuracao de compasso
class DAODaCapoDelSegno: NSObject {
    
    var dataBase: FMDatabase? = nil
    
    /// Retorna uma instância estática da classe DAODaCapoDelSegno
    ///
    /// - Returns: instância da classe DAODaCapoDelSegno
    class func getInstance() -> DAODaCapoDelSegno {
        
        if instanceDsdc.dataBase == nil {
            instanceDsdc.dataBase = FMDatabase(path: Utils.getPackage("musicMate.sqlite"))
        }
        instanceDsdc.dataBase!.open()
        return instanceDsdc
    }

    /// Adiciona uma nova repeticao no banco de dados
    ///
    /// - Parameter model: modelo de repeticao
    func addDsDcToScore(_ model: ModelDaCapoDelSegno, idScore: Int) {
        instanceDsdc.dataBase!.open()
        
        /// String de inserção de repeticao
        let sql = "INSERT INTO mm_daCapoDelSegno (" +
            "dsdc_compassoInicial, "                +
            "dsdc_compassoFinal, "                  +
            "dsdc_toCoda, "                         +
            "dsdc_coda, "                           +
            "dsdc_fine, "                           +
            "dsdc_dsAlCoda, "                       +
            "dsdc_dcAlCoda, "                       +
            "dsdc_dsAlFine, "                       +
            "part_idPartitura, "                    +
            "dsdc_dcAlFine) "                      +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        instanceDsdc.dataBase!.executeUpdate(sql, withArgumentsIn: [model.initialBar,
                                                                      model.finalBar,
                                                                      model.toCoda,
                                                                      model.coda,
                                                                      model.fine,
                                                                      model.dsAlCoda,
                                                                      model.dcAlCoda,
                                                                      model.dsAlFine,
                                                                      idScore,
                                                                      model.dcAlFine])
        
        instanceDsdc.dataBase!.close()
    }
    
    /// Devolve repeticao pelo id da partitura
    ///
    /// - Parameter id: id da partitura
    /// - Returns: dsdcs com o id passado
    func getDsDcsFromScoreId(_ id: Int) -> [ModelDaCapoDelSegno] {
       // instanceDsdc.dataBase!.open()
        
        let sql = "SELECT * FROM mm_daCapoDelSegno WHERE " + "part_idPartitura=" + "?"
        let result: FMResultSet! = instanceDsdc.dataBase!.executeQuery(sql, withArgumentsIn: [id])
        
        var models: [ModelDaCapoDelSegno] = []
        
        if (result != nil) {
            while result.next() {
                let model = ModelDaCapoDelSegno()
                
                model.id         = Int(result.int(forColumn: "dsdc_id"))
                model.initialBar = Int(result.int(forColumn: "dsdc_compassoInicial"))
                model.finalBar   = Int(result.int(forColumn: "dsdc_compassoFinal"))
                model.coda       = Int(result.int(forColumn: "dsdc_coda"))
                model.toCoda     = Int(result.int(forColumn: "dsdc_toCoda"))
                model.fine       = Int(result.int(forColumn: "dsdc_fine"))
                model.dsAlCoda   = Int(result.int(forColumn: "dsdc_dsAlCoda"))
                model.dcAlCoda   = Int(result.int(forColumn: "dsdc_dcAlCoda"))
                model.dsAlFine   = Int(result.int(forColumn: "dsdc_dsAlFine"))
                model.dcAlFine   = Int(result.int(forColumn: "dsdc_dcAlFine"))
                model.idScore    = Int(result.int(forColumn: "part_idPartitura"))
                
                models.append(model)
            }
        }
       
      //  instanceDsdc.dataBase!.close()
        
        return models
    }
    
    /// Remove repeticoes de uma partitura no banco de dados
    ///
    /// - Parameter model: modelo de configuracao de compasso
    func removeDSDCsFromScore(idScore: Int) {
        instanceDsdc.dataBase!.open()
        
        let sql = "DELETE FROM mm_daCapoDelSegno WHERE " + "part_idPartitura=" + "?"
        
        instanceDsdc.dataBase!.executeUpdate(sql, withArgumentsIn: [ idScore ])
        
        instanceDsdc.dataBase!.close()
    }
    
}
