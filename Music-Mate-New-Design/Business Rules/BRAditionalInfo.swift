//
//  BRAditionalInfo.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/1/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class BRAditionalInfo {
    
    func validateComposer(text: String) -> [Bool:String] {
        
        if text.count < 5 {
            return [false : "This field must contain more than 5 characters"]
        }
        
        return [true : "Sucess"]
    }
    
    func validateCountry(text: String) -> [Bool:String] {
        
        if text.count == 0 {
            return [false : "Choose a Country" ]
        }
        
        return [true : "Sucess"]
    }
    
    func validateTitle(text: String) -> [Bool:String] {
        
        if text.count < 5 {
            return [false : "This field must contain more than 5 characters"]
        }
        
        return [true : "Sucess"]
    }
    
    func validateSubTitle(text: String) -> [Bool:String] {
        
        if text.count < 5 {
            return [false : "This field must contain more than 5 characters"]
        }
        
        return [true : "Sucess"]
    }
    
    func validateGener(text: String) -> [Bool:String] {
        if text.count == 0 {
            return [false : "Choose a Gener" ]
        }
        
        return [true : "Sucess"]
    }
    
    func validateSubGener(text: String) -> [Bool:String] {
        if text.count == 0 {
            return [false : "Choose a Subgener" ]
        }
        
        return [true : "Sucess"]
    }

    func validateInstrument(text: String) -> [Bool:String] {
        if text.count == 0 {
            return [false : "Choose an Instrument" ]
        }
        
        return [true : "Sucess"]
    }
    
    func validateDificult(text: String) -> [Bool:String] {
        if text.count == 0 {
            return [false : "Choose a Level"]
        }
        
        return [true : "Sucess"]
    }
    
    func validateDate(text : String) -> [Bool:String] {
        let vls = text.components(separatedBy: "/")
        
        if vls.count == 3 {
            if Int(vls[1]) == 02 {
                return [validateFebruary(day: vls[0], month: vls[1], year: vls[2]):"Date format is incorrect"]
            }
            
            if validateDayOfDate(day: vls[0]) && validateMonthOfDate(month: vls[1]) && validateYearOfDate(year: vls[2]){
                return [true:"Success"]
            }
        }
        
        return [false:"Date format is incorrect"]
    }

    private func validateDayOfDate( day : String ) -> Bool{
        
        if let n = Int(day){
            if n > 0 && n < 32{
                return true
            }
            
        }
        return false
        
    }
    
    
    private func validateMonthOfDate( month : String ) -> Bool{
        
        if let n = Int(month){
            if n > 0 && n < 13{
                return true
            }
            
        }
        
        return false
        
    }
    
    
    private func validateYearOfDate( year : String ) -> Bool{
        
        if let n = Int(year){
            
            let date = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year], from: date)
            
            if let year = components.year {
                if n <= year {
                    return true
                }
            }
            
        }
        
        return false
        
    }
    
    private func validateFebruary( day : String, month: String, year: String) -> Bool{
        
        if let n = Int(year){
            if n%4 == 0{
                if let n = Int (day){
                    if n < 30{
                        return true
                    }
                }
                
            }else{
                if let n = Int (day){
                    if n < 29{
                        return true
                    }
                }
            }
            
        }
        
        return false
        
    }
}
