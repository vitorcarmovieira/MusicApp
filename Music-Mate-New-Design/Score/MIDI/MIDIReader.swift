//
//  MIDIReader.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AudioToolbox

class MIDIReader: NSObject {
    
    var musicSequence:MusicSequence?
    var b64: String?
    var url: URL?
    
    var suggestedQuantization = 4
    
    init(b64: String) {
        self.b64 = b64
        super.init()
        
        self.loadMIDIString()
    }
    override init(){
        
    }
    init(url: URL) {
        self.url = url
        super.init()

        self.loadMIDIFile()
    }
    
    func loadMIDIFile() {
        
        NewMusicSequence(&musicSequence)
        
        var track:MusicTrack?
        MusicSequenceNewTrack(musicSequence!, &track)
        
        let flags:MusicSequenceLoadFlags = MusicSequenceLoadFlags.smf_ChannelsToTracks
        let typeid = MusicSequenceFileTypeID.midiType
        
        MusicSequenceFileLoad(musicSequence!, url! as CFURL, typeid, flags)
    }
    
    func loadMIDIString() {
        
        if (b64!.isEmpty) {
            return
        }
        
        NewMusicSequence(&musicSequence)
        
        var track:MusicTrack?
        MusicSequenceNewTrack(musicSequence!, &track)
        
        //        let fn:CFString = filename as NSString
        //        let ex:CFString = ext as NSString
        //        let midiFileURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), fn, ex, nil)
        let flags:MusicSequenceLoadFlags = MusicSequenceLoadFlags.smf_ChannelsToTracks
        let typeid = MusicSequenceFileTypeID.midiType
        
//        let base64Encoded = "YW55IGNhcm5hbCBwbGVhc3VyZS4="
//
        //try aqui
        if let decodedData = Data(base64Encoded: (b64)!) {
            //        let decodedString = String(data: decodedData, encoding: .utf8)!
            //
            //        // Create a FileHandle instance
            //
            //        let str = "Super long string here"
            let filename = getDocumentsDirectory().appendingPathComponent("temp.midi")
            
            do {
                try decodedData.write(to: filename)
                MusicSequenceFileLoad(musicSequence!, filename as CFURL, typeid, flags)
            } catch {
                //            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
                
            }
        }
//          MusicSequenceFileLoad(musicSequence!, filename as CFURL, typeid, flags)
    }
    //
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getNumberOfTracks() -> Int {
        var trackCount:UInt32 = 0
        MusicSequenceGetTrackCount(musicSequence!, &trackCount)

        return Int(trackCount)
    }
    
    func getTrack(trackIndex:Int) -> MusicTrack {
        var track:MusicTrack? = nil
        MusicSequenceGetIndTrack(musicSequence!, UInt32(trackIndex), &track)

        return track!
    }

    func generateScore(modelScore: inout ModelScore, monophonic : Bool) {
        var track :MusicTrack?  = nil
        
        modelScore.metronome = getMetronomeFromSequence(musicSequence!)
        
        for i in 0..<getNumberOfTracks() {
            MusicSequenceGetIndTrack(musicSequence!, UInt32(i), &track)
            var iterator : MusicEventIterator?  = nil
            NewMusicEventIterator(track!, &iterator)
            var timestamp: MusicTimeStamp = 0
            var eventType: MusicEventType = 0
            var eventData: UnsafeRawPointer? = nil
            var eventDataSize: UInt32 = 0
            var hasNext: DarwinBoolean = true
            MusicEventIteratorHasCurrentEvent(iterator!, &hasNext)
            while hasNext.boolValue {
                MusicEventIteratorGetEventInfo(iterator!, &timestamp, &eventType, &eventData, &eventDataSize)


                
                if eventType == kMusicEventType_MIDINoteMessage {
                    let noteMessage = eventData?.load(as: MIDINoteMessage.self)

                    let note = ModelNote()
                    
                    note.duration          = Int(((noteMessage?.duration)!*960))
                    note.durationQuantized = quantizeNoteDuration(ticks: note.duration)
                    note.initialTime       = Int(timestamp*960)
                    note.tIniQuantized     = quantizeNoteInitialTime(ticks: note.initialTime)
                    note.tone              = Int((noteMessage?.note)!)
                    
                    if(monophonic){
                        note.channel = 1
                    }else {
                        if Int((noteMessage?.channel)!) + 1 > 4 {
                            note.channel = 1
                        } else {
                            note.channel = Int((noteMessage?.channel)!) + 1
                        }
                    }
                    if note.durationQuantized > 0 {
                        modelScore.notes.append(note)
                    }
                }
                if eventType == kMusicEventType_Meta{
                
                    let data = UnsafePointer<MIDIMetaEvent>(eventData?.assumingMemoryBound(to: MIDIMetaEvent.self))
                    if (data?.pointee.metaEventType == 89){
                        modelScore.keySignature = (data?.pointee.data.hashValue)!
                    //    print("meta evento de key signature aqui: ", modelScore.keySignature)
                    }
                    //print("meta evento: \(data?.pointee.metaEventType) data: \(data?.pointee.data)")
                }
                
                
                MusicEventIteratorNextEvent(iterator!)
                MusicEventIteratorHasCurrentEvent(iterator!, &hasNext)
            }
        }
        
        verifyNotes(notes: &modelScore.notes)
        
        calculateQuantization(notes: modelScore.notes)
    }
    
    func verifyNotes(notes: inout [ModelNote]) {
        notes.sort(by: {$1.tIniQuantized > $0.tIniQuantized})
        
        var i = 0
        var j = 0
        
        
        while i < notes.count - 1 {
            j = i + 1
            while j < notes.count {
                var removed = false
                
                if notes[i].tIniQuantized == notes[j].tIniQuantized {
                    if notes[i].tone == notes[j].tone {
                        notes.remove(at: j)
                        removed = true
                    }
                } else {
                    break
                }
                if !removed {
                    j += 1
                }
            }
            i += 1
        }
        
//        while i < notes.count - 1 {
//            if notes[i].tIniQuantized == notes[i + 1].tIniQuantized && notes[i].tone == notes[i + 1].tone {
//                notes.remove(at: i)
//            } else {
//                i += 1
//            }
//        }

    }
    
    func calculateQuantization(notes: [ModelNote]) {
        var lowerNote = 0
        for note in notes {
            lowerNote = min(lowerNote, note.duration)
        }
        
        if lowerNote < (240 / 2) {
            suggestedQuantization = 16
            return
        } else if lowerNote < (480 / 2) {
            suggestedQuantization = 8
            return
        } else if lowerNote < (960 / 2) {
            suggestedQuantization = 4
            return
        }
    }
    
   func getMetronomeFromSequence(_ sequence: MusicSequence)-> Int {
        
        var tempoTrack : MusicTrack? = nil
        MusicSequenceGetTempoTrack(sequence, &tempoTrack)
        var iterator : MusicEventIterator? = nil
        NewMusicEventIterator(tempoTrack!, &iterator)
        var hasNext : DarwinBoolean = true
        var timestamp : MusicTimeStamp  = 0
        var eventType : MusicEventType  = 0
        var eventData: UnsafeRawPointer? = nil
        var eventDataSize: UInt32 = 0
        var bpm : Float64!
        MusicEventIteratorHasCurrentEvent(iterator!, &hasNext)
        while (hasNext).boolValue {
            MusicEventIteratorGetEventInfo(iterator!,
                                           &timestamp,
                                           &eventType,
                                           &eventData,
                                           &eventDataSize)
            
            if eventType == kMusicEventType_ExtendedTempo {
                let noteMessage = eventData?.load(as: ExtendedTempoEvent.self)
                bpm = noteMessage!.bpm
                
            }
            if eventType == kMusicEventType_Meta{
                let data = UnsafePointer<MIDIMetaEvent>(eventData?.assumingMemoryBound(to: MIDIMetaEvent.self))
                if(data?.pointee.metaEventType == 88){
                   var metaEvento = MIDIMetaEvent()
                    metaEvento = (data?.pointee)!
                   //var tempo = [UInt8]()
                    withUnsafeMutablePointer(to: &metaEvento.data, { data in
                        for i in 0 ..< metaEvento.dataLength.hashValue {
                            print("valor do tempo: ", data[i]) }
                    })
                   // let data = UnsafePointer<MIDIMetaEvent>(eventData?.assumingMemoryBound(to: MIDIMetaEvent.self))
                  // print("time sigature aqui: ", data?.pointee.data)
                }
            }
            MusicEventIteratorNextEvent(iterator!)
            MusicEventIteratorHasCurrentEvent(iterator!, &hasNext)
        }
        if (bpm != nil){
            return Int(bpm)
        }
        else {
            return 0
        }
    }
    
     func quantizeNoteDuration(ticks: Int) -> Int {
        let tick1 = ticks
//        if ticks < 30 {
//            return 5760
//        }
        let pairIndex = round(log(Double(tick1)/60)/log(2))
        let oddIndex = round(log(Double(tick1)/90)/log(2))
        let tickPair = Int(60.0 * powf(2, Float(pairIndex)))
        let tickOdd = Int(90.0 * powf(2, Float(oddIndex)))
        if abs(tick1 - tickPair) <= abs(tick1 - tickOdd) {
            return tickPair
        }
        return tickOdd
    }
    
    func quantizeNoteInitialTime(ticks: Int) -> Int {
        let multiplier = round(Double(ticks)/Double(240))
        return Int(multiplier) * 240
    }
}
