//
//  MIDIPlayer.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/25/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import AVFoundation

class MIDIPlayer: NSObject {
    
    private var name        : String!
    private var player      : AVMIDIPlayer!
    private var metronome   : Int!
    private var tickInitial : Int!
    var soundOnOff = true 
    override init() {
        super.init()
    }
    
    init(name: String, metronome: Int, tickInitial: Int) {
        super.init()
        
        self.name        = name
        self.metronome   = metronome
        self.tickInitial = tickInitial
        
        createMidiPlayer()
    }
    
    private func createMidiPlayer() {
        
        let midiFileURL = getFileURL(filePath: "/\(self.name!).mid")
        
        let bankURL = Bundle.main.url(forResource: "Grand_Piano", withExtension: "sf2")

        
        do {
            try self.player = AVMIDIPlayer(contentsOf: midiFileURL as URL, soundBankURL: bankURL)
        } catch let error as NSError {
            print("Error \(error.localizedDescription)")
        }
        
        player.prepareToPlay()
    }
    
    func play() {
        if player.isPlaying {
            player.stop()
        }

        self.player.play({
            self.player.currentPosition = Double(self.tickInitial) * self.getTickInSeconds()
        })
        print(Double(self.tickInitial) * self.getTickInSeconds())
        self.player.currentPosition = Double(self.tickInitial) * self.getTickInSeconds()
    }
    
    private func getTickInSeconds() -> Double {
        return ( ( 60.0 / Double(metronome) ) / 960.0 )
    }
    
    func stop() {
        if player != nil {
            if player.isPlaying {
                player.stop()
            }
        }
    }
    
    private func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
        
    }
    
    private func getFileURL(filePath: String) -> NSURL{
        let path  = getCacheDirectory().appending(filePath)
        let filePath = NSURL(fileURLWithPath: path)
        return filePath
    }
}
