//
//  MIDIGenerator.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/25/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AudioToolbox

class MIDIGenerator: NSObject {
    private var name: String!
    private var metronome: Int!
    private var numerator: Int!
    private var denominator: Int!
    
    var channel12Active = true
    var channel34Active = true
    
    var midiNotes: [MIDINote] = []
    
    override init() {
        super.init()
    }
    
    init(name: String, metronome: Int, numerator: Int, denominator: Int) {
        self.name        = name
        self.metronome   = metronome
        self.numerator   = numerator
        self.denominator = denominator
    }
    
    func addNote(tone: Int, initialTick: Int, duration: Int, channel: Int) {
        if duration != 0 && initialTick >= 0 {
            let note         = MIDINote()
            note.tone        = tone
            note.initialTick = initialTick
            note.duration    = duration
            
            if ( (channel == 1 || channel == 2) && channel12Active ) || ( (channel == 3 || channel == 4) && channel34Active ) {
                midiNotes.append(note)
            }
        }
    }
    
    func generateMIDI() {
        var sequence:MusicSequence?
        NewMusicSequence(&sequence)
        
        addTimeTrack(sequence: &sequence!)
        
        var notesTrack:MusicTrack? = nil
        MusicSequenceNewTrack(sequence!, &notesTrack)
        
        addNotesTrack(sequence: &sequence!, notesTrack: &notesTrack!)
        createMidiFile(sequence: &sequence!, notesTrack: &notesTrack!)
        
    }
    
    private func createMidiFile(sequence: inout MusicSequence, notesTrack: inout MusicTrack) {
        let midiFileURL =  getFileURL(filePath: "/\(self.name!).mid")
        
        MusicSequenceFileCreate(sequence, (midiFileURL as CFURL), MusicSequenceFileTypeID.midiType, MusicSequenceFileFlags.eraseFile, 0)
        MusicSequenceDisposeTrack(sequence, notesTrack)
    }
    
    private func addNotesTrack(sequence: inout MusicSequence, notesTrack: inout MusicTrack) {
        var contador = 0
        var timeMark = 0.0
        
        for i in 0..<self.midiNotes.count {
            contador += 1
            timeMark = Double(self.midiNotes[i].initialTick) / 960
            
            let duration: Float32 = Float32(Double(self.midiNotes[i].duration) / 960)
            
            var mess = MIDINoteMessage(channel: 0,
                                       note: UInt8(self.midiNotes[i].tone),
                                       velocity: 127,
                                       releaseVelocity: 0,
                                       duration: duration)
            MusicTrackNewMIDINoteEvent(notesTrack, MusicTimeStamp(timeMark), &mess)
        }
        
        if self.midiNotes.count == 0 || contador == 0 {
            var mess = MIDINoteMessage(channel: 0,
                                       note: UInt8(64),
                                       velocity: 1,
                                       releaseVelocity: 0,
                                       duration: 0)
            MusicTrackNewMIDINoteEvent(notesTrack, MusicTimeStamp(0), &mess)
        }
    }
    
    private func addTimeTrack(sequence: inout MusicSequence) {
        var metaAssinaturaDeTempo = MIDIMetaEvent()
        metaAssinaturaDeTempo.metaEventType = 0x58
        metaAssinaturaDeTempo.dataLength = 4
        
        let data: [UInt8] = [UInt8(self.denominator), UInt8(self.numerator)]
        withUnsafeMutablePointer(to: &metaAssinaturaDeTempo.data, { ptr in
            for i in 0 ..< data.count { ptr[i] = data[i] }
        })
        
        var timeTrack:MusicTrack? = nil
        MusicSequenceGetTempoTrack(sequence, &timeTrack)
        MusicTrackNewExtendedTempoEvent(timeTrack!, 0.0, Float64(self.metronome))
    }
    
    private func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
        
    }
    
    private func getFileURL(filePath: String) -> NSURL{
        let path  = getCacheDirectory().appending(filePath)
        let filePath = NSURL(fileURLWithPath: path)
        return filePath
    }
}
