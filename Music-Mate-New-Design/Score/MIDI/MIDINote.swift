//
//  MIDINote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/25/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import Foundation

class MIDINote {
    var initialTick = 0
    var duration    = 0
    var tone        = 0
}
