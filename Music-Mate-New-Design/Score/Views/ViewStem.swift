//
//  ViewStem.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewStem: UIView {

    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        addBar(start: false)
    }
    
    func addBar(start: Bool) {
        
        var x: CGFloat = 0.0
        if !start {
            x = self.frame.size.width - 2.0
        }
        
        let width: CGFloat = 1.0

        let viewBar = UIView(frame: CGRect(x: x + (0.2*spacing), y: 0.0, width: width, height:self.frame.size.height))
        viewBar.backgroundColor = UIColor.black
        
        self.addSubview(viewBar)
    }

}
