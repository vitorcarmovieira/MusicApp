//
//  ViewSharp.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewSharp: UIView {

    var space: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        let retangulo = CGRect(x: 0.0, y: (0.1 * self.space), width: self.frame.size.width, height: self.frame.size.height)
        let label = LabelNoBorder(frame: retangulo)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 4.0 * self.space)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        label.text = "D"
        
        self.addSubview(label)
    }

}
