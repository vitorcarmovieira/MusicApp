//
//  ViewEntryBarStripe.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewEntryBarStripe: UIView {

    var space: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        addBar(start: false)
    }
    
    func addBar(start: Bool) {
        
        var x: CGFloat = 0.0
        if !start {
            x = self.frame.size.width - 2.0 - (space / 2.0)
        }
        
        let largura: CGFloat = 2.0
        let altura: CGFloat = self.frame.size.height
        
        let viewBar = UIView(frame: CGRect(x: x, y: 6.0 * self.space, width: largura, height: altura - (12.0 * self.space)))
        viewBar.backgroundColor = UIColor.black
        
        self.addSubview(viewBar)
    }

}
