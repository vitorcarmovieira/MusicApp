//
//  ViewNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/10/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewNote: UIView {

    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    var duration: Int = 960 { didSet { setNeedsDisplay() } }
    var color: UIColor = UIColor.black { didSet { setNeedsDisplay() } }
    var realInitNote     = 0
    var itQuantized: Int = 0
    var tone             = 0
    var bar              = 0
    var positionTone     = 0
    var hasSharp         = false
    var hasFlat          = false
    var channel          = 1
    
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let rectNote = CGRect(x: 0.1 * self.spacing, y: 0.1 * self.spacing, width: frame.size.width, height: frame.size.height)
        let label = UILabel(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textColor = color
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        switch self.duration {
        case 120, 180, 240, 360, 480, 720, 960, 1440:
            label.font = UIFont(name: "Typeface", size: self.spacing * 1.7)
            label.text = "J"
        case 1920, 2880:
            label.text = "H"
        case 3840, 5760:
            label.text = "G"
        default:
            break
        }
        
        switch self.duration {
        case 180, 360, 720, 1440:
            let rectNote = CGRect(x: 1.1 * self.spacing, y: -(0.7 * self.spacing), width: frame.size.width, height: frame.size.height)
            let labelP = UILabel(frame: rectNote)
            labelP.numberOfLines = 0
            labelP.font = UIFont(name: "Typeface", size: 4.0 * self.spacing)
            labelP.textColor = color
            labelP.textAlignment = NSTextAlignment.center
            labelP.backgroundColor = UIColor.clear
            labelP.text = "."
            
            self.addSubview(labelP)
        case 2880:
            let rectNote = CGRect(x: 1.1 * self.spacing, y: 0.7 * self.spacing, width: frame.size.width, height: frame.size.height)
            let labelP = UILabel(frame: rectNote)
            labelP.numberOfLines = 0
            labelP.font = UIFont(name: "Typeface", size: 4.0 * self.spacing)
            labelP.textColor = color
            labelP.textAlignment = NSTextAlignment.center
            labelP.backgroundColor = UIColor.clear
            labelP.text = "."
            
            self.addSubview(labelP)
        default:
            break
        }
        
        if hasFlat {
            var y = -(0.9 * self.spacing)
            
            if duration >= 1920 {
                y = (0.3 * self.spacing)
            }
            let retangulo = CGRect(x: -(1.1*self.spacing), y:  y, width: self.frame.size.width, height: self.frame.size.height)
            let label = LabelNoBorder(frame: retangulo)
            label.numberOfLines = 0
            label.font = UIFont(name: "Typeface", size: 4.7 * self.spacing)
            label.textColor = UIColor.black
            label.textAlignment = NSTextAlignment.center
            label.backgroundColor = UIColor.clear
            label.text = "E"
            
            self.addSubview(label)
        }
        
        if hasSharp {
            var y = -(0.9 * self.spacing)
            
            if duration >= 1920 {
                y = (0.3 * self.spacing)
            }
            let retangulo = CGRect(x: -(1.1*self.spacing), y: y, width: self.frame.size.width, height: self.frame.size.height)
            let label = LabelNoBorder(frame: retangulo)
            label.numberOfLines = 0
            label.font = UIFont(name: "Typeface", size: 4.0 * self.spacing)
            label.textColor = UIColor.black
            label.textAlignment = NSTextAlignment.center
            label.backgroundColor = UIColor.clear
            label.text = "D"
            
            self.addSubview(label)
        }
        
        self.addSubview(label)
    }

    
}
