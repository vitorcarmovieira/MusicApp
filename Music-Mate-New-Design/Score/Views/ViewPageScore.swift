//
//  ViewPageScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewPageScore: UIView {
    
    var labels: [UILabel?] = []
    
//    override func draw(_ rect: CGRect) {
//        for i in 0..<400 {
//            if labels.count > i {
//                if labels[i] != nil {
//                    labels[i]?.frame = CGRect(x: 100, y: 100, width: 300, height: 300)
//                }
//            } else {
//                let lbl = UILabel(frame: CGRect(x: 100, y: 100, width: 300, height: 300))
//                lbl.text = "Teste"
//                addSubview(lbl)
//                labels.append(lbl)
//            }
//        }
//    }
    
    deinit {
        for (i, _) in labels.enumerated() {
            labels[i] = nil
        }
//        print("Entrou deinit ViewPageScore")
    }
}
