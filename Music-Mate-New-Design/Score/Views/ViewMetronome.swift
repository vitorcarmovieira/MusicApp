//
//  ViewMetronome.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewMetronome: UIView {

    var value: Int = 60 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        let label = LabelNoBorder(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height))
        label.text = "= \(value)"
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: bounds.size.height * 0.45)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        
        
        self.addSubview(label)
    }

}
