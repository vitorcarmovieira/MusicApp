//
//  ViewLigature.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewLigature: UIView {

    var space: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    var direction = LigatureDirection.toUp { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        switch direction {
        case .toDown:
            drawLigatureToUp(rect: self.frame)
        case .toUp:
            drawLigatureToUp(rect: self.frame)
        }
    }

//    func drawView() {
//        
//    }
    
    func drawLigatureToUp(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(UIColor.clear.cgColor)
        context!.fill(rect)
        
        let semiCirclePath = UIBezierPath()
        
        semiCirclePath.move(to: CGPoint(x: 0, y: 0))
        
        semiCirclePath.addCurve(to: CGPoint(x: rect.width, y: 0),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height))
        
        semiCirclePath.addCurve(to: CGPoint(x: 0, y: 0),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height*0.8),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height*0.8))
        
        semiCirclePath.lineCapStyle = .round;
        
        semiCirclePath.lineJoinStyle = .round;
        
        semiCirclePath.lineWidth = 1
        
        semiCirclePath.close()
        UIColor.black.setFill()
        
        semiCirclePath.fill()
    }
    
    func desenhaLigaduraParaCimaDireita(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(UIColor.clear.cgColor)
        context!.fill(rect)
        
        let semiCirclePath = UIBezierPath()
        
        semiCirclePath.move(to: CGPoint(x: 0, y: 0))
        semiCirclePath.addCurve(to: CGPoint(x: rect.width, y: rect.height*0.75),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height*0.75),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height*0.75))
        
        semiCirclePath.addLine(to: CGPoint(x: rect.width, y: rect.height))
        
        semiCirclePath.addCurve(to: CGPoint(x: 0, y: 0),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height))
        
        semiCirclePath.lineCapStyle = .round;
        
        semiCirclePath.lineJoinStyle = .round;
        
        semiCirclePath.lineWidth = 1
        
        semiCirclePath.close()
        UIColor.black.setFill()
        
        semiCirclePath.fill()
    }
    
    func desenhaLigaduraParaCimaEsquerda(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(UIColor.clear.cgColor)
        context!.fill(rect)
        
        let semiCirclePath = UIBezierPath()
        
        semiCirclePath.move(to: CGPoint(x: rect.width, y: 0))
        semiCirclePath.addCurve(to: CGPoint(x: 0, y: rect.height*0.75),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height*0.75),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height*0.75))
        
        semiCirclePath.addLine(to: CGPoint(x: 0, y: rect.height))
        
        semiCirclePath.addCurve(to: CGPoint(x: rect.width, y: 0),
                                controlPoint1: CGPoint(x: rect.width/2, y: rect.height),
                                controlPoint2: CGPoint(x: rect.width/2, y: rect.height))
        
        semiCirclePath.lineCapStyle = .round;
        
        semiCirclePath.lineJoinStyle = .round;
        
        semiCirclePath.lineWidth = 1
        
        semiCirclePath.close()
        UIColor.black.setFill()
        
        semiCirclePath.fill()
    }
}

enum LigatureDirection {
    case toUp
    case toDown
}
