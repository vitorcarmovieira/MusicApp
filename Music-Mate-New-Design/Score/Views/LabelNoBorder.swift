//
//  LabelNoBorder.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/10/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class LabelNoBorder: UILabel {

    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)))
    }

}
