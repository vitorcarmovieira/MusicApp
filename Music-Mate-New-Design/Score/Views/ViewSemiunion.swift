//
//  ViewSemiunion.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/13/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ViewSemiunion: UIView {

//    var type: FormatSemiunion = FormatSemiunion.increasing { didSet { setNeedsDisplay() } }
    
    var pIni = CGPoint()
    var pEnd = CGPoint()
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        drawLine(start: pIni, end: pEnd, thickness: 3.0)
        
//        var pIni = CGPoint()
//        var pEnd = CGPoint()
//
//        if type == .increasing {
//            pIni = CGPoint(x: 0.0, y: rect.height - 1.6)
//            pEnd = CGPoint(x: rect.width, y: 1.4)
//        }
//        if type == .decreasing {
//            pIni = CGPoint(x: 0.0, y: 1.4)
//            pEnd = CGPoint(x: rect.width, y: rect.height - 1.6)
//        }
//        if type == .constant {
//            pIni = CGPoint(x: 0.0, y: 1.4)
//            pEnd = CGPoint(x: rect.width, y: 1.4)
//        }
//
//        drawLine(start: pIni, end: pEnd, thickness: 4.0)
    }
    
    func drawLine(start: CGPoint, end: CGPoint, thickness: CGFloat) {
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        path.lineWidth = thickness
        path.stroke()
    }
    
}
