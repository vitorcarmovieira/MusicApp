//
//  ViewBarNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewBarNumber: UIView {

    var bar: Int = 1 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        let label = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        label.text = "\(bar)"
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: self.bounds.size.height * 0.5)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        
        
        addSubview(label)
    }

}
