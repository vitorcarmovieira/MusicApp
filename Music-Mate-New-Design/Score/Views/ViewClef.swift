//
//  ViewClef.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewClef: UIView {

    var type: ClefType = ClefType.SOL { didSet { setNeedsDisplay() } }

    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let label = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        
        var size = self.bounds.size.height
        switch type {
        case .SOL:
            label.text = "B"
            size = size * 0.7
        case .FA:
            label.text = "A"
        default:
            break
        }
        
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: size)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
    }
    
    func drawView() {
        
    }
}
