//
//  ViewBaseNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewBaseNote: UIView {

    var typeNote: TypeBaseNote = TypeBaseNote.quarter { didSet { setNeedsDisplay() } }
    
    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var edit = false
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        var label = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        
        switch typeNote {
        case .quarver, .quarter:
            label = LabelNoBorder(frame: CGRect(x: frame.height * 0.05, y: frame.height * 0.35, width: frame.width * 0.8, height: frame.height * 0.8))
            label.text = "J"
        case .half:
            label.text = "H"
        }
        
        label.numberOfLines = 0
      //  if !edit {
            label.font = UIFont(name: "Typeface", size: self.bounds.size.height * 0.4)
        //} else {
          //  label.font = UIFont(name: "Typeface", size: self.bounds.size.height * 0.3)
        //}
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        switch typeNote {
        case .quarver, .quarter, .half:
            addBar(start: false)
        }
        
        self.addSubview(label)
    }
    
    func addBar(start: Bool) {
        
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        if !start {
            x += (spacing * 1.94)
          //  if edit {
            //    x = 2.7 * spacing
              //  y += 1.5 * spacing
            //}
        }

        let viewBar = UIView(frame: CGRect(x: x, y:y, width: 1.3, height: 4.0 * spacing))
        viewBar.backgroundColor = UIColor.black
        
        addSubview(viewBar)
    }

}
