//
//  ViewPlace.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/5/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewPlace: UIView {

    var initialTime = 0
    var channel     = 1
    
    override func draw(_ rect: CGRect) {
        let width  = rect.height * 0.2
        let height = width
        let x      = (rect.width / 2.0) - (width / 2.0)
        let y      = (rect.height / 2.0) - (height / 2.0)
        
        let rectImg = CGRect(x: x, y: y, width: width, height: height)
        
        let imgView = UIImageView(frame: rectImg)
        imgView.image = UIImage(named: "edit_place_ball.png")
        //  sds
        if imgView.image != nil {
            addSubview(imgView)
        }
    }

}
