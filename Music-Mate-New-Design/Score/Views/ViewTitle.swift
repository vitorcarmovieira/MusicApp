//
//  ViewTitle.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewTitle: UIView {

    var title: String = "Your Score" { didSet { setNeedsDisplay() } }
    var label = LabelNoBorder()

    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        label.frame = self.frame
        label.text = self.title
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: self.bounds.size.height * 0.8)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
    }
    

}
