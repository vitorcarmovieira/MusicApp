//
//  ViewCursor.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/23/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewCursor: UIView {

    var imgCursor: UIImageView!
    
    var isMinimized = false { didSet { setNeedsDisplay() } }
    
    var espaco: CGFloat {
        return frame.height / 8.0
    }
    
    var arrastador: UILongPressGestureRecognizer!
    var dragStart: CGPoint!
    var visaoPartitura = UIView()
    var compassoCursor: ViewBar = ViewBar()
    
    override func draw(_ rect: CGRect) {
        if imgCursor == nil {
            if isMinimized {
                let image = #imageLiteral(resourceName: "cursor_minimized")
                imgCursor = UIImageView(frame: CGRect(x: -(self.frame.width/1.5), y: -(1.5*self.frame.width), width: self.frame.width, height: self.frame.width))
                imgCursor.image = image
            } else {
                let image = #imageLiteral(resourceName: "cursor")
                imgCursor = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
                imgCursor.image = image
            }
            
            
            imgCursor.alpha = 0.8
            
            self.addSubview(imgCursor)
        }
        
//        arrastador = UILongPressGestureRecognizer(target: self as AnyObject,
//                                                  action: #selector(ViewCursor.manipulaArrastoDeCursor(_:)))
//        arrastador.minimumPressDuration = 0.05
//        addGestureRecognizer(arrastador)
    }
    
    func updateLayout() {
        if imgCursor != nil {
            if isMinimized {
                let image = #imageLiteral(resourceName: "cursor_minimized")
                imgCursor.frame = CGRect(x: -(self.frame.width/1.5), y: -(1.5*self.frame.width), width: self.frame.width, height: self.frame.width)
                imgCursor.image = image
            } else {
                let image = #imageLiteral(resourceName: "cursor")
                imgCursor.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
                imgCursor.image = image
            }
            if let superView = superview {
                superView.bringSubview(toFront: imgCursor)
            }
        }
    }
    
    @objc func manipulaArrastoDeCursor(_ manipulador: UILongPressGestureRecognizer) {
        switch manipulador.state {
        case UIGestureRecognizerState.began:
//            if isMinimized {
//                imgCursor.frame.size = CGSize(width: imgCursor.frame.size.width * 2.0, height: imgCursor.frame.size.height * 2.0)
//            } else {
//                imgCursor.frame.size = CGSize(width: imgCursor.frame.size.width, height: imgCursor.frame.size.height * 2.0)
//            }
//
            let view = manipulador.view
            dragStart = manipulador.location(in: view?.superview)
            
        case UIGestureRecognizerState.changed:
            let view = manipulador.view
            
            self.center = manipulador.location(in: view?.superview)
            
        case UIGestureRecognizerState.ended:
//            if isMinimized {
//                imgCursor.frame.size = CGSize(width: imgCursor.frame.size.width / 2.0, height: imgCursor.frame.size.height / 2.0)
//            } else {
//                imgCursor.frame.size = CGSize(width: imgCursor.frame.size.width, height: imgCursor.frame.size.height / 2.0)
//            }
            
            let x = self.frame.origin.x
            let y = self.frame.origin.y - (3 * espaco)
            let comprimento = self.frame.size.width
            let largura = self.frame.size.height
            
            let frameCursor = CGRect(x: x, y: y, width: comprimento, height: largura)
            
            var visoesProximas: [UIView] = []
            
            for sub in visaoPartitura.subviews {
                if sub is ViewPageScore {
                    visoesProximas = sub.subviews.filter {
                        $0 !== self && frameCursor.intersects($0.frame)
                    }
                }
            }
            var cont = 0
            
            for visao in visoesProximas {
                if visao is ViewBar {
                    let x = visao.frame.origin.x + (1 * espaco)
                    var y = visao.frame.origin.y + (4 * espaco)
                    
                    if isMinimized {
                        y -= (2 * espaco)
                    }
                    let comprimento = self.frame.size.width
                    let largura = self.frame.size.height
                    
                    self.frame = CGRect(x: x, y: y, width: comprimento, height: largura)
                    
                    compassoCursor = visao as! ViewBar
                    cont += 1
                }
            }
            
            if cont == 0 {
                compassoCursor = ViewBar()
                compassoCursor.bar = -1
            }
            break
        default:
            break;
        }
    }

}
