//
//  ViewPentagram.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewPentagram: UIView {

    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var bar = 0
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }

        var currentY: CGFloat = 0.5
        
        for _ in 0..<5 {
            let start = CGPoint(x: 0.0, y: currentY)
            let end   = CGPoint(x: rect.size.width, y: currentY)
            
            desenhaLinha(start: start, end: end, thickness: 1.0)
            
            currentY += spacing
        }
    }
    
    
    func desenhaLinha(start: CGPoint, end: CGPoint, thickness: CGFloat) {
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        path.lineWidth = thickness
        path.stroke()
    }
}
