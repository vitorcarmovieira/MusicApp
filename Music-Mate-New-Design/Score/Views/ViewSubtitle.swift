//
//  ViewSubtitle.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 4/5/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ViewSubtitle: UIView {
    
    var subtitle: String = "" { didSet { setNeedsDisplay() } }
    var label = LabelNoBorder()
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        label.frame = self.frame
        label.text = self.subtitle
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: self.bounds.size.height * 0.7)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
    }
    
    
}
