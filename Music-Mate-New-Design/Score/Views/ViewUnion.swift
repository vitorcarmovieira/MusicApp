//
//  ViewUnion.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/12/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewUnion: UIView {

    var type: TypeUnion = TypeUnion.increasing { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
            
        }
        
        var pIni = CGPoint()
        var pEnd = CGPoint()
        
        if type == .increasing {
            pIni = CGPoint(x: 0.0, y: rect.height - 1.6)
            pEnd = CGPoint(x: rect.width, y: 1.4)
        }
        if type == .decreasing {
            pIni = CGPoint(x: 0.0, y: 1.4)
            pEnd = CGPoint(x: rect.width, y: rect.height - 1.6)
        }
        if type == .constant {
            pIni = CGPoint(x: 0.0, y: 1.4)
            pEnd = CGPoint(x: rect.width, y: 1.4)
        }
        
        drawLine(start: pIni, end: pEnd, thickness: 4.0)
    }
    
    func drawLine(start: CGPoint, end: CGPoint, thickness: CGFloat) {
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        path.lineWidth = thickness
        path.stroke()
    }

}
