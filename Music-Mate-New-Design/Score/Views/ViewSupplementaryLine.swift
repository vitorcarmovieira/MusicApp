//
//  ViewSupplementaryLine.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewSupplementaryLine: UIView {

    var space: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var numberLines: Int = 0 { didSet { setNeedsDisplay() } }
    
    var path = UIBezierPath()
    
    override func draw(_ rect: CGRect) {
        path.removeAllPoints()
        
        path = UIBezierPath()
        self.setNeedsDisplay()
        
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        let xIni: CGFloat = 0
        let xFim: CGFloat = self.frame.size.width
        var y: CGFloat = 0.5
        
        for _ in 0..<numberLines {
            let inicio = CGPoint(x: xIni, y: y)
            let fim = CGPoint(x: xFim, y: y)
            
            desenhaLinha(inicio: inicio, fim: fim, espessura: 1.0)
            
            y += space
        }
        
    }
    
    private func desenhaLinha(inicio: CGPoint, fim: CGPoint, espessura: CGFloat) {
        path.move(to: inicio)
        path.addLine(to: fim)
        path.lineWidth = espessura
        path.stroke()
        
    }

}
