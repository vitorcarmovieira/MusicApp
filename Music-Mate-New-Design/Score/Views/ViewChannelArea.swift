//
//  ViewChannelArea.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/21/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewChannelArea: UIView {

    var imagem = UIImageView()
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }
        
        imagem.frame = CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height)

        self.addSubview(imagem)
        self.imagem.image?.draw(in: self.frame)
    }

}
