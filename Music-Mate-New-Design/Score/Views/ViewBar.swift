//
//  ViewBar.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewBar: UIView {

    var startLine  = false  { didSet { setNeedsDisplay() } }
    var position   = CGRect() { didSet { setNeedsDisplay() } }
    
    var start = 0
    var end   = 0
    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var bar = 0
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        if startLine {
            addLine(initial: true)
        }
        addLine(initial: false)
    }

    func addLine(initial: Bool) {
        
        var x: CGFloat = 0.0
        if !initial {
            x = self.frame.size.width - 2.0
        }
        
        let widthBar  : CGFloat = 2.0
        let heightBar : CGFloat = self.frame.size.height
        
        let viewBar = UIView(frame: CGRect(x: x, y: 6.0 * spacing, width: widthBar, height: heightBar - (12.0 * spacing)))
        viewBar.backgroundColor = UIColor.black
        
        self.addSubview(viewBar)
    }
}
