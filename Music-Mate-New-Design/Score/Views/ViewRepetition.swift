//
//  ViewRepetition.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/18/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewRepetition: UIView {

    var type          = TypeRepetition.start  { didSet { setNeedsDisplay() } }
    var position      = CGRect() { didSet { setNeedsDisplay() } }
    var numberOfClefs = 2 { didSet { setNeedsDisplay() } }
    var bar = 0
    
    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }

        if type == TypeRepetition.start {
            addLine(initial: true)
            drawPointsOfRepetition(initial: true)
        } else if type == TypeRepetition.end {
            addLine(initial: false)
            drawPointsOfRepetition(initial: false)
        }
        if type == .bracket1 {
            drawBracket1()
        }
        if type == .bracket2 {
            drawBracket2()
        }
        if type == .segno {
            drawSegno()
        }

        if type == .coda {
            drawCoda()
        }
        if type == .toCoda {
            drawToCoda()
        }
        if type == .dsAlFine {
            drawDsAlFine()
        }
        if type == .dsAlCoda {
            drawDsAlCoda()
        }
        if type == .dcAlFine {
            drawDcAlFine()
        }
        if type == .dcAlCoda {
            drawDcAlCoda()
        }
        if type == .fine {
            drawFine()
        }
        
//        let bg = UIView(frame: CGRect(x: -(rect.width / 5.5), y: -(rect.width / 7.5), width: rect.width / 3.0, height: rect.height / 3.0))
//        bg.backgroundColor = UIColor(red: 119/255, green: 151/255, blue: 198/255, alpha: 0.3)
//        bg.layer.cornerRadius = (rect.width / 3.0) / 4.0
//        
//        addSubview(bg)
//        
//        UIView.animate(withDuration: 1.0, animations: {
//            bg.alpha = 0.0
//        }, completion: { finished in
//            bg.removeFromSuperview()
//        })
        
    }
    
    func drawToCoda() {
        let rectNote = CGRect(x: 0.0, y:  (3.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "To Coda"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawFine() {
        let rectNote = CGRect(x: 0.0, y:  (2.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "Fine"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawDsAlFine() {
        let rectNote = CGRect(x: 0.0, y:  (2.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "D. S. Al Fine"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawDsAlCoda() {
        let rectNote = CGRect(x: 0.0, y:  (2.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "D. S. Al Coda"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawDcAlCoda() {
        let rectNote = CGRect(x: 0.0, y:  (2.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "D. C. Al Coda"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawDcAlFine() {
        let rectNote = CGRect(x: 0.0, y:  (2.0 * self.spacing), width: frame.width, height: 4.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "TimesNewRomanPS-BoldItalicMT", size: 2.5 * self.spacing)
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        label.text = "D. C. Al Fine"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawCoda() {
        let rectNote = CGRect(x: (2.0 * self.spacing), y:  (2.0 * self.spacing), width: frame.width, height: 10.0 * self.spacing)
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        label.text = "X"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawSegno() {
        let rectNote = CGRect(x: (2.0 * self.spacing), y:  (2.0 * self.spacing), width: frame.width, height: 10.0 * self.spacing)

        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        label.text = "W"
        
        addSubview(label)
        
        sendSubview(toBack: label)
    }
    
    func drawBracket1() {
        let viewH = UIView(frame: CGRect(x: frame.width / 2.0, y: 2.0 * self.spacing, width: 0.2 * self.spacing, height: 4.5 * self.spacing))
        viewH.backgroundColor = UIColor.black
        
        let viewV = UIView(frame: CGRect(x: frame.width / 2.0, y: 2.0 * self.spacing, width: frame.width * 0.95, height: 0.2 * self.spacing))
        viewV.backgroundColor = UIColor.black
        
        let label = LabelNoBorder(frame: CGRect(x: (0.5 * self.spacing) + (frame.width / 2.0), y: 2.5 * self.spacing, width: 2.0 * self.spacing, height: 4.0 * self.spacing))
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        label.text = "1"
        
        addSubview(label)
        addSubview(viewH)
        addSubview(viewV)
    }
    
    func drawBracket2() {
        let viewH = UIView(frame: CGRect(x: frame.width / 2.0, y: 2.0 * self.spacing, width: 0.2 * self.spacing, height: 4.5 * self.spacing))
        viewH.backgroundColor = UIColor.black
        
        let viewV = UIView(frame: CGRect(x: frame.width / 2.0, y: 2.0 * self.spacing, width: frame.width * 0.95, height: 0.2 * self.spacing))
        viewV.backgroundColor = UIColor.black
        
        let label = LabelNoBorder(frame: CGRect(x: (0.5 * self.spacing) + (frame.width / 2.0), y: 2.5 * self.spacing, width: 2.0 * self.spacing, height: 4.0 * self.spacing))
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        label.text = "2"
        
        addSubview(label)
        addSubview(viewH)
        addSubview(viewV)
    }
    
    func drawPointsOfRepetition(initial: Bool) {
        let midle = frame.width / 1.81
        var rectNote = CGRect(x: (0.2 * self.spacing) + midle, y: 9.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        if !initial {
            rectNote = CGRect(x: frame.width - (2.2 * self.spacing) + midle, y: 9.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        }
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 8.0 * self.spacing)
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        label.text = "."
        
        addSubview(label)
        
        sendSubview(toBack: label)
        
        var rectNote2 = CGRect(x: (0.2 * self.spacing) + midle, y: 10.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        if !initial {
            rectNote2 = CGRect(x: frame.width - (2.2 * self.spacing) + midle, y: 10.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        }
        
        let label2 = LabelNoBorder(frame: rectNote2)
        label2.numberOfLines = 0
        label2.font = UIFont(name: "Typeface", size: 8.0 * self.spacing)
        label2.textAlignment = NSTextAlignment.left
        label2.backgroundColor = UIColor.clear
        label2.text = "."
        
        addSubview(label2)
        
        sendSubview(toBack: label2)
        
        if numberOfClefs == 2 {
            drawPointsOfRepetition2(initial: initial)
        }
    
    }
    
    func drawPointsOfRepetition2(initial: Bool) {
        let midle = frame.width / 1.81
        var rectNote = CGRect(x: (0.2 * self.spacing) + midle, y: 20.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        if !initial {
            rectNote = CGRect(x: frame.width - (2.2 * self.spacing) + midle, y: 20.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        }
        
        let label = LabelNoBorder(frame: rectNote)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 8.0 * self.spacing)
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        label.text = "."
        
        addSubview(label)
        
        var rectNote2 = CGRect(x: (0.2 * self.spacing) + midle, y: 21.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        if !initial {
            rectNote2 = CGRect(x: frame.width - (2.2 * self.spacing) + midle, y: 21.04 * self.spacing, width: 2.0 * self.spacing, height: 6.0 * self.spacing)
        }
        
        let label2 = LabelNoBorder(frame: rectNote2)
        label2.numberOfLines = 0
        label2.font = UIFont(name: "Typeface", size: 8.0 * self.spacing)
        label2.textAlignment = NSTextAlignment.left
        label2.backgroundColor = UIColor.clear
        label2.text = "."
        
        addSubview(label2)
    }
    
    func addLine(initial: Bool) {
        var x  : CGFloat = 0.0
        var x2 : CGFloat = 0.7 * self.spacing
        if !initial {
            x  = self.frame.size.width
            x2 = self.frame.size.width - (0.5 * self.spacing)
        }
        
        let widthBar  : CGFloat = 0.6 * self.spacing
        let heightBar : CGFloat = self.frame.size.height
        
        x += (frame.width / 1.9) + widthBar

        let viewBar = UIView(frame: CGRect(x: x, y: 12.0 * spacing, width: widthBar, height: heightBar - (12.0 * spacing)))
        viewBar.backgroundColor = UIColor.black
        
        self.addSubview(viewBar)
        
        let widthBar2  : CGFloat = 0.2 * self.spacing
        let heightBar2 : CGFloat = self.frame.size.height
        
        x2 += (frame.width / 1.83) + widthBar2
        
        let viewBar2 = UIView(frame: CGRect(x: x2, y: 12.0 * spacing, width: widthBar2, height: heightBar2 - (12.0 * spacing)))
        viewBar2.backgroundColor = UIColor.black
        
        self.addSubview(viewBar2)
    }

}
