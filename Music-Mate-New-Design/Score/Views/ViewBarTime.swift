//
//  ViewBarTime.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewBarTime: UIView {

    var numerator: Int = 4 { didSet { setNeedsDisplay() } }
    
    var denominator: Int = 4 { didSet { setNeedsDisplay() } }
    
    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        let x: CGFloat = 0.0
        var y = -(spacing * 2.0)
        let width = self.frame.size.width
        let height = self.frame.size.height * 1.2
        
        var pos = CGRect(x: x, y: y, width: width, height: height)
        
        let label = LabelNoBorder(frame: pos)
        label.text = "\(numerator)"
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: height)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
        
        y = y + (height / 2.5)
        pos = CGRect(x: x, y: y, width: width, height: height)
        
        let labelD = LabelNoBorder(frame: pos)
        labelD.text = "\(denominator)"
        labelD.numberOfLines = 0
        labelD.font = UIFont(name: "Typeface", size: height)
        labelD.textColor = UIColor.black
        labelD.textAlignment = NSTextAlignment.left
        labelD.backgroundColor = UIColor.clear
        
        self.addSubview(labelD)
    }

}
