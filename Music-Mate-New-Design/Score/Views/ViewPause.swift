//
//  ViewPause.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewPause: UIView {

    var spacing: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var duration: Int = 960 { didSet { setNeedsDisplay() } }
    
    var channel: Int = 1 { didSet { setNeedsDisplay() } }
    
    var barSize: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    
    var position: PositionPause = PositionPause.normal { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let x: CGFloat = 0.3 * self.spacing
        
//        if duration == 3840 {
//            x = (barSize / 2.2)
//        }
        
        var y = 0.1 * self.spacing
        
        if position == .up {
            y = 0.1 * self.spacing - 4 * spacing
        }
        
        if position == .down {
            y = 0.1 * self.spacing + 4 * spacing
        }
        
        let rectg = CGRect(x: x, y: y, width: self.frame.size.width * 4, height: self.frame.size.height)
        let label = LabelNoBorder(frame: rectg)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        
        switch self.duration {
        case 120:
            label.text = "T"
        case 240:
            label.text = "S"
        case 360:
            label.text = "S T"
        case 480:
            label.text = "R"
        case 720:
            label.text = "R S"
        case 960:
            label.text = "Q"
        case 1440:
            label.text = "Q R"
        case 1920:
            label.text = "P"
            label.frame = CGRect(x: x, y: -(1.25*self.spacing), width: self.frame.size.width * 4, height: self.frame.size.height)
        case 2880:
            label.text = "P Q"
            label.frame = CGRect(x: x, y: -(1.25*self.spacing), width: self.frame.size.width * 4, height: self.frame.size.height)
        case 3840:
            label.text = "P"
            label.frame = CGRect(x: x, y: -(1.75*self.spacing), width: self.frame.size.width * 4, height: self.frame.size.height)
        case 5760:
            label.text = "P Q"
            label.frame = CGRect(x: x, y: -(0.75*self.spacing), width: self.frame.size.width * 4, height: self.frame.size.height)
        default:
            break
        }
        
        self.addSubview(label)
        
    }

}

enum PositionPause {
    case up
    case down
    case normal
}
