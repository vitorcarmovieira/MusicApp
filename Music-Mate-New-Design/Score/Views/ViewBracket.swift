//
//  ViewBracket.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewBracket: UIView {

    var spacing  : CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    var duration : Int     = 480 { didSet { setNeedsDisplay() } }
    var channel  : Int     =  1
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let rectg = CGRect(x: spacing * 0.1, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        let label = LabelNoBorder(frame: rectg)
        label.numberOfLines = 0
        label.font = UIFont(name: "Typeface", size: 5.0 * self.spacing)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        switch self.duration {
        case 480, 720:
            label.text = "L"
        case 240, 360:
            label.text = "M"
        case 120, 180:
            label.text = "N"
        default:
            break
        }
        
        self.addSubview(label)
        
    }
    

}
