//
//  ViewPageNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class ViewPageNumber: UIView {

    var number: Int = 1 { didSet { setNeedsDisplay() } }
    
    var total: Int = 1 { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        for visao in self.subviews {
            visao.removeFromSuperview()
        }

        let label = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        label.text = "\(self.number)/\(self.total)"
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: self.bounds.size.height)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
    }
    
    

}
