//
//  ViewComposer.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 2/23/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ViewComposer: UIView {
    
    var composer: String = "No Composer" { didSet { setNeedsDisplay() } }
    var label = LabelNoBorder()
    
    override func draw(_ rect: CGRect) {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        label.frame = self.frame
        label.text = self.composer
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: self.bounds.size.height * 0.7)
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.right
        label.backgroundColor = UIColor.clear
        
        self.addSubview(label)
    }
    
}
