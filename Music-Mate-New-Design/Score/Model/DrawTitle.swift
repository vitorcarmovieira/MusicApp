//
//  DrawTitle.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawTitle {
    var title = "Your Score"
    var position = CGRect()
    var adapter: AdapterTitle!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterTitle()
        }
        adapter.title = title
        adapter.adapt(parentView: parentView, frame: position)
    }
    deinit {
        adapter = nil
    }
}
