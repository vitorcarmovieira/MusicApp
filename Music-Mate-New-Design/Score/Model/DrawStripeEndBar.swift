//
//  DrawStripeEndBar.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawStripeEndBar {
    var position = CGRect()
    
    private var adapter: AdapterStripeEndBar!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterStripeEndBar()
        }
        
        adapter.parameters = parameters
        adapter.state = state
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
