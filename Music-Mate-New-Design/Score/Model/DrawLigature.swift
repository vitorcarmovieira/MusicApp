//
//  DrawLigadure.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawLigature {
    var placeFrom : DrawPlace = DrawPlace()
    var placeTo   : DrawPlace = DrawPlace()
    var toneFrom  : DrawTone  = DrawTone()
    var toneTo    : DrawTone  = DrawTone()
    var direction : LigatureDirection = LigatureDirection.toUp
    
    var position = CGRect()
    
    private var adapter: AdapterLigature!
    
    deinit {
        adapter = nil
    }
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterLigature()
        }
        adapter.parameters = parameters
        adapter.state = state
        adapter.direction = direction
        adapter.adapt(parentView: parentView, frame: position)
    }
}
