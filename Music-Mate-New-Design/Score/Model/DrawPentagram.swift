//
//  DrawPentagram.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawPentagram {
    var index = 0
    var clef     : DrawClef      = DrawClef()
    var key      : DrawKey       = DrawKey()
    var timeBar  : DrawBarTime   = DrawBarTime()
    var initialBar = false
    var indexBar = 0
    
    var initialTime = 0
    var endTime     = 0
    var isCompose   = false
    
    var channels : [DrawChannel] = []

//    var hasChannelNotes: [Bool] = []
    
    private var placesPerTime = 0
    
    var position = CGRect()
    private var adapter: AdapterPentagram!
    private var spacing: SpacingType = SpacingType.fixed
    
    deinit {
        channels.removeAll()
        adapter = nil
    }
    
    init(placesPerTime: Int, clef: DrawClef, timeBar: DrawBarTime, key: DrawKey, initialTime: Int, endTime: Int) {
        self.clef    = clef
        self.timeBar.numerator = timeBar.numerator
        self.timeBar.denominator = timeBar.denominator
        self.key = key
        
        self.placesPerTime = placesPerTime
        self.initialTime = initialTime
        self.endTime     = endTime
        
        addChannels()
    }
    
    func addChannels() {
        switch clef.type {
        case .SOL:
            let channel1 = DrawChannel(index: 1, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel1.index = 1
            
            let channel2 = DrawChannel(index: 2, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel2.index = 2
            channels.append(channel1)
            channels.append(channel2)
        case .FA:
            
            let channel3 = DrawChannel(index: 3, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel3.index = 3
            
            let channel4 = DrawChannel(index: 4, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel4.index = 4
            channels.append(channel3)
            channels.append(channel4)
        case .DO:
            break
        case .NONE:
            break
        case .SOL_FA:
            
            let channel3 = DrawChannel(index: 3, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel3.index = 3
            
            let channel4 = DrawChannel(index: 4, numberOfTimes: timeBar.numerator, placesPerTime: placesPerTime, initialTime: initialTime, endTime: endTime)
            channel4.index = 4
            channels.append(channel3)
            channels.append(channel4)
        case .FA_DO:
            break
        case .SOL_DO:
            break
        }
    }
    
    func addPause(channel: Int, start: Int, end: Int) {
        var index = 0
        
        switch channel {
        case 1:
            index = 0
        case 2:
            index = 1
        case 3:
            index = 0
        case 4:
            index = 1
        default:
            break
        }
        
        channels[index].addPause(channel: channel, start: start, end: end)
    }
    
    func addNote(midi: Int, initialTime: Int, endTime: Int, channel: Int, bar: Int, predecessor: Bool, successor: Bool, startNote: Bool, realInitialTime: Int, realEndTime: Int, isCorrect: Bool) {
        var index = 0
        
        switch channel {
        case 1:
            index = 0
        case 2:
            index = 1
        case 3:
            index = 0
        case 4:
            index = 1
        default:
            break
        }
        channels[index].addNote(midi: midi, channel: channel, initialTime: initialTime, endTime: endTime, bar: bar, predecessor: predecessor, successor: successor, startNote: startNote, realInitialTime: realInitialTime, realEndTime: realEndTime, key: key.type.rawValue, isCorrect: isCorrect)
    }
    
    func addAccident(linesAccidents: inout [LineAccident] , key: DrawKey) {
        for channel in channels {
            channel.addAccident(linesAccidents: &linesAccidents, key: key)
        }
    }
    
    func joinNotes() {
        for i in 0..<channels.count {
            channels[i].numerator   = timeBar.numerator
            channels[i].denominator = timeBar.denominator
            
            channels[i].joinNotes()
        }
        
//        for _ in 0..<4 {
//            hasChannelNotes.append(false)
//        }
        
//        for (i, channel) in channels.enumerated() {
//            for time in channel.times {
//                for place in time.places {
//                    for note in place.notes {
//                        if note.usable && !note.joined {
//                            hasChannelNotes[i] = true
//                            break
//                        }
//                    }
//                }
//            }
//        }
        
//        for i in 0..<channels.count {
//            channels[i].hasChannelNotes = self.hasChannelNotes
//        }
    }
    
    func getRelativeSize(spacing: SpacingType, quantization: Int) -> CGFloat {
        var size: CGFloat = 0.0
        
        self.spacing = spacing
        size += getBiggerRelativeSizeChannel(spacing: spacing, quantization: quantization)
        size += clef.getClefRelativeSize()
        
        return size
    }
    
    private func getBiggerRelativeSizeChannel(spacing: SpacingType, quantization: Int) -> CGFloat {
        var size: CGFloat = 0.0
        
        for channel in channels {
            size = max(size, channel.getRelativeSize(spacing: spacing, quantization: quantization))
        }
        
        return size
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        //DrawTone().setTonality(tonality: key)
      //  print("tipo de key: ", key.type)
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        if adapter == nil {
            adapter = AdapterPentagram()
            adapter.parameters = parameters
        }
        
        adapter.bar = indexBar
        adapter.state = state
        
        adapter.adapt(parentView: parentView, frame: position)
        
        var widthClef: CGFloat = 0.0
        var widthTime: CGFloat = 0.0
        let widthKey = key.getKeyRelativeSize() * space
        
        if initialBar {
            let x = position.origin.x
            var y = position.origin.y - space
            let width = clef.getClefRelativeSize() * space
            widthClef = width
            var height = position.size.height + (space * 1.2)
            
            if clef.type == .SOL {
                y = position.origin.y - space * 1.20
                height = height + (space * 2.1)
            }
            let positionClef = CGRect(x: x, y: y, width: width, height: height)
            
            clef.position = positionClef
            clef.draw(parentView: parentView, parameters: parameters, state: state)
            
            if indexBar == 1 {
                let x = position.origin.x + widthClef + widthKey
                let y = position.origin.y
                let width = timeBar.getTimeRelativeSize() * space
                widthTime = width
                let height = position.size.height
                let posTimeBar = CGRect(x: x, y: y, width: width, height: height)
                
                timeBar.position = posTimeBar
                timeBar.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }
        
        if key.type != .C_Am {
            let x = position.origin.x + widthClef
            let y = position.origin.y
            let height = position.size.height
            let posKeyBar = CGRect(x: x, y: y, width: widthKey, height: height)
            
            key.position = posKeyBar
            key.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        
        
        let widthChannel  = position.width - widthClef - widthTime - widthKey
        let heightChannel = position.height
        let rectChannel = CGRect(x: position.origin.x + widthClef + widthTime + widthKey, y: position.origin.y, width: widthChannel, height: heightChannel)
        
        for channel in channels {
            channel.position = rectChannel
            channel.draw(parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func drawUnwrittenNotes(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        for channel in channels {
            channel.isCompose = self.isCompose
            channel.drawUnwrittenNotes(parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for channel in channels {
            channel.paintWrongNotes(currentTime: currentTime, notesPlayed: notesPlayed)
        }
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for channel in channels {
            channel.paintAllNotesWithColor()
        }
    }
    
    func sendNotesToFront(parentView: ViewPageScore) {
        for channel in channels {
            channel.sendNotesToFront(parentView: parentView)
        }
    }
}
