//
//  DrawBar.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawBar {
    var index       : Int                = 1
    var numberBar   : DrawBarNumber      = DrawBarNumber()
    var stripeBar   : DrawBarStripe      = DrawBarStripe()
    var entryBar    : DrawEntryBarStripe = DrawEntryBarStripe()
    var endBar      : DrawStripeEndBar   = DrawStripeEndBar()
    var key         : DrawKey           = DrawKey()
    var repetitions : [DrawRepetition]   = []
    var line = 0
    var lastBar = false
    var isCompose = false
    var isToCompletePage = false

    var page = 0
    var timeBar     : DrawBarTime        = DrawBarTime()
    
    var initialTime = 0
    var endTime     = 0
    
    private var numberOfPentagrams   = 0
    private var numberOfPlacesPerTime = 0
    
    var pentagrams : [DrawPentagram]    = []
    
    var initial  = false
    var position = CGRect()
    private var adapter: AdapterBar!
    
    deinit {
        repetitions.removeAll()
        pentagrams.removeAll()
        adapter = nil
    }
    
    init(numberOfPentagrams: Int, numberOfPlacesPerTime: Int, time: DrawBarTime, key: DrawKey, initialTime: Int, endTime: Int, indexBar: Int) {
        self.numberOfPentagrams    = numberOfPentagrams
        self.numberOfPlacesPerTime = numberOfPlacesPerTime
        
        self.timeBar     = time
        self.initialTime = initialTime
        self.endTime     = endTime
        self.index       = indexBar
        self.key = key
        
        addPentagrams()
    }
    
    private func addPentagrams() {
        for i in 0..<numberOfPentagrams {
            let clef = DrawClef()
            clef.type = ClefType(rawValue: i + 1)!

            let pentagram = DrawPentagram(placesPerTime: numberOfPlacesPerTime, clef: clef, timeBar: timeBar, key: key, initialTime: initialTime, endTime: endTime)
            pentagram.indexBar = index
            
            pentagrams.append(pentagram)
        }
    }
    
    func addNote(midi: Int, initialTime: Int, endTime: Int, channel: Int, predecessor: Bool, successor: Bool, startNote: Bool, realInitialTime: Int, realEndTime: Int, isCorrect: Bool) {
        var indexPent = 0
        
        if channel == 3 || channel == 4 {
            indexPent = 1
        }
        
        if pentagrams.count > indexPent {
            pentagrams[indexPent].addNote(midi: midi, initialTime: initialTime, endTime: endTime, channel: channel, bar: index, predecessor: predecessor, successor: successor, startNote: startNote, realInitialTime: realInitialTime, realEndTime: realEndTime, isCorrect: isCorrect)
        }
    }
    
    func addPause(channel: Int, start: Int, end: Int) {
        var indexPent = 0
        
        if channel == 3 || channel == 4 {
            indexPent = 1
        }
        
        if pentagrams.count > indexPent {
            pentagrams[indexPent].addPause(channel: channel, start: start, end: end)
        }
    }
    
    func addAccident(linesAccidents: inout [LineAccident], key: DrawKey) {
        for pentagram in pentagrams {
            resetLinesAccidents(linesAccidents: &linesAccidents, key: key)
            
            pentagram.addAccident(linesAccidents: &linesAccidents, key: key)
        }
    }
    
    func resetLinesAccidents(linesAccidents: inout [LineAccident], key: DrawKey) {
        linesAccidents.removeAll()
        //C------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .D_Bm || key.type == .A_Fsm || key.type == .E_Csm || key.type == .B_Gsm || key.type == .Fs_Dsm {
                let midis: [MidiCode] = [ .c0, .cS0, .c1, .cS1, .c2, .cS2, .c3, .cS3, .c4, .cS4, .c5, .cS5, .c6, .cS6, .c7, .cS7, .c8, .cS8 ]
                let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
                linesAccidents.append(lineAcc)
        }else if key.type == .F_Dm || key.type == .Bb_Gm || key.type == .Eb_Cm || key.type == .Db_BBm || key.type == .Ab_Fm{
            let midis: [MidiCode] = [ .c0, .c1, .c2, .c3, .c4,  .c5, .c6, .c7,  .c8,]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else {
            let midis: [MidiCode] = [ .c0, .cS0, .c1, .cS1, .c2, .cS2, .c3, .cS3, .c4, .cS4, .c5, .cS5, .c6, .cS6, .c7, .cS7, .c8, .cS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        
        //------------------------------------------------------------------------------------------------------------------------------------------------------
        //D------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .E_Csm || key.type == .B_Gsm || key.type == .Fs_Dsm  {
                let midis: [MidiCode] = [ .d0, .d1, .d2, .d3, .d4, .d5, .d6, .d7, .d8, .eB0,.eB1, .eB2, .eB3, .eB4, .eB5, .eB6, .eB7, .eB8 ]
                let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
                linesAccidents.append(lineAcc)
        }
        else if key.type == .Db_BBm || key.type == .Ab_Fm {
            let midis: [MidiCode] = [ .d0, .d1, .d2, .d3, .d4, .d5, .d6, .d7, .d8, .cS0, .cS1, .cS2, .cS3, .cS4, .cS5, .cS6,.cS7,.cS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: true, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .C_Am || key.type == .D_Bm || key.type == .G_Em || key.type == .A_Fsm {
            let midis: [MidiCode] = [  .d0, .d1, .d2, .d3, .d4, .d5, .d6, .d7, .d8, .eB0,.eB1, .eB2, .eB3, .eB4, .eB5, .eB6, .eB7, .eB8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .F_Dm || key.type == .Bb_Gm || key.type == .Eb_Cm {
            let midis: [MidiCode] = [ .d0, .d1, .d2, .d3, .d4, .d5, .d6, .d7, .d8, .cS0, .cS1, .cS2, .cS3, .cS4, .cS5, .cS6,.cS7,.cS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
       //------------------------------------------------------------------------------------------------------------------------------------------------------
        //E------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .Fs_Dsm  {
                let midis: [MidiCode] = [ .e0, .e1, .e2, .e3, .e4, .e5, .e6, .e7, .e8 ]
                let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
                linesAccidents.append(lineAcc)
        }
        else if key.type == .Db_BBm || key.type == .Ab_Fm || key.type == .Bb_Gm || key.type == .Eb_Cm {
            let midis: [MidiCode] = [.e0, .eB0, .e1, .eB1, .e2, .eB2, .e3, .eB3, .e4, .eB4, .e5, .eB5, .e6, .eB6, .e7, .eB7, .e8, .eB8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: true, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .F_Dm{
            let midis: [MidiCode] = [ .e0, .eB0, .e1, .eB1, .e2, .eB2, .e3, .eB3, .e4, .eB4, .e5, .eB5, .e6, .eB6, .e7, .eB7, .e8, .eB8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .C_Am || key.type == .D_Bm || key.type == .G_Em || key.type == .A_Fsm || key.type == .E_Csm || key.type == .B_Gsm{
            let midis: [MidiCode] = [ .e0, .e1, .e2, .e3, .e4, .e5, .e6, .e7, .e8]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        
       //------------------------------------------------------------------------------------------------------------------------------------------------------
       //F------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .G_Em || key.type == .D_Bm || key.type == .A_Fsm || key.type == .E_Csm || key.type == .B_Gsm || key.type == .Fs_Dsm {
            let midis: [MidiCode] = [ .f0, .fS0, .f1, .fS1, .f2, .fS2, .f3, .fS3, .f4, .fS4, .f5, .fS5, .f6, .fS6, .f7, .fS7, .f8, .fS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        } else if key.type == .C_Am {
            let midis: [MidiCode] = [ .f0, .fS0, .f1, .fS1, .f2, .fS2, .f3, .fS3, .f4, .fS4, .f5, .fS5, .f6, .fS6, .f7, .fS7, .f8, .fS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }  else if key.type == .Db_BBm || key.type == .Ab_Fm || key.type == .Bb_Gm || key.type == .Eb_Cm || key.type == .F_Dm {
            let midis: [MidiCode] = [ .f0, .f1, .f2, .f3, .f4, .f5, .f6, .f7, .f8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        
         //------------------------------------------------------------------------------------------------------------------------------------------------------
        //G------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .A_Fsm || key.type == .E_Csm || key.type == .B_Gsm || key.type == .Fs_Dsm {
                let midis: [MidiCode] = [ .g0, .gS0, .g1, .gS1, .g2, .gS2, .g3, .gS3, .g4, .gS4, .g5, .gS5, .g6, .gS6, .g7, .gS7, .g8, .gS8 ]
                let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
                linesAccidents.append(lineAcc)
        }
        else if key.type == .Db_BBm {
            let midis: [MidiCode] = [ .g0, .fS0, .g1, .fS1, .g2, .fS2, .g3, .fS3, .g4, .fS4, .g5, .fS5, .g6, .fS6, .g7, .fS7, .g8, .fS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: true, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .C_Am || key.type == .D_Bm || key.type == .G_Em {
            let midis: [MidiCode] = [ .g0, .gS0, .g1, .gS1, .g2, .gS2, .g3, .gS3, .g4, .gS4, .g5, .gS5, .g6, .gS6, .g7, .gS7, .g8, .gS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }else if key.type == .Ab_Fm || key.type == .Bb_Gm || key.type == .Eb_Cm || key.type == .F_Dm {
            let midis: [MidiCode] = [ .g0, .fS0, .g1, .fS1, .g2, .fS2, .g3, .fS3, .g4, .fS4, .g5, .fS5, .g6, .fS6, .g7, .fS7, .g8, .fS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        
       //------------------------------------------------------------------------------------------------------------------------------------------------------
       //A------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .B_Gsm || key.type == .Fs_Dsm{
                let midis: [MidiCode] = [ .a0, .a1, .a2, .a3, .a4, .a5, .a6, .a7, .a8, .bB0, .bB1, .bB2, .bB3, .bB4, .bB5, .bB6, .bB7, .bB8]
                let lineAcc = LineAccident(midisOfLine: midis, hasSharp: true, hasFlat: false, hasNatural: false)
                linesAccidents.append(lineAcc)
        }
        else if key.type == .Db_BBm  || key.type == .Eb_Cm  || key.type == .Ab_Fm{
            let midis: [MidiCode] = [ .a0, .gS0, .a1, .gS1, .a2, .gS2, .a3, .gS3, .a4, .gS4, .a5, .gS5, .a6, .gS6, .a7, .gS7, .a8, .gS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: true, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else if key.type == .C_Am || key.type == .D_Bm || key.type == .G_Em || key.type == .A_Fsm || key.type == .E_Csm{
            let midis: [MidiCode] = [  .a0, .a1, .a2, .a3, .a4, .a5, .a6, .a7, .a8, .bB0, .bB1, .bB2, .bB3, .bB4, .bB5, .bB6, .bB7, .bB8]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }else if key.type == .F_Dm  || key.type == .Bb_Gm{
            let midis: [MidiCode] = [ .a0, .gS0, .a1, .gS1, .a2, .gS2, .a3, .gS3, .a4, .gS4, .a5, .gS5, .a6, .gS6, .a7, .gS7, .a8, .gS8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        
        //------------------------------------------------------------------------------------------------------------------------------------------------------
       //B------------------------------------------------------------------------------------------------------------------------------------------------------
        if key.type == .Bb_Gm || key.type == .Eb_Cm || key.type == .Ab_Fm || key.type == .Db_BBm || key.type == .F_Dm {
            let midis: [MidiCode] = [ .b0, .bB0, .b1, .bB1, .b2, .bB2, .b3, .bB3, .b4, .bB4, .b5, .bB5, .b6, .bB6, .b7, .bB7, .b8, .bB8 ]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: true, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
        else {
            let midis: [MidiCode] = [ .b0, .b1, .b2, .b3, .b4, .b5, .b6, .b7, .b8]
            let lineAcc = LineAccident(midisOfLine: midis, hasSharp: false, hasFlat: false, hasNatural: false)
            linesAccidents.append(lineAcc)
        }
         //------------------------------------------------------------------------------------------------------------------------------------------------------
    }
    
    func addRepeat(type: TypeRepetition) {
        let repetition = DrawRepetition()
        repetition.numberOfPentagrams = pentagrams.count
        repetition.type = type
        repetition.bar  = self.index
        repetitions.append(repetition)
    }
    
    func addDSDC(type: TypeRepetition) {
        let repetition = DrawRepetition()
        repetition.numberOfPentagrams = pentagrams.count
        repetition.type = type
        repetition.bar  = self.index
        repetitions.append(repetition)
    }
    
    func joinNotes() {
        for i in 0..<pentagrams.count {
            pentagrams[i].joinNotes()
        }
    }
    
    func getRelativeSize(spacing: SpacingType, quantization: Int) -> CGFloat {
        var size: CGFloat = 0.0
        
        for pentagram in pentagrams {
            size = max(size, pentagram.getRelativeSize(spacing: spacing, quantization: quantization))
        }
        
        size += timeBar.getTimeRelativeSize()
        
        return size
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        if adapter == nil {
            adapter = AdapterBar()
            adapter.parameters = parameters
        }
        
//        if index == 1 {
//            entryBar.position = position
//            entryBar.draw(parentView: parentView, parameters: parameters, state: state)
//        }
        
        if lastBar {
            endBar.position = position
            endBar.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        if (self.initial && self.index > 1) || self.index == 2 {
            numberBar.bar      = self.index - 1
            numberBar.position = position
            numberBar.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        adapter.state = state
        adapter.bar   = index
        adapter.start = self.initialTime
        adapter.end   = self.endTime
        adapter.adapt(parentView: parentView, frame: position, startLine: initial)
        
        var y = position.origin.y + (6.0 * space)
        
        for repetition in repetitions {
            if (index == 1 || initial) && repetition.type == .start {
                var desloc = (pentagrams[0].clef.getClefRelativeSize() * space)
                if index == 1 {
                    desloc += pentagrams[0].timeBar.getTimeRelativeSize() * space
                }
                let x = position.origin.x + desloc - (2 * space) - ((position.width - desloc) / 2.0)
                let y = position.origin.y - (6 * space)
                let width = position.width - desloc
                let height = position.height
                let posRepetition = CGRect(x: x, y: y, width: width, height: height)
                
                repetition.position = posRepetition
                repetition.draw(parentView: parentView, parameters: parameters, state: state)
            } else {
                
                let width = position.width
                let height = position.height
                
                var x = position.origin.x - (width / 2.0) - (1.5 * space)
                
                if repetition.type == .dsAlCoda || repetition.type == .dsAlFine || repetition.type == .dcAlCoda || repetition.type == .dcAlFine || repetition.type == .fine || repetition.type == .toCoda {
                    x = position.origin.x
                }
                let y = position.origin.y - (6.0 * space)
                
                if repetition.type != .start && repetition.type != .end {
                    repetition.position = CGRect(x: x, y: y, width: width, height: height / 3.0)
                } else {
                    repetition.position = CGRect(x: x, y: y, width: width, height: height)
                }
                
                repetition.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }

        for pentagram in pentagrams {
            let x = position.origin.x
            let width = position.size.width
            let height = 4.0 * space + 2.0
            let rect = CGRect(x: x, y: y, width: width, height: height)
            pentagram.position = rect
            pentagram.draw(parentView: parentView, parameters: parameters, state: state)
            
            y += 11.0 * space
        }
    }
    
    func drawUnwrittenNotes(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        for pentagram in pentagrams {
            pentagram.isCompose = self.isCompose
            pentagram.drawUnwrittenNotes(parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for pentagram in pentagrams {
            pentagram.paintWrongNotes(currentTime: currentTime, notesPlayed: notesPlayed)
        }
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for pentagram in pentagrams {
            pentagram.paintAllNotesWithColor()
        }
    }
    
    func sendNotesToFront(parentView: ViewPageScore) {
        for pentagram in pentagrams {
            pentagram.sendNotesToFront(parentView: parentView)
        }
    }
}
