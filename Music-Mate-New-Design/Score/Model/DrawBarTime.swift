//
//  DrawTimeBar.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawBarTime {
    var numerator   = 4
    var denominator = 4
    
    var position = CGRect()
    
    private var adapter: AdapterBarTime!
    
    func getTimeRelativeSize() -> CGFloat {
        return 4.0
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterBarTime()
        }
        adapter.numerator = numerator
        adapter.denominator = denominator
        adapter.parameters = parameters
        adapter.state = state
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
