//
//  DrawChannelLine.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawChannelLine {
    private var points: [CGPoint] = []
    private var direction: LineDirection {

        return .right
    }
    
    func addPoint(point: CGPoint) -> Bool {
        if points.count < 2 {
            points.append(point)
            return true
        } else {
            let last = points.count - 1
            if direction == .right {
                if points[last].x < point.x {
                    points.append(point)
                    return true
                }
            } else {
                if points[last].x > point.x {
                    points.append(point)
                    return true
                }
            }
        }
        
        return false
    }
    
    func removePointsOfLine() {
        self.points.removeAll()
    }
    
    func unionOfPoints() -> [[CGPoint]] {
        var linePoints: [[CGPoint]] = [[]]
        linePoints.removeAll()
        if points.count >= 2 {
            for i in 0..<points.count - 1 {
                linePoints.append([points[i], points[i + 1]])
            }
        }
        
        return linePoints
    }
}

enum LineDirection {
    case left
    case right
}
