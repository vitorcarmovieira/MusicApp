//
//  DrawChannelArea.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawChannelArea {
    var type = TypeCanalization.oneTree
    var line = 1
    var placesPositions : [CGFloat] = []
    var places          : [DrawPlace] = []
    
    var position = CGRect()
    
    var adapter: AdapterChannelArea!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterChannelArea()
        }

        adapter.parameters = parameters
        adapter.places = places
        adapter.placesPosition = placesPositions
        adapter.type = type
        adapter.adapt(parentView: parentView, frame: position)
    }
}

enum TypeCanalization {
    case oneTree
    case oneTwo
    case treeFour
    case hAcima
    case hMeio
    case hAbaixo
}
