//
//  DrawNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import AVFoundation
import UIKit

class DrawNote {
    var tone        = DrawTone()
    var channel     = 1
    var duration    = NoteDuration.quarter
    var bar         = 0
    var itQuantized = 0
    var realInitialTime = 0
    var realEndTime     = 0
    
    var predecessor  : Bool = false
    var successor    : Bool = false
    var completeTime : Bool = false
    var usable       : Bool = true
    var played       : Bool = false
    var startNote    : Bool = false
    var startTime    : Bool = false
    var joined       : Bool = false
    var drawed       : Bool = false
    var ligatureActive = false
    var isCompose    : Bool = false
    var isCorrect    : Bool = true
    
    var position = CGRect()
    var adapter: AdapterNote!
    
    init() {
        
    }
    
    init(midi: MidiCode, tickStart: Int, tickEnd: Int, predecessor: Bool, sucessor: Bool) {
        
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        self.drawed = true

        if adapter == nil {
            adapter = AdapterNote()
        }
        adapter.parameters   = parameters
        adapter.duration     = duration.rawValue
        adapter.state        = state
        adapter.bar          = bar
        adapter.tone         = tone.midi.rawValue
        adapter.itQuantized  = itQuantized
        adapter.posTone      = tone.getPositionTone()
        adapter.realInitNote = realInitialTime
        adapter.channel      = channel
        
        if isCorrect {
            adapter.color = UIColor.black
        } else {
            adapter.color = UIColor.red
        }
        
        if isCompose {
            adapter.hasSharp    = tone.hasSharp
            adapter.hasFlat     = tone.hasFlat
        }
        
        adapter.adapt(parentView: parentView, frame: position)
    }

    deinit {
        adapter = nil
    }
}

enum NoteDuration: Int {
    case dsemiquave        = 120
    case dsemiquavePontued = 180
    case semiquaver        = 240
    case semiquaverPontued = 360
    case quaver            = 480
    case quaverPontued     = 720
    case quarter           = 960
    case quarterPontued    = 1440
    case half              = 1920
    case halfPontued       = 2880
    case whole             = 3840
    case wholePontued      = 5760
}
