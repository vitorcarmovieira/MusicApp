//
//  DrawClef.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawClef {
    var type : ClefType = ClefType.NONE
    var position = CGRect()
    
    private var adapter: AdapterClef!
    
    deinit {
        adapter = nil
    }
    
    func getClefRelativeSize() -> CGFloat {
        if type == .SOL || type == .FA {
            return 4.0
        }
        
        return 0.0
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterClef()
        }
        adapter.type = type
        adapter.adapt(parentView: parentView, frame: position)
    }
}

enum ClefType: Int {
    case SOL  = 1
    case FA   = 2
    case DO   = 3
    case SOL_FA = 4
    case FA_DO = 5
    case SOL_DO = 6
    case NONE = 7
}
