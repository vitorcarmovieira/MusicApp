//
//  DrawSharp.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawSharp {
    var tone = DrawTone()
    var position = CGRect()
    var local = 1
    var drawed = false
    var adapter: AdapterSharp!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        drawed = true
        if adapter == nil {
            adapter = AdapterSharp()
        }
        adapter.parameters  = parameters
        adapter.state       = state

        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
