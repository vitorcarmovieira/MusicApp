//
//  DrawTone.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawTone: NSObject, Comparable {

    var clef : ClefType = ClefType.SOL
    var midi : MidiCode = MidiCode.c4
    var tonalidade : KeyType = KeyType.F_Dm
    private var desenhou = false
    
    var hasSharp   : Bool {
        //print("midi", midi)
        switch midi {
        case .cS0, .cS1, .cS2, .cS3, .cS4, .cS5, .cS6, .cS7, .cS8, .eB0, .eB1, .eB2, .eB3, .eB4, .eB5, .eB6, .eB7, .eB8,.fS0, .fS1, .fS2, .fS3, .fS4, .fS5, .fS6, .fS7, .fS8, .gS0, .gS1, .gS2, .gS3, .gS4, .gS5, .gS6, .gS7, .gS8, .bB0, .bB1, .bB2, .bB3, .bB4, .bB5, .bB6, .bB7, .bB8:
                if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                    return true
                } else {
                    return false
            }
        default:
            return false
        }
    }
    var hasFlat    : Bool {
        
        switch midi {
    case .cS0, .cS1, .cS2, .cS3, .cS4, .cS5, .cS6, .cS7, .cS8, .eB0, .eB1, .eB2, .eB3, .eB4, .eB5, .eB6, .eB7, .eB8,.fS0, .fS1, .fS2, .fS3, .fS4, .fS5, .fS6, .fS7, .fS8, .gS0, .gS1, .gS2, .gS3, .gS4, .gS5, .gS6, .gS7, .gS8, .bB0, .bB1, .bB2, .bB3, .bB4, .bB5, .bB6, .bB7, .bB8:
            if(tonalidade  == .F_Dm || tonalidade == .Bb_Gm || tonalidade == .Db_BBm  || tonalidade == .Eb_Cm || tonalidade == .Ab_Fm){
                return true
            } else {
                return false
            }
        default:
            return false
        }
    }

    var position = CGRect()

    var height: CGFloat {
        switch clef {
        case .SOL:
            return getHeightToneInSolClef()
        case .FA:
            return getHeightToneInFaClef()
        case .DO:
            return getHeightToneInSolClef()
        default:
            return 0
        }
    }
//    override init(){
//    }
//    init(type: KeyType) {
//        tonalidade = type
//         }
    
    
//    func setTonality(tonality : DrawKey){
//        tonalidade = tonality
//        print("oioi4se: ", tonalidade)
//        }
//
    
    //Posicao relativa do tom em uma clave
    var clefPosition : CGFloat {
        switch clef {
        case .SOL:
            return (CGFloat(midi.rawValue) - CGFloat(MidiCode.c4.rawValue)) / 2.0
        case .FA:
            return (CGFloat(midi.rawValue) - CGFloat(MidiCode.a2.rawValue)) / 2.0
        case .DO:
            return (CGFloat(midi.rawValue) - CGFloat(MidiCode.c4.rawValue)) / 2.0
        default:
            return 0
        }
    }
    
    static func < (lhs: DrawTone, rhs: DrawTone) -> Bool {
        if lhs.midi.rawValue < rhs.midi.rawValue {
            return true
        } else {
            return false
        }
    }
    
    func getPositionTone() -> Int {
//        print("midi: ",midi)
        
        switch midi {
            
        case .c0:
            return 0
        case .cS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 0
            }
            else{
                return 1
            }
        case .d0:
            return 1
        case .eB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 1
            }
            else{
                return 2
            }
        case .e0:
            return 2
        case .f0:
            return 3
        case .fS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 3
            }
            else{
                return 4
            }
        case .g0:
            return 4
        case .gS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 4
            }
            else{
                return 5
            }
        case .a0:
            return 5
        case .bB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 5
            }
            else{
                return 6
            }
        case .b0:
            return 6
            
        case .c1:
            return 7
        case .cS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 7
            }
            else{
                return 8
            }
        case .d1:
            return 8
        case .eB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 8
            }
            else{
                return 9
            }
        case .e1:
            return 9
        case .f1:
            return 10
        case .fS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 10
            }
            else{
                return 11
            }
        case .g1:
            return 11
        case .gS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 11
            }
            else{
                return 12
            }
        case .a1:
            return 12
        case .bB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 12
            }
            else{
                return 13
            }
        case .b1:
            return 13
            
        case .c2:
            return 14
        case .cS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 14
            }
            else{
                return 15
            }
        case .d2:
            return 15
        case .eB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 15
            }
            else{
                return 16
            }
        case .e2:
            return 16
        case .f2:
            return 17
        case .fS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 17
            }
            else{
                return 18
            }
        case .g2:
            return 18
        case .gS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 18
            }
            else{
                return 19
            }
        case .a2:
            return 19
        case .bB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 19
            }
            else{
                return 20
            }
        case .b2:
            return 20
            
        case .c3:
            return 21
        case .cS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 21
            }
            else{
                return 22
            }
        case .d3:
            return 22
        case .eB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 22
            }
            else{
                return 23
            }
        case .e3:
            return 23
        case .f3:
            return 24
        case .fS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 24
            }
            else{
                return 25
            }
        case .g3:
            return 25
        case .gS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 25
            }
            else{
                return 26
            }
        case .a3:
            return 26
        case .bB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 26
            }
            else{
                return 27
            }
        case .b3:
            return 27
            
        case .c4:
            return 28
        case .cS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 28
            }
            else{
                return 29
            }
        case .d4:
            return 29
        case .eB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 29
            }
            else{
                return 30
            }
        case .e4:
            return 30
        case .f4:
            return 31
        case .fS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 31
            }
            else{
                return 32
            }
        case .g4:
            return 32
        case .gS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 32
            }
            else{
                return 33
            }
        case .a4:
            return 33
        case .bB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 33
            }
            else{
                return 34
            }
        case .b4:
            return 34
            
        case .c5:
            return 35
        case .cS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 35
            }
            else{
                return 36
            }
        case .d5:
            return 36
        case .eB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 36
            }
            else{
                return 37
            }
        case .e5:
            return 37
        case .f5:
            return 38
        case .fS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 38
            }
            else{
                return 39
            }
        case .g5:
            return 39
        case .gS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 39
            }
            else{
                return 40
            }
        case .a5:
            return 40
        case .bB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 40
            }
            else{
                return 41
            }
        case .b5:
            return 41
            
        case .c6:
            return 42
        case .cS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 42
            }
            else{
                return 43
            }
        case .d6:
            return 43
        case .eB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 43
            }
            else{
                return 44
            }
        case .e6:
            return 44
        case .f6:
            return 45
        case .fS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 45
            }
            else{
                return 46
            }
        case .g6:
            return 46
        case .gS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 46
            }
            else{
                return 47
            }
        case .a6:
            return 47
        case .bB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 47
            }
            else{
                return 48
            }
        case .b6:
            return 48
            
        case .c7:
            return 49
        case .cS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 49
            }
            else{
                return 50
            }
        case .d7:
            return 50
        case .eB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 50
            }
            else{
                return 51
            }
        case .e7:
            return 51
        case .f7:
            return 52
        case .fS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 52
            }
            else{
                return 53
            }
        case .g7:
            return 53
        case .gS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 53
            }
            else{
                return 54
            }
        case .a7:
            return 54
        case .bB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 54
            }
            else{
                return 55
            }
        case .b7:
            return 55
            
        case .c8:
            return 56
        case .cS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 56
            }
            else{
                return 57
            }
        case .d8:
            return 57
        case .eB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 57
            }
            else{
                return 58
            }
        case .e8:
            return 58
        case .f8:
            return 59
        case .fS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 59
            }
            else{
                return 60
            }
        case .g8:
            return 60
        case .gS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 60
            }
            else{
                return 61
            }
        case .a8:
            return 61
        case .bB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 61
            }
            else{
                return 62
            }
        case .b8:
            return 62
            
        default:
            break
        }
        
        return 0
    }
    
    func getHeightToneInSolClef() -> CGFloat {
    
      
        switch midi {
            
        case .c0:
            return -15.5
        case .cS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -15.5
            }
            else{
                return -15.0
            }
        case .d0:
            return -15.0
        case .eB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -15.0
            }
            else{
                 return -14.5
            }
        case .e0:
            return -14.5
        case .f0:
            return -14.0
        case .fS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                 return -14.0
            }
            else{
                return -13.5
            }
           
        case .g0:
            return -13.5
        case .gS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -13.5
            }
            else{
                return -13.0
            }
            
        case .a0:
            return -13.0
        case .bB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -13.0
            }
            else{
                return -12.5
            }
        case .b0:
            return -12.5
            
            
        case .c1:
            return -12.0
        case .cS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -12.0
            }
            else{
                return -11.5
            }
        case .d1:
            return -11.5
        case .eB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -11.5
            }
            else{
                return -11.0
            }
        case .e1:
            return -11.0
        case .f1:
            return -10.5
        case .fS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -10.5
            }
            else{
                return -10.0
            }
        case .g1:
            return -10.0
        case .gS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -10.0
            }
            else{
                return -9.5
            }
        case .a1:
            return -9.5
        case .bB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -9.5
            }
            else{
                return -9.0
            }
        case .b1:
            return -9.0
            
        case .c2:
            return -8.5
        case .cS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -8.5
            }
            else{
                return -8.0
            }
        case .d2:
            return -8.0
        case .eB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -9.0
            }
            else{
                return -7.5
            }
        case .e2:
            return -7.5
        case .f2:
            return -7.0
        case .fS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -7.0
            }
            else{
                return -6.5
            }
        case .g2:
            return -6.5
        case .gS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -6.5
            }
            else{
                return -6.0
            }
        case .a2:
            return -6.0
        case .bB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -6.0
            }
            else{
                return -5.5
            }
        case .b2:
            return -5.5
            
        case .c3:
            return -5.0
        case .cS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -5.0
            }
            else{
                return -4.5
            }
        case .d3:
            return -4.5
        case .eB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -4.5
            }
            else{
                return -4.5
            }
        case .e3:
            return -4.0
        case .f3:
            return -3.5
        case .fS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -3.5
            }
            else{
                return -3.5
            }
        case .g3:
            return -3.0
        case .gS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -3.0
            }
            else{
                return -2.5
            }
        case .a3:
            return -2.5
        case .bB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -2.5
            }
            else{
                return -2.0
            }
        case .b3:
            return -2.0
            
        case .c4:
            return -1.5
        case .cS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -1.5
            }
            else{
                return -1.0
            }
            
        case .d4:
            return -1.0
        case .eB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -1.0
            }
            else{
                return -0.5
            }
        case .e4:
            return -0.5
        case .f4:
            return 0.0
        case .fS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 0.0
            }
            else{
                return 0.5
            }
        case .g4:
            return 0.5
        case .gS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 0.5
            }
            else{
                return 1.0
            }
        case .a4:
            return 1.0
        case .bB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 1.0
            }
            else{
                return 1.5
            }
        case .b4:
            return 1.5
            
        case .c5:
            return 2.0
        case .cS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 2.0
            }
            else{
                return 2.5
            }
        case .d5:
            return 2.5
        case .eB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 2.5
            }
            else{
                return 3.0
            }
        case .e5:
            return 3.0
        case .f5:
            return 3.5
        case .fS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 3.5
            }
            else{
                return 4.0
            }
        case .g5:
            return 4.0
        case .gS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 4.0
            }
            else{
                return 4.5
            }
        case .a5:
            return 4.5
        case .bB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 4.5
            }
            else{
                return 5.0
            }
        case .b5:
            return 5.0
            
        case .c6:
            return 5.5
        case .cS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 5.5
            }
            else{
                return 6.0
            }
        case .d6:
            return 6.0
        case .eB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 6.0
            }
            else{
                return 6.5
            }
        case .e6:
            return 6.5
        case .f6:
            return 7.0
        case .fS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 7.0
            }
            else{
                return 7.5
            }
        case .g6:
            return 7.5
        case .gS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 7.0
            }
            else{
                return 8.0
            }
        case .a6:
            return 8.0
        case .bB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 8.0
            }
            else{
                return 8.5
            }
        case .b6:
            return 8.5
            
        case .c7:
            return 9.0
        case .cS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 9.0
            }
            else{
                return 9.5
            }
        case .d7:
            return 9.5
        case .eB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 9.5
            }
            else{
                return 10.0
            }
        case .e7:
            return 10.0
        case .f7:
            return 10.5
        case .fS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 10.5
            }
            else{
                return 11.0
            }
        case .g7:
            return 11.0
        case .gS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 11.0
            }
            else{
                return 11.5
            }
        case .a7:
            return 11.5
        case .bB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 11.5
            }
            else{
                return 12.0
            }
        case .b7:
            return 12.0
            
        case .c8:
            return 12.5
        case .cS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 12.5
            }
            else{
                return 13.0
            }
        case .d8:
            return 13.0
        case .eB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 13.0
            }
            else{
                return 13.5
            }
        case .e8:
            return 13.5
        case .f8:
            return 14.0
        case .fS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 14.0
            }
            else{
                return 14.5
            }
        case .g8:
            return 14.5
        case .gS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 14.5
            }
            else{
                return 15.0
            }
        case .a8:
            return 15.0
        case .bB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 15.0
            }
            else{
                return 15.5
            }
        case .b8:
            return 15.5
            
        default:
            break
        }
        
        return 0.0
    }
    
    func getHeightToneInFaClef() -> CGFloat {
    
        switch midi {
            
        case .c0:
            return -9.5
        case .cS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -9.5
            }
            else{
                return -9.0
            }
        case .d0:
            return -9.0
        case .eB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -9.0
            }
            else{
                return -8.5
            }
        case .e0:
            return -8.5
        case .f0:
            return -8.0
        case .fS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -8.0
            }
            else{
                return -7.5
            }
        case .g0:
            return -7.5
        case .gS0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -7.5
            }
            else{
                return -7.0
            }
        case .a0:
            return -7.0
        case .bB0:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -7.0
            }
            else{
                return -6.5
            }
        case .b0:
            return -6.5
            
        case .c1:
            return -6.0
        case .cS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -6.0
            }
            else{
                return -5.5
            }
        case .d1:
            return -5.5
        case .eB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -5.5
            }
            else{
                return -5.0
            }
        case .e1:
            return -5.0
        case .f1:
            return -4.5
        case .fS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -4.5
            }
            else{
                return -4.0
            }
        case .g1:
            return -4.0
        case .gS1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -4.0
            }
            else{
                return -3.5
            }
        case .a1:
            return -3.5
        case .bB1:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -3.5
            }
            else{
                return -3.0
            }
        case .b1:
            return -3.0
            
        case .c2:
            return -2.5
        case .cS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -2.5
            }
            else{
                return -2.0
            }
        case .d2:
            return -2.0
        case .eB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -2.0
            }
            else{
                return -1.5
            }
        case .e2:
            return -1.5
        case .f2:
            return -1.0
        case .fS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -1.0
            }
            else{
                return -0.5
            }
        case .g2:
            return -0.5
        case .gS2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return -0.5
            }
            else{
                return 0.0
            }
        case .a2:
            return 0.0
        case .bB2:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 0.0
            }
            else{
                return 0.5
            }
        case .b2:
            return 0.5
            
        case .c3:
            return 1.0
        case .cS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 1.0
            }
            else{
                return 1.5
            }
        case .d3:
            return 1.5
        case .eB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 1.5
            }
            else{
                return 2.0
            }
        case .e3:
            return 2.0
        case .f3:
            return 2.5
        case .fS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 2.5
            }
            else{
                return 3.0
            }
        case .g3:
            return 3.0
        case .gS3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 3.0
            }
            else{
                return 3.5
            }
        case .a3:
            return 3.5
        case .bB3:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 3.5
            }
            else{
                return 4.0
            }
        case .b3:
            return 4.0
            
        case .c4:
            return 4.5
        case .cS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 4.5
            }
            else{
                return 5.0
            }
        case .d4:
            return 5.0
        case .eB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 5.0
            }
            else{
                return 5.5
            }
        case .e4:
            return 5.5
        case .f4:
            return 6.0
        case .fS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 6.0
            }
            else{
                return 6.5
            }
        case .g4:
            return 6.5
        case .gS4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 6.5
            }
            else{
                return 7.0
            }
        case .a4:
            return 7.0
        case .bB4:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 7.0
            }
            else{
                return 7.5
            }
        case .b4:
            return 7.5
            
        case .c5:
            return 8.0
        case .cS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 8.0
            }
            else{
                return 8.5
            }
        case .d5:
            return 8.5
        case .eB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 8.5
            }
            else{
                return 9.0
            }
        case .e5:
            return 9.0
        case .f5:
            return 9.5
        case .fS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 9.5
            }
            else{
                return 10.0
            }
        case .g5:
            return 10.0
        case .gS5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 10.0
            }
            else{
                return 10.5
            }
        case .a5:
            return 10.5
        case .bB5:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 10.5
            }
            else{
                return 11.0
            }
        case .b5:
            return 11.0
            
        case .c6:
            return 11.5
        case .cS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 11.5
            }
            else{
                return 12.0
            }
        case .d6:
            return 12.0
        case .eB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 12.5
            }
            else{
                return 12.5
            }
        case .e6:
            return 12.5
        case .f6:
            return 13.0
        case .fS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 13.0
            }
            else{
                return 13.5
            }
        case .g6:
            return 13.5
        case .gS6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 13.5
            }
            else{
                return 14.0
            }
        case .a6:
            return 14.0
        case .bB6:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 14.0
            }
            else{
                return 14.5
            }
        case .b6:
            return 14.5
            
        case .c7:
            return 15.0
        case .cS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 15.0
            }
            else{
                return 15.5
            }
        case .d7:
            return 15.5
        case .eB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 15.5
            }
            else{
                return 16.0
            }
        case .e7:
            return 16.0
        case .f7:
            return 16.5
        case .fS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 60.5
            }
            else{
                return 17.0
            }
        case .g7:
            return 17.0
        case .gS7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 17.0
            }
            else{
                return 17.5
            }
        case .a7:
            return 17.5
        case .bB7:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 17.5
            }
            else{
                return 18.0
            }
        case .b7:
            return 18.0
            
        case .c8:
            return 18.5
        case .cS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 18.5
            }
            else{
                return 19.0
            }
        case .d8:
            return 19.0
        case .eB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 19.0
            }
            else{
                return 19.5
            }
        case .e8:
            return 19.5
        case .f8:
            return 20.0
        case .fS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 20.0
            }
            else{
                return 20.5
            }
        case .g8:
            return 20.5
        case .gS8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 20.5
            }
            else{
                return 21.0
            }
        case .a8:
            return 21.0
        case .bB8:
            if(tonalidade == .C_Am || tonalidade == .D_Bm || tonalidade == .E_Csm || tonalidade == .Fs_Dsm || tonalidade == .G_Em || tonalidade == .A_Fsm || tonalidade == .B_Gsm){
                return 21.0
            }
            else{
                return 21.5
            }
        case .b8:
            return 21.5
            
        default:
            break
        }
        
        return 0.0
    }
}

enum MidiCode: Int {
    
    //Oitava 0
    case c0  = 12
    case cS0 = 13
    case d0  = 14
    case eB0 = 15
    case e0  = 16
    case f0  = 17
    case fS0 = 18
    case g0  = 19
    case gS0 = 20
    case a0  = 21
    case bB0 = 22
    case b0  = 23
    
    //1 Oitava
    case c1  = 24
    case cS1 = 25
    case d1  = 26
    case eB1 = 27
    case e1  = 28
    case f1  = 29
    case fS1 = 30
    case g1  = 31
    case gS1 = 32
    case a1  = 33
    case bB1 = 34
    case b1  = 35
    
    //2 Oitava
    case c2  = 36
    case cS2 = 37
    case d2  = 38
    case eB2 = 39
    case e2  = 40
    case f2  = 41
    case fS2 = 42
    case g2  = 43
    case gS2 = 44
    case a2  = 45
    case bB2 = 46
    case b2  = 47
    
    //3 Oitava
    case c3  = 48
    case cS3 = 49
    case d3  = 50
    case eB3 = 51
    case e3  = 52
    case f3  = 53
    case fS3 = 54
    case g3  = 55
    case gS3 = 56
    case a3  = 57
    case bB3 = 58
    case b3  = 59
    
    //4 Oitava
    case c4  = 60
    case cS4 = 61
    case d4  = 62
    case eB4 = 63
    case e4  = 64
    case f4  = 65
    case fS4 = 66
    case g4  = 67
    case gS4 = 68
    case a4  = 69
    case bB4 = 70
    case b4  = 71
    
    //5 Oitava
    case c5  = 72
    case cS5 = 73
    case d5  = 74
    case eB5 = 75
    case e5  = 76
    case f5  = 77
    case fS5 = 78
    case g5  = 79
    case gS5 = 80
    case a5  = 81
    case bB5 = 82
    case b5  = 83
    
    //6 Oitava
    case c6  = 84
    case cS6 = 85
    case d6  = 86
    case eB6 = 87
    case e6  = 88
    case f6  = 89
    case fS6 = 90
    case g6  = 91
    case gS6 = 92
    case a6  = 93
    case bB6 = 94
    case b6  = 95
    
    //7 Oitava
    case c7  = 96
    case cS7 = 97
    case d7  = 98
    case eB7 = 99
    case e7  = 100
    case f7  = 101
    case fS7 = 102
    case g7  = 103
    case gS7 = 104
    case a7  = 105
    case bB7 = 106
    case b7  = 107
    
    //8 Oitava
    case c8  = 108
    case cS8 = 109
    case d8  = 110
    case eB8 = 111
    case e8  = 112
    case f8  = 113
    case fS8 = 114
    case g8  = 115
    case gS8 = 116
    case a8  = 117
    case bB8 = 118
    case b8  = 119
    
    
    case none = -1
    case none2 = 0
    case none3 = 200
}
