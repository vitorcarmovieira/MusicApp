//
//  DrawSubtitle.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 4/5/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class DrawSubtitle {
    var subtitle = ""
    var position = CGRect()
    var adapter: AdapterSubtitle!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterSubtitle()
        }
        adapter.subtitle = subtitle
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
