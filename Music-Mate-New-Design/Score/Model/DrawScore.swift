//
//  DrawScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol DrawScoreDelegate: class {
    func changePage(_ newPage: Int)
    @objc optional func scorePaused()
    @objc optional func scoreStopped()
    @objc optional func scoreStart()
}

class DrawScore: NSObject, NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let source = dataSource?.copy()
        
        let score = DrawScore(dataSource: source as! ModelScore)
        
        return score
    }
    
    var timeTeste: UInt64 = 0
    
    weak var delegate: DrawScoreDelegate?
    
    var scroll  = false
    var state   = ScoreState.playing
    var funcion = EspecificFunction.general { didSet { verifyFunction() } }

    private var paused = false
    var dataSource : ModelScore?
    
    var pages        : [DrawPageScore]  = []
    var instruments  : [DrawInstrument] = []
    var bars         : [DrawBar]        = []
    var startTimeBar : [DrawBarTime]    = []
    var startKeyBar  : [DrawKey]        = []
    
    deinit {
        pages.removeAll()
        instruments.removeAll()
        bars.removeAll()
        startTimeBar.removeAll()
        startKeyBar.removeAll()
        posCursor.removeAll()
    }
    var isSoundCursor    = true
    var isMinimizeCursor = false
    var isSoundLeft      = true
    var isSoundRight     = true
    var isPerformance    = true
    var isMidi           = false
    var isPlayCompose    = false
    
    var posCursor : [PosCursor] = []
    
    private var linesAccidents: [LineAccident] = []
    private var spacingType = SpacingType.fixed
    private var spacing: CGFloat = 0.0
    private var sizeScore : CGSize = CGSize()
    
    private var title: String {
        if dataSource != nil {
            return dataSource!.title
        }
        return "Score"
    }
    
    private var author: String {
        if dataSource != nil {
            return "\(dataSource!.author[0].firstName) \(dataSource!.author[0].lastName)"
        }
        return ""
    }
    
    private var metronome: Int {
        if dataSource != nil {
            return dataSource!.metronome
        }
        return 60
    }
    
    private var quantization: Quantization {
        if dataSource != nil {
            switch dataSource!.quantization {
            case 16:
                self.ticksQuantization = 240
                return Quantization(type: .semiquaver)
            case 8:
                self.ticksQuantization = 480
                return Quantization(type: .quaver)
            case 4:
                self.ticksQuantization = 960
                return Quantization(type: .quarter)
            case 2:
                self.ticksQuantization = 1920
                return Quantization(type: .half)
            case 1:
                self.ticksQuantization = 3840
                return Quantization(type: .whole)
            default:
                self.ticksQuantization = 960
                return Quantization(type: .quarter)
            }
        }
        return Quantization(type: .quarter)
    }
    
    private var baseNote: BaseNote {
        if dataSource != nil {
            switch dataSource!.baseNote {
            case 16:
                return .semiquaver
            case 8:
                return .quaver
            case 4:
                return .quarter
            case 2:
                return .half
            case 1:
                return .whole
            default:
                return BaseNote.quarter
            }
        }
        return BaseNote.quarter
    }
    
    var durationTime: Double {
        return 60.0 / (Double(metronome) * (4.0 / Double(baseNote.rawValue) ) )
    }
    
    var durationBar: Double {
        if let time = startTimeBar.first {
            return durationTime * Double(time.numerator)
        }
        
        return 0.0
    }
    
    private var placesPerTime: Int {
        if let time = startTimeBar.first {
            let den = time.denominator
            let quant = quantization.type.rawValue
            
            if quant < den {
                return 1
            }
            return quant / den
        }
        return 2
    }
    
    private var placesPerBar: Int {
        if let time = startTimeBar.first {
            let num = time.numerator
            return num * self.placesPerTime
        }
        return 8
    }
    
    private var numberOfClefs: Int {
        if let source = dataSource {
            if source.typeInstrument.clef == 1 {
                return 1
            } else {
                return 2
            }
        }

        return 2
    }
    
    var numberOfPages: Int {
        return self.pages.count
    }

    var barDuration: Int {
        if let time = startTimeBar.first {
            let num = time.numerator
            var duration = 960
            switch time.denominator {
            case 2:
                duration = 1920
            case 4:
                duration = 960
            case 8:
                duration = 480
            case 16:
                duration = 240
            default:
                return 3840
            }
            return num * duration
        }
        return 3840
    }

    required override init() {
        super.init()
    }

    init(score: DrawScore) {
        
    }
    
    init(dataSource: ModelScore) {
        super.init()
        self.dataSource = dataSource
//        changeStartConfigurations()
//        addNotesFromDataSource()
    }
    
    /// Altera as configurações iniciais da partitura (tempo, armadura) a partir dos dados do datasource
    func changeStartConfigurations() {
        startTimeBar.removeAll()
        startKeyBar.removeAll()
        
        if let source = dataSource {
            for config in source.configurations {
                if config.bar == 1 {
                    for i in 0..<numberOfClefs {
                        //--------------Add Initial Time--------------
                        let timeBar = DrawBarTime()
                        timeBar.numerator   = config.numerator
                        timeBar.denominator = config.denominator
                        
                        startTimeBar.append(timeBar)
                        //--------------------------------------------
                        
                        //--------------Add Initial Key---------------
                        let keyBar = DrawKey()
                       // let inst = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
                        
                        if config.key < 0 {
                            keyBar.type = KeyType(rawValue: config.key*(-1))!
                        } else {
                            keyBar.type = KeyType(rawValue: config.key)!
                        }
                        
                        
                        if i == 0 {
                            keyBar.clef = .SOL
                        } else {
                            keyBar.clef = .FA
                        }
                        keyBar.initializeAccidentsTone()
                        keyBar.createKey()
                        
                        startKeyBar.append(keyBar)
                        //--------------------------------------------
                    }
                }
            }
        }
        // tone.setTonality(tonality: startKeyBar.first!)
    }

    
    /// Altera o metronomo atual da partitura
    ///
    /// - Parameter metronome: Novo valor do metronomo
    func changeMetronome(metronome: Int) {
        if let source = dataSource {
            source.metronome = metronome
        }
    }
    
    /// Altera a formatação da partitura de acordo com o seu estado atual
    func verifyState() {
        switch state {
        case .composing:
            spacingType = .fixed
            changeStartConfigurations()
            bars.removeAll()
            if let source = dataSource {
                if source.notes.count == 0 {
                    for _ in 0..<50 {
                        addBar()
                    }
                } else {
                    spacingType = .flexible
                    addNotesFromDataSource()
                    addPausesFromDataSource()
                    addRepetitionsFromDataSource()
                    addDSDCFromDataSource()
//                    for _ in 0..<20 {
//                        addBar()
//                    }
                }
            }
//            for bar in bars {
//                bar.joinNotes()
//            }
            
        case .resumeComposing:
            spacingType = .fixed
        case .playing:
            spacingType = .flexible
            changeStartConfigurations()
            addNotesFromDataSource()
            addPausesFromDataSource()
            addRepetitionsFromDataSource()
            addDSDCFromDataSource()
        case .resumePlaying:
            spacingType = .flexible
        case .performance:
            spacingType = .flexible
        case .resumePerformance:
            spacingType = .flexible
        case .editable:
            spacingType = .fixed
            changeStartConfigurations()
            addNotesFromDataSource()
            addPausesFromDataSource()
            addRepetitionsFromDataSource()
            addDSDCFromDataSource()
        case .analysing:
            spacingType = .fixed
        case .compare:
            spacingType = .fixed
            changeStartConfigurations()
            addNotesFromDataSource(isToPaint: true)
            addPausesFromDataSource()
            addRepetitionsFromDataSource()
            addDSDCFromDataSource()
        }
    }
    
    /// Verifica a função atual da partitura e mostra ou esconde as areas de canalização
    func verifyFunction() {
        switch funcion {
        case .general:
            hiddenChannelAreas()
        case .canalyse13:
            showChannelAreas()
        case .canalyse12:
            showChannelAreas()
        case .canalyse34:
            showChannelAreas()
        case .canalyseAbove:
            showChannelAreas()
        case .canalyseCenter:
            showChannelAreas()
        case .canalyseManual:
            showChannelAreas()
        }
    }
    
    /// Esconde as áreas de canalização
    func hiddenChannelAreas() {
        for page in pages {
            for area in page.channelAreas {
                area.adapter.hiddenViews()
            }
        }
    }
    
    /// Mostra as areas de canalização
    func showChannelAreas() {
        DispatchQueue.main.async(execute: {
            for page in self.pages {
                for area in page.channelAreas {
                    if (self.funcion == .canalyse13 && area.type == .oneTree) || (self.funcion == .canalyse12 && area.type == .oneTwo) || (self.funcion == .canalyse34 && area.type == .treeFour) || (self.funcion == .canalyseAbove && area.type == .hAcima) || (self.funcion == .canalyseCenter && area.type == .hMeio) {
                        area.adapter.showViews()
                    } else {
                        area.adapter.hiddenViews()
                    }
                }
            }
        })
    }
    
    func showPlaces() {
        DispatchQueue.main.async(execute: {
            for bar in self.bars {
                for pentagram in bar.pentagrams {
                    for channel in pentagram.channels {
                        for time in channel.times {
                            for place in time.places {
                                if place.adapter != nil {
                                    if place.adapter.view != nil {
                                        place.adapter.view.isHidden = false
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    func hiddenPlaces() {
        for bar in bars {
            for pentagram in bar.pentagrams {
                for channel in pentagram.channels {
                    for time in channel.times {
                        for place in time.places {
                            if place.adapter != nil {
                                if place.adapter.view != nil {
                                    place.adapter.view.isHidden = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /// Adiciona um compasso ao final da partitura
    func addBar(isToCompletePage: Bool = false) {
        if let startT = startTimeBar.first {
            let time = DrawBarTime()
            time.numerator   = startT.numerator
            time.denominator = startT.denominator
            
            if let startK = startKeyBar.first {
                let key = DrawKey()
                key.type = startK.type
                
                let tIni = barDuration * bars.count
                let tEnd = barDuration * (bars.count + 1)

                let bar = DrawBar(numberOfPentagrams: self.numberOfClefs, numberOfPlacesPerTime: self.placesPerTime, time: startT, key: key, initialTime: tIni, endTime: tEnd, indexBar: bars.count + 1)
                bar.index = bars.count + 1
                bar.isToCompletePage = isToCompletePage

                if isToCompletePage || state != .composing {
                    bar.lastBar = false
                }
                //Verificar (Doubletap fez "entrar" duas vezes seguidas em vez de sair da primeira
                bars.append(bar)
            }
        }
    }

    private func addPausesFromDataSource() {
        if let source = dataSource {
            for pause in source.pauses {
                addPause(channel: pause.getChannel(), start: pause.getStartTick(), end: pause.getEndTick())
            }
        }
    }
    
    private func addPause(channel: Int, start: Int, end: Int) {
        insertPause(start: start, end: end, channel: channel)
    }
    
    func insertPause(start: Int, end: Int, channel: Int) {
        if start % 960 == 0 {
            insertPauseInCorrectTime(start: start, end: end, channel: channel)
        } else {
            insertPauseInIncorrectTime(start: start, end: end, channel: channel)
        }
    }
    
    private func insertPauseInCorrectTime(start: Int, end: Int, channel: Int) {
        var startPause = start
        var remainingDuration = end - start
        
        var pauses: [DrawPause] = []
        
        while remainingDuration > 0 {
            let barOfThePause = getBarOfTheNote(tickStart: startPause)
            let remainingTimeInBar = getRemainingTimeInBar(tick: startPause, tickEndBar: barOfThePause.endTime)
            
            if remainingDuration <= remainingTimeInBar {
                insertPauseFinal(pauses: &pauses, start: startPause, end: end, channel: channel, bar: barOfThePause)
                remainingDuration -= end - startPause
            } else {
                let endPause = startPause + remainingTimeInBar
                insertPauseFinal(pauses: &pauses, start: startPause, end: endPause, channel: channel, bar: barOfThePause)
                remainingDuration -= (endPause - startPause)
                startPause = endPause
            }
        }
        
//        for pause in pauses {
//
//        }
    }
    
    private func insertPauseInIncorrectTime(start: Int, end: Int, channel: Int) {
        var startPause = start
        var remainingDuration = end - start
        
        var pauses: [DrawPause] = []
        
        while remainingDuration > 0 {
            let barOfThePause = getBarOfTheNote(tickStart: startPause)
            let endTime = ((startPause / 960) + 1) * 960
            
            var endPause = endTime
            
            if startPause + remainingDuration < endTime {
                endPause = startPause + remainingDuration
            }
            
            insertPauseFinal(pauses: &pauses, start: startPause, end: endPause, channel: channel, bar: barOfThePause)
            
            remainingDuration -= (endTime - startPause)
            startPause = endTime
            
        }
        
//        for pause in pauses {
//
//        }
    }
    
    private func insertPauseFinal(pauses: inout [DrawPause], start: Int, end: Int, channel: Int, bar: DrawBar) {
        var remainingDuration = end - start
        
        var startPause = start
        
        while remainingDuration > 0 {
            let possibleDuration = getGreaterDurationOfPausePossible(duration: remainingDuration, bar: bar)
            
            if (startPause + possibleDuration) - startPause != 0 {
                bar.addPause(channel: channel, start: startPause, end: startPause + possibleDuration)
//                bar.addNote(midi: midi, initialTime: startNote, endTime: startNote + possibleDuration, channel: channel, predecessor: p, successor: s, startNote: !p, realInitialTime: startNote, realEndTime: startNote + possibleDuration)
            } else {
                bar.addPause(channel: channel, start: startPause, end: startPause + getTicksOfQuantization())
//                bar.addNote(midi: midi, initialTime: startNote, endTime: startNote + getTicksOfQuantization(), channel: channel, predecessor: p, successor: s, startNote: !p, realInitialTime: startNote, realEndTime: startNote + possibleDuration)
                remainingDuration = 0
            }
//            let pause = DrawPause(channel: channel, start: start, end: end)
//            pauses.append(pause)
            
            remainingDuration -= possibleDuration
            
            startPause += possibleDuration
        }
    }
    
    private func getGreaterDurationOfPausePossible(duration: Int, bar: DrawBar) -> Int {
        var possibleDuration = 0
        
        let possiblesDurations = [240, 480, 960, 1920, 3840]
        
        for p in possiblesDurations {
            if p <= duration {
                possibleDuration = p
            }
        }
        
        return possibleDuration
    }
    
    private var ticksQuantization = 0
    /// Adiciona todas as notas presentes no datasource
    private func addNotesFromDataSource(isToPaint: Bool = false) {
        
        bars.removeAll()

        if let source = dataSource {
            var greaterTime = 0
            for modelNote in source.notes {
                greaterTime = max(modelNote.tIniQuantized + modelNote.durationQuantized, greaterTime)
            }
            
            let endBar = getBarEndTime(time: greaterTime)

            for _ in 0..<endBar {
                addBar()
            }

            ticksQuantization = quantization.ticks
            durationBarUse    = barDuration

            
            for i in 0..<source.notes.count {
              //  let tomVisivel = source.notes[i].tone + DAOTypeInstrument.getInstance().getTypeInstrumentActive().midiTransform
                
                if isToPaint {
                    addNote(midi: source.notes[i].tone, tIniQuantized: source.notes[i].tIniQuantized, durationQuantized: source.notes[i].durationQuantized, channel: source.notes[i].channel, isCorrect: source.notes[i].isCorrect)
                } else {
                    addNote(midi: source.notes[i].tone, tIniQuantized: source.notes[i].tIniQuantized, durationQuantized: source.notes[i].durationQuantized, channel: source.notes[i].channel)
                }
            }
        
        }

        
        for bar in bars {
            bar.joinNotes()
        }
        
     
    }
    
    /// Adiciona todas as repetições presentes no datasource
    private func addRepetitionsFromDataSource() {
        if let source = dataSource {
            for modelRepeat in source.repeats {
                addRepeat(initialBar: modelRepeat.initialBar, finalBar: modelRepeat.finalBar, voltaBracket: modelRepeat.voltaBrackets)
            }
        }
    }
    
    /// Adiciona todos os DS DC presentes no datasource
    private func addDSDCFromDataSource() {
        if let source = dataSource {
            for modelRepeat in source.dsDcs {
                addDSDC(initialBar: modelRepeat.initialBar, finalBar: modelRepeat.finalBar, coda: modelRepeat.coda, toCoda: modelRepeat.toCoda, dsAlFine: modelRepeat.dsAlFine, dsAlCoda: modelRepeat.dsAlCoda, dcAlCoda: modelRepeat.dcAlCoda, dcAlFine: modelRepeat.dcAlFine, fine: modelRepeat.fine)
            }
        }
    }
    
    /// Altera o título exibido na partitura
    func changeTitle() {
        if pages.count > 0 {
            if let source = dataSource {
                if pages[0].header.title.adapter != nil {
                    if pages[0].header.title.adapter.view != nil {
                        pages[0].header.title.adapter.view.title = source.title
                    }
                }
            }
        }
    }
    
    func changeSubTitle() {
        if pages.count > 0 {
            if let source = dataSource {
                if pages[0].header.subtitle.adapter != nil {
                    if pages[0].header.subtitle.adapter.view != nil {
                        pages[0].header.subtitle.adapter.view.subtitle = source.subtitle
                    }
                }
            }
        }
    }
    
    func changeAuthor() {
        if pages.count > 0 {
            if let source = dataSource {
                if pages[0].header.author.adapter != nil {
                    if pages[0].header.author.adapter.view != nil {
                        pages[0].header.author.adapter.view.composer = "\(source.author[0].firstName) \(source.author[0].lastName)"
                    }
                }
            }
        }
    }
    
    /// Altera o metronomo exibido na partitura
    func changeMetronome() {
        if pages.count > 0 {
            if let source = dataSource {
                if pages[0].header.metronome.value.adapter != nil {
                    if pages[0].header.metronome.value.adapter.view != nil {
                        pages[0].header.metronome.value.adapter.view.value = source.metronome
                    }
                }
            }
        }
    }
    
    /// Adiciona uma repetição a partitura
    ///
    /// - Parameters:
    ///   - initialBar: Posição do compasso inicial
    ///   - finalBar: Posição do compasso final
    ///   - voltaBracket: Numero da chave de volta (0 - sem chave, 1, 2)
    func addRepeat(initialBar: Int, finalBar: Int, voltaBracket: Int) {
//        if bars.count > finalBar + voltaBracket {
        if bars.count >= finalBar {
            bars[initialBar - 1].addRepeat(type: .start)
            bars[finalBar - 1].addRepeat(type: .end)
            if voltaBracket != 0 {
                bars[finalBar - voltaBracket].addRepeat(type: .bracket1)
//                if finalBar < bars.count {
                bars[finalBar].addRepeat(type: .bracket2)
//                }
            }
//            if voltaBracket == 2 {
//                bars[finalBar - 2].addRepeat(type: .bracket1)
//                bars[finalBar].addRepeat(type: .bracket2)
//            }
        }
    }
    
    /// Adiciona um DS ou DC a partitura
    ///
    /// - Parameters:
    ///   - initialBar: Compasso Inicial
    ///   - finalBar: Compasso Final
    ///   - coda: Compasso onde se encontra a coda (para -1 não há coda no DS DC)
    ///   - toCoda: Compasso onde se encontra o toCoda (para -1 não há toCoda no DS DC)
    ///   - dsAlFine: Compasso onde se encontra o dsAlFine (para -1 não há dsAlFine no DS DC)
    ///   - dsAlCoda: Compasso onde se encontra o dsAlCoda (para -1 não há dsAlCoda no DS DC)
    ///   - dcAlCoda: Compasso onde se encontra o dcAlCoda (para -1 não há dcAlCoda no DS DC)
    ///   - dcAlFine: Compasso onde se encontra o dcAlFine (para -1 não há dcAlFine no DS DC)
    ///   - fine: Compasso onde se encontra fine (para -1 não há fine no DS DC)
    func addDSDC(initialBar: Int, finalBar: Int, coda: Int, toCoda: Int, dsAlFine: Int, dsAlCoda: Int, dcAlCoda: Int, dcAlFine: Int, fine: Int) {
        if initialBar != -1 {
            bars[initialBar - 1].addDSDC(type: .segno)
        }
        if finalBar != -1 {
            bars[finalBar - 1].addDSDC(type: .segno)
        }
        if coda != -1 {
            bars[coda - 1].addDSDC(type: .coda)
        }
        if toCoda != -1 {
            bars[toCoda - 1].addDSDC(type: .toCoda)
        }
        if dsAlFine != -1 {
            bars[dsAlFine - 1].addDSDC(type: .dsAlFine)
        }
        if dsAlCoda != -1 {
            bars[dsAlCoda - 1].addDSDC(type: .dsAlCoda)
        }
        if dcAlFine != -1 {
            bars[dcAlFine - 1].addDSDC(type: .dcAlFine)
        }
        if dcAlCoda != -1 {
            bars[dcAlCoda - 1].addDSDC(type: .dcAlCoda)
        }
        if fine != -1 {
            bars[fine - 1].addDSDC(type: .fine)
        }
    }
    
    /// Adiciona novos compassos enquanto o tempo final da paritura for menor que o tempo final passado
    ///
    /// - Parameter endTime: Tempo Final passado
    private func addNecessaryBars(endTime: Int) {
        let endBar = getBarEndTime(time: endTime)
        
        //Adiciona novos compassos enquanto a quantidade de compassos for menor que endBar
        while bars.count < endBar {
            addBar()
        }
    }
    
    /// Devolve o compasso em que o tempo passado se encontra
    ///
    /// - Parameter time: Tempo que se deseja saber o compasso (em ticks)
    /// - Returns: Compasso ao qual o tempo passado pertence
    private func getBarInitialTime(time: Int) -> Int {
        return (time / barDuration) + 1
    }
    
    /// Devolve o compasso final em que o tempo passado se encontra
    ///
    /// - Parameter time: Tempo que se deseja saber o compasso (em ticks)
    /// - Returns: Compasso final ao qual o tempo passado pertence
    private func getBarEndTime(time: Int) -> Int {
        var endBar = (time / barDuration) + 1
        
        if time % barDuration == 0 {
            endBar -= 1
        }
        
        return endBar
    }
    
    /// Adiciona uma nota a partitura
    ///
    /// - Parameters:
    ///   - midi: Código midi da nota (12 - 120)
    ///   - tIniQuantized: Tempo inicial quantizado (em ticks)
    ///   - durationQuantized: Duração quantizada (em ticks)
    ///   - channel: Canal da nota (1, 2, 3, 4)
    func addNote(midi: Int, tIniQuantized: Int, durationQuantized: Int, channel: Int, isCorrect: Bool = true) {
//        addNecessaryBars(endTime: tIniQuantized + durationQuantized)

        _ = insertNote(midi: midi, start: tIniQuantized, end: tIniQuantized + durationQuantized, channel: channel, isCorrect: isCorrect)
        
//        var residualDuration = durationQuantized
//        var currentTime = tIniQuantized
//        var successor = true
//        var predecessor = false
//        var startNote = true
//
//        /*Aqui a nota é fracionada em quantidade de tempo igual ao tempo da nota de quantização
//         Por exemplo, nota com duração 3840 ticks em uma quantização de 1/8:
//         Será divida em 8 frações de 1/8 (somando-se um total de 3840 ticks - (8 * 480 = 3840) )
//
//         Posteriormente essas frações posteriormente serão unidas
//         */
//        while residualDuration > 0 {
//            print(durationBarUse,midi, tIniQuantized, durationQuantized, channel)
//            let bar = Int(currentTime / barDuration)
//
//            if currentTime + ticksQuantization == tIniQuantized + durationQuantized {
//                successor = false
//            }
//
//            if currentTime > tIniQuantized {
//                startNote = false
//            }
//
//            bars[bar].addNote(midi: midi, initialTime: currentTime, endTime: currentTime + ticksQuantization, channel: channel, predecessor: predecessor, successor: successor, startNote: startNote, realInitialTime: tIniQuantized, realEndTime: tIniQuantized + durationQuantized)
//
//            predecessor = true
//
//            currentTime += ticksQuantization
//            residualDuration -= ticksQuantization
//        }
        
    }
    
    func insertNote(midi: Int, start: Int, end: Int, channel: Int, isCorrect: Bool) -> [Bool : String] {
        
        if midi < MidiCode.c0.rawValue || midi > MidiCode.b8.rawValue {
            return [false : "Midi range is invalid"]
        }
        
        if start % 960 == 0 {
            insertNoteInCorrectTime(midi: midi, start: start, end: end, channel: channel, isCorrect: isCorrect)
        } else {
            insertNoteInIncorrectTime(midi: midi, start: start, end: end, channel: channel, isCorrect: isCorrect)
        }
        
        return [true : "Success"]
    }
    
    private func insertNoteInCorrectTime(midi: Int, start: Int, end: Int, channel: Int, isCorrect: Bool) {
        var barOfTheNote = getBarOfTheNote(tickStart: start)
        
        var startNote = start
        var remainingDuration = end - start
        
        var notes: [DrawNote] = []
        
        var predecessor = false
        var sucessor    = false
        
        let startBar = barOfTheNote.initialTime
        let diferenceStart = start - startBar
        let diferenceEnd   = end - startBar
        let timeStart      = diferenceStart / 960
        let timeEnd        = (diferenceEnd - 1) / 960

        while remainingDuration > 0 {
            let remainingTimeInBar = getRemainingTimeInBar(tick: startNote, tickEndBar: barOfTheNote.endTime)
            
            if remainingDuration <= remainingTimeInBar {
                let barTime = startTimeBar[0]

                if timeStart == 1 && timeEnd > 1 && (barTime.numerator == 4 && barTime.denominator == 4) {
                    sucessor = true
                    
                    let endTime1 = startBar + (960 * 2)
                    insertNoteFinal(notes: &notes, midi: midi, start: startNote, end: endTime1, channel: channel, predecessor: predecessor, sucessor: sucessor, realStart: start, bar: barOfTheNote, isCorrect: isCorrect)
                    
                    sucessor = false
                    
                    remainingDuration -= end - startNote
                    
                    startNote = endTime1
                    
                    insertNoteFinal(notes: &notes, midi: midi, start: startNote, end: end, channel: channel, predecessor: predecessor, sucessor: sucessor, realStart: start, bar: barOfTheNote, isCorrect: isCorrect)
                } else {
                    sucessor = false
                    insertNoteFinal(notes: &notes, midi: midi, start: startNote, end: end, channel: channel, predecessor: predecessor, sucessor: sucessor, realStart: start, bar: barOfTheNote, isCorrect: isCorrect)
                    remainingDuration -= end - startNote
                }
            } else {
                let endNote = startNote + remainingTimeInBar
                sucessor = true
                insertNoteFinal(notes: &notes, midi: midi, start: startNote, end: endNote, channel: channel, predecessor: predecessor, sucessor: sucessor, realStart: start, bar: barOfTheNote, isCorrect: isCorrect)
                predecessor = true
                remainingDuration -= (endNote - startNote)
                startNote = endNote
                barOfTheNote = getBarOfTheNote(tickStart: startNote)
            }
        }
        
        for note in notes {
            notes.append(note)
        }
    }
    
    private func insertNoteInIncorrectTime(midi: Int, start: Int, end: Int, channel: Int, isCorrect: Bool) {
        var startNote = start
        var remainingDuration = end - start
        
        var notes: [DrawNote] = []
        
        var predecessor = false
        var sucessor    = true
        
        while remainingDuration > 0 {
            let barOfTheNote = getBarOfTheNote(tickStart: startNote)
            
            let endTime = ((startNote / 960) + 1) * 960
            
            if endTime >= end { sucessor = false }
            
            var endNote = endTime
            
            if startNote + remainingDuration < endTime {
                endNote = startNote + remainingDuration
            }
            
            insertNoteFinal(notes: &notes, midi: midi, start: startNote, end: endNote, channel: channel, predecessor: predecessor, sucessor: sucessor, realStart: start, bar: barOfTheNote, isCorrect: isCorrect)
            
            remainingDuration -= (endTime - startNote)
            startNote = endTime
            
            predecessor = true
        }
        
        for note in notes {
            //            print(note.getMidi(), note.getTickStart(), note.getDuration(), note.getPredecessor(), note.getSucessor())
            notes.append(note)
        }
    }
    
    private func insertNoteFinal(notes: inout [DrawNote], midi: Int, start: Int, end: Int, channel: Int, predecessor: Bool, sucessor: Bool, realStart: Int, bar: DrawBar, isCorrect: Bool) {
        var remainingDuration = end - start
        
        var p = predecessor
        var s = sucessor
        var startNote = start

        while remainingDuration > 0 {
            let possibleDuration = getGreaterDurationOfNotePossible(duration: remainingDuration)
            if startNote != start { p = true }
            if remainingDuration - possibleDuration > 0 { s = true } else if !sucessor { s = false }
            
            if (startNote + possibleDuration) - startNote != 0 {
                bar.addNote(midi: midi, initialTime: startNote, endTime: startNote + possibleDuration, channel: channel, predecessor: p, successor: s, startNote: !p, realInitialTime: realStart, realEndTime: end, isCorrect: isCorrect)
            } else {
                bar.addNote(midi: midi, initialTime: startNote, endTime: startNote + getTicksOfQuantization(), channel: channel, predecessor: p, successor: s, startNote: !p, realInitialTime: realStart, realEndTime: end, isCorrect: isCorrect)
                remainingDuration = 0
            }
//            let note = DrawNote(midi: MidiCode(rawValue: midi)!, tickStart: startNote, tickEnd: startNote + possibleDuration, predecessor: p, sucessor: s)
//            notes.append(note)
            
            remainingDuration -= possibleDuration
            
            startNote += possibleDuration
        }
    }
    
    private func getGreaterDurationOfNotePossible(duration: Int) -> Int {
        var possibleDuration = 0
        
        let possiblesDurations = [240, 360, 480, 720, 960, 1440, 1920, 2880, 3840, 5760]
        
        for p in possiblesDurations {
            if p <= duration {
                possibleDuration = p
            }
        }
        
        return possibleDuration
    }
    
    private func getTicksOfQuantization() -> Int {
        if let source = dataSource {
            switch source.quantization {
            case 2:
                return 1920
            case 4:
                return 960
            case 8:
                return 480
            case 16:
                return 240
            default:
                break
            }
        }
        
        return 960
    }
    
    private func durationNoteIsPossible(duration: Int) -> Bool {
        switch duration {
        case 240:
            return true
        case 360:
            return true
        case 480:
            return true
        case 720:
            return true
        case 960:
            return true
        case 1440:
            return true
        case 1920:
            return true
        case 2880:
            return true
        case 3840:
            return true
        case 5760:
            return true
        default:
            return false
        }
    }

    private func getBarOfTheNote(tickStart: Int) -> DrawBar {
        for bar in bars {
            if bar.initialTime <= tickStart && bar.endTime > tickStart {
                return bar
            }
        }
        
        var foundBar = false
        
        while !foundBar {
            addBar()
            
            if bars.count > 0 {
                if bars[bars.count - 1].initialTime <= tickStart  && bars[bars.count - 1].endTime > tickStart {
                    foundBar = true
                    
                    return bars[bars.count - 1]
                }
            }
        }
    }
    
    private func getRemainingTimeInBar(tick: Int, tickEndBar: Int) -> Int {
        return tickEndBar - tick
    }
    
    /// Adiciona os acidentes necessários a cada lugar da partitura
    private func addAccidents() {
        for bar in bars {
            if let key = startKeyBar.first {
                
                bar.addAccident(linesAccidents: &linesAccidents, key: key)
            }
        }
    }
    
    private func joinNotes() {
        for i in 0..<bars.count {
            bars[i].joinNotes()
        }
    }
    
    private var durationBarUse = 0
    
    private func getNoteBar(initialTime: Int, endTime: Int) -> Int {
        
        return Int(initialTime / durationBarUse)
        
//        for i in 0..<bars.count {
//            if bars[i].initialTime <= initialTime && endTime <= bars[i].endTime {
//                return i
//            }
//        }
//
//        return 0
    }
    
    func createPages(parameters: DrawParameter, isToAddPage: Bool = false) {

        pages = []

        pages.removeAll()

        for bar in bars {
            if let key = startKeyBar.first {
                bar.resetLinesAccidents(linesAccidents: &linesAccidents, key: key)
            }
        }

        addAccidents()
        
        
        let space = parameters.spacing
        let initialYWithHeader = parameters.initialYWithHeader
        let initialY: CGFloat = (parameters.height * 0.05)
        var spcNec: CGFloat = parameters.spaceNecessary
        var border: CGFloat = parameters.borderBar
        if state == .editable {
//            space = parameters.spacingEdit
//            initialY = (parameters.height * 0.13)
            spcNec = 0
            border = parameters.borderBarEdit
        }
        
        self.spacing = space
        
        var currentX: CGFloat = parameters.borderFirstBar
        var currentY: CGFloat = initialYWithHeader + (getBarRelativeHeight() * space) + initialY
        
        addPage(parameters: parameters)
        
        var barsInLine = 0
        var linesInPage = 0
        
        var i = 0
        
        var founded = false
        while i < bars.count {
            bars[i].lastBar = false

            
            var widthTime: CGFloat = 0.0
            if bars[i].index == 1 {
                let clefSize = (bars[i].pentagrams[0].clef.getClefRelativeSize() * space)
                let keySize  = (startKeyBar.first?.getKeyRelativeSize())! * space

                widthTime = (bars[i].pentagrams[0].timeBar.getTimeRelativeSize() * space) + clefSize + keySize
            }
            
            bars[i].initial = false
            
            if i == 0 {
                bars[i].initial = true
            }
            
            let barWidth  = (bars[i].getRelativeSize(spacing: spacingType, quantization: (dataSource?.quantization)!) * space) + widthTime + (2.0 * border)
            let barHeight = getBarRelativeHeight() * space
            
            currentX += barWidth
            
            bars[i].position.size = CGSize(width: barWidth, height: barHeight)
            
            if (currentX <= parameters.width - parameters.rightBorder) && (barsInLine <= 4) {
                bars[i].position.origin = CGPoint(x: currentX - barWidth, y: currentY - barHeight)
                pages[pages.count - 1].bars.append(bars[i])
                barsInLine += 1
                if i == bars.count - 1 {
                    if !founded {
                        bars[i].lastBar = true
                        founded = true
                    }
                    if !scroll {
                        addBar(isToCompletePage: true)
                    }
                }
            } else if (currentY + barHeight + spcNec) <= parameters.height || scroll {
                let keySize  = bars[i].pentagrams[0].key.getKeyRelativeSize() * space
                currentX = parameters.leftBorder + barWidth + (bars[i].pentagrams[0].clef.getClefRelativeSize() * space) + (2.0 * border) + keySize
                currentY += barHeight
                bars[i].initial = true
                let totalBarWidth = barWidth + (bars[i].pentagrams[0].clef.getClefRelativeSize() * space) + (2.0 * border)
                bars[i].position.origin = CGPoint(x: currentX - totalBarWidth, y: currentY - barHeight)
                bars[i].position.size = CGSize(width: totalBarWidth, height: barHeight)
                pages[pages.count - 1].bars.append(bars[i])
                barsInLine = 0
                linesInPage += 1
                if i == bars.count - 1 {
                    if !founded {
                        bars[i].lastBar = true
                        founded = true
                    }
                    if !scroll {
                        addBar(isToCompletePage: true)
                    }
                }
            } else {
                if i == bars.count - 1 {
                    if !founded {
                        bars[i].lastBar = true
                        founded = true
                    }
                    if !scroll {
                        addBar(isToCompletePage: true)
                    }
                }
                if bars[i].isToCompletePage {
                    break
                }
                
                let keySize  = bars[i].pentagrams[0].key.getKeyRelativeSize() * space
                currentX = parameters.leftBorder + barWidth + (bars[i].pentagrams[0].clef.getClefRelativeSize() * space) + (2.0 * border) + keySize
                currentY = initialYWithHeader + barHeight + initialY
                addPage(parameters: parameters)
                bars[i].initial = true
                let totalBarWidth = barWidth + (bars[i].pentagrams[0].clef.getClefRelativeSize() * space) + (2.0 * border)
                bars[i].position.origin = CGPoint(x: currentX - totalBarWidth, y: currentY - barHeight)
                bars[i].position.size = CGSize(width: totalBarWidth, height: barHeight)
                pages[pages.count - 1].bars.append(bars[i])
                barsInLine = 0
                linesInPage += 1
            }
            
            if bars[i].initial {
                for pentagrama in bars[i].pentagrams {
                    pentagrama.initialBar = true
                    let key = DrawKey(type: (startKeyBar.first?.type)!, clef: pentagrama.clef.type)
                    key.initializeAccidentsTone()
                    key.createKey()
                    pentagrama.key = key
                }
            } else {
                for pentagrama in bars[i].pentagrams {
                    pentagrama.initialBar = false
                    pentagrama.key.eraseKey()
//                    pentagrama.clef.type = .NONE
                }
            }
            
            sizeScore = CGSize(width: parameters.width, height: currentY)
            
            i += 1
        }
        
        
        
        for i in 0..<pages.count {
            for bar in pages[i].bars {
                bar.page = i
            }
            pages[i].adjustsPosition()
            pages[i].numberOfPages = pages.count
            pages[i].generateLigatures()
        }
    }
    
    func addPageToScore() {
//        createPages(parameters: par)
    }
    
    func getSizeScore() -> CGSize {
        return sizeScore
    }
    
    private func addPage(parameters: DrawParameter) {
        let page = DrawPageScore()
        if dataSource != nil {
            page.header.title.title = (dataSource?.title)!
            if((dataSource?.author.count)!>0){
                page.header.author.author = "\(dataSource!.author[0].firstName) \(dataSource!.author[0].lastName)"
            }else {
                page.header.author.author = ""
            }
            page.header.subtitle.subtitle = (dataSource?.subtitle)!
        }
        page.state = state
        page.index = pages.count + 1
        page.parameters = parameters
        if let source = dataSource {
            page.metronome = source.metronome
        }
        
        pages.append(page)
    }
    
    func drawUnwrittenNotes() {
//        for bar in bars {
//            bar.joinNotes()
//        }
        
        for page in pages {
            page.isCompose = true
            page.drawUnwrittenNotes()
        }
    }
    
    func paintWrongNotes() {
        for page in pages {
            if let source = dataSource {
                page.paintWrongNotes(currentTime: getCurrentTime(), notesPlayed: source.playedNotes)
            }
        }
    }
    
    private func getCurrentTime() -> Int {
        return self.posCursor[currentIndex1].tIni - 1
//        var contL = 1
//
//        for (i, page) in self.pages.enumerated() {
//            if i >= self.currentPage {
//                self.delegate?.changePage(self.currentPage)
//                self.currentPage += 1
//            }
//
//            for (_, bar) in page.bars.enumerated() {
//                for (_, time) in bar.pentagrams[0].channels[0].times.enumerated() {
//                    if contL > self.cont {
//                        return time.initialTime
//                    }
//                    contL += 1
//                }
//            }
//        }
//        return 0
    }
    
    func getBarRelativeHeight() -> CGFloat {
        let borderSpaces: CGFloat = 6.0 * 2.0
        let middleSpaces: CGFloat = 7.0 * CGFloat(numberOfClefs - 1)
        let pentagramSpaces: CGFloat = 4.0 * CGFloat(numberOfClefs)
        
        return borderSpaces + middleSpaces + pentagramSpaces
    }
    
    func updateChannelNotes() -> Bool{
        var changed = false
        for bar in bars {
        for pentagram in bar.pentagrams {
        for channel in pentagram.channels {
        for time in channel.times {
        for place in time.places {
            
                if let source = dataSource {
                    for note in source.notes {
                        var channelTone = MidiCode.c1
                        var channelMidle = MidiCode.c1
                        if note.channel == 1 || note.channel == 2 {
                            channelTone  = place.channelTone12
                            channelMidle = place.channelTone12Midle
                        } else {
                            channelTone  = place.channelTone34
                            channelMidle = place.channelTone34Midle
                        }
                        if channelTone != .c0 {
                        
                            if note.tIniQuantized == place.initialTime {
                                if funcion == .canalyse12 {
                                    if note.tone <= channelTone.rawValue && (note.channel == 2 || note.channel == 1) {
                                        note.channel = 2
                                    } else if note.channel == 2 || note.channel == 1 {
                                        note.channel = 1
                                    }
                                    changed = true
                                }
                                if funcion == .canalyse13 {
//                                    print(channelTone.rawValue, note.tone)
                                    if note.tone <= channelTone.rawValue {
                                        note.channel = 3
                                    } else {
                                        note.channel = 1
                                    }
                                    changed = true
                                }
                                if funcion == .canalyse34 {
                                    if note.tone <= channelTone.rawValue && (note.channel == 4 || note.channel == 3) {
                                        note.channel = 4
                                    } else if note.channel == 4 || note.channel == 3 {
                                        note.channel = 3
                                    }
                                    changed = true
                                }
                                if funcion == .canalyseAbove {
                                    if note.tone >= channelTone.rawValue {
                                        note.isToDelete = true
                                    }
                                }
                                if funcion == .canalyseCenter {
                                    if note.tone >= channelTone.rawValue && note.tone <= channelMidle.rawValue {
                                        note.isToDelete = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        }
        }
        }

        if let source = dataSource {
            var i = 0
            while i < source.notes.count {
                if source.notes[i].isToDelete {
                    source.notes.remove(at: i)
                } else {
                    i += 1
                }
            }

            source.extendNotes(numerator: (startTimeBar.first?.numerator)!, denominator: (startTimeBar.first?.denominator)!)
        }
    
        return changed
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for bar in bars {
            bar.paintAllNotesWithColor()
        }
    }
    
    var viewCursor = ViewCursor()
    var cont = 0
    var timer = Timer()
    var player = AVAudioPlayer()
    var currentPage = 0
    var midiPlayer = MIDIPlayer()
    var lastInitialTick = 0
    var timerCountDown = Timer()
    var contCursor = 4
    var lblContage = LabelNoBorder()
    
    func verifyPostitions() {
        positionCursor()
        
        if delegate != nil {
            delegate?.changePage(currentPage)
        }
    }
    
    func playMIDI(onlyPlayedNotes: Bool = false) {
//         let musicaURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "metronomo111", ofType: "wav")!)
//
//        player = try! AVAudioPlayer(contentsOf: musicaURL as URL)
        if let source = dataSource {
            if state != .composing || isPlayCompose {
                createMidi(onlyPlayedNotes: onlyPlayedNotes)
                midiPlayer = MIDIPlayer(name: "midi", metronome: source.metronome, tickInitial: lastInitialTick)
                midiPlayer.play()
            }
        }
    }
    func stopMIDI(){
        midiPlayer.stop()
    }
    
    func play(viewCursor: ViewCursor, soundOn : Bool) {
//        self.generatePositionsCursor()
        paused = false
        
        let musicaURL = NSURL(fileURLWithPath: Bundle.main.path(forResource: "metronomo111", ofType: "wav")!)
        player = try! AVAudioPlayer(contentsOf: musicaURL as URL)
        
        self.viewCursor.isMinimized = isMinimizeCursor
        self.viewCursor = viewCursor
        self.viewCursor.updateLayout()
        
//        findBarCursor()
//        positionCursor()
//        showCursor()
        
        let framePg = (viewCursor.superview?.frame)!
        
        lblContage.frame = CGRect(x: 0, y: 0, width: framePg.width, height: framePg.height)
        lblContage.font = UIFont(name: "Lato-Bold", size: viewCursor.frame.width * 4.0)
        lblContage.textColor = UIColor(red: 140/255, green: 28/255, blue: 37/255, alpha: 1.0)
        lblContage.textAlignment = NSTextAlignment.center
        lblContage.text = "Ready"
        
        contCursor = ((startTimeBar.first?.numerator)! * 2) + 1
    
        if let source = dataSource {
            if state != .composing || isPlayCompose {
                
                midiPlayer = MIDIPlayer(name: "midi", metronome: source.metronome, tickInitial: self.posCursor[currentIndex1].tIni)
                print(self.posCursor[currentIndex1].tIni)
                midiPlayer.soundOnOff = soundOn
            }
        }
        
//        if state == .composing {
//            if let source = dataSource {
//                source.notes.removeAll()
//            }
//        }
        
        viewCursor.superview?.addSubview(lblContage)
        
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if !self.paused {
                if(soundOn){
                    self.countDown(soundOn: true)
                } else {
                    self.countDown(soundOn: false)
                }
                
                let timeTick = (60.0/Double((self.dataSource?.metronome)!)) * (4.0 / Double((self.startTimeBar.first?.denominator)!))
                self.timerCountDown = Timer.scheduledTimer(timeInterval: TimeInterval(timeTick), target: self, selector: #selector(self.countDown), userInfo:nil, repeats:true)
            }
        }
    }
    
    @objc private func countDown(soundOn : Bool) {
        if contCursor > (startTimeBar.first?.numerator)! + 1 {
            self.playMetronome()
        }
        //
        contCursor -= 1
        
        self.viewCursor.alpha = 0.4
        UIView.animate(withDuration: 0.3, animations: {
            self.viewCursor.alpha = 1.0
        })
        
        lblContage.text = "\(contCursor)"
        
        if contCursor > (startTimeBar.first?.numerator)! {
            lblContage.text = "Ready"
        } else {
            lblContage.frame = CGRect(x: viewCursor.frame.origin.x, y: viewCursor.frame.origin.y, width: viewCursor.frame.width * 40, height: viewCursor.frame.width * 10)
            lblContage.font = UIFont(name: "Lato-Regular", size: viewCursor.frame.width * 4.0)
            lblContage.textAlignment = NSTextAlignment.left
        }

        if contCursor == 0 {
            lblContage.removeFromSuperview()
            self.timerCountDown.invalidate()
        }

        if contCursor == startTimeBar.first?.numerator {
            if (delegate != nil) && (state == .composing || isPerformance) {
                delegate?.scoreStart!()
            }
            startPlay(soundOn: soundOn)
        }
    }
    
    private func startPlay(soundOn : Bool) {
        if !paused {
            moveCursor1()
//            moveCursor()
            let timeTick = (60.0/Double((dataSource?.metronome)!)) * (4.0 / Double((startTimeBar.first?.denominator)!))
            self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(timeTick), target: self, selector: #selector(self.moveCursor1), userInfo:nil, repeats:true)
            
            if (state != .composing || isPlayCompose) && !isPerformance {
                if midiPlayer.soundOnOff{
                    midiPlayer.play()
                }
            }
        }
    }
    
    func pause(tapped: Bool = false) {
        self.timerCountDown.invalidate()
        
        if lblContage.superview != nil {
            lblContage.removeFromSuperview()
        }
        
        self.timer.invalidate()
        
        if currentPage != 0 {
             currentPage -= 1
        }
        
        if let deleg = delegate {
            if tapped {
                deleg.scorePaused!()
            }
        }
        
        midiPlayer.stop()
        
        paused = true
    }
    
    func stop(terminated: Bool = false) {
        if self.timerCountDown.isValid {
            self.timerCountDown.invalidate()
            if lblContage.superview != nil {
                lblContage.removeFromSuperview()
            }
        }
        self.timer.invalidate()
        self.cont = 0
        self.currentPage = 0
        self.lastInitialTick = 0
        
        if let deleg = delegate {
            if terminated {
                deleg.scoreStopped!()
            }
        }
    }
    
    func playMetronome() {
        
        if player.isPlaying {
            player.stop()
        }
        if isSoundCursor {
            player.volume = 1.0
        } else {
            player.volume = 0.0
        }
        player.play()
    }
    
    func ajustaTInicialPosCursor() {
        if let tempoComp = self.startTimeBar.first {
            let den = tempoComp.denominator
            
            var durTick = 960
            
            if den == 8 {
                durTick = 480
            }
            if den == 16 {
                durTick = 240
            }
            
            var tini = 0
            
            for i in 0..<posCursor.count {
                posCursor[i].tIni = tini
                tini += durTick
            }
        }
    }
    
    func createMidi(onlyPlayedNotes: Bool = false) {
        ajustaTInicialPosCursor()
        
        if let source = dataSource {
            let generator = MIDIGenerator(name: "midi", metronome: source.metronome, numerator: (self.startTimeBar.first?.numerator)!, denominator: (self.startTimeBar.first?.denominator)!)
            
            generator.channel12Active = isSoundRight
            generator.channel34Active = isSoundLeft
            
            if(self.posCursor.count-1 > 0){
                for i in 0..<self.posCursor.count-1 {
                    for note in source.notes {
                        if note.tIniQuantized >= self.posCursor[i].tIniReal && note.tIniQuantized < self.posCursor[i + 1].tIniReal {
                            let desloc = note.tIniQuantized - self.posCursor[i].tIniReal
                            generator.addNote(tone: note.tone, initialTick: self.posCursor[i].tIni + desloc, duration:  note.durationQuantized, channel: note.channel)
                        }
                    }
                }
            }
            generator.generateMIDI()
        }
        
//        if let source = dataSource {
//            let generator = MIDIGenerator(name: "midi", metronome: source.metronome, numerator: (self.startTimeBar.first?.numerator)!, denominator: (self.startTimeBar.first?.denominator)!)
//
//            generator.channel12Active = isSoundRight
//            generator.channel34Active = isSoundLeft
//
//            if onlyPlayedNotes {
//                for note in source.notes {
//                    generator.addNote(tone: note.tone, initialTick: note.tIniQuantized, duration: note.durationQuantized, channel: note.channel)
//                }
//            } else {
//                for note in source.notes {
//                    generator.addNote(tone: note.tone, initialTick: note.tIniQuantized, duration: note.durationQuantized, channel: note.channel)
//                }
//            }
//
//            generator.generateMIDI()
//        }
    }
    
    func findBarCursor() {
        if viewCursor.compassoCursor.bar != 0 {
            self.cont = 0
            for page in self.pages {
                for bar in page.bars {
                    if bar.index == viewCursor.compassoCursor.bar {
                        self.lastInitialTick = bar.initialTime
                        return
                    }
                    for _ in bar.pentagrams[0].channels[0].times {
                        self.cont += 1
                    }
                }
            }
        }
    }
    
    func showCursor() {
        var contL = 1
        
        for page in self.pages {
            for bar in page.bars {
                for time in bar.pentagrams[0].channels[0].times {
                    if contL > self.cont {
                        self.viewCursor.backgroundColor = UIColor.clear
                        let x = time.position.origin.x + (1 * self.spacing)
                        let y = time.position.origin.y + (4 * self.spacing)
                        let width  = (3 * self.spacing)
                        let height = (self.getBarRelativeHeight() * self.spacing) - (6 * self.spacing)
                        self.viewCursor.frame = CGRect(x: x, y: y, width: width, height: height)
                        
                        return
                    }
                    contL += 1
                }
            }
        }
    }
    
    func positionCursor() {
        self.cont = 0
//        var isToMove = true
        for page in self.pages {
            for bar in page.bars {
                if self.lastInitialTick >= bar.initialTime && bar.endTime >= self.lastInitialTick {
                    self.lastInitialTick = bar.initialTime
                    for time in bar.pentagrams[0].channels[0].times {
                        self.cont += 1
                        if time.index == 1 {
                            self.viewCursor.backgroundColor = UIColor.clear
                            let x = time.position.origin.x + (4 * self.spacing)
                            let y = time.position.origin.y + (4 * self.spacing)
                            let width  = (3 * self.spacing)
                            let height = (self.getBarRelativeHeight() * self.spacing) - (6 * self.spacing)
                            self.viewCursor.frame = CGRect(x: x, y: y, width: width, height: height)
                            
                        }
                    }
                }
            }
        }
        
        self.cont = 0
        for page in self.pages {
            for bar in page.bars {
                if bar.initialTime < self.lastInitialTick {
                    for _ in bar.pentagrams[0].channels[0].times {
                        self.cont += 1
                    }
                } else {
                    self.lastInitialTick = bar.initialTime
                    return
                }
            }
        }
    }
    
    private var lastBar    = 1
    private var currentBar = 0
    
    func movePage() {
        
        //        DispatchQueue.main.async(execute: {
        
        var contL = 1
        var toStop = false
        for (i, page) in self.pages.enumerated() {
            if i >= self.currentPage {
                self.delegate?.changePage(self.currentPage)
                self.currentPage += 1
            }
            
            for (_, bar) in page.bars.enumerated() {
                self.currentBar = bar.index
                for (_, time) in bar.pentagrams[0].channels[0].times.enumerated() {
                    if contL > self.cont {
                        self.lastInitialTick = time.initialTime + (time.endTime - time.initialTime)
                        self.viewCursor.backgroundColor = UIColor.clear
                        let x = time.position.origin.x + (4 * self.spacing)
                        let y = time.position.origin.y + (4 * self.spacing)
                        let width  = (3 * self.spacing)
                        let height = (self.getBarRelativeHeight() * self.spacing) - (6 * self.spacing)
                        self.viewCursor.frame = CGRect(x: x, y: y, width: width, height: height)
                        self.cont += 1
                        
                        toStop = true
                        break
                    }
                    contL += 1
                    if toStop {
                        break
                    }
                }
                if toStop {
                    break
                }
            }
            if toStop {
                break
            }
        }
    }
    
    func generatePositionsCursor() {
        self.posCursor.removeAll()
        var ritIni = false
        var ritFim = false
        var chave1 = false
        var chave2 = false
        var segno  = false
        var capo   = false
        var toCoda = false
        var fine   = false
        var coda   = false
        var dsAlCoda = false
        var dsAlFine = false
        var dcAlCoda = false
        var dcAlFine = false
        
        var tempPos: [PosCursor] = []
        
        for page in self.pages {
            for bar in page.bars {
                for repetition in bar.repetitions {
                    if repetition.bar != -1 {
                        if repetition.type == .start {
                            ritIni = true
                        }
                        if repetition.type == .end {
                            ritFim = true
                        }
                        if repetition.type == .bracket1 {
                            chave1 = true
                        }
                        if repetition.type == .bracket2 {
                            chave2 = true
                        }
                        if repetition.type == .delSegno {
                            segno  = true
                        }
                        if repetition.type == .daCapo   {
                            capo   = true
                        }
                        if repetition.type == .toCoda   {
                            toCoda = true
                        }
                        if repetition.type == .coda     {
                            coda   = true
                        }
                        if repetition.type == .fine     {
                            fine   = true
                        }
                        if repetition.type == .dsAlCoda {
                            dsAlCoda = true
                        }
                        if repetition.type == .dsAlFine {
                            dsAlFine = true
                        }
                        if repetition.type == .dcAlCoda {
                            dcAlCoda = true
                        }
                        if repetition.type == .dcAlFine {
                            dcAlFine = true
                        }
                    }
                }
                
                for time in bar.pentagrams[0].channels[0].times {
                    let x = time.position.origin.x + (4 * self.spacing)
                    let y = time.position.origin.y + (4 * self.spacing)
                    let width  = (3 * self.spacing)
                    let height = (self.getBarRelativeHeight() * self.spacing) - (6 * self.spacing)
                    let positionC = CGRect(x: x, y: y, width: width, height: height)
                    
                    let posC = PosCursor(pagina: page.index, posicao: positionC, duracao: getTimeCursor(), compasso: bar.index, tempo: time.index, tIni: 0, tIniReal: time.initialTime)
                    
                    if ritIni == true && chave1 == false {
                        tempPos.append(posC)
                    }
                    
                    self.posCursor.append(posC)
                
                    if chave1 == true && chave2 == true {
                        self.posCursor.append(contentsOf: tempPos)
                        ritIni = false
                        ritFim = false
                        chave1 = false
                        chave2 = false
                        tempPos.removeAll()
                    }
                    
                    if ritFim == true && time.index == bar.pentagrams[0].channels[0].times.count {
                        self.posCursor.append(contentsOf: tempPos)
                        ritIni = false
                        ritFim = false
                        tempPos.removeAll()
                    }
                    
                    if segno == true && toCoda == false && coda == false {
                        tempPos.append(posC)
                    }
                    
                    if segno == true && coda == true {
                        self.posCursor.append(contentsOf: tempPos)
                        toCoda = false
                        coda   = false
                        tempPos.removeAll()
                    }
                    
                    if segno == true && dsAlCoda == true {
                        self.posCursor.append(contentsOf: tempPos)
                        segno    = false
                        dsAlCoda = false
                        tempPos.removeAll()
                    }
                }
                if bar.lastBar {
                    break
                }
            }
        }
        
        createMidi()
    }
    
    private func getTimeCursor() -> Double {
        return (60.0/Double((dataSource?.metronome)!)) * (4.0 / Double((startTimeBar.first?.denominator)!))
    }
    
    var currentPage1  = 0
    var currentIndex1 = 0
    
    @objc func moveCursor1() {
        self.viewCursor.frame = self.posCursor[currentIndex1].posicao
        
        if currentIndex1 == self.posCursor.count - 1 {
            self.stop(terminated: true)
            
            return
        }
        
        if currentPage1 != self.posCursor[currentIndex1].pagina - 1 {
            self.currentPage1 = self.posCursor[currentIndex1].pagina - 1
            self.delegate?.changePage(self.currentPage1)
        }
        
        if self.posCursor[currentIndex1].compasso != 1 && self.posCursor[currentIndex1].tempo == 1 {
            verifyType()
        }
        
        self.currentIndex1 += 1
        self.playMetronome()
    }
    
    func changeTStartCursor(_ tStart: Int) {
        for i in 0..<self.posCursor.count {
            if self.posCursor[i].tIni == tStart {
                currentIndex1 = i
                currentPage1 = self.posCursor[i].pagina
                self.viewCursor.frame = self.posCursor[i].posicao
                self.viewCursor.frame.origin.x = self.viewCursor.frame.origin.x - 10
                self.viewCursor.isHidden = false
                break
            }
        }
    }
    
    func getPosCursor() -> PosCursor {
        return self.posCursor[currentIndex1]
    }
    
    func verifyType() {
        DispatchQueue.global(qos: .background).async {
            if self.state == .composing && !self.isPlayCompose {
                if let source = self.dataSource {
                    source.extendNotes(numerator: (self.startTimeBar.first?.numerator)!, denominator: (self.startTimeBar.first?.denominator)!)
                    
                    for note in source.notes {
                        if note.cantDraw && !note.drawed  {
                            self.addNote(midi: note.tone, tIniQuantized: note.tIniQuantized, durationQuantized: note.durationQuantized, channel: note.channel)
                            note.drawed = true
                        }
                    }
                }
                
                if self.state == .composing && self.isMidi && !self.isPlayCompose {
                    DispatchQueue.main.async(execute: {
                        self.drawUnwrittenNotes()
                    })
                }
            }
            
            if self.isPerformance && !self.isPlayCompose {
                self.lastBar = self.currentBar - 1
                DispatchQueue.main.async(execute: {
                    self.paintWrongNotes()
                })
            }
        }
    }
    
    @objc func moveCursor() {

//        DispatchQueue.main.async(execute: {
        
            var contL = 1
            var toStop = false
            for (i, page) in self.pages.enumerated() {
                if i >= self.currentPage {
                    self.delegate?.changePage(self.currentPage)
                    self.currentPage += 1
                }
                
                for (iB, bar) in page.bars.enumerated() {
                    self.currentBar = bar.index
                    for (iT, time) in bar.pentagrams[0].channels[0].times.enumerated() {
                        if contL > self.cont {
                            self.lastInitialTick = time.initialTime + (time.endTime - time.initialTime)
                            self.viewCursor.backgroundColor = UIColor.clear
                            let x = time.position.origin.x + (4 * self.spacing)
                            let y = time.position.origin.y + (4 * self.spacing)
                            let width  = (3 * self.spacing)
                            let height = (self.getBarRelativeHeight() * self.spacing) - (6 * self.spacing)
                            self.viewCursor.frame = CGRect(x: x, y: y, width: width, height: height)
                            self.cont += 1

                            if i == (self.pages.count - 1) && iB == (self.pages[i].bars.count - 1) && iT == (bar.pentagrams[0].channels[0].times.count - 1) {
                                self.stop(terminated: true)
                            }
                            toStop = true
                            break
                        }
                        contL += 1
                        if toStop {
                            break
                        }
                    }
                    if toStop {
                        break
                    }
                }
                if toStop {
                    break
                }
            }
            
        
//        })
        

        DispatchQueue.global(qos: .background).async {
            if self.state == .composing && self.lastBar != (self.currentBar - 1) {
                self.lastBar = self.currentBar - 1
                if let source = self.dataSource {
                    source.extendNotes(numerator: (self.startTimeBar.first?.numerator)!, denominator: (self.startTimeBar.first?.denominator)!)
                    
                    for note in source.notes {
                        if note.cantDraw && !note.drawed  {//&& !note.lastGroup
                            self.addNote(midi: note.tone, tIniQuantized: note.tIniQuantized, durationQuantized: note.durationQuantized, channel: note.channel)
                            note.drawed = true
                        }
                    }
                }
                
                if self.state == .composing && self.lastBar != self.currentBar && self.isMidi && !self.isPlayCompose {
                    DispatchQueue.main.async(execute: {
                        self.drawUnwrittenNotes()
                    })
                }
            }
            
            if self.isPerformance && self.lastBar != (self.currentBar - 1) {
                self.lastBar = self.currentBar - 1
                DispatchQueue.main.async(execute: {
                    self.paintWrongNotes()
                })
            }
        }

        let testanto = mach_absolute_time()
        let res = testanto - self.timeTeste
//        self.timeTeste = testanto
//        print("\(res/1000000)")
        self.playMetronome()
    }

}

enum ScoreState : Int {
    case composing         = 1
    case resumeComposing   = 2
    case playing           = 3
    case resumePlaying     = 4
    case performance       = 5
    case resumePerformance = 6
    case editable          = 7
    case analysing         = 8
    case compare           = 9
}

enum EspecificFunction : Int {
    case general        = 1
    case canalyse13     = 2
    case canalyse12     = 3
    case canalyse34     = 4
    case canalyseAbove  = 5
    case canalyseCenter = 6
    case canalyseManual = 7
}

enum QuantizationType: Int {
    case semiquaver  = 16
    case quaver      = 8
    case quarter     = 4
    case half        = 2
    case whole       = 1
}

enum BaseNote: Int {
    case semiquaver  = 16
    case quaver      = 8
    case quarter     = 4
    case half        = 2
    case whole       = 1
}

enum SpacingType {
    case fixed
    case flexible
}

struct Quantization {
    var type = QuantizationType.quarter
    var ticks: Int {
        switch type {
        case .semiquaver:
            return 240
        case .quaver:
            return 480
        case .quarter:
            return 960
        case .half:
            return 1920
        case .whole:
            return 3840
        }
    }
}

struct NotePosition {
    var initialBar   = 0
    var initialTime  = 0
    var initialPlace = 0
    var endBar       = 0
    var endTime      = 0
    var endPlace     = 0
    var duration     = 0
}

struct LineAccident {
    var midisOfLine : [MidiCode] = []
    var hasSharp    : Bool       = false
    var hasFlat     : Bool       = false
    var hasNatural  : Bool       = false
}
