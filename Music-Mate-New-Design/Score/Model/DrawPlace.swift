//
//  DrawPlace.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawPlace {
    var index  = 0
    var channel = 1
    var stem    : DrawStem    = DrawStem()
    var bracket : DrawBracket = DrawBracket()
    var pause   : DrawPause   = DrawPause()
    var supLine : DrawSupplementaryLine = DrawSupplementaryLine()
    var infLine : DrawSupplementaryLine = DrawSupplementaryLine()
    
    var sharps   : [DrawSharp]   = []
    var flats    : [DrawFlat]    = []
    var naturals : [DrawNatural] = []
    
    var channelTone12      : MidiCode = MidiCode.c0
    var channelTone12Midle : MidiCode = MidiCode.c0
    var channelTone34      : MidiCode = MidiCode.c0
    var channelTone34Midle : MidiCode = MidiCode.c0
    
    var channelTone  : MidiCode = MidiCode.c0
    var channelTone2 : MidiCode = MidiCode.c0
    var initialTime = 0
    var endTime     = 0
    var barDuration = 0
    
    var has8  = false
    var has16 = false
    var joined = false
    var isCompose = false
    
    var notes   : [DrawNote]   = []
    
    var position = CGRect()
    
    var adapter: AdapterPlace!
    
    var lowerTone: DrawTone {
        var lowerTone = DrawTone()
        lowerTone.midi = MidiCode(rawValue: 200)!
        for note in self.notes {
            if note.usable {
                lowerTone = min(lowerTone, note.tone)
                lowerTone.clef = note.tone.clef
            }
        }
        
        return lowerTone
    }
    
    var greaterTone: DrawTone {
        var greaterTone = DrawTone()
        greaterTone.midi = MidiCode(rawValue: 0)!
        for note in self.notes {
            if note.usable {
                greaterTone = max(greaterTone, note.tone)
                greaterTone.clef = note.tone.clef
            }
        }
        
        return greaterTone
    }
    
    func getRelativeSize(spacing: SpacingType, quantization: Int) -> CGFloat {
        if spacing == .fixed {
            return 1.5
        }

        for note in notes {
            if note.usable {
                if quantization == 16 {
                    if !hasNotesSemioctavus() {
                        return 1.0
                    }
                    return 2.0
                }
                if quantization == 8 {
                    return 1.6
                }
                if quantization == 4 {
                    return 2.0
                }
            }
        }

        return 0.4
    }
    
    func hasNotesUsable() -> Bool {
        for note in notes {
            if note.usable {
                return true
            }
        }
        
        return false
    }
    
    func hasNotesOctavus() -> Bool {
        for note in notes {
            if note.usable && (note.duration.rawValue == 480 || note.duration.rawValue == 720) {
                has8 = true
                return true
            }
        }
        
        return false
    }
    
    func hasNotesSemioctavus() -> Bool {
        for note in notes {
            if note.usable && (note.duration.rawValue == 240 || note.duration.rawValue == 360) {
                has16 = true
                return true
            }
        }
        
        return false
    }
    
    func addPause(channel: Int, start: Int, end: Int) {
        let pause = DrawPause(channel: channel, start: start, end: end)
        pause.type = TypePause(rawValue: end - start)!
        pause.exists = true
        self.pause = pause
    }
    
    func addNote(midi: Int, channel: Int, duration: Int, bar: Int, time: Int, clef: Int, predecessor: Bool, successor: Bool, startNote: Bool, startTime: Bool, realInitialTime: Int, realEndTime: Int, key: Int, isCorrect: Bool) {
        let tone = DrawTone()
     
        tone.midi  = MidiCode(rawValue: midi)!
        tone.clef  = ClefType(rawValue: clef)!
        tone.tonalidade = KeyType(rawValue: key)!
        
        let note         = DrawNote()
        note.tone        = tone
        note.channel     = channel
        note.duration    = NoteDuration(rawValue: duration)!
        note.successor   = successor
        note.predecessor = predecessor
        note.startNote   = startNote
        note.startTime   = startTime
        note.bar         = bar
        note.itQuantized = initialTime
        note.realInitialTime = realInitialTime
        note.realEndTime     = realEndTime
        note.joined      = true
        note.usable      = true
        note.isCorrect   = isCorrect
        
        notes.append(note)
    }
    
    
    
    func addAccident(linesAccidents: inout [LineAccident],key: DrawKey) {
        flats.removeAll()
        sharps.removeAll()
        naturals.removeAll()
    
        
        
        for note in notes {
            if note.usable {
                if note.tone.hasSharp {
                    for (i, line) in linesAccidents.enumerated() {
                        for midi in line.midisOfLine {
                            
                            if note.tone.midi == midi && !line.hasSharp {
                                
                                let sharp = DrawSharp()
                                sharp.tone = note.tone
                                sharps.append(sharp)
                                linesAccidents[i].hasSharp = true
                            }
                        }
                    }
                } else {
                    for (i, line) in linesAccidents.enumerated() {
                        for midi in line.midisOfLine {
                            if note.tone.midi == midi && line.hasSharp {
                                let natural = DrawNatural()
                                natural.tone = note.tone
                                naturals.append(natural)
                                linesAccidents[i].hasSharp = false
                            }
                        }
                    }
                }
                
                if note.tone.hasFlat {
                    for (i, line) in linesAccidents.enumerated() {
                        for midi in line.midisOfLine {
                            if note.tone.midi == midi && !line.hasFlat {
                                let flat = DrawFlat()
                                flat.tone = note.tone
                                flats.append(flat)
                                linesAccidents[i].hasFlat = true
                            }
                        }
                    }
                } else {
                    for (i, line) in linesAccidents.enumerated() {
                        for midi in line.midisOfLine {
                            if note.tone.midi == midi && line.hasFlat {
                                let natural = DrawNatural()
                                natural.tone = note.tone
                                naturals.append(natural)
                                linesAccidents[i].hasFlat = false
                            }
                        }
                    }
                }
            }
        }
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        var hasUsableNote = false
        var needsStem     = false
        var needsBracket  = false
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        if state == .editable {
            if adapter == nil {
                adapter = AdapterPlace()
            }
            adapter.number      = index
            adapter.initialTime = initialTime
            adapter.channel     = self.channel
            adapter.adapt(parentView: parentView, frame: position)
        }
        
        var countNotes   = 0
        var sumMidis     = 0
        
        for note in notes {
            if note.usable && note.joined && !note.drawed  {
                sumMidis += note.tone.midi.rawValue
                countNotes += 1
                
                hasUsableNote = true
                if note.duration.rawValue <= 2880 {
                    needsStem = true
                }
                
                if note.duration.rawValue < 960 {
                    needsBracket = true
                }
            }
        }
        
        var turn = false
        if countNotes > 0 {
            let averageNotes = sumMidis / countNotes
            
            if averageNotes >= 70 && (channel == 1 || channel == 2) {
                turn = true
            }
            
            if averageNotes >= 50 && (channel == 3 || channel == 4) {
                turn = true
            }
        }
        
        if hasUsableNote  && needsStem && !joined {
            drawStem(parentView: parentView, parameters: parameters, state: state, turn: turn)
        }
        
        if hasUsableNote {
            drawSupplementaryLines(parentView: parentView, parameters: parameters, state: state)
        }
        
        if hasUsableNote && needsBracket && !joined {
            drawBracket(parentView: parentView, parameters: parameters, state: state, turn: turn)
        }
        
        if pause.exists {
            if pause.type.rawValue == barDuration {
                pause.type = .semibreve
            }
            let posPause = CGRect(x: position.origin.x, y: position.origin.y, width: 2 * space, height: 4.0 *  space)
            pause.position = posPause
            pause.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        for sharp in sharps {
            if !sharp.drawed {
                let rectSharp = CGRect(x: position.origin.x - (1.2 * space), y: position.origin.y - (sharp.tone.height * space), width: 2 * space, height: 5.0 * space)
                sharp.position = rectSharp
                sharp.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }
        
        for flat in flats {
            if !flat.drawed {
                let rectSharp = CGRect(x: position.origin.x - (1.2 * space), y: position.origin.y - (flat.tone.height * space), width: 2 * space, height: 5.0 * space)
                flat.position = rectSharp
                flat.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }
        
        for natural in naturals {
            if !natural.drawed {
                let rectSharp = CGRect(x: position.origin.x - (1.2 * space), y: position.origin.y - (natural.tone.height * space), width: 2 * space, height: 5.0 * space)
                natural.position = rectSharp
                natural.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }
        
        for note in notes {
            if note.usable && note.joined && !note.drawed {
                var rectNote = CGRect(x: position.origin.x, y: position.origin.y - (note.tone.height * space), width: 2 * space, height: 4.0 * space)
                switch note.duration.rawValue {
                case 120, 180, 240, 360, 480, 720, 960, 1440:
                    rectNote = CGRect(x: position.origin.x, y: position.origin.y - (note.tone.height * space) + (1.5 * space), width: 2 * space, height: 4.0 * space)
                default:
                    break
                }
                note.position = rectNote
                note.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }

    }
    
    private func drawSupplementaryLines(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        supLine.tone = greaterTone
        infLine.tone = lowerTone
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        if supLine.numberOfLineSup != 0 {
            let x = position.origin.x
            let y = position.origin.y - (CGFloat(supLine.numberOfLineSup) * space)
            let comprimento = space * 1.8
            let largura = (CGFloat(supLine.numberOfLineSup) * space)
            let posLinha = CGRect(x: x, y: y, width: comprimento, height: largura)
            
            supLine.position = posLinha
            supLine.numberLines = supLine.numberOfLineSup
            supLine.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        if infLine.numberOfLineInf != 0 {
            let x = position.origin.x
            let y = position.origin.y + (5.0*space)
            let comprimento = space * 1.8
            let largura = (CGFloat(infLine.numberOfLineInf) * space * 1.2)
            let posLinha = CGRect(x: x, y: y, width: comprimento, height: largura)
            
            infLine.position = posLinha
            infLine.numberLines = infLine.numberOfLineInf
            infLine.draw(parentView: parentView, parameters: parameters, state: state)
        }

    }
    
    func drawUnwrittenNotes(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        var hasUsableNote = false
        var needsStem     = false
        var needsBracket  = false
        
        var greaterDuration = 0
        
        var space = parameters.spacing
        if state == .editable {
            space = parameters.spacingEdit
        }
        
        for note in notes {
            greaterDuration = max(greaterDuration, note.duration.rawValue)
        }
        
        var countNotes   = 0
        var sumMidis     = 0
        
        for note in notes {
            if note.usable && note.joined && !note.drawed {
                sumMidis += note.tone.midi.rawValue
                countNotes += 1
                hasUsableNote = true
                if note.duration.rawValue <= 2880 {
                    needsStem = true
                }
                
                if note.duration.rawValue < 960 && greaterDuration < 960{
                    needsBracket = true
                }
            }
        }

        var turn = false
        if countNotes > 0 {
            let averageNotes = sumMidis / countNotes
            
            if averageNotes >= 70 && (channel == 1 || channel == 2) {
                turn = true
            }
            
            if averageNotes >= 50 && (channel == 3 || channel == 4) {
                turn = true
            }
        }
        
        if hasUsableNote && needsStem {
            drawStem(parentView: parentView, parameters: parameters, state: state, turn: turn)
        }
        
        if hasUsableNote {
            drawSupplementaryLines(parentView: parentView, parameters: parameters, state: state)
        }
        
        if hasUsableNote && needsBracket && (!joined) {// || (state == .composing)
            drawBracket(parentView: parentView, parameters: parameters, state: state, turn: turn)
        }
//        else {
//            eraseBracket()
//        }
        
        if pause.exists {
            if pause.type.rawValue == barDuration {
                pause.type = .semibreve
            }
            let posPause = CGRect(x: position.origin.x, y: position.origin.y, width: 2 * space, height: 4.0 *  space)
            pause.position = posPause
            pause.draw(parentView: parentView, parameters: parameters, state: state)
        } else {
            pause.erase(parentView: parentView, parameters: parameters, state: state)
        }
        
        for note in notes {
            if note.usable && note.joined && !note.drawed {
                var rectNote = CGRect(x: position.origin.x, y: position.origin.y - (note.tone.height * space), width: 2 * space, height: 4.0 * space)
                switch note.duration.rawValue {
                case 120, 180, 240, 360, 480, 720, 960, 1440:
                    rectNote = CGRect(x: position.origin.x, y: position.origin.y - (note.tone.height * space) + (1.5 * space), width: 2 * space, height: 4.0 * space)
                default:
                    break
                }
                note.isCompose = self.isCompose
                note.position  = rectNote
                note.draw(parentView: parentView, parameters: parameters, state: state)
            } else if note.drawed {
//                note.duration = NoteDuration(rawValue: greaterDuration)!
                note.draw(parentView: parentView, parameters: parameters, state: state)
            }
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for note in notes {
            if note.usable && note.joined && note.drawed && (note.itQuantized + note.duration.rawValue) <= currentTime {
                var cont = 0
                for played in notesPlayed {
                    if note.itQuantized == played.tIniQuantized && note.tone.midi.rawValue == played.tone {
                        if note.tone.clef == .SOL {
                            played.channel = 1
                        } else {
                            played.channel = 3
                        }
                        cont += 1
                    }
                }
                if cont == 0 {
                    if let adp = note.adapter {
                        if let viewNote = adp.view {
                            if viewNote.color != UIColor.red {
                                viewNote.color = UIColor.red
                            }
                        }
                    }
                } else {
                    if let adp = note.adapter {
                        if let viewNote = adp.view {
                            if viewNote.color != UIColor.black {
                                viewNote.color = UIColor.black
                            }
                        }
                    }
                }
            } else {
                if let adp = note.adapter {
                    if let viewNote = adp.view {
                        if viewNote.color != UIColor.black {
                            viewNote.color = UIColor.black
                        }
                    }
                }
            }
        }
    }
    
    private func drawBracket(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState, turn: Bool) {
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        var x = position.origin.x + (0.93 * space)
        var yIni = position.origin.y - (greaterTone.height * space)
//        if channel == 2 || channel == 4 {
        if turn {
            yIni = position.origin.y - (lowerTone.height * space)
            x -= (1.08 * space)
            yIni += 1.0 * space
        }
        
        let posLine = CGRect(x: x, y: yIni, width: 2 * space, height: 6.5 * space)
        
        bracket.turn     = turn
        bracket.position = posLine
        bracket.channel  = self.channel
        bracket.duration = notes[0].duration.rawValue
        bracket.draw(parentView: parentView, parameters: parameters, state: state)
    }
    
    private func eraseBracket() {
        bracket.erase()
    }
    
    private func drawStem(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState, turn: Bool) {
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        let heightGreaterTone = space * greaterTone.height
        let heightLowerTone = space * lowerTone.height
        
        var x      = position.origin.x
        var yIni   = (position.origin.y + position.size.height) - heightGreaterTone - (4.1 * space)
        let yFim   = (position.origin.y + position.size.height) - heightLowerTone
        let width  = space * 1.6
        let height = yFim - yIni - (0.7 * space)
        
//        if channel == 2 || channel == 4 {
        if turn {
            x -= (1.08 * space)
            yIni += 3.6 * space
        }
        
        if yIni.isNaN {
            print("Entrou")
        }
        let posStem = CGRect(x: x, y: yIni, width: width, height: height)
        
        stem.position = posStem
        stem.draw(parentView: parentView, parameters: parameters, state: state)
        
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for note in notes {
            if note.adapter != nil {
                if note.adapter.view != nil {
                    note.adapter.view.color = color
                }
            }
        }
    }
    
    func sendNotesToFront(parentView: ViewPageScore) {
        for note in notes {
            if note.adapter != nil {
                if note.adapter.view != nil {
                    parentView.bringSubview(toFront: note.adapter.view)
                }
            }
        }
    }
    
    deinit {
        adapter = nil
        sharps.removeAll()
        flats.removeAll()
        naturals.removeAll()
        notes.removeAll()
        print("Entrou deinit place")
    }
}
