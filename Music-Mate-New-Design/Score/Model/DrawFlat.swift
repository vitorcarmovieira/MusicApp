//
//  DrawFlat.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawFlat {
    var tone = DrawTone()
    var local = 1
    var position = CGRect()
    var drawed = false
    var adapter: AdapterFlat!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        drawed = true
        
        if adapter == nil {
            adapter = AdapterFlat()
        }
        adapter.parameters  = parameters
        adapter.state       = state
        
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
