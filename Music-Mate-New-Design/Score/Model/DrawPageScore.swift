//
//  DrawScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/2/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawPageScore {
    var index = 0
    var bars: [DrawBar] = []
    var numberOfPages = 0
    var metronome = 60
    var header = DrawHeader()
    var footer = DrawFooter()
    var ligatures: [DrawLigature] = []
    private var placesPositions: [[CGFloat]] = []
    var channelAreas: [DrawChannelArea] = []
    var state  = ScoreState.playing
    var isCompose = false
    
    var parameters = DrawParameter()
    private var adapter: AdapterPageScore!
    
    var position = CGRect()
    
    deinit {
        bars.removeAll()
        ligatures.removeAll()
        channelAreas.removeAll()
        adapter = nil
        placesPositions.removeAll()
        adapter = nil
    }
    
    func adjustsPosition() {
        let maxXs = getMaxXInLineY()
        let barsCount = getContBarsInLineY()
        
        let widthScreen = parameters.width - parameters.rightBorder
        
        var currentX: CGFloat = 0.0
        var currentY: CGFloat = 0.0
        
        var line = 0
        
        for bar in bars {
            if currentY != bar.position.origin.y {
                currentX = bar.position.origin.x
                currentY = bar.position.origin.y
                
                line += 1
            }
            
            bar.line = line
            
            let widthRemaining = widthScreen - maxXs[bar.position.origin.y]!
            let newWidth  = bar.position.width + (widthRemaining / CGFloat(barsCount[bar.position.origin.y]!))
            let newHeight = bar.position.height
            
            bar.position = CGRect(x: currentX, y: currentY, width: newWidth, height: newHeight)
            
            currentX += newWidth
        }
    }

    private func getMaxXInLineY() -> Dictionary<CGFloat, CGFloat> {

        var maxX = Dictionary<CGFloat, CGFloat>()
        
        for bar in bars {
            maxX[bar.position.origin.y] = bar.position.origin.x + bar.position.width
        }
        
        return maxX
    }
//
//    private func getMinXInLineY() -> Dictionary<CGFloat, CGFloat> {
//
//        var minX = Dictionary<CGFloat, CGFloat>()
//
//        for bar in bars {
//            minX[bar.position.origin.y] = 10000.0
//        }
//
//        for bar in bars {
//            minX[bar.position.origin.y] = min(minX[bar.position.origin.y]!, bar.position.origin.x)
//        }
//
//        return minX
//    }
//
    private func getContBarsInLineY() -> Dictionary<CGFloat, Int> {
        
        var barsCount = Dictionary<CGFloat, Int>()
        
        for bar in bars {
            barsCount[bar.position.origin.y] = 0
        }
        
        for bar in bars {
            barsCount[bar.position.origin.y] = barsCount[bar.position.origin.y]! + 1
        }

        return barsCount
    }
    
    func generateLigatures() {
        ligatures.removeAll()
        for bar in bars {
        for pentagram in bar.pentagrams {
        for channel in pentagram.channels {
        for time in channel.times {
        for place in time.places {
            for note in place.notes {
                    if note.usable && note.successor && !note.ligatureActive {
                        for barN in bars {
                        for pentagramN in barN.pentagrams {
                        for channelN in pentagramN.channels {
                        for timeN in channelN.times {
                        for placeN in timeN.places {
                            for noteN in placeN.notes {
                                if note.itQuantized != noteN.itQuantized && note.tone.midi == noteN.tone.midi && noteN.usable && !note.ligatureActive && note.realInitialTime == noteN.realInitialTime {
                                    //
                                    let ligature = DrawLigature()
                                    ligature.placeFrom = place
                                    ligature.placeTo   = placeN
                                    ligature.toneFrom  = note.tone
                                    ligature.toneTo    = noteN.tone
                                                                
                                    note.ligatureActive = true
                                    ligatures.append(ligature)
                                }
                            }
                        }
                        }
                        }
                        }
                        }
                    }
                }
            }
        }
        }
        }
        }
    }
    
    func draw(parentView: UIView) {
        if adapter == nil {
            adapter = AdapterPageScore()
        }
        
        let x = (parentView.frame.height * 0.08) * 0.3
        let y = parentView.frame.height * 0.05

        let width  = parentView.frame.width - (2.0 * x)
        let height = parentView.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        adapter.adapt(parentView: parentView, frame: rect)
        
        if index == 1 {
            header.metronome.value.value = metronome
            header.draw(parentView: adapter.view, parameters: parameters, state: state)
        }
        
        for bar in bars {
            bar.draw(parentView: adapter.view, parameters: parameters, state: state)
        }
        
        if state == .editable {
            fillChannelAreas(parameters: parameters, width: parameters.width)
            for area in channelAreas {
                area.draw(parentView: adapter.view, parameters: parameters, state: state)
                area.adapter.hiddenViews()
            }
        }
        
        footer.pageNumber.number = index
        footer.pageNumber.total  = numberOfPages
        footer.draw(parentView: adapter.view, parameters: parameters, state: state)
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        for ligature in ligatures {
            let x = ligature.placeFrom.position.origin.x + (1.0 * space)
            let y = ligature.placeFrom.position.origin.y - (ligature.toneTo.height * space) + (4.0 * space)
            let width  = ligature.placeTo.position.origin.x - ligature.placeFrom.position.origin.x
            let height = 1.5 * space
            let posi = CGRect(x: x, y: y, width: width, height: height)
            if ligature.placeFrom.position.origin.x < ligature.placeTo.position.origin.x {
                ligature.position = posi
                ligature.draw(parentView: adapter.view, parameters: parameters, state: state)
            }
        }
        
        for bar in bars {
            bar.sendNotesToFront(parentView: adapter.view)
        }
    }
 //verificar 
    func drawUnwrittenNotes() {
        for bar in bars {
            bar.isCompose = self.isCompose
            if let _ = adapter {
                if let _ = adapter.view {
                    bar.drawUnwrittenNotes(parentView: adapter.view, parameters: parameters, state: state)
                }
            }
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for bar in bars {
            bar.paintWrongNotes(currentTime: currentTime, notesPlayed: notesPlayed)
        }
        
    }
    
    func fillChannelAreas(parameters: DrawParameter, width: CGFloat) {
        var heights: [CGFloat] = []
        
        for bar in bars {
            heights.append(bar.position.origin.y)
        }
        
        heights = heights.unique()
        
        fillPlacesPositions()
        
        fillAreasOneTree(heights: &heights, parameters: parameters, width: width)
        fillAreasOneTwo(heights: &heights, parameters: parameters, width: width)
        fillAreasTreeFour(heights: &heights, parameters: parameters, width: width)
        fillAreasDeleteAbove(heights: &heights, parameters: parameters, width: width)
        fillAreasDeleteCenter(heights: &heights, parameters: parameters, width: width)
    }
    
    private func fillPlacesPositions() {
        placesPositions.removeAll()
        var line = 0
        for bar in bars {
            if bar.line != line {
                placesPositions.append([])
                line = bar.line
            }
        }
        
        if line != -1 {
            for bar in bars {
                for pentagram in bar.pentagrams {
                    for channel in pentagram.channels {
                        for time in channel.times {
                            for place in time.places {
                                placesPositions[bar.line - 1].append(place.position.origin.x)
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func fillAreasOneTree(heights: inout[CGFloat], parameters: DrawParameter, width: CGFloat) {
        var i = 1
        for height in heights {
            let area = DrawChannelArea()
            area.type = .oneTree
            area.line = i
            area.placesPositions  = placesPositions[i - 1]
            
            for bar in bars {
                if bar.line == i {
                    for pentagram in bar.pentagrams {
                        for channel in pentagram.channels {
                            for time in channel.times {
                                for place in time.places {
                                    area.places.append(place)
                                }
                            }
                        }
                    }
                }
            }
            
            let x = parameters.leftBorder
            let y = height + (6 * parameters.spacingEdit)
            let width = width - parameters.rightBorder - parameters.leftBorder
            let heig: CGFloat = (15 * parameters.spacingEdit)
            
            area.position = CGRect(x: x, y: y, width: width, height: heig)
            
            channelAreas.append(area)
            i += 1
        }
    }
    
    private func fillAreasOneTwo(heights: inout[CGFloat], parameters: DrawParameter, width: CGFloat) {
        var i = 1
        for height in heights {
            let area = DrawChannelArea()
            area.type = .oneTwo
            area.line = i
            area.placesPositions  = placesPositions[i - 1]
            
            for bar in bars {
                if bar.line == i {
                    for pentagram in bar.pentagrams {
                        for channel in pentagram.channels {
                            for time in channel.times {
                                for place in time.places {
                                    area.places.append(place)
                                }
                            }
                        }
                    }
                }
            }
            
            let x = parameters.leftBorder
            let y = height
            let width = width - parameters.rightBorder - parameters.leftBorder
            let heig: CGFloat = (19 * parameters.spacingEdit)
            
            area.position = CGRect(x: x, y: y, width: width, height: heig)
            
            channelAreas.append(area)
            i += 1
        }
    }
    
    private func fillAreasTreeFour(heights: inout[CGFloat], parameters: DrawParameter, width: CGFloat) {
        var i = 1
        for height in heights {
            let area = DrawChannelArea()
            area.type = .treeFour
            area.line = i
            area.placesPositions  = placesPositions[i - 1]
            
            for bar in bars {
                if bar.line == i {
                    for pentagram in bar.pentagrams {
                        for channel in pentagram.channels {
                            for time in channel.times {
                                for place in time.places {
                                    area.places.append(place)
                                }
                            }
                        }
                    }
                }
            }
            
            let x = parameters.leftBorder
            let y = height + (8 * parameters.spacingEdit)
            let width = width - parameters.rightBorder - parameters.leftBorder
            let heig: CGFloat = (21 * parameters.spacingEdit)
            
            area.position = CGRect(x: x, y: y, width: width, height: heig)
            
            channelAreas.append(area)
            i += 1
        }
    }
    
    private func fillAreasDeleteAbove(heights: inout[CGFloat], parameters: DrawParameter, width: CGFloat) {
        var i = 1
        for height in heights {
            let area = DrawChannelArea()
            area.type = .hAcima
            area.line = i
            area.placesPositions  = placesPositions[i - 1]
            
            for bar in bars {
                if bar.line == i {
                    for pentagram in bar.pentagrams {
                        for channel in pentagram.channels {
                            for time in channel.times {
                                for place in time.places {
                                    area.places.append(place)
                                }
                            }
                        }
                    }
                }
            }
            
            let x = parameters.leftBorder
            let y = height
            let width = width - parameters.rightBorder - parameters.leftBorder
            let heig: CGFloat = (19 * parameters.spacingEdit)
            
            area.position = CGRect(x: x, y: y, width: width, height: heig)
            
            channelAreas.append(area)
            i += 1
        }
    }
    
    private func fillAreasDeleteCenter(heights: inout[CGFloat], parameters: DrawParameter, width: CGFloat) {
        var i = 1
        for height in heights {
            let area = DrawChannelArea()
            area.type = .hMeio
            area.line = i
            area.placesPositions  = placesPositions[i - 1]
            
            for bar in bars {
                if bar.line == i {
                    for pentagram in bar.pentagrams {
                        for channel in pentagram.channels {
                            for time in channel.times {
                                for place in time.places {
                                    area.places.append(place)
                                }
                            }
                        }
                    }
                }
            }
            
            let x = parameters.leftBorder
            let y = height + (6 * parameters.spacingEdit)
            let width = width - parameters.rightBorder - parameters.leftBorder
            let heig: CGFloat = (15 * parameters.spacingEdit)
            
            area.position = CGRect(x: x, y: y, width: width, height: heig)
            
            channelAreas.append(area)
            i += 1
        }
    }
    
    func showPlaces() {
        for bar in bars {
            for pentagram in bar.pentagrams {
                for channel in pentagram.channels {
                    for time in channel.times {
                        for place in time.places {
                            if place.adapter != nil {
                                if place.adapter.view != nil && place.adapter.view.isHidden {
                                    place.adapter.view.isHidden = false
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func hiddenPlaces() {
        for bar in bars {
            for pentagram in bar.pentagrams {
                for channel in pentagram.channels {
                    for time in channel.times {
                        for place in time.places {
                            if place.adapter != nil {
                                if place.adapter.view != nil && !place.adapter.view.isHidden {
                                    place.adapter.view.isHidden = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}
