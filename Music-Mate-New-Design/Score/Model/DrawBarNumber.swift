//
//  DrawBarNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawBarNumber {
    var position = CGRect()
    var bar = 0
    
    private var adapter: AdapterBarNumber!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterBarNumber()
        }
        adapter.parameters = parameters
        adapter.state      = state
        adapter.bar        = bar
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
