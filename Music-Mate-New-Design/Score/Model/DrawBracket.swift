//
//  DrawBracket.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawBracket {
    var position = CGRect()
    var duration = 960
    var channel  = 1
    var turn     = false
    
    private var adapter: AdapterBracket!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterBracket()
        }
        adapter.channel  = channel
        adapter.duration = duration
        adapter.parameters = parameters
        adapter.state = state
        adapter.turn  = turn
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    func erase() {
        if adapter != nil {
            adapter.view.removeFromSuperview()
        }
    }
    
    deinit {
        adapter = nil
    }
}
