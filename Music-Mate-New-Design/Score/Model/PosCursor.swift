//
//  PosCursor.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 6/7/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

struct PosCursor {
    var pagina   : Int
    var posicao  : CGRect
    var duracao  : Double
    var compasso : Int
    var tempo    : Int
    var tIni     : Int
    var tIniReal : Int
}
