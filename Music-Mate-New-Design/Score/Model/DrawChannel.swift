//
//  DrawChannel.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawChannel {
    var index      : Int = 1
    var bar = 0
    
    var initialTime = 0
    var endTime     = 0
    
    var numerator   = 4
    var denominator = 4

    var isCompose = false
    var unions             : [DrawUnion]    = []
    var times              : [DrawTime]     = []
//    var hasChannelNotes    : [Bool]         = []
    private var timesUnion : [TimeUnion]    = []
    
    private var numberOfTimes = 0
    private var placesPerTime = 0
    
    var position = CGRect()
    
    var clef : Int {
        switch index {
        case 1, 2:
            return 1
        case 3, 4:
            return 2
        default:
            return 0
        }
    }
    
    deinit {
        unions.removeAll()
        times.removeAll()
        timesUnion.removeAll()
        unionsPerTime.removeAll()
    }
    
    init(index: Int, numberOfTimes: Int, placesPerTime: Int, initialTime: Int, endTime: Int) {
        self.numberOfTimes = numberOfTimes
        self.placesPerTime = placesPerTime
        self.initialTime = initialTime
        self.endTime     = endTime
        self.index       = index
        addTimes()
    }
    
    private func addTimes() {
        let durationTime = (endTime - initialTime) / numberOfTimes
        var currentTime  = initialTime
        for i in 0..<numberOfTimes {
            let time = DrawTime(placesPerTime: placesPerTime, initialTime: currentTime, endTime: currentTime + durationTime, barDuration: (endTime - initialTime), channel: self.index)
            time.index = i + 1
            times.append(time)
            
            currentTime += durationTime
        }
    }
    
    func addPause(channel: Int, start: Int, end: Int) {
        for i in 0..<times.count {
            if times[i].initialTime <= start && start + 1 <= times[i].endTime {
                times[i].addPause(channel: channel, start: start, end: end)
                break
            }
        }
    }
    
    func addNote(midi: Int, channel: Int, initialTime: Int, endTime: Int, bar: Int, predecessor: Bool, successor: Bool, startNote: Bool, realInitialTime: Int, realEndTime: Int, key: Int, isCorrect: Bool) {
        
        for i in 0..<times.count {
            if times[i].initialTime <= initialTime && initialTime + 1 <= times[i].endTime { 
                times[i].addNote(midi: midi, channel: channel, initialTime: initialTime, endTime: endTime, bar: bar, clef: clef, predecessor: predecessor, successor: successor, startNote: startNote, realInitialTime: realInitialTime, realEndTime: realEndTime, key: key, isCorrect: isCorrect)
                break
            }
        }
    }
    
    func addAccident(linesAccidents: inout [LineAccident], key: DrawKey) {
        for time in times {
            time.addAccident(linesAccidents: &linesAccidents, key: key)
        }
    }
    
    func generateUnions() {
        unions.removeAll()
        
        initializeUnions()
        
        if numerator == 4 && denominator == 4 {
            adjustsRhythmProfile4X4()
        }
        
        if denominator == 8 {
            adjustsRhythmProfileWithOctavus()
        }
        
        createUnions()
    }
    
    private func createUnions() {
        for unionPerTime in unionsPerTime {
            let union = DrawUnion()
            union.channel = self.index
            union.places = unionPerTime.places
            
            var allOctavus     = true
            var allSemiOctavus = true
            
            var lowerTone = DrawTone()
            lowerTone.midi = MidiCode(rawValue: 200)!
            var greaterTone = DrawTone()
            greaterTone.midi = MidiCode(rawValue: 0)!
            
            for place in union.places {
                if place.hasNotesOctavus() {
                    allSemiOctavus = false
                }
                if place.hasNotesSemioctavus() {
                    allOctavus = false
                }
                for note in place.notes {
                    if note.usable {
                        lowerTone = min(lowerTone, note.tone)
                        lowerTone.clef = note.tone.clef
                        greaterTone = max(greaterTone, note.tone)
                        greaterTone.clef = note.tone.clef
                    }
                }
            }
            union.greaterTone = greaterTone
            union.lowerTone = lowerTone
            union.allOctavus     = allOctavus
            union.allSemioctavus = allSemiOctavus
            
            if union.places.count > 1 {
                union.generateSemiunions()
                addUnion(union: union)
            }
        }
    }
    
    private func adjustsRhythmProfileWithOctavus() {
        var i = 0
        var deleted = false
        
        while i<(unionsPerTime.count-1) {
            let isTime12 = (unionsPerTime[i].time == 1 && unionsPerTime[i + 1].time == 2)
            let isTime13 = (unionsPerTime[i].time == 1 && unionsPerTime[i + 1].time == 3)
            
            let isTime45 = (unionsPerTime[i].time == 4 && unionsPerTime[i + 1].time == 5)
            let isTime46 = (unionsPerTime[i].time == 4 && unionsPerTime[i + 1].time == 6)
            
            let isTime78 = (unionsPerTime[i].time == 7 && unionsPerTime[i + 1].time == 8)
            let isTime79 = (unionsPerTime[i].time == 7 && unionsPerTime[i + 1].time == 9)
            
            let isTime1011 = (unionsPerTime[i].time == 10 && unionsPerTime[i + 1].time == 11)
            let isTime1012 = (unionsPerTime[i].time == 10 && unionsPerTime[i + 1].time == 12)
            
            if isTime12 || isTime13 || isTime45 || isTime46 || isTime78 || isTime79 || isTime1011 || isTime1012 {
                for place in unionsPerTime[i + 1].places {
                    unionsPerTime[i].places.append(place)
                }
                unionsPerTime.remove(at: i + 1)
                deleted = true
            }
            
            if !deleted {
                i += 1
            } else {
                deleted = false
            }
        }
    }
    
    private func adjustsRhythmProfile4X4() {
        var i = 0
        var deleted = false
        
        while i<(unionsPerTime.count-1) {
            let isTime12 = (unionsPerTime[i].time == 1 && unionsPerTime[i + 1].time == 2)
            let isTime34 = (unionsPerTime[i].time == 3 && unionsPerTime[i + 1].time == 4)
            
            if isTime12 || isTime34 {
                if unionsPerTime[i].every8 && unionsPerTime[i + 1].every8 {
                    for place in unionsPerTime[i + 1].places {
                        unionsPerTime[i].places.append(place)
                    }
                    unionsPerTime.remove(at: i + 1)
                    deleted = true
                }
            }
            
            if !deleted {
                i += 1
            } else {
                deleted = false
            }
        }
    }
    
    private var unionsPerTime: [UnionPerTime] = []
    
    private func initializeUnions() {
        var currentsUnions: [UnionPerTime] = []
        
        for time in times {
            var unionTime = UnionPerTime()
            unionTime.time = time.index
            currentsUnions.append(unionTime)
            
            for place in time.places {
                if place.hasNotesUsable() {
                    let max = currentsUnions.count - 1
                    if place.hasNotesOctavus() {
                        currentsUnions[max].every16 = false
                        currentsUnions[max].places.append(place)
                    }
                    if place.hasNotesSemioctavus() {
                        currentsUnions[max].every8 = false
                        currentsUnions[max].places.append(place)
                    }
                } else if place.notes.count == 0 && (place.index == 1 || place.index == 3) {
                    let max = currentsUnions.count - 1
                    currentsUnions[max].every8 = false
                    currentsUnions[max].every16 = false
                    
                    var unionTime = UnionPerTime()
                    unionTime.time = time.index
                    
                    currentsUnions.append(unionTime)
                }
            }
        }
        
        unionsPerTime.removeAll()
        
        for current in currentsUnions {
            let isUnion4 = (current.places.count > 1 && denominator == 4)
            let isUnion8 = (current.places.count > 0 && denominator == 8)
            if isUnion4 || isUnion8  {
                unionsPerTime.append(current)
            }
        }
    }
    
//    func generateUnion() {
//        unions.removeAll()
//
//        getTimesUnion()
//
//        if (numerator == 4 && denominator == 4) || (numerator == 2 && denominator == 4) {
//            for k in 0..<(numerator / 2) {
//                let c1 = timesUnion[(k * 2)].places.count
//                let c2 = timesUnion[(k * 2) + 1].places.count
//
//                if timesUnion[(k * 2)].every8 && timesUnion[(k * 2) + 1].every8 && c1 == c2 {
//                    let unionTime12 = DrawUnion()
//                    for i in 0..<timesUnion[(k * 2)].places.count {
//                        unionTime12.places.append(timesUnion[(k * 2)].places[i])
//                        unionTime12.places.append(timesUnion[(k * 2) + 1].places[i])
//                    }
//
//                    addUnion(union: unionTime12)
//                } else {
//                    let unionTime1 = DrawUnion()
//                    let unionTime2 = DrawUnion()
//
//                    for i in 0..<timesUnion[(k * 2)].places.count {
//                        unionTime1.places.append(timesUnion[(k * 2)].places[i])
//                    }
//                    for i in 0..<timesUnion[(k * 2) + 1].places.count {
//                        unionTime2.places.append(timesUnion[(k * 2) + 1].places[i])
//                    }
//
//                    addUnion(union: unionTime1)
//                    addUnion(union: unionTime2)
//                }
//            }
//        }
//
//        if (numerator == 3 && denominator == 4) || (numerator == 2 && denominator == 2) || (numerator == 3 && denominator == 2) || (numerator == 5 && denominator == 4) || (numerator == 6 && denominator == 4) || (numerator == 3 && denominator == 8) || (numerator == 6 && denominator == 8) || (numerator == 9 && denominator == 8) || (numerator == 12 && denominator == 8) {
//            for i in 0..<numerator {
//                let unionTime = DrawUnion()
//
//                for j in 0..<timesUnion[i].places.count {
//                    unionTime.places.append(timesUnion[i].places[j])
//                }
//
//                addUnion(union: unionTime)
//            }
//        }
//    }
//
//    private func getTimesUnion() {
//        timesUnion.removeAll()
//
//        var placesUnion: [PlaceOfUnion] = []
//
//        for (i, time) in times.enumerated() {
//            for (j, place) in time.places.enumerated() {
//                if place.has8 && time.index <= numerator {
//                    let placeUnion = PlaceOfUnion(time: time.index, place: times[i].places[j])
//                    placesUnion.append(placeUnion)
//                }
//            }
//        }
//
//        for i in 0..<numerator {
//            let timeUnion = TimeUnion(every8: false, every16: false, every32: false, time: i + 1, places: [])
//            timesUnion.append(timeUnion)
//        }
//
//        for i in 0..<placesUnion.count {
//            timesUnion[placesUnion[i].time - 1].places.append(placesUnion[i].place)
//        }
//
//        for i in 0..<timesUnion.count {
//            var cont8 = 0
//            var cont16 = 0
//
//            for j in 0..<timesUnion[i].places.count {
//                if timesUnion[i].places[j].has8 {
//                    cont8 += 1
//                }
//                if timesUnion[i].places[j].has16 {
//                    cont16 += 1
//                }
//            }
//
//            if cont8 == timesUnion[i].places.count && cont8 > 0 {
//                timesUnion[i].every8 = true
//            }
//
//            if cont16 == timesUnion[i].places.count && cont16 > 0 {
//                timesUnion[i].every16 = true
//            }
//        }
//    }
    
    private func addUnion(union: DrawUnion) {
        if union.places.count > 1 {
            var greaterTone = 0
            var lowerTone = 999
            var typeUnion = TypeUnion.noteInitiated

            for place in union.places {
                place.joined = true
                let isInc  = (typeUnion == TypeUnion.increasing)
                let isDec  = (typeUnion == TypeUnion.decreasing)
                let isIni  = (typeUnion == TypeUnion.initiated)
                let isNIni = (typeUnion == TypeUnion.noteInitiated)
                
                if isNIni {
                    typeUnion = .initiated
                }
                
                if isIni {
                    if place.lowerTone.midi.rawValue < lowerTone {
                        typeUnion = .decreasing
                    } else if place.greaterTone.midi.rawValue > greaterTone {
                        typeUnion = .increasing
                    } else {
                        typeUnion = .constant
                    }
                }
                
                if isInc {
                    if place.greaterTone.midi.rawValue <= greaterTone || place.greaterTone.midi.rawValue == greaterTone + 1 || place.greaterTone.midi.rawValue > greaterTone + 4 {
                        typeUnion = .constant
                    }
                }
                
                if isDec {
                    if place.lowerTone.midi.rawValue >= lowerTone || place.lowerTone.midi.rawValue == lowerTone - 1 || place.lowerTone.midi.rawValue < lowerTone - 4 {
                        typeUnion = .constant
                    }
                }
                
                greaterTone = max(greaterTone, place.greaterTone.midi.rawValue)
                lowerTone   = min(lowerTone, place.lowerTone.midi.rawValue)
            }

            union.type = .constant

            unions.append(union)
        }
    }
    
    private func isToTurn(union: DrawUnion) -> Bool {
        var turn = false
        
        let averageNotes = union.averageMidis()
        
        if averageNotes >= 70 && (union.channel == 1 || union.channel == 2) {
            turn = true
        }
        
        if averageNotes >= 50 && (union.channel == 3 || union.channel == 4) {
            turn = true
        }
        return turn
    }
    
    private func getUnionTone(midi: Int) -> DrawTone {
        var typeClef = ClefType.SOL
        if index == 3 || index == 4 {
            typeClef = ClefType.FA
        }
        
        let tone   = DrawTone()
        tone.midi  = MidiCode(rawValue: midi)!
        tone.clef = typeClef
        
        return tone
    }
    
    func joinNotes() {
//        for i in 0..<times.count {
//            times[i].joinNotes()
//        }
//
//        for i in 0..<times.count {
//            for j in 0..<times[i].places.count {
//                for k in 0..<times[i].places[j].notes.count {
//
//                    if times[i].places[j].notes[k].usable
//                        && times[i].places[j].notes[k].completeTime
//                        && times[i].places[j].notes[k].successor
//                        && !times[i].places[j].notes[k].joined {
//
//                        let midi = times[i].places[j].notes[k].tone.midi
//                        var duration = times[i].places[j].notes[k].duration.rawValue
//                        var successor = times[i].places[j].notes[k].successor
//
//                        for l in 0..<times.count {
//                            if l > i {
//                                for m in 0..<times[l].places.count {
//                                    if successor {
//                                        for n in 0..<times[l].places[m].notes.count {
//                                            if times[l].places[m].notes[n].usable {
//                                                if times[l].places[m].notes[n].tone.midi == midi
//                                                    && (times[l].places[m].notes[n].completeTime
//                                                        || (times[i].places[j].notes[k].startNote && times[i].places[j].notes[k].startTime) )
//                                                    && times[l].places[m].notes[n].usable
//                                                    && times[l].places[m].notes[n].predecessor
//                                                    && times[i].places[j].notes[k].realInitialTime == times[l].places[m].notes[n].realInitialTime
//                                                    && times[i].places[j].notes[k].realEndTime == times[l].places[m].notes[n].realEndTime {
//                                                    duration += times[l].places[m].notes[n].duration.rawValue
//
//                                                    times[l].places[m].notes[n].usable = false
//                                                    successor = times[l].places[m].notes[n].successor
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//
//                        if let durationNote = NoteDuration(rawValue: duration) {
//                            times[i].places[j].notes[k].duration = durationNote
//                        }
//
//                        times[i].places[j].notes[k].successor = successor
//                    }
//
//                    times[i].places[j].notes[k].joined = true
//                }
//            }
//        }
        
        
        
        
//        
//        
//        for (i, time) in times.enumerated() {
//            for (j, place) in time.places.enumerated() {
//                for (k, note) in place.notes.enumerated() {
//                    
//                    if note.usable && note.completeTime && note.successor && !note.joined {
//                        let midi = note.tone.midi
//                        var duration = note.duration.rawValue
//                        var successor = note.successor
//                        
//                        for (l, timeBar) in times.enumerated() where l > i {
//                            for (m, placeBar) in timeBar.places.enumerated() {
//                                if successor {
//                                    for (n, noteBar) in placeBar.notes.enumerated() where noteBar.usable {
//                                        if noteBar.tone.midi == midi && (noteBar.completeTime || (note.startNote && note.startTime) ) && noteBar.usable && noteBar.predecessor && note.realInitialTime == noteBar.realInitialTime && note.realEndTime == noteBar.realEndTime {
//                                            duration += noteBar.duration.rawValue
//                                            
//                                            times[l].places[m].notes[n].usable = false
//                                            successor = times[l].places[m].notes[n].successor
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        
//                        if let durationNote = NoteDuration(rawValue: duration) {
//                            times[i].places[j].notes[k].duration = durationNote
//                        }
//                        
//                        times[i].places[j].notes[k].successor = successor
//                    }
//                    
//                    times[i].places[j].notes[k].joined = true
//                }
//            }
//        }
        
//        verifyPauses()
        
        generateUnions()
//        verifyUnions()
    }
    
//    func verifyUnions() {
//        for (i, time) in times.enumerated() {
//            for (j, _) in time.places.enumerated() {
//                times[i].places[j].has8 = false
//                times[i].places[j].has16 = false
//                times[i].places[j].joined = false
//            }
//        }
//
//        for (i, time) in times.enumerated() {
//            for (j, place) in time.places.enumerated() {
//                for (_, note) in place.notes.enumerated() {
//                    if note.usable {
//                        if note.duration == .quaver || note.duration == .quaverPontued {
//                            times[i].places[j].has8 = true
//                        }
//
//                        if note.duration == .semiquaver || note.duration == .semiquaverPontued {
//                            times[i].places[j].has16 = true
//                        }
//                    }
//                }
//            }
//        }
//
////        generateUnion()
//
//
//    }
    
    func verifyPauses() {
        for i in 0..<times.count {
            var hasNote = false
            
            for j in 0..<times[i].places.count {
                if times[i].places[j].notes.count > 0 {
                    hasNote = true
                }
                if !hasNote {
                    times[i].places[j].pause.exists = true
                    times[i].places[j].pause.type   = TypePause(rawValue: times[i].places[j].endTime - times[i].places[j].initialTime)!
                } else {
                    times[i].places[j].pause.exists = false
                }
                if j == times[i].places.count - 1 && times[i].places[j].notes.count == 0 {
                    times[i].places[j].pause.exists = true
                    times[i].places[j].pause.type   = TypePause(rawValue: times[i].places[j].endTime - times[i].places[j].initialTime)!
                }
            }
        }
        
//        for (i, time) in times.enumerated() {
//            
//            var hasNote = false
//            
//            for (j, place) in time.places.enumerated() {
//                if place.notes.count > 0 {
//                    hasNote = true
//                }
//                
//                if !hasNote {
//                    times[i].places[j].pause.exists = true
//                    times[i].places[j].pause.type   = TypePause(rawValue: place.endTime - place.initialTime)!
//                } else {
//                    times[i].places[j].pause.exists = false
//                }
//                
//                if j == time.places.count - 1 && place.notes.count == 0 {
//                    times[i].places[j].pause.exists = true
//                    times[i].places[j].pause.type   = TypePause(rawValue: place.endTime - place.initialTime)!
//                }
//            }
//            
//        }
        
        joinPauses()
    }
    
    func joinPauses() {
        for i in 0..<times.count {
            times[i].joinPauses()
        }
        
        for i in 0..<times.count {
            for j in 0..<times[i].places.count {
                if times[i].places[j].pause.exists && times[i].places[j].pause.completeTime {
                    var duration = times[i].places[j].pause.type.rawValue
                    
                    for l in 0..<times.count {
                        if l > i {
                            for m in 0..<times[l].places.count {
                                if times[l].places[m].pause.exists && times[l].places[m].pause.completeTime {
                                    duration += times[l].places[m].pause.type.rawValue
                                    times[l].places[m].pause.exists = false
                                }
                            }
                        }
                    }
                    
                    if let pauseDuration = TypePause(rawValue: duration) {
                        times[i].places[j].pause.type = pauseDuration
                    }
                }
            }
        }

//        for (i, time) in times.enumerated() {
//            for (j, place) in time.places.enumerated() {
//                if place.pause.exists && place.pause.completeTime {
//                    var duration = place.pause.type.rawValue
//
//                    for (l, timeBar) in times.enumerated() where l > i {
//                        for (m, placeBar) in timeBar.places.enumerated() {
//                            if placeBar.pause.exists && placeBar.pause.completeTime {
//                                duration += placeBar.pause.type.rawValue
//                                times[l].places[m].pause.exists = false
//                            }
//                        }
//                    }
//
//                    if let pauseDuration = TypePause(rawValue: duration) {
//                        times[i].places[j].pause.type = pauseDuration
//                    }
//
//                }
//
//            }
//        }

        if !verifyIfHasNote() && self.index != 1 && self.index != 3 {
            for i in 0..<times.count {
                for j in 0..<times[i].places.count {
                    times[i].places[j].pause.exists = false
                }
            }
        }
        
//        if !verifyIfHasNote() && self.index != 1 && self.index != 3 {
//            for (i, time) in times.enumerated() {
//                for (j, _) in time.places.enumerated() {
//                    times[i].places[j].pause.exists = false
//                }
//            }
//        }
    }
    
    func verifyIfHasNote() -> Bool {
        
        for time in times {
            for place in time.places {
                for _ in place.notes {
                    return true
                }
            }
        }
        
        return false
    }
    
    func getRelativeSize(spacing: SpacingType, quantization: Int) -> CGFloat {
        var size: CGFloat = 0.0
        
        for time in times {
            size += time.getRelativeSize(spacing: spacing, quantization: quantization)
        }
        
        return size
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        let border: CGFloat = parameters.borderBar
//        if state == .editable {
//            border = parameters.borderBarEdit
//        }
        var currentX = position.origin.x + border
        let currentY = position.origin.y
        let widthTime  = (position.width - (2.0 * border)) / CGFloat(numberOfTimes)
        let heightTime = position.height
        
        for time in times {
            let rectTime = CGRect(x: currentX, y: currentY, width: widthTime, height: heightTime)
            time.position = rectTime
            time.draw(parentView: parentView, parameters: parameters, state: state)
            
            currentX += widthTime
        }
        
        for union in unions {
            drawUnion(union: union, parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func drawUnwrittenNotes(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        for time in times {
            time.isCompose = self.isCompose
            time.drawUnwrittenNotes(parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for time in times {
            time.paintWrongNotes(currentTime: currentTime, notesPlayed: notesPlayed)
        }
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for time in times {
            time.paintAllNotesWithColor()
        }
    }
    
    private func drawUnion(union: DrawUnion, parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        
        var space = parameters.spacing
        if state == .editable {
            space = parameters.spacingEdit
        }
        
        var x1: CGFloat = 3000.0
        var x2: CGFloat = 0.0
        var y1: CGFloat = 3000.0
        var y2: CGFloat = 0.0
        
        for j in 0..<union.places.count {
            x1 = min(x1, union.places[j].position.origin.x)
            x2 = max(x2, union.places[j].position.origin.x)
            y1 = min(y1, union.places[j].position.origin.y)
            y2 = max(y2, union.places[j].position.origin.y)
        }
        
        var height: CGFloat = 2.0
        if y2 - y1 != 0 {
            height = y2 - y1
        }
        
        let pos1 = CGRect(x: x1 + (1.3 * space), y: y1, width: x2 - x1, height: height)
        
        let turn = isToTurn(union: union)
        
        y1 = pos1.origin.y - (union.lowerTone.height * space)
        y2 = pos1.origin.y - (union.greaterTone.height * space)
        
        if union.type == .constant {
//            if turn {
//                y1 = pos1.origin.y + (union.lowerTone.height * space)
//                y2 = pos1.origin.y + (union.lowerTone.height * space)
//            } else {
                y1 = pos1.origin.y - (union.greaterTone.height * space)
                y2 = pos1.origin.y - (union.greaterTone.height * space)
//            }
        }
        
        
        var desloc: CGFloat = 0.0
        
        if turn {
//            var lower = union.lowerTone.height * space
//
//            if lower < 0 {
//                lower *= -1
//            }
//
//            var greater = union.greaterTone.height * space
//
//            if greater < 0 {
//                greater *= -1
//            }
//
//            desloc = lower + greater + (5 * space)

            let centralTone = DrawTone()
            centralTone.clef = union.greaterTone.clef
            if union.channel == 1 || union.channel == 2 {
                centralTone.midi = MidiCode.bB4
            } else {
                centralTone.midi = MidiCode.d3
            }
            centralTone.tonalidade = union.greaterTone.tonalidade
            
            let greater = union.greaterTone.height
            let central = centralTone.height
            
            var desloc1 = greater - central
            
            if desloc1 < 0 {
                desloc1 *= -1
            }
            
            let lower = union.lowerTone.height

            var isToInclude = false
            
            if union.channel == 1 || union.channel == 2 {
                if union.lowerTone.midi.rawValue <= MidiCode.f4.rawValue {
                    isToInclude = true
                }
            }
            
            if union.channel == 3 || union.channel == 4 {
                if union.lowerTone.midi.rawValue <= MidiCode.d3.rawValue {
                    isToInclude = true
                }
            }
            
            var desloc2 = lower - central
            
            if desloc2 < 0 {
                desloc2 *= -1
            }
            
            if !isToInclude {
                desloc2 = 0
            }
            
            desloc = (desloc1 + desloc2 + 7) * space
        }
        
        let yMenor = min(y1, y2) + desloc
        let yMaior = max(y1, y2) + desloc
        
        var pos = CGRect(x: pos1.origin.x + (space * 0.25), y: yMenor, width: pos1.width + (space * 0.1), height: (yMaior - yMenor) + 3.0)
        
        if turn {
            pos = CGRect(x: pos1.origin.x, y: yMenor, width: pos1.width + (space * 0.15), height: (yMaior - yMenor) + 3.0)
        }
        
        x1 = pos1.origin.x + (space * 0.1)
        x2 = x1 + pos1.width
        var yI = yMenor + 2.0
        var yF = yI + (yMaior - yI) + 2.0
        
        if union.type == .increasing {
            let aux = yI
            yI = yF
            yF = aux
        }

        union.b        = ( ( x1 * yF ) - ( x2 * yI ) ) / ( x1 - x2 )
        if (x1 - x2) == 0 {
            union.b = 1
        }
        if x1 == 0 {
            x1 = 1
        }
        union.a        = (yI - union.b) / x1
        if union.a.isNaN {
            
        }
        union.channel  = index
        union.position = pos
        union.draw(parentView: parentView, parameters: parameters, state: state)
    }
    
    func sendNotesToFront(parentView: ViewPageScore) {
        for time in times {
            time.sendNotesToFront(parentView: parentView)
        }
    }
}

struct PlaceOfUnion {
    var time = 1
    var place = DrawPlace()
}

struct TimeUnion {
    var every8  = false
    var every16 = false
    var every32 = false
    var time   = 1
    var places: [DrawPlace] = []
}

struct UnionPerTime {
    var time    = 1
    var every8  = true
    var every16 = true
    var places: [DrawPlace] = []
}
