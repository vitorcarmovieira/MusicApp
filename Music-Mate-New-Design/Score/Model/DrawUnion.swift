//
//  DrawUnion.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawUnion {
    var places       : [DrawPlace] = []
    var stem         : [DrawStem] = []
    var semiUnions   : [DrawSemiunion] = []
    var greaterTone = DrawTone()
    var lowerTone   = DrawTone()
    var type        = TypeUnion.increasing
    var channel     = 1
    var turn        = false
    
    var allOctavus     = false
    var allSemioctavus = false
    
    var a: CGFloat = 0.0
    var b: CGFloat = 0.0
    
    var position = CGRect()
    private var adapter: AdapterUnion!
    
    deinit {
        places.removeAll()
        stem.removeAll()
        semiUnions.removeAll()
        adapter = nil
    }
    func averageMidis() -> Int {
        var sumMidis = 0
        var countNotes = 0
        
        for place in places {
            for note in place.notes {
                if note.usable && note.joined  {
                    sumMidis += note.tone.midi.rawValue
                    countNotes += 1
                }
            }
        }
        
        if countNotes > 0 {
            return sumMidis/countNotes
        }
        return 0
    }
    
    func generateSemiunions() {
        semiUnions.removeAll()
        var currentSemiunions: [DrawSemiunion] = []
        
        var semiunion = DrawSemiunion()
        semiunion.channel = channel
        semiunion.type    = .initiated
        
        for i in 0..<places.count-1 {
            if places[i].hasNotesSemioctavus() && !places[i + 1].hasNotesSemioctavus() && semiunion.type == .initiated {
                semiunion.placeStart = places[i]
                semiunion.placeEnd   = places[i]
                semiunion.type       = .left
                
                currentSemiunions.append(semiunion)
                
                semiunion = DrawSemiunion()
                semiunion.type = .initiated
            }
            if places[i].hasNotesSemioctavus() && !places[i + 1].hasNotesSemioctavus() && semiunion.type != .initiated {
                semiunion.placeEnd   = places[i]
                semiunion.type       = .complete
                
                currentSemiunions.append(semiunion)
                
                semiunion = DrawSemiunion()
                semiunion.type = .initiated
            }
            if places[i].hasNotesSemioctavus() && places[i + 1].hasNotesSemioctavus() && semiunion.type != .initiated {
                semiunion.placeEnd   = places[i]
                semiunion.type       = .complete
            }
            if places[i].hasNotesSemioctavus() && places[i + 1].hasNotesSemioctavus() && semiunion.type == .initiated {
                semiunion.placeStart   = places[i]
                semiunion.placeEnd   = places[i]
                semiunion.type       = .complete
            }
            if (i == places.count-2) && places[i + 1].hasNotesSemioctavus() {
                if semiunion.type != .initiated  {
                    semiunion.placeEnd   = places[i + 1]
                    semiunion.type       = .complete
                    
                    currentSemiunions.append(semiunion)
                } else {
                    semiunion.placeStart = places[i + 1]
                    semiunion.placeEnd   = places[i + 1]
                    semiunion.type       = .right
                    
                    currentSemiunions.append(semiunion)
                }
            }
        }
        
        for current in currentSemiunions {
            semiUnions.append(current)
        }
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        var turn = false
        
        let averageNotes = averageMidis()
        
        if averageNotes >= 70 && (channel == 1 || channel == 2) {
            turn = true
        }
        
        if averageNotes >= 50 && (channel == 3 || channel == 4) {
            turn = true
        }
        
        let space = parameters.spacing

        if adapter == nil {
            adapter = AdapterUnion()
        }
        adapter.type = type

        if turn {
            position.origin = CGPoint(x: position.origin.x - (0.8*space), y: position.origin.y)
//            ss
        }
        
        adapter.adapt(parentView: parentView, frame: position)
      
        

        for i in 0..<places.count {
            var alturaMenorTom = space * places[i].lowerTone.height
            
            var x = places[i].position.origin.x
            
            if turn {
                x -= 1.08 * space
                alturaMenorTom = space * places[i].greaterTone.height
            }
            
            let yIni = getY(a: a, b: b, x: x + (space * 1.6)) - (space * 0.2)
            let yFim = (places[i].position.origin.y + places[i].position.size.height) - alturaMenorTom

            let width  = space * 1.6
            let height = (yFim - yIni) - (space * 0.7)

            var yStart = yIni
            
            if turn {
                yStart = yFim - (space * 0.4)
            }
            
            if yStart.isNaN {
                print("Entrou")
            }
            
            let posStem = CGRect(x: x, y: yStart, width: width, height: height)
            places[i].stem.position = posStem
            places[i].stem.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        var desloc = (space * 0.3)
        
        if turn {
            desloc = -(space * 1.2)
        }
        for semiunion in semiUnions {
            if semiunion.type == .left {
                var xIni = semiunion.placeStart.position.origin.x + (space * 1.4)
                var xEnd = semiunion.placeStart.position.origin.x + (space*1.2) + (space * 1.4)

                if turn {
                    xIni -= 0.8 * space
                    xEnd -= 0.8 * space
                } else {
                    xIni += 0.3 * space
                    xEnd += 0.3 * space
                }
                
                let yIni = (self.a * xIni) + self.b + desloc
                let yEnd = (self.a * xEnd) + self.b + desloc
                
                let pIni = CGPoint(x: xIni, y: yIni)
                let pEnd = CGPoint(x: xEnd, y: yEnd)
                
                semiunion.draw(parentView: parentView, parameters: parameters, state: state, pIni: pIni, pEnd: pEnd)
                
            }
            
            if semiunion.type == .complete {
                var xIni = semiunion.placeStart.position.origin.x + (space * 1.4)
                var xEnd = semiunion.placeEnd.position.origin.x + (space * 1.4)

                if turn {
                    xIni -= 0.8 * space
                    xEnd -= 0.8 * space
                } else {
                    xIni += 0.3 * space
                    xEnd += 0.3 * space
                }
                
                let yIni = (self.a * xIni) + self.b + desloc
                let yEnd = (self.a * xEnd) + self.b + desloc
                
                let pIni = CGPoint(x: xIni, y: yIni)
                let pEnd = CGPoint(x: xEnd, y: yEnd)
                
                semiunion.draw(parentView: parentView, parameters: parameters, state: state, pIni: pIni, pEnd: pEnd)
                
            }
            
            if semiunion.type == .right {
                var xIni = semiunion.placeStart.position.origin.x - (space*1.2) + (space * 1.4)
                var xEnd = semiunion.placeStart.position.origin.x + (space * 1.4)

                if turn {
                    xIni -= 0.8 * space
                    xEnd -= 0.8 * space
                } else {
                    xIni += 0.3 * space
                    xEnd += 0.3 * space
                }
                
                let yIni = (self.a * xIni) + self.b + desloc
                let yEnd = (self.a * xEnd) + self.b + desloc
                
                let pIni = CGPoint(x: xIni, y: yIni)
                let pEnd = CGPoint(x: xEnd, y: yEnd)
                
                semiunion.draw(parentView: parentView, parameters: parameters, state: state, pIni: pIni, pEnd: pEnd)
                
            }
        }
    }
    
    private func getY(a: CGFloat, b: CGFloat, x: CGFloat) -> CGFloat {
        return (a * x) + b
    }
}

enum TypeUnion {
    case noteInitiated
    case initiated
    case increasing
    case decreasing
    case constant
}
