//
//  DrawFooter.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawFooter {
    var position = CGRect()
    
    var pageNumber = DrawPageNumber()
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        let initialYFooter = parameters.initialYFooter
//        if state == .editable {
//            initialYFooter = parameters.initialYFooterEdit
//        }
        let positionNumber = CGRect(x: 0, y: initialYFooter, width: parameters.width, height: parameters.heightFooter)
        pageNumber.position = positionNumber
        
        pageNumber.draw(parentView: parentView, parameters: parameters, state: state)
    }
}
