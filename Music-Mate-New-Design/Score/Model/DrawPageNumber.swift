//
//  DrawPageNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawPageNumber {
    var position = CGRect()
    var number = 1
    var total  = 1
    private var adapter: AdapterPageNumber!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterPageNumber()
        }
        adapter.number = number
        adapter.total  = total
        adapter.parameters = parameters
        adapter.adapt(parentView: parentView, frame: position)
    }
}
