//
//  DrawSemiunion.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/13/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class DrawSemiunion {
    var placeStart = DrawPlace()
    var placeEnd   = DrawPlace()
    
    var format  = FormatSemiunion.increasing
    var type    = TypeSemiunion.complete
    var channel     = 1
    
    private var adapter: AdapterSemiunion!
    
    var a: CGFloat = 0.0
    var b: CGFloat = 0.0
    
    deinit {
        adapter = nil
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState, pIni: CGPoint, pEnd: CGPoint) {
//        var space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }
        
        if adapter == nil {
            adapter = AdapterSemiunion()
        }
        var height = pEnd.y - pIni.y
        
        if height < 2.0 {
            height = 2.0
        }
        
        let position = CGRect(x: pIni.x, y: pIni.y, width: pEnd.x - pIni.x + 10, height: height + 10)
        
        adapter.type = format
        adapter.pIni = CGPoint(x: pIni.x - position.origin.x, y: pIni.y - position.origin.y + 3)
        adapter.pEnd = CGPoint(x: pEnd.x - position.origin.x, y: pEnd.y - position.origin.y + 3)

        adapter.adapt(parentView: parentView, frame: position)
    }
}

enum FormatSemiunion {
    case initiated
    case increasing
    case decreasing
    case constant
}

enum TypeSemiunion {
    case initiated
    case left
    case right
    case complete
}
