//  DrawTime.swift

//  Music-Mate-New-Design

//  Created by Diego Lopes on 10/8/17.

//  Copyright © 2017 LT Music Developer. All rights reserved.


import UIKit

class DrawTime {
    var index = 0
    
    var initialTime = 0
    var endTime     = 0
    var channel     = 1
    var isCompose   = false
    
    var timeDuration : Int {
        return self.endTime - self.initialTime
    }
    
    var barDuration = 0
    
    var places : [DrawPlace] = []
    
    private var placesPerTime = 0
    
    var position = CGRect()
    
    init(placesPerTime: Int, initialTime: Int, endTime: Int, barDuration: Int, channel: Int) {
        self.placesPerTime = placesPerTime
        self.initialTime = initialTime
        self.endTime     = endTime
        self.barDuration = barDuration
        self.channel     = channel
        addPlaces()
    }
    
    func addPlaces() {
        let durationPlace = (endTime - initialTime) / placesPerTime
        var currentTime  = initialTime
        for i in 0..<placesPerTime {
            let place = DrawPlace()
            place.index = i + 1
            place.channel = channel
            place.initialTime = currentTime
            place.endTime = currentTime + durationPlace
            place.barDuration = self.barDuration
//            for j in 0..<1 {
//                let tone = DrawTone()
//                tone.clef = .SOL
//                tone.midi = MidiCode(rawValue: 64 + (3 * j))!
//                let note = DrawNote()
//                note.tone = tone
//                place.notes.append(note)
//            }
            places.append(place)
            
            currentTime += durationPlace
        }
    }
    
    func addPause(channel: Int, start: Int, end: Int) {
        for i in 0..<places.count {
            if places[i].initialTime <= start && start + 1 <= places[i].endTime  {
                places[i].addPause(channel: channel, start: start, end: end)
                break
            }
        }
    }
    
    func addNote(midi: Int, channel: Int, initialTime: Int, endTime: Int, bar: Int, clef: Int, predecessor: Bool, successor: Bool, startNote: Bool, realInitialTime: Int, realEndTime: Int, key: Int, isCorrect: Bool) {
        var initialTimeBar = false
        if self.initialTime == initialTime {
            initialTimeBar = true
        }
        
        for i in 0..<places.count {
            if places[i].initialTime <= initialTime && initialTime + 1 <= places[i].endTime  { 
                places[i].addNote(midi: midi, channel: channel, duration: endTime - initialTime, bar: bar, time: index, clef: clef, predecessor: predecessor, successor: successor, startNote: startNote, startTime: initialTimeBar, realInitialTime: realInitialTime, realEndTime: realEndTime, key: key, isCorrect: isCorrect)
                break
            }
        }
    }
    
    func addAccident(linesAccidents: inout [LineAccident], key: DrawKey) {
        for place in places {
            place.addAccident(linesAccidents: &linesAccidents, key: key)
        }
    }
    
    func joinNotes() {
        
        for i in 0..<places.count {
            for j in 0..<places[i].notes.count {
                let midi = places[i].notes[j].tone.midi
                var duration = places[i].notes[j].duration.rawValue
                var successor = places[i].notes[j].successor
                var lastK = i
                
                if places[i].notes[j].successor && places[i].notes[j].usable {
                    for k in 0..<places.count {
                        if k > i {
                            for l in 0..<places[k].notes.count {
                                if places[k].notes[l].tone.midi == midi {
                                    if places[k].notes[l].tone.midi == midi
                                        && places[k].notes[l].usable
                                        && successor
                                        && places[k].notes[l].predecessor
                                        && k != lastK
                                        && places[i].notes[j].realInitialTime == places[k].notes[l].realInitialTime
                                        && places[i].notes[j].realEndTime == places[k].notes[l].realEndTime {
                                        duration += places[k].notes[l].duration.rawValue
                                        places[k].notes[l].usable = false
                                        successor = places[k].notes[l].successor
                                        places[k].notes[l].successor = false
                                        lastK = k
                                    }
                                }
                            }
                        }
                    }
                    
                    if let noteDuration = NoteDuration(rawValue: duration) {
                        places[i].notes[j].duration = noteDuration
                    }
                    places[i].notes[j].successor = successor
                    
                    if duration == timeDuration {
                        places[i].notes[j].completeTime = true
                    } else {
                        places[i].notes[j].completeTime = false
                    }
                }
                
            }
        }
        
        
        
        
        
//        for (i, place) in places.enumerated() {
//            for (j, note) in place.notes.enumerated() {
//                let midi = note.tone.midi
//                var duration = note.duration.rawValue
//                var successor = note.successor
//                var lastK = i
//                
//                if note.successor && note.usable {
//                    for (k, placeBar) in places.enumerated() where k > i {
//                        for (l, noteBar) in placeBar.notes.enumerated() where noteBar.tone.midi == midi {
//                            if noteBar.tone.midi == midi && noteBar.usable && successor && noteBar.predecessor && k != lastK && note.realInitialTime == noteBar.realInitialTime && note.realEndTime == noteBar.realEndTime {
//                                duration += noteBar.duration.rawValue
//                                places[k].notes[l].usable = false
//                                successor = places[k].notes[l].successor
//                                places[k].notes[l].successor = false
//                                lastK = k
//                            }
//                        }
//                    }
//                    
//                    if let noteDuration = NoteDuration(rawValue: duration) {
//                        places[i].notes[j].duration = noteDuration
//                    }
//                    places[i].notes[j].successor = successor
//                    
//                    if duration == timeDuration {
//                        places[i].notes[j].completeTime = true
//                    } else {
//                        places[i].notes[j].completeTime = false
//                    }
//                }
//                
//            }
//        }
    }
    
    func joinPauses() {
//        for place in places {
//            if place.pause.exists {
//                var duration = place.pause.type.rawValue
//
//                for placeBar in places {
//                    if placeBar.index > place.index {
//                        if placeBar.pause.exists {
//                            duration += placeBar.pause.type.rawValue
//                            placeBar.pause.exists = false
//                        }
//                    }
//                }
//
//                if let pauseDuration = TypePause(rawValue: duration) {
//                    place.pause.type = pauseDuration
//                }
//
//                if duration == timeDuration {
//                    place.pause.completeTime = true
//                } else {
//                    place.pause.completeTime = false
//                }
//            }
//        }
        
        for i in 0..<places.count {
            if places[i].pause.exists {
                var duration = places[i].pause.type.rawValue
                
                for k in 0..<places.count {
                    if k > i {
                        if places[k].pause.exists {
                            duration += places[k].pause.type.rawValue
                            places[k].pause.exists = false
                        }
                    }
                }
                
                if let pauseDuration = TypePause(rawValue: duration) {
                    places[i].pause.type = pauseDuration
                }
                
                if duration == timeDuration {
                    places[i].pause.completeTime = true
                } else {
                    places[i].pause.completeTime = false
                }
            }
        }
        
//        for (i, place) in places.enumerated() {
//            if place.pause.exists {
//                var duration = place.pause.type.rawValue
//                
//                for (k, placeBar) in places.enumerated() where k > i {
//                    if placeBar.pause.exists {
//                        duration += placeBar.pause.type.rawValue
//                        places[k].pause.exists = false
//                    }
//                }
//                
//                if let pauseDuration = TypePause(rawValue: duration) {
//                    places[i].pause.type = pauseDuration
//                }
//                
//                if duration == timeDuration {
//                    places[i].pause.completeTime = true
//                } else {
//                    places[i].pause.completeTime = false
//                }
//            }
//        }
    }
    
    func getRelativeSize(spacing: SpacingType, quantization: Int) -> CGFloat {
        var size: CGFloat = 0.0
        
        for place in places {
            size += place.getRelativeSize(spacing: spacing, quantization: quantization)
        }
        
        return size
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        var currentX = position.origin.x
        let currentY = position.origin.y
        let widthPlace  = position.width / CGFloat(placesPerTime)
        let heightPlace = position.height
        
        for place in places {
            let rectTime = CGRect(x: currentX, y: currentY, width: widthPlace, height: heightPlace)
            place.position = rectTime
            place.draw(parentView: parentView, parameters: parameters, state: state)
            
            currentX += widthPlace
        }
    }
    
    func drawUnwrittenNotes(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        for place in places {
            place.isCompose = self.isCompose
            place.drawUnwrittenNotes(parentView: parentView, parameters: parameters, state: state)
        }
    }
    
    func paintWrongNotes(currentTime: Int, notesPlayed: [ModelNotePlayed]) {
        for place in places {
            place.paintWrongNotes(currentTime: currentTime, notesPlayed: notesPlayed)
        }
    }
    
    func paintAllNotesWithColor(color: UIColor = UIColor.black) {
        for place in places {
            place.paintAllNotesWithColor()
        }
    }
    
    func sendNotesToFront(parentView: ViewPageScore) {
        for place in places {
            place.sendNotesToFront(parentView: parentView)
        }
    }
    
    deinit {
        places.removeAll()
    }
}
