//
//  DrawRepetition.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawRepetition {
    var bar = 0
    var type = TypeRepetition.none
    var numberOfPentagrams = 1
    
    var position = CGRect()
    private var adapter: AdapterRepetition!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterRepetition()
            adapter.parameters = parameters
        }
        
        adapter.bar   = bar
        adapter.state = state
        adapter.type  = type
        adapter.numberOfPentagrams = numberOfPentagrams
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}

enum TypeRepetition {
    case start
    case end
    case bracket1
    case bracket2
    case segno
    case coda
    case toCoda
    case daCapo
    case delSegno
    case dsAlFine
    case dsAlCoda
    case dcAlFine
    case dcAlCoda
    case fine
    case none
}
