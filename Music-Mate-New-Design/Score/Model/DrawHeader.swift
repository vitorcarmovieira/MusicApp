//
//  DrawHeader.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawHeader {
    var position  = CGRect()
    var title     = DrawTitle()
    var subtitle  = DrawSubtitle()
    var author    = DrawAuthor()
    var metronome = DrawMetronome()
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        let initialY: CGFloat = parameters.initialYTitle
//        if state == .editable {
//            initialY *= 4
//        }
        let positionTitle = CGRect(x: 0, y: initialY, width: parameters.width, height: parameters.heightTitle)
        title.position = positionTitle
        title.draw(parentView: parentView, parameters: parameters, state: state)
        
        let initialYAuthor: CGFloat = parameters.initialYBaseNote
        let space: CGFloat = parameters.spacing
        let heightNote: CGFloat = parameters.heightBaseNote
//        if state == .editable {
//            initialYAuthor *= 2
//            space *= 2
//            heightNote *= 2
//        }
        
        if state != .editable {
            let positionAuthor = CGRect(x: 0, y: initialY*1.9, width: parameters.width * 0.98, height: heightNote * 0.6)
            author.position = positionAuthor
            author.draw(parentView: parentView, parameters: parameters, state: state)
            
            let positionSubtitle = CGRect(x: 0, y: initialY*1.9, width: parameters.width, height: heightNote * 0.6)
            subtitle.position = positionSubtitle
            subtitle.draw(parentView: parentView, parameters: parameters, state: state)
        }
        
        metronome.draw(parentView: parentView, parameters: parameters, state: state)
    }
}
