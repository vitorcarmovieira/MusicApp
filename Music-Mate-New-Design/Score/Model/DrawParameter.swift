//
//  DrawParameter.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawParameter {
    
    var width  : CGFloat = 0.0
    var height : CGFloat = 0.0
    
    var spacing : CGFloat {
        return (height * 0.0075)
    }
    
    var spacingEdit : CGFloat {
        return spacing //* 1.33
    }
    
    init() {
        
    }
    
    init(width: CGFloat, height: CGFloat) {
        self.width  = width
        self.height = height
    }
    var borderBar     : CGFloat { return self.spacing * 1.5 }
    var borderBarEdit : CGFloat { return self.spacingEdit * 1.5 }
    
    var heightTitle   : CGFloat { return self.spacing * 5 }
    var initialYTitle : CGFloat { return self.spacing * 2.5 }
    
    var heightBaseNote: CGFloat { return self.spacing * 5 }
    var initialYBaseNote: CGFloat { return self.spacing * 11 }
    
    var heightFooter: CGFloat { return self.spacing * 3 }
    var initialYFooter: CGFloat { return self.height * 0.95 }
    var initialYFooterEdit: CGFloat { return self.height * 0.95 }
    
    var widthUtilizable: CGFloat {
        return width
    }
    
    var topBorder: CGFloat {
        return 2.0 * self.spacing
    }
    
    var rightBorder: CGFloat {
        return 2.0 * self.spacing
    }
    
    var leftBorder: CGFloat {
        return rightBorder
    }
    
    var borderFirstBar: CGFloat {
        return 5.0 * self.spacing
    }
    
    var initialY: CGFloat {
        return heightHeader + topBorder + (6.0 * spacing)
    }
    
    var initialYWithHeader: CGFloat {
        return topBorder + (6.0 * spacing)
    }
    
    var initialYWithHeaderEdit: CGFloat {
        return initialYWithHeader //* 2.0
    }
    
    var heightHeader: CGFloat {
        return self.spacing * 10.0
    }

    var spaceNecessary: CGFloat {
        return 10 * self.spacing
    }
}
