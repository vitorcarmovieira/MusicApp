//
//  DrawPause.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawPause {
    var exists = false
    var completeTime = false
    var type = TypePause.seminima
    private var channel : Int = 1
    private var start   : Int = 1
    private var end     : Int = 1
    
    init() {
        
    }
    
    deinit {
        adapter = nil
    }
    
    init(channel: Int, start: Int, end: Int) {
        self.channel = channel
        self.start   = start
        self.end     = end
    }
    
    func getChannel() -> Int {
        return channel
    }
    
    func getStart() -> Int {
        return start
    }
    
    func getEnd() -> Int {
        return end
    }
    
    var position = CGRect()
    
    private var adapter: AdapterPause!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterPause()
        }
        adapter.parameters = parameters
        adapter.channel = channel
        adapter.duration = type.rawValue
        adapter.state = state
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    func erase(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter != nil {
            adapter.view.removeFromSuperview()
        }
    }
}

enum TypePause: Int {
    case fusa                 = 120
    case fusaPontuada         = 180
    case semicolcheia         = 240
    case semicolcheiaPontuada = 360
    case colcheia             = 480
    case colcheiaPontuada     = 720
    case seminima             = 960
    case seminimaPontuada     = 1440
    case minima               = 1920
    case minimaPontuada       = 2880
    case semibreve            = 3840
    case nova2                = 4320
    case nova1                = 4800
    case semibrevePontuada    = 5760
}
