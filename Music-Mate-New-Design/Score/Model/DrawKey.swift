//
//  DrawKey.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawKey {
    var type            : KeyType        = KeyType.C_Am
    var clef            : ClefType       = ClefType.SOL
    var sharps          : [DrawSharp]    = []
    var flats           : [DrawFlat]     = []
    var accidentsTones  : [AccidentTone] = []
    var greaterPosition : CGFloat = 0.0
    
    var position = CGRect()

    deinit {
        sharps.removeAll()
        flats.removeAll()
        accidentsTones.removeAll()
    }
    
    init() {
        
    }
    
    init(type: KeyType, clef: ClefType) {
        self.type = type
        self.clef = clef
        initializeAccidentsTone()
    }
    
    func eraseKey() {
        sharps.removeAll()
        flats.removeAll()
        greaterPosition = 0.0
    }
    
    func createKey() {
        if clef == ClefType.SOL {
            switch type {
            case .C_Am:
                break
            case .G_Em:
                criaG_EmSol()
            case .D_Bm:
                criaD_BmSol()
            case .A_Fsm:
                criaA_FsmSol()
            case .E_Csm:
                criaE_CsmSol()
            case .B_Gsm:
                criaB_GsmSol()
            case .Fs_Dsm:
                criaFs_DsmSol()
            case .F_Dm:
                criaF_DmSol()
            case .Bb_Gm:
                criaBb_GmSol()
            case .Eb_Cm:
                criaEb_CmSol()
            case .Ab_Fm:
                criaAb_FmSol()
            case .Db_BBm:
                criaDb_BbmSol()
            case .Gb_Ebm:
                criaGb_EbmSol()
            case .Cs_Asm:
                criaCs_AsmSol()
            }
        }
        
        if clef == ClefType.FA {
            switch type {
            case .C_Am:
                break
            case .G_Em:
                criaG_EmFa()
            case .D_Bm:
                criaD_BmFa()
            case .A_Fsm:
                criaA_FsmFa()
            case .E_Csm:
                criaE_CsmFa()
            case .B_Gsm:
                criaB_GsmFa()
            case .Fs_Dsm:
                criaFs_DsmFa()
            case .F_Dm:
                criaF_DmFa()
            case .Bb_Gm:
                criaBb_GmFa()
            case .Eb_Cm:
                criaEb_CmFa()
            case .Ab_Fm:
                criaAb_FmFa()
            case .Db_BBm:
                criaDb_BbmFa()
            case .Gb_Ebm:
                criaGb_EbmFa()
            case .Cs_Asm:
                criaCs_AsmFa()
            }
        }
    }
    
    func adicionaSustenido(posicao: Int, tom: DrawTone) {
        let sharp = DrawSharp()
        sharp.local = posicao
        sharp.tone     = tom
        
        self.sharps.append(sharp)
    }
    
    func adicionaBemol(posicao: Int, tom: DrawTone) {
        let flat = DrawFlat()
        flat.local = posicao
        flat.tone     = tom
        
        self.flats.append(flat)
    }
    
    
    private func marcaSustenidoVerdadeiroTom(tom: MidiCode) {
        var tomAtual = tom.rawValue
        
        while tomAtual >= 12 {
            accidentsTones[tomAtual - 12].sharp = true
            tomAtual -= 12
        }
        
        tomAtual = tom.rawValue
        
        while tomAtual <= 119 {
            accidentsTones[tomAtual - 12].sharp = true
            tomAtual += 12
        }
    }
    
    private func marcaBemolVerdadeiroTom(tom: MidiCode) {
        var tomAtual = tom.rawValue
        
        while tomAtual >= 12 {
            accidentsTones[tomAtual - 12].flat = true
            tomAtual -= 12
        }
        
        tomAtual = tom.rawValue
        
        while tomAtual <= 119 {
            accidentsTones[tomAtual - 12].flat = true
            tomAtual += 12
        }
    }
    
    private func criaG_EmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        
        self.greaterPosition = 1
    }
    
    private func criaD_BmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS5)
        
        self.greaterPosition = 2
    }
    
    private func criaA_FsmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g5
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS5)
        
        self.greaterPosition = 3
    }
    
    private func criaE_CsmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g5
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS5)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d5
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB5)
        
        
        self.greaterPosition = 4
    }
    
    private func criaB_GsmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g5
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS5)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d5
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a4
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.bB4)
        
        
        self.greaterPosition = 5
    }
    
    private func criaFs_DsmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g5
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS5)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d5
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a4
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.bB4)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi = MidiCode.e5
        adicionaSustenido(posicao: 6, tom: tom6)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.e5)
        
        self.greaterPosition = 6
    }
    
    private func criaCs_AsmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f5
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c5
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g5
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d5
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a4
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a4)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi = MidiCode.e5
        adicionaSustenido(posicao: 6, tom: tom6)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.e5)
        
        let tom7 = DrawTone()
        tom7.clef = self.clef
        tom7.midi = MidiCode.b4
        adicionaSustenido(posicao: 7, tom: tom7)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.b4)
        
        self.greaterPosition = 7
    }
    
    private func criaF_DmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB4)
        
        self.greaterPosition = 1
    }
    
    private func criaBb_GmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB4)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e5
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e5)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB5)
        
        self.greaterPosition = 2
    }
    
    private func criaEb_CmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB4)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e5
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e5)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a4
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a4)
         marcaBemolVerdadeiroTom(tom: MidiCode.gS4)
        
        self.greaterPosition = 3
    }
    
    private func criaAb_FmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB4)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e5
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e5)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a4
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a4)
        marcaBemolVerdadeiroTom(tom: MidiCode.gS4)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d5
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d5)
        marcaBemolVerdadeiroTom(tom: MidiCode.cS5)
        
        self.greaterPosition = 4
    }
    
    private func criaDb_BbmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB4)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e5
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e5)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a4
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a4)
        marcaBemolVerdadeiroTom(tom: MidiCode.gS4)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d5
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d5)
        marcaBemolVerdadeiroTom(tom: MidiCode.cS5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi  = MidiCode.g4
        adicionaBemol(posicao: 5, tom: tom5)
        marcaBemolVerdadeiroTom(tom: MidiCode.g4)
        marcaBemolVerdadeiroTom(tom: MidiCode.fS4)
        
        self.greaterPosition = 5
    }
    
    private func criaGb_EbmSol() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b4
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b4)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e5
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a4
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a4)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d5
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi  = MidiCode.g4
        adicionaBemol(posicao: 5, tom: tom5)
        marcaBemolVerdadeiroTom(tom: MidiCode.g4)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi  = MidiCode.c5
        adicionaBemol(posicao: 6, tom: tom6)
        marcaBemolVerdadeiroTom(tom: MidiCode.c5)
        
        self.greaterPosition = 6
    }
    
    //--------------------------Clave Fa-------------------------------
    private func criaG_EmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        self.greaterPosition = 1
    }
    
    private func criaD_BmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS3)
        
        self.greaterPosition = 2
    }
    
    private func criaA_FsmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS3)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g3
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS3)
        
        self.greaterPosition = 3
    }
    
    private func criaE_CsmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS3)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g3
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS3)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d3
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB3)
        
        self.greaterPosition = 4
    }
    
    private func criaB_GsmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c3)
         marcaSustenidoVerdadeiroTom(tom: MidiCode.cS3)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g3
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS3)
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d3
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB3)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a3
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.bB3)
        
        self.greaterPosition = 5
    }
    
    private func criaFs_DsmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.fS3)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.cS3)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g3
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.gS3)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d3
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.eB3)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a2
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.bB3)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi = MidiCode.e3
        adicionaSustenido(posicao: 6, tom: tom6)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.e3)
        
        self.greaterPosition = 6
    }
    
    private func criaCs_AsmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.f3
        adicionaSustenido(posicao: 1, tom: tom1)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.f5)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi = MidiCode.c3
        adicionaSustenido(posicao: 2, tom: tom2)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.c5)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi = MidiCode.g3
        adicionaSustenido(posicao: 3, tom: tom3)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.g5)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi = MidiCode.d3
        adicionaSustenido(posicao: 4, tom: tom4)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.d5)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi = MidiCode.a2
        adicionaSustenido(posicao: 5, tom: tom5)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.a4)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi = MidiCode.e3
        adicionaSustenido(posicao: 6, tom: tom6)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.e5)
        
        let tom7 = DrawTone()
        tom7.clef = self.clef
        tom7.midi = MidiCode.b2
        adicionaSustenido(posicao: 7, tom: tom7)
        marcaSustenidoVerdadeiroTom(tom: MidiCode.b4)
        
        self.greaterPosition = 7
    }
    
    private func criaF_DmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB2)
        
        self.greaterPosition = 1
    }
    
    private func criaBb_GmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB2)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e3
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e3)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB2)
        
        self.greaterPosition = 2
    }
    
    private func criaEb_CmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB2)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e3
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e3)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB2)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a2
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a2)
        marcaBemolVerdadeiroTom(tom: MidiCode.gS2)
        
        self.greaterPosition = 3
    }
    
    private func criaAb_FmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB2)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e3
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e3)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB2)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a2
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a2)
        marcaBemolVerdadeiroTom(tom: MidiCode.gS2)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d3
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d3)
        marcaBemolVerdadeiroTom(tom: MidiCode.cS2)
        
        self.greaterPosition = 4
    }
    
    private func criaDb_BbmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        marcaBemolVerdadeiroTom(tom: MidiCode.bB2)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e3
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e3)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB2)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a2
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a2)
        marcaBemolVerdadeiroTom(tom: MidiCode.gS2)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d3
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d3)
        marcaBemolVerdadeiroTom(tom: MidiCode.eB2)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi  = MidiCode.g2
        adicionaBemol(posicao: 5, tom: tom5)
        marcaBemolVerdadeiroTom(tom: MidiCode.g2)
        marcaBemolVerdadeiroTom(tom: MidiCode.fS2)
        
        self.greaterPosition = 5
    }
    
    private func criaGb_EbmFa() {
        let tom1 = DrawTone()
        tom1.clef = self.clef
        tom1.midi  = MidiCode.b2
        adicionaBemol(posicao: 1, tom: tom1)
        marcaBemolVerdadeiroTom(tom: MidiCode.b2)
        
        let tom2 = DrawTone()
        tom2.clef = self.clef
        tom2.midi  = MidiCode.e3
        adicionaBemol(posicao: 2, tom: tom2)
        marcaBemolVerdadeiroTom(tom: MidiCode.e3)
        
        let tom3 = DrawTone()
        tom3.clef = self.clef
        tom3.midi  = MidiCode.a2
        adicionaBemol(posicao: 3, tom: tom3)
        marcaBemolVerdadeiroTom(tom: MidiCode.a2)
        
        let tom4 = DrawTone()
        tom4.clef = self.clef
        tom4.midi  = MidiCode.d3
        adicionaBemol(posicao: 4, tom: tom4)
        marcaBemolVerdadeiroTom(tom: MidiCode.d3)
        
        let tom5 = DrawTone()
        tom5.clef = self.clef
        tom5.midi  = MidiCode.g2
        adicionaBemol(posicao: 5, tom: tom5)
        marcaBemolVerdadeiroTom(tom: MidiCode.g2)
        
        let tom6 = DrawTone()
        tom6.clef = self.clef
        tom6.midi  = MidiCode.c3
        adicionaBemol(posicao: 6, tom: tom6)
        marcaBemolVerdadeiroTom(tom: MidiCode.c3)
        
        self.greaterPosition = 6
    }
    
    func getKeyRelativeSize() -> CGFloat {
        return CGFloat(self.greaterPosition) * 1.3
    }
    
    func initializeAccidentsTone() {
        for i in 12...119 {
            let accidenteTone = AccidentTone(tone: MidiCode(rawValue: i)!, sharp: false, flat: false, natural: false)
            accidentsTones.append(accidenteTone)
        }
    }
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        
        let space = parameters.spacing
//        if state == .editable {
//            space = parameters.spacingEdit
//        }

        var xIni = position.origin.x
        
        for i in 0..<sharps.count {
            let posSustenido = CGRect(x: xIni, y: position.origin.y - (sharps[i].tone.height * space), width: 2 * space, height: 4.5 * space)
            sharps[i].position = posSustenido
            sharps[i].draw(parentView: parentView, parameters: parameters, state: state)
            
            xIni += space
        }
        
        xIni = position.origin.x
        
        for i in 0..<flats.count {
            let posBemol = CGRect(x: xIni, y: position.origin.y - (flats[i].tone.height * space), width: 2 * space, height: 4.5 * space)
            flats[i].position = posBemol
            flats[i].draw(parentView: parentView, parameters: parameters, state: state)
            
            xIni += space
        }
    }
}

struct AccidentTone {
    var tone    : MidiCode = MidiCode.c0
    var sharp   : Bool     = false
    var flat    : Bool     = false
    var natural : Bool     = false
}

enum KeyType: Int {
    case C_Am    = 1
    case Cs_Asm  = 2
    case Db_BBm  = 3
    case D_Bm    = 4
    case Eb_Cm   = 5
    case E_Csm   = 6
    case F_Dm    = 7
    case Fs_Dsm  = 8
    case Gb_Ebm  = 9
    case G_Em    = 10
    case Ab_Fm   = 11
    case A_Fsm   = 12
    case Bb_Gm   = 13
    case B_Gsm   = 14
}
