//
//  DrawBaseNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawBaseNote {
    var type = TypeBaseNote.quarter
    var position = CGRect()
    
    private var adapter: AdapterBaseNote!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterBaseNote()
        }
        adapter.type  = type
        adapter.parameters = parameters
        adapter.state = state
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}

enum TypeBaseNote {
    case half
    case quarter
    case quarver
}
