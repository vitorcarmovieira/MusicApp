//
//  DrawMetronome.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawMetronome {
    var position = CGRect()
    var baseNote : DrawBaseNote       = DrawBaseNote()
    var value    : DrawMetronomeValue = DrawMetronomeValue()
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        let initialY: CGFloat = parameters.initialYBaseNote
        let space: CGFloat = parameters.spacing
        let heightNote: CGFloat = parameters.heightBaseNote
//        if state == .editable {
//            initialY *= 2
//            space *= 2
//            heightNote *= 2
//        }
        
        let positionBN = CGRect(x: parameters.leftBorder, y: initialY, width: space * 3.0, height: heightNote)
        baseNote.position = positionBN
        baseNote.draw(parentView: parentView, parameters: parameters, state: state)
        
        let positionMet = CGRect(x: parameters.leftBorder + (3 * space), y: initialY, width: space * 10.0, height: heightNote)
        value.position = positionMet

        value.draw(parentView: parentView, parameters: parameters, state: state)
    }
}
