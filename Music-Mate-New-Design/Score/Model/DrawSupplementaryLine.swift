//
//  DrawSupplementaryLine.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawSupplementaryLine {
    var position = CGRect()
    var tone : DrawTone = DrawTone()
    var numberLines = 0
    var numberOfLineSup : Int {
        switch tone.clef {
        case .SOL:
            return devolveLinhasSuperioresSol()
        case .FA:
            return devolveLinhasSuperioresFa()
        case .DO:
            return devolveLinhasSuperioresSol()
        default:
            return 0
        }
    }
    
    var numberOfLineInf : Int {
        switch tone.clef {
        case .SOL:
            return devolveLinhasInferioresSol()
        case .FA:
            return devolveLinhasInferioresFa()
        case .DO:
            return devolveLinhasInferioresSol()
        default:
            return 0
        }
    }
    
    private var adapter: AdapterSupplementaryLine!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterSupplementaryLine()
        }
        adapter.parameters = parameters
        adapter.state = state
        adapter.numberLines = numberLines
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    func devolveLinhasSuperioresSol() -> Int {
        switch tone.midi {
        case .a5:
            return 1
        case .bB5, .b5:
            return 1
            
        case .c6, .cS6:
            return 2
        case .d6:
            return 2
        case .eB6, .e6:
            return 3
        case .f6, .fS6:
            return 3
        case .g6, .gS6:
            return 4
        case .a6:
            return 4
        case .bB6, .b6:
            return 5
            
        case .c7, .cS7:
            return 5
        case .d7:
            return 6
        case .eB7, .e7:
            return 6
        case .f7, .fS7:
            return 7
        case .g7, .gS7:
            return 7
        case .a7:
            return 8
        case .bB7, .b7:
            return 8
            
        case .c8, .cS8:
            return 9
        case .d8:
            return 9
        case .eB8, .e8:
            return 10
        case .f8, .fS8:
            return 10
        case .g8, .gS8:
            return 11
        case .a8:
            return 11
        case .bB8, .b8:
            return 12
            
        default:
            break
        }
        
        return 0
    }
    
    func devolveLinhasSuperioresFa() -> Int {
        switch tone.midi {
        case .c4, .cS4:
            return 1
        case .d4:
            return 1
        case .eB4, .e4:
            return 2
        case .f4, .fS4:
            return 2
        case .g4, .gS4:
            return 3
        case .a4:
            return 3
        case .bB4, .b4:
            return 4
            
        case .c5, .cS5:
            return 4
        case .d5:
            return 5
        case .eB5, .e5:
            return 5
        case .f5, .fS5:
            return 6
        case .g5, .gS5:
            return 6
        case .a5:
            return 7
        case .bB5, .b5:
            return 7
            
        case .c6, .cS6:
            return 8
        case .d6:
            return 8
        case .eB6, .e6:
            return 9
        case .f6, .fS6:
            return 9
        case .g6, .gS6:
            return 10
        case .a6:
            return 11
        case .bB6, .b6:
            return 12
            
        case .c7, .cS7:
            return 12
        case .d7:
            return 13
        case .eB7, .e7:
            return 13
        case .f7, .fS7:
            return 14
        case .g7, .gS7:
            return 14
        case .a7:
            return 15
        case .bB7, .b7:
            return 15
            
        case .c8, .cS8:
            return 16
        case .d8:
            return 16
        case .eB8, .e8:
            return 17
        case .f8, .fS8:
            return 17
        case .g8, .gS8:
            return 18
        case .a8:
            return 18
        case .bB8, .b8:
            return 19
            
        default:
            break
        }
        
        return 0
    }
    
    func devolveLinhasInferioresSol() -> Int {
        switch tone.midi {
            
        case .c0, .cS0:
            return 15
        case .d0:
            return 14
        case .eB0, .e0:
            return 14
        case .f0, .fS0:
            return 13
        case .g0, .gS0:
            return 13
        case .a0:
            return 12
        case .bB0, .b0:
            return 12
            
        case .c1, .cS1:
            return 11
        case .d1:
            return 11
        case .eB1, .e1:
            return 10
        case .f1, .fS1:
            return 10
        case .g1, .gS1:
            return 9
        case .a1:
            return 9
        case .bB1, .b1:
            return 8
            
        case .c2, .cS2:
            return 8
        case .d2:
            return 7
        case .eB2, .e2:
            return 7
        case .f2, .fS2:
            return 6
        case .g2, .gS2:
            return 6
        case .a2:
            return 5
        case .bB2, .b2:
            return 5
            
        case .c3, .cS3:
            return 4
        case .d3:
            return 4
        case .eB3, .e3:
            return 3
        case .f3, .fS3:
            return 3
        case .g3, .gS3:
            return 2
        case .a3:
            return 2
        case .bB3, .b3:
            return 1
            
        case .c4, .cS4:
            return 1
            
        default:
            break
        }
        
        return 0
    }
    
    func devolveLinhasInferioresFa() -> Int {
        switch tone.midi {
        case .c2, .cS2:
            return 2
        case .d2:
            return 1
        case .eB2, .e2:
            return 1
            
        case .c1, .cS1:
            return 5
        case .d1:
            return 5
        case .eB1, .e1:
            return 4
        case .f1, .fS1:
            return 4
        case .g1, .gS1:
            return 3
        case .a1:
            return 3
        case .bB1, .b1:
            return 2
            
        case .c0, .cS0:
            return 9
        case .d0:
            return 8
        case .eB0, .e0:
            return 8
        case .f0, .fS0:
            return 7
        case .g0, .gS0:
            return 7
        case .a0:
            return 6
        case .bB0, .b0:
            return 6
            
            
        default:
            break
        }
        
        return 0
    }
}
