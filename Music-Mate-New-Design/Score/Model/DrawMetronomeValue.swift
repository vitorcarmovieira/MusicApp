//
//  DrawMetronomeValue.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/8/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DrawMetronomeValue {
    var value = 60
    
    var position = CGRect()
    
    var adapter: AdapterMetronome!
    
    func draw(parentView: ViewPageScore, parameters: DrawParameter, state: ScoreState) {
        if adapter == nil {
            adapter = AdapterMetronome()
        }
        adapter.value      = value
        adapter.parameters = parameters
        adapter.state = state
        adapter.adapt(parentView: parentView, frame: position)
    }
    
    deinit {
        adapter = nil
    }
}
