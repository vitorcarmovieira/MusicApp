//
//  AdapterPlace.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/5/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
class AdapterPlace {
    
    var number = 0
    var view: ViewPlace!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var initialTime = 0
    var channel     = 1
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewPlace(frame: frame)
            v.channel = channel
            v.initialTime = initialTime
            if number % 2 == 0 {
                v.backgroundColor = UIColor(red: 201/255, green: 214/255, blue: 247/255, alpha: 0.1)
            } else {
                v.backgroundColor = UIColor(red: 119/255, green: 151/255, blue: 198/255, alpha: 0.1)
            }
            
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
}
