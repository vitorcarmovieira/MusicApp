//
//  AdapterPageNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterPageNumber {
    var view: ViewPageNumber!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var number = 1
    var total  = 1
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewPageNumber(frame: frame)
            v.backgroundColor = UIColor.clear
            v.number = number
            v.total = total
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageNumber")
    }
}
