//
//  AdapterPause.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterPause {
    var view: ViewPause!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var channel  = 1
    var duration = 960
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            let v = ViewPause(frame: frame)
            v.backgroundColor = UIColor.clear
            v.spacing = space
            v.channel = channel
            v.duration = duration
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        view.duration = duration
        parentView.sendSubview(toBack: view)
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageNumber")
    }
}
