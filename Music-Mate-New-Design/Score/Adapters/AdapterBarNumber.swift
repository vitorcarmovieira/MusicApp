//
//  AdapterBarNumber.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterBarNumber {
    var view: ViewBarNumber!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var bar = 0
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        var space = parameters.spacing
        if state == .editable {
            space = parameters.spacingEdit
        }
        
        let x = frame.origin.x - (1.0 * space)
        let y = frame.origin.y + (2.0 * space)
        let comprimento = 3.0 * space
        let largura = 3.0 * space
        let posNumber = CGRect(x: x, y: y, width: comprimento, height: largura)
        
        if view == nil || view.superview == nil || state == .editable {
            let v = ViewBarNumber(frame: posNumber)
            lastFrame = posNumber
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = posNumber
                lastFrame = posNumber
            }
        }
        
        view.bar = bar
    }
}

