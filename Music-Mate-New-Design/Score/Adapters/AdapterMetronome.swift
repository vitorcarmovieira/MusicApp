//
//  AdapterMetronome.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterMetronome {
    var view: ViewMetronome!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var value = 60
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame

            let v = ViewMetronome(frame: frame)
            v.backgroundColor = UIColor.clear
            v.value           = value
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterMetronome")
    }
}
