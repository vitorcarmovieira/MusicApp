//
//  AdapterAuthor.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 2/23/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterAuthor {
    var view: ViewComposer!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var composer = "No Composer"
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewComposer(frame: frame)
            v.backgroundColor = UIColor.clear
            if composer == "" {
                v.composer = "No Author"
            } else {
                v.composer = composer
            }
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
        //        print("Entrou deinit AdapterTitle")
    }
}
