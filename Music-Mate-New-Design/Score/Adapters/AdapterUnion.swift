//
//  AdapterUnion.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/12/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterUnion {
    var view: ViewUnion!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var type = TypeUnion.increasing
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewUnion(frame: frame)
            v.backgroundColor = UIColor.clear
            v.type = type
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageNumber")
    }
}
