//
//  AdapterRepetition.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/18/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterRepetition {
    var view: ViewRepetition!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var type = TypeRepetition.start
    var numberOfPentagrams = 2
    var bar = 0
    
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
    
            let v = ViewRepetition(frame: frame)
            v.bar       = bar
            v.position  = frame
            v.spacing   = space
            v.type      = type
            v.numberOfClefs = numberOfPentagrams
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        parentView.bringSubview(toFront: view)
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterRepetition")
    }
}
