//
//  AdapterBar.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterBar {
    var view: ViewBar!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var bar   = 0
    var start = 0
    var end   = 0
    
    func adapt(parentView: ViewPageScore, frame: CGRect, startLine: Bool) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            let v = ViewBar(frame: frame)
            v.position  = frame
            v.spacing   = space
            v.startLine = startLine
            v.bar       = bar
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                var space = parameters.spacing
                if state == .editable {
                    space = parameters.spacingEdit
                }
                view.frame     = frame
                lastFrame      = frame
                view.position  = frame
                view.spacing   = space
                view.startLine = startLine
                view.bar       = bar
            }
        }
        
        view.start = self.start
        view.end   = self.end
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageScore")
    }
}
