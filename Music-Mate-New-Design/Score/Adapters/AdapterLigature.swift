//
//  AdapterLigature.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterLigature {
    var view: ViewLigature!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var direction = LigatureDirection.toUp
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewLigature(frame: frame)
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            v.space = space
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        view.direction = direction
    }
}
