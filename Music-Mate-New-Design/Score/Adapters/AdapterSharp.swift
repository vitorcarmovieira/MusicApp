//
//  AdapterSharp.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterSharp {

    var view: ViewSharp!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewSharp(frame: frame)
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            v.space = space
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }

}
