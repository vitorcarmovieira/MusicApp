//
//  AdapterBracket.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterBracket {
    var view: ViewBracket!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var duration   = 960
    var state      = ScoreState.playing
    var channel    = 1
    var turn = false
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            let v = ViewBracket(frame: frame)
            v.channel   = channel
            v.spacing   = space

            v.backgroundColor = UIColor.clear
            v.duration = duration
            
//            if channel == 2 || channel == 4 {
            if turn {
//                v.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi) * (-1))
                v.transform = CGAffineTransform(scaleX: 1.0, y: -1.0)
            }
            
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterBracket")
    }
}
