//
//  AdapterNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/10/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterNote {
    var view: ViewNote!
    var lastFrame    = CGRect()
    var parameters   = DrawParameter()
    var duration     = 960
    var itQuantized  = 0
    var realInitNote = 0
    var tone         = 12
    var state        = ScoreState.playing
    var bar          = 0
    var posTone      = 0
    var hasSharp     = false
    var hasFlat      = false
    var channel      = 1
    var color: UIColor = UIColor.black
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
//        let textLayer = CATextLayer()
//        textLayer.frame = CGRect(x: frame.origin.x + (frame.height * 0.06), y: frame.origin.y + (frame.height / 3.6), width: frame.width, height: frame.height)
//
//        textLayer.string = "J"
//        
//        // 3
//        textLayer.font = CTFontCreateWithName("Typeface" as CFString, 12, nil)
//        textLayer.fontSize = frame.height / 2.2
//        // 4
//        textLayer.foregroundColor = UIColor.black.cgColor
////        textLayer.isWrapped = true
//        textLayer.alignmentMode = kCAAlignmentLeft
////        textLayer.contentsScale = UIScreen.main.scale
//        parentView.layer.addSublayer(textLayer)
        
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame

            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            let v = ViewNote(frame: frame)
            v.spacing   = space
            v.backgroundColor = UIColor.clear
            v.duration        = duration
            v.itQuantized     = itQuantized
            v.bar             = bar
            v.tone            = tone
            v.positionTone    = posTone
            v.hasFlat         = hasFlat
            v.hasSharp        = hasSharp
            v.realInitNote    = realInitNote
            v.channel         = channel
            v.color           = color
            
            parentView.addSubview(v)
            view = v
//            parentView.setNeedsDisplay()
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        view.duration = duration
        parentView.bringSubview(toFront: view)
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterNote")
    }
}
