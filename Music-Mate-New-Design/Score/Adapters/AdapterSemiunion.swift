//
//  AdapterSemiunioin.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/13/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterSemiunion {
    
    var view: ViewSemiunion!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var type = FormatSemiunion.increasing
    var pIni = CGPoint()
    var pEnd = CGPoint()
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewSemiunion(frame: frame)
            v.backgroundColor = UIColor.clear
            v.pIni = pIni
            v.pEnd = pEnd
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
}
