//
//  AdapterBarTime.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/11/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterBarTime {
    var view: ViewBarTime!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var numerator   = 4
    var denominator = 4
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            let v = ViewBarTime(frame: frame)
            v.backgroundColor = UIColor.clear
            v.numerator   = numerator
            v.denominator = denominator
            v.spacing = space
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageNumber")
    }
}
