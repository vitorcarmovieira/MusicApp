//
//  AdapterSubtitle.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 4/5/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterSubtitle {
    var view: ViewSubtitle!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var subtitle = ""
    var state = ScoreState.playing
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewSubtitle(frame: frame)
            v.backgroundColor = UIColor.clear
            if subtitle == "" {
                v.subtitle = ""
            } else {
                v.subtitle = subtitle
            }
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
        //        print("Entrou deinit AdapterTitle")
    }
}
