//
//  AdapterChannelArea.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/21/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterChannelArea {
    var view: ViewChannelArea!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var places : [DrawPlace] = []
    
    private var handView  = UIImageView()
    private var closeView = UIButton()
    var line  = DrawChannelLine()
    var line2 = DrawChannelLine()
    var placesTones          : [MidiCode] = []
    var placesPosition       : [CGFloat]    = []
    var placesTones2         : [MidiCode] = []
    
    var placesTonesCh12      : [MidiCode] = []
    var placesTonesCh34      : [MidiCode] = []
    
    var placesTonesCh12Midle : [MidiCode] = []
    var placesTonesCh34Midle : [MidiCode] = []
    
    var placesPosition2      : [CGFloat]    = []
    var type                 : TypeCanalization = TypeCanalization.oneTree
    
    private var initialPoint  : CGPoint = CGPoint()
    private var lastPoint     : CGPoint = CGPoint()
    private var currentPoint  : CGPoint = CGPoint()
    private var initialCenter : CGPoint = CGPoint()
    private var lastPlace     : Int     = 0
    private var newPointIni   : CGPoint = CGPoint(x: 0, y: 0)
    private var secondTime = false
    
    func hiddenViews() {
        view.isHidden      = true
        handView.isHidden  = true
        closeView.isHidden = true
    }
    
    func showViews() {
        view.isHidden      = false
        handView.isHidden  = false
        closeView.isHidden = false
    }
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            let v = ViewChannelArea(frame: frame)
            v.backgroundColor = UIColor(red: 119/255, green: 151/255, blue: 198/255, alpha: 0.5)
            parentView.addSubview(v)
            view = v
            
            UIGraphicsBeginImageContext(view.frame.size)
            
            let x = frame.origin.x + frame.width - 30
            let y = frame.origin.y + 5
            closeView.frame = CGRect(x: x, y: y, width: 25, height: 25)
            closeView.setImage(#imageLiteral(resourceName: "btn_canalize_close"), for: .normal)
            closeView.addTarget(self, action:#selector(eraseLine(gesto:)), for: .touchUpInside)
            
            parentView.addSubview(closeView)
            
            handView.frame = CGRect(x: 0, y: frame.origin.y + (frame.height / 2), width: 75, height: 85)
            handView.image = #imageLiteral(resourceName: "btn_canalize_hand")
            handView.backgroundColor = UIColor.clear
            handView.isUserInteractionEnabled = true
            
            parentView.addSubview(handView)
            
            addPanGesture(viewScore: parentView)
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        
//        placesTones.removeAll()
//        placesTones2.removeAll()
        
        placesTonesCh12.removeAll()
        placesTonesCh34.removeAll()
        placesTonesCh12Midle.removeAll()
        placesTonesCh34Midle.removeAll()
        
        for _ in 0..<placesPosition.count {
//            placesTones.append(MidiCode.none)
//            placesTones2.append(MidiCode.none)
            
            placesTonesCh12.append(MidiCode.none)
            placesTonesCh34.append(MidiCode.none)
            placesTonesCh12Midle.append(MidiCode.none)
            placesTonesCh34Midle.append(MidiCode.none)
        }
        
    }
    
    @objc func eraseLine(gesto: UITapGestureRecognizer) {
        secondTime = false
        newPointIni = CGPoint(x: 0, y: 0)
        handView.frame = CGRect(x: 0, y: lastFrame.origin.y + (lastFrame.height / 2), width: 75, height: 85)
        line.removePointsOfLine()
        line2.removePointsOfLine()
        view.imagem.removeFromSuperview()
        view.imagem = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: lastFrame.width, height: lastFrame.height))
        view.addSubview(view.imagem)
        
//        placesTones.removeAll()
//        placesTones2.removeAll()
        
        placesTonesCh12.removeAll()
        placesTonesCh34.removeAll()
        placesTonesCh12Midle.removeAll()
        placesTonesCh34Midle.removeAll()
        
        for _ in 0..<placesPosition.count {
//            placesTones.append(MidiCode.none)
//            placesTones2.append(MidiCode.none)
            
            placesTonesCh12.append(MidiCode.none)
            placesTonesCh34.append(MidiCode.none)
            placesTonesCh12Midle.append(MidiCode.none)
            placesTonesCh34Midle.append(MidiCode.none)
        }
        
        for i in 0..<places.count {
            for j in 0..<places[i].notes.count {
                if places[i].notes[j].usable {
                    if places[i].notes[j].adapter.view != nil {
                        if places[i].notes[j].usable && places[i].notes[j].joined {
                            places[i].notes[j].adapter.view.alpha = 1.0
                        }
                    }
                }
            }
        }
        
        lastPlace = 0
        
        drawLine()
    }
    
    func addPanGesture(viewScore: ViewPageScore) {
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(sender:)))
        handView.addGestureRecognizer(pan)

        UIApplication.shared.keyWindow!.bringSubview(toFront: handView)
    }
    
    func moveHandToStart() {
        lastPlace = 0
        newPointIni = CGPoint(x: 0, y: 0)
        handView.frame = CGRect(x: 0, y: lastFrame.origin.y + (lastFrame.height / 2), width: 75, height: 85)
    }
    
    @objc func handlePan(sender: UIPanGestureRecognizer) {
        
        let fileView = sender.view!
        let translation = sender.translation(in: fileView)
        
        switch sender.state {
            
        case .began:
            self.initialPoint = CGPoint(x: fileView.center.x - lastFrame.origin.x, y: fileView.center.y - lastFrame.origin.y - 35)
        case .changed:
            var y = self.lastPoint.y + translation.y
            
            if y < lastFrame.origin.y {
                y = 0
            }
            if y > lastFrame.origin.y + fileView.frame.height {
                y = fileView.frame.height
            }
            
            self.currentPoint = CGPoint(x: self.initialPoint.x + translation.x, y: self.initialPoint.y + translation.y)
            
            if self.type == .hMeio && secondTime {
                if (line2.addPoint(point: self.currentPoint)) {
                    drawLine()
                    
                    self.lastPoint = self.currentPoint
                    let point = self.currentPoint.x + view.frame.origin.x
                    
                    if (lastPlace + 1) < placesPosition.count {
                        for i in (lastPlace + 1)..<placesPosition.count {
                            if point > placesPosition[i] && self.placesTonesCh12[i] == MidiCode.none {
                                if self.type == .oneTree || self.type == .hMeio {
                                    self.placesTonesCh12[i] = getToneOneTree2(y: currentPoint.y)
                                }
                                if self.type == .oneTwo {
                                    self.placesTonesCh12[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                if self.type == .treeFour {
                                    self.placesTonesCh12[i] = getToneTreeFour(y: currentPoint.y)
                                }
                                if self.type == .hAcima {
                                    self.placesTonesCh12[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                
                                lastPlace = i
                            }
                            
                            if point > placesPosition[i] && self.placesTonesCh34[i] == MidiCode.none {
                                if self.type == .oneTree || self.type == .hMeio {
                                    self.placesTonesCh34Midle[i] = getToneOneTree2(y: currentPoint.y)
                                }
                                if self.type == .oneTwo {
                                    self.placesTonesCh34Midle[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                if self.type == .treeFour {
                                    self.placesTonesCh34Midle[i] = getToneTreeFour(y: currentPoint.y)
                                }
                                if self.type == .hAcima {
                                    self.placesTonesCh34Midle[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                
                                lastPlace = i
                            }

                        }
                    }
                }
            } else {
                if (line.addPoint(point: self.currentPoint)) {
                    drawLine()
                    
                    self.lastPoint = self.currentPoint
                    let point  = self.currentPoint.x + view.frame.origin.x

                    if (lastPlace + 1) < placesPosition.count {
                        for i in (lastPlace + 1)..<placesPosition.count {
                            if point >= placesPosition[i] && self.placesTonesCh12[i] == MidiCode.none {
                                if self.type == .oneTree || self.type == .hMeio {
                                    self.placesTonesCh12[i] = getToneOneTree(y: currentPoint.y)
                                }
                                if self.type == .oneTwo {
                                    self.placesTonesCh12[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                if self.type == .treeFour {
                                    self.placesTonesCh12[i] = getToneTreeFour(y: currentPoint.y)
                                }
                                if self.type == .hAcima {
                                    self.placesTonesCh12[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                
                                lastPlace = i
                            }
                            
                            if point >= placesPosition[i] && self.placesTonesCh34[i] == MidiCode.none {
                                if self.type == .oneTree || self.type == .hMeio {
                                    self.placesTonesCh34[i] = getToneOneTree2(y: currentPoint.y)
                                }
                                if self.type == .oneTwo {
                                    self.placesTonesCh34[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                if self.type == .treeFour {
                                    self.placesTonesCh34[i] = getToneTreeFour(y: currentPoint.y)
                                }
                                if self.type == .hAcima {
                                    self.placesTonesCh34[i] = getToneOneTwo(y: currentPoint.y)
                                }
                                
                                lastPlace = i
                            }
                        }
                    }
                }
            }
            fileView.center = CGPoint(x: self.currentPoint.x + lastFrame.origin.x, y: self.currentPoint.y + lastFrame.origin.y + 40)
            
            self.newPointIni = fileView.center
        case .ended:
            var lAtual = 0
            
            if self.type == .hMeio && !secondTime {
                moveHandToStart()
                
            } else if self.type == .hMeio && secondTime {
                for i in 0..<places.count {
                    for k in 0..<placesPosition.count {
                        if places[i].position.origin.x == placesPosition[k] {
                            lAtual = k
                        }
                    }
                    for j in 0..<places[i].notes.count {
                        let maxTone12 = max(placesTonesCh12[lAtual].rawValue, placesTonesCh12Midle[lAtual].rawValue)
                        let minTone12 = min(placesTonesCh12[lAtual].rawValue, placesTonesCh12Midle[lAtual].rawValue)
                        let maxTone34 = max(placesTonesCh34[lAtual].rawValue, placesTonesCh34Midle[lAtual].rawValue)
                        let minTone34 = min(placesTonesCh34[lAtual].rawValue, placesTonesCh34Midle[lAtual].rawValue)
                        
//                        let maxTone = max(placesTones[lAtual].rawValue, placesTones2[lAtual].rawValue)
//                        let minTone = min(placesTones[lAtual].rawValue, placesTones2[lAtual].rawValue)
                        
                        let valid12 = places[i].notes[j].tone.midi.rawValue >= minTone12 && places[i].notes[j].tone.midi.rawValue <= maxTone12
                        let valid34 = places[i].notes[j].tone.midi.rawValue >= minTone34 && places[i].notes[j].tone.midi.rawValue <= maxTone34
                        
//                        let valid = places[i].notes[j].tone.midi.rawValue >= minTone && places[i].notes[j].tone.midi.rawValue <= maxTone

                        places[i].channelTone12      = MidiCode(rawValue: minTone12)!
                        places[i].channelTone12Midle = MidiCode(rawValue: maxTone12)!
                        places[i].channelTone34      = MidiCode(rawValue: minTone34)!
                        places[i].channelTone34Midle = MidiCode(rawValue: maxTone34)!
                        
                        
//                        places[i].channelTone  = MidiCode(rawValue: minTone)!
//                        places[i].channelTone2 = MidiCode(rawValue: maxTone)!
                        
                        let valid = (places[i].channel == 1 && valid12) || (places[i].channel == 2 && valid12) || (places[i].channel == 3 && valid34) || (places[i].channel == 4 && valid34)
                        
                        if valid && places[i].notes[j].usable {
                            if places[i].notes[j].adapter.view != nil {
                                if places[i].notes[j].usable && places[i].notes[j].joined {
                                    let isChannel13 = self.type == .oneTree
                                    let isChannel1 = self.type == .oneTwo && places[i].channel == 1
                                    let isChannel2 = self.type == .oneTwo && places[i].channel == 2
                                    let isChannel3 = self.type == .treeFour && places[i].channel == 3
                                    let isChannel4 = self.type == .treeFour && places[i].channel == 4
                                    
                                    if isChannel13 || isChannel1 || isChannel2 || isChannel3 || isChannel4 {
                                        places[i].notes[j].adapter.view.alpha = 0.3
                                    } else if self.type == .hMeio {
                                        if valid12 || valid34 {
                                            places[i].notes[j].adapter.view.alpha = 0.3
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                for i in 0..<places.count {
                    for k in 0..<placesPosition.count {
                        if Int(places[i].position.origin.x) == Int(placesPosition[k]) {
                            lAtual = k
                        }
                    }
                    
                    places[i].channelTone12 = placesTonesCh12[lAtual]
                    places[i].channelTone34 = placesTonesCh34[lAtual]
                    
                    for j in 0..<places[i].notes.count {
                        var valid12 = places[i].notes[j].tone.midi.rawValue <= placesTonesCh12[lAtual].rawValue
                        var valid34 = places[i].notes[j].tone.midi.rawValue <= placesTonesCh34[lAtual].rawValue

                        if self.type == .hAcima {
                            valid12 = places[i].notes[j].tone.midi.rawValue >= placesTonesCh12[lAtual].rawValue
                            valid34 = places[i].notes[j].tone.midi.rawValue >= placesTonesCh34[lAtual].rawValue
                        }

                        let valid = (places[i].channel == 1 && valid12) || (places[i].channel == 2 && valid12) || (places[i].channel == 3 && valid34) || (places[i].channel == 4 && valid34)
                        
                        if valid && places[i].notes[j].usable {
                            if places[i].notes[j].adapter.view != nil {
                                if places[i].notes[j].usable && places[i].notes[j].joined {
                                    let isChannel13 = self.type == .oneTree
                                    let isChannel1 = self.type == .oneTwo && places[i].channel == 1
                                    let isChannel2 = self.type == .oneTwo && places[i].channel == 2
                                    let isChannel3 = self.type == .treeFour && places[i].channel == 3
                                    let isChannel4 = self.type == .treeFour && places[i].channel == 4
                                    
                                    if isChannel13 || isChannel1 || isChannel2 || isChannel3 || isChannel4 {
                                        places[i].notes[j].adapter.view.alpha = 0.3
                                    }
                                }
                            }
                        }
                    }
                }
            }

            secondTime = true
        default:
            break
        }
        
    }
    
    func paintNotes(currentPlace: Int) {
        
    }
    func drawLine() {
        UIGraphicsBeginImageContext(lastFrame.size)
        self.view.imagem.image?.draw(in: lastFrame)
        
        if let context = UIGraphicsGetCurrentContext() {
            var unions = line.unionOfPoints()
            
            for i in 0..<unions.count {
                if unions[i].count == 2 {
                    context.move(to: unions[i][0])
                    context.addLine(to: unions[i][1])
                }
            }
            
            if secondTime {
                unions = line2.unionOfPoints()
                for i in 0..<unions.count {
                    if unions[i].count == 2 {
                        context.move(to: unions[i][0])
                        context.addLine(to: unions[i][1])
                    }
                }
            }
 
            context.setBlendMode(CGBlendMode.normal)
            context.setLineCap(CGLineCap.round)
            context.setLineWidth(1)
            context.setStrokeColor(UIColor.red.cgColor)
            
            context.strokePath()
            
            view.imagem.image = UIGraphicsGetImageFromCurrentImageContext()!
            
        }
        UIGraphicsEndImageContext()
    }
    
    func getToneOneTree(y: CGFloat) -> MidiCode {
        let valor = y / parameters.spacingEdit
        
        if valor < 0.5 {
            return MidiCode.eB5
        } else if valor >= 0.5 && valor < 1.0 {
            return MidiCode.d5
        } else if valor >= 1.0 && valor < 1.5 {
            return MidiCode.c5
        } else if valor >= 1.5 && valor < 2.0 {
            return MidiCode.bB4
        } else if valor >= 2.0 && valor < 2.5 {
            return MidiCode.a4
        } else if valor >= 2.5 && valor < 3.0 {
            return MidiCode.g4
        } else if valor >= 3.0 && valor < 3.5 {
            return MidiCode.f4
        } else if valor >= 3.5 && valor < 4.0 {
            return MidiCode.eB4
        } else if valor >= 4.0 && valor < 4.5 {
            return MidiCode.d4
        } else if valor >= 4.5 && valor < 5.0 {
            return MidiCode.c4
        } else if valor >= 5.0 && valor < 5.5 {
            return MidiCode.bB3
        } else if valor >= 5.5 && valor < 6.0 {
            return MidiCode.a3
        } else if valor >= 6.0 && valor < 6.5 {
            return MidiCode.g3
        } else if valor >= 6.5 && valor < 7.0 {
            return MidiCode.f3
        } else if valor >= 7.0 && valor < 7.5 {
            return MidiCode.eB3
        } else if valor >= 7.5 && valor < 8.0 {
            return MidiCode.d3
        } else if valor >= 8.0 && valor < 8.5 {
            return MidiCode.c3
        } else if valor >= 8.5 && valor < 9.0 {
            return MidiCode.bB2
        } else if valor >= 9.0 && valor < 9.5 {
            return MidiCode.a2
        } else if valor >= 9.5 && valor < 10.0 {
            return MidiCode.g2
        } else if valor >= 10.0 && valor < 10.5 {
            return MidiCode.f2
        } else if valor >= 10.5 && valor < 11.0 {
            return MidiCode.eB2
        } else if valor >= 11.0 && valor < 11.5 {
            return MidiCode.d2
        } else if valor >= 11.5 && valor < 12.0 {
            return MidiCode.c2
        }
        

        return MidiCode.c2
    }
    
    func getToneOneTree2(y: CGFloat) -> MidiCode {
        let valor = y / parameters.spacingEdit
        
        if valor < 0.5 {
            return MidiCode.a6
        }else if valor >= 0.5 && valor < 1.0 {
             return MidiCode.g6
        } else if valor >= 1.0 && valor < 1.5 {
            return MidiCode.f6
        } else if valor >= 1.5 && valor < 2.0 {
            return MidiCode.eB6
        } else if valor >= 2.0 && valor < 2.5 {
            return MidiCode.d6
        } else if valor >= 2.5 && valor < 3.0 {
            return MidiCode.c6
        } else if valor >= 3.0 && valor < 3.5 {
            return MidiCode.bB5
        } else if valor >= 3.5 && valor < 4.0 {
            return MidiCode.a5
        } else if valor >= 4.0 && valor < 4.5 {
            return MidiCode.g5
        } else if valor >= 4.5 && valor < 5.0 {
            return MidiCode.f5
        } else if valor >= 5.0 && valor < 5.5 {
            return MidiCode.e5
        } else if valor >= 5.5 && valor < 6.0 {
            return MidiCode.d5
        } else if valor >= 6.0 && valor < 6.5 {
            return MidiCode.c5
        } else if valor >= 6.5 && valor < 7.0 {
            return MidiCode.bB4
        } else if valor >= 7.0 && valor < 7.5 {
            return MidiCode.a4
        } else if valor >= 7.5 && valor < 8.0 {
            return MidiCode.g4
        } else if valor >= 8.0 && valor < 8.5 {
            return MidiCode.f4
        } else if valor >= 8.5 && valor < 9.0 {
            return MidiCode.e4
        } else if valor >= 9.0 && valor < 9.5 {
            return MidiCode.d4
        } else if valor >= 9.5 && valor < 10.0 {
            return MidiCode.c4
        } else if valor >= 10.0 && valor < 10.5 {
            return MidiCode.bB3
        } else if valor >= 10.5 && valor < 11.0 {
            return MidiCode.a3
        } else if valor >= 11.0 && valor < 11.5 {
            return MidiCode.g3
        } else if valor >= 11.5 && valor < 12.0 {
            return MidiCode.f3
        } else if valor >= 12.0 && valor < 12.5 {
            return MidiCode.e3
        } else if valor >= 12.5 && valor < 13.0 {
            return MidiCode.d3
        } else if valor >= 13.0 && valor < 13.5 {
            return MidiCode.c3
        } else if valor >= 13.5 && valor < 14.0 {
            return MidiCode.bB2
        } else if valor >= 14.0 && valor < 14.5 {
            return MidiCode.a2
        } else if valor >= 14.5 {
            return MidiCode.g2
        }
        
        return MidiCode.g2
    }
    
    func getToneOneTwo(y: CGFloat) -> MidiCode {
        let valor = y / parameters.spacingEdit
        if valor < 0.5  {
            return MidiCode.c7
        }else if valor >= 0.5 && valor < 1.0 {
            return MidiCode.bB6
        }else if valor >= 1.0 && valor < 1.5 {
            return MidiCode.a6
        }else if valor >= 1.5 && valor < 2.0 {
            return MidiCode.g6
        }else if valor >= 2.0 && valor < 2.5 {
            return MidiCode.f6
        }else if valor >= 2.5 && valor < 3.0 {
            return MidiCode.eB6
        }else if valor >= 3.0 && valor < 3.5 {
            return MidiCode.d6
        }else if valor >= 3.5 && valor < 4.0 {
            return MidiCode.c6
        }else if valor >= 4.0 && valor < 4.5 {
            return MidiCode.bB5
        }else if valor >= 4.5 && valor < 5.0 {
            return MidiCode.a5
        }else if valor >= 5.0 && valor < 5.5 {
            return MidiCode.g5
        }else if valor >= 5.5 && valor < 6.0 {
            return MidiCode.f5
        } else if valor >= 6.0 && valor < 6.5 {
            return MidiCode.eB5
        } else if valor >= 6.5 && valor < 7.0 {
            return MidiCode.d5
        } else if valor >= 7.0 && valor < 7.5 {
            return MidiCode.c5
        } else if valor >= 7.5 && valor < 8.0 {
            return MidiCode.bB4
        } else if valor >= 8.0 && valor < 8.5 {
            return MidiCode.a4
        } else if valor >= 8.5 && valor < 9.0 {
            return MidiCode.g4
        } else if valor >= 9.0 && valor < 9.5 {
            return MidiCode.f4
        } else if valor >= 9.5 && valor < 10.0 {
            return MidiCode.eB4
        } else if valor >= 10.0 && valor < 10.5 {
            return MidiCode.d4
        }else if valor >= 10.5 && valor < 11.0 {
            return MidiCode.c4
        }else if valor >= 11.0 && valor < 11.5 {
            return MidiCode.bB3
        }else if valor >= 11.5 && valor < 12.0 {
            return MidiCode.a3
        }else if valor >= 12.0 && valor < 12.5 {
            return MidiCode.g3
        }else if valor >= 12.5 && valor < 13.0 {
            return MidiCode.f3
        }else if valor >= 13.0 && valor < 13.5 {
            return MidiCode.eB3
        }else if valor >= 13.5 {
            return MidiCode.d3
        }

        return MidiCode.c4
    }
    
    //b3
    //a3 -------------
    //g3
    //f3 -------------
    //e3
    //d3 -------------
    //
    func getToneTreeFour(y: CGFloat) -> MidiCode {
        
        let valor = y / parameters.spacingEdit

        if valor < 0.5 {
            return MidiCode.d6
        } else if valor >= 0.5 && valor < 1.0 {
            return MidiCode.c6
        } else if valor >= 1.0 && valor < 1.5 {
            return MidiCode.bB5
        } else if valor >= 1.5 && valor < 2.0 {
            return MidiCode.a5
        } else if valor >= 2.0 && valor < 2.5 {
            return MidiCode.g5
        } else if valor >= 2.5 && valor < 3.0 {
            return MidiCode.f5
        } else if valor >= 3.0 && valor < 3.5 {
            return MidiCode.eB5
        } else if valor >= 3.5 && valor < 4.0 {
            return MidiCode.d5
        } else if valor >= 4.0 && valor < 4.5 {
            return MidiCode.c5
        } else if valor >= 4.5 && valor < 5.0 {
            return MidiCode.bB4
        } else if valor >= 5.0 && valor < 5.5 {
            return MidiCode.a4
        } else if valor >= 5.5 && valor < 6.0 {
            return MidiCode.g4
        } else if valor >= 6.0 && valor < 6.5 {
            return MidiCode.f4
        } else if valor >= 6.5 && valor < 7.0 {
            return MidiCode.eB4
        } else if valor >= 7.0 && valor < 7.5 {
            return MidiCode.d4
        } else if valor >= 7.5 && valor < 8.0 {
            return MidiCode.c4
        } else if valor >= 8.0 && valor < 8.5 {
            return MidiCode.bB3
        } else if valor >= 8.5 && valor < 9.0 {
            return MidiCode.a3
        } else if valor >= 9.0 && valor < 9.5 {
            return MidiCode.g3
        } else if valor >= 9.5 && valor < 10.0 {
            return MidiCode.f3
        } else if valor >= 10.0 && valor < 10.5 {
            return MidiCode.eB3
        } else if valor >= 10.5 && valor < 11.0 {
            return MidiCode.d3
        } else if valor >= 11.0 && valor < 11.5 {
            return MidiCode.c3
        } else if valor >= 11.5 && valor < 12.0 {
            return MidiCode.bB2
        } else if valor >= 12.0 && valor < 12.5 {
            return MidiCode.a2
        } else if valor >= 12.5 && valor < 13.0 {
            return MidiCode.g2
        } else if valor >= 13.0 && valor < 13.5 {
            return MidiCode.f2
        } else if valor >= 13.5 && valor < 14.0 {
            return MidiCode.eB2
        } else if valor >= 14.0 && valor < 14.5 {
            return MidiCode.d2
        } else if valor >= 14.5 && valor < 15.0 {
            return MidiCode.c2
        } else if valor >= 15.0 && valor < 15.5 {
            return MidiCode.bB1
        } else if valor >= 15.5 && valor < 16.0 {
            return MidiCode.a1
        } else if valor >= 16.0 && valor < 16.5 {
            return MidiCode.g1
        } else if valor >= 16.5 && valor < 17.0 {
            return MidiCode.f1
        } else if valor >= 17.0 && valor < 17.5 {
            return MidiCode.eB1
        } else if valor >= 17.5 && valor < 18.0 {
            return MidiCode.d1
        } else if valor >= 18.0 && valor < 18.5 {
            return MidiCode.c1
        }
        
//        if valor < 0.5 {
//            return MidiCode.a4
//        }else if valor >= 0.5 && valor < 1.0 {
//            return MidiCode.g4
//        }else if valor >= 1.0 && valor < 1.5 {
//            return MidiCode.f4
//        }else if valor >= 1.5 && valor < 2.0 {
//            return MidiCode.eB4
//        }else if valor >= 2.0 && valor < 2.5 {
//            return MidiCode.d4
//        }else if valor >= 2.5 && valor < 3.0 {
//            return MidiCode.c4
//        }else if valor >= 3.0 && valor < 9.0 {
//            return MidiCode.bB3
//        }else if valor >= 9.0 && valor < 9.5 {
//            return MidiCode.a3
//        }else if valor >= 9.5 && valor < 10.0 {
//            return MidiCode.g3
//        }else if valor >= 10.0 && valor < 10.5 {
//            return MidiCode.f3
//        }else if valor >= 10.5 && valor < 11.0 {
//            return MidiCode.eB3
//        } else if valor >= 11.0 && valor < 11.5 {
//            return MidiCode.d3
//        } else if valor >= 11.5 && valor < 12.0 {
//            return MidiCode.c3
//        } else if valor >= 12.0 && valor < 12.5 {
//            return MidiCode.bB2
//        } else if valor >= 12.5 && valor < 13.0 {
//            return MidiCode.a2
//        } else if valor >= 13.0 && valor < 13.5 {
//            return MidiCode.g2
//        } else if valor >= 13.5 && valor < 14.0 {
//            return MidiCode.f2
//        } else if valor >= 14.0 && valor < 14.5 {
//            return MidiCode.eB2
//        } else if valor >= 14.5 && valor < 15.0 {
//            return MidiCode.d2
//        } else if valor >= 15.0 && valor < 16.0 {
//            return MidiCode.c2
//        }
        
        return MidiCode.c1
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterChannelArea")
    }
}
