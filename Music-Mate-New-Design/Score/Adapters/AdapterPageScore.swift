//
//  AdapterPageScore.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterPageScore {
    var view: ViewPageScore!
    var ultimoFrame = CGRect()
    var state = ScoreState.playing
    
    func adapt(parentView: UIView, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            ultimoFrame = frame
            let v = ViewPageScore(frame: frame)
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if ultimoFrame.origin.x != frame.origin.x || ultimoFrame.origin.y != frame.origin.y || ultimoFrame.width != frame.width || ultimoFrame.height != frame.height {
                view.frame = frame
                ultimoFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageScore")
    }
}
