//
//  AdapterPentagram.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/9/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterPentagram {
    var view: ViewPentagram!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var bar = 0
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            var space = parameters.spacing
            if state == .editable {
                space = parameters.spacingEdit
            }
            
            lastFrame = frame
            
            let v = ViewPentagram(frame: frame)
            v.spacing   = space
            v.bar = bar
            v.backgroundColor = UIColor.clear
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
        parentView.sendSubview(toBack: view)
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterPageScore")
    }
}
