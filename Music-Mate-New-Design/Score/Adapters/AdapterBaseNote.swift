//
//  AdapterBaseNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class AdapterBaseNote {
    var view: ViewBaseNote!
    var lastFrame  = CGRect()
    var parameters = DrawParameter()
    var state = ScoreState.playing
    var type = TypeBaseNote.quarter
    
    func adapt(parentView: ViewPageScore, frame: CGRect) {
        if view == nil || view.superview == nil || state == .editable {
            lastFrame = frame
            
            var space = parameters.spacing
            var edit = false
            if state == .editable {
                space = parameters.spacingEdit
                edit = true
            }
            
            let v = ViewBaseNote(frame: frame)
            v.spacing         = space
            v.backgroundColor = UIColor.clear
            v.typeNote        = type
            v.edit            = edit
            parentView.addSubview(v)
            view = v
        } else {
            if lastFrame.origin.x != frame.origin.x || lastFrame.origin.y != frame.origin.y || lastFrame.width != frame.width || lastFrame.height != frame.height {
                view.frame = frame
                lastFrame = frame
            }
        }
    }
    
    deinit {
        view = nil
//        print("Entrou deinit AdapterBaseNote")
    }
}
