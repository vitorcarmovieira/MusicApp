//
//  SplashViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    weak var viewImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let images: [UIImage] = [#imageLiteral(resourceName: "SplashScreen1"), #imageLiteral(resourceName: "SplashScreen2"), #imageLiteral(resourceName: "SplashScreen3"), #imageLiteral(resourceName: "SplashScreen4"), #imageLiteral(resourceName: "SplashScreen5"), #imageLiteral(resourceName: "SplashScreen6"), #imageLiteral(resourceName: "SplashScreen7"), #imageLiteral(resourceName: "SplashScreen8"), #imageLiteral(resourceName: "SplashScreen9"), #imageLiteral(resourceName: "SplashScreen10"), #imageLiteral(resourceName: "SplashScreen11"), #imageLiteral(resourceName: "SplashScreen12"), #imageLiteral(resourceName: "SplashScreen13"), #imageLiteral(resourceName: "SplashScreen14"), #imageLiteral(resourceName: "SplashScreen15"), #imageLiteral(resourceName: "SplashScreen16"), #imageLiteral(resourceName: "SplashScreen17"), #imageLiteral(resourceName: "SplashScreen18"), #imageLiteral(resourceName: "SplashScreen19"), #imageLiteral(resourceName: "SplashScreen20"), #imageLiteral(resourceName: "SplashScreen21"), #imageLiteral(resourceName: "SplashScreen22"), #imageLiteral(resourceName: "SplashScreen23"), #imageLiteral(resourceName: "SplashScreen24"), #imageLiteral(resourceName: "SplashScreen25"), #imageLiteral(resourceName: "SplashScreen26"), #imageLiteral(resourceName: "SplashScreen27"), #imageLiteral(resourceName: "SplashScreen28"), #imageLiteral(resourceName: "SplashScreen29"), #imageLiteral(resourceName: "SplashScreen30"), #imageLiteral(resourceName: "SplashScreen31"), #imageLiteral(resourceName: "SplashScreen32"), #imageLiteral(resourceName: "SplashScreen33"), #imageLiteral(resourceName: "SplashScreen34")]
        
        if viewImage == nil {
            let viewImg = UIImageView(frame: view.frame)
            view.addSubview(viewImg)
            
            viewImage = viewImg
        }
        
        viewImage.animationImages = images.reversed()
        viewImage.animationDuration = 1.1
        viewImage.animationRepeatCount = 1
        viewImage.startAnimating()
        UIView.animate(withDuration: 0.2, delay: 1.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.viewImage.alpha = 0.0
        }, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        sleep(1)

        let controller = HomeViewController()
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen

        self.present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        viewImage = nil
        print("Entrou deinit SplashViewController")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
