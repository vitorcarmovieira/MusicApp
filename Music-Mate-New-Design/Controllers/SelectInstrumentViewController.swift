//
//  SelectInstrumentViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/22/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

//var instrumentAppSelected  = ModelInstrument()
//var instrumentUserSelected = ModelInstrument()
//var typeInstAppSelected    = ModelTypeInstrument()
//var typeInstUserSelected   = ModelTypeInstrument()
//asda
class SelectInstrumentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var txtInstrument: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var btnDone: UIButton!
    let defaults = UserDefaults.standard
    @IBOutlet weak var instrumentTableView: UITableView!
    
    @IBOutlet weak var typeTableView: UITableView!
    
    var instrumentName = ["Piano", "Guitar"]
    var typeName : [String]       = []
    var categories: [ModelInstrument] = []
    var instruments: [ModelTypeInstrument] = []
    
    var isUser = false
    var isPartiture = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instrumentTableView.isHidden = true
        instrumentName.removeAll()
        
        
        instrumentTableView.delegate = self
        instrumentTableView.dataSource = self
        
        typeTableView.delegate = self
        typeTableView.dataSource = self
        
        let frameLblInstrument = CGRect(x: 0.0, y: 0.0, width: txtInstrument.frame.width * 0.2, height: txtInstrument.frame.height)
        let frameLblType       = CGRect(x: 0.0, y: 0.0, width: txtType.frame.width * 0.2, height: txtType.frame.height)
        
        let lblInstrument = UILabel(frame: frameLblInstrument)
        let lblType       = UILabel(frame: frameLblType)
        
        lblInstrument.textAlignment = .center
        lblType.textAlignment       = .center
        
        lblInstrument.textColor = UIColor.white
        lblType.textColor       = UIColor.white
        
        lblInstrument.font = UIFont(name: "Lato-Bold", size: 17)
        lblType.font       = UIFont(name: "Lato-Bold", size: 17)
        
        lblInstrument.text = "Category"
        lblType.text       = "Instrument"
        
        txtInstrument.leftViewMode = .always
        txtType.leftViewMode       = .always
        
        let vInst = UIView(frame: frameLblInstrument)
        let vType = UIView(frame: frameLblType)
        
        vInst.addSubview(lblInstrument)
        vType.addSubview(lblType)
        
        txtInstrument.leftView     = vInst
        txtType.leftView           = vType
        
        lblInstrument.backgroundColor = UIColor.clear
        lblType.backgroundColor       = UIColor.clear
        
        txtInstrument.leftView?.backgroundColor = UIColor(red: 10/255, green: 81/255, blue: 158/255, alpha: 1.0)
        txtType.leftView?.backgroundColor       = UIColor(red: 10/255, green: 81/255, blue: 158/255, alpha: 1.0)
        
        vInst.layer.cornerRadius = 10
        vType.layer.cornerRadius = 10
        
        typeTableView.isHidden       = true
        
        txtInstrument.addTarget(self, action: #selector(textFieldActive(_:)), for: .touchDown)
        txtType.addTarget(self, action: #selector(textFieldActive(_:)), for: .touchDown)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SelectInstrumentViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SelectInstrumentViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if isUser {
            if let user = DAOUser.getInstance().getUserActive() {
                txtInstrument.text = user.instrument.name
                txtType.text       =  user.typeInst.name
                typeName.removeAll()
                instruments = DAOTypeInstrument.getInstance().getTypesInstrumentsById(user.instrument.id)
                for instrument in instruments {
                    typeName.append(instrument.name)
                }
                
                btnDone.isEnabled = true
                instrumentTableView.isHidden = true
            }
        } else {
            txtInstrument.text = DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory.name
            txtType.text  =  DAOTypeInstrument.getInstance().getTypeInstrumentActive().name
            instruments = DAOTypeInstrument.getInstance().getTypesInstrumentsById(DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory.id)
            
            
            typeName.removeAll()
            
            instruments = DAOTypeInstrument.getInstance().getTypesInstrumentsById(DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory.id)
            
            for instrument in instruments {
                typeName.append(instrument.name)
            }
            
            txtInstrument.text = DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory.name
            txtType.text       = DAOTypeInstrument.getInstance().getTypeInstrumentActive().name
            btnDone.isEnabled = true
            instrumentTableView.isHidden = true
        }
        
        let user = UserManager.sharedUserManager
        let token = user.token
        user.server = "http://37.208.116.28"
        
        let instrumentsAddeds = DAOInstrument.getInstance().getAllInstruments()
        
        if instrumentsAddeds.count == 0 {
            self.serviceGetInstruments(urlToRequest: user.server + ":3003/instrument/all?token=" + token, method: ServiceRemote.Method.GET.rawValue)
        }
        
        categories = DAOInstrument.getInstance().getAllInstruments()
        print(categories.count)
        for category in categories {
            instrumentName.append(category.name)
        }
        instrumentTableView.reloadData()
        
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if txtInstrument.isFirstResponder || txtType.isFirstResponder {
            view.endEditing(true)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if txtInstrument.isFirstResponder || txtType.isFirstResponder {
            view.endEditing(true)
        }
    }
    
    @objc func textFieldActive(_ field: UITextField) {
        if field == txtInstrument {
            instrumentTableView.isHidden  = !instrumentTableView.isHidden
            typeTableView.isHidden = true
        }
        if field == txtType {
            typeTableView.isHidden = !typeTableView.isHidden
            instrumentTableView.isHidden  = true
        }
        view.endEditing(true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        txtInstrument.leftView?.layer.cornerRadius = 10
        //        txtType.leftView?.layer.cornerRadius       = 10
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////// Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == instrumentTableView){
            return instrumentName.count
        }
        else {
            return typeName.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == instrumentTableView){
            
            let cellInstrument = tableView.dequeueReusableCell(withIdentifier: "cell_Instrument", for: indexPath)
            cellInstrument.textLabel?.text = instrumentName[indexPath.row]
            
            
            return cellInstrument
        }
        else{
            
            let cellType = tableView.dequeueReusableCell(withIdentifier: "cell_Type", for: indexPath)
            cellType.textLabel?.text = typeName[indexPath.row]
            
            return cellType
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == instrumentTableView){
            txtInstrument.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
            
            typeName.removeAll()
            txtType.text = ""
            instruments = DAOTypeInstrument.getInstance().getTypesInstrumentsById(categories[indexPath.row].id)
            
            for instrument in instruments {
                typeName.append(instrument.name)
            }
            
            if isUser {
                if let user = DAOUser.getInstance().getUserActive() {
                    user.instrument = categories[indexPath.row]
                    DAOUser.getInstance().changeUser(model: user)
                }
            } else {
                for category in categories {
                    instrumentName.append(category.name)
                }
            }
            typeTableView.reloadData()
            
            //    btnDone.isEnabled = false
            
        }
            
        else {
            txtType.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
            if isUser {
                if let user = DAOUser.getInstance().getUserActive() {
                    user.typeInst = instruments[indexPath.row]
                    DAOUser.getInstance().changeUser(model: user)
                }
            } else {
                
                DAOTypeInstrument.getInstance().setInstrumentActive(model: instruments[indexPath.row])
                print("instrumento ativo: ", DAOTypeInstrument.getInstance().getTypeInstrumentActive().status)
                if DAOTypeInstrument.getInstance().getTypeInstrumentActive().clef == 3 || DAOTypeInstrument.getInstance().getTypeInstrumentActive().clef == 4 {
                    self.defaults.set(false, forKey: "Mono")
                } else {
                    self.defaults.set(true, forKey: "Mono")
                }
            }
            btnDone.isEnabled = true
        }
        tableView.isHidden = true
        
    }
    
    @IBAction func actionBtnDone(_ sender: UIButton) {
        
        if((txtType.text! == "") || (txtInstrument.text! == "")){
            let alert = UIAlertController(title: "", message: "Please select an Instrument", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion:nil)
        } else{
        
        if(UserDefaults.standard.integer(forKey: "login") == 0){
            let controller = HomeViewController()
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            //    controller.titleFooter = self.titleFooter
            self.present(controller, animated: true, completion: nil)
        }
                if isUser{
                    if let user = DAOUser.getInstance().getUserActive() {
                       // user.instrument =
                    }
        //        }else {
        //
        //}
    }
        }
        
    }
    
}

extension SelectInstrumentViewController{
    func serviceGetInstruments(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    print(data1)
                    if let data = data1["instruments"] as? [AnyObject] {
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            
                            
                            let instrument   = ModelInstrument()
                            instrument.id    = d["_id"] as! String
                            instrument.name  = d["category"] as! String
                            self.defaults.set(instrument.id, forKey: "idInstCat")
                            self.defaults.set(instrument.name, forKey: "nameInstCat")
                            
                            DAOInstrument.getInstance().addInstrument(instrument)
                            if let types = d["instruments"] as? [AnyObject] {
                                for type in types {
                                    let tp = type as! [String:AnyObject]
                                    let modelType             = ModelTypeInstrument()
                                    modelType.id              = tp["_id"] as! String
                                    modelType.name            = tp["name"] as! String
                                    modelType.instrumentCategory.id = d["_id"] as! String
                                    modelType.clef = tp["clef"] as! Int
                                    modelType.midiRangeFinal = tp["midiRangeFinal"] as! Int
                                    modelType.midiRangeInitial = tp["midiRangeInitial"] as! Int
                                    modelType.midiTransform = tp["midiTransform"] as! Int
                                    modelType.status = tp["status"] as! Int
                                    modelType.tonality = tp["tonality"] as! Int
                                    DAOTypeInstrument.getInstance().addInstrument(modelType)
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        self.categories = DAOInstrument.getInstance().getAllInstruments()
                        print(self.categories.count)
                        for category in self.categories {
                            self.instrumentName.append(category.name)
                        }
                        self.instrumentTableView.reloadData()
                        
                    })
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
}
