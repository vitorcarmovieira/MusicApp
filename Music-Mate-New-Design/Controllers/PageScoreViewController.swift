//
//  PageScoreViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/5/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class PageScoreViewController: UIViewController {

    var page: DrawPageScore!
    var isMenuNotes = false
    weak var score: DrawScore!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        if page == nil {
            page = DrawPageScore()
       }
        
        updateScore()
        
        if score != nil {
            score.verifyFunction()
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        updateScore()
    }
    
    func updateScore() {
        page.draw(parentView: view)
        
        if !isMenuNotes {
            page.hiddenPlaces()
        } else {
            page.showPlaces()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    deinit {
        page = nil
//        print("Entrou deinit PageScoreViewController")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
