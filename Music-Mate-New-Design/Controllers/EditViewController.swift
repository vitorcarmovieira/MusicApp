//
//  EditViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import Appsee

class EditViewController: UIPageViewController {

    weak var menu             : MenuEditView!
    weak var menuGeneral      : MenuGeneralEditView!
    weak var menuKey          : MenuKeyEdit!
    weak var menuCanalyse     : MenuCanalyseEdit!
    weak var menuRepetition   : MenuRepetitionView!
    weak var menuDSDC         : MenuDSDCEditView!
    weak var menuNotes        : MenuNotesEditView!
    weak var menuSelNotes     : MenuNotes!
    weak var menuToneNotes    : MenuTonesNotes!
    weak var menuChannelNotes : MenuChannelNote!
    weak var popRepManual     : PopUpRepetitionManual!
    weak var addInfo          : AddInfo!
    weak var footer           : Footer!
    
    var option = OptionMenuEdit.general
    
    var currentOption = OptionSelectedEdit.none
    
    var actualPage = 0
    
    var score: DrawScore!
    //ll
    lazy var controllers: [PageScoreViewController] = []
    
    var currentId = 0
    var toFront   = false
    
    var dsdc = ModelDaCapoDelSegno()
    var dsdcIsActive = false
    
    var historyScores: [DrawScore] = []
    var startBarRepManual = 0
    
    var countBarsScore = 0
    
    var helpView = HelpView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateMenu()
        
        updateScore()
        
        menuGeneral.isHidden      = false
        menuCanalyse.isHidden     = true
        menuKey.isHidden          = true
        menuRepetition.isHidden   = true
        menuDSDC.isHidden         = true
        menuNotes.isHidden        = true
        menuChannelNotes.isHidden = true
        
        view.backgroundColor = UIColor.white
        
        dataSource = self
        delegate   = self
        
//        if presentingViewController is ComposeViewController {
//            while score.bars.count < countBarsScore {
//                score.addBar()
//            }
//        }
    }
    

    @objc func keyboardWillShow(notification: NSNotification) {
        
        if addInfo.addNewComposerView != nil && addInfo.addNewComposerView != nil{
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if addInfo.txtDate.isFirstResponder || addInfo.txtTags.isFirstResponder {
                self.view.frame.origin = CGPoint(x: self.view.frame.origin.x, y: -keyboardSize.height)
            } else if !addInfo.txtTitle.isFirstResponder && !addInfo.txtSubtitle.isFirstResponder && !addInfo.txtDate.isFirstResponder && !addInfo.txtTags.isFirstResponder && !addInfo.addNewComposerView.firstNameTxtF.isFirstResponder && !addInfo.addNewComposerView.lastNameTxtF.isFirstResponder {
                view.endEditing(true)
                if self.view.frame.origin.y != 0 {
                    self.view.frame.origin.y = 0
                }
              }
            }
        } else {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if addInfo.txtDate.isFirstResponder || addInfo.txtTags.isFirstResponder {
                    self.view.frame.origin = CGPoint(x: self.view.frame.origin.x, y: -keyboardSize.height)
                } else if !addInfo.txtTitle.isFirstResponder && !addInfo.txtSubtitle.isFirstResponder && !addInfo.txtDate.isFirstResponder && !addInfo.txtTags.isFirstResponder {
                    if !popRepManual.lblNumber1.isFirstResponder && !popRepManual.lblNumber2.isFirstResponder {
                        view.endEditing(true)
                    }
                    if self.view.frame.origin.y != 0 {
                        self.view.frame.origin.y = 0
                    }
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if addInfo.addNewComposerView != nil && addInfo.addNewComposerView != nil{
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if addInfo.txtDate.isFirstResponder || addInfo.txtTags.isFirstResponder {
                self.view.frame.origin.y = 0
            } else if !addInfo.txtTitle.isFirstResponder && !addInfo.txtSubtitle.isFirstResponder && !addInfo.txtDate.isFirstResponder && !addInfo.txtTags.isFirstResponder && !addInfo.addNewComposerView.firstNameTxtF.isFirstResponder && !addInfo.addNewComposerView.lastNameTxtF.isFirstResponder {
                view.endEditing(true)
                if self.view.frame.origin.y != 0 {
                    self.view.frame.origin.y = 0
                }
            }
            }
            
        } else {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if addInfo.txtDate.isFirstResponder || addInfo.txtTags.isFirstResponder {
                    self.view.frame.origin.y = 0
                } else if !addInfo.txtTitle.isFirstResponder && !addInfo.txtSubtitle.isFirstResponder && !addInfo.txtDate.isFirstResponder && !addInfo.txtTags.isFirstResponder{
                    view.endEditing(true)
                    if self.view.frame.origin.y != 0 {
                        self.view.frame.origin.y = 0
                    }
                }
            }
        }
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        score.hiddenPlaces()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
//        for i in 0..<score.pages.count {
//            let controllerPage = PageScoreViewController()
//            controllerPage.page = score.pages[i]
//            //let gesture = UITapGestureRecognizer(target: self, action: #selector(showScoreFull(sender:)))
//            //gesture.numberOfTapsRequired = 2
//            //controllerPage.view.addGestureRecognizer(gesture)
//            controllers.append(controllerPage)
//        }
//
//        setViewControllers([controllers.first!], direction: .forward, animated: false, completion: nil)
        
        removeTapRecognizer()
        
        if let source = score.dataSource {
            for modelDsDc in source.dsDcs {
                dsdc = modelDsDc
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        menu.btnGeneral.addTarget(self, action: #selector(actionBtnGeneral(sender:)), for: .touchUpInside)
        menu.btnInfo.addTarget(self, action: #selector(actionBtnInfo(sender:)), for: .touchUpInside)
        menu.btnCanalyze.addTarget(self, action: #selector(actionBtnCanalyse(sender:)), for: .touchUpInside)
        menu.btnRepetition.addTarget(self, action: #selector(actionBtnRepetition(sender:)), for: .touchUpInside)
        menu.btnDSDC.addTarget(self, action: #selector(actionBtnDSDC(sender:)), for: .touchUpInside)
        menu.btnNotes.addTarget(self, action: #selector(actionBtnNotes(sender:)), for: .touchUpInside)
        menu.btnUndo.addTarget(self, action: #selector(actionBtnUndo(sender:)), for: .touchUpInside)
        menu.btnDone.addTarget(self, action: #selector(actionBtnDone(sender:)), for: .touchDown)
//        footer.btnHelp.addTarget(self, action: #selector(actionBtnHelp(sender:)), for: .touchUpInside)
        
        self.view.bringSubview(toFront: helpView)
    
        menuGeneral.btnFirstBar.addTarget(self, action: #selector(actionBtnFirstBar(sender:)), for: .touchUpInside)
        
        menuGeneral.setMetronome(metronome: (score.dataSource?.metronome)!)
        menuGeneral.setTimeSignature(numerator: (score.startTimeBar.first?.numerator)!, denominator: (score.startTimeBar.first?.denominator)!)
        
        if let source = score.dataSource {
          
            for key in source.configurations {
                if key.key != -1 {
                    menuGeneral.setKey(key: key.key)
                    break
                }
            }
            
            addInfo.setTitle(source.title)
            addInfo.setArranger(source.arranger)
            addInfo.setGenre(source.genre.name)
            if(source.genre.id == source.subgenre.idGenre){
                addInfo.setSubgenre(source.subgenre.name)
            }else {
                 addInfo.setSubgenre("")
            }
            addInfo.setSubtitle(source.subtitle)
            if(source.author.count > 0){
                addInfo.setComposer("\(source.author[0].firstName) \(source.author[0].lastName)")
            }
            else {
                addInfo.setComposer("")
            }
           
            if(source.artist.count > 0){
                addInfo.setArranger("\(source.artist[0].firstName) \(source.artist[0].lastName)")
            }
            else {
                addInfo.setArranger("")
            }
            
            
            addInfo.setDifficult(source.difficulty)
            addInfo.setScoreType(source.isPoliphonic)
            addInfo.setKeyType(source.configurations[0].keyType)
            addInfo.setCopyright(source.copyright.name)
            print("data aqui: ",source.createdyear)
            addInfo.setDate(source.createdyear)
            
            
            
            var tags = ""
            for tag in source.tags{
                tags = "\(tags)\(tag.name), "
            }
            
            addInfo.setTags(tags)
            addInfo.txtTitle.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
            addInfo.txtSubtitle.addTarget(self, action: #selector(txtSubTitleDidChange(textField:)), for: .editingChanged)
            addInfo.txtSubtitle.addTarget(self, action: #selector(txtSubtitleDidChange(textField:)), for: .editingChanged)
            
        }
        
        menuGeneral.delegate = self

        menuSelNotes.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(moveMenuNotes(_:))))
        
        option = .general
        score.funcion = .general
        score.hiddenChannelAreas()
        score.hiddenPlaces()
        
//        let user = UserManager.sharedUserManager
//        let token = user.token
//        user.server = "http://37.208.116.28"
//        authors.removeAll()
//        serviceGetAuthors(urlToRequest: user.server + ":3003/composers?token=\(token)", method: ServiceRemote.Method.GET.rawValue)
        footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
        
       // footer.btnLblInstrument.setTitle(titleFooter, for: .normal)
    }
    
    func serviceGetAuthors(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        print(urlToRequest)
        let session = URLSession.shared
        
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                print("sda: ",parseResult)
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    print("a", data1)
                    if let data = data1["composers"] as? [AnyObject] {
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            
                            let author   = ModelAuthor()
                            author.idUser    = d["_id"] as! String
                            author.firstName  = d["firstName"] as! String
                            if(d["lastName"] != nil){
                                author.lastName  = d["lastName"] as! String
                            }
                            authors.append(author)
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
    
    @objc func actionBtnHelp(sender: UIButton) {
        
        helpView.isHidden = false
    }
    
    @objc func moveMenuNotes(_ sender: UIPanGestureRecognizer) {
        
        let view = sender.view
        
        let newCenter = sender.location(in: view?.superview)
        let cX = menuSelNotes.center.x - newCenter.x
        let cY = menuSelNotes.center.y - newCenter.y
        
        menuSelNotes.center = newCenter
        
        menuToneNotes.center = CGPoint(x: menuToneNotes.center.x - cX, y: menuToneNotes.center.y - cY)
    }
    
    @objc func txtTitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.title = textField.text!
            score.changeTitle()
        }
    }
    
    @objc func txtSubTitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.subtitle = textField.text!
            score.changeSubTitle()
        }
    }
    
    @objc func txtComposerDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.author[0].idUser = textField.text!
            score.changeAuthor()
        }
    }
    
    @objc func txtSubtitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.subtitle = textField.text!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is HelpView || subView is MenuEditView || subView is MenuGeneralEditView || subView is MenuKeyEdit || subView is MenuCanalyseEdit || subView is MenuRepetitionView || subView is MenuDSDCEditView || subView is MenuNotesEditView || subView is MenuNotes || subView is MenuTonesNotes || subView is PopUpRepetitionManual || subView is MenuChannelNote || subView is AddInfo || subView is Footer || subView is LoadingView {
                self.view.bringSubview(toFront: subView)
            }
        }
        
        super.viewDidLayoutSubviews()
    }
    
    func updateMenu() {
        if menu == nil {
            let m = MenuEditView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
//            let backgroundImage = UIImageView(frame: m.bounds)
//            backgroundImage.image = #imageLiteral(resourceName: "background_menu_edit")
//            m.insertSubview(backgroundImage, at: 0)
            view.addSubview(m)
            menu = m
        } else {
            menu.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if let customView = Bundle.main.loadNibNamed("HelpViewX", owner: self, options: nil)?.first as? HelpView {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.helpCompose()
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            helpView = customView
        }
        
        if menuGeneral == nil {
            let m = MenuGeneralEditView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            
            menuGeneral = m
        } else {
            menuGeneral.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuKey == nil {
            let m = MenuKeyEdit(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuKey = m
        } else {
            menuKey.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuCanalyse == nil {
            let m = MenuCanalyseEdit(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuCanalyse = m
        } else {
            menuCanalyse.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuRepetition == nil {
            let m = MenuRepetitionView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuRepetition = m
        } else {
            menuRepetition.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuDSDC == nil {
            let m = MenuDSDCEditView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuDSDC = m
        } else {
            menuDSDC.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuNotes == nil {
            let m = MenuNotesEditView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuNotes = m
        } else {
            menuNotes.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuSelNotes == nil {
            let width = view.frame.width * 0.26
            let m = MenuNotes(frame: CGRect(x: 100, y: 800, width: width, height: width * 0.85))
            m.backgroundColor = UIColor.clear
            
            view.addSubview(m)
            
            menuSelNotes = m
        } else {
            menuSelNotes.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuToneNotes == nil {
            let width = view.frame.width * 0.05
            let m = MenuTonesNotes(frame: CGRect(x: view.frame.width * 0.26 + 150, y: 800, width: width, height: width * 5.0))
            m.backgroundColor = UIColor.clear
            
            view.addSubview(m)
            
            menuToneNotes = m
        } else {
            menuToneNotes.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if popRepManual == nil {
            let m = PopUpRepetitionManual(frame: CGRect(x: 500, y: 500, width: 100, height: 50))
            m.backgroundColor = UIColor.white
            
            view.addSubview(m)
            
            popRepManual = m
        } else {
            popRepManual.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuChannelNotes == nil {
            let m = MenuChannelNote(frame: CGRect(x: 500, y: 500, width: 40, height: 160))
            m.backgroundColor = UIColor.white
            
            view.addSubview(m)
            
            menuChannelNotes = m
        } else {
            menuChannelNotes.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if addInfo == nil {
            let y = (view.frame.height * 0.08)
            let m = AddInfo(frame: CGRect(x: 0, y: y, width: view.frame.width, height: view.frame.height - y))
            m.backgroundColor = UIColor(red: 212/255, green: 224/255, blue: 235/255, alpha: 1.0)
            
            view.addSubview(m)
            
            addInfo = m
        } else {
            addInfo.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if footer == nil {
            let height = view.frame.height * 0.033
            let m = Footer(frame: CGRect(x: 0.0, y: view.frame.height - height, width: view.frame.width, height: height))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            footer = m
        } else {
            footer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        menuKey.delegate          = self
        menuCanalyse.delegate     = self
        menuRepetition.delegate   = self
        menuDSDC.delegate         = self
        menuNotes.delegate        = self
        popRepManual.delegate     = self
        menuSelNotes.delegate     = self
        menuToneNotes.delegate    = self
        menuChannelNotes.delegate = self
        addInfo.delegate          = self

        menuSelNotes.isHidden     = true
        menuToneNotes.isHidden    = true
        popRepManual.isHidden     = true
        menuChannelNotes.isHidden = true
        addInfo.isHidden          = true
        
        if let source = score.dataSource {
            addInfo.currentCountry   = source.country
            addInfo.currentGender    = source.genre
            addInfo.currentSubgender = source.subgenre
        }
    }
    
    func showErrorMessageDSDC() {
        let alert = UIAlertController(title: "", message: "Error DSDC", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion:nil)
    }
    
    @objc func actionBtnGeneral(sender: UIButton) {

        self.currentOption = .none
        self.menuGeneral.btnFirstBar.alpha = 1.0
        self.score.hiddenPlaces()
        
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .general {
                menuGeneral.isHidden      = false
                menuCanalyse.isHidden     = true
                menuKey.isHidden          = true
                menuRepetition.isHidden   = true
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = true
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                addInfo.isHidden          = true
                footer.isHidden           = false
                
                menuGeneral.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuGeneral.alpha = 1.0
                })
                
                option = .general
                score.funcion = .general
                score.hiddenPlaces()
                score.funcion = .general
                score.hiddenPlaces()
            } else {
                menuGeneral.isHidden = !menuGeneral.isHidden
            }
        } else {
            showErrorMessageDSDC()
        }
    }
    
    @objc func actionBtnKey(sender: UIButton) {
    
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .key {
                menuGeneral.isHidden      = true
                menuKey.isHidden          = false
                menuCanalyse.isHidden     = true
                menuRepetition.isHidden   = true
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = true
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                addInfo.isHidden          = true
                footer.isHidden           = false

                
                menuKey.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuKey.alpha = 1.0
                })
                
                option = .key
                score.funcion = .general
                score.hiddenPlaces()
                score.funcion = .general
                score.hiddenPlaces()
            } else {
                menuKey.isHidden = !menuKey.isHidden
            }
        } else {
            showErrorMessageDSDC()
        }
    }
    
    @objc func actionBtnCanalyse(sender: UIButton) {
        self.score.hiddenPlaces()
        
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .canalyse {
                menuGeneral.isHidden      = true
                menuKey.isHidden          = true
                menuCanalyse.isHidden     = false
                menuRepetition.isHidden   = true
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = true
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                addInfo.isHidden          = true
                footer.isHidden           = false

                
                menuCanalyse.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuCanalyse.alpha = 1.0
                })
                
                option = .canalyse
                
                //---Esconde as cores dos lugares da partitura---
                score.hiddenPlaces()
                //-----------------------------------------------
            } else {
                menuCanalyse.isHidden = !menuCanalyse.isHidden
            }
        } else {
            showErrorMessageDSDC()
        }
    }
    
    @objc func actionBtnRepetition(sender: UIButton) {

        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .repetition {
                menuGeneral.isHidden      = true
                menuKey.isHidden          = true
                menuCanalyse.isHidden     = true
                menuRepetition.isHidden   = false
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = true
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                addInfo.isHidden          = true
                footer.isHidden           = false

                
                menuRepetition.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuRepetition.alpha = 1.0
                })
                
                option = .repetition
                score.funcion = .general
                score.hiddenPlaces()
                score.funcion = .general
                score.hiddenPlaces()
            } else {
                menuRepetition.isHidden = !menuRepetition.isHidden
            }
        } else {
            showErrorMessageDSDC()
        }
    }
    
    @objc func actionBtnDSDC(sender: UIButton) {
     
        if option != .dsdc {
            menuGeneral.isHidden      = true
            menuKey.isHidden          = true
            menuCanalyse.isHidden     = true
            menuRepetition.isHidden   = true
            menuDSDC.isHidden         = false
            menuNotes.isHidden        = true
            menuSelNotes.isHidden     = true
            menuToneNotes.isHidden    = true
            popRepManual.isHidden     = true
            menuChannelNotes.isHidden = true
            addInfo.isHidden          = true
            footer.isHidden           = false

            
            menuDSDC.alpha = 0.92
            UIView.animate(withDuration: 0.5, animations: {
                self.menuDSDC.alpha = 1.0
            })
            
            option = .dsdc
            score.funcion = .general
            score.hiddenPlaces()
            score.funcion = .general
            score.hiddenPlaces()
        } else {
            menuDSDC.isHidden = !menuDSDC.isHidden
        }
    }
    
    @objc func actionBtnNotes(sender: UIButton) {
       
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .notes {
                menuGeneral.isHidden      = true
                menuKey.isHidden          = true
                menuCanalyse.isHidden     = true
                menuRepetition.isHidden   = true
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = false
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                addInfo.isHidden          = true
                footer.isHidden           = false

                
                menuNotes.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.menuNotes.alpha = 1.0
                })
                
                option = .notes
                score.funcion = .general
                score.showPlaces()
                score.funcion = .general
                score.showPlaces()
            } else {
                menuNotes.isHidden = !menuNotes.isHidden
            }
        } else {
            showErrorMessageDSDC()
        }
    }
    
    @objc func actionBtnInfo(sender: UIButton) {
     
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            if option != .info {
                menuGeneral.isHidden      = true
                menuKey.isHidden          = true
                menuCanalyse.isHidden     = true
                menuRepetition.isHidden   = true
                menuDSDC.isHidden         = true
                menuNotes.isHidden        = true
                menuSelNotes.isHidden     = true
                menuToneNotes.isHidden    = true
                popRepManual.isHidden     = true
                menuChannelNotes.isHidden = true
                footer.isHidden           = true
                addInfo.isHidden          = false
                
                addInfo.alpha = 0.92
                UIView.animate(withDuration: 0.5, animations: {
                    self.addInfo.alpha = 1.0
                })
                
                option = .info
                score.hiddenPlaces()
            }
        } else {
            showErrorMessageDSDC()
        }
        addInfo.tbAuthor.reloadData()
        addInfo.tbArtist.reloadData()
        
    }
    
    @objc func actionBtnUndo(sender: UIButton) {
      
        if historyScores.count > 0 {
            score = historyScores[historyScores.count - 1]
            historyScores.remove(at: historyScores.count - 1)
            
            if let source = score.dataSource {
                if source.dsDcs.count == 0 {
                    self.dsdc.coda       = -1
                    self.dsdc.dcAlCoda   = -1
                    self.dsdc.dcAlFine   = -1
                    self.dsdc.dsAlCoda   = -1
                    self.dsdc.dsAlFine   = -1
                    self.dsdc.finalBar   = -1
                    self.dsdc.fine       = -1
                    self.dsdc.initialBar = -1
                    self.dsdc.toCoda     = -1
                    self.dsdc.type       = -1
                    self.dsdcIsActive    = false
                }
                
                let instrument = ModelTypeInstrument()
                instrument.name = "Piano"
                
                let isMonophonic = UserDefaults.standard.bool(forKey: "Mono")
                
                if !isMonophonic {
                    instrument.clef = 3
                } else {
                    instrument.clef = 1
                }
                
                source.typeInstrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
                source.instrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory
                
            }

            updateScore(currentPage: actualPage)
            if option == .notes {
                score.showPlaces()
            }
        }
    }
    
    @objc func actionBtnDone(sender: UIButton) {
   
        if (option == .dsdc && dsdc.isValid()) || option != .dsdc {
            
//            _ = score.updateChannelNotes()
            
            if presentingViewController is ComposeViewController {
                _ = presentingViewController as! ComposeViewController
               
//                (presentingViewController as! ComposeViewController).score = score
//                (presentingViewController as! ComposeViewController).updatePages()
//                (presentingViewController as! ComposeViewController).popUpCompose.setMetronome(metronome: (score.dataSource?.metronome)!)
//                (presentingViewController as! ComposeViewController).popUpCompose.setInitialTonality(tom:  (self.score.dataSource?.configurations[0].key)!)
                (presentingViewController as!
                    ComposeViewController).popUpCompose.getTimeSignature((score.dataSource?.configurations[0].numerator)!, (score.dataSource?.configurations[0].denominator)!)
            
                let present = presentingViewController as! ComposeViewController
                present.score = score
                present.updatePages()
                
            }
            dismiss(animated: true, completion: nil)
        } else {
            showErrorMessageDSDC()
        }
    }
    
    var firstSelected = false
    
    @objc func actionBtnFirstBar(sender: UIButton) {
        Appsee.addScreenAction("Clicked First Bar Button")
        currentOption = .firstBar
        
        firstSelected = !firstSelected
        
        if firstSelected {
            menuGeneral.btnFirstBar.alpha = 0.5
            self.score.showPlaces()
        } else {
            menuGeneral.btnFirstBar.alpha = 1.0
            currentOption = .none
            self.score.hiddenPlaces()
        }
    }

    func changeKey(newKey: OptionKeySelected) {
        //----Adiciona uma partitura ao vetor de partituras do para UNDO----
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        //------------------------------------------------------------------
        var keySelect = KeyType.C_Am
        switch newKey {
        case .F:
            keySelect = .F_Dm
        case .Bb:
            keySelect = .Bb_Gm
        case .Eb:
            keySelect = .Eb_Cm
        case .Ab:
            keySelect = .Ab_Fm
        case .Db:
            keySelect = .Db_BBm
        case .Gb:
            keySelect = .Gb_Ebm
        case .G:
            keySelect = .G_Em
        case .D:
            keySelect = .D_Bm
        case .A:
            keySelect = .A_Fsm
        case .E:
            keySelect = .E_Csm
        case .B:
            keySelect = .B_Gsm
        case .Fs:
            keySelect = .Fs_Dsm
        case .Cs:
            keySelect = .Cs_Asm
        case .Cb:
            keySelect = .C_Am
        case .C:
            keySelect = .C_Am
        }
        
        if let source = score.dataSource {
            source.changeTonality(newTonality: keySelect)

            updateScore(currentPage: actualPage)
        }
    }
    
    func showCanalizationAreas(type: OptionCanalyseSelected) {
        //----Adiciona uma partitura ao vetor de partituras do para UNDO----
        //        DispatchQueue.global(qos: .userInteractive).async {
        self.historyScores.append(self.score.copy() as! DrawScore)
        //        }
        //------------------------------------------------------------------
        _ = score.updateChannelNotes()
        
        if type == .umtres {
            score.funcion = .canalyse13
            menuChannelNotes.isHidden = true
        }
        if type == .umdois {
            score.funcion = .canalyse12
            menuChannelNotes.isHidden = true
        }
        if type == .tresquatro {
            score.funcion = .canalyse34
            menuChannelNotes.isHidden = true
        }
        if type == .above {
            score.funcion = .canalyseAbove
            menuChannelNotes.isHidden = true
        }
        if type == .center {
            score.funcion = .canalyseCenter
            menuChannelNotes.isHidden = true
        }
        if type == .manual {
            score.funcion = .canalyseManual
            currentOption = .canalyseManual
        }
    }
    
    func addRepetionToScore(type: OptionRepetitionSelected) {
       
        
        popRepManual.isHidden = true
        
        
        if type == .oito {
            currentOption = .repetition8
        }
        if type == .seteum {
            currentOption = .repetition71
        }
        if type == .seisdois {
            currentOption = .repetition62
        }
        if type == .desesseis {
            currentOption = .repetition16
        }
        if type == .quinzeum {
            currentOption = .repetition151
        }
        if type == .quatorzedois {
            currentOption = .repetition142
        }
        if type == .manual {
            currentOption = .repetitionmanual
        }
        if type == .delete {
            currentOption = .repetitiondelete
        }
        if type == .deleteBar {
            currentOption = .deleteBar
        }
        if type == .endBar{
            currentOption = .endBar
        }
    }
    
    func addDSDCToScore(type: OptionDSDCSelected) {
       
        if type == .segno {
            currentOption = .segno
        }
        if type == .coda {
            currentOption = .coda
        }
        if type == .toCoda {
            currentOption = .toCoda
        }
        if type == .dcAlCoda {
            currentOption = .dcAlCoda
        }
        if type == .dcAlFine {
            currentOption = .dcAlFine
        }
        if type == .dsAlCoda {
            currentOption = .dsAlCoda
        }
        if type == .dsAlFine {
            currentOption = .dsAlFine
        }
        if type == .fine {
            currentOption = .fine
        }
        if type == .delete {
            currentOption = .dsdcdelete
        }
    }
    
    func addNoteToScore(type: OptionNotesSelected) {
      
        switch type {
        case .whole:
            currentOption = .addWhole
        case .half:
            currentOption = .addHalf
        case .quarter:
            currentOption = .addQuarter
        case .quaver:
            currentOption = .addQuaver
        case .semiquaver:
            currentOption = .addSemiQuaver
        default:
            break
        }
    }
    
    func addPauseToScore(type: OptionNotesSelected) {
       
        if type == .pausewhole {
            currentOption = .addPauseWhole
        }
        if type == .pausehalf {
            currentOption = .addPauseHalf
        }
        if type == .pausequarter {
            currentOption = .addPauseQuarter
        }
        if type == .pausequaver {
            currentOption = .addPauseQuaver
        }
        if type == .pausesemiquaver {
            currentOption = .addPauseSemiQuaver
        }
    }
    
    func addAccidentToScore(type: OptionNotesSelected) {
       
        
        if type == .sharp {
            currentOption = .addSharp
        }
        if type == .flat {
            currentOption = .addFlat
        }
        if type == .natural {
            currentOption = .addNatural
        }
    }
    
    func changeScoreTime(type: OptionTimeSelected) {
     

        //----Adiciona uma partitura ao vetor de partituras do para UNDO----
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        //------------------------------------------------------------------
        
        var numerator   = 4
        var denominator = 4
        switch type {
        case .doisdois:
            numerator = 2
            denominator = 2
        case .tresdois:
            numerator = 3
            denominator = 2
        case .doisquartos:
            numerator = 2
            denominator = 4
        case .tresquartos:
            numerator = 3
            denominator = 4
        case .quatroquartos:
            numerator = 4
            denominator = 4
        case .cincoquartos:
            numerator = 5
            denominator = 4
        case .seisquartos:
            numerator = 6
            denominator = 4
        case .tresoitavos:
            numerator = 3
            denominator = 8
        case .seisoitavos:
            numerator = 6
            denominator = 8
        case .noveoitavos:
            numerator = 9
            denominator = 8
        case .dozeoitavos:
            numerator = 12
            denominator = 8
        }
        
        if let source = score.dataSource {
            var found = false
            for i in 0..<source.configurations.count {
                if source.configurations[i].bar == 1  {
                    source.configurations[i].numerator = numerator
                    source.configurations[i].denominator = denominator
                    
                    found = true
                }
            }
            
            if !found {
                let config = ModelBarConfiguration()
                config.numerator = numerator
                config.denominator = denominator
                source.configurations.append(config)
            }

            updateScore(currentPage: actualPage)
        }
    }
    
    func changeMetronomeScore(value: Int) {
        
        //----Adiciona uma partitura ao vetor de partituras do para UNDO----
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        //------------------------------------------------------------------
        
        if let source = score.dataSource {
            source.metronome = value
        }
        
        score.changeMetronome()
    }

    func addNewNote() {
        self.historyScores.append(self.score.copy() as! DrawScore)
        
        if channelTapped == 4 {
            channelTapped = 3
        }
        if channelTapped == 2 {
            channelTapped = 1
        }
        switch currentOption {
        case .addWhole:
            if let source = score.dataSource {
                source.addNote(tone: 60, initialTime: initialTimeTapped, duration: 3840, channel: channelTapped)
            }
        case .addHalf:
            if let source = score.dataSource {
                source.addNote(tone: 60, initialTime: initialTimeTapped, duration: 1920, channel: channelTapped)
            }
        case .addQuarter:
            if let source = score.dataSource {
                source.addNote(tone: 60, initialTime: initialTimeTapped, duration: 960, channel: channelTapped)
            }
        case .addQuaver:
            if let source = score.dataSource {
                source.addNote(tone: 60, initialTime: initialTimeTapped, duration: 480, channel: channelTapped)
            }
        case .addSemiQuaver:
            if let source = score.dataSource {
                source.addNote(tone: 60, initialTime: initialTimeTapped, duration: 240, channel: channelTapped)
            }
        default:
            break
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        if let source = score.dataSource {
            source.extendNotes(numerator: score.startTimeBar[0].numerator, denominator: score.startTimeBar[0].denominator)
        }
        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func addPause() {
        self.historyScores.append(self.score.copy() as! DrawScore)
        
        if let source = score.dataSource {
            switch currentOption {
            case .addPauseWhole:
                source.addPause(initialTime: initialTimeTapped, duration: 3840)
            case .addPauseHalf:
                source.addPause(initialTime: initialTimeTapped, duration: 1920)
            case .addPauseQuarter:
                source.addPause(initialTime: initialTimeTapped, duration: 960)
            case .addPauseQuaver:
                source.addPause(initialTime: initialTimeTapped, duration: 480)
            case .addPauseSemiQuaver:
                source.addPause(initialTime: initialTimeTapped, duration: 240)
            default:
                break
            }
            
            menuSelNotes.isHidden  = true
            menuToneNotes.isHidden = true

            updateScore(currentPage: actualPage)
            score.showPlaces()
        }
    }
    
    func addAccident() {
        if let source = score.dataSource {
            if currentOption == .addSharp {
                source.addSustain(tone: toneNoteTapped, initialTime: iniTimeNoteTapped)
            }
            if currentOption == .addFlat {
                source.addBemol(tone: toneNoteTapped, initialTime: iniTimeNoteTapped)
            }
            if currentOption == .addNatural {
                source.addNatural(tone: toneNoteTapped, initialTime: iniTimeNoteTapped)
            }
            
            if currentOption == .addSharp || currentOption == .addFlat || currentOption == .addNatural {
                menuSelNotes.isHidden  = true
                menuToneNotes.isHidden = true

                updateScore(currentPage: actualPage)
                score.showPlaces()
            }
        }
    }
    
    var lastNoteTapped = ViewNote()
    var iniTimeNoteTapped  = 0
    var toneNoteTapped     = 0
    var initialTimeTapped  = 0
    var positionToneTapped = 0
    var realInitNote       = 0
    var channelTapped      = 1
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        var selectedBar  = -1
        var timeNote     = -1
        var durationNote = -1
        var desloc       = -1
        
        if touches.first?.view is ViewPlace {
           
            initialTimeTapped = (touches.first?.view as! ViewPlace).initialTime
            channelTapped     = (touches.first?.view as! ViewPlace).channel
            
            if currentOption == .addWhole || currentOption == .addHalf || currentOption == .addQuarter || currentOption == .addQuaver || currentOption == .addSemiQuaver {
                addNewNote()
            }
            if currentOption == .addPauseWhole || currentOption == .addPauseHalf || currentOption == .addPauseQuarter || currentOption == .addPauseQuaver || currentOption == .addPauseSemiQuaver {
                addPause()
            }
        }
        
        if touches.first?.view is ViewBar {
            
            selectedBar = (touches.first?.view as! ViewBar).bar
            
            if option == .repetition && currentOption == .endBar {
                let barSelected = (touches.first?.view as! ViewBar)
                //score.dataSource barSelected.bar
            }
            
            if option == .repetition && currentOption == .deleteBar {
                let barSelected = (touches.first?.view as! ViewBar)
                showMsgDeleteBar(start: barSelected.start, end: barSelected.end)
            }
        }
        
        if touches.first?.view is ViewPentagram {
           
            selectedBar = (touches.first?.view as! ViewPentagram).bar
        }
        
        if touches.first?.view is ViewRepetition {
            let vRep = touches.first?.view as! ViewRepetition
        
            
            if option == .repetition && currentOption == .repetitiondelete {
                if let source = score.dataSource {
                    source.deleteRepetition(type: vRep.type, bar: vRep.bar)
                    
                    //----Adiciona uma partitura ao vetor de partituras do para UNDO----
//                    DispatchQueue.global(qos: .userInteractive).async {
                        self.historyScores.append(self.score.copy() as! DrawScore)
//                    }
                    //------------------------------------------------------------------

                    updateScore(currentPage: actualPage)
                }
            }
            
            if option == .dsdc && currentOption == .dsdcdelete {
                if let source = score.dataSource {
                    source.deleteDsDc(type: vRep.type, bar: vRep.bar)
                    
                    //----Adiciona uma partitura ao vetor de partituras do para UNDO----
//                    DispatchQueue.global(qos: .userInteractive).async {
                        self.historyScores.append(self.score.copy() as! DrawScore)
//                    }
                    //------------------------------------------------------------------

                    updateScore(currentPage: actualPage)
                }
            }
        }
        
        if touches.first?.view is ViewNote {
        
            selectedBar = (touches.first?.view as! ViewNote).bar
            timeNote = (touches.first?.view as! ViewNote).itQuantized
            durationNote = (touches.first?.view as! ViewNote).duration
            positionToneTapped = (touches.first?.view as! ViewNote).positionTone
            channelTapped      = (touches.first?.view as! ViewNote).channel
            
            menuToneNotes.pickerTone.selectRow(positionToneTapped, inComponent: 0, animated: false)
            
            let n = touches.first?.view as! ViewNote
            iniTimeNoteTapped  = n.itQuantized
            initialTimeTapped  = n.itQuantized
            if option == .canalyse && currentOption == .canalyseManual {
                
                toneNoteTapped     = n.tone

                menuChannelNotes.isHidden = false
                let x = (touches.first?.view?.frame.origin.x)! - 40
                let y = (touches.first?.view?.frame.origin.y)! - 10
                let width  = menuChannelNotes.frame.width
                let height = menuChannelNotes.frame.height
                
                menuChannelNotes.frame = CGRect(x: x, y: y, width: width, height: height)
            }
            
            var barDuration = 3840

            if let source = score.dataSource {
                barDuration = source.getTimeBar()
            }

            let newZero = barDuration //- durationNote
            desloc = timeNote - newZero
            
            if option == .notes {
                let n = touches.first?.view as! ViewNote
                
                iniTimeNoteTapped = n.itQuantized
                toneNoteTapped    = n.tone
                initialTimeTapped = n.itQuantized
                realInitNote      = n.realInitNote

                addAccident()
                
                if lastNoteTapped == n {
                    lastNoteTapped.color = UIColor.black
                    lastNoteTapped = ViewNote()
                    menuSelNotes.isHidden  = true
                    menuToneNotes.isHidden = true
                } else {
                    lastNoteTapped.color = UIColor.black
                    n.color = UIColor(red: 9/255, green: 78/255, blue: 148/255, alpha: 1.0)
                    lastNoteTapped = n
                    menuSelNotes.isHidden  = false
                    menuToneNotes.isHidden = false
                }
            }
        }
        
        if option == .general && currentOption == .firstBar {
            //            if timeNote != -1 && durationNote != -1 {
            if let source = score.dataSource {
                var error = false
                var minTime = 999999
                for note in source.notes {
                    minTime = min(note.tIniQuantized, minTime)
                }
                let desloc = initialTimeTapped - minTime
                
                for note in source.notes {
                    if note.tIniQuantized + desloc < 0 {
                        error = true
                    }
                }
                //
                if !error {
                    //----Adiciona uma partitura ao vetor de partituras do para UNDO----
                    //                        DispatchQueue.global(qos: .userInteractive).async {
                    self.historyScores.append(self.score.copy() as! DrawScore)
                }
                //------------------------------------------------------------------
                for note in source.notes {
                    note.tIniQuantized += desloc
                }
                
                source.extendNotes(numerator: score.startTimeBar[0].numerator, denominator: score.startTimeBar[0].denominator)
                updateScore(currentPage: actualPage)
                //                    }
            }
            //            }
        }
        
        if option == .repetition && selectedBar != -1 {
            var finalBar = -1
            var brackets = 0
            if currentOption == .repetition8 {
                finalBar = selectedBar + 7
            }
            if currentOption == .repetition71 {
                finalBar = selectedBar + 7
                brackets = 1
            }
            if currentOption == .repetition62 {
                finalBar = selectedBar + 7
                brackets = 2
            }
            if currentOption == .repetition16 {
                finalBar = selectedBar + 15
            }
            if currentOption == .repetition151 {
                finalBar = selectedBar + 15
                brackets = 1
            }
            if currentOption == .repetition142 {
                finalBar = selectedBar + 15
                brackets = 2
            }
            if currentOption == .repetitionmanual {
//                if touches.first?.view is ViewBar {
                if selectedBar != -1 {
                    startBarRepManual = selectedBar
                    popRepManual.frame.origin = (touches.first?.view?.frame.origin)!
                    popRepManual.lblNumber1.text = ""
                    popRepManual.lblNumber2.text = ""
                    popRepManual.isHidden = false
                    popRepManual.lblNumber1.becomeFirstResponder()
                    
                    
                }
//                }
            }
            
            if selectedBar != -1 && finalBar != -1 && currentOption != .repetitiondelete{
                let repetition = ModelRepeat()
                repetition.initialBar    = selectedBar
                repetition.finalBar      = finalBar
                repetition.voltaBrackets = brackets
                
//                DispatchQueue.global(qos: .userInteractive).async {
                    self.historyScores.append(self.score.copy() as! DrawScore)
//                }
                
                let refreshAlert = UIAlertController(title: "Options", message: "Remove bars later?", preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    if (self.score.dataSource?.addRepeat(repetition: repetition, isToDelete: true))! {

                        self.updateScore(currentPage: self.actualPage)
                    } else {
                        self.showErrorAddRepeat()
                    }
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                    if (self.score.dataSource?.addRepeat(repetition: repetition, isToDelete: false))! {

                        self.updateScore(currentPage: self.actualPage)
                    } else {
                        self.showErrorAddRepeat()
                    }
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                
                present(refreshAlert, animated: true, completion: nil)
            }
        }
        
        if option == .dsdc && selectedBar != -1 {
            if currentOption == .segno {
                dsdc.initialBar = selectedBar
                dsdc.finalBar = selectedBar
            }
            
            if currentOption == .coda {
                dsdc.coda = selectedBar
            }
            
            if currentOption == .toCoda {
                dsdc.toCoda = selectedBar
            }
            
            if currentOption == .dsAlCoda {
                dsdc.dsAlCoda = selectedBar
            }
            
            if currentOption == .dsAlFine {
                dsdc.dsAlFine = selectedBar
            }
            
            if currentOption == .dcAlCoda {
                dsdc.dcAlCoda = selectedBar
            }
            
            if currentOption == .dcAlFine {
                dsdc.dcAlFine = selectedBar
            }
            
            if currentOption == .fine {
                dsdc.fine = selectedBar
            }
            
            if !dsdcIsActive {
//                dsdc.initialBar = -1
//                dsdc.finalBar = -1
//
//                DispatchQueue.global(qos: .userInteractive).async {
                    self.historyScores.append(self.score.copy() as! DrawScore)
//                }
                
                
//                if (score.dataSource?.addDSDC(dsdc: dsdc))! {
//
//
                if score.dataSource?.dsDcs.count == 0 {
                    if (score.dataSource?.addDSDC(dsdc: dsdc))! {
                        updateScore(currentPage: actualPage)
                    }
                }
                
                
//                }
            } else {
                updateScore(currentPage: actualPage)
            }
            
            dsdcIsActive = true
        }

    }
    
    func showErrorAddRepeat() {
        let alert = UIAlertController(title: "", message: "Invalid Repetition", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion:nil)
    }
    
    func showMsgDeleteBar(start: Int, end: Int) {
        let alert = UIAlertController(title: "", message: "Delete bar?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(action) in
            if let source = self.score.dataSource {
                self.historyScores.append(self.score.copy() as! DrawScore)
                
                source.deleteBar(tStart: start, tEnd: end)
                
                self.updateScore(currentPage: self.actualPage)
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            
        }))
        self.present(alert, animated: true, completion:nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
    
    }
    
    let activity = LoadingView()
    
    /// Atualiza páginas da partitura
    ///
    /// - Parameter currentPage: página que está selecionada atualmente
    func updateScore(currentPage: Int = 0) {
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        
        if activity.superview == nil {
            view.addSubview(activity)
        }
        
        activity.start()
        //Desinicializa o datasource do pageviewcontroller
        //        dataSource = nil
        //        dataSource = self
        
        //        let currentId = currentPage
        
        //-------------Calcula frame das páginas da partitura-------------
        let x = (view.frame.height * 0.08) * 0.3
        let y = view.frame.height * 0.08
        let width  = view.frame.width - (2.0 * x)
        let height = view.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        //----------------------------------------------------------------
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            //Calcula nova quantização da partitura
            if let source = self.score.dataSource {
                //Calcula-se nova quantização
                source.calculateQuantization()
                //Muda quantização da partitura
                source.quantization = source.suggestedQuantization
            }
           
            if let source = self.score.dataSource {
                source.generatePauses()
            }
            
            self.score.state = .editable
            print("Entrou")
            self.score.verifyState()
            
            //Caso seja compose, adiciona-se compassos a partitura
            if self.presentingViewController is ComposeViewController {
                while self.score.bars.count < self.countBarsScore {
                    self.score.addBar()
                }
            }
            
            //Cria páginas da partitura
            self.score.createPages(parameters: parameters)
            
            //Remove os controlers (pages) da partitura anterior
            self.controllers.removeAll()
            
            //Cria os controlers (pages) a partir das páginas criadas na partitura
            for i in 0..<self.score.pages.count {
                let controllerPage = PageScoreViewController()
                controllerPage.page = self.score.pages[i]
                self.controllers.append(controllerPage)
            }
            
            DispatchQueue.main.async(execute: {
//                self.score.addNote(midi: 60, tIniQuantized: 960, durationQuantized: 960, channel: 1)
//                for bar in self.score.bars {
//                    bar.joinNotes()
//                }

                self.activity.removeFromSuperview()
//                self.activity.done()
//                UIView.animate(withDuration: 0.0, delay: 0.0, options: [], animations: {
//                    if self.activity.imgDone != nil {
//                        self.activity.imgDone.alpha = 0.0
//                    }
//                }, completion: { (finished: Bool) in
//                    self.activity.removeFromSuperview()
//                     if self.activity.imgDone != nil {
//                        self.activity.imgDone.alpha = 1.0
//                    }
//                })
                
                
                
                
                
                //Atualiza os controllers com as novas páginas da partitura
                if self.currentId < self.controllers.count {
                    self.setViewControllers([self.controllers[self.currentId]], direction: .forward, animated: false, completion: nil)
                } else {
                    self.setViewControllers([self.controllers[self.controllers.count - 1]], direction: .forward, animated: false, completion: nil)
                }
                
                //Esconde as cores dos lugares da partitura
                if self.option != .notes && self.currentOption != .firstBar {
                    self.score.hiddenPlaces()
                } else {
                    self.score.showPlaces()
                }
                print("Saiu")
            })
        }
    }
    
    deinit {
        print("Entrou deinit EditViewController")
    }


}

extension EditViewController: AddInfoDelegate {
    func changeCopyright(copyright: ModelCopyright){
        if let source = score.dataSource {
            source.copyright = copyright
        }
    }
    
 
    func changeArranger(newArranger: ModelArtist) {
        if let source = score.dataSource {
            source.artist.removeAll()
            source.artist.append(newArranger)
        }
    }
    
    func changeTitle(newTitle: String) {
        score.dataSource?.title = newTitle
    }
    
    func changeSubtitle(newSubtitle: String) {
        score.dataSource?.subtitle = newSubtitle
    }
    
    func changeDificult(newDificult: Int) {
          score.dataSource?.difficulty = newDificult
    }
    
    func changeDate(newString: String) {
          score.dataSource?.createdyear = newString
    }
    
    func changeTags(newTag: String) {
        score.dataSource?.tags.removeAll()
        let scoreTags = newTag.components(separatedBy: ", ")
        for tags in scoreTags {
            let modeltag = ModelTag()
            modeltag.idPartitura = score.dataSource?.id
            modeltag.name = tags
            score.dataSource?.tags.append(modeltag)
        }
    }
    
    func changeScoreType(newType: Int) {
          score.dataSource?.isPoliphonic = newType
//        print(score.dataSource?.isPoliphonic)
    }
    
    func changeKeyType(newKey: Int) {
        score.dataSource?.configurations[0].keyType = newKey
    }
    
    func changeAuthor(newAuthor: ModelAuthor) {
        if let source = score.dataSource {
            source.author.removeAll()
            source.author.append(newAuthor)
        }
    }
    
    func changeInstrument(newInstrument: ModelInstrument) {
        if let source = score.dataSource {
            source.instrument = newInstrument
        }
    }
    
  
    
    func changeType(newType: ModelTypeInstrument) {
        if let source = score.dataSource {
            source.typeInstrument = newType
        }
    }
    
    func changeCountry(newCountry: ModelCountry) {
        if let source = score.dataSource {
            source.country = newCountry
        }
    }
    
    func changeGenre(newGenre: ModelGenre) {
        if let source = score.dataSource {
            source.genre = newGenre
        }
    }
    
    func changeSubgenre(newSubgenre: ModelSubGenre) {
        if let source = score.dataSource {
            source.subgenre = newSubgenre
        }
    }
    
    
}

extension EditViewController: MenuChannelNoteDelegate {
    func changeChannel(newChannel: Int) {
        if let source = score.dataSource {
            source.changeNoteChannel(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, newChannel: newChannel)
            
            menuChannelNotes.isHidden = true
            
            updateScore(currentPage: actualPage)
        }
    }
    
    
}

extension EditViewController: MenuNotesViewDelegate {
    func divideNote() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        if let source = score.dataSource {
            source.divideNote(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true
        
        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func divideNoteAndPutNoteMorePause() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.divideNoteAndPutNoteMorePause(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func divideNoteAndPutPauseMoreNote() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.divideNoteAndPutPauseMoreNote(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func deleteNoteAndExtendAfter() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.deleteNoteAndExtendBefore(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func deleteNoteAndExtendBefore() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.deleteNoteAndExtendAfter(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func deleteNoteAndReplaceWithPause() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.deleteNoteAndReplaceWithPause(realInitTime: realInitNote, initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    func deleteNoteAndExtendNoteBefore() {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.deleteNoteAndExtendNoteBefore(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, channel: channelTapped)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
    
    
}

extension EditViewController: MenuTonesNotesViewDelegate {
    func changeTone(_ newTone: Int) {
//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        if let source = score.dataSource {
            source.changeNoteTone(initialTime: iniTimeNoteTapped, tone: toneNoteTapped, newTone: newTone)
        }
        
        menuSelNotes.isHidden  = true
        menuToneNotes.isHidden = true

        updateScore(currentPage: actualPage)
        score.showPlaces()
    }
}

extension EditViewController: PopUpRepetitionManualDelegate {
    func addRepetition(size: Int, bracket: Int) {
        let repetition = ModelRepeat()
        repetition.initialBar    = startBarRepManual
      
        repetition.finalBar      = startBarRepManual + size + bracket - 1
        repetition.voltaBrackets = bracket
        
        self.historyScores.append(self.score.copy() as! DrawScore)
        print(score.bars.count)
      
        
        
        let refreshAlert = UIAlertController(title: "Options", message: "Remove bars later?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            if (self.score.dataSource?.addRepeat(repetition: repetition, isToDelete: true))! {
                self.popRepManual.isHidden = true
                self.popRepManual.lblNumber1.text = ""
                self.self.popRepManual.lblNumber2.text = ""
                self.updateScore(currentPage: self.actualPage)
            } else {
                self.showErrorAddRepeat()
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            if (self.score.dataSource?.addRepeat(repetition: repetition, isToDelete: false))! {
                self.updateScore(currentPage: self.actualPage)
                
                self.popRepManual.isHidden = true
                self.popRepManual.lblNumber1.text = ""
                self.self.popRepManual.lblNumber2.text = ""
                self.updateScore(currentPage: self.actualPage)
            } else {
                self.showErrorAddRepeat()
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        
        present(refreshAlert, animated: true, completion: nil)
    }
}

extension EditViewController: MenuKeyEditDelegate {
    func changeOption(_ option: OptionKeySelected) {
        changeKey(newKey: option)
    }
}

extension EditViewController: MenuCanalyseEditDelegate {
    func changeOption(_ option: OptionCanalyseSelected) {
        showCanalizationAreas(type: option)
        showCanalizationAreas(type: option)
    }
}

extension EditViewController: MenuRepetitionViewDelegate {
    func changeOption(_ option: OptionRepetitionSelected) {
        addRepetionToScore(type: option)
    }
}

extension EditViewController: MenuDSDCEditViewDelegate {
    func changeOption(_ option: OptionDSDCSelected) {
        addDSDCToScore(type: option)
    }
}

extension EditViewController: MenuGeneralEditViewDelegate {
    func changeKey(_ key: KeyType) {
       

//        DispatchQueue.global(qos: .userInteractive).async {
            self.historyScores.append(self.score.copy() as! DrawScore)
//        }
        
        var found = false
        if let source = score.dataSource {
            for i in 0..<source.configurations.count {
                if source.configurations[i].bar == 1  {
                    source.configurations[i].key = key.rawValue
                    found = true
                }
            }
            
            if !found {
                let config = ModelBarConfiguration()
                config.key = key.rawValue
                source.configurations.append(config)
            }

            updateScore(currentPage: actualPage)
        }
    }
    
    func changeOptionTime(_ option: OptionTimeSelected) {
        changeScoreTime(type: option)
    }
    
    func changeMetronome(_ value: Int) {
        changeMetronomeScore(value: value)
    }
}

extension EditViewController: MenuNotesEditViewDelegate {
    func changeOption(_ option: OptionNotesSelected) {
        if option == .cancel {
            currentOption = .none
        }
        
        if option == .whole || option == .half || option == .quarter || option == .quaver || option == .semiquaver {
            addNoteToScore(type: option)
            
        }
        
        if option == .flat || option == .sharp || option == .natural {
            addAccidentToScore(type: option)
        }
        
        if option == .pausewhole || option == .pausehalf || option == .pausequarter || option == .pausequaver || option == .pausesemiquaver {
            addPauseToScore(type: option)
        }
    }
}

extension EditViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if previousViewControllers[0] is PageScoreViewController && finished {
            let pg = previousViewControllers[0] as! PageScoreViewController
            if toFront {
                currentId = (pg.page.index - 1) + 1
            } else {
                currentId = (pg.page.index - 1) - 1
            }
            
          
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex < 1 {
            return nil
        }
        
        if viewControllerIndex - 1 >= 0 {
            actualPage = viewControllerIndex - 1
            controllers[viewControllerIndex - 1].score = self.score

            if option == .notes || (currentOption == .firstBar && option == .general) {
                controllers[viewControllerIndex - 1].page.showPlaces()
                controllers[viewControllerIndex - 1].isMenuNotes = true
            } else {
                controllers[viewControllerIndex - 1].page.hiddenPlaces()
                controllers[viewControllerIndex - 1].isMenuNotes = false
            }
            
            toFront = false
            
            return controllers[viewControllerIndex - 1]
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex > controllers.count - 2 {
            return nil
        }
        
        if controllers.count > (viewControllerIndex + 1) {
            actualPage = viewControllerIndex + 1
            controllers[viewControllerIndex + 1].score = self.score

            if option == .notes || (currentOption == .firstBar && option == .general) {
                controllers[viewControllerIndex + 1].page.showPlaces()
                controllers[viewControllerIndex + 1].isMenuNotes = true
            } else {
                controllers[viewControllerIndex + 1].page.hiddenPlaces()
                controllers[viewControllerIndex + 1].isMenuNotes = false
            }
            
            toFront = true
            
            return controllers[viewControllerIndex + 1]
        }
        
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstController = viewControllers?.first,
            let firstViewControllerIndex = controllers.index(of: firstController as! PageScoreViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
//        print("Entrou")
    }

}

extension UIPageViewController {
    func removeTapRecognizer() {
        let gestureRecognizers = self.gestureRecognizers
        
        var tapGesture: UIGestureRecognizer?
        gestureRecognizers.forEach { recognizer in
            if recognizer.isKind(of: UITapGestureRecognizer.self) {
                tapGesture = recognizer
            }
        }
        if let tapGesture = tapGesture {
            self.view.removeGestureRecognizer(tapGesture)
        }
    }
}

enum OptionSelectedEdit {
    case firstBar
    case endBar
    case repetition8
    case repetition71
    case repetition62
    case repetition16
    case repetition151
    case repetition142
    case repetitionmanual
    case repetitiondelete
    case deleteBar
    case segno
    case coda
    case toCoda
    case dsAlCoda
    case dsAlFine
    case dcAlCoda
    case dcAlFine
    case fine
    case dsdcdelete
    case none
    case addWhole
    case addHalf
    case addQuarter
    case addQuaver
    case addSemiQuaver
    case addFlat
    case addSharp
    case addNatural
    case addPauseWhole
    case addPauseHalf
    case addPauseQuarter
    case addPauseQuaver
    case addPauseSemiQuaver
    case canalyseManual
}

enum OptionMenuEdit {
    case general
    case key
    case info
    case canalyse
    case repetition
    case dsdc
    case notes
}

/*--------------Usar depois--------------*/

/*
 let vN = [ "A", "B", "C", "D", "E", "F", "G" ]
 let vD = [ 1, 1, 1, 1, 1, 1, 1 ]
 
 let dict = Dictionary(uniqueKeysWithValues: zip(vN, vD))
 */


