//
//  InstrumentsViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/7/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class InstrumentsViewController: UIViewController {

    lazy var collectionView:UICollectionView = {
        let widthMenu = (view.frame.width * 0.8) / 6.0
        let border: CGFloat = widthMenu / 3.0
        let x: CGFloat = 0
        let y: CGFloat = 80.0 + view.frame.width * 0.06 + view.frame.width * 0.08
        let widht  : CGFloat = view.frame.width
        let height : CGFloat = view.frame.height - y
        let rect = CGRect(x: x + border, y: y, width: widht - (border * 2.0), height: height)
        var cv = UICollectionView(frame: rect, collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.alwaysBounceVertical = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(InstrumentsCollectionViewCell.self, forCellWithReuseIdentifier: "cellInstrument")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        return flow
    }()
    
    lazy var items:NSMutableArray = {
        var it:NSMutableArray = NSMutableArray()
        return it
    }()
    
    var lblWelcome = UILabel()
    var lblChoose = UILabel()
    var segment = UISegmentedControl()
    
    let defaults = UserDefaults.standard
    
    var isMidi: Bool {
        if defaults.bool(forKey: "MIDI") {
            return true
        } else {
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(red: 8 / 255, green: 78 / 255, blue: 147 / 255, alpha: 1.0)

        self.items.addObjects(from: ["Piano", "Piano Monophonic", "Piano", "Piano", "Piano", "Piano", "Piano", "Piano"])
        self.view.addSubview(self.collectionView)
        
        lblWelcome = UILabel(frame: CGRect(x: 0.0, y: 40.0, width: view.frame.width, height: view.frame.width * 0.05))
        lblWelcome.textAlignment = NSTextAlignment.center
        lblWelcome.text = "Welcome!"
        lblWelcome.font = UIFont(name: "Lato-Bold", size: lblWelcome.frame.height * 0.7)
        lblWelcome.textColor = UIColor.white
        view.addSubview(lblWelcome)
        
        lblChoose = UILabel(frame: CGRect(x: 0.0, y: 40.0 + view.frame.width * 0.06, width: view.frame.width, height: view.frame.width * 0.05))
        lblChoose.textAlignment = NSTextAlignment.center
        lblChoose.text = "Choose your instrument"
        lblChoose.font = UIFont(name: "Lato-Regular", size: lblWelcome.frame.height * 0.55)
        lblChoose.textColor = UIColor.white
        view.addSubview(lblChoose)
    }

    override func loadView() {
        super.loadView()

        // Initialize
        let items = ["Acoustic", "MIDI"]
        
        segment = UISegmentedControl(items: items)
        
        if isMidi {
            segment.selectedSegmentIndex = 1
        } else {
            segment.selectedSegmentIndex = 0
        }

        // Set up Frame and SegmentedControl
        let width  : CGFloat = view.frame.width * 0.28
        let height : CGFloat = 23.0
        let x = view.frame.midX - (width / 2.0)
        let y = 40.0 + view.frame.width * 0.06 + view.frame.width * 0.08
        segment.frame = CGRect(x: x, y: y, width: width, height: height)
        
        // Style the Segmented Control
        segment.layer.cornerRadius = 5.0  // Don't let background bleed
        segment.backgroundColor = UIColor.clear
        segment.tintColor = UIColor.white
        
        // Add target action method
        segment.addTarget(self, action: #selector(changeColor(sender:)), for: .valueChanged)
        
        // Add this custom Segmented Control to our view
        self.view.addSubview(segment)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        lblWelcome.frame.size = CGSize(width: view.frame.width, height: view.frame.width * 0.05)
        lblChoose.frame.size  = CGSize(width: view.frame.width, height: view.frame.width * 0.05)

        var x = view.frame.midX - ( segment.frame.width / 2.0)
        var y = segment.frame.origin.y
        segment.frame.origin = CGPoint(x: x, y: y)
        
        x = 0
        y = 80 + (view.frame.height * 0.08) + view.frame.height * 0.04
        let widthMenu = (view.frame.width * 0.8) / 6.0
        let border: CGFloat = widthMenu / 3.0
        let widht  : CGFloat = view.frame.width
        let height : CGFloat = view.frame.height - y
        let rect = CGRect(x: x + border, y: y, width: widht - (border * 2.0), height: height)
        collectionView.frame = rect
    }
    
    @objc func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            defaults.set(false, forKey: "MIDI")
        case 1:
            defaults.set(true, forKey: "MIDI")
        case 2:
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("Entrou deinit InstrumentsViewController")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InstrumentsViewController:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let parameter: CGFloat = 414.0
        let factor: CGFloat = view.frame.width / parameter
        let width:CGFloat = 80.0 * factor
        
        let height:CGFloat = width * 1.3
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellInstrument", for: indexPath as IndexPath) as! InstrumentsCollectionViewCell
        cell.setCardText(text: self.items[indexPath.row] as! String)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        
        if index == 1 {
            defaults.set(true, forKey: "Mono")
        } else {
            defaults.set(false, forKey: "Mono")
        }
        
        dismiss(animated: true, completion: nil)
//        let cell = collectionView.cellForItem(at: indexPath as IndexPath)
        
    }

}
