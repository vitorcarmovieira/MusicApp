//
////
////  HelpViewController.swift
////  Music-Mate-New-Design
////
////  Created by Nicolas Fernandes de Lima on 05/04/2018.
////  Copyright © 2018 LT Music Developer. All rights reserved.
////
//
//import UIKit
//
//class HelpViewController: UIViewController {
//    
//    var images = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
//    var titles = ["My Music", "Compose", "Store", "Profile", "Into Field"]
//    var DescriptionLevel1 = ["Define the key-signature. In EDIT this will migrate the fixe flats and sharps to the key-signature without changing the actual notes. If importing a MIDI file, this may or may not come with the right key-signature. Especially MIDI in C, this may be the wrong key-signature.", "Drawing a line between all notes that should be part of the upper pentagram (righthand) and the lower pentagram (left hand), the notes will be organized accordingly.\n After drawing the line, click on [APPLY] or [DONE]. You will see the notes change incolour as they change sides. The app has a default line right on the central C.", "The app automatically prolonges a note to the next note. However, in multiple-voices on either the right and or the left hand, you may need to define which voice a note is in. By drawing a line between the voices, the notes will organize them accordingly. You will see the notes change incolour as they change sides. This button is or voice 3 and 4 in the right hand. \n After drawing the line, click on [APPLY] or [DONE]. You will see the notes change incolour as they change sides.", "You may define a voice  (1, 2, 3or 4) of any individual note manually, by activating the button, clicking on the note and choose the voice.", "This is the same function as the [8] button, but now creating a 16-bar repetition."]
//    let descriptionCell1 = ["inerginrg", "hbadiub", "inerginrg", "hbadiub","inerginrg"]
//    let imagesCell1 = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"),#imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "prof_off")]
//    
//    var collectionContentDescriptions = ["Define the key-signature. In EDIT this will migrate the fixe flats and sharps to the key-signature without changing the actual notes. If importing a MIDI file, this may or may not come with the right key-signature. Especially MIDI in C, this may be the wrong key-signature.", "Drawing a line between all notes that should be part of the upper pentagram (righthand) and the lower pentagram (left hand), the notes will be organized accordingly.\n After drawing the line, click on [APPLY] or [DONE]. You will see the notes change incolour as they change sides. The app has a default line right on the central C.", "The app automatically prolonges a note to the next note. However, in multiple-voices on either the right and or the left hand, you may need to define which voice a note is in. By drawing a line between the voices, the notes will organize them accordingly. You will see the notes change incolour as they change sides. This button is or voice 3 and 4 in the right hand. \n After drawing the line, click on [APPLY] or [DONE]. You will see the notes change incolour as they change sides.", "You may define a voice  (1, 2, 3or 4) of any individual note manually, by activating the button, clicking on the note and choose the voice.", "This is the same function as the [8] button, but now creating a 16-bar repetition."]
//    var collectionContentImages = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
//    
//    @IBOutlet weak var btnBack: UIButton!
//    
//    
//    
//
//    @IBOutlet weak var helpCollectionView: UICollectionView!
//    @IBOutlet weak var ctrlPage: UIPageControl!
//    var flowLaytou: UICollectionViewFlowLayout!
//    var aux = false
//    var nivel = 1
//    
//    
//    @IBAction func backBtnAction(_ sender: Any) {
//        
//        self.nivel = 1
//        collectionContentDescriptions.removeAll()
//        collectionContentImages.removeAll()
//        
//        collectionContentDescriptions = DescriptionLevel1
//        collectionContentImages = images
//        helpCollectionView.reloadData()
//        self.btnBack.isHidden = true
//        
//        
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.btnBack.isHidden = true
//        flowLaytou = UICollectionViewFlowLayout()
//        flowLaytou.itemSize = CGSize(width: self.helpCollectionView.frame.width, height: self.helpCollectionView.frame.height * 0.19)
//        flowLaytou.scrollDirection = .horizontal
//        flowLaytou.minimumLineSpacing = 0.0
//        flowLaytou.minimumInteritemSpacing = -1.0
//        flowLaytou.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        helpCollectionView.collectionViewLayout = flowLaytou
//        
//        helpCollectionView.isPagingEnabled = true
//        helpCollectionView.isScrollEnabled = true
//        
//        
//        
////        helpCollectionView.
//
//    }
//    
//    func helpHome(){
//        
//        self.nivel = 1
//        collectionContentDescriptions.removeAll()
//        collectionContentImages.removeAll()
//        
//        collectionContentDescriptions = DescriptionLevel1
//        collectionContentImages = images
//        helpCollectionView.reloadData()
//        self.btnBack.isHidden = true
//        
//    }
//    
//}
//
//extension HelpViewController: UICollectionViewDelegate, UICollectionViewDataSource {
//    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//      
//        
//        if(nivel == 1){
//            if (indexPath.section == 0 && indexPath.row == 0){
//                collectionContentDescriptions.removeAll()
//                collectionContentImages.removeAll()
//                collectionContentDescriptions = descriptionCell1
//                collectionContentImages = imagesCell1
//                helpCollectionView.reloadData()
//                self.btnBack.isHidden = false
//                nivel = 2
//                
//            }
//        }
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        
//        return collectionContentImages.count
//    }
//    
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReusableCell", for: indexPath) as!helpCollectionViewCell
//        let myColor : UIColor = UIColor( red: 0.86, green: 0.86, blue:0.86, alpha: 1.0 )
//        cell.layer.borderColor = myColor.cgColor
//        
//        if(nivel == 1){
//            cell.imgHelp.image = self.collectionContentImages[indexPath.item]
//            cell.lblTitle.text = self.titles[indexPath.item]
//            cell.lblDescription.text = self.collectionContentDescriptions[indexPath.item]
//            cell.isLevelOne()
//            return cell
//        }
//        else{
//            cell.imgHelp.image = self.collectionContentImages[indexPath.item]
//            cell.lblDescription.text = self.collectionContentDescriptions[indexPath.item]
//            cell.isLevelTwo()
//            return cell
//        }
//        
//    }
//    
//    
//    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, fortitleAt indexPath: IndexPath) {
//        
//        self.ctrlPage.currentPage = indexPath.section
//    }
//    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        
//        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
//        ctrlPage.currentPage = Int(pageNumber)
//        ctrlPage.numberOfPages = 1
//    }
//}
