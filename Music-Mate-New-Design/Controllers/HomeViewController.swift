//
//  HomeViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/5/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreMIDI
import CoreAudio
import AudioToolbox
import Foundation

var authors  : [ModelAuthor] = []
var artists  : [ModelArtist] = []
var publishes  : [ModelCopyright] = []

class HomeViewController: UIViewController, UIDocumentPickerDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    weak var storeView : StoreView!
    
    weak var popup : PopUpLongPress!
    weak var menu        : MenuHomeView!
    // weak var menuInstrument : InstrumentSelectedHomeView!
    weak var searchView  : SearchView!
    weak var footer      : Footer!
    weak  var storeFilter : StoreFilter!
    weak var popUpAlert  : PopUpAlert!
    weak var btnnewcomposition : UIButton!
    
    var instrumentSelected = ModelTypeInstrument()
    var btnPlayOriginalScore : UIButton!
    var btnPlayPlayedScore : UIButton!
    
    
    var profileView = ProfileView()
    var myActivityIndicator: UIActivityIndicatorView!
    var isMonophonic = false
    var helpView = HelpView()
    var btn : UIButton!
    
    lazy var collectionView:UICollectionView = {
        let widthMenu = (view.frame.width * 0.8) / 6.0
        let border: CGFloat = widthMenu / 3.0
        let x: CGFloat = view.frame.width * 0.032
        let y: CGFloat = 40 + (view.frame.height * 0.08) + view.frame.height * 0.04
        let widht  : CGFloat = view.frame.width
        let height : CGFloat = view.frame.height - y
        let rect = CGRect(x: 0, y: y, width: self.view.frame.width*0.952, height: height - (border * 0.62))
        var cv = UICollectionView(frame: rect, collectionViewLayout: self.flowLayout)
        cv.collectionViewLayout = flowLayout
        cv.delegate = self
        cv.dataSource = self
        cv.isPagingEnabled = false
        cv.bounces = false
        cv.alwaysBounceVertical = false
        cv.showsVerticalScrollIndicator = false
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(HomeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    lazy var collectionViewAllStore:UICollectionView = {
        let widthMenu = (view.frame.width * 0.8) / 6.0
        let border: CGFloat = widthMenu / 3.0
        let x: CGFloat = 0
        let y: CGFloat = 40 + (view.frame.height * 0.08) + view.frame.height * 0.04
        let widht  : CGFloat = view.frame.width
        let height : CGFloat = view.frame.height - y
        let rect =  CGRect(x: 0, y: self.view.frame.height*0.475, width: self.view.frame.width*0.952, height: self.view.frame.height*0.5)
        var cv = UICollectionView(frame: rect, collectionViewLayout: self.flowLayout2)
        cv.delegate = self
        cv.dataSource = self
        cv.isPagingEnabled = false
        cv.bounces = false
        cv.alwaysBounceVertical = false
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(HomeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    lazy var collectionViewNewsStore:UICollectionView = {
        let widthMenu = (view.frame.width * 0.8) / 6.0
        let border: CGFloat = widthMenu / 3.0
        let x: CGFloat = 0
        let y: CGFloat = 40 + (view.frame.height * 0.08) + view.frame.height * 0.04
        let widht  : CGFloat = view.frame.width
        let height : CGFloat = view.frame.height - y
        let rect =  CGRect(x: 0, y: self.view.frame.height*0.182, width: self.view.frame.width*0.952, height: self.view.frame.height*0.25)
        var cv = UICollectionView(frame: rect, collectionViewLayout: self.flowLayout3)
        cv.delegate = self
        cv.dataSource = self
        cv.isPagingEnabled = false
        cv.bounces = false
        cv.alwaysBounceVertical = false
        cv.alwaysBounceHorizontal = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(HomeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    lazy var flowLayout3:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(self.view.frame.height*0.0049 , self.view.frame.width*0.039, self.view.frame.height*0.0049, 0.0)
        flow.minimumLineSpacing = CGFloat(self.view.frame.width*0.045)
        flow.itemSize = CGSize(width: self.view.frame.width*0.195, height: self.view.frame.height*0.195)
        flow.scrollDirection = .horizontal
        
        return flow
    }()
    lazy var flowLayout2:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.itemSize = CGSize(width: self.view.frame.width*0.195, height: self.view.frame.height*0.195)
        flow.sectionInset = UIEdgeInsetsMake(self.view.frame.height*0.0097 , self.view.frame.width*0.039, self.view.frame.height*0.0049, 0.0)
        flow.scrollDirection = .vertical
        
        return flow
    }()
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.itemSize = CGSize(width: self.view.frame.width*0.195, height: self.view.frame.height*0.195)
        flow.sectionInset = UIEdgeInsetsMake(self.view.frame.height*0.0097 , self.view.frame.width*0.039, self.view.frame.height*0.0049, 0.0)
        flow.scrollDirection = .vertical
        return flow
    }()
    
    lazy var items:NSMutableArray = {
        var it:NSMutableArray = NSMutableArray()
        return it
    }()
    
    lazy var itemsNews:NSMutableArray = {
        var it:NSMutableArray = NSMutableArray()
        return it
    }()
    var longPress : UILongPressGestureRecognizer!
    var longPressNews : UILongPressGestureRecognizer!
    var longPressAll : UILongPressGestureRecognizer!
    var tapOnScreen : UITapGestureRecognizer!
    var tapOnScreen2 : UITapGestureRecognizer!
    var tapOnScreen3 : UITapGestureRecognizer!
    private var activeCell : HomeCollectionViewCell!
    var lblMM = UILabel()
    var searchBar : UISearchBar!
    var tableViewResults : UITableView!
    var tableViewCategoryMyMusic : UITableView!
    var tableViewCategoryMyCompositions : UITableView!
    var fakebackGroundTableViewResultMyMusic : UIView!
    var fakebackGroundTableViewResultMyCompositions : UIView!
    var fakebackGroundPopUpLongPress : UIView!
    
    var titleFooter = ""
    
    var scores: [ModelScore] = []
    var scoresNews: [ModelScore] = []
    
    // footer.btnLblInstrument.setTitle(titleFooter, for: .normal)


func serviceGetAuthors(urlToRequest: String, method: String) {
    var request = URLRequest(url:URL(string:urlToRequest)!)
    request.httpMethod = "GET"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
    print(urlToRequest)
    let session = URLSession.shared
    
    session.dataTask(with: request){data, response, err in
        //Guard: ws there error ?
        guard(err == nil) else {
            print("\(String(describing: err))")
            return
        }
        //Guard: check was any data returned?
        guard let data = data else{
            print("no data return")
            return
        }
        //Convert Json to Object
        let parseResult: [String:AnyObject]!
        do{//
            parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
            print("sda: ",parseResult)
            if let data1 = parseResult["results"] as? [String:AnyObject] {
                print("a", data1)
                if let data = data1["composers"] as? [AnyObject] {
                    for dataI in data {
                        let d = dataI as! [String:AnyObject]
                        
                        let author   = ModelAuthor()
                        author.idUser    = d["_id"] as! String
                        author.firstName  = d["firstName"] as! String
                        if(d["lastName"] != nil){
                            author.lastName  = d["lastName"] as! String
                        }
                        authors.append(author)
                    }
                }
                  DispatchQueue.main.async(execute: {
                    self.storeFilter.tbAuthor.reloadData()
                    
                  })
            }
        } catch {
            print("Could not parse data as Json \(data)")
            return
        }
        
        }.resume()
    
    
}
    
    
    func initButtonNewComposer(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            btn.isHidden = true
            view.addSubview(btn)
            
            button = btn
            
        } else {
            button.frame = frame
        }
        
    }
    
    
    @objc func networkStatusChanged(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
        
        
        var email = ""
        
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
            print(user.token)
        }
        if(defaults.integer(forKey: "entrou") == 1 && email == "noreply@ltmdev.com"){
            // self.salvaMidiApple()
            defaults.set(2, forKey: "entrou")
        }
        if(defaults.integer(forKey: "entrou") == 0){
            defaults.set(1, forKey: "entrou")
            defaults.set(true, forKey: "ArrangementsMM")
            defaults.set(true, forKey: "StoreMM")
            defaults.set(true, forKey: "CompositionsMM")
            defaults.set(true, forKey: "ArrangementsMC")
            defaults.set(true, forKey: "CompositionsMC")
            defaults.set("All MyMusic", forKey: "BuscaSalvaMM")
            defaults.set("All MyCompositions", forKey: "BuscaSalvaMC")
            //  self.salvaMidiApple()
        }
        
        
        fakebackGroundTableViewResultMyMusic = UIView(frame: self.view.frame)
        fakebackGroundTableViewResultMyMusic.backgroundColor = UIColor.clear
        fakebackGroundTableViewResultMyCompositions = UIView(frame: self.view.frame)
        fakebackGroundTableViewResultMyCompositions.backgroundColor = UIColor.clear
        
        fakebackGroundPopUpLongPress = UIView(frame: self.view.frame)
        fakebackGroundPopUpLongPress.backgroundColor = UIColor.clear
        
        self.view.addSubview(fakebackGroundPopUpLongPress)
        self.fakebackGroundPopUpLongPress.isHidden = true
        
        self.view.addSubview(fakebackGroundTableViewResultMyCompositions)
        self.fakebackGroundTableViewResultMyCompositions.isHidden = true
        self.view.addSubview(fakebackGroundTableViewResultMyMusic)
        self.fakebackGroundTableViewResultMyMusic.isHidden = true
        longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPress.minimumPressDuration = 0.5
        longPressNews = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressNewsStore))
        longPressNews.minimumPressDuration = 0.5
        longPressAll = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressAllStore))
        longPressAll.minimumPressDuration = 0.5
        tapOnScreen = UITapGestureRecognizer(target: self, action: #selector(tapPress(gesture:)))
        tapOnScreen.cancelsTouchesInView = false
        tapOnScreen.numberOfTapsRequired = 1
        tapOnScreen2 = UITapGestureRecognizer(target: self, action: #selector(tapPressMyCompositions(gesture:)))
        tapOnScreen2.cancelsTouchesInView = false
        tapOnScreen2.numberOfTapsRequired = 1
        tapOnScreen3 = UITapGestureRecognizer(target: self, action: #selector(tapPressPopUp(gesture:)))
        tapOnScreen3.cancelsTouchesInView = false
        tapOnScreen3.numberOfTapsRequired = 1
        self.fakebackGroundPopUpLongPress.addGestureRecognizer(tapOnScreen3)
        self.fakebackGroundTableViewResultMyMusic.addGestureRecognizer(tapOnScreen)
        self.fakebackGroundTableViewResultMyCompositions.addGestureRecognizer(tapOnScreen2)
        view.backgroundColor =  UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
        
        
        
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
        }
        
        scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: "")
        
        for score in scores {
            self.items.add(score.title)
        }
        
        self.view.addSubview(self.collectionView)
        self.view.addSubview(self.collectionViewAllStore)
        self.view.addSubview(self.collectionViewNewsStore)
        self.collectionViewAllStore.isHidden = true
        self.collectionViewNewsStore.isHidden = true
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        updateMenu()
        self.initButtonNewComposer(button: &btnnewcomposition, image: #imageLiteral(resourceName: "ComposeV"), frame: CGRect(x:  view.frame.width*0.864, y: view.frame.height*0.87, width: view.frame.width*0.105, height: view.frame.width*0.105))
        
        collectionView.addGestureRecognizer(longPress)
        
        
        collectionViewNewsStore.addGestureRecognizer(longPressNews)
        collectionViewAllStore.addGestureRecognizer(longPressAll)
        menu.function = .myMusic
        
        let user = UserManager.sharedUserManager
        let token = user.token
        user.server = "http://37.208.116.28"
        
        let genrsAddeds = DAOGenre.getInstance().getAllGenrs()
        
        if genrsAddeds.count == 0 {
            serviceGetGenders(urlToRequest: user.server + ":3003/genre?token=" + token, method: ServiceRemote.Method.GET.rawValue)
        }
        
        let countriesAddeds = DAOCountry.getInstance().getAllCountries()
        
        if countriesAddeds.count == 0 {
            serviceGetCountries(urlToRequest: user.server + ":3003/country?token=" + token, method: ServiceRemote.Method.GET.rawValue)
        }
        
        let instrumentsAddeds = DAOInstrument.getInstance().getAllInstruments()
        
        if instrumentsAddeds.count == 0 {
            serviceGetInstruments(urlToRequest: user.server + ":3003/instrument/all?token=" + token, method: ServiceRemote.Method.GET.rawValue)
        }
        self.initPopUp()
    }
    
    
    func getCellAtPoint(point: CGPoint) -> HomeCollectionViewCell? {
        // Function for getting item at point. Note optionals as it could be nil
        let indexPath = collectionView.indexPathForItem(at: point)
        var cell : HomeCollectionViewCell?
        
        if indexPath != nil {
            cell = collectionView.cellForItem(at: indexPath!) as? HomeCollectionViewCell
        } else {
            cell = nil
        }
        
        return cell
        
        
     
        
    }
    
    
    func animationDuration() -> Double {
        return 0.5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is HelpView {
                self.view.bringSubview(toFront: subView)
            }
            if subView is StoreFilter {
                self.view.bringSubview(toFront: subView)
            }
        }
        
        super.viewDidLayoutSubviews()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        //        updateMenu()
        
        if fromInterfaceOrientation.isPortrait {
            menu.direction = .horizontal
            // searchView.direction = .horizontal
        } else {
            menu.direction = .vertical
            //  searchView.direction = .vertical
        }
        
        self.flowLayout.invalidateLayout()
    }
    
    @objc func tapPressMyCompositions(gesture : UITapGestureRecognizer) {
        if(menu.function == .myComposition){
            self.tableViewCategoryMyCompositions.isHidden = true
            self.fakebackGroundTableViewResultMyCompositions.isHidden = true
        }
        
        
        //        if(self.tableViewResults != nil){
        //            self.tableViewResults.isHidden = true
        //
        //        }
        //            self.view.endEditing(true)
        //  }
        
    }
    
    @objc func tapPress(gesture : UITapGestureRecognizer) {
        if(menu.function == .myMusic){
            self.tableViewCategoryMyMusic.isHidden = true
            self.fakebackGroundTableViewResultMyMusic.isHidden = true
        }
    }
    
    
    @objc func tapPressPopUp(gesture : UITapGestureRecognizer) {
        self.fakebackGroundPopUpLongPress.isHidden = true
        self.popup.isHidden = true
        self.shadowView.isHidden = true
    }
    
    func initPopUp(){
        if popup == nil  {
            shadowView = UIView(frame: CGRect(x: self.view.frame.width*0.05, y: self.view.frame.height*0.2, width: self.view.frame.width*0.9, height: self.view.frame.height*0.6))
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: 0, height: 3)
            shadowView.layer.shadowOpacity = 1.0
            shadowView.layer.shadowRadius = 5
            let m = PopUpLongPress(frame: CGRect(x: 0, y: 0, width: self.shadowView.frame.width, height: self.shadowView.frame.height))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 0.98)
            m.layer.cornerRadius = 10
            m.layer.borderWidth = 3
            m.clipsToBounds = true
            m.layer.borderColor = UIColor.black.withAlphaComponent(0.01).cgColor
            self.view.addSubview(shadowView)
            shadowView.addSubview(m)
            popup = m
            
            
        }
        popup.isHidden = true
        shadowView.isHidden = true
        fakebackGroundPopUpLongPress.isHidden = true
    }
    
    
    var shadowView : UIView!
    
    func retornaStringKeySignature(key: Int) -> String{
        switch key {
        case 1:
            return "C/Am"
        case 2:
            return "Db/Bbm"
        case 3:
            return "Db/Bbm"
        case 4:
            return "D/Bm"
        case 5:
            return "Eb/Cm"
        case 6:
            return "E/C#m"
        case 7:
            return "F/Dm"
        case 8:
            return "F#/D#m"
        case 9:
            return "F#/D#m"
        case 10:
            return "G/Em"
        case 11:
            return "Ab/Fm"
        case 12:
            return "A/F#m"
        case 13:
            return "Bb/Gm"
        case 14:
            return "B/G#m"
        default:
            return "0"
        }
    }
    
    @objc func handleLongPressAllStore(gesture : UILongPressGestureRecognizer!) {
        //        let p = gesture.location(in: self.collectionViewAllStore)
        //
        //        if gesture.state != .recognized {
        //            if let indexPath = self.collectionViewAllStore.indexPathForItem(at: p) {
        //                let user = UserManager.sharedUserManager
        //                let token = user.token
        //                user.server = "http://37.208.116.28"
        //                serviceGetPartitureFromStore(urlToRequest: user.server + ":3003/store/score/id?token=\(token)&id=\(scores[indexPath.row].idStore)", method: ServiceRemote.Method.GET.rawValue)
        //
        //
        //                popup.lblTitleMusic.text = scoreSelected.title
        //                popup.lblTonality.text = self.retornaStringKeySignature(key: scoreSelected.configurations[0].key)
        //                popup.lblTimeSignature.text = ("\(scoreSelected.configurations[0].numerator)/\(scoreSelected.configurations[0].denominator)")
        //                popup.lblNameComposer.text = "Unknow"
        //                fakebackGroundPopUpLongPress.isHidden = false
        //                self.shadowView.isHidden = false
        //                self.popup.isHidden = false
        //            }
        //        }
    }
    
    @objc func handleLongPressNewsStore(gesture : UILongPressGestureRecognizer!) {
        //        let p = gesture.location(in: self.collectionViewNewsStore)
        //
        //        if gesture.state != .recognized {
        //            if let indexPath = self.collectionViewNewsStore.indexPathForItem(at: p) {
        //                let user = UserManager.sharedUserManager
        //                let token = user.token
        //                user.server = "http://37.208.116.28"
        //                serviceGetPartitureFromStore(urlToRequest: user.server + ":3003/store/score/id?token=\(token)&id=\(scoresNews[indexPath.row].idStore)", method: ServiceRemote.Method.GET.rawValue)
        //
        //
        //                popup.lblTitleMusic.text = scoreSelected.title
        //                popup.lblTonality.text = self.retornaStringKeySignature(key: scoreSelected.configurations[0].key)
        //                popup.lblTimeSignature.text = ("\(scoreSelected.configurations[0].numerator)/\(scoreSelected.configurations[0].denominator)")
        //                popup.lblNameComposer.text = "Unknow"
        //                fakebackGroundPopUpLongPress.isHidden = false
        //                self.shadowView.isHidden = false
        //                self.popup.isHidden = false
        //            }
        //        }
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        let p = gesture.location(in: self.collectionView)
        
        if gesture.state != .recognized {
            if let indexPath = self.collectionView.indexPathForItem(at: p) {
                self.scoreSelected = scores[indexPath.row]
                popup.lblTitleMusic.text = scores[indexPath.row].title
                popup.lblSubTitleMusic.text = scores[indexPath.row].subtitle
                self.popup.lblTonality.text = self.retornaStringKeySignature(key: self.scoreSelected.configurations[0].key)
                popup.lblTimeSignature.text = ("\(scores[indexPath.row].configurations[0].numerator)/\(scores[indexPath.row].configurations[0].denominator)")
                if(scores[indexPath.row].author.count>0){
                    popup.lblNameComposer.text = "\(scores[indexPath.row].author[0].firstName) \(scores[indexPath.row].author[0].lastName)"
                }else {
                    popup.lblNameComposer.text = ""
                }
                fakebackGroundPopUpLongPress.isHidden = false
                self.shadowView.isHidden = false
                self.popup.isHidden = false
                
                
                updatePages()
            }
        }
    }
    
    var score = DrawScore()
    
    func updatePages() {
        
        for sub in popup.partituraView.subviews {
            sub.removeFromSuperview()
        }
        //        let frame = popup.partituraView.frame
        
        //        popup.partituraView.removeFromSuperview()
        //        popup.partituraView = UIView(frame: frame)
        //        popup.addSubview(popup.partituraView)
        //        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        //        activity.backgroundColor = UIColor.clear
        //
        //        if activity.superview == nil {
        //            view.addSubview(activity)
        //        }
        
        let x:CGFloat = 0// (view.frame.height * 0.08) * 0.3
        let y:CGFloat = 0// view.frame.height * 0.08
        let width  = popup.partituraView.frame.width * 0.9
        let height = popup.partituraView.frame.height
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        //        dataSource = self
        
        //        DispatchQueue.global(qos: .userInteractive).async {
        self.scoreSelected.typeInstrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
        self.score.dataSource = self.scoreSelected
        
        /// Aqui pode dar merda
        
        if let source = self.score.dataSource {
            source.generatePauses()
//
//            for key in source.configurations {
//                if key.key != -1 {
//                    menuGeneral.setKey(key: key.key)
//                    break
//                }
//            }
//            storeFilter.setTitle(source.title)
//            storeFilter.setArranger(source.arranger)
//            storeFilter.setGenre(source.genre.name)
//            storeFilter.setSubgenre(source.subgenre.name)
//            storeFilter.setSubtitle(source.subtitle)
//            storeFilter.setComposer("\(source.author.firstName) \(source.author.lastName)")
//            storeFilter.setCategoryInstrument(source.instrument.name)
//            storeFilter.setInstrument(source.typeInstrument.name)
//
//
//            if(source.difficulty == 1){
//                storeFilter.setDifficult("Easy")
//            }
//            else if(source.difficulty == 2){
//                storeFilter.setDifficult("Medium")
//            }
//            else if(source.difficulty == 3){
//                storeFilter.setDifficult("Hard")
//            }
//
//            if(source.isPoliphonic == 0){
//                storeFilter.setScoreType("Monophonic")
//            } else {
//                storeFilter.setScoreType("Polyphonic")
//            }
//            if(source.configurations[0].key) > 0{
//                storeFilter.setKeyType("Major")
//            }else {
//                storeFilter.setKeyType("Minor")
//            }
            storeFilter.txtTitle.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
//            storeFilter.txtSubtitle.addTarget(self, action: #selector(txtSubTitleDidChange(textField:)), for: .editingChanged)
//            storeFilter.txtSubtitle.addTarget(self, action: #selector(txtSubtitleDidChange(textField:)), for: .editingChanged)
        }
        
        self.score.state = .playing
        self.score.verifyState()
        
        //            DispatchQueue.main.async(execute: {
        self.score.createPages(parameters: parameters)
        self.score.pages[0].draw(parentView: self.popup.partituraView)
        //            })
        //        }
    }
    
    var itensCategory = ["Downloads","Compositions","Arrangements" ]
    //TABLEVIEW---------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tableViewCategoryMyMusic){
            return itensCategory.count
        }
        if(tableView == tableViewCategoryMyCompositions){
            return itensCategory.count-1
        }
        else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        if(tableView == tableViewCategoryMyMusic){
            cell.textLabel?.text = itensCategory[indexPath.item]
            cell.textLabel?.textAlignment = .left
            // cell.accessoryType = .checkmark
            cell.selectionStyle = .none
            cell.textLabel?.textColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Lato-Regular", size: self.view.frame.width*0.0255)
            return cell
            
        }
        if(tableView == tableViewCategoryMyCompositions ){
            cell.textLabel?.text = itensCategory[indexPath.item+1]
            cell.textLabel?.textAlignment = .left
            //   cell.accessoryType = .checkmark
            cell.selectionStyle = .none
            cell.textLabel?.textColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Lato-Regular", size: self.view.frame.width*0.0255)
            return cell
        }
        else {
            return cell
        }
    }
    
    let defaults = UserDefaults.standard
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var email = ""
        
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
        }
        if(tableView == tableViewCategoryMyMusic){
            
            if let cell = tableView.cellForRow(at: indexPath) {
                if(cell.accessoryType == .checkmark){
                    if(cell.textLabel!.text! == "Arrangements" && (defaults.bool(forKey: "CompositionsMM") == true || defaults.bool(forKey: "StoreMM") == true)){
                        cell.accessoryType = .none
                        defaults.set(false, forKey: "ArrangementsMM")
                        self.scores.removeAll()
                        self.items.removeAllObjects()
                        
                        if(defaults.bool(forKey: "CompositionsMM") == true && defaults.bool(forKey: "StoreMM") == true){
                            scores = DAOScore.getInstance().getScores("Store and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Store and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "CompositionsMM") == true && defaults.bool(forKey: "StoreMM") == false){
                            scores = DAOScore.getInstance().getScores("Compositions MyMusic", email: email, busca: "")
                            defaults.set("Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "CompositionsMM") == false && defaults.bool(forKey: "StoreMM") == true){
                            scores = DAOScore.getInstance().getScores("Store MyMusic", email: email, busca: "")
                            defaults.set("Store MyMusic", forKey: "BuscaSalvaMM")
                        }
                        for score in scores {
                            self.items.add(score.title)
                        }
                        self.collectionView.reloadData()
                    }
                    
                    if(cell.textLabel!.text! == "Downloads" && (defaults.bool(forKey: "CompositionsMM") == true || defaults.bool(forKey: "ArrangementsMM") == true)){
                        cell.accessoryType = .none
                        defaults.set(false, forKey: "StoreMM")
                        self.scores.removeAll()
                        self.items.removeAllObjects()
                        if(defaults.bool(forKey: "CompositionsMM") == true && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("Arrangements and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Arrangements and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "CompositionsMM") == true && defaults.bool(forKey: "ArrangementsMM") == false){
                            scores = DAOScore.getInstance().getScores("Compositions MyMusic", email: email, busca: "")
                            defaults.set("Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "CompositionsMM") == false && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("Arrangements MyMusic", email: email, busca: "")
                            defaults.set("Arrangements MyMusic", forKey: "BuscaSalvaMM")
                        }
                        for score in scores {
                            self.items.add(score.title)
                        }
                        self.collectionView.reloadData()
                    }
                    
                    if(cell.textLabel!.text! == "Compositions" && (defaults.bool(forKey: "StoreMM") == true || defaults.bool(forKey: "ArrangementsMM") == true)){
                        cell.accessoryType = .none
                        defaults.set(false, forKey: "CompositionsMM")
                        self.scores.removeAll()
                        self.items.removeAllObjects()
                        if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("Store and Arrangements MyMusic", email: email, busca: "")
                            defaults.set("Store and Arrangements MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "ArrangementsMM") == false){
                            scores = DAOScore.getInstance().getScores("Store MyMusic", email: email, busca: "")
                            defaults.set("Store MyMusic", forKey: "BuscaSalvaMM")
                        }
                        else if(defaults.bool(forKey: "StoreMM") == false && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("Arrangements MyMusic", email: email, busca: "")
                            defaults.set("Arrangements MyMusic", forKey: "BuscaSalvaMM")
                        }
                        for score in scores {
                            self.items.add(score.title)
                        }
                        self.collectionView.reloadData()
                    }
                    
                }
                else if(cell.accessoryType == .none){
                    self.scores.removeAll()
                    self.items.removeAllObjects()
                    cell.accessoryType = .checkmark
                    if(cell.textLabel?.text == "Arrangements"){
                        defaults.set(true, forKey: "ArrangementsMM")
                        if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "CompositionsMM") == true){
                            scores = DAOScore.getInstance().getScores("All MyMusic", email: email, busca: "")
                            defaults.set("All MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "StoreMM") == false && defaults.bool(forKey: "CompositionsMM") == true){
                            scores = DAOScore.getInstance().getScores("Arrangements and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Arrangements and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "CompositionsMM") == false){
                            scores = DAOScore.getInstance().getScores("Store and Arrangements MyMusic", email: email, busca: "")
                            defaults.set("Store and Arrangements MyMusic", forKey: "BuscaSalvaMM")
                        }
                    }
                    if(cell.textLabel?.text == "Compositions"){
                        defaults.set(true, forKey: "CompositionsMM")
                        if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("All MyMusic", email: email, busca: "")
                            defaults.set("All MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "StoreMM") == false && defaults.bool(forKey: "ArrangementsMM") == true){
                            scores = DAOScore.getInstance().getScores("Arrangements and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Arrangements and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "StoreMM") == true && defaults.bool(forKey: "ArrangementsMM") == false){
                            scores = DAOScore.getInstance().getScores("Store and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Store and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                    }
                    if(cell.textLabel?.text == "Downloads"){
                        defaults.set(true, forKey: "StoreMM")
                        if(defaults.bool(forKey: "ArrangementsMM") == true && defaults.bool(forKey: "CompositionsMM") == true){
                            scores = DAOScore.getInstance().getScores("All MyMusic", email: email, busca: "")
                            defaults.set("All MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "ArrangementsMM") == false && defaults.bool(forKey: "CompositionsMM") == true){
                            scores = DAOScore.getInstance().getScores("Store and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Store and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                        if(defaults.bool(forKey: "ArrangementsMM") == true && defaults.bool(forKey: "CompositionsMM") == false){
                            scores = DAOScore.getInstance().getScores("Arrangements and Compositions MyMusic", email: email, busca: "")
                            defaults.set("Arrangements and Compositions MyMusic", forKey: "BuscaSalvaMM")
                        }
                    }
                    
                    for score in scores {
                        self.items.add(score.title)
                    }
                    self.collectionView.reloadData()
                }
            }
        }
        
        if(tableView == tableViewCategoryMyCompositions){
            
            if let cell = tableView.cellForRow(at: indexPath) {
                
                
                if(cell.accessoryType == .checkmark){
                    if(cell.textLabel!.text! == "Arrangements" && defaults.bool(forKey: "CompositionsMC") == true){
                        cell.accessoryType = .none
                        defaults.set(false, forKey: "ArrangementsMC")
                        self.scores.removeAll()
                        self.items.removeAllObjects()
                        scores = DAOScore.getInstance().getScores("Compositions MyCompositions", email: email, busca: "")
                        for score in scores {
                            self.items.add(score.title)
                        }
                        self.collectionView.reloadData()
                        defaults.set("Compositions MyCompositions", forKey: "BuscaSalvaMC")
                        
                    }
                    else if(cell.textLabel!.text! == "Compositions" && defaults.bool(forKey: "ArrangementsMC") == true){
                        cell.accessoryType = .none
                        defaults.set(false, forKey: "CompositionsMC")
                        self.scores.removeAll()
                        self.items.removeAllObjects()
                        scores = DAOScore.getInstance().getScores("Arrangements MyCompositions", email: email, busca: "")
                        for score in scores {
                            self.items.add(score.title)
                        }
                        self.collectionView.reloadData()
                        defaults.set("Arrangements MyCompositions", forKey: "BuscaSalvaMC")
                    }
                }
                else if(cell.accessoryType == .none){
                    cell.accessoryType = .checkmark
                    if(cell.textLabel?.text == "Arrangements"){
                        defaults.set(true, forKey: "ArrangementsMC")
                    }
                    if(cell.textLabel?.text == "Compositions"){
                        defaults.set(true, forKey: "CompositionsMC")
                    }
                    self.scores.removeAll()
                    self.items.removeAllObjects()
                    scores = DAOScore.getInstance().getScores("All MyCompositions", email: email, busca: "")
                    for score in scores {
                        self.items.add(score.title)
                    }
                    self.collectionView.reloadData()
                    defaults.set("All MyCompositions", forKey: "BuscaSalvaMC")
                }
                
            }
        }
        
    }
    //----------------------------------------------------------------------------------------------
    
    //SEARCHBAR---------------------------------------------------------------------------------------------
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    if (menu.function != .shop) {
        if(searchText.count>0){
            self.scores.removeAll()
            self.items.removeAllObjects()
            var email = ""
            var local = ""
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }
            if(menu.function == .myMusic){
                local = (defaults.object(forKey: "BuscaSalvaMM")) as! String
            }
            if(menu.function == .myComposition){
                local = (defaults.object(forKey: "BuscaSalvaMC")) as! String
            }
            scores = DAOScore.getInstance().getScores("Search \(local)", email: email, busca: searchText)
            for score in scores {
                self.items.add(score.title)
            }
            self.collectionView.reloadData()
        }
        if(searchText.count==0){
            self.scores.removeAll()
            self.items.removeAllObjects()
            var email = ""
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }
            if(menu.function == .myMusic){
                scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: searchText)
            }
            if(menu.function == .myComposition){
                scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: searchText)
            }
            for score in scores {
                self.items.add(score.title)
            }
            self.collectionView.reloadData()
        //self.tableViewResults.isHidden = true
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("as")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("as")
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        
    }
    //-------------
    func updateMenu() {
        
        if storeView == nil {
            let m = StoreView(frame:CGRect(x: 0, y: view.frame.height*0.14, width: view.frame.width, height: view.frame.height*0.86))
            m.backgroundColor = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
            m.isHidden = true
            view.addSubview(m)
            storeView = m
            
        }
        
//        storeFilter.delegate    = self as! StoreFilterDelegate
//        storeFilter.isHidden    = true
        
        if footer == nil {
            let height = view.frame.height * 0.033
            let m = Footer(frame: CGRect(x: 0.0, y: view.frame.height - height, width: view.frame.width, height: height))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            footer = m
        } else {
            footer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menu == nil {
            let m = MenuHomeView(frame: CGRect(x: 0, y:0, width: view.frame.width, height: view.frame.height))
            m.backgroundColor = UIColor.clear
            view.addSubview(m)
            menu = m
        } else {
            menu.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        }
        
        if let customView = Bundle.main.loadNibNamed("ProfileView", owner: self, options: nil)?.first as? ProfileView {
            let y = (view.frame.height * 0.1)
            customView.frame = CGRect(x: 0, y: y, width: view.frame.width, height: view.frame.height - y)
            view.addSubview(customView)
            
            customView.translatesAutoresizingMaskIntoConstraints = false
            
            profileView = customView
        }
        
        if let customView = Bundle.main.loadNibNamed("PopUpAlert", owner: self, options: nil)?.first as? PopUpAlert {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            popUpAlert = customView
        }
        
        if let customView = Bundle.main.loadNibNamed("HelpViewX", owner: self, options: nil)?.first as? HelpView {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            helpView = customView
        }
        
        if storeFilter == nil {
            let y = (self.view.frame.height * 0.1)
            let m = StoreFilter(frame: CGRect(x: 0, y: y, width: view.frame.width, height: view.frame.height - y))
            m.backgroundColor = UIColor(red: 212/255, green: 224/255, blue: 235/255, alpha: 1.0)
            
            view.addSubview(m)
            m.isHidden = true
            storeFilter = m
        } else {
            storeFilter.frame = CGRect(x: 0, y: view.frame.height * 0.0878, width: view.frame.width, height: view.frame.height * 0.08)
            footer.isHidden = false
        }
        
        
        
//        if let customView = Bundle.main.loadNibNamed("FilterStoreView", owner: self, options: nil)?.first as? StoreFilterView {
//            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
//            view.addSubview(customView)
//            customView.isHidden = true
//            customView.translatesAutoresizingMaskIntoConstraints = false
//            filterStoreView = customView
//        }
        
        btn = UIButton(frame: CGRect(x: view.frame.width - (view.frame.height * 0.04) - 25, y: 30, width: view.frame.height * 0.06, height: view.frame.height * 0.06) )
        btn.setImage(#imageLiteral(resourceName: "btn_test_import"), for: .normal)
        btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
        //        if let user = DAOUser.getInstance().getUserActive() {
        //            if (user.login != "noreply@ltmdev.com"){
        view.addSubview(btn)
        //            }
        //        }
        btn.addTarget(self, action: #selector(actionBtnImport(sender:)), for: .touchDown)
        
        
        
        
        searchBar = UISearchBar(frame: CGRect(x: self.view.frame.width*0.05, y: self.view.frame.height*0.1147, width: self.view.frame.width*0.846, height: self.view.frame.height*0.034
        ))
        //        searchBar.layer.cornerRadius = 15.0
        //        searchBar.layer.borderWidth = 2
        //        searchBar.layer.borderColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0).cgColor
        searchBar.searchBarStyle = .minimal
        searchBar.isTranslucent = true
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        searchBar.setSearchFieldBackgroundImage(#imageLiteral(resourceName: "barra_buscar"), for: .normal)
        
        searchBar.backgroundColor = UIColor.clear
        searchBar.barTintColor = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
        searchBar.tintColor  = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
        searchBar.setImage(#imageLiteral(resourceName: "search_icon"), for: .search, state: .normal)
        searchBar.keyboardAppearance = .default
        searchBar.keyboardType = .default
        
        
        tableViewResults = UITableView(frame: CGRect(x: self.view.frame.width*0.1, y: self.view.frame.height*0.105+searchBar.frame.height, width: self.view.frame.width*0.8, height: (self.view.frame.height*0.028)*8))
        tableViewResults.layer.cornerRadius = 15.0
        tableViewResults.layer.borderWidth = 2
        tableViewResults.separatorStyle = .none
        tableViewResults.layer.borderColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0).cgColor
        tableViewResults.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableViewResults.dataSource = self
        tableViewResults.delegate = self
        
        self.view.addSubview(tableViewResults)
        
        self.tableViewResults.isHidden = true
        self.view.addSubview(searchBar)
        
        profileView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let user = UserManager.sharedUserManager
        let token = user.token
        user.server = "http://37.208.116.28"
        self.serviceValidateToken(urlToRequest:  user.server + ":3003/token/validation?token=\(token)")
        
        
        menu.btnFilter.addTarget(self, action: #selector(actionBtnCattegory(sender:)), for: .touchDown)
        menu.btnMyMusic.addTarget(self, action: #selector(actionBtnMyMusic(sender:)), for: .touchDown)
        menu.btnShop.addTarget(self, action: #selector(actionBtnShop(sender:)), for: .touchDown)
        menu.btnPerfil.addTarget(self, action: #selector(actionBtnPerfil(sender:)), for: .touchDown)
        menu.btnCompose.addTarget(self, action: #selector(actionBtnMyComposition(sender:)), for: .touchDown)
        btnnewcomposition.addTarget(self, action: #selector(actionBtnNewComposition(sender:)), for: .touchDown)
        popup.btnDelete.addTarget(self, action: #selector(actionBtnDeleteScore(sender:)), for: .touchDown)
        popup.btnSendToMyLibrary.addTarget(self, action: #selector(actionBtnSendToMyMusic(sender:)), for: .touchDown)
        popup.btnNewArrangement.addTarget(self, action: #selector(actionBtnNewArrangement(sender:)), for: .touchDown)
        popup.btnOpen.addTarget(self, action: #selector(actionBtnOpen(sender:)), for: .touchDown)
        popup.btnDownload.addTarget(self, action: #selector(actionBtnDownload(sender:)), for: .touchDown)
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.verifyIfMidiIsConnected), userInfo:nil, repeats:true)
        
        //        footer.btnImgInstrument.addTarget(self, action: #selector(showInstrumentsScreen(sender:)), for: .touchUpInside)
        footer.btnLblInstrument.addTarget(self, action: #selector(showInstrumentsScreen(sender:)), for: .touchDown)
        footer.btnHelp.addTarget(self, action: #selector(actionBtnHelp(sender:)), for: .touchDown)
        
        storeFilter.btnDone.addTarget(self, action: #selector(actionBtnCancel(sender:)), for: .touchUpInside)
//        storeFilter.btnClearAll.addTarget(self, action: #selector(actionBtnClearAll(sender:)), for: .touchUpInside)

        
        
        self.view.bringSubview(toFront: helpView)
//        self.view.bringSubview(toFront: filterStoreView)
        self.btnnewcomposition.isHidden = true
        
        view.bringSubview(toFront: tableViewResults)
        view.bringSubview(toFront: collectionView)
        view.bringSubview(toFront: btnnewcomposition)
        view.bringSubview(toFront: profileView)
        view.bringSubview(toFront: storeView)
        view.bringSubview(toFront: collectionViewAllStore)
        view.bringSubview(toFront: collectionViewNewsStore)
        view.bringSubview(toFront: tableViewResults)
        view.bringSubview(toFront: fakebackGroundPopUpLongPress)
        view.bringSubview(toFront: popup)
        view.bringSubview(toFront: shadowView)
        view.bringSubview(toFront: helpView)
        view.bringSubview(toFront: searchBar)
        view.bringSubview(toFront: storeFilter)
        view.bringSubview(toFront: footer)
        view.bringSubview(toFront: popUpAlert)

        
        
        
        
        //        tableViewResults.backgroundColor = UIColor.blue
        
        if let user = DAOUser.getInstance().getUserActive() {
            if user.image != "" {
                profileView.imgProfile.image = base64ToImage(strBase64: user.image)
            }
            
            profileView.txtName.text       = user.firstName
            profileView.txtLasName.text    = user.lastName
            profileView.txtInterests.text  = ""//user.interests
            profileView.txtBirthday.text   = user.birthDate
            profileView.txtEmail.text      = user.email
            profileView.txtInstrument.text = user.typeInst.name
            if user.genre == 1 {
                profileView.txtGender.text = "Female"
            } else {
                profileView.txtGender.text = "Male"
            }
            profileView.txtCountry.text = user.country.name
            countryUser = user.country
            titleFooter = user.typeInst.name
            footer.btnLblInstrument.setTitle(titleFooter, for: .normal)
        }
        
        profileView.delegate = self
        
        profileView.txtGender.addTarget(self, action: #selector(textFieldActive(_:)), for: .touchDown)
        profileView.txtCountry.addTarget(self, action: #selector(textFieldActive(_:)), for: .touchDown)
        profileView.btnLogout.addTarget(self, action: #selector(actionBtnLogout(sender:)), for: .touchDown)
        popUpAlert.btnConfirm.addTarget(self, action: #selector(actionBtnConfirm(sender:)), for: .touchDown)
        popUpAlert.btnCancel.addTarget(self, action: #selector(actionBtnCancelPopUpAlert(sender:)), for: .touchDown)

        
        profileView.btnSave.addTarget(self, action: #selector(actionBtnSaveProfile(sender:)), for: .touchDown)
        profileView.btnCancel.addTarget(self, action: #selector(actionCancelChangesProfile(sender:)), for: .touchDown)
        profileView.txtGender.delegate  = self
        profileView.txtCountry.delegate = self
        
        profileView.bringSubview(toFront: profileView.imgProfile)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        profileView.txtInstrument.addTarget(self, action: #selector(showInstruments(_:)), for: .touchDown)
        footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
        
        
        view.bringSubview(toFront: footer)
        
        
        
    }
    
    @objc func actionCancelChangesProfile(sender: UIButton) {
        if let user = DAOUser.getInstance().getUserActive() {
            if user.image != "" {
                profileView.imgProfile.image = base64ToImage(strBase64: user.image)
            }
            profileView.txtName.text       = user.firstName
            profileView.txtLasName.text    = user.lastName
            profileView.txtInterests.text  = ""//user.interests
            profileView.txtBirthday.text   = user.birthDate
            profileView.txtEmail.text      = user.email
            profileView.txtInstrument.text = user.typeInst.name
            if user.genre == 1 {
                profileView.txtGender.text = "Female"
            } else {
                profileView.txtGender.text = "Male"
            }
            profileView.txtCountry.text = user.country.name
            countryUser = user.country
        }
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if profileView.txtEmail.isFirstResponder {
                self.view.frame.origin.y -= keyboardSize.height / 1.5
            }
        
            if profileView.txtGender.isFirstResponder || profileView.txtCountry.isFirstResponder || storeFilter.txtGener1.isFirstResponder || storeFilter.txtGener2.isFirstResponder || storeFilter.txtTypeInstrument.isFirstResponder || storeFilter.txtInstrument.isFirstResponder || storeFilter.txtDificult.isFirstResponder || storeFilter.txtComposer.isFirstResponder{
            view.endEditing(true)
            }
       
//            if storeFilter.txtDate.isFirstResponder || storeFilter.txtTags.isFirstResponder{
//                self.view.frame.origin.y -= keyboardSize.height
//                }
//            else if !storeFilter.txtArrangement.isFirstResponder && !storeFilter.txtTitle.isFirstResponder && !storeFilter.txtSubtitle.isFirstResponder && !storeFilter.txtDate.isFirstResponder && !storeFilter.txtTags.isFirstResponder{
//                view.endEditing(true)
//                    if self.view.frame.origin.y != 0 {
//                        self.view.frame.origin.y = 0
//                    }
//                }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if profileView.txtEmail.isFirstResponder {
                self.view.frame.origin.y += keyboardSize.height / 1.5
            }
            if profileView.txtGender.isFirstResponder || profileView.txtCountry.isFirstResponder || storeFilter.txtGener1.isFirstResponder || storeFilter.txtGener2.isFirstResponder || storeFilter.txtTypeInstrument.isFirstResponder || storeFilter.txtInstrument.isFirstResponder || storeFilter.txtTypeInstrument.isFirstResponder || storeFilter.txtComposer.isFirstResponder || storeFilter.txtTypeInstrument.isFirstResponder || storeFilter.txtDificult.isFirstResponder{
                view.endEditing(true)
            }
//            if storeFilter.txtDate.isFirstResponder || storeFilter.txtTags.isFirstResponder {
//                self.view.frame.origin.y += keyboardSize.height
//            } else if !storeFilter.txtArrangement.isFirstResponder && !storeFilter.txtTitle.isFirstResponder && !storeFilter.txtSubtitle.isFirstResponder && !storeFilter.txtDate.isFirstResponder && !storeFilter.txtTags.isFirstResponder{
//                view.endEditing(true)
//                if self.view.frame.origin.y != 0 {
//                    self.view.frame.origin.y = 0
//                }
//            }
        }
    }
    
    //
    @objc func textFieldActive(_ field: UITextField) {
        if field == profileView.txtGender {
            profileView.tbGender.isHidden  = !profileView.tbGender.isHidden
            profileView.tbCountry.isHidden = true
        }
        if field == profileView.txtCountry {
            profileView.tbCountry.isHidden = !profileView.tbCountry.isHidden
            profileView.tbGender.isHidden  = true
        }
        
        view.endEditing(true)
    }
    
    func base64ToImage(strBase64: String) -> UIImage {
        let dataDecoded : Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        
        return decodedimage!
    }
    
    @objc private func showInstruments(_ sender: UITextField) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "controllerInstruments")
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        (controller as! SelectInstrumentViewController).isUser = true
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc private func showInstrumentsScreen(sender: UIButton) {
        //        let controller = InstrumentsViewController()
        //        controller.modalTransitionStyle = .crossDissolve
        //        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        //
        //        self.present(controller, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "controllerInstruments")
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        (controller as! SelectInstrumentViewController).isUser = false
        self.present(controller, animated: true, completion: nil)
    }
    
    var scoreSelected = ModelScore()
    @objc func actionBtnDeleteScore(sender: UIButton) {
        
        DAOScore.getInstance().deleteScore(scoreSelected.id)
        
        self.scores.removeAll()
        self.items.removeAllObjects()
        var email = ""
        
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
        }
        
        if(menu.function == .myComposition){
            scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
        }
        else if(menu.function == .myMusic){
            scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: "")
        }
        for score in scores {
            self.items.add(score.title)
        }
        self.collectionView.reloadData()
        
        shadowView.isHidden = true
        popup.isHidden = true
        fakebackGroundPopUpLongPress.isHidden = true
        
    }
    
    @objc func actionBtnHelp(sender: UIButton) {
        //  self.helpView.helpHome()
        switch menu.function {
        case .myMusic,.myComposition, .shop:
            self.helpView.helpHome()
        case .profile:
            self.helpView.helpProfile()
        default:
            print ("")
        }
        self.helpView.isHidden = false
    }
    
    
    @objc func actionBtnNewArrangement(sender: UIButton) {
        
        self.scoreSelected.idPartOriginal = self.scoreSelected.id
        self.scoreSelected.type = 3
        self.scoreSelected.status = 2
        var email = ""
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
            scoreSelected.user = user
        }
        
        DAOScore.getInstance().createArrangement(scoreSelected)
        self.scores.removeAll()
        self.items.removeAllObjects()
        
        
        
        
        scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: "")
        for score in scores {
            self.items.add(score.title)
        }
        self.collectionView.reloadData()
        shadowView.isHidden = true
        popup.isHidden = true
        fakebackGroundPopUpLongPress.isHidden = true
    }
    
    @objc func actionBtnSendToMyMusic(sender: UIButton) {
        
        DAOScore.getInstance().updateStatusScore(self.scoreSelected.id, 1)
        self.scores.removeAll()
        self.items.removeAllObjects()
        var email = ""
        
        if let user = DAOUser.getInstance().getUserActive() {
            email = user.email
        }
        scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
        for score in scores {
            self.items.add(score.title)
        }
        self.collectionView.reloadData()
        shadowView.isHidden = true
        popup.isHidden = true
        fakebackGroundPopUpLongPress.isHidden = true
    }
    
    
    @objc private func verifyIfMidiIsConnected() {
        let sourceCount = MIDIGetNumberOfSources()
        
        if sourceCount > 1 {
            footer.setMidiIsConnected(connected: true)
        } else {
            footer.setMidiIsConnected(connected: false)
        }
    }
    
    @objc func actionBtnNewComposition(sender: UIButton){
        let a = ModelScore()
        a.type = 2
    
        goToScore(modelScore: a, isCompose:  true, isImport: false, index: 1)
    }
    
    @objc func actionBtnCancel(sender: UIButton){
        storeFilter.isHidden = true
        menu.btnMyMusic.isEnabled = true
        menu.btnCompose.isEnabled = true
        menu.btnPerfil.isEnabled = true
        menu.btnShop.isEnabled = true
        btn.isEnabled = true
    }
    
    @objc func actionBtnDownload(sender: UIButton) {
        let user = UserManager.sharedUserManager
        let token = user.token
        user.server = "http://37.208.116.28"
     //   serviceDownloadToStore(urlToRequest: user.server + ":3003/store/score/downloaded?token=\(token)&id=\(scoreSelected.idStore)", method: ServiceRemote.Method.GET.rawValue)
        
        popup.btnDownload.isEnabled = false
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            self.scoreSelected.user.email = user.email
            self.scoreSelected.type   = 1
            self.scoreSelected.status = 1
            
            DAOScore.getInstance().addScore(self.scoreSelected)
            
            DispatchQueue.main.async(execute: {
                self.popup.btnDownload.backgroundColor =  UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.8)
                self.popup.btnDownload.isEnabled = false
                self.popup.btnOpen.isEnabled = true
                self.popup.btnOpen.backgroundColor =  UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0)
                
                // self.goToScore(modelScore: self.scoreSelected, isCompose: false)
            })
        }
    }
    
    @objc func actionBtnOpen(sender: UIButton) {
        self.popup.isHidden = true
        self.shadowView.isHidden = true

        if(menu.function == .myComposition){
            self.goToScore(modelScore: self.scoreSelected, isCompose: true, isImport: false)
        }
        else if(menu.function == .myMusic){
            self.goToScore(modelScore: self.scoreSelected, isCompose: false, isImport: false)
        }
        else if (menu.function == .shop){
            self.goToScore(modelScore: self.scoreSelected, isCompose: false, isImport: false)
        }
        
    }
    
    
    @objc func actionBtnCattegory(sender: UIButton) {
        if menu.function == .myComposition{
            if(self.tableViewCategoryMyCompositions == nil){
                tableViewCategoryMyCompositions = UITableView(frame: CGRect(x: self.view.frame.width*0.709, y: self.view.frame.height*0.124+searchBar.frame.height, width: self.view.frame.width*0.24, height: self.view.frame.height*0.1))
                tableViewCategoryMyCompositions.layer.cornerRadius = 15.0
                tableViewCategoryMyCompositions.layer.borderWidth = 2
                tableViewCategoryMyCompositions.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
                tableViewCategoryMyCompositions.separatorStyle = .none
                tableViewCategoryMyCompositions.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
                tableViewCategoryMyCompositions.dataSource = self
                tableViewCategoryMyCompositions.delegate = self
                tableViewCategoryMyCompositions.isScrollEnabled = false
                view.bringSubview(toFront: fakebackGroundTableViewResultMyCompositions)
                self.view.addSubview(tableViewCategoryMyCompositions)
                view.bringSubview(toFront: tableViewCategoryMyCompositions)
                self.fakebackGroundTableViewResultMyCompositions.isHidden = false
                tableViewCategoryMyCompositions.restorationIdentifier = "categoria"
                
                if(defaults.bool(forKey: "CompositionsMC") == false){
                    tableViewCategoryMyCompositions.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .none
                }else {
                    tableViewCategoryMyCompositions.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .checkmark
                }
                if(defaults.bool(forKey: "ArrangementsMC") == false){
                    tableViewCategoryMyCompositions.cellForRow(at: IndexPath(row: 1, section: 0))?.accessoryType = .none
                }else {
                    tableViewCategoryMyCompositions.cellForRow(at: IndexPath(row: 1, section: 0))?.accessoryType = .checkmark
                }
            }
            else{
                tableViewCategoryMyCompositions.frame = CGRect(x: self.view.frame.width*0.709, y: self.view.frame.height*0.124+searchBar.frame.height, width: self.view.frame.width*0.24, height: self.view.frame.height*0.1)
                tableViewCategoryMyCompositions.isHidden = false
                fakebackGroundTableViewResultMyCompositions.frame = self.view.frame
                fakebackGroundTableViewResultMyCompositions.isHidden = false
            }
        }
        if menu.function == .myMusic{
            if(self.tableViewCategoryMyCompositions == nil){
                tableViewCategoryMyMusic = UITableView(frame: CGRect(x: self.view.frame.width*0.709, y: self.view.frame.height*0.124+searchBar.frame.height, width: self.view.frame.width*0.24, height: self.view.frame.height*0.15))
                tableViewCategoryMyMusic.layer.cornerRadius = 15.0
                tableViewCategoryMyMusic.layer.borderWidth = 2
                tableViewCategoryMyMusic.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
                tableViewCategoryMyMusic.separatorStyle = .none
                tableViewCategoryMyMusic.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
                tableViewCategoryMyMusic.dataSource = self
                tableViewCategoryMyMusic.delegate = self
                tableViewCategoryMyMusic.isScrollEnabled = false
                view.bringSubview(toFront: fakebackGroundTableViewResultMyMusic)
                self.view.addSubview(tableViewCategoryMyMusic)
                view.bringSubview(toFront: tableViewCategoryMyMusic)
                self.fakebackGroundTableViewResultMyMusic.isHidden = false
                tableViewCategoryMyMusic.restorationIdentifier = "categoria"
                
                if(defaults.bool(forKey: "CompositionsMM") == false){
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 1, section: 0))?.accessoryType = .none
                }else {
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 1, section: 0))?.accessoryType = .checkmark
                }
                if(defaults.bool(forKey: "ArrangementsMM") == false){
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 2, section: 0))?.accessoryType = .none
                }else {
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 2, section: 0))?.accessoryType = .checkmark
                }
                if(defaults.bool(forKey: "StoreMM") == false){
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .none
                }else {
                    tableViewCategoryMyMusic.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .checkmark
                }
            }
            else{
                tableViewCategoryMyMusic.frame = CGRect(x: self.view.frame.width*0.7, y: self.view.frame.height*0.124+searchBar.frame.height, width: self.view.frame.width*0.24, height: self.view.frame.height*0.15)
                tableViewCategoryMyMusic.isHidden = false
                fakebackGroundTableViewResultMyMusic.frame = self.view.frame
                fakebackGroundTableViewResultMyMusic.isHidden = false
                
                
            }
        }
        
                if storeView == nil {
                    let m = StoreView(frame:CGRect(x: 0, y: view.frame.height*0.14, width: view.frame.width, height: view.frame.height*0.86))
                    m.backgroundColor = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
                    m.isHidden = true
                    view.addSubview(m)
                    storeView = m
        
                }
        
        
            if menu.function == .shop{
                self.storeFilter.isHidden = false
                self.view.bringSubview(toFront: storeFilter)
                self.profileView.isHidden = true
                menu.btnMyMusic.isEnabled = false
                menu.btnCompose.isEnabled = false
                menu.btnPerfil.isEnabled = false
                menu.btnShop.isEnabled = false
                btn.isEnabled = false
                
                if authors.count == 0 {
                    let user = UserManager.sharedUserManager
                    let token = user.token
                    user.server = "http://37.208.116.28"
                    serviceGetAuthors(urlToRequest: user.server + ":3003/composers?token=\(token)", method: ServiceRemote.Method.GET.rawValue)
                    
                }


                
            }
    }
    
    
    
    @objc func actionBtnMyMusic(sender: UIButton) {
        if menu.function != .myMusic {
            menu.btnFilter.isEnabled = true
            menu.function = .myMusic
            self.btnnewcomposition.isHidden = true
            menu.btnMyMusic.setImage(#imageLiteral(resourceName: "mymusic_select"), for: .normal)
            menu.btnCompose.setImage(#imageLiteral(resourceName: "impost"), for:.normal)
            menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
            menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
            menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
            profileView.isHidden = true
            storeView.isHidden = true
            self.collectionViewAllStore.isHidden = true
            self.collectionViewNewsStore.isHidden = true
            self.scores.removeAll()
            self.items.removeAllObjects()
            var email = ""
            
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }
            scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: "")
            for score in scores {
                self.items.add(score.title)
            }
            self.collectionView.reloadData()
            
            if(popup != nil){
                popup.isMyMusic()
            }
        }
    }
    
    @objc func actionBtnMyComposition(sender: UIButton) {
        if menu.function != .myComposition{
            self.btnnewcomposition.isHidden = false
            menu.btnFilter.isEnabled = true
            menu.function = .myComposition
            menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
            menu.btnCompose.setImage(#imageLiteral(resourceName: "impost_select"), for:.normal)
            menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
            menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
            menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
            
            storeView.isHidden = true
            profileView.isHidden = true
            self.collectionViewAllStore.isHidden = true
            self.collectionViewNewsStore.isHidden = true
            if popup != nil {
                popup.isHidden = true
                self.shadowView.isHidden = true
                popup.isMyCommpositions()
                view.bringSubview(toFront: popup)
            }
            self.scores.removeAll()
            self.items.removeAllObjects()
            var email = ""
            
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }
            scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
            for score in scores {
                self.items.add(score.title)
            }
            self.collectionView.reloadData()
            
            if(popup != nil){
                popup.isMyCommpositions()
            }
            
        }
    }
    
    @objc func actionBtnShop(sender: UIButton) {
        if menu.function != .shop{
            menu.btnFilter.isEnabled = true
            self.items.removeAllObjects()
            self.scores.removeAll()
            self.itemsNews.removeAllObjects()
            self.scoresNews.removeAll()
            self.collectionViewNewsStore.reloadData()
            self.collectionViewAllStore.reloadData()
            if(popup != nil){
                popup.isStore()
            }
            
            let user = UserManager.sharedUserManager
            let token = user.token
            user.server = "http://37.208.116.28"
            
            
            
            if storeView == nil {
                let m = StoreView(frame:CGRect(x: 0, y: view.frame.height*0.14, width: view.frame.width, height: view.frame.height*0.86))
                m.backgroundColor = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
                
                view.addSubview(m)
                storeView = m
            } else {
                storeView.frame = CGRect(x: 0, y: view.frame.height*0.14, width: view.frame.width, height: view.frame.height*0.86)
                // storeView.items.removeAllObjects()
                //storeView.scoresNewStore.removeAll()
                serviceGetNewsStore(urlToRequest: user.server + ":3003/store/score/news?token=\(token)", method: ServiceRemote.Method.GET.rawValue)
                serviceGetAllStore(urlToRequest: user.server + ":3003/store/score/all?token=\(token)&page=1", method: ServiceRemote.Method.GET.rawValue)
                storeView.isHidden = false
                self.collectionViewAllStore.isHidden = false
                self.collectionViewNewsStore.isHidden = false
//                self.collectionViewAllStore.showsVerticalScrollIndicator = true
            }
            
            
            menu.function = .shop
            menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
            menu.btnCompose.setImage(#imageLiteral(resourceName: "impost"), for:.normal)
            menu.btnShop.setImage(#imageLiteral(resourceName: "store_select"), for: .normal)
            menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
            menu.btnFilter.setImage(#imageLiteral(resourceName: "filter-1"), for: .normal)
            profileView.isHidden = true
            
        }
    }
    
    @objc func actionBtnPerfil(sender: UIButton) {
        if menu.function != .profile {
            menu.function = .profile
            menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
            menu.btnCompose.setImage(#imageLiteral(resourceName: "impost"), for:.normal)
            menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
            menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil_select"), for: .normal)
            
            profileView.isHidden = false
            view.bringSubview(toFront: profileView)
            
            view.bringSubview(toFront: footer)
        }
    }

    
    @objc func txtTitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.title = textField.text!
            score.changeTitle()
        }
    }
    
    @objc func txtSubTitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.subtitle = textField.text!
            score.changeSubTitle()
        }
    }
    
    @objc func txtComposerDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.author[0].firstName = textField.text!
            score.changeAuthor()
        }
    }
    
    @objc func txtSubtitleDidChange(textField: UITextField) {
        if let source = score.dataSource {
            source.subtitle = textField.text!
        }
    }

    
    @objc func actionBtnImport(sender: UIButton) {
     
        if (myActivityIndicator ==  nil) {
            myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            //myActivityIndicator.backgroundColor = UIColor.blue
            
            
            myActivityIndicator.center = self.view.center
            self.view.addSubview(myActivityIndicator)
            
            self.myActivityIndicator.isHidden = false
            // Start Activity Indicator
            self.myActivityIndicator.startAnimating()
            
        } else {
            self.myActivityIndicator.isHidden = false
            // Start Activity Indicator
            self.myActivityIndicator.startAnimating()
        }
        
        importWithBase64()
        
        //
//                let documentPicker = UIDocumentPickerViewController(documentTypes: [(kUTTypeMIDIAudio as NSString) as String], in: .import)
//                documentPicker.modalPresentationStyle = UIModalPresentationStyle.formSheet
//                documentPicker.delegate = self
//                self.present(documentPicker, animated: true, completion: nil)
        
    }

    var countryUser = ModelCountry()
    
    func imgToBase64(image: UIImage) -> String {
        let img : UIImage = image
        //Now use image to create into NSData format
        let imageData:Data = img.jpeg(.lowest)!
        
        let strBase64 = imageData.base64EncodedString()
        
        return strBase64
    }
    
    @objc func actionBtnLogout(sender: UIButton) {
        popUpAlert.alertLogout()
        popUpAlert.isHidden = false
        view.bringSubview(toFront: popUpAlert)
        popUpAlert.acao = 2
    }
    
    @objc func actionBtnSaveProfile(sender: UIButton) {
        popUpAlert.alertSaveProfile()
        popUpAlert.isHidden = false
        view.bringSubview(toFront: popUpAlert)
        popUpAlert.acao = 3
    }
    @objc func actionBtnCancelPopUpAlert(sender: UIButton) {
            popUpAlert.isHidden = true
    }
    
    @objc func actionBtnConfirm(sender: UIButton) {
        if popUpAlert.acao == 2 {
            popUpAlert.isHidden = true
            self.logout()
        }
        if popUpAlert.acao == 3 {
            popUpAlert.isHidden = true
            self.saveProfile()
        }
    }
    
    private func logout() {
        
        let user = UserManager.sharedUserManager
        user.server = "http://37.208.116.28"
        let token = user.token
        serviceLogout(urlToRequest: user.server + ":3003/logout?token="+token, method: ServiceRemote.Method.POST.rawValue)
        
       
    }
    
    private func saveProfile() {
        if let user = DAOUser.getInstance().getUserActive() {
            user.firstName = profileView.txtName.text!
            user.lastName = profileView.txtLasName.text!
            user.birthDate = profileView.txtBirthday.text!
            if profileView.txtGender.text == "Female" {
                user.genre = 1
            } else {
                user.genre = 0
            }
            user.image  = imgToBase64(image: profileView.imgProfile.image!)
            user.interests = ""// profileView.txtInterests.text!
            user.country   = countryUser
            //            user.instrument = instrumentUserSelected
            //            user.typeInst  = typeInstUserSelected
            
            DAOUser.getInstance().changeUser(model: user)
            
            serviceChangeUser(user: user)
        }
    }
    func importWithBase64() {
        let user = UserManager.sharedUserManager
        user.server = "http://37.208.116.28"
        let token = user.token
        service(urlToRequest: user.server + ":3001/operator/midi?token="+token, method: ServiceRemote.Method.GET.rawValue)
    }
    //token    String    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQxMmRkOWJlMzY5YjBjMzYyYWZjNTMiLCJmaXJzdE5hbWUiOiJTYWxvbWFvIiwibGFzdE5hbWUiOiJWaWxhw6dhIiwiZW1haWwiOiJzYWxvbWFvQGx0bWRldi5jb20iLCJ0eXBlIjoyLCJjdXJyZW50VGltZSI6MTUyODgxNTkyNTA1NSwiaWF0IjoxNTI4ODE1OTI1fQ.8BMgb4F7NbdONhBVvbMxg8gKiGxkRchzF5zlC5nHCKc"    
    
    //"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWQxMmRkOWJlMzY5YjBjMzYyYWZjNTMiLCJmaXJzdE5hbWUiOiJTYWxvbWFvIiwibGFzdE5hbWUiOiJWaWxhw6dhIiwiZW1haWwiOiJzYWxvbWFvQGx0bWRldi5jb20iLCJ0eXBlIjoyLCJjdXJyZW50VGltZSI6MTUyODgxNjczMTA1NCwiaWF0IjoxNTI4ODE2NzMxfQ.B-hsNp2HY-0Q3OdlFdQ3NmK2bguEvaL74LlPVbHDHIo"
    @objc func actionGoToSelectionInstruments(sender: UITapGestureRecognizer) {
        //        let controller = InstrumentsViewController()
        //        controller.modalTransitionStyle = .crossDissolve
        //        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        //
        //        self.present(controller, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "controllerInstruments")
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    func goToScore(modelScore: ModelScore, isCompose: Bool, isImport : Bool ,fromMidi: Bool = false, index: Int = -1) {
        let x = (view.frame.height * 0.08) * 0.3
        let y = view.frame.height * 0.08
        let width  = view.frame.width - (2.0 * x)
        let height = view.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        
        print("tipo: ", modelScore.type)
        if modelScore.type == 1 {
            defaults.set(0, forKey: "compose")
        }else {
            defaults.set(1, forKey: "compose")
        }
        
        
        if isCompose {
            if modelScore.type != 3 {
                if isImport || modelScore.type == 1{
                    modelScore.status = 2
                    modelScore.type = 1
                }
                else {
                    modelScore.status = 2
                    modelScore.type = 2
                }
            }
            let controller = ComposeViewController()
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            //    controller.titleFooter = self.titleFooter
            controller.parentControl = self
            let instrument = ModelTypeInstrument()
            instrument.name = "Piano"
            
            isMonophonic = UserDefaults.standard.bool(forKey: "Mono")
            
            instrument.clef = DAOTypeInstrument.getInstance().getTypeInstrumentActive().clef
            
            let config = ModelBarConfiguration()
            config.bar         = 1
            config.key         = modelScore.returnKeyValue()
            config.metronome   = modelScore.metronome
            config.numerator   = 4
            config.denominator = 4
            let model = modelScore
            
            if modelScore.notes.count == 0 {
                model.quantization = 8
            } else {
                model.calculateQuantization()
                model.quantization = modelScore.suggestedQuantization
            }
            model.typeInstrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
            model.configurations.append(config)
            
            let score = DrawScore()
            score.dataSource = model
            score.state = .composing
            
            score.createPages(parameters: parameters)
            
          
            controller.score = score
            self.present(controller, animated: true, completion: nil)
        } else {
            
            defaults.set(1, forKey: "compose")
            let controller = ScoreViewController()
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            controller.titleFooter = self.titleFooter
            controller.parentControl = self
            
            let instrument = ModelTypeInstrument()
            instrument.name = "Piano"
            
            isMonophonic = UserDefaults.standard.bool(forKey: "Mono")
            
            if !isMonophonic {
                instrument.clef = 3
            } else {
                instrument.clef = 1
            }
            
            let config = ModelBarConfiguration()
            config.bar         = 1
            config.key         = modelScore.returnKeyValue()
            config.metronome   = modelScore.metronome
            config.numerator   = 4
            config.denominator = 4
            
            let model = modelScore
            model.typeInstrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
            print()
            model.configurations.append(config)
            
            controller.parentControl = self
            
            model.calculateQuantization()
            model.quantization = modelScore.suggestedQuantization
            
            let score = DrawScore()
            score.dataSource = model
            
            controller.score = score
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController,
                        didPickDocumentAt url: URL) {
        
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            if FileManager.default.fileExists(atPath: url.path) {
                let midi = MIDIReader(url: url)
                var modelScore = ModelScore()
                modelScore.title = url.lastPathComponent.replacingOccurrences(of: ".midi", with: " ")
                midi.generateScore(modelScore: &modelScore, monophonic: false)
                modelScore.calculateQuantization()
                modelScore.quantization = modelScore.suggestedQuantization
                modelScore.extendNotes(numerator: 4, denominator: 4)
                
                if let user = DAOUser.getInstance().getUserActive() {
                    modelScore.user = user
                }
                goToScore(modelScore: modelScore, isCompose: true, isImport: true, fromMidi: true)
            }
            else {
                print("nao existe")
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if let viewT = touches.first?.view {
            if viewT != profileView.txtCountry {
                profileView.tbCountry.isHidden = true
            }
            if viewT != profileView.txtGender {
                profileView.tbGender.isHidden = true
            }
            if viewT == profileView.txtCountry && viewT == profileView.txtGender {
                view.endEditing(true)
            }
            if viewT is UITextField {
                
            } else {
                view.endEditing(true)
            }
            if viewT == profileView.imgProfile {
                actionImgUser()
            }
        }
    }
    
    func actionImgUser() {
        let actionSheet = UIAlertController(title: "Please Select Camera or Photo Library", message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Upload a Photo", style: .default, handler: { (UIAlertAction) in
            self.openPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (UIAlertAction) in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.profileView.imgProfile
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func unwindToVCHome(segue:UIStoryboardSegue) {
        if(UserDefaults.standard.integer(forKey: "login") == 1){
            if segue.source is SelectInstrumentViewController {
                let controller = segue.source as! SelectInstrumentViewController
                footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
                titleFooter = DAOTypeInstrument.getInstance().getTypeInstrumentActive().name
                if controller.isUser {
                    profileView.txtInstrument.text = DAOUser.getInstance().getUserActive()?.typeInst.name
                } else {
                    footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
                    titleFooter = DAOTypeInstrument.getInstance().getTypeInstrumentActive().name
                }
            }
        }
        if(UserDefaults.standard.integer(forKey: "login") == 0){
            UserDefaults.standard.set(1, forKey: "login")
        }
    }
}

extension HomeViewController: ProfileViewDelegate {
    func changeCountry(_ newCountry: ModelCountry) {
        countryUser = newCountry
    }
}

extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if info[UIImagePickerControllerOriginalImage] is UIImage {
            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
            profileView.imgProfile.contentMode = .scaleAspectFit //3
            profileView.imgProfile.image       = chosenImage //4
            
            if let user = DAOUser.getInstance().getUserActive() {
                user.image = imgToBase64(image: profileView.imgProfile.image!)
                DAOUser.getInstance().changeUser(model: user)
            }
        }
        
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension HomeViewController:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    ////        let parameter: CGFloat = 414.0
    ////        let factor: CGFloat = view.frame.width / parameter
    //        let width:CGFloat = view.frame.width * 0.895
    //        let height:CGFloat = width * 1.6
    //        return CGSize(width: width, height: height)
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if(collectionView == self.collectionViewNewsStore){
            return self.itemsNews.count
        }
        
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! HomeCollectionViewCell
        
        if(collectionView == self.collectionViewNewsStore){
            if(scoresNews[indexPath.row].isPoliphonic == 1){
                cell.setCardPolyImage(image: #imageLiteral(resourceName: "poli_icon"))
            }else {
                cell.setCardPolyImage(image: #imageLiteral(resourceName: "mono_icon"))
            }
            cell.setCardText(text: self.itemsNews[indexPath.row] as! String)
            cell.setCardTypeImage(image: #imageLiteral(resourceName: "download-1"))
        }else{
            if(scores[indexPath.row].isPoliphonic == 1){
                
                cell.setCardPolyImage(image: #imageLiteral(resourceName: "poli_icon"))
                
            }else {
                cell.setCardPolyImage(image: #imageLiteral(resourceName: "mono_icon"))
            }
            cell.setCardText(text: self.items[indexPath.row] as! String)
            if(self.scores[indexPath.row].type == 1){
                cell.setCardTypeImage(image: #imageLiteral(resourceName: "download-1"))
            } else if(self.scores[indexPath.row].type == 2){
                cell.setCardTypeImage(image: #imageLiteral(resourceName: "composition"))
            } else if(self.scores[indexPath.row].type == 3){
                cell.setCardTypeImage(image: #imageLiteral(resourceName: "arrangement"))
            }
            if(menu.function == .shop){
                cell.setCardTypeImage(image: #imageLiteral(resourceName: "download-1"))
            }
            
        }
        
        // view.bringSubview(toFront: cell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        if(menu.function == .shop){
            if(collectionView == self.collectionViewAllStore){
                
                let user = UserManager.sharedUserManager
                let token = user.token
                user.server = "http://37.208.116.28"
                serviceGetPartitureFromStore(urlToRequest: user.server + ":3003/store/score/id?token=\(token)&id=\(scores[indexPath.row].idStore)", method: ServiceRemote.Method.GET.rawValue)
             
            } else {
                let user = UserManager.sharedUserManager
                let token = user.token
                user.server = "http://37.208.116.28"
                serviceGetPartitureFromStore(urlToRequest: user.server + ":3003/store/score/id?token=\(token)&id=\(scoresNews[indexPath.row].idStore)", method: ServiceRemote.Method.GET.rawValue)
            }
        }
        
        if(menu.function == .myComposition){
            
            goToScore(modelScore: scores[index], isCompose:  true, isImport: false, index: index)
        }
        if(menu.function == .myMusic){
           
            if index < scores.count {
                goToScore(modelScore: scores[index], isCompose:  false, isImport: false, index: index)
            }
        }
    }
    
}

extension HomeViewController{
    
    func serviceValidateToken(urlToRequest: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        print(urlToRequest)
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                print("sda: ",parseResult)
                if let status = parseResult["status"] as? Bool{
                    if (!status){
                        DispatchQueue.main.async(execute: {
                            if let _ = DAOUser.getInstance().getUserActive() {
                                DAOUser.getInstance().removeAllUsers()
                            }
                            UserDefaults.standard.set(0, forKey: "login")
                            let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                controller.modalTransitionStyle = .crossDissolve
                                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                self.present(controller, animated: true, completion: nil)
                            }))
                            self.present(refreshAlert, animated: true, completion: nil)
                        })
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
    
    
    func serviceLogout(urlToRequest: String, method:String){
        
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
       
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{
                
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                print("resultado1: ", parseResult)
                if let status = parseResult["status"] as? Int {
                    print("resultado: ", status)
                    if(status == 1){
                    DispatchQueue.main.async(execute: {
                        if let _ = DAOUser.getInstance().getUserActive() {
                            DAOUser.getInstance().removeAllUsers()
                        }
                        UserDefaults.standard.set(0, forKey: "login")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                        controller.modalTransitionStyle = .crossDissolve
                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        self.present(controller, animated: false, completion: nil)
                        
                        })
                        
                    }
                }
                
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
                
            } catch {
                    print("Could not parse data as Json \(data)")
                    return
            }
            
            }.resume()
        
    }
    
    func serviceDownloadToStore(urlToRequest: String, method:String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            
            } catch {
                    print("Could not parse data as Json \(data)")
                    return
            }
            
            }.resume()
    }
    
    
    func serviceGetPartitureFromStore(urlToRequest: String, method:String) {
        
        let scoreStore = ModelScore()
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    if let score = data1["score"] {
                        print(score)
                        
                        //Bar Settings---------------
                        if let barSettings = score["barSettings"] as? [AnyObject]{
                            for barSetting in barSettings {
                                let config = ModelBarConfiguration()
                                config.bar = barSetting["bar"] as! Int
                                config.denominator = barSetting["timeDenominator"] as! Int
                                config.numerator = barSetting["timeNumerator"] as! Int
                                config.key = barSetting["keySignatureValue"] as! Int
                                config.keyType = barSetting["keySignatureType"] as! Int
                                // config.id = barSetting["_id"] as! Int
                                scoreStore.configurations.append(config)
                            }
                        }
                        //-----------------------------------
                        
                        
                        //Notes------------------------------
                        if let notes = score["notes"] as? [AnyObject]{
                            for note in notes {
                                let midiReader = MIDIReader()
                                let nota = ModelNote()
                                nota.duration = note["durationTicks"] as! Int
                                nota.durationQuantized = midiReader.quantizeNoteDuration(ticks: nota.duration)
                                nota.initialTime = note["initialTimeTicks"] as! Int
                                nota.tIniQuantized = midiReader.quantizeNoteInitialTime(ticks: nota.initialTime)
                                nota.intensity = note["intensity"] as! Int
                                nota.channel = note["channel"] as! Int
                                nota.tone = note["tone"] as! Int
                                if(nota.durationQuantized > 0){
                                    scoreStore.notes.append(nota)
                                }
                            }
                        }
                        //-----------------------------------
                        
                        //Repetitions-------------------------------------
                        if let repetitions = score["repetitions"] as? [AnyObject]{
                            for repet in repetitions{
                                let rep = ModelRepeat()
                                rep.initialBar = repet["initialBar"] as! Int
                                rep.voltaBrackets = repet["bracket"] as! Int
                                rep.finalBar = repet["finalBar"] as! Int
                                scoreStore.repeats.append(rep)
                            }
                            
                        }
                        //------------------------------------------------
                        
                        //DSDC---------------------------------------------
                        if let dsdc = score["daCaposDelSegnos"] as? [AnyObject]{
                            for dsDc in dsdc{
                                let dd = ModelDaCapoDelSegno()
                                dd.initialBar = dsDc["initialBar"] as! Int
                                dd.finalBar = dsDc["finalBar"] as! Int
                                dd.toCoda = dsDc["toCoda"] as! Int
                                dd.coda = dsDc["coda"] as! Int
                                dd.fine = dsDc["fine"] as! Int
                                dd.type = dsDc["type"] as! Int
                                dd.dcAlCoda = dsDc["dcAlCoda"] as! Int
                                dd.dsAlCoda = dsDc["dsAlCoda"] as! Int
                                dd.dcAlFine = dsDc["dcAlFine"] as! Int
                                dd.dsAlFine = dsDc["dsAlFine"] as! Int
                                scoreStore.dsDcs.append(dd)
                                
                            }
                            
                        }
                        //-------------------------------------------------
                        
                        //Author------------------------------
                        if let authors = score["authors"] as? [AnyObject]{
                            for autor in authors {
                                if(autor["type"] as! Int == 1){
                                        let auto = ModelAuthor()
                                        auto._id = autor["_id"] as! String
                                        auto.idUser = autor["idUser"] as! String
                                        auto.type = autor["type"] as! Int
                                        auto.firstName = score["author"] as! String
                                    
                                    scoreStore.author.append(auto)
                                    }
                                else if(autor["type"] as! Int == 3){
                                    let art = ModelArtist()
                                    art._id = autor["_id"] as! String
                                    art.idUser = autor["idUser"] as! String
                                    art.type = autor["type"] as! Int
                                    art.firstName = score["author"] as! String
                                    
                                    scoreStore.artist.append(art)
                                }
                                   
                                }
                            }
                        //------------------------------------
                        //Tag--------------------------------------
                        if let tagss = score["tags"] as? [AnyObject]{
                            for tag in tagss {
                                let marc = ModelTag()
                                marc.name = tag["name"] as! String
                                marc.idPartitura = scoreStore.id
                                scoreStore.tags.append(marc)
                            }
                        }
                        //-----------------------------------------
                        scoreStore.idStore = score["_id"] as! String
                        scoreStore.baseNote = score["baseNote"] as! Int
                        scoreStore.createdyear = score["creationDate"] as! String
                        scoreStore.difficulty = score["difficulty"] as! Int
                        scoreStore.quantization = score["quantization"] as! Int
                        scoreStore.title = score["title"] as! String
                        
                        for sub in score.allKeys {
                            if sub as! String == "subtitle" {
                                scoreStore.subtitle = score["subtitle"] as! String
                            }
                        }
                        print(score.allKeys)
                        
//                        if(score["subtitle"] != nil){
//                            scoreStore.subtitle = score["subtitle"] as! String
//                        }
                 
                        scoreStore.isPoliphonic = score["polyphonic"] as! Int
                        
                        DispatchQueue.main.async(execute: {
                            self.scoreSelected = scoreStore
                            self.popup.lblTitleMusic.text = self.scoreSelected.title
                            self.popup.lblSubTitleMusic.text = self.scoreSelected.subtitle
                            self.popup.lblTonality.text = self.retornaStringKeySignature(key: self.scoreSelected.configurations[0].key)
                            //  self.popup.lblTonality.text = self.retornaStringKeySignature(key: self.scoreSelected.configurations[0].key)
                            self.popup.lblTimeSignature.text = ("\(self.scoreSelected.configurations[0].numerator)/\(self.scoreSelected.configurations[0].denominator)")
                            self.popup.lblNameComposer.text = "\(self.scoreSelected.author[0].firstName) \(self.scoreSelected.author[0].lastName)"
                            self.fakebackGroundPopUpLongPress.isHidden = false
                            self.shadowView.isHidden = false
                            self.popup.btnDownload.isHidden = false
                            if(self.menu.function == .shop){
                                self.popup.isHidden = false
                            }
                            self.popup.btnDownload.backgroundColor =  UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0)
                            self.popup.btnDownload.isEnabled = true
                            self.popup.btnOpen.isEnabled = false
                            self.popup.btnOpen.backgroundColor =  UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.7)
                            self.updatePages()
                        })
                        
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            }.resume()
    }
    
    func serviceGetNewsStore(urlToRequest: String, method:String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    
                    if let data = data1["scores"] as? [AnyObject] {
                        self.itemsNews.removeAllObjects()
                        self.scoresNews.removeAll()
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            let score = ModelScore()
                            score.title = d["title"] as! String
                            score.idStore = d["_id"] as! String
                            score.isPoliphonic = d["polyphonic"] as! Int
                            // score.id = d["_id"] as! String
                            self.scoresNews.append(score)
                        }
                        DispatchQueue.main.async(execute: {
                            for score in self.scoresNews {
                                self.itemsNews.add(score.title)
                            }
                            self.collectionViewNewsStore.reloadData()
                            
                        })
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
                
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
        
    }
    func serviceGetAllStore(urlToRequest: String, method:String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    if let data = data1["scores"] as? [AnyObject] {
                        self.items.removeAllObjects()
                        self.scores.removeAll()
                        
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            let score = ModelScore()
                            score.title = d["title"] as! String
                            score.idStore = d["_id"] as! String
                            score.isPoliphonic = d["polyphonic"] as! Int
                            self.scores.append(score)
                            
                        }
                        for score in self.scores {
                            self.items.add(score.title)
                        }
                        DispatchQueue.main.async(execute: {
                            self.collectionViewAllStore.reloadData()
                            
                        })
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
                
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
        
    }
    
    
    
    func service(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        //request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    print("data", data1)
                    if let data = data1["midi"] as? [String:AnyObject] {
                        
                        let b64 = data["content"] as! String
                        
                        let monophonic = data["genre"] as! String
                        print(monophonic)
                        var options = NSString.CompareOptions()
                        options.insert(NSString.CompareOptions.caseInsensitive)
                        options.insert(NSString.CompareOptions.diacriticInsensitive)
                       
                        
                        
                        let midi = MIDIReader(b64: b64)
                        var modelScore = ModelScore()
                        
                        print("conteudo midi: ",data)
                        
                        
                        modelScore.title = data["title"] as! String
                        if monophonic.range(of: "Piano", options: options) != nil {
                             midi.generateScore(modelScore: &modelScore, monophonic: false)
                        }else {
                             midi.generateScore(modelScore: &modelScore, monophonic: true)
                        }
                        modelScore.calculateQuantization()
                        modelScore.quantization = modelScore.suggestedQuantization
                        modelScore.idMidi = data["_id"] as! String
                        if let user = DAOUser.getInstance().getUserActive() {
                            modelScore.user = user
                        }
                        modelScore.type = 1
                        modelScore.extendNotes(numerator: 4, denominator: 4)
                        DispatchQueue.main.async(execute: {
                            self.myActivityIndicator.isHidden = true
                            self.myActivityIndicator.stopAnimating()
                            self.goToScore(modelScore: modelScore, isCompose: true, isImport: true, fromMidi: true)
                        })
                    }
                }
                
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        print("Voce não tem mais MIDI's")
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.showMessage()
                    self.myActivityIndicator.isHidden = true
                    self.myActivityIndicator.stopAnimating()
                })
            } catch {
                DispatchQueue.main.async(execute: {
                    self.myActivityIndicator.isHidden = true
                    self.myActivityIndicator.stopAnimating()
                })
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
        
    }
    
    func showMessage() {
        let alert = UIAlertController(title: "", message: "No new midi available at this moment. Please contact Rob van Reymersdal.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion:nil)
    }
    
    func serviceGetInstruments(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    if let data = data1["instruments"] as? [AnyObject] {
                        //      print(data)
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            
                            
                            let category   = ModelInstrument()
                            category.id    = d["_id"] as! String
                            category.name  = d["category"] as! String
                            
                            DAOInstrument.getInstance().addInstrument(category)
                            if let types = d["instruments"] as? [AnyObject] {
                                for type in types {
                                    let tp = type as! [String:AnyObject]
                                    //  print("transform: ", tp["midiTransform"] )
                                    let instrument               = ModelTypeInstrument()
                                    instrument.id                 = tp["_id"] as! String
                                    instrument.name               = tp["name"] as! String
                                    instrument.instrumentCategory.id = d["_id"] as! String
                                    instrument.instrumentCategory.name = d["category"] as! String
                                    DAOTypeInstrument.getInstance().addInstrument(instrument)
                                }
                            }
                        }
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
    
    func serviceGetCountries(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    if let data = data1["countries"] as? [AnyObject] {
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            
                            let country   = ModelCountry()
                            country.id    = d["_id"] as! String
                            country.name  = d["name"] as! String
                            
                            DAOCountry.getInstance().addCountry(country)
                        }
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
    
    func serviceGetGenders(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        
        let session = URLSession.shared
        
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let data1 = parseResult["results"] as? [String:AnyObject] {
                    if let data = data1["genres"] as? [AnyObject] {
                        for dataI in data {
                            let d = dataI as! [String:AnyObject]
                            
                            let gender   = ModelGenre()
                            gender.id    = d["_id"] as! String
                            gender.name  = d["name"] as! String
                            
                            DAOGenre.getInstance().addGenre(gender)
                            if let subgenrs = d["subGenres"] as? [AnyObject] {
                                for subgenre in subgenrs {
                                    let sub = subgenre as! [String:AnyObject]
                                    
                                    let modelSubgenre   = ModelSubGenre()
                                    modelSubgenre.id    = sub["_id"] as! String
                                    modelSubgenre.name  = sub["name"] as! String
                                    modelSubgenre.idGenre = gender.id
                                    
                                    DAOSubGenre.getInstance().addSubGenre(modelSubgenre)
                                }
                            }
                        }
                    }
                }
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            //            do{
            //                if let data = parseResult["status"] as? Bool {
            //                    print(data)
            //                }
            //            }catch {
            //                print("Could not parse data as Json \(data)")
            //                return
            //            }
            
            }.resume()
    }
    func serviceChangeUser(user: ModelUser) {
        let name      = user.firstName
        let lastName  = user.lastName
        let birthdate = "1994-03-04"
        var gender    = user.genre
        let image     = user.image
        
        if user.genre == 0 {
            gender = 1
            
        }
        
        if user.genre == 1 {
            gender = 2
        }
        
        var data: [String:Any] = [:]
        data["firstName"]        = name
        data["lastName"]         = lastName
        //    data["image"]            = image
        data["gender"]           = gender
        data["idInstrument"]     = user.typeInst.id
        data["idCountry"]        = user.country.id
        data["birthdate"]        = birthdate
        
        let user  = UserManager.sharedUserManager
        
        user.server = "http://37.208.116.28"
        
        let postString : [String : Any] = ["user":data, "token":user.token ]
        
        let urlToRequest = user.server + ":3003/profile"
        
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                if let status = parseResult["status"] as? Bool {
                    if !status {
                        if let code = parseResult["errorCode"] as? Int {
                            if code == 6 {
                                
                                DispatchQueue.main.async(execute: {
                                    if let _ = DAOUser.getInstance().getUserActive() {
                                        DAOUser.getInstance().removeAllUsers()
                                    }
                                    UserDefaults.standard.set(0, forKey: "login")
                                    let refreshAlert = UIAlertController(title: "Expired login", message: "You are logged in a different device, please login again in this device.", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "controllerLogin")
                                        controller.modalTransitionStyle = .crossDissolve
                                        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                        self.present(controller, animated: true, completion: nil)
                                    }))
                                    self.present(refreshAlert, animated: true, completion: nil)
                                })
                            }
                        }
                    }
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
