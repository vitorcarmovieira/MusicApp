//
//  ViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/2/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import CoreMIDI
import CoreAudio
import AudioToolbox
import Foundation
import AppsFlyerLib

class ScoreViewController: UIPageViewController {
    
    weak var menu            : MenuView!
    weak var leftBar         : MenuBarView!
    weak var rightBar        : MenuBarView!
    weak var popUpPlay       : PopUpPlay!
    weak var viewCompare     : CompareView!
    weak var popUpHistory    : PopUpHistory!
    weak var footer          : Footer!
    weak var menuKey         : MenuKeyEdit!
    //let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    var processor = NotesProcessor()
    let analyser1 = NoteAnalyser1(8820, fourierLenght: 8192, intWindowLenght: 256)
    
    var actualPage = 0
    
     var helpView = HelpView()
    
    var score: DrawScore!
    
    private var scrOriginal = DrawScore()
    private var scrPlayed   = DrawScore()
    
    //Gravador de audio
    var recorderAudio = RecordAudio()
    
    lazy var controllers: [PageScoreViewController] = []
    
    private var isFull: Bool = false
    
    var soundOn = true
    
    var function = FunctionPlayer.none
    var isEndPerformance = false
    
    var tickInitial = 0
    var samplerUnit: AudioUnit?
    var client = MIDIClientRef()
    var inputPort = MIDIPortRef()
    var initialTimeStamp: UInt64 = 0
    var myActivityIndicator: UIActivityIndicatorView!
    
    var initialMetronome = 0
    var initialTonality = 0
    
    var titleFooter = ""
    
    weak var parentControl: HomeViewController? = nil
    
    var collectionView: UICollectionView!
    private let cellReuseIdentifier = "collectionCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        dataSource = self
        delegate = self
        updatePages()
        
        updateMenu()
        
        score.delegate = self
        
        for _ in 0..<20 {
            openConnectionMIDIDevice()
            closeConnectionMIDIDevice()
        }
        
        recorderAudio.delegate = self
        recorderAudio.startAudioUnit()
        initialTimeStamp = MIDITimeStamp(mach_absolute_time())
        initialMetronome = (self.score.dataSource?.metronome)!
        initialTonality = (self.score.dataSource?.configurations[0].key)!

        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.itemSize = CGSize(width: view.frame.width, height: view.frame.height)
//        let flowLayout = UICollectionViewFlowLayout()
        
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        collectionView.register(ScoreCollectionViewCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.white
 
       // self.view.addSubview(collectionView)
    }

    func getMidiIsConnected() -> Bool {
        let sourceCount = MIDIGetNumberOfSources()
        
        if sourceCount > 1 {
            return true
        }
        
        return false
    }
    
    //-----------------------Performance----------------------------
    func openConnectionMIDIDevice() {
        let np:MIDINotifyProc = { (notification:UnsafePointer<MIDINotification>, refcon:UnsafeMutableRawPointer?) in}
        MIDIClientCreate("MyMIDIClient" as CFString, np, nil, &client)
        MIDIInputPortCreateWithBlock(client, "MyMIDIClient" as CFString, &inputPort, receivingEventMidi)
        
        //        setupAudioUnit(samplerUnit: &samplerUnit)
    }
    
    func openPorts() {
        let sourceCount = MIDIGetNumberOfSources()
        
        for srcIndex in 0 ..< sourceCount {
            let midiEndPoint = MIDIGetSource(srcIndex)
            MIDIPortConnectSource(inputPort, midiEndPoint, nil)
        }
    }
    
    func closeConnectionMIDIDevice() {
        let sourceCount = MIDIGetNumberOfSources()
        for srcIndex in 0 ..< sourceCount {
            let midiEndPoint = MIDIGetSource(srcIndex)
            MIDIPortDisconnectSource(inputPort, midiEndPoint)
            MIDIPortDispose(inputPort)
        }
    }
    
    var notesBeingPlayed: [NoteMidiPlayed] = []
    
    func receivingEventMidi(packetList: UnsafePointer<MIDIPacketList>, srcConnRefCon: UnsafeMutableRawPointer?) -> Void {
        
        let packets = packetList.pointee
        let packet:MIDIPacket = packets.packet
        
        var ap = UnsafeMutablePointer<MIDIPacket>.allocate(capacity: 1)
        ap.initialize(to: packet)
        
        for _ in 0 ..< packets.numPackets {
            let p = ap.pointee
            var strTime = TimerDarwin()
            strTime.startTime = initialTimeStamp
            strTime.stopTime = p.timeStamp
            
            handle(packet: p, timeInSeconds: strTime.seconds)
            
            ap = MIDIPacketNext(ap)
            
        }
    }
    
    func handle(packet:MIDIPacket, timeInSeconds: Double) {
        
        let status = packet.data.0
        let d1 = packet.data.1
        let d2 = packet.data.2
        let rawStatus = status & 0xF0
        let channel = status & 0x0F
        
        switch rawStatus {
            
        case 0x80:
            print("Note off. Channel \(channel) note \(d1) velocity \(d2)")
            //            playNoteOff(channel: UInt32(channel), noteNum: UInt32(d1))
            noteOff(d1: d1, d2: d2, timeInSeconds: timeInSeconds)
            
        case 0x90:
            //            print("Note on. Channel \(channel) note \(d1) velocity \(d2)")
            //            playNoteOn(channel: UInt32(channel), noteNum:UInt32(d1), velocity: UInt32(d2))
            if UInt32(d2) == 0 {
                noteOff(d1: d1, d2: d2, timeInSeconds: timeInSeconds)
            } else if isFull {
                let noteMidi = NoteMidiPlayed(initialTime: timeInSeconds, finalTime: -1.0, midi: Int(d1))
                notesBeingPlayed.append(noteMidi)
            }
            
        case 0xA0:
            print("Polyphonic Key Pressure (Aftertouch). Channel \(channel) note \(d1) pressure \(d2)")
            
        case 0xB0:
            print("Control Change. Channel \(channel) controller \(d1) value \(d2)")
            
        case 0xC0:
            print("Program Change. Channel \(channel) program \(d1)")
            
        case 0xD0:
            print("Channel Pressure (Aftertouch). Channel \(channel) pressure \(d1)")
            
        case 0xE0:
            print("Pitch Bend Change. Channel \(channel) lsb \(d1) msb \(d2)")
            
        default: print("Unhandled message \(status)")
        }
        
    }
    
    func noteOff(d1: UInt8, d2: UInt8, timeInSeconds: Double) {
        for (i, notePlayed) in notesBeingPlayed.enumerated() {
            if notePlayed.midi == Int(d1) && notePlayed.midi >= 12 && notePlayed.initialTime >= 0.0 && notePlayed.finalTime == -1.0 && isFull {
                let tickIni = (score.dataSource?.getTimeTicks(time: notePlayed.initialTime))!
                let tickFin = (score.dataSource?.getTimeTicks(time: timeInSeconds))!
                
                var tickIniQ  = (score.dataSource?.quantizeInitialTime(tick: tickIni))!
                let durationQ = (score.dataSource?.quantizeDuration(tick: tickFin - tickIniQ))!
                
                if tickIniQ < 0 {
                    tickIniQ = 0
                }
                
                if tickIniQ >= 0 && durationQ > 0 {
                    
                    let notePlayed = ModelNotePlayed()
                    notePlayed.duration          = durationQ
                    notePlayed.durationQuantized = durationQ
                    notePlayed.initialTime       = tickIni
                    notePlayed.tIniQuantized     = tickIniQ
                    notePlayed.tone              = Int(d1)
                    
                    if notePlayed.tone < 60 {
                        notePlayed.channel = 3
                    } else {
                        notePlayed.channel = 1
                    }
                    
                    if let source = score.dataSource {
                        source.playedNotes.append(notePlayed)
                    }
                    //                            score.addNote(midi: Int(d1), tIniQuantized: tickIniQ, durationQuantized: durationQ, channel: channel)
                }
                print("Note MIDI: ", Int(d1), tickIni, durationQ)
                notesBeingPlayed.remove(at: i)
                break
            }
        }
    }
    //----------------------------------------------------------------
    
    let activity = LoadingView()
    
    func updatePages() {
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear

        if activity.superview == nil {
            view.addSubview(activity)
        }
        
        let x = (view.frame.height * 0.08) * 0.3
        let y = view.frame.height * 0.08
        let width  = view.frame.width - (2.0 * x)
        let height = view.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        dataSource = self
        
        DispatchQueue.global(qos: .userInteractive).async {
            if let source = self.score.dataSource {
                source.generatePauses()
            }

            self.score.state = .playing
            self.score.verifyState()
            DispatchQueue.main.async(execute: {
                self.score.createPages(parameters: parameters)
                
                self.activity.removeFromSuperview()
                
                self.controllers.removeAll()
        
//                var newControllers: [PageScoreViewController] = []
                for i in 0..<self.score.pages.count {
                
                    let controllerPage   = PageScoreViewController()
                    controllerPage.page  = self.score.pages[i]
                    let gesture = UITapGestureRecognizer(target: self, action: #selector(self.showScoreFull(sender:)))
                    gesture.numberOfTapsRequired = 2
                    controllerPage.view.addGestureRecognizer(gesture)
                    self.controllers.append(controllerPage)
                    
                    if i == 0 {
//                        DispatchQueue.main.async(execute: {
                            self.setViewControllers([self.controllers.first!], direction: .forward, animated: false, completion: nil)
//                        })
                    }
                }
                
//                self.controllers = newControllers
        
                self.score.generatePositionsCursor()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        updatePages()
//
//        updateMenu()
        
        score.delegate = self
        
        for _ in 0..<20 {
            openConnectionMIDIDevice()
            closeConnectionMIDIDevice()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        menu.btnSettings.addTarget(self, action: #selector(actionBtnSettings(sender:)), for: .touchUpInside)
        menu.btnResume.addTarget(self, action: #selector(actionBtnResume(sender:)), for: .touchUpInside)
        menu.btnExit.addTarget(self, action: #selector(actionBtnExit(sender:)), for: .touchUpInside)
        menu.btnSoundOnOff.addTarget(self, action: #selector(actionBtnSoundOnOff(sender:)), for: .touchUpInside)
        menu.btnPerform.addTarget(self, action: #selector(actionSwitchPerform(sender:)), for: .touchUpInside)
        menu.btnCompare.addTarget(self, action: #selector(actionBtnCompare(sender:)), for: .touchUpInside)
        menu.btnStatistic.addTarget(self, action: #selector(actionBtnStatistic(sender:)), for: .touchDown)
        menu.btnQuantization.addTarget(self, action: #selector(actionBtnQuantization(sender:)), for: .touchDown)
        
        
        popUpPlay.btnPlusTenMetronome.addTarget(self, action: #selector(actionBtnPlusTenMetronome(sender:)), for: .touchUpInside)
        popUpPlay.btnMinusTenMetronome.addTarget(self, action: #selector(actionBtnMinusTenMetronome(sender:)), for: .touchUpInside)
    
        popUpPlay.btnDefault.addTarget(self, action: #selector(actionBtnDefault(sender:)), for: .touchUpInside)
        popUpPlay.btnDone.addTarget(self, action: #selector(actionBtnDone(sender:)), for: .touchUpInside)
        popUpPlay.btnPlusOctaveTranspose.addTarget(self, action: #selector(actionBtnPlusOctave(sender:)), for: .touchUpInside)
        popUpPlay.btnMinusOctaveTranspose.addTarget(self, action: #selector(actionBtnMinusOctave(sender:)), for: .touchUpInside)
        popUpPlay.swtSoundLeft.addTarget(self, action: #selector(soundRight(_sender:)), for: .valueChanged)
        popUpPlay.swtSoundRight.addTarget(self, action: #selector(soundLeft(_sender:)), for: .valueChanged)
        
        popUpHistory.btnDone.addTarget(self, action: #selector(actionBtnDoneStatistic(sender:)), for: .touchUpInside)
       
        
        footer.btnHelp.addTarget(self, action: #selector(actionBtnHelp(sender:)), for: .touchUpInside)
        
        helpView.btnClose.addTarget(self, action: #selector(actionBtnCloseHelp(sender:)), for: .touchUpInside)
        self.view.bringSubview(toFront: popUpPlay)
        self.view.bringSubview(toFront: popUpHistory)
        self.view.bringSubview(toFront: footer)
        self.view.bringSubview(toFront: helpView)

        popUpPlay.swtMinimizeCursor.addTarget(self, action: #selector(changeCursorSize(_sender:)), for: .valueChanged)
        
        viewCompare.btnPlayOriginal.addTarget(self, action:  #selector(actionPlayOriginal(sender:)), for: .touchUpInside)
        viewCompare.btnPlayPlayed.addTarget(self, action:  #selector(actionPlayPlayed(sender:)), for: .touchUpInside)
        DispatchQueue.main.async(execute: {
            let inst = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
            self.popUpPlay.setInitialTonality(tom:  inst.getPartitureTonalityForInstrument(inst.midiTransform,  self.initialTonality))
            self.popUpPlay.setMetronome(metronome: (self.score.dataSource?.metronome)!)
        })
        
        self.popUpPlay.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
        
    
        if controllers.count > 0 {
            self.viewCursor.visaoPartitura = controllers[0].view
        }
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.verifyIfMidiIsConnected), userInfo:nil, repeats:true)
       
        self.score.viewCursor = self.viewCursor
        self.score.positionCursor()
        self.score.showCursor()
        
        score.lastInitialTick = 0
        score.positionCursor()
        
        score.isPerformance = false
       
        footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
        
        if let source = score.dataSource {
            quantPlayed = source.quantization
            if source.quantization == 4 {
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_4"), for: .normal)
            }
            if source.quantization == 8 {
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_8"), for: .normal)
            }
            if source.quantization == 16 {
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_16"), for: .normal)
            }
        }
     
    }
    
    @objc private func soundRight(_sender: UISwitch) {
        if self.popUpPlay.swtSoundLeft.isOn == false && self.popUpPlay.swtSoundRight.isOn == false {
             menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            self.soundOn = false
        } else {
            self.soundOn = true
            menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "on"), for: .normal)
        }
    }
    
    @objc private func soundLeft(_sender: UISwitch) {
        if self.popUpPlay.swtSoundLeft.isOn == false && self.popUpPlay.swtSoundRight.isOn == false {
            menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            self.soundOn = false
        } else {
            self.soundOn = true
            menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "on"), for: .normal)
        }
    }
    
    @objc private func changeCursorSize(_sender: UISwitch) {
        if _sender.isOn {
            viewCursor.isMinimized = true
        } else {
            viewCursor.isMinimized = false
        }
        self.viewCursor.updateLayout()
    }
    
    @objc private func verifyIfMidiIsConnected() {
        let sourceCount = MIDIGetNumberOfSources()
        
        if sourceCount > 1 {
            footer.setMidiIsConnected(connected: true)
        } else {
            footer.setMidiIsConnected(connected: false)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        updateMenu()
        
        if fromInterfaceOrientation.isPortrait {
            menu.direction = .horizontal
        } else {
            menu.direction = .vertical
        }
        
        menu.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
    }
    
    func updateMenu() {
        if let customView = Bundle.main.loadNibNamed("HelpViewX", owner: self, options: nil)?.first as? HelpView {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.helpPlayPerform()
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            helpView = customView
        }
        
        if menu == nil {
            let m = MenuView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            
            view.addSubview(m)
            menu = m
        } else {
            menu.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if leftBar == nil {
            let l = MenuBarView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) ))
            l.backgroundColor = UIColor(red: 154 / 255, green: 179 / 255, blue: 205 / 255, alpha: 1.0)
            view.addSubview(l)
            leftBar = l
        } else {
            leftBar.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) )
        }
        
        if rightBar == nil {
            let r = MenuBarView(frame:  CGRect(x: view.frame.width - ((view.frame.height * 0.08) * 0.3), y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) ))
            r.backgroundColor = UIColor(red: 154 / 255, green: 179 / 255, blue: 205 / 255, alpha: 1.0)
            view.addSubview(r)
            rightBar = r
        } else {
            rightBar.frame =  CGRect(x: view.frame.width - ((view.frame.height * 0.08) * 0.3), y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) )
        }
        
        if popUpPlay == nil {
            let r = PopUpPlay(frame:  CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.92)))
            r.backgroundColor = UIColor.clear
            view.addSubview(r)
            popUpPlay = r
        } else {
            popUpPlay.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.92))
        }
       
        if viewCompare == nil {
            let x = (view.frame.height * 0.08) * 0.3
            let y = view.frame.height * 0.08
            let width  = view.frame.width - (2.0 * x)
            let height = view.frame.height - y
            let m = CompareView(frame: CGRect(x: x, y: y, width: width, height: height))
            m.backgroundColor = UIColor.clear
            view.addSubview(m)
            viewCompare = m
        } else {
            viewCompare.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        if popUpHistory == nil {
            let r = PopUpHistory(frame:  CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.895)))
            r.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(r)
            popUpHistory = r
        } else {
            popUpHistory.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.895))
        }
        
        if footer == nil {
            let height = view.frame.height * 0.033
            let m = Footer(frame: CGRect(x: 0.0, y: view.frame.height - height, width: view.frame.width, height: height))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            footer = m
        } else {
            footer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if menuKey == nil {
            let m = MenuKeyEdit(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08))
            
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            
            menuKey = m
        } else {
            menuKey.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        popUpPlay.isHidden = true
      
        viewCompare.isHidden     = true
        popUpHistory.isHidden    = true
        menuKey.isHidden         = true
        
        popUpPlay.delegate       = self
    
        menuKey.delegate         = self
        
        
    }
    
    func getParameters() -> DrawParameter {
        let x = (view.frame.height * 0.08) * 0.3
        let y = view.frame.height * 0.08
        let width  = view.frame.width - (3.0 * x)
        let height = view.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        
        return parameters
    }
    //fghg
    func updateOriginalScore() {
        let mdlScrOriginal = ModelScore()
        
        if let source = score.dataSource {
            mdlScrOriginal.author       = source.author
            mdlScrOriginal.quantization = quantPlayed//source.quantization
            mdlScrOriginal.metronome    = source.metronome
            mdlScrOriginal.title        = source.title
            for note in source.notes {
                mdlScrOriginal.notes.append(note)
            }
            for config in source.configurations {
                mdlScrOriginal.configurations.append(config)
            }
            mdlScrOriginal.typeInstrument = source.typeInstrument
            
            scrOriginal.dataSource = mdlScrOriginal
            scrOriginal.scroll     = true
            source.generatePauses()
            scrOriginal.state      = .compare
            scrOriginal.verifyState()
            
            if let source = self.scrOriginal.dataSource {
                source.generatePauses()
            }
            
            scrOriginal.createPages(parameters: getParameters())
            
            for sub in viewCompare.scrlOriginal.subviews {
                sub.removeFromSuperview()
            }
            
            scrOriginal.pages[0].draw(parentView: viewCompare.scrlOriginal)
            
            viewCompare.scrlOriginal.contentSize = scrOriginal.getSizeScore()
            
            scrOriginal.generatePositionsCursor()
        }
    }
    

    var quantPlayed = 4
    
    func updatePlayedScore() {
        let mdlScrOriginal = ModelScore()
        
        if let source = score.dataSource {
            mdlScrOriginal.author       = source.author
            mdlScrOriginal.quantization = quantPlayed
            mdlScrOriginal.metronome    = source.metronome
            mdlScrOriginal.title        = source.title
            
            source.verifyCorrectsNotes()
            
            for note in source.playedNotes {
                let n = ModelNote()
                n.tone              = note.tone
                n.duration          = note.duration
                n.durationQuantized = note.durationQuantized
                n.initialTime       = note.initialTime
                n.tIniQuantized     = note.tIniQuantized
                n.channel           = note.channel
                n.isCorrect         = note.isCorrect
                
                mdlScrOriginal.notes.append(n)
            }
            
            for config in source.configurations {
                mdlScrOriginal.configurations.append(config)
            }
            mdlScrOriginal.typeInstrument = source.typeInstrument
            
            scrPlayed.dataSource = mdlScrOriginal
            scrPlayed.scroll     = true
            source.generatePauses()
            scrPlayed.state      = .compare
            scrPlayed.verifyState()
            
            while scrPlayed.bars.count < scrOriginal.bars.count {
                scrPlayed.addBar()
            }
            
            if let source = self.scrPlayed.dataSource {
                source.generatePauses()
            }
            
            scrPlayed.createPages(parameters: getParameters())
            
            for sub in viewCompare.scrPlayed.subviews {
                sub.removeFromSuperview()
            }
            scrPlayed.pages[0].draw(parentView: viewCompare.scrPlayed)
            
            viewCompare.scrPlayed.contentSize = scrPlayed.getSizeScore()
            
            scrPlayed.generatePositionsCursor()
        }
    }
    
    @objc func showScoreFull(sender: UITapGestureRecognizer) {
        
        if !isFull {
            applyCrescentAnimation(time: TimeInterval(0.3))
            menu.disableButtons()
        } else {
            applyDecrescentAnimation(time: TimeInterval(0.3))
            menu.enableButtonsByState()
            
            if isPlaying {
                isPlaying = false
                score.pause(tapped: true)
                applyDecrescentAnimation()
                function = .none
            }
        }
        
    }
    
    func applyCrescentAnimation(time: TimeInterval = 0.7) {
        isFull = true
        viewCursor.isHidden = false
        self.scrOriginal.stopMIDI()
        self.scrPlayed.stopMIDI()
        viewCompare.btnPlayPlayed.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
        viewCompare.btnPlayOriginal.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
//        score.verifyPostitions()
//        score.movePage()
        
        if !self.score.isPerformance {
            score.paintAllNotesWithColor()
        }
        
        popUpHistory.isHidden = true
        UIView.animate(withDuration: time, animations: {
            self.menu.alpha = 0.0
            self.leftBar.alpha = 0.0
            self.rightBar.alpha = 0.0
       
            self.viewCompare.alpha     = 0.0
            self.menuKey.alpha         = 0.0
        }, completion: { (value: Bool) in
            if self.function == .start {
                self.score.isSoundCursor      = self.popUpPlay.swtSoundCursor.isOn
                self.score.isMinimizeCursor   = self.popUpPlay.swtMinimizeCursor.isOn
                self.score.isSoundLeft        = self.popUpPlay.swtSoundLeft.isOn
                self.score.isSoundRight       = self.popUpPlay.swtSoundRight.isOn
                self.isEndPerformance         = false
               
                
                self.score.stop()
                self.viewCursor.compassoCursor.bar = 0
                self.score.play(viewCursor: self.viewCursor, soundOn: self.soundOn)
                self.isPlaying = true
                
                if self.score.isPerformance {
                    self.score.dataSource?.playedNotes.removeAll()
                    self.openConnectionMIDIDevice()
                }
            }
            if self.function == .resume {
                self.score.isSoundCursor      = self.popUpPlay.swtSoundCursor.isOn
                self.score.isMinimizeCursor   = self.popUpPlay.swtMinimizeCursor.isOn
                self.score.isSoundLeft        = self.popUpPlay.swtSoundLeft.isOn
                self.score.isSoundRight       = self.popUpPlay.swtSoundRight.isOn
               
                
                self.score.pause()
                self.score.play(viewCursor: self.viewCursor, soundOn: self.soundOn)
                self.isPlaying = true
                
                self.viewCursor.compassoCursor.bar = 0
                
                if self.score.isPerformance {
                    self.openConnectionMIDIDevice()
                }
            }
        })
        
    }
    
    func applyDecrescentAnimation(time: TimeInterval = 0.7) {
        isFull = false
        
        UIView.animate(withDuration: time, animations: {
            self.menu.alpha = 1.0
            self.leftBar.alpha = 1.0
            self.rightBar.alpha = 1.0
          
            self.menuKey.alpha = 1.0
        }, completion: { (value: Bool) in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is HelpView || subView is MenuView || subView is ViewCursor || subView is MenuBarView || subView is PopUpPlay || subView is MenuPerformance || subView is CompareView || subView is PopUpHistory || subView is Footer || subView is MenuKeyEdit || subView is UICollectionView {
                self.view.bringSubview(toFront: subView)
            }
        }
        
        if viewCursor.superview == nil { 
            view.addSubview(self.viewCursor)
        }
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var viewCursor = ViewCursor()
    var isPlaying  = false
    
    @objc func actionBtnDefault(sender: UIButton) {
        self.popUpPlay.swtSoundRight.isOn = true
        self.popUpPlay.swtSoundLeft.isOn = true
        self.popUpPlay.swtSoundCursor.isOn = true
        self.popUpPlay.swtMinimizeCursor.isOn = false
        menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "on"), for: .normal)
        soundOn = true
        viewCursor.isMinimized = false
        self.viewCursor.updateLayout()
        
        if(self.initialMetronome != self.score.dataSource?.metronome){
            self.popUpPlay.setMetronome(metronome: self.initialMetronome)
        }
        if(self.initialTonality != self.score.dataSource?.configurations[0].key){
            let inst = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
          
            self.popUpPlay.setInitialTonality(tom:  inst.getPartitureTonalityForInstrument(inst.midiTransform,  self.initialTonality))
        }
        if(score.dataSource?.octave != 0){
            if let source = score.dataSource {
                source.backToDefaultNotes()
                updatePages()
            }
        }
        
    }
    
    @objc func actionBtnSoundOnOff(sender: UIButton) {
        soundOn = !soundOn
        if soundOn{
            self.popUpPlay.swtSoundRight.isOn = true
            self.popUpPlay.swtSoundLeft.isOn = true
            menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "on"), for: .normal)
        } else {
            self.popUpPlay.swtSoundRight.isOn = false
            self.popUpPlay.swtSoundLeft.isOn = false
            menu.btnSoundOnOff.setImage(#imageLiteral(resourceName: "off"), for: .normal)
        }
    }
    
    
    @objc func actionBtnDoneStatistic(sender: UIButton) {
        menu.enableButtons()
        popUpHistory.isHidden = true
    }
    
    @objc func actionBtnPlusTenMetronome(sender: UIButton) {
        if((self.score.dataSource?.metronome)! <= 290){
            self.popUpPlay.setMetronome(metronome: 10+(self.score.dataSource?.metronome)!)
        }
    }
    @objc func actionBtnMinusTenMetronome(sender: UIButton) {
         if((self.score.dataSource?.metronome)! >= 40){
        self.popUpPlay.setMetronome(metronome: (self.score.dataSource?.metronome)! - 10)
        }
    }

    @objc func actionBtnEdit(sender: UIButton) {
        let controller = EditViewController()
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.score = self.score
     //   controller.titleFooter = self.titleFooter
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func actionBtnNextTonality(sender: UIButton) {
        self.popUpPlay.nextTonality()
    }
    @objc func actionBtnPreviousTonality(sender: UIButton) {
        self.popUpPlay.previousTonality()
    }
    
    @objc func actionBtnBackToBegin(sender: UIButton) {
        changePage(0)
        self.viewCursor.compassoCursor.bar = 0
        score.lastInitialTick = 0
        score.positionCursor()
        
        if let source = score.dataSource {
            source.playedNotes.removeAll()
        }
    }
    
    @objc func actionBtnDone(sender: UIButton) {
        if !popUpPlay.isHidden {
            UIView.animate(withDuration: 0.2, animations: {
                self.popUpPlay.alpha = 0.0
                if(self.score.isPerformance){
                    self.menu.enableButtons()
                    self.menu.btnSoundOnOff.isEnabled = false
                    self.menu.lblCursorBegin.isEnabled = false
                }else{
                    self.menu.btnPerform.isEnabled = true
                    self.menu.btnSettings.isEnabled = true
                    
                    self.menu.btnSoundOnOff.isEnabled = true
                    self.menu.btnResume.isEnabled = true
                    self.menu.lblPerform.isEnabled = true
                    self.menu.lblResume.isEnabled = true
                    self.menu.lblCursorBegin.isEnabled = true
                    self.menu.lblSettings.isEnabled = true
                }
            }){ _ in
                self.popUpPlay.isHidden = true
            }
        }
        self.viewCursor.isHidden = false
    }
    
    @objc func actionBtnSettings(sender: UIButton) {
        self.viewCursor.isHidden = !self.viewCursor.isHidden
        viewCompare.isHidden = viewCompare.isHidden
        
        if popUpPlay.isHidden {
            popUpPlay.isHidden = false
            popUpPlay.alpha = 0.0
            UIView.animate(withDuration: 0.2, animations: {
                self.popUpPlay.alpha = 1.0
                if(self.score.isPerformance){
                    self.menu.disableButtons()
                    self.menu.btnSoundOnOff.isEnabled = false
                    self.menu.lblCursorBegin.isEnabled = false
                }else{
                    self.menu.btnPerform.isEnabled = false
                    self.menu.btnSettings.isEnabled = true
                    self.menu.btnSoundOnOff.isEnabled = false
                    self.menu.btnResume.isEnabled = false
                    self.menu.lblPerform.isEnabled = false
                    self.menu.lblSettings.isEnabled = false
                    self.menu.lblCursorBegin.isEnabled = false
                    self.menu.lblResume.isEnabled = false
                }
            })
        } else {
            popUpPlay.isHidden = true
            if(self.score.isPerformance){
                self.menu.enableButtons()
            }else {
                self.menu.enableButtons()
                self.score.isPerformance = false
                self.menu.btnPerform.setImage(#imageLiteral(resourceName: "PerformOFF"), for: .normal)
                self.menu.btnCompare.isEnabled = false
                self.menu.btnQuantization.isEnabled = false
                self.menu.btnStatistic.isEnabled = false
                self.menu.lblCompare.isEnabled = false
                self.menu.lblQuantization.isEnabled = false
                self.menu.lblStatistic.isEnabled = false
//                self.popUpPlay.swtSoundLeft.isEnabled = true
//                self.popUpPlay.swtSoundRight.isEnabled = true
//                self.popUpPlay.lblSoundLeft.isEnabled = true
//                self.popUpPlay.lblSoundRight.isEnabled = true
                score.paintAllNotesWithColor()
            }
        }
        self.menu.btnSettings.isEnabled = true
        self.menu.lblSettings.isEnabled = true
    }
    
    @objc func actionBtnRestart(sender: UIButton) {
      
        popUpPlay.isHidden = true
        viewCursor.removeFromSuperview()
        self.footer.isHidden = true
        addCursor()
        function = .start
        self.score.viewCursor.isHidden = false
        self.score.delegate?.changePage(self.score.getPosCursor().pagina - 1)
        applyCrescentAnimation()
    }
    
    @objc func actionBtnHelp(sender: UIButton) {
        
        helpView.isHidden = false
        viewCursor.isHidden = true
    }
    
    @objc func actionBtnCloseHelp(sender: UIButton) {
        
        helpView.isHidden = true
        viewCursor.isHidden = false
    }
    
    @objc func actionBtnResume(sender: UIButton) {
        AppsFlyerTracker.shared().trackEvent(AFEventPlayMusicClicked, withValues: [:])
        popUpPlay.isHidden = true
        self.footer.isHidden = true
        viewCursor.removeFromSuperview()
        addCursor()
        function = .resume
        self.score.viewCursor.isHidden = false
        self.score.delegate?.changePage(self.score.getPosCursor().pagina - 1)
        applyCrescentAnimation()
    }
    var tocouPlayCompare = false
    var tocouPlayPlayedCompare = false
    
    @objc func actionPlayOriginal(sender: UIButton) {
        
        if(tocouPlayCompare){
            tocouPlayCompare = false
            viewCompare.btnPlayOriginal.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
            self.scrOriginal.stopMIDI()
        }
        else{
            viewCompare.btnPlayOriginal.setBackgroundImage(#imageLiteral(resourceName: "pausecomp"), for: .normal)
            tocouPlayCompare = true
            self.scrOriginal.playMIDI()
            tocouPlayPlayedCompare = false
            viewCompare.btnPlayPlayed.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
            self.scrPlayed.stopMIDI()
            
        }
    }
    
    @objc func actionPlayPlayed(sender: UIButton) {
        
        if(tocouPlayPlayedCompare){
            tocouPlayPlayedCompare = false
            viewCompare.btnPlayPlayed.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
            self.scrPlayed.stopMIDI()
        }
        else{
            viewCompare.btnPlayPlayed.setBackgroundImage(#imageLiteral(resourceName: "pausecomp"), for: .normal)
            tocouPlayPlayedCompare = true
            self.scrPlayed.playMIDI(onlyPlayedNotes: true)
            tocouPlayCompare = false
            viewCompare.btnPlayOriginal.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
            self.scrOriginal.stopMIDI()
        }
    }
    
    @objc func actionSwitchPerform(sender: UIButton) {
        AppsFlyerTracker.shared().trackEvent(AFEventPerformClicked, withValues: [:])
        if(!self.score.isPerformance){
            self.popUpPlay.swtSoundRight.isEnabled = false
            self.popUpPlay.swtSoundLeft.isEnabled = false
            self.popUpPlay.lblSoundRight.isEnabled = false
            self.popUpPlay.lblSoundLeft.isEnabled = false
            self.menu.btnSoundOnOff.isEnabled = false
            self.menu.lblCursorBegin.isEnabled = false
            self.score.isPerformance = true
            self.menu.btnPerform.setImage(#imageLiteral(resourceName: "PerformON"), for: .normal)
            self.menu.btnCompare.isEnabled = true
            self.menu.btnQuantization.isEnabled = true
            self.menu.btnStatistic.isEnabled = true
            self.menu.lblCompare.isEnabled = true
            self.menu.lblQuantization.isEnabled = true
            self.menu.lblStatistic.isEnabled = true
//
        } else {
            self.menu.btnSoundOnOff.isEnabled = true
            self.menu.lblCursorBegin.isEnabled = true
            self.popUpPlay.swtSoundRight.isEnabled = true
            self.popUpPlay.swtSoundLeft.isEnabled = true
            self.popUpPlay.lblSoundRight.isEnabled = true
            self.popUpPlay.lblSoundLeft.isEnabled = true
            self.score.isPerformance = false
            self.menu.btnPerform.setImage(#imageLiteral(resourceName: "PerformOFF"), for: .normal)
            self.menu.btnCompare.isEnabled = false
            self.menu.btnQuantization.isEnabled = false
            self.menu.btnStatistic.isEnabled = false
            self.menu.lblCompare.isEnabled = false
            self.menu.lblQuantization.isEnabled = false
            self.menu.lblStatistic.isEnabled = false

        }
    }
    
    func addCursor() {
        if viewCursor.superview == nil {
            view.addSubview(viewCursor)
        }
    }
    
    
    @objc func actionBtnSave(sender: UIButton) {
       
        
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        
        if activity.superview == nil {
            view.addSubview(activity)
        }
        
        DispatchQueue.global(qos: .userInteractive).async {
            var message = ""
            if let source = self.score.dataSource {
                if source.inserted {
                    message = "Score updated"
                    source.status = 2
                    source.inserted = true
                    DAOScore.getInstance().updateScore(source)
                } else {
                    
               
                    message = "Score saved"
                    source.status = 2
                    DAOScore.getInstance().addScore(source)
                    let user = UserManager.sharedUserManager
                    let token = user.token
                    
                    
                    self.service(urlToRequest: user.server + ":3001/operator/status", method: ServiceRemote.Method.POST.rawValue, token: token, idMidi: String(source.idMidi))
                    
                }
            }
            
            DispatchQueue.main.async(execute: {
                self.activity.removeFromSuperview()
                let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion:nil)
            })
        }
        
    }
    
    // muda o status
    func service(urlToRequest: String, method: String, token:String, idMidi: String) {
     
        var request = URLRequest(url:URL(string:urlToRequest)!)
        let postString = ["token":token, "id": idMidi] //.md5()
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
       
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){ data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            }.resume()
    }
    
   
   
    let defaults = UserDefaults.standard
   
    @objc func actionBtnExit(sender: UIButton) {
        if let contr = parentControl {
            self.scrOriginal.stopMIDI()
            self.scrPlayed.stopMIDI()
            contr.items.removeAllObjects()
            contr.scores.removeAll()
            var email = ""
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }

            let defaults = UserDefaults.standard
           
            contr.profileView.isHidden = true
            contr.storeView.isHidden = true
            contr.collectionViewAllStore.isHidden = true
            contr.collectionViewNewsStore.isHidden = true
            contr.menu.function = .myMusic
            contr.popup.isHidden = true
            contr.popup.isMyMusic()
            contr.btnnewcomposition.isHidden = true
            contr.menu.btnFilter.isEnabled = true
            contr.menu.btnMyMusic.setImage(#imageLiteral(resourceName: "mymusic_select"), for: .normal)
            contr.menu.btnCompose.setImage(#imageLiteral(resourceName: "impost"), for:.normal)
            contr.menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
            contr.menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
            contr.menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
            
            contr.scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMM") as! String, email: email, busca: "")

            for score in contr.scores {
                contr.items.add(score.title)
            }

            contr.collectionView.reloadData()
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func actionBtnPlusOctave(sender: UIButton) {
        if let source = score.dataSource {
            if source.plusOctaveTonality() {
                updatePages()
            }
        }
    }
    @objc func actionBtnMinusOctave(sender: UIButton) {
        if let source = score.dataSource {
            if source.minusOctaveTonality() {
                updatePages()
            }
        }
    }
    @objc func actionBtnCompare(sender: UIButton) {
        viewCursor.isHidden = !viewCursor.isHidden
        viewCompare.isHidden = !viewCompare.isHidden
        
        if let source = score.dataSource {
            quantPlayed = source.quantization
        }
    }
    
    @objc func actionBtnStatistic(sender: UIButton) {
        popUpHistory.isHidden = !popUpHistory.isHidden
        if(popUpHistory.isHidden){
            menu.enableButtons()
            
        }else {
            menu.disableButtons()
        }
        menu.btnStatistic.isEnabled = true
    }
    
    @objc func actionBtnQuantization(sender: UIButton) {
        if let source = scrOriginal.dataSource {
            if source.quantization == 4 {
                source.quantization = 8
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_8"), for: .normal)
            } else if source.quantization == 8 {
                source.quantization = 16
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_16"), for: .normal)
            } else if source.quantization == 16 {
                source.quantization = 4
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_4"), for: .normal)
            }
            
            for note in source.notes {
                note.tIniQuantized = source.quantizeInitialTime(tick: note.tIniQuantized)
            }

            quantPlayed = source.quantization
            
            source.extendNotes(numerator: source.configurations[0].numerator, denominator: source.configurations[0].denominator)
            
            updateOriginalScore()
        }
        
        if let source = scrPlayed.dataSource {
            if source.quantization == 4 {
                source.quantization = 8
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_8"), for: .normal)
            } else if source.quantization == 8 {
                source.quantization = 16
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_16"), for: .normal)
            } else if source.quantization == 16 {
                source.quantization = 4
                menu.btnQuantization.setImage(#imageLiteral(resourceName: "1_4"), for: .normal)
            }
            
            for note in source.notes {
                note.tIniQuantized = source.quantizeInitialTime(tick: note.tIniQuantized)
            }
            
            source.extendNotes(numerator: source.configurations[0].numerator, denominator: source.configurations[0].denominator)
            
            updatePlayedScore()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if isPlaying {
            isPlaying = false
            applyDecrescentAnimation()
            score.pause(tapped: true)
            function = .none
            if self.score.isPerformance {
                isEndPerformance = true
                closeConnectionMIDIDevice()
            }
        }
        
        if let touch = touches.first {
            if let viewT = touch.view {
                if viewT is ViewBar {
                    let viewBar = viewT as! ViewBar
                    self.score.changeTStartCursor(viewBar.start)
                    print("Entrou bar", viewBar.start)
                }
            }
        }
    }

    var canUpdate = false
    
    func changeKey(newKey: OptionKeySelected) {
       
        var keySelect = KeyType.C_Am
        
        switch newKey {
        case .F:
            keySelect = .F_Dm
        case .Bb:
            keySelect = .Bb_Gm
        case .Eb:
            keySelect = .Eb_Cm
        case .Ab:
            keySelect = .Ab_Fm
        case .Db:
            keySelect = .Db_BBm
        case .Gb:
            keySelect = .Gb_Ebm
        case .G:
            keySelect = .G_Em
        case .D:
            keySelect = .D_Bm
        case .A:
            keySelect = .A_Fsm
        case .E:
            keySelect = .E_Csm
        case .B:
            keySelect = .B_Gsm
        case .Fs:
            keySelect = .Fs_Dsm
        case .Cs:
            keySelect = .Cs_Asm
        case .C:
            keySelect = .C_Am
        case .Cb:
            keySelect = .C_Am
        }
        
        if canUpdate {
            if let source = score.dataSource {
                source.changeTonality(newTonality: keySelect)
                updatePages()
            }
        } else {
            canUpdate = true
        }
    }
    
   
    
    deinit {
        menu = nil
        leftBar = nil
        rightBar = nil
        
     
    }
    
    var currentController = PageScoreViewController()
}

extension ScoreViewController: MenuKeyEditDelegate {
    func changeOption(_ option: OptionKeySelected) {
        changeKey(newKey: option)
    }
}


extension ScoreViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if isPlaying || !popUpPlay.isHidden {
            return nil
        }
        
        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex < 1 {
            return nil
        }
        
        self.viewCursor.visaoPartitura = controllers[viewControllerIndex - 1].view
        currentController = controllers[viewControllerIndex - 1]
        
        if viewControllerIndex - 1 != self.score.getPosCursor().pagina - 1 {
            self.score.viewCursor.isHidden = true
        } else {
            self.score.viewCursor.isHidden = false
        }
        
        return controllers[viewControllerIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if isPlaying || !popUpPlay.isHidden {
            return nil
        }
        
        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex > controllers.count - 2 {
            return nil
        }
        
        self.viewCursor.visaoPartitura = controllers[viewControllerIndex + 1].view
        currentController = controllers[viewControllerIndex + 1]
        
        if viewControllerIndex + 1 != self.score.getPosCursor().pagina - 1 {
            self.score.viewCursor.isHidden = true
        } else {
            self.score.viewCursor.isHidden = false
        }
        
        return controllers[viewControllerIndex + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
  
        self.viewCursor.removeFromSuperview()
        self.view.addSubview(viewCursor)
        self.viewCursor.visaoPartitura = currentController.view
    }
    
}

extension ScoreViewController: PopUpPlayDelegate {
    func changeKey(_ key: KeyType) {
        
        if let source = score.dataSource {
            source.changeTonality(newTonality: key)

//            updatePages()
        }
    }
    func transpose(_ option: OptionKeySelected) {
        changeKey(newKey: option)
    }
    
    func changeTimeSignature(_ option: TimeSelected) {
        var numerator   = 4
        var denominator = 4
        
        switch option {
        case .doisdois:
            numerator   = 2
            denominator = 2
        case .tresdois:
            numerator   = 3
            denominator = 2
        case .doisQuartos:
            numerator   = 2
            denominator = 4
        case .tresQuartos:
            numerator   = 3
            denominator = 4
        case .quatroQuartos:
            numerator   = 4
            denominator = 4
        case .cincoQuartos:
            numerator   = 5
            denominator = 4
        case .seisQuartos:
            numerator   = 6
            denominator = 4
        case .tresOitavos:
            numerator   = 3
            denominator = 8
        case .seisOitavos:
            numerator   = 6
            denominator = 8
        case .noveOitavos:
            numerator   = 9
            denominator = 8
        case .dozeOitavos:
            numerator   = 12
            denominator = 8
        }
        
        var found = false
        if let source = score.dataSource {
            var i = 0
            while i < source.configurations.count {
                if source.configurations[i].bar == 1  {
                    source.configurations[i].numerator   = numerator
                    source.configurations[i].denominator = denominator
                    found = true
                }
                
                i += 1
            }
            if !found {
                let config = ModelBarConfiguration()
                config.numerator = numerator
                config.denominator = denominator
                source.configurations.append(config)
            }
            
//            updatePages()
        }
    }
    
    func changeMetronome(_ value: Int) {
        if let source = score.dataSource {
            source.metronome = value
        }
        
        score.changeMetronome()
    }
    
}


extension ScoreViewController: DrawScoreDelegate {
    
    func scoreStart() {
        if getMidiIsConnected() {
            score.isMidi = true
            self.initialTimeStamp = MIDITimeStamp(mach_absolute_time())
            openPorts()
        } else {
            self.recorderAudio.sampleRate = 44100.0
            let time     = 60.0 / Float((self.score.dataSource?.metronome)!)
            let baseNote = 4.0 / Float(((self.score.startTimeBar.first)!.denominator))
            let bar      = Float(self.score.startTimeBar.first!.numerator) * baseNote
            
            let numberOfSamples = Int(time * bar * Float(8820))
            
            self.recorderAudio.bufferSize = numberOfSamples

            self.recorderAudio.startRecording()
            
            self.score.isMidi = false
        }
        
        self.initialTimeStamp = MIDITimeStamp(mach_absolute_time())
        openPorts()
    }
    
    func scorePaused() {
        let lblPaused = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        lblPaused.text = "Paused"
        
        lblPaused.numberOfLines = 0
        lblPaused.font = UIFont(name: "Lato-Bold", size: self.view.bounds.size.height * 0.08)
        lblPaused.textColor = UIColor(red: 140/255, green: 28/255, blue: 37/255, alpha: 1.0)
        lblPaused.textAlignment = NSTextAlignment.center
        lblPaused.backgroundColor = UIColor.clear
        
        view.addSubview(lblPaused)
        
        UIView.animate(withDuration: 2.0, animations: {
            lblPaused.alpha = 0.0
        }, completion: { (value: Bool) in
            lblPaused.removeFromSuperview()
            if self.score.isPerformance {
                self.updateOriginalScore()
                self.updatePlayedScore()
            }
        })
        
        if score.isPerformance {
            viewCompare.isHidden     = true
            viewCompare.alpha        = 1.0
            closeConnectionMIDIDevice()
            
            
            if !score.isMidi {
                recorderAudio.stopRecording()
            }
        }
        self.footer.isHidden = false
    }
    
    func scoreStopped() {
        let lblStopped = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        lblStopped.text = "Complete"
        
        lblStopped.numberOfLines = 0
        lblStopped.font = UIFont(name: "Lato-Bold", size: self.view.bounds.size.height * 0.08)
        lblStopped.textColor = UIColor(red: 140/255, green: 28/255, blue: 37/255, alpha: 1.0)
        lblStopped.textAlignment = NSTextAlignment.center
        lblStopped.backgroundColor = UIColor.clear
        
        view.addSubview(lblStopped)
        
        applyDecrescentAnimation(time: 1.5)
        self.footer.isHidden = false
        UIView.animate(withDuration: 2.0, animations: {
            lblStopped.alpha = 0.0
        }, completion: { (value: Bool) in
            lblStopped.removeFromSuperview()
            self.updateOriginalScore()
            self.updatePlayedScore()
        })
        
        isPlaying = false
        function = .none
        
        if self.score.isPerformance {
          //PODE TESTAR?
            viewCompare.isHidden     = true
            viewCompare.alpha        = 1.0
            closeConnectionMIDIDevice()
            
            if !score.isMidi {
                recorderAudio.stopRecording()
            }
        }
    }
    
    func changePage(_ newPage: Int) {
        
        setViewControllers([controllers[newPage]], direction: .forward, animated: false, completion: nil)
//        viewCursor.removeFromSuperview()
//        addCursor()
    }
    
}

extension ScoreViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return score.numberOfPages
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! ScoreCollectionViewCell

        let index = indexPath.row
        
        score.pages[index].draw(parentView: cell.pageView)

//        cell.pageView = viewPage
        
        return cell
    }
    
    
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension UIPageViewController {
    
    func goToNextPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: animated, completion: nil)
    }
    
    func goToPreviousPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        setViewControllers([previousViewController], direction: .reverse, animated: animated, completion: nil)
    }
    
}

extension ScoreViewController: RecordAudioDelegate {
    func ticksQuant(_ quant: Int) -> Int {
        switch quant {
        case 4:
            return 960
        case 8:
            return 480
        case 16:
            return 240
        default:
            return 240
        }
    }
    
    
    func bufferFull(_ samples: [Float], index: Int) {
        var tempSamp: [Float] = []
        
        DispatchQueue.global(qos: .background).async {

            for i in 0..<samples.count {
                if i % 5 == 0 {
                    tempSamp.append(samples[i])
                }
            }

            let v1 = 60.0 / Double(self.score.dataSource!.metronome)
            let vlQuantTicks = Double(self.ticksQuant(self.score.dataSource!.quantization)) / 960.0
            let v2 = Int(round(v1 * vlQuantTicks * 8820))

            self.processor.metronome = self.score.dataSource!.metronome

            let filters = self.analyser1.frequencies(in: tempSamp, extWindowLenght: v2)
            var notas: [[Int]] = []
            
            for filter in filters {
                var normalizedSignal = self.processor.filter(input: tempSamp, filtro: filter)
                notas.append(contentsOf: self.processor.extractNotes(input: &normalizedSignal, filtro: filter))
            }
            
            for note in notas {
                let tone  = note[0] + 24

                if index >= 0 {
                    var initialTime = (3840 * index)

                    initialTime += note[1]
                    let duration = note[2]
                    
                    let note = ModelNotePlayed()
                    note.tone              = tone
                    note.initialTime       = initialTime
                    note.tIniQuantized     = (self.score.dataSource?.quantizeInitialTime(tick: initialTime))!
                    note.duration          = duration
                    note.durationQuantized = (self.score.dataSource?.quantizeDuration(tick: duration))!
                    
                    
                    if note.tone < 60 && !self.defaults.bool(forKey: "Mono") {
                        note.channel = 3
                    } else {
                        note.channel = 1
                    }

                    if let source = self.score.dataSource {
                        source.playedNotes.append(note)
                    }
                }
            }
            
            DispatchQueue.main.async(execute: {
                self.score.paintWrongNotes()

            })
        }
    }
    
}

enum FunctionPlayer {
    case start
    case resume
    case none
}


