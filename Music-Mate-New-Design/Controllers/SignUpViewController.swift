//
//  SignUpViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/20/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit
import AppsFlyerLib

class SignUpViewController: UIViewController {

    @IBOutlet weak var lblUpload: UILabel!
    @IBOutlet weak var imgUser     : UIImageView!
    @IBOutlet weak var viewSignUp  : UIView!
    @IBOutlet weak var txtName     : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtEmail    : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var txtConfirm  : UITextField!
    
    let activity = LoadingView()
    var imgChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewSignUp.layer.cornerRadius = 10
        
        let imgUser = UIImageView(image: #imageLiteral(resourceName: "login_user"))
        if let size = imgUser.image?.size {
            imgUser.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        let imgUser1 = UIImageView(image: #imageLiteral(resourceName: "login_user"))
        if let size = imgUser1.image?.size {
            imgUser1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        let imgName = UIImageView(image: #imageLiteral(resourceName: "login_mail"))
        if let size = imgName.image?.size {
            imgName.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        let imgPass = UIImageView(image: #imageLiteral(resourceName: "login_password"))
        if let size = imgPass.image?.size {
            imgPass.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        let imgPass1 = UIImageView(image: #imageLiteral(resourceName: "login_password"))
        if let size = imgPass1.image?.size {
            imgPass1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        imgUser.contentMode  = .center
        imgUser1.contentMode = .center
        imgName.contentMode  = .center
        imgPass.contentMode  = .center
        imgPass1.contentMode = .center
        
        txtName.leftViewMode     = .always
        txtLastName.leftViewMode = .always
        txtEmail.leftViewMode    = .always
        txtPassword.leftViewMode = .always
        txtConfirm.leftViewMode  = .always
        
        txtName.leftView     = imgUser
        txtLastName.leftView = imgUser1
        txtEmail.leftView    = imgName
        txtPassword.leftView = imgPass
        txtConfirm.leftView  = imgPass1
        
        if let left = txtName.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtName.frame.size.width * 0.1, height: txtName.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        if let left = txtLastName.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtLastName.frame.size.width * 0.1, height: txtLastName.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        if let left = txtEmail.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtEmail.frame.size.width * 0.1, height: txtEmail.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        if let left = txtPassword.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtPassword.frame.size.width * 0.1, height: txtPassword.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        if let left = txtConfirm.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtConfirm.frame.size.width * 0.1, height: txtConfirm.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        activity.isHidden    = true
        
        self.view.addSubview(activity)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height / 1.5
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += keyboardSize.height / 1.5
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        imgUser.isUserInteractionEnabled = true
        viewSignUp.bringSubview(toFront: imgUser)
    }
    
    func actionImgUser() {
        let actionSheet = UIAlertController(title: "Please Select Camera or Photo Library", message: "", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Upload a Photo", style: .default, handler: { (UIAlertAction) in
            self.openPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (UIAlertAction) in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.lblUpload
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func actionBtnCreate(_ sender: UIButton) {
        view.endEditing(true)
        activity.isHidden   = false
        activity.start()
        viewSignUp.isHidden = true
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let result = formatter.string(from: date)
        let name     = txtName.text
        let lastName = txtLastName.text
        let email    = txtEmail.text
        let password = txtPassword.text
        
        var data: [String:Any] = [:]
        data["firstName"] = name!
        data["lastName"]  = lastName!
        data["email"]     = email!
        data["password"]  = md5(password!)
        data["lastEditionDate"] = result
        
        if imgChanged {
            if let img = imgUser.image {
//                if let compressed = img. {
                    data["image"]     = imgToBase64(image: img)
//                }
            }
        }
        
        let user  = UserManager.sharedUserManager

        user.server = "http://37.208.116.28"
        
        let postString : [String : Any] = ["user":data ]
        
        let urlToRequest = user.server + ":3002/signup"
        
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]

                if let data = parseResult["status"] as? Bool {
                    let status = data
                    
                    if status {
                        let user = UserManager.sharedUserManager
                        
                        if (parseResult["status"] as! Bool == true) {
                            if let dataUser = parseResult["results"] as? [String:AnyObject] {
                                user.token  = dataUser["token"] as! String
                                
                                if let data = dataUser["user"] as? [String:AnyObject] {
                                    //                        user.tipo   = data["type"] as! Int
                                    user.status = true
                                    user.email  = data["email"] as! String
                                    if let image = data["image"] {
                                        user.image  = image as! String
                                    } else {
                                        user.image = ""
                                    }
                                    
                                    let dao            = DAOUser.getInstance()
                                    let modelUser      = ModelUser()
                                    modelUser.email    = data["email"] as! String
                                    modelUser.firstName     = data["firstName"] as! String
                                    modelUser.token    = user.token
                                    modelUser.image = user.image
                                    modelUser.lastName = data["lastName"] as! String
                                    
                                    
                                    // Salva o usuário
                                    dao.addUser(modelUser)
                                }
                            }
                        }

                        DispatchQueue.main.async {
                            let controller = HomeViewController()
                            controller.modalTransitionStyle = .crossDissolve
                            controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                            
                            self.present(controller, animated: true, completion: nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.activity.isHidden    = true
                            self.viewSignUp.isHidden  = false

                            self.activity.done()
                            
                            self.showAlert(error: true)
                        }
                    }
                    
                }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
        AppsFlyerTracker.shared().trackEvent(AFEventUserRegistered, withValues: [:])
    }
    
    func showAlert(error: Bool = false) {
        if !error {
            let alert = UIAlertController(title: "Success", message: "Account created.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                
                self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Error", message: "Complete all the fields correctly.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    deinit {
        print("Entrou deinit SignUpViewController")
    }
    
    func imgToBase64(image: UIImage) -> String {
        let img : UIImage = image
        //Now use image to create into NSData format
        let imageData:Data = img.jpeg(.lowest)!

        let strBase64 = imageData.base64EncodedString()
        
        return strBase64
    }
    
    func md5(_ string: String) -> String {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        return hexString
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if let v = touches.first?.view {
            if v == imgUser {
                actionImgUser()
            }
        }
        
        if let first = touches.first {
            if let v = first.view {
                if v != txtName && v != txtLastName && v != txtEmail && v != txtPassword && v != txtConfirm {
                    view.endEditing(true)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}

extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if info[UIImagePickerControllerOriginalImage] is UIImage {
            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
            imgUser.contentMode = .scaleAspectFit //3
            imgUser.image = chosenImage //4
            imgChanged = true
        }
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
