//
//  ComposeViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/30/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import CoreMIDI
import CoreAudio
import AudioToolbox
import Foundation
import Darwin
import AVFoundation
import MessageUI
import AppsFlyerLib

class ComposeViewController: UIPageViewController {
    
    var strSamp44100 = ""
    var strSamp8820 = ""
    
     let activity = LoadingView()
    
    var processor = NotesProcessor()
    let analyser1 = NoteAnalyser1(8820, fourierLenght: 8192, intWindowLenght: 256)
    
    var timeTeste: UInt64 = 0
    weak var menu         : MenuViewCompose!
    weak var leftBar      : MenuBarView!
    weak var rightBar     : MenuBarView!
    weak var popUpCompose : PopUpCompose!
    weak var footer       : Footer!
    weak var popUpAlert   : PopUpAlert!
     weak var parentControl: HomeViewController? = nil
    var actualPage = 0
    
    var score: DrawScore!
    var historyScores: [DrawScore] = []
    var helpView = HelpView()
    
    private let SAMPLE_RATE = 8820.0
    
    lazy var controllers: [PageScoreViewController] = []
    
    private var isFull      : Bool = false
    private var isComposing : Bool = false
    
    private var recorder : AVAudioRecorder!
    var nextIndex = 0
    var fila = OperationQueue()
    var operations: [BlockOperation] = []
    
    var function = FunctionPlayer.none
    
    var tickInitial = 0
    var samplerUnit: AudioUnit?
    var client = MIDIClientRef()
    var inputPort = MIDIPortRef()
    
    var initialTimeStamp: UInt64 = 0

    let analyser = SpectrumAnalyser(sampleRate: 8820)
    
    //Gravador de audio
    var recorderAudio = RecordAudio()
    var canUpdate = false
    
    
    override func viewDidLoad() {
        AppsFlyerTracker.shared().trackEvent(AFEventComposeClicked, withValues: [:])
        super.viewDidLoad()
        
        dataSource = self
        

        updatePages()
        updateMenu()
       
        score.delegate = self
        
        for _ in 0..<20 {
            openConnectionMIDIDevice()
            closeConnectionMIDIDevice()
        }
        
        initialTimeStamp = MIDITimeStamp(mach_absolute_time())
        
        hideKeyboardWhenTappedAround()
        
        recorderAudio.delegate = self
        
        recorderAudio.startAudioUnit()
        recorderAudio.startRecording()
        recorderAudio.stopRecording()
        
        if let source = score.dataSource {
            if(source.title == ""){
                source.title = "New Composition"
            }
        }
        score.changeTitle()
        
    }
   
    func openConnectionMIDIDevice() {
        let np:MIDINotifyProc = { (notification:UnsafePointer<MIDINotification>, refcon:UnsafeMutableRawPointer?) in}
        MIDIClientCreate("MyMIDIClient" as CFString, np, nil, &client)
        MIDIInputPortCreateWithBlock(client, "MyMIDIClient" as CFString, &inputPort, receivingEventMidi)
        
//        setupAudioUnit(samplerUnit: &samplerUnit)
    }
    
    func openPorts() {
        let sourceCount = MIDIGetNumberOfSources()
        
        for srcIndex in 0 ..< sourceCount {
            let midiEndPoint = MIDIGetSource(srcIndex)
            MIDIPortConnectSource(inputPort, midiEndPoint, nil)
        }
    }
    
    func closeConnectionMIDIDevice() {
        let sourceCount = MIDIGetNumberOfSources()
        
        for srcIndex in 0 ..< sourceCount {
            let midiEndPoint = MIDIGetSource(srcIndex)
            MIDIPortDisconnectSource(inputPort, midiEndPoint)
            MIDIPortDispose(inputPort)
        }
    }
    
    func setupAudioUnit(samplerUnit: inout AudioUnit?) {
        let inputBus   : UInt32 =  1
        let outputBus  : UInt32 =  0
        let sampleRate : Double = 44100.0
        
        var componentDesc:  AudioComponentDescription
            = AudioComponentDescription(
                componentType:          OSType(kAudioUnitType_Output),
                componentSubType:       OSType(kAudioUnitSubType_RemoteIO),
                componentManufacturer:  OSType(kAudioUnitManufacturer_Apple),
                componentFlags:         UInt32(0),
                componentFlagsMask:     UInt32(0) )
        
        let component: AudioComponent! = AudioComponentFindNext(nil, &componentDesc)
        
        AudioComponentInstanceNew(component, &samplerUnit)
        
        var one_ui32: UInt32 = 1
        AudioUnitSetProperty(samplerUnit!,
                             kAudioOutputUnitProperty_EnableIO,
                             kAudioUnitScope_Input,
                             inputBus,
                             &one_ui32,
                             UInt32(MemoryLayout<UInt32>.size))
        
        let nc = 2  // 2 channel stereo
        var streamFormatDesc:AudioStreamBasicDescription = AudioStreamBasicDescription(
            mSampleRate:        Double(sampleRate),
            mFormatID:          kAudioFormatLinearPCM,
            mFormatFlags:       ( kAudioFormatFlagsNativeFloatPacked ),
            mBytesPerPacket:    UInt32(nc * MemoryLayout<UInt32>.size),
            mFramesPerPacket:   1,
            mBytesPerFrame:     UInt32(nc * MemoryLayout<UInt32>.size),
            mChannelsPerFrame:  UInt32(nc),
            mBitsPerChannel:    UInt32(8 * (MemoryLayout<UInt32>.size)),
            mReserved:          UInt32(0)
        )
        
        AudioUnitSetProperty(samplerUnit!,
                             kAudioUnitProperty_StreamFormat,
                             kAudioUnitScope_Input, outputBus,
                             &streamFormatDesc,
                             UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
        
        AudioUnitSetProperty(samplerUnit!,
                             kAudioUnitProperty_StreamFormat,
                             kAudioUnitScope_Output,
                             inputBus,
                             &streamFormatDesc,
                             UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
        
        AudioUnitSetProperty(samplerUnit!,
                             AudioUnitPropertyID(kAudioUnitProperty_ShouldAllocateBuffer),
                             AudioUnitScope(kAudioUnitScope_Output),
                             inputBus,
                             &one_ui32,
                             UInt32(MemoryLayout<UInt32>.size))
    }
    
    func receivingEventMidi(packetList: UnsafePointer<MIDIPacketList>, srcConnRefCon: UnsafeMutableRawPointer?) -> Void {
        let packets = packetList.pointee
        let packet:MIDIPacket = packets.packet
        
        var ap = UnsafeMutablePointer<MIDIPacket>.allocate(capacity: 1)
        ap.initialize(to: packet)
        
        for _ in 0 ..< packets.numPackets {
            let p = ap.pointee
            var strTime = TimerDarwin()
            strTime.startTime = initialTimeStamp
            strTime.stopTime = p.timeStamp

            handle(packet: p, timeInSeconds: strTime.seconds)
            
            ap = MIDIPacketNext(ap)
            
        }
    }

    var notesBeingPlayed: [NoteMidiPlayed] = []
    
    func handle(packet:MIDIPacket, timeInSeconds: Double) {
        
        let status = packet.data.0
        let d1 = packet.data.1
        let d2 = packet.data.2
        let rawStatus = status & 0xF0
        let channel = status & 0x0F
        
        switch rawStatus {
            
        case 0x80:
            print("Note off. Channel \(channel) note \(d1) velocity \(d2)")
//            playNoteOff(channel: UInt32(channel), noteNum: UInt32(d1))
            noteOff(d1: d1, d2: d2, timeInSeconds: timeInSeconds)
        case 0x90,0x91,0x92,0x93,0x94:
            print("Note on. Channel \(channel) note \(d1) velocity \(d2)")
//            playNoteOn(channel: UInt32(channel), noteNum:UInt32(d1), velocity: UInt32(d2))
            if UInt32(d2) == 0 {
                noteOff(d1: d1, d2: d2, timeInSeconds: timeInSeconds)
            } else if isComposing {
                let noteMidi = NoteMidiPlayed(initialTime: timeInSeconds, finalTime: -1.0, midi: Int(d1))
                notesBeingPlayed.append(noteMidi)
            }
        case 0xA0:
            print("Polyphonic Key Pressure (Aftertouch). Channel \(channel) note \(d1) pressure \(d2)")
            
        case 0xB0:
            print("Control Change. Channel \(channel) controller \(d1) value \(d2)")
            
        case 0xC0:
            print("Program Change. Channel \(channel) program \(d1)")
            
        case 0xD0:
            print("Channel Pressure (Aftertouch). Channel \(channel) pressure \(d1)")
            
        case 0xE0:
            print("Pitch Bend Change. Channel \(channel) lsb \(d1) msb \(d2)")
            
        default:
            print("\(rawStatus)")
            break
            
//            print("Unhandled message \(status)")
        }
        
    }
    
    func noteOff(d1: UInt8, d2: UInt8, timeInSeconds: Double) {
        for (i, notePlayed) in notesBeingPlayed.enumerated() {
            if notePlayed.midi == Int(d1) && notePlayed.midi >= 12 && notePlayed.initialTime >= 0.0 && notePlayed.finalTime == -1.0 && isComposing {
                let tickIni = (score.dataSource?.getTimeTicks(time: notePlayed.initialTime))!
                let tickFin = (score.dataSource?.getTimeTicks(time: timeInSeconds))!
                
                var tickIniQ  = (score.dataSource?.quantizeInitialTime(tick: tickIni))!
                let durationQ = (score.dataSource?.quantizeDuration(tick: tickFin - tickIniQ))!
                
                if tickIniQ < 0 {
                    tickIniQ = 0
                }
                
                if tickIniQ >= 0 && durationQ > 0 {
                    var channel = 1
                    
                    if Int(d1) < 60 {
                        channel = 3
                    }
                    
                    if let source = score.dataSource {
                        source.addNote(midi: Int(d1), ti: tickIni, tiQ: tickIniQ, dur: tickFin - tickIni, durQ: durationQ, chnl: channel)
                    }
//                    score.addNote(midi: Int(d1), tIniQuantized: tickIniQ, durationQuantized: durationQ, channel: channel)
                }
                
                notesBeingPlayed.remove(at: i)
                break
            }
        }
    }
    
    func playNoteOn(channel:UInt32, noteNum:UInt32, velocity:UInt32)    {
        let noteCommand = UInt32(0x90 | channel)
        MusicDeviceMIDIEvent(self.samplerUnit!, noteCommand, noteNum, velocity, 0)
    }
    
    func playNoteOff(channel:UInt32, noteNum:UInt32)    {
        let noteCommand = UInt32(0x80 | channel)
        MusicDeviceMIDIEvent(self.samplerUnit!, noteCommand, noteNum, 0, 0)
    }
    
    var amostrasWavTeste: [Float] = []
    
//    func readTxtFile() {
//        amostrasWavTeste.removeAll()
//        if let path = Bundle.main.path(forResource: "amostras", ofType: "txt") {
//            do {
//                let data = try String(contentsOfFile: path, encoding: .utf8)
//                let strs = data.components(separatedBy: ", ")
//
//                for str in strs {
//                    amostrasWavTeste.append(Float(str)!)
//                }
//            } catch {
//                print(error)
//            }
//        }
//    }
//
    func updatePages() {
        
//        readTxtFile()
//
//        var todasAmostras = amostrasWavTeste
//
//        var index = 0
//
//        while todasAmostras.count >= 35280 {
//            var amostrasProc: [Float] = []
//
//
//
//            for _ in 0..<35280 {
//                amostrasProc.append(todasAmostras[0])
//                todasAmostras.remove(at: 0)
//            }
//
//            processa(amostrasProc, index: index)
//
//            index += 1
//        }
        
        let x = (view.frame.height * 0.08) * 0.3
        let y = view.frame.height * 0.08
        let width  = view.frame.width - (2.0 * x)
        let height = view.frame.height - y
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        let parameters = DrawParameter(width: rect.width, height: rect.height)
        
        dataSource = nil
        dataSource = self
        
        if let source = self.score.dataSource {
            source.generatePauses()
        }
        
        score.state = .composing
        score.verifyState()
        score.createPages(parameters: parameters)
        
        controllers.removeAll()
        
        for i in 0..<score.pages.count {
            let controllerPage   = PageScoreViewController()
            controllerPage.page  = score.pages[i]
            let gesture = UITapGestureRecognizer(target: self, action: #selector(showScoreFull(sender:)))
            gesture.numberOfTapsRequired = 2
            controllerPage.view.addGestureRecognizer(gesture)
            controllers.append(controllerPage)
        }
        
        setViewControllers([controllers.first!], direction: .forward, animated: false, completion: nil)
        self.score.generatePositionsCursor()
    }
    
    func processa(_ samples: [Float], index: Int) {
        let processor = NotesProcessor()
        
        processor.metronome = 90
        
        let analyser  = NoteAnalyser(Fs: 8820)
        let filters = analyser.getFilters(inputArray: samples)
        
        var notas: [[Int]] = []
        
        for filter in filters {
            var normalizedSignal = processor.filter(input: samples, filtro: filter.sample)
            
            notas.append(contentsOf: processor.extractNotes(input: &normalizedSignal, filtro: filter.sample))
            
        }
        
        for note in notas {
            let tone  = note[0] + 24
            
            if index >= 0 {
                var initialTime = (3840 * index)
                
                initialTime += note[1]
                let duration = note[2]
                
                let note = ModelNote()
                note.tone              = tone
                note.initialTime       = initialTime
                note.tIniQuantized     = (self.score.dataSource?.quantizeInitialTime(tick: initialTime))!
                note.duration          = duration
                note.durationQuantized = (self.score.dataSource?.quantizeDuration(tick: duration))!
                
                if note.tone >= 60 {
                    note.channel = 1
                } else {
                    note.channel = 3
                }
                
                if let source = self.score.dataSource {
                    var jaAdded = false
                    
                    for noteAdded in source.notes {
                        if note.tone == noteAdded.tone && note.tIniQuantized == noteAdded.tIniQuantized {
                            jaAdded = true
                            
                            break
                        }
                    }
                    
                    if !jaAdded {
                        print(note.tone, note.tIniQuantized, note.durationQuantized)
                        source.notes.append(note)
                    }
                }
            }
        }
        
        if let source = score.dataSource {
            source.extendNotes(numerator: 4, denominator: 4)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        menu.btnSettings.addTarget(self, action: #selector(actionBtnSettings(sender:)), for: .touchUpInside)
     //   menu.btnRestart.addTarget(self, action: #selector(actionBtnRestart(sender:)), for: .touchUpInside)
        menu.btnResume.addTarget(self, action: #selector(actionBtnResume(sender:)), for: .touchUpInside)
        menu.btnEdit.addTarget(self, action: #selector(actionBtnEdit(sender:)), for: .touchUpInside)
        menu.btnSave.addTarget(self, action: #selector(actionBtnSave(sender:)), for: .touchUpInside)
        menu.btnSend.addTarget(self, action: #selector(actionBtnSend(sender:)), for: .touchDown)
        menu.btnSendLater.addTarget(self, action: #selector(actionBtnSendToServer(sender:)), for: .touchDown)
        menu.btnRec.addTarget(self, action: #selector(actionBtnRestart(sender:)), for: .touchUpInside)
        menu.btnExit.addTarget(self, action: #selector(actionBtnExit(sender:)), for: .touchUpInside)
        menu.btnCursorBegin.addTarget(self, action: #selector(actionBtnCursorBegin(sender:)), for: .touchUpInside)
        
        popUpAlert.btnConfirm.addTarget(self, action: #selector(actionBtnConfirm(sender:)), for: .touchDown)
        popUpAlert.btnCancel.addTarget(self, action: #selector(actionBtnCancelPopUpAlert(sender:)), for: .touchDown)
        
    
        popUpCompose.btnPlusTenMetronome.addTarget(self, action: #selector(actionBtnPlusTenMetronome(sender:)), for: .touchUpInside)
        popUpCompose.btnMinusTenMetronome.addTarget(self, action: #selector(actionBtnMinusTenMetronome(sender:)), for: .touchUpInside)
        popUpCompose.btnNextTimeSignature.addTarget(self, action: #selector(actionBtnNextTimeSignature(sender:)), for: .touchUpInside)
        popUpCompose.btnPreviousTimeSignature.addTarget(self, action: #selector(actionBtnPreviousTimeSignature(sender:)), for: .touchUpInside)
        popUpCompose.btnMinusOctaveTranspose.addTarget(self, action: #selector(actionBtnMinusOctave(sender:)), for: .touchUpInside)
        popUpCompose.btnplusOctaveTranspose.addTarget(self, action: #selector(actionBtnPlusOctave(sender:)), for: .touchUpInside)
        popUpCompose.swtMinimizeCursor.addTarget(self, action: #selector(changeCursorSize(_sender:)), for: .valueChanged)
        popUpCompose.btnDone.addTarget(self, action: #selector(actionBtnDone(sender:)), for: .touchUpInside)
        footer.btnHelp.addTarget(self, action: #selector(actionBtnHelp(sender:)), for: .touchUpInside)
        helpView.btnBack.addTarget(self, action: #selector(actionBtnBackHelp(sender:)), for: .touchUpInside)
        
        
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.verifyIfMidiIsConnected), userInfo:nil, repeats:true)
        DispatchQueue.main.async(execute: {
            self.popUpCompose.setInitialTonality(tom: (self.score.dataSource?.configurations[0].key)!)
            self.popUpCompose.setMetronome(metronome: (self.score.dataSource?.metronome)!)
            self.popUpCompose.setInitialQuantization(quantization: (self.score.dataSource?.quantization)!)
        })
        
        if (defaults.integer(forKey: "compose") == 1){
            menu.btnSend.isEnabled = false
            menu.btnSendLater.isEnabled = false
            menu.lblSend.isEnabled = false
            menu.lblSendLater.isEnabled = false
        }else {
            menu.btnSend.isEnabled = true
            menu.btnSendLater.isEnabled = true
            menu.lblSend.isEnabled = true
            menu.lblSendLater.isEnabled = true
        }
        
        self.view.bringSubview(toFront: popUpCompose)
        self.view.bringSubview(toFront: footer)
        self.view.bringSubview(toFront: helpView)
        self.score.viewCursor = self.viewCursor
        self.score.positionCursor()
        self.score.showCursor()
        self.score.dataSource?.instrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory
        self.score.dataSource?.typeInstrument = DAOTypeInstrument.getInstance().getTypeInstrumentActive()
        footer.btnLblInstrument.setTitle(DAOTypeInstrument.getInstance().getTypeInstrumentActive().name, for: .normal)
        
        if controllers.count > 0 {
            self.viewCursor.visaoPartitura = controllers[0].view
        }
    }
    
    @objc private func changeCursorSize(_sender: UISwitch) {
        if _sender.isOn {
            viewCursor.isMinimized = true
        } else {
            viewCursor.isMinimized = false
        }
        self.viewCursor.updateLayout()
    }
    @objc private func verifyIfMidiIsConnected() {
        let sourceCount = MIDIGetNumberOfSources()
        
        if sourceCount > 1 {
            footer.setMidiIsConnected(connected: true)
        } else {
            footer.setMidiIsConnected(connected: false)
        }
    }
    
    func getMidiIsConnected() -> Bool {
        let sourceCount = MIDIGetNumberOfSources()
        
        if sourceCount > 1 {
            return true
        }
        
        return false
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        updateMenu()
        
        if fromInterfaceOrientation.isPortrait {
            menu.direction = .horizontal
        } else {
            menu.direction = .vertical
        }
        
    }
    
    func updateMenu() {
        
        if let customView = Bundle.main.loadNibNamed("HelpViewX", owner: self, options: nil)?.first as? HelpView {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.helpCompose()
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            helpView = customView
        }
        
        if menu == nil {
            let m = MenuViewCompose(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            menu = m
        } else {
            menu.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if leftBar == nil {
            let l = MenuBarView(frame: CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) ))
            l.backgroundColor = UIColor(red: 154 / 255, green: 179 / 255, blue: 205 / 255, alpha: 1.0)
            view.addSubview(l)
            leftBar = l
        } else {
            leftBar.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) )
        }
        
        if rightBar == nil {
            let r = MenuBarView(frame:  CGRect(x: view.frame.width - ((view.frame.height * 0.08) * 0.3), y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) ))
            r.backgroundColor = UIColor(red: 154 / 255, green: 179 / 255, blue: 205 / 255, alpha: 1.0)
            view.addSubview(r)
            rightBar = r
        } else {
            rightBar.frame =  CGRect(x: view.frame.width - ((view.frame.height * 0.08) * 0.3), y: view.frame.height * 0.08, width: (view.frame.height * 0.08) * 0.3, height: view.frame.height - (view.frame.height * 0.08) )
        }
        
        if popUpCompose == nil {
            let r = PopUpCompose(frame:  CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.92)))
            r.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(r)
            popUpCompose = r
        } else {
            popUpCompose.frame = CGRect(x: 0, y: view.frame.height * 0.08, width: (view.frame.width * 0.33), height: (view.frame.height * 0.92))
        }
        
        if footer == nil {
            let height = view.frame.height * 0.033
            let m = Footer(frame: CGRect(x: 0.0, y: view.frame.height - height, width: view.frame.width, height: height))
            m.backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 1.0)
            view.addSubview(m)
            footer = m
        } else {
            footer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height * 0.08)
        }
        
        if let customView = Bundle.main.loadNibNamed("PopUpAlert", owner: self, options: nil)?.first as? PopUpAlert {
            customView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(customView)
            customView.isHidden = true
            customView.translatesAutoresizingMaskIntoConstraints = false
            popUpAlert = customView
        }
        
        popUpCompose.isHidden = true
        
        popUpCompose.delegate = self
    }
    
    @objc func showScoreFull(sender: UITapGestureRecognizer) {
        
        if !isFull {
            applyCrescentAnimation(time: TimeInterval(0.3))
//            menu.disableButtons()
        } else {
            applyDecrescentAnimation(time: TimeInterval(0.3))
//            menu.enableButtonsByState()
            
            if isPlaying {
                isPlaying = false
                score.pause(tapped: true)
                closeConnectionMIDIDevice()
                applyDecrescentAnimation()
                function = .none
            }
        }
        
    }
    
    func applyCrescentAnimation(time: TimeInterval = 0.7) {
        isFull = true
        
        if !score.isPlayCompose {
            recorderAudio.startRecording()
            recorderAudio.stopRecording()
        }
        
        UIView.animate(withDuration: time, animations: {
            self.menu.alpha = 0.0
            self.leftBar.alpha = 0.0
            self.rightBar.alpha = 0.0
            self.footer.alpha = 0.0
            self.openConnectionMIDIDevice()
            
        }, completion: { (value: Bool) in
            if self.function == .start {
                self.score.isSoundCursor      = self.popUpCompose.swtSoundCursor.isOn
                self.score.isMinimizeCursor   = self.popUpCompose.swtMinimizeCursor.isOn
                self.score.stop()
//                self.score.currentIndex1 = 1
                self.score.play(viewCursor: self.viewCursor, soundOn: true)
                self.isPlaying = true
            }
            if self.function == .resume {
                self.score.isSoundCursor      = self.popUpCompose.swtSoundCursor.isOn
                self.score.isMinimizeCursor   = self.popUpCompose.swtMinimizeCursor.isOn
                self.score.pause()
//                self.score.currentIndex1 = 1
                self.score.play(viewCursor: self.viewCursor, soundOn: true)
                self.isPlaying = true
                
            }
        })
        
    }
    
    func applyDecrescentAnimation(time: TimeInterval = 0.7) {
        isFull = false
        
        UIView.animate(withDuration: time, animations: {
            self.menu.alpha = 1.0
            self.leftBar.alpha = 1.0
            self.rightBar.alpha = 1.0
            self.footer.alpha = 1.0
        }, completion: { (value: Bool) in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is MenuViewCompose || subView is HelpView || subView is ViewCursor || subView is MenuBarView || subView is PopUpCompose || subView is Footer {
                self.view.bringSubview(toFront: subView)
            }
        }
        
        if viewCursor.superview == nil {
            view.addSubview(self.viewCursor)
        }
        
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var viewCursor = ViewCursor()
    var isPlaying  = false
    
    @objc func actionBtnCursorBegin(sender: UIButton) {
        
        let alert = UIAlertController(title: "Alert", message: "All bar will be erased. Do you want to continue?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(action) in
            if let source = self.score.dataSource {
                source.notes.removeAll()
            }
            
            self.updatePages()
            
            self.viewCursor.compassoCursor.bar = 0
            self.score.lastInitialTick = 0
            self.score.delegate?.changePage(0)
            self.score.changeTStartCursor(0)
            
            self.popUpCompose.isHidden = true
            self.viewCursor.removeFromSuperview()
            self.addCursor()
            self.function = .resume
            self.score.isPlayCompose = true
            self.score.isPerformance = false
            self.score.viewCursor.isHidden = false
            self.score.delegate?.changePage(self.score.getPosCursor().pagina - 1)
            self.score.isPlayCompose = false
            self.applyCrescentAnimation()
            
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            
        }))
        
        self.present(alert, animated: true, completion:nil)
        
//        if let source = score.dataSource {
//            source.playedNotes.removeAll()
//            source.notes.removeAll()
//        }
    }
    
    @objc func actionBtnDone(sender: UIButton) {
        menu.enableButtons()
        self.viewCursor.isHidden = false
    }
    @objc func actionBtnPlusTenMetronome(sender: UIButton) {
        if((self.score.dataSource?.metronome)! <= 290){
            self.popUpCompose.setMetronome(metronome: 10+(self.score.dataSource?.metronome)!)
        }
    }
    @objc func actionBtnMinusTenMetronome(sender: UIButton) {
        if((self.score.dataSource?.metronome)! >= 40){
            self.popUpCompose.setMetronome(metronome: (self.score.dataSource?.metronome)! - 10)
        }
    }
    
    @objc func actionBtnPlusOctave(sender: UIButton) {
        if let source = score.dataSource {
            if source.plusOctaveTonality() {
                updatePages()
            }
        }
    }
    @objc func actionBtnMinusOctave(sender: UIButton) {
        if let source = score.dataSource {
            if source.minusOctaveTonality() {
                updatePages()
            }
        }
    }
    
    @objc func actionBtnNextTimeSignature(sender: UIButton){
        popUpCompose.nextTimeSignature()
    }
    @objc func actionBtnPreviousTimeSignature(sender: UIButton){
        popUpCompose.previousTimeSignature()
    }
    @objc func actionBtnSettings(sender: UIButton) {
     self.viewCursor.isHidden = !self.viewCursor.isHidden
        if popUpCompose.isHidden {
            popUpCompose.isHidden = false
            popUpCompose.alpha = 0.0
            menu.disableButtons()
            UIView.animate(withDuration: 0.3, animations: {
                self.popUpCompose.alpha = 1.0
            })
        } else {
            popUpCompose.isHidden = true
             menu.enableButtons()
        }
        
        menu.btnSettings.isEnabled = true
        
    }
    
    @objc func actionBtnRestart(sender: UIButton) {
        updatePages()
        popUpCompose.isHidden = true
        viewCursor.removeFromSuperview()
        addCursor()
        function = .start
        score.isPlayCompose = false
        score.isPerformance = false
        self.score.viewCursor.isHidden = false
        self.score.delegate?.changePage(self.score.getPosCursor().pagina - 1)
        applyCrescentAnimation()
    }
    
    @objc func actionBtnResume(sender: UIButton) {
      
        popUpCompose.isHidden = true
        viewCursor.removeFromSuperview()
        addCursor()
        function = .resume
        score.isPlayCompose = true
        score.isPerformance = false
        self.score.viewCursor.isHidden = false
        self.score.delegate?.changePage(self.score.getPosCursor().pagina - 1)
        applyCrescentAnimation()
    }
    
    func addCursor() {
        if viewCursor.superview == nil {
            view.addSubview(viewCursor)
        }
    }
    
    @objc func actionBtnEdit(sender: UIButton) {

        let controller = EditViewController()
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.score = self.score
      
        controller.countBarsScore = self.score.bars.count
      //  controller.titleFooter = self.titleFooter
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func actionBtnHelp(sender: UIButton) {
     
        helpView.isHidden = false
        
        
    }
    
    @objc func actionBtnBackHelp (sender: UIButton){
        
        helpView.isHidden = true
        
    }
    
    func service(urlToRequest: String, method: String, token:String, idMidi: String) {
      
        var request = URLRequest(url:URL(string:urlToRequest)!)
        let postString = ["token":token, "id": idMidi] //.md5()
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        print(postString)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){ data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            
            }.resume()
        
    }
    func serviceSendLater(urlToRequest: String, method: String) {
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        //request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                print("\(String(describing: err))")
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{//
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
             //   if let data1 = parseResult["results"] as? [String:AnyObject] {
                print("data: ", parseResult)
               // }
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            
            }.resume()
        
    }
    @objc func actionBtnSave(sender: UIButton) {
      
        
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        
        if activity.superview == nil {
            view.addSubview(activity)
        }
        
        DispatchQueue.global(qos: .userInteractive).async {
            var message = ""
            if let source = self.score.dataSource {
                
                if source.inserted {
                    print("tipo update: ", source.type)
                    message = "Score updated"
                    source.inserted = true
                    DAOScore.getInstance().updateScore(source)
                } else {
                
                    message = "Score saved"
                    source.inserted = true
                    source.user = DAOUser.getInstance().getUserActive()!
                    DAOScore.getInstance().addScore(source)
                    let user = UserManager.sharedUserManager
                    let token = user.token
                  
                    self.service(urlToRequest: user.server + ":3001/operator/status", method: ServiceRemote.Method.POST.rawValue, token: token, idMidi: String(source.idMidi))
                    
                }
            }
            
            DispatchQueue.main.async(execute: {
                self.activity.removeFromSuperview()
                let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion:nil)
            })
        }
        
    }
    @objc func actionBtnSendToServer(sender: UIButton) {
        
        
        if let source = self.score.dataSource {
            
            let user = UserManager.sharedUserManager
            let token = user.token
            user.server = "http://37.208.116.28"
            self.serviceSendLater(urlToRequest: user.server + ":3001/operator/midi/later?token=\(token)&id=\(source.idMidi)", method: ServiceRemote.Method.GET.rawValue)
            
            DAOScore.getInstance().deleteScore(source)
        }
        let alert = UIAlertController(title: "", message: "MIDI sended to server", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            if let contr = self.parentControl {
                var email = ""
                
                if let user = DAOUser.getInstance().getUserActive() {
                    email = user.email
                }
                contr.items.removeAllObjects()
                contr.scores.removeAll()
                contr.menu.function = .myComposition
                contr.menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
                contr.menu.btnCompose.setImage(#imageLiteral(resourceName: "impost_select"), for:.normal)
                contr.menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
                contr.menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
                contr.menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
                contr.scores = DAOScore.getInstance().getScores(self.defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
                
                
                
                for score in contr.scores {
                    contr.items.add(score.title)
                }
                
                //            contr.items.insert("Compose", at: 0)
                
                contr.collectionView.reloadSections(IndexSet(0...0))
                
            }
            alert.dismiss(animated: true, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion:nil)
        
    }
    @objc func actionBtnSend(sender: UIButton) {
        let user = UserManager.sharedUserManager
        let token = user.token
        
        user.server = "http://37.208.116.28"
        score.dataSource?.status = 1
       //
       
        if ((score.dataSource?.author.count)! > 0 && score.dataSource?.author[0].firstName != "" && score.dataSource?.createdyear != "" && score.dataSource?.genre.name != "" && score.dataSource?.title != ""){
        serviceSend(urlToRequest: user.server + ":3003/operator/score/library", method: ServiceRemote.Method.POST.rawValue, token: token)
        
        if let source = self.score.dataSource {
            DAOScore.getInstance().deleteScore(source)
        }
        let alert = UIAlertController(title: "", message: "Score sended!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            if let contr = self.parentControl {
                var email = ""
                
                if let user = DAOUser.getInstance().getUserActive() {
                    email = user.email
                }
                contr.items.removeAllObjects()
                contr.scores.removeAll()
                contr.menu.function = .myComposition
                contr.menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
                contr.menu.btnCompose.setImage(#imageLiteral(resourceName: "impost_select"), for:.normal)
                contr.menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
                contr.menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
                contr.menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
                contr.scores = DAOScore.getInstance().getScores(self.defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
                for score in contr.scores {
                    contr.items.add(score.title)
                }
                contr.collectionView.reloadSections(IndexSet(0...0))
                
            }
            alert.dismiss(animated: true, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion:nil)
        } else {
            let alert = UIAlertController(title: "Additional Info ERROR", message: "Fill all the required fields (SUBTITLE, ARTIST and TAGS is not required) on the Additional screen", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion:nil)
        }
    }
    
    func serviceSend(urlToRequest: String, method: String, token:String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let result = formatter.string(from: date)
        
        
        var stringScore      : [String : Any] = [:]
        var stringRepetition : [Dictionary<String, Any>] = []
        var stringDsDc       : [Dictionary<String, Any>] = []
        var stringConfig     : [Dictionary<String, Any>] = []
        var stringNote       : [Dictionary<String, Any>] = []
        var stringAuthor     : [Dictionary<String, Any>] = []
        var stringTag        : [Dictionary<String, Any>] = []
       
            
        if let source = score.dataSource {
         
            var dictRepAuthor = Dictionary<String, Any>()
            dictRepAuthor["idUser"]      = source.author[0].idUser
            dictRepAuthor["type"]       = 1
            stringAuthor.append(dictRepAuthor)
           
            if(source.artist.count>0){
                var dictRepArtist = Dictionary<String, Any>()
                dictRepArtist["idUser"]      = source.artist[0].idUser
                dictRepArtist["type"]       = 3
                stringAuthor.append(dictRepArtist)
            }
            
            
            for tag in source.tags {
                var dictTag = Dictionary<String, Any>()
                dictTag["name"]       = tag.name
                stringTag.append(dictTag)
            }
            
            for repet in source.repeats {
                var dictRep = Dictionary<String, Any>()
                dictRep["initialBar"]       = repet.initialBar
                dictRep["finalBar"]         = repet.finalBar
                dictRep["bracket"] = repet.voltaBrackets
                stringRepetition.append(dictRep)
            }
            
            
            for dsdc in source.dsDcs {
                var dictRep = Dictionary<String, Any>()
                dictRep["initialBar"]  = dsdc.initialBar
                dictRep["finalBar"]    = dsdc.finalBar
                dictRep["toCoda"]      = dsdc.toCoda
                dictRep["coda"]        = dsdc.coda
                dictRep["fine"]         = dsdc.fine
                dictRep["type"]        = dsdc.type
                dictRep["dcAlCoda"]        = dsdc.dcAlCoda
                dictRep["dsAlCoda"]        = dsdc.dsAlCoda
                dictRep["dcAlFine"]        = dsdc.dcAlFine
                dictRep["dsAlFine"]        = dsdc.dsAlFine
                
                stringDsDc.append(dictRep)
            }
            
            for config in source.configurations {
                var dictRep = Dictionary<String, Any>()
                dictRep["bar"]             = config.bar
                dictRep["timeDenominator"] = config.denominator
                dictRep["timeNumerator"]   = config.numerator
                dictRep["metronome"]       = config.metronome
                dictRep["keySignatureValue"]    = config.key
                dictRep["keySignatureType"]     = config.keyType
                stringConfig.append(dictRep)
            }
            
            for note in source.notes {
                var dictRep = Dictionary<String, Any>()
                dictRep["tone"]                   = note.tone
                dictRep["channel"]                = note.channel
                dictRep["initialTimeTicks"]        = note.tIniQuantized
                dictRep["durationTicks"]          = note.durationQuantized
                dictRep["intensity"]              = note.intensity
                stringNote.append(dictRep)
            }
            
            stringScore = ["title" : source.title,"status": source.status, "authors" : stringAuthor, "creationDate" : source.createdyear, "trendingYear" : result, "lastEditionDate" : result, "version" : "1", "baseNote" : source.baseNote, "quantization" : source.quantization, "stoppedBar" : "1", "difficulty" : source.difficulty, "idParentScore" : source.idMidi, "publisher" : source.copyright.id, "repetitions" : stringRepetition, "daCaposDelSegnos" : stringDsDc, "barSettings" : stringConfig, "notes" : stringNote, "polyphonic": source.isPoliphonic, "tags": stringTag]
            
            if(source.subtitle != ""){
                stringScore.updateValue(source.subtitle, forKey: "subtitle")
            }
            if (source.subgenre.id != ""){
                stringScore.updateValue(source.subgenre.id, forKey: "idSubGenre")
            }
            
            let postString : [String : Any] = ["score": stringScore, "token":token ]
            
            var request = URLRequest(url:URL(string:urlToRequest)!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
            request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
            let session = URLSession.shared
            //Post
            session.dataTask(with: request){data, response, err in
                //Guard: ws there error ?
                guard(err == nil) else {
                    print("\(String(describing: err))")
                    return
                }
                //Guard: check was any data returned?
                guard let data = data else{
                    print("no data return")
                    return
                }
                
                let parseResult: [String:AnyObject]!
                do{//
                    parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                  
                    if let data = parseResult["results"] as? [String:AnyObject] {
                        print("aqui: ", data)
              
                        //                    let b64 = data["errors"] as! String
                        //                    print(b64)
                    }
                    
                    if let data = parseResult["status"] as? Bool {
                        if data {
                            
                        }
                        
                      
                    }
                } catch {
                    print("Could not parse data as Json \(data)")
                    return
                }
                
                }.resume()
        }
        
    }
    
    let defaults = UserDefaults.standard
    
    
    
    @objc func actionBtnExit(sender: UIButton) {
        if historyScores.count > 0 {
            popUpAlert.alertSaveCompose()
            popUpAlert.isHidden = false
            view.bringSubview(toFront: popUpAlert)
            popUpAlert.acao = 3
        }
        else{
            self.exitCompose()
        }
    }
    
    @objc func actionBtnCancelPopUpAlert(sender: UIButton) {
        popUpAlert.isHidden = true
    }
    
    @objc func actionBtnConfirm(sender: UIButton) {
        if popUpAlert.acao == 3 {
            popUpAlert.isHidden = true
            self.exitCompose()
        }
    }
    
    private func exitCompose (){
        
        if let contr = parentControl {
            contr.items.removeAllObjects()
            contr.scores.removeAll()
            var email = ""
            if let user = DAOUser.getInstance().getUserActive() {
                email = user.email
            }
        contr.popup.isMyCommpositions()
        contr.storeView.isHidden = true
        contr.collectionViewNewsStore.isHidden = true
        contr.collectionViewAllStore.isHidden = true
        contr.collectionView.isHidden = false
        contr.menu.btnFilter.isEnabled = true
        contr.profileView.isHidden = true
        contr.menu.function = .myComposition
        contr.btnnewcomposition.isHidden = false
        contr.menu.btnMyMusic.setImage(#imageLiteral(resourceName: "my_music"), for: .normal)
        contr.menu.btnCompose.setImage(#imageLiteral(resourceName: "impost_select"), for:.normal)
        contr.menu.btnShop.setImage(#imageLiteral(resourceName: "store"), for: .normal)
        contr.menu.btnPerfil.setImage(#imageLiteral(resourceName: "perfil"), for: .normal)
        contr.menu.btnFilter.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
        contr.scores = DAOScore.getInstance().getScores(defaults.object(forKey: "BuscaSalvaMC") as! String, email: email, busca: "")
        
        for score in contr.scores {
            contr.items.add(score.title)
        }
        
        contr.collectionView.reloadSections(IndexSet(0...0))
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if isPlaying {
            isPlaying = false
            score.pause(tapped: true)
            applyDecrescentAnimation()
            function = .none
            closeConnectionMIDIDevice()
        }
        
        if let touch = touches.first {
            if let viewT = touch.view {
                if viewT is ViewBar {
                    let viewBar = viewT as! ViewBar
                    self.score.changeTStartCursor(viewBar.start)
                    print("Entrou bar", viewBar.start)
                }
            }
        }
    }
    
    deinit {
        menu = nil
        leftBar = nil
        rightBar = nil
    }

}

extension ComposeViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if isPlaying || !popUpCompose.isHidden {
            return nil
        }
        
        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex < 1 {
            return nil
        }
        
        print(viewControllerIndex - 1, self.score.getPosCursor().pagina - 1)
        if viewControllerIndex - 1 != self.score.getPosCursor().pagina - 1 {
            self.score.viewCursor.isHidden = true
        } else {
            self.score.viewCursor.isHidden = false
        }
        
        return controllers[viewControllerIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if isPlaying || !popUpCompose.isHidden {
            return nil
        }
        
        guard let viewControllerIndex = controllers.index(of: viewController as! PageScoreViewController) else {
            return nil
        }
        
        if viewControllerIndex > controllers.count - 2 {
            return nil
        }
        
        print(viewControllerIndex + 1, self.score.getPosCursor().pagina - 1)
        if viewControllerIndex + 1 != self.score.getPosCursor().pagina - 1 {
            self.score.viewCursor.isHidden = true
        } else {
            self.score.viewCursor.isHidden = false
        }
        
        return controllers[viewControllerIndex + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    
    }

}

extension ComposeViewController: PopUpComposeDelegate {
    func changeQuantization(_ quantization : Int){
        if let source = score.dataSource {
            source.quantization = quantization
            for note in source.notes {
                note.tIniQuantized = source.quantizeInitialTime(tick: note.initialTime)
            }
            source.extendNotes(numerator: source.configurations[0].numerator, denominator: source.configurations[0].denominator)
            
            
        }
        updatePages()
    }
    
    func changeKey(_ key: KeyType) {
        if let source = score.dataSource {
            source.changeTonality(newTonality: key)
            //            updatePages()
        }
    }
    
    func changeTitle(_ title: String) {
        if let source = score.dataSource {
            source.title = title
        }
        score.changeTitle()
    }
    
    func changeTimeSignature(_ option: TimeSelected) {
        var numerator   = 4
        var denominator = 4
        switch option {
        case .doisdois:
            numerator   = 2
            denominator = 2
        case .tresdois:
            numerator   = 3
            denominator = 2
        case .doisQuartos:
            numerator   = 2
            denominator = 4
        case .tresQuartos:
            numerator   = 3
            denominator = 4
        case .quatroQuartos:
            numerator   = 4
            denominator = 4
        case .cincoQuartos:
            numerator   = 5
            denominator = 4
        case .seisQuartos:
            numerator   = 6
            denominator = 4
        case .tresOitavos:
            numerator   = 3
            denominator = 8
        case .seisOitavos:
            numerator   = 6
            denominator = 8
        case .noveOitavos:
            numerator   = 9
            denominator = 8
        case .dozeOitavos:
            numerator   = 12
            denominator = 8
        }
        
    
        if let source = score.dataSource {
            var found = false
            for i in 0..<source.configurations.count {
                if source.configurations[i].bar == 1  {
                    source.configurations[i].numerator = numerator
                    source.configurations[i].denominator = denominator
                    found = true
                }
            }
            
            if !found {
                let config = ModelBarConfiguration()
                config.numerator = numerator
                config.denominator = denominator
                source.configurations.append(config)
            }
            
            score.state = .composing
            updatePages()
        }
    }
    
    func changeMetronome(_ value: Int) {
        if let source = score.dataSource {
            source.metronome = value
        }
        
        score.changeMetronome()
    }
    func transpose(_ option: OptionKeySelected) {
        changeKey(newKey: option)
    }
    
    func changeKey(newKey: OptionKeySelected) {

        var keySelect = KeyType.C_Am
        
        switch newKey {
        case .F:
            keySelect = .F_Dm
        case .Bb:
            keySelect = .Bb_Gm
        case .Eb:
            keySelect = .Eb_Cm
        case .Ab:
            keySelect = .Ab_Fm
        case .Db:
            keySelect = .Db_BBm
        case .Gb:
            keySelect = .Gb_Ebm
        case .G:
            keySelect = .G_Em
        case .D:
            keySelect = .D_Bm
        case .A:
            keySelect = .A_Fsm
        case .E:
            keySelect = .E_Csm
        case .B:
            keySelect = .B_Gsm
        case .Fs:
            keySelect = .Fs_Dsm
        case .Cs:
            keySelect = .Cs_Asm
        case .C:
            keySelect = .C_Am
        case .Cb:
            keySelect = .C_Am
        }
        
        if canUpdate {
            if let source = score.dataSource {
                source.changeTonality(newTonality: keySelect)
                
                updatePages()
                
            }
        }
        else {
            canUpdate = true
        }
    }
}

extension ComposeViewController: DrawScoreDelegate {
    
    func scoreStart() {
        timeTeste = mach_absolute_time()
        score.timeTeste = timeTeste
        
        if getMidiIsConnected() {
            score.isMidi = true
            self.initialTimeStamp = MIDITimeStamp(mach_absolute_time())
            openPorts()
        } else {
            self.recorderAudio.sampleRate = 44100.0

            let time     = 60.0 / Float((self.score.dataSource?.metronome)!)
            let baseNote = 4.0 / Float(((self.score.startTimeBar.first)!.denominator))
            let bar      = Float(self.score.startTimeBar.first!.numerator) * baseNote

            let numberOfSamples = Int(time * bar * Float(SAMPLE_RATE))

            self.recorderAudio.bufferSize = numberOfSamples
            if !score.isPlayCompose {
                self.recorderAudio.startRecording()
            }
            self.score.isMidi = false
        }
        
        isComposing = true
    }
    
    func scorePaused() {
//        writeFile("samples8820.txt", name44100: "samples44100.txt", text8820: self.strSamp8820, text44100: self.strSamp44100)
        let lblPaused = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        lblPaused.text = "Paused"
        
        lblPaused.numberOfLines = 0
        lblPaused.font = UIFont(name: "Lato-Bold", size: self.view.bounds.size.height * 0.08)
        lblPaused.textColor = UIColor(red: 140/255, green: 28/255, blue: 37/255, alpha: 1.0)
        lblPaused.textAlignment = NSTextAlignment.center
        lblPaused.backgroundColor = UIColor.clear
        
        view.addSubview(lblPaused)
        
        UIView.animate(withDuration: 2.0, animations: {
            lblPaused.alpha = 0.0
        }, completion: { (value: Bool) in
            lblPaused.removeFromSuperview()
        })
        
        if !score.isPlayCompose {
            recorderAudio.stopRecording()
        }
        
        isComposing = false

        nextIndex = 0
    }
    
    func scoreStopped() {
//        writeFile("samples8820.txt", name44100: "samples44100.txt", text8820: self.strSamp8820, text44100: self.strSamp44100)
        let lblStopped = LabelNoBorder(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        lblStopped.text = "Complete"
        
        lblStopped.numberOfLines = 0
        lblStopped.font = UIFont(name: "Lato-Bold", size: self.view.bounds.size.height * 0.08)
        lblStopped.textColor = UIColor(red: 140/255, green: 28/255, blue: 37/255, alpha: 1.0)
        lblStopped.textAlignment = NSTextAlignment.center
        lblStopped.backgroundColor = UIColor.clear
        
        view.addSubview(lblStopped)
        
        applyDecrescentAnimation(time: 1.5)
        
        UIView.animate(withDuration: 2.0, animations: {
            lblStopped.alpha = 0.0
        }, completion: { (value: Bool) in
            lblStopped.removeFromSuperview()
        })
        
        if !score.isPlayCompose {
            recorderAudio.stopRecording()
        }
        
        isComposing = false
        isPlaying   = false
        function    = .none
 
        nextIndex = 0
    }
    
    func changePage(_ newPage: Int) {
        setViewControllers([controllers[newPage]], direction: .forward, animated: false, completion: nil)
        viewCursor.removeFromSuperview()
        addCursor()
    }

}

extension ComposeViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ComposeViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

struct TimerDarwin {
    var startTime: UInt64 = 0
    var stopTime: UInt64  = 0
    let numer: UInt64
    let denom: UInt64
    
    init() {
        var info = mach_timebase_info(numer: 0, denom: 0)
        mach_timebase_info(&info)
        numer = UInt64(info.numer)
        denom = UInt64(info.denom)
    }
    
    mutating func start() {
        startTime = mach_absolute_time()
    }
    
    mutating func stop() {
        stopTime = mach_absolute_time()
    }
    
    var nanoseconds: UInt64 {
        return ((stopTime - startTime) * numer) / denom
    }
    
    var milliseconds: Double {
        return Double(nanoseconds) / 1_000_000
    }
    
    var seconds: Double {
        return Double(nanoseconds) / 1_000_000_000
    }
}

extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}

extension ComposeViewController: RecordAudioDelegate {
    func ticksQuant(_ quant: Int) -> Int {
        switch quant {
        case 4:
            return 960
        case 8:
            return 480
        case 16:
            return 240
        default:
            return 240
        }
    }
    
    func bufferFull(_ samples: [Float], index: Int) {
        var tempSamp: [Float] = []
     
        DispatchQueue.global(qos: .background).async {
            print("Entrou Engine")
            for i in 0..<samples.count {
                if i % 5 == 0 {
                    tempSamp.append(samples[i])
                }
            }

//            for sample in tempSamp {
//                print(sample)
//            }
            
            
            let v1 = 60.0 / Double(self.score.dataSource!.metronome)
            let vlQuantTicks = Double(self.ticksQuant(self.score.dataSource!.quantization)) / 960.0
            let v2 = Int(round(v1 * vlQuantTicks * 8820))
            
            
            
//            for i in 0..<samples.count {
//                self.strSamp44100 += "\(samples[i])\n"
//            }

//            for i in 0..<tempSamp.count {
//                self.strSamp8820 += "\(tempSamp[i])\n"
//            }

//            var notes: [RawNote] = []
            
//            notes = self.analyser.getNotes(signal: tempSamp, poli: false)
            
            self.processor.metronome = self.score.dataSource!.metronome
            
//            print("Taxa de Amostragem 44100")
//            let analyser  = NoteAnalyser(Fs: 44100)
//            let filters = analyser.getFilters(inputArray: samples)
            
//            for peak in filters {
//                print(peak.sample, peak.amplitude)
//            }
            
//            print("Taxa de Amostragem 8820")
//            let analyser  = NoteAnalyser(Fs: 8820)
//            let filters = analyser.getFilters(inputArray: tempSamp)
            
//            for peak in filters1 {
//                print(peak.sample, peak.amplitude)
//            }
            
            let filters = self.analyser1.frequencies(in: tempSamp, extWindowLenght: v2)
            var notas: [[Int]] = []
            
            for filter in filters {
                var normalizedSignal = self.processor.filter(input: tempSamp, filtro: filter)
                notas.append(contentsOf: self.processor.extractNotes(input: &normalizedSignal, filtro: filter))
            }
            
            
//            let ticksPerSeconds = Double((self.score.dataSource?.metronome)!) * 960.0 / 60.0
            
            print("Saiu Engine")
            
            print("Entrou AddNote")
            for note in notas {
                let tone  = note[0] + 24
//                let start = Double(note.Start())
//                let end   = Double(note.end())
//
//                var initialTime = Int((start / self.SAMPLE_RATE) * ticksPerSeconds)
//                var finalTime   = Int((end / self.SAMPLE_RATE) * ticksPerSeconds)
                
                if index >= 0 {
                    var initialTime = (3840 * index)
                    
//                    print(index, initialTime, finalTime, start, end)
                    
                    initialTime += note[1]
                    let duration = note[2]
                    
                    let note = ModelNote()
                    note.tone              = tone
                    note.initialTime       = initialTime
                    note.tIniQuantized     = (self.score.dataSource?.quantizeInitialTime(tick: initialTime))!
                    note.duration          = duration
                    note.durationQuantized = (self.score.dataSource?.quantizeDuration(tick: duration))!
                    

                    if note.tone < 60 && !self.defaults.bool(forKey: "Mono") {
                        note.channel = 3
                    } else {
                        note.channel = 1
                    }
 
                    if let source = self.score.dataSource {
                        var add = false
                        for i in 0..<source.notes.count {
                            if source.notes[i].tIniQuantized == note.tIniQuantized {
                                source.notes[i].tone = min(source.notes[i].tone, note.tIniQuantized)
                                add = true
                            }
                        }
//                        print(note.tone, "\t", note.initialTime, "\t", note.duration)
                        if !add {
                            source.notes.append(note)
                        }
                    
//                        source.deleteHighsNotes()
                    }
                }
            }
            print("Saiu AddNote")
            
            DispatchQueue.main.async(execute: {
                print("Entrou Desenho")
//                if notes.count > 0 {
                if !self.score.isPlayCompose {
                    self.score.dataSource?.generatePauses()
                    self.score.drawUnwrittenNotes()
                }
                print("Saiu Desenho")
//                }
            })
        }
    }

    func writeFile(_ name8820: String, name44100: String, text8820: String, text44100: String) {
        let test8820 = FileTest()
        test8820.fileName = name8820
        test8820.saveFile(dataToSave: text8820.data(using: .utf8)! as NSData)
        
        let test44100 = FileTest()
        test44100.fileName = name44100
        test44100.saveFile(dataToSave: text44100.data(using: .utf8)! as NSData)
        
        sendMail(test8820.loadFile(), data44100: test44100.loadFile(), name8820: name8820, name44100: name44100)
        
//        let file = "\(name).txt" //this is the file. we will write to and read from it
//
//        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//
//            let fileURL = dir.appendingPathComponent(file)
//
//            print(fileURL.absoluteString)
//            //writing
//            do {
//                try text.write(to: fileURL, atomically: false, encoding: .utf8)
//            }
//            catch {
//                print("Deu erro")
//            }
//        }
    }
}

extension ComposeViewController : MFMailComposeViewControllerDelegate {
    func sendMail(_ data8820: NSData?, data44100: NSData?, name8820: String, name44100: String) {
        if( MFMailComposeViewController.canSendMail()){
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set to recipients
            mailComposer.setToRecipients(["gabriel@ltmdev.com"])
            
            //Set the subject
            mailComposer.setSubject("email with document txt")
            
            //set mail body
            mailComposer.setMessageBody("This is what they sound like.", isHTML: true)

            if let fileData = data8820 {
                mailComposer.addAttachmentData(fileData as Data, mimeType: "text/plain", fileName: "\(name8820)")
            }
            
//            if let fileData = data44100 {
//                mailComposer.addAttachmentData(fileData as Data, mimeType: "text/plain", fileName: "\(name44100).txt")
//            }
            
            //this will compose and present mail to user
            self.present(mailComposer, animated: true, completion: nil)
        }
        else {
            print("email is not supported")
        }
    }
    
    func mailComposeController(_ didFinishWithcontroller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class FileTest {
    let directories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    var fileName = "test.doc"
    
    func saveFile(dataToSave: NSData) {
        guard let directory = directories.first else { return }
        
        let fileSaveLocation = URL(fileURLWithPath: fileName, relativeTo: directory)
        dataToSave.write(to: fileSaveLocation, atomically: true)
        print("*** file saved to path: \(fileSaveLocation)")
    }
    
    func loadFile() -> NSData? {
        guard let directory = directories.first else { return nil }

        let fileLocationForLoad = NSURL(fileURLWithPath: fileName, relativeTo: directory)
        
        print("*** file loading from path: \(fileLocationForLoad)")
        return NSData(contentsOf: fileLocationForLoad as URL)
    }
}


extension ComposeViewController: AudioControllerDelegate {
    
    func processSampleData(_ data: Data) {
        let arr2 = data.withUnsafeBytes {
            Array(UnsafeBufferPointer<Float>(start: $0, count: data.count/MemoryLayout<Float>.size))
        }
//        print(arr2.count)
    }

}

struct NoteMidiPlayed {
    var initialTime : Double = -1.0
    var finalTime   : Double = -1.0
    var midi        : Int    = -1
}

