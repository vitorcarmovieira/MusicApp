//
//  LoginViewController.swift
//  Music Mate
//
//  Created by Ultimo Alves on 15/07/17.
//  Copyright © 2017 Diego Lopes. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {

    @IBOutlet weak var viewLogin: UIView!
    
    @IBOutlet weak var msgIncorrect: UILabel!
    @IBOutlet weak var usernameEmailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnLoginDemo: UIButton!
    
    @IBOutlet weak var bgLogin: UIView!
    
    @IBAction func enterLoginBtnAction(_ sender: Any) {
        if usernameEmailTxt.text == "" || passwordTxt.text == "" {
            makeAnimationOnTxt(field: usernameEmailTxt)
            makeAnimationOnTxt(field: passwordTxt)
            
            msgIncorrect.text     = "Enter the fields"
            msgIncorrect.isHidden = false
        } else {
            view.endEditing(true)
            btnLogin.alpha = 0.5
            loginHttp()
        }
    }
    
    @IBAction func actionBtnLoginDemo(_ sender: UIButton) {
        let controller = HomeViewController()
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        self.present(controller, animated: true, completion: nil)
    }
    
    let activity = LoadingView()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        btnLogin.layer.borderWidth = 1
//        btnLogin.layer.borderColor = UIColor.white.cgColor
//        btnLogin.layer.cornerRadius = 10
        
        bgLogin.layer.cornerRadius = 10
        
        let imgName = UIImageView(image: #imageLiteral(resourceName: "login_mail"))
        if let size = imgName.image?.size {
            imgName.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        let imgPass = UIImageView(image: #imageLiteral(resourceName: "login_password"))
        if let size = imgPass.image?.size {
            imgPass.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        imgName.contentMode = .center
        imgPass.contentMode = .center
        
        usernameEmailTxt.leftViewMode = .always
        passwordTxt.leftViewMode      = .always
        
        usernameEmailTxt.leftView = imgName
        passwordTxt.leftView      = imgPass
        
        if let left = usernameEmailTxt.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: usernameEmailTxt.frame.size.width * 0.1, height: usernameEmailTxt.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        if let left = passwordTxt.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: usernameEmailTxt.frame.size.width * 0.1, height: usernameEmailTxt.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        msgIncorrect.isHidden = true
        
        usernameEmailTxt.keyboardType = .emailAddress
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        
        if activity.superview == nil {
            view.addSubview(activity)
        }
        
        activity.isHidden = true
        UserDefaults.standard.set(0, forKey: "login")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let first = touches.first {
            if let v = first.view {
                if v != usernameEmailTxt && v != passwordTxt {
                    view.endEditing(true)
                }
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin = CGPoint(x: 0, y: -keyboardSize.height / 2.0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin = CGPoint(x: 0, y: 0)
        }
    }
    
    func loginHttp() {
        
        activity.isHidden = false
        bgLogin.isHidden  = true
        
        let user = UserManager.sharedUserManager
        let email : String = self.usernameEmailTxt.text!
        let password = md5(self.passwordTxt.text!)
        
        let data = ["email":email.lowercased(), "password": password] //.md5()
        let postString: [String: Any] = ["user":data]
        //print (postString)
        user.server = "http://37.208.116.28"
        
      
        var request = URLRequest(url:URL(string:user.server+":3002/login")!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            //Guard: ws there error ?
            guard(err == nil) else {
                
                DispatchQueue.main.async{
                    self.activity.isHidden = true
                    self.bgLogin.isHidden  = false
                    
                    self.makeAnimationOnTxt(field: self.usernameEmailTxt)
                    self.makeAnimationOnTxt(field: self.passwordTxt)
                    self.msgIncorrect.text = "Unknown error"
                    self.msgIncorrect.isHidden = false
                    self.btnLogin.alpha = 1.0
                }
                return
            }
            //Guard: check was any data returned?
            guard let data = data else{
                print("no data return")
                return
            }
            //Convert Json to Object
            let parseResult: [String:AnyObject]!
            do{
                parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                //print("\(parseResult)")
                if (parseResult["status"] as! Bool == true) {
                    if let dataUser = parseResult["results"] as? [String:AnyObject] {
                        user.token  = dataUser["token"] as! String
                        let user = UserManager.sharedUserManager
                        let token = user.token
                        user.server = "http://37.208.116.28"
                        
//                        let genrsAddeds = DAOGenre.getInstance().getAllGenrs()
//                        
//                        if genrsAddeds.count == 0 {
//                            self.serviceGetGenders(urlToRequest: user.server + ":3003/genre?token=" + token, method: ServiceRemote.Method.GET.rawValue)
//                        }
//                        
//                        let countriesAddeds = DAOCountry.getInstance().getAllCountries()
//                        
//                        if countriesAddeds.count == 0 {
//                            self.serviceGetCountries(urlToRequest: user.server + ":3003/country?token=" + token, method: ServiceRemote.Method.GET.rawValue)
//                        }
                        
                        if let data = dataUser["user"] as? [String:AnyObject] {
                            //                        user.tipo   = data["type"] as! Int
                          
                            user.status = true
                            user.email  = data["email"] as! String
                            if let image = data["image"] {
                                user.image  = image as! String
                            } else {
                                user.image = ""
                            }
                            
                            let dao            = DAOUser.getInstance()
                            let modelUser      = ModelUser()
                            modelUser.email    = user.email
                            modelUser.firstName     = data["firstName"] as! String
                            modelUser.lastName = data["lastName"] as! String
                            modelUser.token    = user.token
                            modelUser.image = user.image
                            modelUser.interests   = data["_id"] as! String
                            modelUser.firstName          = data["firstName"] as! String
                            modelUser.lastName      = data["lastName"]  as! String
                            
                            if let _ = data["image"]  {
                                modelUser.image      = data["image"]  as! String
                            }
                            if let _ = data["gender"] {
                                modelUser.genre        = (data["gender"]  as! Int) - 1
                            }
//                            if let _ = data["idInstrument"] {
//                                modelUser.instrument.id = data["idInstrument"]  as! String
//                            }
                            if let _ = data["idInstrument"] {
                                modelUser.typeInst.id   = data["idInstrument"]  as! String
                            }
                            if let _ = data["idCountry"] {
                                modelUser.country.id    = data["idCountry"]  as! String
                            }
                            if let _ = data["birthdate"] {
                                modelUser.birthDate      = data["birthdate"]  as! String
                            }
                            
                            // Salva o usuário
                            dao.addUser(modelUser)
                            
                         
                            
                            DispatchQueue.main.async{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller = storyboard.instantiateViewController(withIdentifier: "controllerInstruments")
                                controller.modalTransitionStyle = .crossDissolve
                                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                                self.present(controller, animated: true, completion: nil)
                                
//                                let controller = HomeViewController()
//                                controller.modalTransitionStyle = .crossDissolve
//                                controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
//                                
//                                self.present(controller, animated: true, completion: nil)
                            }
                        }
                    }
                } else {
                    DispatchQueue.main.async{
                        self.activity.isHidden = true
                        self.bgLogin.isHidden  = false
                        
                        self.makeAnimationOnTxt(field: self.usernameEmailTxt)
                        self.makeAnimationOnTxt(field: self.passwordTxt)
                        self.btnLogin.alpha = 1.0
                    }
                }
                
                if (parseResult["status"] as! Bool == false) {
                    if let errorCode = parseResult["errorCode"] {
                        if (errorCode as! Int == 4) {
                            DispatchQueue.main.async{
                                self.activity.isHidden = true
                                self.bgLogin.isHidden  = false
                                
                                self.msgIncorrect.text = "User or password incorrect"
                                self.msgIncorrect.isHidden = false
                                self.btnLogin.alpha = 1.0
                            }
                        }
                    }
                }
 
            } catch {
                print("Could not parse data as Json \(data)")
                return
            }
            //Check jsonDictionary
            guard let jsonArray = parseResult["success"] as? [String:AnyObject] else{
                print("jsonDictionary error")
                return
            }
            //check jsonArray and switch to LoginViewController
            if(jsonArray.count == 0 ){
                print("jsonArray not found")
                return
            } else{
                
            }
            
            }.resume()
        
        
    }
    
    func md5(_ string: String) -> String {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        return hexString
    }
    
    func makeAnimationOnTxt(field: UITextField) {
        UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
            field.center = CGPoint(x: field.center.x + 20, y: field.center.y)
        }, completion: { (finished: Bool) in
            UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                field.center = CGPoint(x: field.center.x - 40, y: field.center.y)
            }, completion: { (finished: Bool) in
                UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                    field.center = CGPoint(x: field.center.x + 40, y: field.center.y)
                }, completion: { (finished: Bool) in
                    UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                        field.center = CGPoint(x: field.center.x - 40, y: field.center.y)
                    }, completion: { (finished: Bool) in
                        UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                            field.center = CGPoint(x: field.center.x + 20, y: field.center.y)
                        }, completion: { (finished: Bool) in
                            
                        })
                    })
                })
            })
        })
    }

    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
//        segue.source
    }
    
    deinit {
        print("Entrou deinit LoginViewController")
    }
}

