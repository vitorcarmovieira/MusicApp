//
//  ForgotPasswordViewController.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/21/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var txtMsgIncorrect: UILabel!
    @IBOutlet weak var viewForgot: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    let activity = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewForgot.layer.cornerRadius = 10
        
        let imgName = UIImageView(image: #imageLiteral(resourceName: "login_mail"))
        if let size = imgName.image?.size {
            imgName.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        
        imgName.contentMode = .center
        
        txtEmail.leftViewMode = .always
        txtEmail.leftView     = imgName
        
        if let left = txtEmail.leftView {
            left.frame.origin = CGPoint(x: 0.0, y: 0.0)
            left.frame.size   =  CGSize(width: txtEmail.frame.size.width * 0.1, height: txtEmail.frame.size.height)
            left.backgroundColor = UIColor(red: 227/255, green: 235/255, blue: 243/255, alpha: 1.0)
        }
        
        txtMsgIncorrect.isHidden = true
        
        activity.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        activity.backgroundColor = UIColor.clear
        activity.isHidden    = true
        
        self.view.addSubview(activity)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height / 2.0
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += keyboardSize.height / 2.0
        }
    }
    
    @IBAction func actionBtnSend(_ sender: UIButton) {
        if txtEmail.text != "" {
            view.endEditing(true)
            
            activity.isHidden    = false
            activity.start()
            viewForgot.isHidden  = true
            
            let email    = txtEmail.text
            
            var data: [String:Any] = [:]
            
            data["email"] = email!
            
            let user  = UserManager.sharedUserManager
            
            user.server = "http://37.208.116.28"
            
            let postString = data
            
            let urlToRequest = user.server + ":3002/forgot"
            
            var request = URLRequest(url:URL(string:urlToRequest)!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
            request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
            let session = URLSession.shared
            //Post
            session.dataTask(with: request){data, response, err in
                //Guard: ws there error ?
                guard(err == nil) else {
                    print("\(String(describing: err))")
                    return
                }
                //Guard: check was any data returned?
                guard let data = data else{
                    print("no data return")
                    return
                }
                
                let parseResult: [String:AnyObject]!
                do{//
                    parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                    
                    if let data = parseResult["status"] as? Bool {
                        let status = data
                        
                        if status {
                            DispatchQueue.main.async{
                                self.activity.isHidden    = true
                                self.viewForgot.isHidden  = true
                                self.showAlert()
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.activity.isHidden    = true
                                self.viewForgot.isHidden  = false
                                
                                self.txtMsgIncorrect.isHidden = false
                                self.activity.done()
                                
                                self.makeAnimationOnTxt(field: self.txtEmail)
                            }
                        }
                        
                    }
                } catch {
                    print("Could not parse data as Json \(data)")
                    return
                }
                
                }.resume()
        } else {
            txtMsgIncorrect.isHidden = false
            self.makeAnimationOnTxt(field: txtEmail)
        }
    }
    
    func makeAnimationOnTxt(field: UITextField) {
        UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
            field.center = CGPoint(x: field.center.x + 20, y: field.center.y)
        }, completion: { (finished: Bool) in
            UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                field.center = CGPoint(x: field.center.x - 40, y: field.center.y)
            }, completion: { (finished: Bool) in
                UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                    field.center = CGPoint(x: field.center.x + 40, y: field.center.y)
                }, completion: { (finished: Bool) in
                    UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                        field.center = CGPoint(x: field.center.x - 40, y: field.center.y)
                    }, completion: { (finished: Bool) in
                        UIView.animate(withDuration: 0.10, delay: 0.0, options: [], animations: {
                            field.center = CGPoint(x: field.center.x + 20, y: field.center.y)
                        }, completion: { (finished: Bool) in
                            
                        })
                    })
                })
            })
        })
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Success", message: "Verify your email.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionBtnCancel(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let first = touches.first {
            if let v = first.view {
                if v != txtEmail {
                    view.endEditing(true)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("Entrou deinit ForgotPasswordViewController")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
