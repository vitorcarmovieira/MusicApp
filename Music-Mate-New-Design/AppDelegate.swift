
//  AppDelegate.swift
//  Music-Mate-New-Design

//  Created by Diego Lopes on 10/2/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AVFoundation
import Appsee
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, AppsFlyerTrackerDelegate {
    //asd
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.isIdleTimerDisabled = true
        Appsee.start();
        AppsFlyerTracker.shared().appsFlyerDevKey = "acdiD8uRRhsaxfZ77UaPC4"
        AppsFlyerTracker.shared().appleAppID = "1299773502"
        AppsFlyerTracker.shared().delegate = self
        //        let defaults = UserDefaults.standard
        //        defaults.set(true, forKey: "MIDI")
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window?.makeKeyAndVisible()
//
//        let controller = HomeViewController()
//        window?.rootViewController = UINavigationController(rootViewController: controller)
//
        Utils.copyFile("musicMate.sqlite")
        
        let user = UserManager.sharedUserManager
        //
        if let userModel = DAOUser.getInstance().getUserActive() {
            user.email     = userModel.email
            user.firstName = userModel.firstName
            user.token     = userModel.token
            user.image     = userModel.image
            user.server = "http://37.208.116.28"
            
            let controller = HomeViewController()
            window?.rootViewController = UINavigationController(rootViewController: controller)
            //
            //            let controller = HomeViewController()
            //            window?.rootViewController = UINavigationController(rootViewController: controller)
            ////
            //        }
            //
            let session = AVAudioSession.sharedInstance()
            do {
                try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
                try session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            } catch _ {
                print("Error")
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if let userAtivo = DAOUser.getInstance().getUserActive(){
            AppsFlyerTracker.shared().customerUserID = userAtivo.interests;
        }
        AppsFlyerTracker.shared().trackAppLaunch()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}




