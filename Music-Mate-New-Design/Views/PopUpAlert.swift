//
//  PopUpAlert.swift
//  Music-Mate-New-Design
//
//  Created by Nicolas Fernandes de Lima on 24/05/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class PopUpAlert : UIView {
    
    @IBOutlet weak var btnCancel     : UIButton!
    @IBOutlet weak var btnConfirm    : UIButton!
    @IBOutlet weak var txtDescription: UITextField!
    
    
    
    var txtSaveProfile   = "save profile?"
    var txtLogout        = "logout?"
    var txtSaveCompose   = "save compose?"
    var txtSaveEdit      = "save edit?"
    var txtSendToServer  = "send to server?"

    var acao = 0
    
    
    
    func alertSaveProfile () {
        acao = 1
        txtDescription.text = txtSaveProfile
    }
    
    func alertLogout () {
        txtDescription.text = txtLogout
    }
    func alertSaveCompose () {
        acao = 3
        txtDescription.text = txtSaveCompose
    }
    func alertSaveEdit () {
        acao = 4
        txtDescription.text = txtSaveEdit
    }
    func alertSendToServer () {
        acao = 5
        txtDescription.text = txtSendToServer
    }


    
}
