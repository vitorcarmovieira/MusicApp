//
//  FilterStoreView.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 30/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class FilterStoreView: UIView {
    //Opções de países
    weak var tbCountry: UITableView!
    //Opções de genero 1
    weak var tbGener1: UITableView!
    //Opções de genero 2
    weak var tbGener2: UITableView!
    //Opções de instrumentos
    weak var tbInstrument: UITableView!
    //Opções de dificuldade
    weak var tbDificult: UITableView!
    
    ///Label sobre o artista
    weak var lblAboutArtist   : UILabel!
    
    ///Label usuario
    weak var lblProfile       : UILabel!
    
    ///Label sobre o artista
    weak var lblComposer      : UILabel!
    ///Label compositor requerido
    weak var lblReqComposer   : UILabel!
    ///Label pais
    weak var lblCountry       : UILabel!
    ///Label pais requerido
    weak var lblReqCountry    : UILabel!
    
    ///Label sobre partitura
    weak var lblAboutScore    : UILabel!
    ///Label titulo
    weak var lblTitle         : UILabel!
    ///Label titulo requerido
    weak var lblReqTitle      : UILabel!
    ///Label genero
    weak var lblGenero        : UILabel!
    ///Label genero requerido
    weak var lblReqGenero     : UILabel!
    ///Label genero2
    weak var lblGenero2       : UILabel!
    ///Label genero2 requerido
    weak var lblReqGenero2    : UILabel!
    ///Label instrumento
    weak var lblInstrument    : UILabel!
    ///Label instrumento requerido
    weak var lblReqInstrument : UILabel!
    ///Label tipo requerido
    weak var lblReqType       : UILabel!
    ///Label data
    weak var lblDate          : UILabel!
    ///Label data requerida
    weak var lblReqDate       : UILabel!
    ///Label dificuldade
    weak var lblDifficulty    : UILabel!
    ///Label dificuldade requerido
    weak var lblReqDifficulty : UILabel!
    
    ///Barra horizontal esquerda do sobre artista
    weak var barLAboutArtist  : UIView!
    ///Barra horizontal direita do sobre artista
    weak var barRAboutArtist  : UIView!
    
    ///Barra horizontal esquerda do sobre partitura
    weak var barLAboutScore   : UIView!
    ///Barra horizontal direita do sobre partitura
    weak var barRAboutScore   : UIView!
    
    ///Imagem perfil usuario
    weak var imgProfile       : UIImageView!
    
    /// Campo Composer
    weak var txtComposer      : UITextField!
    /// Campo Pais
    weak var txtCountry       : UITextField!
    /// Campo Title
    weak var txtTitle         : UITextField!
    /// Campo Subtitle
    weak var txtSubtitle      : UITextField!
    /// Campo Gener1
    weak var txtGener1        : UITextField!
    /// Campo Gener2
    weak var txtGener2        : UITextField!
    /// Campo Instrumento
    weak var txtInstrument    : UITextField!
    /// Campo Instrumento
    weak var txtDate          : UITextField!
    /// Campo Instrumento
    weak var txtDificult      : UITextField!
    /// Campo Instrumento
    weak var txtTags          : UITextField!
    
    /// Campo Composer
    var vComposer      : UIView!
    /// Campo Pais
    var vCountry       : UIView!
    /// Campo Title
    var vTitle         : UIView!
    /// Campo Subtitle
    var vSubtitle      : UIView!
    /// Campo Gener1
    var vGener1        : UIView!
    /// Campo Gener2
    var vGener2        : UIView!
    /// Campo Instrumento
    var vInstrument    : UIView!
    /// Campo Instrumento
    var vDate          : UIView!
    /// Campo Instrumento
    var vDificult      : UIView!
    /// Campo Instrumento
    var vTags      : UIView!
    
    var countries   = ["Argentina", "Argelia", "Brasil", "Canada", "Estados Unidos", "México", "Nova Zelandia", "Holanda"]
    var geners1     = ["Classic", "Modern"]
    var geners2     = ["Jazz", "Pop", "Rock", "Romantic" ]
    var instruments = ["Flute", "Guitar", "Piano", "Sax", "Violin" ]
    var dificulties = ["Easy", "Medium", "Hard"]
    
    override func draw(_ rect: CGRect) {
        let xIni = rect.width * 0.08
        let widthAbout = rect.width
        let height1 = rect.height * 0.095
        let height2 = rect.height * 0.082
        let height3 = rect.height * 0.10
        
        //--------------------Posiciona barras----------------------
        let yBar          = height2 * 0.475
        let widthBar      = rect.width / 3.0
        let heigthBar     = height2 * 0.05
        let rectLBarArtist = CGRect(x: 0.0, y: yBar, width: rect.width / 3.0, height: heigthBar)
        let rectRBarArtist = CGRect(x: rect.width - widthBar, y: yBar, width: widthBar, height: heigthBar)
        
        initializeBar(bar: &barLAboutArtist, rect: rectLBarArtist)
        initializeBar(bar: &barRAboutArtist, rect: rectRBarArtist)
        
        barLAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        barRAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        let color = UIColor(red: 14/255, green: 86/255, blue: 160/255, alpha: 1.0)
        let rectAboutArtist = CGRect(x: 0, y: 0, width: widthAbout, height: height2)
        
        initializeLabel(label: &lblAboutArtist, text: "About Artist", frame: rectAboutArtist, color: color, prop: 0.25)
        //----------------------------------------------------------
        
        //----------------Posiciona imagem usuario------------------
        let yImg      = height2
        let widthImg  = height3 * 0.5
        let heightImg = height3 * 0.5
        let rectImg   = CGRect(x: xIni, y: yImg, width: widthImg, height: heightImg)
        
        initializeImage(imageView: &imgProfile, image: #imageLiteral(resourceName: "peter"), frame: rectImg)
        //----------------------------------------------------------
        
        //-----------------Posiciona label usuario------------------
        let xLblUser      = xIni + widthImg + (xIni / 3.0)
        let yLblUser      = yImg
        let widthLblUser  = rect.width - xLblUser
        let heightLblUser = heightImg
        let rectLblUser   = CGRect(x: xLblUser, y: yLblUser, width: widthLblUser, height: heightLblUser)
        
        initializeLabel(label: &lblProfile, text: "Peter Tilanus", frame: rectLblUser, color: UIColor.black, prop: 0.4, align: .left)
        //----------------------------------------------------------
        
        //---------------Posiciona txts sobre artista---------------
        let widthTxtComplete = rect.width - (2.0 * xIni)
        let heightTxts       = height2 * 0.6
        
        let yTxtComposer     = height2 + height3
        let yTxtCountry      = yTxtComposer + height2
        
        let rectTxtComposer  = CGRect(x: xIni, y: yTxtComposer, width: widthTxtComplete, height: heightTxts)
        let rectTxtCountry   = CGRect(x: xIni, y: yTxtCountry, width: widthTxtComplete, height: heightTxts)
        
        initializeTextField(field: &txtComposer, text: "Composer", frame: rectTxtComposer, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtCountry, text: "Country", frame: rectTxtCountry, widthComplete: widthTxtComplete)
        //----------------------------------------------------------
        
        //--------------------Posiciona barras----------------------
        let yBarA          = yTxtCountry + (height1 * 0.7) + (height2 * 0.475)
        let rectLBarScore  = CGRect(x: 0.0, y: yBarA, width: rect.width / 3.0, height: heigthBar)
        let rectRBarScore  = CGRect(x: rect.width - widthBar, y: yBarA, width: widthBar, height: heigthBar)
        
        initializeBar(bar: &barLAboutScore, rect: rectLBarScore)
        initializeBar(bar: &barRAboutScore, rect: rectRBarScore)
        
        barLAboutScore.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        barRAboutScore.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        let yLblS          = yTxtCountry + (height1 * 0.7)
        let rectAboutScore = CGRect(x: 0, y: yLblS, width: widthAbout, height: height2)
        
        initializeLabel(label: &lblAboutScore, text: "About Score", frame: rectAboutScore, color: color, prop: 0.25)
        //----------------------------------------------------------
        
        //-------------Posiciona txts sobre partitura---------------
        let yTxtTitle      = yLblS + height2
        let yTxtSubtitle   = yTxtTitle + height2
        let yTxtGener1     = yTxtSubtitle + height2
        let yTxtGener2     = yTxtGener1 + height2
        let yTxtInstrument = yTxtGener2 + height2
        let yTxtDate       = yTxtInstrument + height2//
        let yTxtTags       = yTxtDate + height2
        let widthMidle = (rect.width - (2.0 * xIni)) / 2.3
        let xMidle     = rect.width / 1.8
        
        let rectTxtTitle      = CGRect(x: xIni, y: yTxtTitle, width: widthTxtComplete, height: heightTxts)
        let rectTxtSubtitle   = CGRect(x: xIni, y: yTxtSubtitle, width: widthTxtComplete, height: heightTxts)
        let rectTxtGener1     = CGRect(x: xIni, y: yTxtGener1, width: widthTxtComplete, height: heightTxts)
        let rectTxtGener2     = CGRect(x: xIni, y: yTxtGener2, width: widthTxtComplete, height: heightTxts)
        let rectTxtInstrument = CGRect(x: xIni, y: yTxtInstrument, width: widthTxtComplete, height: heightTxts)
        let rectTxtDate       = CGRect(x: xMidle, y: yTxtDate, width: widthMidle, height: heightTxts)
        let rectTxtDificult   = CGRect(x: xIni, y: yTxtDate, width: widthMidle, height: heightTxts)
        let rectTxtTags       = CGRect(x: xIni, y: yTxtTags, width: widthTxtComplete, height: heightTxts)
        
        initializeTextField(field: &txtTitle, text: "Title", frame: rectTxtTitle, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtSubtitle, text: "Subtitle", frame: rectTxtSubtitle, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtGener1, text: "Gener", frame: rectTxtGener1, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtGener2, text: "Subgener", frame: rectTxtGener2, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtInstrument, text: "Instrument", frame: rectTxtInstrument, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtDate, text: "Date", frame: rectTxtDate, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtDificult, text: "Difficult", frame: rectTxtDificult, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtTags, text: "Tags", frame: rectTxtTags, widthComplete: widthTxtComplete)
        //----------------------------------------------------------
        
        txtCountry.rightViewMode    = .always
        txtGener1.rightViewMode     = .always
        txtGener2.rightViewMode     = .always
        txtInstrument.rightViewMode = .always
        txtDate.rightViewMode       = .always
        txtDificult.rightViewMode   = .always
        
        setRightView(field: &txtCountry, type: 1)
        setRightView(field: &txtGener1, type: 1)
        setRightView(field: &txtGener2, type: 1)
        setRightView(field: &txtInstrument, type: 1)
        setRightView(field: &txtDate, type: 2)
        setRightView(field: &txtDificult, type: 1)
        
        txtDate.keyboardType = .decimalPad
        
        let boderTxts       = rect.height * 0.005
        let inc             = 2.0 * boderTxts
        let rectVComposer   = CGRect(x: xIni - boderTxts, y: yTxtComposer - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVCountry    = CGRect(x: xIni - boderTxts, y: yTxtCountry - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVTitle      = CGRect(x: xIni - boderTxts, y: yTxtTitle - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVSubtitle   = CGRect(x: xIni - boderTxts, y: yTxtSubtitle - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVGener1     = CGRect(x: xIni - boderTxts, y: yTxtGener1 - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVGener2     = CGRect(x: xIni - boderTxts, y: yTxtGener2 - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVInstrument = CGRect(x: xIni - boderTxts, y: yTxtInstrument - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVDate       = CGRect(x: xMidle - boderTxts, y: yTxtDate - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        let rectVDificult   = CGRect(x: xIni - boderTxts, y: yTxtDate - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        let rectVTags   = CGRect(x: xIni - boderTxts, y: yTxtTags - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        
        vComposer   = UIView(frame: rectVComposer)
        vCountry    = UIView(frame: rectVCountry)
        vTitle      = UIView(frame: rectVTitle)
        vSubtitle   = UIView(frame: rectVSubtitle)
        vGener1     = UIView(frame: rectVGener1)
        vGener2     = UIView(frame: rectVGener2)
        vInstrument = UIView(frame: rectVInstrument)
        vDate       = UIView(frame: rectVDate)
        vDificult   = UIView(frame: rectVDificult)
        vTags       = UIView(frame: rectVTags)
        
        let colorBg = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
        
        vComposer.backgroundColor   = colorBg
        vCountry.backgroundColor    = colorBg
        vTitle.backgroundColor      = colorBg
        vSubtitle.backgroundColor   = colorBg
        vGener1.backgroundColor     = colorBg
        vGener2.backgroundColor     = colorBg
        vInstrument.backgroundColor = colorBg
        vDate.backgroundColor       = colorBg
        vDificult.backgroundColor   = colorBg
        vTags.backgroundColor       = colorBg
        
        vComposer.layer.cornerRadius   = heightTxts * 0.1
        vCountry.layer.cornerRadius    = heightTxts * 0.1
        vTitle.layer.cornerRadius      = heightTxts * 0.1
        vSubtitle.layer.cornerRadius   = heightTxts * 0.1
        vGener1.layer.cornerRadius     = heightTxts * 0.1
        vGener2.layer.cornerRadius     = heightTxts * 0.1
        vInstrument.layer.cornerRadius = heightTxts * 0.1
        vDate.layer.cornerRadius       = heightTxts * 0.1
        vDificult.layer.cornerRadius   = heightTxts * 0.1
        vTags.layer.cornerRadius       = heightTxts * 0.1
        
        //        addSubview(vComposer)
        //        addSubview(vCountry)
        //        addSubview(vTitle)
        //        addSubview(vSubtitle)
        //        addSubview(vGener1)
        //        addSubview(vGener2)
        //        addSubview(vInstrument)
        //        addSubview(vDate)
        //        addSubview(vDificult)
        //        addSubview(vTags)
        
        sendSubview(toBack: vComposer)
        sendSubview(toBack: vCountry)
        sendSubview(toBack: vTitle)
        sendSubview(toBack: vSubtitle)
        sendSubview(toBack: vGener1)
        sendSubview(toBack: vGener2)
        sendSubview(toBack: vInstrument)
        sendSubview(toBack: vDate)
        sendSubview(toBack: vDificult)
        sendSubview(toBack: vTags)
        
        txtDate.placeholder = "___/___/______"
        txtDate.textColor = UIColor.black
        txtDate.addTarget(self, action: #selector(applyFilterDateOnTextField(_:)), for: UIControlEvents.editingChanged)
        
        let desloc = widthTxtComplete * 0.2
        
        let rectTbCountry    = CGRect(x: xIni + desloc, y: yTxtCountry + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        let rectTbGener1     = CGRect(x: xIni + desloc, y: yTxtGener1 + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        let rectTbGener2     = CGRect(x: xIni + desloc, y: yTxtGener2 + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        let rectTbInstrument = CGRect(x: xIni + desloc, y: yTxtInstrument + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        let rectTbDificult   = CGRect(x: xIni + desloc, y: yTxtDate + heightTxts - (heightTxts * 3.0), width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        
        initializeTableView(table: &tbCountry, frame: rectTbCountry)
        initializeTableView(table: &tbGener1, frame: rectTbGener1)
        initializeTableView(table: &tbGener2, frame: rectTbGener2)
        initializeTableView(table: &tbInstrument, frame: rectTbInstrument)
        initializeTableView(table: &tbDificult, frame: rectTbDificult)
        
        tbCountry.register(UITableViewCell.self, forCellReuseIdentifier: "cellCountry")
        tbGener1.register(UITableViewCell.self, forCellReuseIdentifier: "cellGener1")
        tbGener2.register(UITableViewCell.self, forCellReuseIdentifier: "cellGener2")
        tbInstrument.register(UITableViewCell.self, forCellReuseIdentifier: "cellInstrument")
        tbDificult.register(UITableViewCell.self, forCellReuseIdentifier: "cellDificult")
        
        txtCountry.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtGener1.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtGener2.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtInstrument.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtDificult.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        
        txtCountry.placeholder = "Choose a country"
        
        addBorderRed(field: &txtComposer)
        addBorderRed(field: &txtCountry)
        addBorderRed(field: &txtTitle)
        addBorderRed(field: &txtSubtitle)
        addBorderRed(field: &txtGener1)
        addBorderRed(field: &txtGener2)
        addBorderRed(field: &txtInstrument)
        addBorderRed(field: &txtDate)
        addBorderRed(field: &txtDificult)
        addBorderRed(field: &txtTags)
        
        //        let deslocVertical    = boderTxts
        //        let rectLblTitle      = CGRect(x: xIni, y: yTxtTitle, width: widthTxtComplete, height: heightTxts)
        //        let rectLblSubtitle   = CGRect(x: xIni, y: yTxtSubtitle, width: widthTxtComplete, height: heightTxts)
        //        let rectLblGener1     = CGRect(x: xIni, y: yTxtGener1, width: widthTxtComplete, height: heightTxts)
        //        let rectLblGener2     = CGRect(x: xIni, y: yTxtGener2, width: widthTxtComplete, height: heightTxts)
        //        let rectLblInstrument = CGRect(x: xIni, y: yTxtInstrument, width: widthTxtComplete, height: heightTxts)
        let rectLblDate       = CGRect(x: xMidle, y: yTxtDate + heightTxts , width: widthMidle, height: heightTxts/2.0)
        //        let rectLblDificult   = CGRect(x: xIni, y: yTxtDate, width: widthMidle, height: heightTxts)
        //        let rectLblTags       = CGRect(x: xIni, y: yTxtTags, width: widthTxtComplete, height: heightTxts)
        
        initializeLabel(label: &lblDate, text: "Enter a valid date", frame: rectLblDate, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        
    }
    
    func addBorderRed(field: inout UITextField!) {
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.red.cgColor
    }
    
    private var indexText = 0
    
    @objc private func applyFilterDateOnTextField(_ field: UITextField) {
        let text = field.text!
        var textWithMask = ""
        
        if text.count - 1 >= 0{
            if isNumber(textToValidate: text[text.count - 1]) {
                for i in 0..<text.count {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i < 10 {
                        textWithMask += text[i]
                    }
                    
                }
            } else {
                for i in 0..<(text.count - 1) {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    if i < 10 {
                        textWithMask += text[i]
                    }
                }
            }
        }
        
        
        
        let validator = Validator()
        
        if validator.validateDate(date: txtDate.text!){
            txtDate.layer.borderColor = UIColor.clear.cgColor
        }else{
            txtDate.layer.borderColor = UIColor.red.cgColor
        }
        
        field.text = textWithMask
    }
    
    public func isNumber(textToValidate:String) -> Bool{
        
        let num = Int(textToValidate)
        
        if num != nil {
            return true
        }
        
        return false
    }
    
    func setRightView(field: inout UITextField!, type: Int) {
        let border         = field.frame.height * 0.2
        var widthImgRight  = field.frame.height * 0.3
        var heightImgRight = widthImgRight * 1.6
        if type == 2 {
            widthImgRight  = field.frame.height * 0.6
            heightImgRight = widthImgRight * 0.9
        }
        
        let x = field.frame.width - widthImgRight - (border * 3.0)
        let right = UIImageView(frame: CGRect(x: x, y: border, width: widthImgRight, height: heightImgRight))
        
        if type == 1 {
            right.image = #imageLiteral(resourceName: "up_down")
        } else {
            right.image = #imageLiteral(resourceName: "mini_calendar")
        }
        field.rightView = right
    }
    
    func getLeftView(text: String, widthTxtComplete: CGFloat, height: CGFloat) -> UIView {
        let heightTxts = height
        let left = UIView(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        left.backgroundColor = UIColor(red: 8/255, green: 78/255, blue: 148/255, alpha: 1.0)
        left.layer.cornerRadius = heightTxts * 0.1
        
        let widthWhiteView = (heightTxts * 0.1)
        let xWhiteView = (widthTxtComplete * 0.2) - widthWhiteView
        let whiteView = UIView(frame: CGRect(x: xWhiteView, y: 0, width: widthWhiteView, height: heightTxts))
        whiteView.backgroundColor = UIColor.white
        left.addSubview(whiteView)
        
        let lblLeft = UILabel(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        lblLeft.text = text
        lblLeft.textColor = UIColor.white
        lblLeft.textAlignment = NSTextAlignment.center
        lblLeft.font = UIFont(name: "Lato-Regular", size: heightTxts * 0.3)
        left.addSubview(lblLeft)
        
        return left
    }
    
    func initializeTextField(field: inout UITextField!, text: String, frame: CGRect, widthComplete: CGFloat) {
        if field == nil {
            let f = UITextField(frame: frame)
            f.backgroundColor = UIColor.white
            f.layer.cornerRadius = frame.height * 0.1
            f.leftViewMode = .always
            f.delegate = self
            f.leftView = getLeftView(text: text, widthTxtComplete: widthComplete, height: frame.height)
            addSubview(f)
            field = f
        } else {
            field.frame = frame
        }
    }
    
    func initializeImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            addSubview(img)
            imageView = img
        } else {
            imageView.frame = frame
            imageView.image = image
        }
    }
    
    func initializeBar(bar: inout UIView!, rect: CGRect) {
        if bar == nil {
            let b = UIView(frame: rect)
            b.backgroundColor = UIColor(red: 201/255, green: 201/255, blue: 201/255, alpha: 1.0)
            
            addSubview(b)
            bar = b
        } else {
            bar.frame = frame
        }
    }
    
    func initializeTableView(table: inout UITableView!, frame: CGRect) {
        if table == nil {
            let tb = UITableView(frame: frame)
            tb.delegate = self
            tb.dataSource = self
            tb.isHidden = true
            addSubview(tb)
            
            table = tb
        } else {
            table.frame = frame
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect, color: UIColor, prop: CGFloat, align: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = color
            lbl.textAlignment = align
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * prop)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
}

extension FilterStoreView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbCountry {
            return countries.count
        }
        if tableView == tbGener1 {
            return geners1.count
        }
        if tableView == tbGener2 {
            return geners2.count
        }
        if tableView == tbInstrument {
            return instruments.count
        }
        if tableView == tbDificult {
            return dificulties.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellName = ""
        
        if tableView == tbCountry {
            cellName = "cellCountry"
        }
        if tableView == tbGener1 {
            cellName = "cellGener1"
        }
        if tableView == tbGener2 {
            cellName = "cellGener2"
        }
        if tableView == tbInstrument {
            cellName = "cellInstrument"
        }
        if tableView == tbDificult {
            cellName = "cellDificult"
        }
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellName) as UITableViewCell!
        
        if tableView == tbCountry {
            cell.textLabel?.text = countries[indexPath.row]
            cell.textLabel?.font = txtCountry.font
        }
        if tableView == tbGener1 {
            cell.textLabel?.text = geners1[indexPath.row]
            cell.textLabel?.font = txtGener1.font
        }
        if tableView == tbGener2 {
            cell.textLabel?.text = geners2[indexPath.row]
            cell.textLabel?.font = txtGener2.font
        }
        if tableView == tbInstrument {
            cell.textLabel?.text = instruments[indexPath.row]
            cell.textLabel?.font = txtInstrument.font
        }
        if tableView == tbDificult {
            cell.textLabel?.text = dificulties[indexPath.row]
            cell.textLabel?.font = txtDificult.font
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Row selected, so set textField to relevant value, hide tableView
        // endEditing can trigger some other action according to requirements
        if tableView == tbCountry {
            txtCountry.text = countries[indexPath.row]
            tbCountry.isHidden = true
            txtCountry.endEditing(true)
            txtCountry.layer.borderColor = UIColor.clear.cgColor
            txtCountry.isEnabled = true
        }
        
        if tableView == tbGener1 {
            txtGener1.text = geners1[indexPath.row]
            tbGener1.isHidden = true
            txtGener1.endEditing(true)
            txtGener1.layer.borderColor = UIColor.clear.cgColor
            txtGener1.isEnabled = true
        }
        
        if tableView == tbGener2 {
            txtGener2.text = geners2[indexPath.row]
            tbGener2.isHidden = true
            txtGener2.endEditing(true)
            txtGener2.layer.borderColor = UIColor.clear.cgColor
            txtGener2.isEnabled = true
        }
        
        if tableView == tbInstrument {
            txtInstrument.text = instruments[indexPath.row]
            tbInstrument.isHidden = true
            txtInstrument.endEditing(true)
            txtInstrument.layer.borderColor = UIColor.clear.cgColor
            txtInstrument.isEnabled = true
        }
        
        
        if tableView == tbDificult {
            txtDificult.text = dificulties[indexPath.row]
            tbDificult.isHidden = true
            txtDificult.endEditing(true)
            txtDificult.layer.borderColor = UIColor.clear.cgColor
            txtDificult.isEnabled = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
}

extension FilterStoreView: UITextFieldDelegate {
    @objc func textFieldActive(_ field: UITextField) {
        if field == txtCountry {
            tbCountry.isHidden = !tbCountry.isHidden
        } else if field == txtGener1 {
            tbGener1.isHidden = !tbCountry.isHidden
        } else if field == txtGener2 {
            tbGener2.isHidden = !tbCountry.isHidden
        } else if field == txtInstrument {
            tbInstrument.isHidden = !tbCountry.isHidden
        } else if field == txtDificult {
            tbDificult.isHidden = !tbCountry.isHidden
        }
        
        field.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField == txtComposer || textField == txtTitle {
            textField.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // TODO: Your app can do something when textField finishes editing
        print("The textField ended editing. Do something based on app requirements.")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
