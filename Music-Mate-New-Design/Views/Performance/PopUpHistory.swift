//
//  PopUpHistory.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/20/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class PopUpHistory: UIView {


    weak var lblStatistic  : UILabel!
    weak var lblMusic      : UILabel!
    weak var lblComposer   : UILabel!
    weak var lblArtist     : UILabel!
    weak var lblSchedule   : UILabel!
    weak var lblPlayed     : UILabel!
    weak var prgPlayed     : UIProgressView!
    weak var stripeView1   : UIView!
    weak var btnDone  : UIButton!
    weak var lblAcuracy    : UILabel!
    weak var lblpercent    : UILabel!
    weak var lblHystory    : UILabel!
    weak var historyCollection: UICollectionView!
    var percentAcuracy = CGFloat(0.6)
    var percentCell = CGFloat(0.6)
    override func draw(_ rect: CGRect) {
        let height1 = rect.height * 0.1
        let height2 = rect.height * 0.2
        let height3 = rect.height * 0.25
//        let height4 = rect.height * 0.3
//        let height5 = rect.height * 0.15
        
        let borderTop  = rect.height * 0.03
        let borderLeft = rect.width * 0.09
        let heightLbls = (height2 / 4.0) - (borderTop / 2.0)
        
        let rectLblAcuracy    = CGRect(x: borderLeft*1.2, y: rect.height*0.01, width: rect.width, height: heightLbls)
        let rectLblPercent    = CGRect(x: borderLeft*8.4, y: rect.height*0.032, width: rect.width*0.2, height: heightLbls)
        let rectLblHystory    = CGRect(x: 0, y: rect.height*0.11, width: rect.width, height: heightLbls)
        
        initLabel(label: &lblAcuracy, text: "Acuracy", frame:rectLblAcuracy)
        initLabel(label: &lblpercent, text: "50%", frame:rectLblPercent)
        initLabelBlue(label: &lblHystory, text: "History", frame: rectLblHystory)
        lblAcuracy.textAlignment = .left
        
        initStripeView(stripe: &stripeView1, frame: CGRect(x: 0, y: frame.height * 0.09, width: frame.width, height: frame.height*0.001))
       
        initProgress(progress: &prgPlayed, value: 0.5, frame: CGRect(x: borderLeft, y:rect.height*0.05, width: rect.width*0.7, height: rect.height*0.01))
        
        
        initializeButtonCancelDone(button: &btnDone, image: #imageLiteral(resourceName: "botao"), txt: "Done", frame: CGRect(x: frame.width * 0.2, y: frame.height * 0.9, width: frame.width*0.6, height: frame.height*0.04))

        
        let rectCollection = CGRect(x:frame.width*0.05, y: rect.height*0.15, width: frame.width*0.9, height: (height3 * 1.1))
        
        initCollectionView(collection: &historyCollection, frame: rectCollection)

        historyCollection.register(CollectionViewCellHistory.self, forCellWithReuseIdentifier: "cell")
     
    }
    
    
    
    func initLabelBlue(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.6)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initCollectionView(collection: inout UICollectionView!, frame: CGRect) {
        if collection == nil {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: frame.height * 0.1, left: frame.width * 0.0, bottom: frame.height * 0.03, right: frame.width * 0.0)
            layout.minimumLineSpacing = frame.width*0.04
            layout.itemSize = CGSize(width: frame.width*0.21, height: frame.height)
            layout.scrollDirection = .horizontal
            let tb = UICollectionView(frame: frame, collectionViewLayout: layout)
            tb.backgroundColor = UIColor.clear
            tb.delegate = self
            tb.dataSource = self

            addSubview(tb)
            
            collection = tb
        } else {
            collection.frame = frame
        }
    }
    
    func initButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initializeButtonCancelDone(button: inout UIButton!, image: UIImage, txt : String, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            //  btn.setImage(image, for: .normal)
            btn.setBackgroundImage(image, for: .normal)
            btn.setTitle(txt, for: .normal)
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: frame.height * 0.5)
            btn.titleLabel?.textColor = UIColor.white
            btn.titleLabel?.textAlignment = .center
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.center
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    
    
    func initProgress(progress: inout UIProgressView!, value: Float, frame: CGRect) {
        if progress == nil {
            let prg = UIProgressView(frame: frame)
            prg.progress = value
            prg.progressTintColor = UIColor(red: 26/255, green: 216/255, blue: 117/255, alpha: 1.0)
            prg.trackTintColor = UIColor.white
            prg.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
            prg.layer.borderWidth = frame.height * 0.006
            prg.layer.cornerRadius = 10
            prg.clipsToBounds = true
            prg.contentMode = .scaleAspectFit
            prg.transform = CGAffineTransform(scaleX: 1.0, y: 5.0)
            addSubview(prg)
            progress = prg
        } else {
            progress.frame = frame
        }
    }
    
    func initLabel(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.4)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    
    
    func initImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let btn = UIImageView(frame: frame)
            btn.image = image
            addSubview(btn)
            
            imageView = btn
        } else {
            imageView.frame = frame
        }
    }
    
    
}

extension PopUpHistory: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCellHistory
        
        let borderTop = collectionView.frame.height * 0.05
        let height1 = collectionView.frame.height * 0.5
        let height2 = collectionView.frame.height * 0.1
        let height3 = collectionView.frame.height * 0.2
        let height4 = collectionView.frame.height * 0.1
        
        let bg = UIImageView(frame: CGRect(x: 0, y: borderTop, width: cell.frame.width, height: height1))
        bg.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        bg.layer.borderWidth = frame.height * 0.0013
        bg.backgroundColor = UIColor(red: 26/255, green: 216/255, blue: 117/255, alpha: 1.0)
        cell.addSubview(bg)
        cell.barBg = bg
        
        let prgPlayed = UIView(frame: CGRect(x: cell.frame.width*0.028, y: borderTop*1.1, width: cell.frame.width*0.944, height: height1 - (height1 * self.percentAcuracy)))
        prgPlayed.backgroundColor = UIColor.white
        cell.addSubview(prgPlayed)
        cell.progressBar = prgPlayed
        
        
        
        let lblMonth = UILabel(frame: CGRect(x: 0, y: borderTop + height1, width: cell.frame.width, height: height2))
        lblMonth.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
        lblMonth.text = "AGO"
        lblMonth.textAlignment = NSTextAlignment.center
        lblMonth.font = UIFont(name: "Lato-Regular", size: height2 * 0.5)
        cell.addSubview(lblMonth)
        cell.monthLbl = lblMonth
        
        let lblDay = UILabel(frame: CGRect(x: 0, y:  height1 + height2, width: cell.frame.width, height: height3))
        lblDay.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
        lblDay.text = "22"
        lblDay.textAlignment = NSTextAlignment.center
        lblDay.font = UIFont(name: "Lato-Regular", size: height3 * 0.5)
        cell.addSubview(lblDay)
        cell.dayLbl = lblDay
        
        let stp = UIView(frame: CGRect(x: cell.frame.width*0.05, y: height1 + height2 + height3*0.9, width: cell.frame.width*0.9, height: cell.frame.height*0.002))
        stp.backgroundColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
        cell.addSubview(stp)
        
        let lblYear = UILabel(frame: CGRect(x: 0, y: height1 + height2 + height3, width: cell.frame.width, height: height4))
        lblYear.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
        lblYear.text = "2017"
        lblYear.textAlignment = NSTextAlignment.center
        lblYear.font = UIFont(name: "Lato-Regular", size: height4 * 0.5)
        cell.addSubview(lblYear)
        cell.yearLbl = lblYear

        

        return cell
    }
}
