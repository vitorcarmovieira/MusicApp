//
//  CompareView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/19/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class CompareView: UIView {
    
    weak var scrlOriginal : UIScrollView!
    weak var scrPlayed    : UIScrollView!
    weak var prtOriginal  : ViewPageScore!
    weak var prtPlayed    : ViewPageScore!
    weak var imgSeparator : UIImageView!
    weak var btnPlayOriginal : UIButton!
    weak var btnPlayPlayed : UIButton!
    
    override func draw(_ rect: CGRect) {
        let heightBar = rect.height * 0.025
        let rectSeparator = CGRect(x: 0, y: (rect.height / 2.0) - (heightBar / 2.0), width: rect.width, height: heightBar)
        
        let widthScroll  = rect.width
        let heightScroll = (rect.height / 2.0) - (heightBar / 2.0)
        
        let rectScrollOriginal = CGRect(x: 0, y: 0, width: widthScroll, height: heightScroll)
        let rectScrollPlayed   = CGRect(x: 0, y: (rect.height / 2.0) + (heightBar / 2.0), width: widthScroll, height: heightScroll)
        
        initScroll(scroll: &scrlOriginal, frame: rectScrollOriginal)
        initScroll(scroll: &scrPlayed, frame: rectScrollPlayed)
        
        initImage(imageView: &imgSeparator, image: #imageLiteral(resourceName: "Separador"), frame: rectSeparator)
        
        initButton(button: &btnPlayPlayed, frame: CGRect(x: rect.width*0.9, y: rect.height*0.55, width: rect.width*0.05, height: rect.width*0.05))
        initButton(button: &btnPlayOriginal, frame: CGRect(x: rect.width*0.9, y: rect.height*0.03, width: rect.width*0.05, height: rect.width*0.05))
        
        scrlOriginal.delegate = self
        scrPlayed.delegate    = self
    }

    func setSizeScroll(size: CGSize) {
        scrlOriginal.contentSize = size
        scrPlayed.contentSize    = size
    }
    
    func initScroll(scroll: inout UIScrollView!, frame: CGRect) {
        if scroll == nil {
            let scl = UIScrollView(frame: frame)
            scl.backgroundColor = UIColor.white
            scl.contentSize = CGSize(width: frame.width, height: frame.height * 2.0)
            addSubview(scl)
            
            scroll = scl
        } else {
            scroll.frame = frame
        }
    }
    
    func initButton(button: inout UIButton!, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setBackgroundImage(#imageLiteral(resourceName: "playcomp"), for: .normal)
            addSubview(btn)
            
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let btn = UIImageView(frame: frame)
            btn.image = image
            addSubview(btn)
            
            imageView = btn
        } else {
            imageView.frame = frame
        }
    }
}

extension CompareView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scrlOriginal {
            scrPlayed.setContentOffset(scrollView.contentOffset, animated: false)
        }
        if scrollView == scrPlayed {
            scrlOriginal.setContentOffset(scrollView.contentOffset, animated: false)
        }
        if scrollView.contentOffset.y < 0.0 {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        }
    }

    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
}
