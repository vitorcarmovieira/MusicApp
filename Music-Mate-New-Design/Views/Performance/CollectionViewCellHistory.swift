//
//  CollectionViewCellHistory.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/22/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class CollectionViewCellHistory: UICollectionViewCell {
    
    @IBOutlet weak var porcentagemLbl: UILabel!
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var barBg: UIImageView!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    
}
