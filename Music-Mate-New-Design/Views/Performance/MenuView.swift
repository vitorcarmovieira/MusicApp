//
//  MenuView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/2/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

/// View responsável pela exibição do menu principal
@IBDesignable
class MenuView: UIView {
    
    ///Botão start do menu
    weak var btnSettings     : UIButton!
    ///Botão restart do menu
    weak var btnCursorBegin   : UIButton!
    ///Botão resume do menu
    weak var btnResume    : UIButton!
    weak var btnPerform   : UIButton!
    weak var btnCompare   : UIButton!
    weak var btnStatistic   : UIButton!
    weak var btnQuantization   : UIButton!
    weak var btnSave   : UIButton!
    weak var btnSend   : UIButton!
    weak var btnEdit   : UIButton!
    weak var btnExit      : UIButton!
    weak var btnSoundOnOff  : UIButton!
    
    
    ///Label start do menu
    weak var lblSettings     : UILabel!
    ///Label restart do menu
    weak var lblCursorBegin   : UILabel!
    ///Label resume do menu
    weak var lblResume    : UILabel!
    ///Label edit do menu
    weak var lblEdit      : UILabel!
    weak var lblPerform      : UILabel!
    weak var lblCompare      : UILabel!
    weak var lblStatistic      : UILabel!
    weak var lblQuantization      : UILabel!
    ///Label save do menu
    weak var lblSave      : UILabel!
    ///Label send do menu
    weak var lblSend      : UILabel!
    ///Label exit do menu
    weak var lblExit      : UILabel!

    weak var stripeView : UIView!
    weak var stripeView2 : UIView!
    weak var stripeView3 : UIView!
    weak var stripeView4 : UIView!
    
    ///Estado atual do menu (popUp, save)
    var state     : StateMenu  = StateMenu.popUpOpened { didSet { setNeedsDisplay() } }
    ///Direção atual do menu (horizontal, vertical)
    var direction : DirectionMenu = DirectionMenu.horizontal { didSet { setNeedsDisplay() } }

    override func draw(_ rect: CGRect) {
        
        backgroundColor = UIColor(red: 3 / 255, green: 65 / 255, blue: 120 / 255, alpha: 1.0)
        
        //Calculo do comprimento e largura do botão do menu
        let width  = frame.height * 0.555
        let heigth = frame.height * 0.555
        
        //Calculo das bordas do botão
        let borderTop    = frame.height * 0.111
        //Calculo do comprimento e largura dos labels do menu
        let widthLbl  = width*1.4
        let heightLbl = rect.height * 0.15
        
        //Calculo da borda de cima do label
        let borderTopLbl = borderTop + width
        //
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnSettings, image: #imageLiteral(resourceName: "settings"), frame: CGRect(x: frame.width * 0.026, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnResume, image: #imageLiteral(resourceName: "start_play"), frame:CGRect(x: frame.width * 0.167, y: borderTop, width: width, height: heigth))
        
      //  initializeButton(button: &btnCursorBegin, image: #imageLiteral(resourceName: "BackBegginer"), frame:CGRect(x: frame.width * 0.273, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnSoundOnOff, image: #imageLiteral(resourceName: "on"), frame:CGRect(x: frame.width * 0.273, y: borderTop, width: width, height: heigth))
        
         initializeButton(button: &btnPerform, image: #imageLiteral(resourceName: "PerformOFF"), frame: CGRect(x: frame.width * 0.416, y: borderTop, width: width + frame.width * 0.013, height: heigth))
   
        initializeButton(button: &btnCompare, image: #imageLiteral(resourceName: "compare"), frame: CGRect(x: frame.width * 0.533, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnStatistic, image: #imageLiteral(resourceName: "statistic"), frame: CGRect(x: frame.width * 0.636, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnQuantization, image: #imageLiteral(resourceName: "1_8"), frame: CGRect(x: frame.width * 0.753, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnExit, image: #imageLiteral(resourceName: "exit"), frame: CGRect(x: frame.width * 0.911, y: borderTop, width: width, height: heigth))
        
//        initializeButton(button: &btnEdit, image: #imageLiteral(resourceName: "btn_menu_edit"), frame: CGRect(x: frame.width * 0.982 - (width * 4.0), y: borderTop, width: width, height: heigth))
//        
//        initializeButton(button: &btnSave, image: #imageLiteral(resourceName: "btn_menu_save"), frame: CGRect(x: frame.width * 0.982 - (width * 3.0), y: borderTop, width: width, height: heigth))
//        
//        initializeButton(button: &btnSend, image: #imageLiteral(resourceName: "btn_menu_send"), frame: CGRect(x: frame.width * 0.982 - (width * 2.0), y: borderTop, width: width, height: heigth))
        
        //-----------------------------------------------------------
     
        
        //--------------------Posiciona labels-----------------------
        initializeLabel(label: &lblSettings, text: "Settings", frame:CGRect(x:btnSettings.frame.minX - frame.width * 0.012 , y: borderTopLbl, width: widthLbl, height: heightLbl))
        
          initializeLabel(label: &lblResume, text: "Play", frame:CGRect(x: btnResume.frame.minX - frame.width * 0.012 , y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblCursorBegin, text: "Sound", frame: CGRect(x: btnSoundOnOff.frame.minX - frame.width * 0.012, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblPerform, text: "Perform", frame: CGRect(x: btnPerform.frame.minX - frame.width * 0.006, y: borderTopLbl, width: widthLbl, height: heightLbl))
       
        initializeLabel(label: &lblCompare, text: "Compare", frame: CGRect(x:  btnCompare.frame.minX - frame.width * 0.012, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
         initializeLabel(label: &lblStatistic, text: "Statistic", frame: CGRect(x: btnStatistic.frame.minX - frame.width * 0.011, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
         initializeLabel(label: &lblQuantization, text: "Quantization", frame: CGRect(x:  btnQuantization.frame.minX - frame.width * 0.010, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblExit, text: "Home", frame: CGRect(x: btnExit.frame.minX - frame.width * 0.010, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
//        initializeLabel(label: &lblEdit, text: "Edit", frame: CGRect(x: frame.width * 0.97 - (width * 4.0), y: borderTopLbl, width: widthLbl, height: heightLbl))
//
//        initializeLabel(label: &lblSave, text: "Save", frame: CGRect(x: frame.width * 0.97 - (width * 3.0), y: borderTopLbl, width: widthLbl, height: heightLbl))
//
//        initializeLabel(label: &lblSend, text: "Send", frame: CGRect(x: frame.width * 0.97 - (width * 2.0), y: borderTopLbl, width: widthLbl, height: heightLbl))
        

        //-----------------------------------------------------------
        
        initStripeView(stripe: &stripeView, frame: CGRect(x: frame.width * 0.13, y: frame.height*0.11, width: frame.width*0.002, height: frame.height*0.777))
        initStripeViewBigger(stripe: &stripeView2, frame: CGRect(x:0, y: frame.height*0.985, width: frame.width, height: frame.height*0.01))
        initStripeView(stripe: &stripeView3, frame: CGRect(x: frame.width * 0.376, y: frame.height*0.11, width: frame.width*0.002, height: frame.height*0.777))
        initStripeView(stripe: &stripeView4, frame: CGRect(x: frame.width * 0.869, y: frame.height*0.11, width: frame.width*0.002, height: frame.height*0.777))
        
        enableButtonsByState()
        
        btnCompare.isEnabled = false
        btnQuantization.isEnabled = false
        btnStatistic.isEnabled = false
        lblCompare.isEnabled = false
        lblQuantization.isEnabled = false
        lblStatistic.isEnabled = false
    }

    /// Habilita os botões do menu de acordo com o estado do menu
    func enableButtonsByState() {
        switch state {
        case .popUpOpened:
            btnSettings.isEnabled   = true
            btnSoundOnOff.isEnabled = true
            btnResume.isEnabled  = true
            btnExit.isEnabled    = true
        case .save:
            btnSettings.isEnabled   = true
            btnSoundOnOff.isEnabled = true
            btnResume.isEnabled  = true
            btnExit.isEnabled    = true
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Bold", size: frame.height * 0.85)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)//UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    func initStripeViewBigger(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    deinit {
        btnSettings   = nil
        btnCursorBegin = nil
        btnResume  = nil
        btnExit    = nil
        
        lblSettings   = nil
        lblCursorBegin = nil
        lblResume  = nil
        lblExit    = nil
    }
    
    func enableButtons() {
        
        
        btnSettings.isEnabled   = true
        btnQuantization.isEnabled = true
        btnPerform.isEnabled = true
        btnCompare.isEnabled = true
        btnStatistic.isEnabled = true
        btnResume.isEnabled = true
     
        lblSettings.isEnabled   = true
        lblQuantization.isEnabled = true
        lblPerform.isEnabled = true
        lblCompare.isEnabled = true
        lblStatistic.isEnabled = true
        lblResume.isEnabled = true
        lblCursorBegin.isEnabled = true
        
    }
    
    func disableButtons() {
        btnSettings.isEnabled   = false
        btnQuantization.isEnabled = false
        btnPerform.isEnabled = false
        btnCompare.isEnabled = false
        btnStatistic.isEnabled = false
        btnResume.isEnabled = false
        btnSoundOnOff.isEnabled = false
        
        lblSettings.isEnabled   = false
        lblQuantization.isEnabled = false
        lblPerform.isEnabled = false
        lblCompare.isEnabled = false
        lblStatistic.isEnabled = false
        lblResume.isEnabled = false
        lblCursorBegin.isEnabled = false
    }
}

enum StateMenu {
    case popUpOpened
    case save
}

enum DirectionMenu {
    case horizontal
    case vertical
}
