//
//  MenuPerformance.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/18/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuPerformanceDelegate: class {
    func changeQuantization(_ value: Int)
}

class MenuPerformance: UIView {

    weak var delegate: MenuPerformanceDelegate?
    
    /// Botão de comparação
    weak var btnCompare      : UIButton!
    /// Botão de estatística
    weak var btnStatistic    : UIButton!
    /// Botão de quantização
    weak var btnQuantization : UIButton!
    
    /// Label de comparação
    weak var lblCompare      : UILabel!
    /// Label de estatística
    weak var lblStatistic    : UILabel!
    /// Label de quantização
    weak var lblQuantization : UILabel!
    
    /// Progress tocado
    weak var prgPlayed       : UIProgressView!
    /// Progress total
    weak var prgTotal        : UIProgressView!
    
    /// Label tocado
    weak var lblPlayed       : UILabel!
    /// Label total
    weak var lblTotal        : UILabel!
    
    var currentQuantization = 4 { didSet { quantizationChanged() } }
    
    override func draw(_ rect: CGRect) {
        let widthBtns      = rect.width * 0.18
        let heightBtns     = rect.height * 0.7
        let widthProgress  = rect.width * 0.3
        let heightProgress = rect.height * 0.4
        
        let widthLbl  = widthBtns * 1.2
        let heightLbl = rect.height * 0.15
        let deslocLbl = widthBtns * 0.1
        
        let border       = rect.width * 0.04
        let borderTop    = rect.height * 0.05
        let borderTopLbl = borderTop + heightBtns
        
        let rectBtnCompare   = CGRect(x: border, y: borderTop, width: widthBtns, height: heightBtns)
        let rectLblCompare   = CGRect(x: border - deslocLbl, y: borderTopLbl, width: widthLbl, height: heightLbl)
        let rectBtnStatistic = CGRect(x: (border * 2.0) + widthBtns, y: borderTop, width: widthBtns, height: heightBtns)
        let rectLblStatistic = CGRect(x: (border * 2.0) + widthBtns - deslocLbl, y: borderTopLbl, width: widthLbl, height: heightLbl)
        let x1 = (border * 3.0) + (widthBtns * 2.0)
        let rectLblPrgPlayed = CGRect(x: x1 - deslocLbl, y: borderTop * 3.0, width: widthLbl, height: heightLbl)
        let rectPrgPlayed    = CGRect(x: x1, y: (borderTop * 3.0) + (heightLbl * 1.3), width: widthProgress, height: heightProgress)
        let rectLblPrgTotal  = CGRect(x: x1 - deslocLbl, y: heightLbl + heightProgress, width: widthLbl, height: heightLbl)
        let rectPrgTotal     = CGRect(x: x1, y: (heightLbl * 2.3) + heightProgress, width: widthProgress, height: heightProgress)
        let x2 = (border * 3.0) + (widthBtns * 2.0) + widthProgress + border
        let rectBtnQuantization = CGRect(x: x2, y: borderTop, width: widthBtns, height: heightBtns)
        let rectLblQuantization = CGRect(x: x2 - deslocLbl, y: borderTopLbl, width: widthLbl, height: heightLbl)
        
        initButton(button: &btnCompare, image: #imageLiteral(resourceName: "btn_performance_compare"), frame: rectBtnCompare)
        initLabel(label: &lblCompare, text: "Compare", frame: rectLblCompare)
        initButton(button: &btnStatistic, image: #imageLiteral(resourceName: "btn_performance_statistic"), frame: rectBtnStatistic)
        initLabel(label: &lblStatistic, text: "Statistic", frame: rectLblStatistic)
        initLabel(label: &lblPlayed, text: "% of Played", frame: rectLblPrgPlayed)
        initProgress(progress: &prgPlayed, value: 0.5, frame: rectPrgPlayed)
        initLabel(label: &lblTotal, text: "% of Partitura", frame: rectLblPrgTotal)
        initProgress(progress: &prgTotal, value: 0.5, frame: rectPrgTotal)
        initButton(button: &btnQuantization, image: #imageLiteral(resourceName: "btn_performance_quantization_1_4"), frame: rectBtnQuantization)
        initLabel(label: &lblQuantization, text: "Quantization", frame: rectLblQuantization)
        
        btnQuantization.addTarget(self, action: #selector(actionBtnQuantization(_:)), for: .touchUpInside)
    }

    @objc private func actionBtnQuantization(_ sender: UIButton) {
        if currentQuantization == 4 {
            currentQuantization = 8
        } else if currentQuantization == 8 {
            currentQuantization = 16
        }else if currentQuantization == 16 {
            currentQuantization = 4
        }
        if let deleg = delegate {
            deleg.changeQuantization(currentQuantization)
        }
    }
    
    private func quantizationChanged() {
        switch currentQuantization {
        case 4:
            btnQuantization.setImage(#imageLiteral(resourceName: "btn_performance_quantization_1_4"), for: .normal)
        case 8:
            btnQuantization.setImage(#imageLiteral(resourceName: "btn_performance_quantization_1_8"), for: .normal)
        case 16:
            btnQuantization.setImage(#imageLiteral(resourceName: "btn_performance_quantization_1_16"), for: .normal)
        default:
            break
        }
    }
    
    func initProgress(progress: inout UIProgressView!, value: Float, frame: CGRect) {
        if progress == nil {
            let prg = UIProgressView(frame: frame)
            prg.progress = value
            prg.progressTintColor = UIColor.green
            prg.contentMode = .scaleAspectFit
            prg.transform = CGAffineTransform(scaleX: 1.0, y: 5.0)
            addSubview(prg)
            progress = prg
        } else {
            progress.frame = frame
        }
    }
    
    func initLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor.white
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
}
