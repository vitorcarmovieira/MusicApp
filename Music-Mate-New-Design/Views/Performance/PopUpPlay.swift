//
//  PopUpPlay.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/25/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import MediaPlayer

protocol PopUpPlayDelegate: class {
    func changeMetronome(_ value: Int)
    func changeKey(_ key: KeyType)
    func transpose(_ option: OptionKeySelected)
}

class PopUpPlay: UIView {

    weak var delegate: PopUpPlayDelegate?
    
 
    weak var btnClose       : UIButton!
    weak var btnPlusTenMetronome      : UIButton!
    weak var btnMinusTenMetronome      : UIButton!
    weak var btnPlusTranspose     : UIButton!
    weak var btnMinusTranspose      : UIButton!
    weak var btnPlusOctaveTranspose     : UIButton!
    weak var btnMinusOctaveTranspose      : UIButton!
    weak var btnDone                  : UIButton!
    weak var btnDefault                : UIButton!

    weak var stripeView : UIView!
    weak var stripeView1 : UIView!
    weak var stripeView2 : UIView!
    weak var stripeView3 : UIView!
    
    weak var lblSoundMetronome    : UILabel!
    weak var lblMinimizeCursor : UILabel!
    /// right e left hand
    weak var lblSoundLeft      : UILabel!
    weak var lblSoundRight     : UILabel!
    weak var swtSoundLeft      : UISwitch!
    weak var swtSoundRight     : UISwitch!

    weak var lbl34Channel      : UILabel!
    weak var lbl12Channel      : UILabel!
    weak var lblTranspose      : UILabel!
    weak var lblMetronomeTxt   : UILabel!
    
    weak var swtSoundCursor    : UISwitch!
    weak var swtMinimizeCursor : UISwitch!
    
    weak var btnStart : UIButton!
    weak var lblStart : UILabel!
    
    weak var lblMetronome           : UILabel!
    weak var metronomoCircleCompose : MHRotaryKnob!
    
    var metronomeValue : Int!
    
    var pickerMetronome     : UIPickerView!
    var pickerTranspose     : UIPickerView!
    
    var metronomeContent :[String] = [String]()
    var transposeContent : [String] = [String]()
   
    
    var widPickr: CGFloat  = 0.0
    var hegtPickr: CGFloat = 0.0
    
    override func draw(_ rect: CGRect) {
        metronomeContent = ["0","1","2","3","4","5","6","7","8","9"]
        transposeContent = ["C/Am","Db/Bbm","D/Bm","Eb/Cm","E/C#m","F/Dm","F#/D#m","G/Em","Ab/Fm","A/F#m","Bb/Gm","B/G#m"]
        let border = frame.width * 0.03
        
        /// botoes de left e right hand removidos, para volta-los, só descomenta-los e apagar todas as constantes " - frame.height * 0.14
        
        initializeButtonCancelDone(button: &btnDone, image: #imageLiteral(resourceName: "botao"), txt: "Done", frame: CGRect(x: frame.width * 0.2, y: frame.height * 0.834, width: frame.width*0.6, height: frame.height*0.04))
        initializeButtonCancelDone(button: &btnDefault, image: #imageLiteral(resourceName: "botao"), txt: "Default", frame: CGRect(x: frame.width * 0.2, y: frame.height * 0.9, width: frame.width*0.6, height: frame.height*0.04))
        
        initializeButton(button: &btnMinusTenMetronome, image: #imageLiteral(resourceName: "menos"), frame: CGRect(x: (border*3.0), y: frame.height * 0.5, width: frame.height*0.025, height: frame.height*0.025
        ))
        initializeButton(button: &btnPlusTenMetronome, image: #imageLiteral(resourceName: "mais"), frame: CGRect(x: frame.width*0.82, y: frame.height * 0.5, width: frame.height*0.025, height: frame.height*0.025
        ))
        
        initializeButton(button: &btnMinusOctaveTranspose, image: #imageLiteral(resourceName: "8vb"), frame:CGRect(x: (border*3.0), y: frame.height * 0.456 - frame.height * 0.14, width: frame.height*0.025, height: frame.height*0.025
        ))
        initializeButton(button: &btnPlusOctaveTranspose, image: #imageLiteral(resourceName: "8va"), frame:CGRect(x: frame.width*0.82, y: frame.height * 0.456 - frame.height * 0.14, width: frame.height*0.025, height: frame.height*0.025
        ))
        
//        initializeButton(button: &btnMinusTranspose, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_left"), frame: CGRect(x: (border*3.0), y: frame.height * 0.305, width: frame.height*0.025, height: frame.height*0.025
//        ))
//        initializeButton(button: &btnPlusTranspose, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_right"), frame: CGRect(x: frame.width*0.82, y: frame.height * 0.305, width: frame.height*0.025, height: frame.height*0.025
//        ))
        initStripeView(stripe: &stripeView1, frame: CGRect(x: 0, y: frame.height * 0.1, width: frame.width, height: frame.height*0.001))
        initStripeView(stripe: &stripeView2, frame: CGRect(x: 0, y: frame.height * 0.4, width: frame.width, height: frame.height*0.001))
        initStripeView(stripe: &stripeView3, frame: CGRect(x: 0, y: frame.height * 0.36  - frame.height * 0.14, width: frame.width, height: frame.height*0.001))

        initializeLabelBold(label: &lblSoundMetronome, text: "Sound Metronome", frame: CGRect(x: (border*3.0), y: frame.height * 0.005, width: frame.width, height: frame.height * 0.035))
        
        initializeLabelBold(label: &lblMinimizeCursor, text: "Minimize Cursor", frame: CGRect(x: (border*3.0), y: frame.height * 0.05, width: frame.width, height: frame.height * 0.035))
        
        //---------- Left hand e Right hand --------------------
        
        initializeLabelBold(label: &lblSoundLeft, text: "Sound Left Hand", frame: CGRect(x: (border*3.0), y: frame.height * 0.12, width: frame.width, height: frame.height * 0.035))
        
     //   initializeLabelSmall(label: &lbl34Channel, text: "3-4 Channel", frame: CGRect(x: (border*3.0), y: frame.height * 0.13, width: frame.width, height: frame.height * 0.035))
        
        initializeLabelBold(label: &lblSoundRight, text: "Sound Right Hand",frame: CGRect(x: (border*3.0), y: frame.height * 0.178, width: frame.width, height: frame.height * 0.035))
        
      //  initializeLabelSmall(label: &lbl12Channel, text: "1-2 Channel", frame: CGRect(x: (border*3.0), y: frame.height * 0.185, width: frame.width, height: frame.height * 0.035))
        
        initializeLabelBold(label: &lblTranspose, text: "Transpose", frame: CGRect(x: (border*3.0), y: frame.height * 0.376  - frame.height * 0.14, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblMetronomeTxt, text: "Metronome", frame: CGRect(x: (border*3.0), y: frame.height * 0.42, width: frame.width, height: frame.height * 0.035))
        initializeLabel(label: &lblMetronome, text: "\(metronomeValue)", frame: CGRect(x:
            frame.width*0.43, y: frame.height * 0.465, width: frame.height*0.04, height: frame.height*0.03
            ))
        
        
        pickerTranspose = UIPickerView(frame: CGRect(x: frame.width*0.445, y: frame.height * 0.4 - frame.height * 0.14, width: frame.height*0.03, height: frame.width*0.5))
        widPickr  = pickerTranspose.frame.width
        hegtPickr = pickerTranspose.frame.height
        
        pickerTranspose.dataSource = self
        pickerTranspose.delegate = self
        addSubview(pickerTranspose)
        pickerTranspose.backgroundColor = UIColor.white
        pickerTranspose.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerTranspose.layer.borderWidth = 1
        
        pickerTranspose.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
       
        pickerMetronome = UIPickerView(frame: CGRect(x: frame.width*0.445, y: frame.height * 0.445, width: frame.height*0.03, height: frame.width*0.5))
        pickerMetronome.dataSource = self
        pickerMetronome.delegate = self
        addSubview(pickerMetronome)
        pickerMetronome.backgroundColor = UIColor.white
        pickerMetronome.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerMetronome.layer.borderWidth = 1
        pickerMetronome.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        
//        pickerMetronome.selectRow(8, inComponent: 0, animated: false)
        
        let widthSwitch  = frame.width * 0.1
        let heightSwitch = frame.height * 0.4
        
        initializeSwitch(switchE: &swtSoundCursor, isOn: true, frame: CGRect(x: frame.width*0.77  , y: frame.height * 0.015, width: widthSwitch, height: heightSwitch))
        
        initializeSwitch(switchE: &swtMinimizeCursor, isOn: false, frame: CGRect(x: frame.width*0.77, y: frame.height * 0.06, width: widthSwitch, height: heightSwitch))
        
        
        ///Switch de right e left hand
        
        initializeSwitch(switchE: &swtSoundLeft, isOn: true, frame: CGRect(x: frame.width*0.77, y: frame.height * 0.125, width: widthSwitch, height: heightSwitch))

        initializeSwitch(switchE: &swtSoundRight, isOn: true, frame: CGRect(x: frame.width*0.77, y: frame.height * 0.185, width: widthSwitch, height: heightSwitch))
    
    }

    func nextTonality(){
        if(pickerTranspose.selectedRow(inComponent: 0).hashValue+1 < transposeContent.count){
            pickerTranspose.selectRow(pickerTranspose.selectedRow(inComponent: 0).hashValue+1, inComponent: 0, animated: true)
        }else {
            pickerTranspose.selectRow(0, inComponent: 0, animated: true)
        }
        switch pickerTranspose.selectedRow(inComponent: 0) {
        case 0:
            delegate?.transpose(OptionKeySelected.C)
        case 1:
            delegate?.transpose(OptionKeySelected.Db)
        case 2:
            delegate?.transpose(OptionKeySelected.D)
        case 3:
            delegate?.transpose(OptionKeySelected.Eb)
        case 4:
            delegate?.transpose(OptionKeySelected.E)
        case 5:
            delegate?.transpose(OptionKeySelected.F)
        case 6:
            delegate?.transpose(OptionKeySelected.Fs)
        case 7:
            delegate?.transpose(OptionKeySelected.G)
        case 8:
            delegate?.transpose(OptionKeySelected.Ab)
        case 9:
            delegate?.transpose(OptionKeySelected.A)
        case 10:
            delegate?.transpose(OptionKeySelected.Bb)
        case 11:
            delegate?.transpose(OptionKeySelected.B)
        default:
            break
        }
        
    }
    func previousTonality(){
        if(pickerTranspose.selectedRow(inComponent: 0).hashValue-1 > 0){
            pickerTranspose.selectRow(pickerTranspose.selectedRow(inComponent: 0).hashValue-1, inComponent: 0, animated: true)
        }else {
            pickerTranspose.selectRow(transposeContent.count-1, inComponent: 0, animated: true)
        }
        switch pickerTranspose.selectedRow(inComponent: 0) {
        case 0:
            delegate?.transpose(OptionKeySelected.C)
        case 1:
            delegate?.transpose(OptionKeySelected.Db)
        case 2:
            delegate?.transpose(OptionKeySelected.D)
        case 3:
            delegate?.transpose(OptionKeySelected.Eb)
        case 4:
            delegate?.transpose(OptionKeySelected.E)
        case 5:
            delegate?.transpose(OptionKeySelected.F)
        case 6:
            delegate?.transpose(OptionKeySelected.Fs)
        case 7:
            delegate?.transpose(OptionKeySelected.G)
        case 8:
            delegate?.transpose(OptionKeySelected.Ab)
        case 9:
            delegate?.transpose(OptionKeySelected.A)
        case 10:
            delegate?.transpose(OptionKeySelected.Bb)
        case 11:
            delegate?.transpose(OptionKeySelected.B)
        default:
            break
        }
    }
   
    func setInitialTonality(tom : Int){
        switch tom {
        case 1:
             pickerTranspose.selectRow(0, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.C)
        case 2:
             pickerTranspose.selectRow(1, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Db)
        case 3:
             pickerTranspose.selectRow(1, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Db)
        case 4:
             pickerTranspose.selectRow(2, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.D)
        case 5:
             pickerTranspose.selectRow(3, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Eb)
        case 6:
             pickerTranspose.selectRow(4, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.E)
        case 7:
             pickerTranspose.selectRow(5, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.F)
        case 8:
             pickerTranspose.selectRow(6, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Fs)
        case 9:
             pickerTranspose.selectRow(6, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Fs)
        case 10:
             pickerTranspose.selectRow(7, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.G)
        case 11:
             pickerTranspose.selectRow(8, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Ab)
        case 12:
             pickerTranspose.selectRow(9, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.A)
        case 13:
             pickerTranspose.selectRow(10, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.Bb)
        case 14:
             pickerTranspose.selectRow(11, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.B)
        default:
             pickerTranspose.selectRow(0, inComponent: 0, animated: false)
             delegate?.transpose(OptionKeySelected.C)
        }
    }
    
    func setMetronome(metronome: Int) {
        lblMetronome.text = "\(metronome)"
        metronomeValue = metronome
        var unidade = 0
        unidade = metronome-Int(metronome/10)*10
        for i in 0..<metronomeContent.count {
            if(Int(metronomeContent[i])==unidade){
                self.pickerMetronome.selectRow(unidade, inComponent: 0, animated: false)
                break
            }
        }
        if let deleg = self.delegate {
            deleg.changeMetronome(metronomeValue)
        }
    }

    func initializeImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let btn = UIImageView(frame: frame)
            btn.image = image
            addSubview(btn)
            
            imageView = btn
        } else {
            imageView.frame = frame
        }
    }
    
    func initializeButtonCancelDone(button: inout UIButton!, image: UIImage, txt : String, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
          //  btn.setImage(image, for: .normal)
            btn.setBackgroundImage(image, for: .normal)
            btn.setTitle(txt, for: .normal)
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: frame.height * 0.5)
            btn.titleLabel?.textColor = UIColor.white
            btn.titleLabel?.textAlignment = .center
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.center
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    
    
    func initializeLabelSmall(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.left) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.3)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Bold", size: frame.height * 0.6)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeLabelBold(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.left) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.5)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    
    func initializeSwitch(switchE: inout UISwitch!, isOn: Bool, frame: CGRect) {
        if switchE == nil {
            let swt = UISwitch(frame: frame)
            swt.isOn = isOn
            addSubview(swt)
            switchE = swt
        } else {
            switchE.frame = frame
        }
    }

    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    deinit {
        print("Entrou deinit PopUpPlay")
    }
}

extension PopUpPlay: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerMetronome{
            return metronomeContent.count
        }
        if pickerView == pickerTranspose{
            return transposeContent.count
        }
        return 0
    }
   
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerMetronome {
            lblMetronome.text = "\(Int(metronomeValue/10))\(metronomeContent[row])"
            metronomeValue = Int(lblMetronome.text!)
            setMetronome(metronome: metronomeValue)
        }
        
        if pickerView == pickerTranspose {
            print("ROW: ",row)
            switch row {
            case 0:
                delegate?.transpose(OptionKeySelected.C)
            case 1:
                 delegate?.transpose(OptionKeySelected.Db)
            case 2:
                 delegate?.transpose(OptionKeySelected.D)
            case 3:
                 delegate?.transpose(OptionKeySelected.Eb)
            case 4:
                 delegate?.transpose(OptionKeySelected.E)
            case 5:
                 delegate?.transpose(OptionKeySelected.F)
            case 6:
                 delegate?.transpose(OptionKeySelected.Fs)
            case 7:
                 delegate?.transpose(OptionKeySelected.G)
            case 8:
                 delegate?.transpose(OptionKeySelected.Ab)
            case 9:
                 delegate?.transpose(OptionKeySelected.A)
            case 10:
                 delegate?.transpose(OptionKeySelected.Bb)
            case 11:
                 delegate?.transpose(OptionKeySelected.B)
            default:
                break
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if pickerView == pickerMetronome{
            return frame.height*0.03
        }
        return frame.height*0.1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        if(pickerView == pickerMetronome){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
//        let imagem = UIImageView()
//        imagem.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        imagem.contentMode = .scaleAspectFit
//        imagem.image = pickerDataSource[row]
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
            label.textAlignment = .center
            label.textColor = UIColor(red: 71/255, green: 74/255, blue: 81/255, alpha: 1.0)
            label.font = UIFont(name: "Lato-Regular", size: frame.width*0.08)
            label.text = metronomeContent[row]
            view1.addSubview(label)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        if(pickerView == pickerTranspose){
            let view2 = UIView()
            view2.frame = CGRect(x: 0, y: 0, width: hegtPickr, height: widPickr)
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: hegtPickr, height: widPickr)
            label.textAlignment = .center
            label.textColor = UIColor(red: 71/255, green: 74/255, blue: 81/255, alpha: 1.0)
            label.font = UIFont(name: "Lato-Regular", size: frame.width*0.08)
            label.text = transposeContent[row]
            view2.addSubview(label)
            view2.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view2
        }
        return view
    }

}
enum TimeSelected {
    case doisdois
    case tresdois
    case doisQuartos
    case tresQuartos
    case quatroQuartos
    case cincoQuartos
    case seisQuartos
    case tresOitavos
    case seisOitavos
    case noveOitavos
    case dozeOitavos
}

