//
//  SettingsView.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 02/03/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class SettingsView: UIView {

    
    weak var imgProfile : UIImageView!
   
    
  
    
    override func draw(_ rect: CGRect) {
        
       
    }
    
  
    
    func initImageProfile(imageView: inout UIImageView!, frame: CGRect, image: UIImage){
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            self.addSubview(img)
            imageView = img
        }
        
    }
}

