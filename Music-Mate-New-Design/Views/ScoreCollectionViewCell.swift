//
//  ScoreCollectionViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 09/03/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class ScoreCollectionViewCell: UICollectionViewCell {
    var pageView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        addSubview(view)
        
        pageView = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
