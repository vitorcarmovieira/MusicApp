//
//  MenuNotes.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/28/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuNotesViewDelegate: class {
    func divideNote()
    func divideNoteAndPutNoteMorePause()
    func divideNoteAndPutPauseMoreNote()
    func deleteNoteAndExtendAfter()
    func deleteNoteAndExtendBefore()
    func deleteNoteAndReplaceWithPause()
    func deleteNoteAndExtendNoteBefore()
}

class MenuNotes: UIView {
    
    weak var delegate: MenuNotesViewDelegate?
    
    /// Botão dividir
    weak var btnDivide      : UIButton!
    /// Botão nota + pausa
    weak var btnNotePause   : UIButton!
    /// Botão pausa + nota
    weak var btnPauseNote   : UIButton!
    /// Botão nota prolongada + nota
    weak var btnProlongNote : UIButton!
    /// Botão nota nota + prolongada
    weak var btnNoteProlong : UIButton!
    /// Botão deleta pausa
    weak var btnDelPause    : UIButton!
    /// Botão deleta note
    weak var btnDelNote     : UIButton!
    
    
    override func draw(_ rect: CGRect) {
        /// Comprimento dos botões superiores do menu
        let width1 = rect.width / 3.0
        /// Comprimento dos demais botões do menu
        let width2 = rect.width / 2.0
        /// Largura dos botões
        let height = rect.height / 3.0
        
        /// Retângulo do primeiro botão
        let rect1 = CGRect(x: (width1 * 0), y: (height * 0), width: width1, height: height)
        /// Retângulo do segundo botão
        let rect2 = CGRect(x: (width1 * 1), y: (height * 0), width: width1, height: height)
        /// Retângulo do terceiro botão
        let rect3 = CGRect(x: (width1 * 2), y: (height * 0), width: width1, height: height)
        /// Retângulo do quarto botão
        let rect4 = CGRect(x: (width2 * 0), y: (height * 1), width: width2, height: height)
        /// Retângulo do quinto botão
        let rect5 = CGRect(x: (width2 * 1), y: (height * 1), width: width2, height: height)
        /// Retângulo do sexto botão
        let rect6 = CGRect(x: (width2 * 0), y: (height * 2), width: width2, height: height)
        /// Retângulo do sétimo botão
        let rect7 = CGRect(x: (width2 * 1), y: (height * 2), width: width2, height: height)
        
        initializeButton(button: &btnDivide, image: #imageLiteral(resourceName: "btn_menu_edit_notes_duplicate"), frame: rect1)
        initializeButton(button: &btnNotePause, image: #imageLiteral(resourceName: "btn_menu_edit_notes_divide_pause"), frame: rect2)
        initializeButton(button: &btnPauseNote, image: #imageLiteral(resourceName: "btn_menu_edit_notes_pause_div"), frame: rect3)
        initializeButton(button: &btnProlongNote, image: #imageLiteral(resourceName: "btn_menu_edit_notes_prolong_div"), frame: rect4)
        initializeButton(button: &btnNoteProlong, image: #imageLiteral(resourceName: "btn_menu_edit_notes_divide_prolong"), frame: rect5)
        initializeButton(button: &btnDelPause, image: #imageLiteral(resourceName: "btn_menu_edit_notes_delete_pause"), frame: rect6)
        initializeButton(button: &btnDelNote, image: #imageLiteral(resourceName: "btn_menu_edit_notes_delete_prolong"), frame: rect7)
        
        btnDivide.addTarget(self, action: #selector(actionBtnDivide(sender:)), for: UIControlEvents.touchUpInside)
        btnNotePause.addTarget(self, action: #selector(actionBtnNotePause(sender:)), for: UIControlEvents.touchUpInside)
        btnPauseNote.addTarget(self, action: #selector(actionBtnPauseNote(sender:)), for: UIControlEvents.touchUpInside)
        btnProlongNote.addTarget(self, action: #selector(actionBtnProlongNote(sender:)), for: UIControlEvents.touchUpInside)
        btnNoteProlong.addTarget(self, action: #selector(actionBtnNoteProlong(sender:)), for: UIControlEvents.touchUpInside)
        btnDelPause.addTarget(self, action: #selector(actionBtnDelPause(sender:)), for: UIControlEvents.touchUpInside)
        btnDelNote.addTarget(self, action: #selector(actionBtnDelNote(sender:)), for: UIControlEvents.touchUpInside)
    }

    @objc func actionBtnDivide(sender: UIButton) {
        if let deleg = delegate {
            deleg.divideNote()
        }
    }
    
    @objc func actionBtnNotePause(sender: UIButton) {
        if let deleg = delegate {
            deleg.divideNoteAndPutNoteMorePause()
        }
    }
    
    @objc func actionBtnPauseNote(sender: UIButton) {
        if let deleg = delegate {
            deleg.divideNoteAndPutPauseMoreNote()
        }
    }
    
    @objc func actionBtnProlongNote(sender: UIButton) {
        if let deleg = delegate {
            deleg.deleteNoteAndExtendBefore()
        }
    }
    
    @objc func actionBtnNoteProlong(sender: UIButton) {
        if let deleg = delegate {
            deleg.deleteNoteAndExtendAfter()
        }
    }
    
    @objc func actionBtnDelPause(sender: UIButton) {
        if let deleg = delegate {
            deleg.deleteNoteAndReplaceWithPause()
        }
    }
    
    @objc func actionBtnDelNote(sender: UIButton) {
        if let deleg = delegate {
            deleg.deleteNoteAndExtendNoteBefore()
        }
    }
    
    
    /// Inicializa um botão do menu
    ///
    /// - Parameters:
    ///   - button: Instância de UIButton
    ///   - image: Imagem do botão
    ///   - frame: Frame do botão
    private func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
}
