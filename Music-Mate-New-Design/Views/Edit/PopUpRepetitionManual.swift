//
//  PopUpRepetitionManual.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/1/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol PopUpRepetitionManualDelegate: class {
    func addRepetition(size: Int, bracket: Int)
}

class PopUpRepetitionManual: UIView, UITextFieldDelegate {

    weak var delegate: PopUpRepetitionManualDelegate?
    
    /// Label numero 1
    weak var lblNumber1      : UITextField!
    
    /// Label numero 2
    weak var lblNumber2      : UITextField!
    
    /// Label numero 2
    weak var lblMore      : UILabel!
    
    override func draw(_ rect: CGRect) {
        layer.borderWidth = 1.5
        layer.borderColor = UIColor.init(red:151/255.0, green:151/255.0, blue:151/255.0, alpha: 1.0).cgColor
        layer.cornerRadius = 5.0
        
        lblNumber1 = UITextField(frame: CGRect(x: 0, y: 0, width: frame.width * 0.45, height: frame.height))
        lblNumber1.textAlignment = NSTextAlignment.center
        lblNumber1.delegate = self
        lblNumber1.returnKeyType = .next
        lblNumber1.keyboardType = .decimalPad
        
        lblNumber2 = UITextField(frame: CGRect(x: frame.width * 0.55, y: 0, width: frame.width * 0.45, height: frame.height))
        lblNumber2.textAlignment = NSTextAlignment.center
        lblNumber2.delegate = self
        lblNumber2.keyboardType = .decimalPad
        lblNumber2.returnKeyType = .done
        
        addSubview(lblNumber1)
        addSubview(lblNumber2)
        
        initializeLabel(label: &lblMore, text: "+", frame: CGRect(x: frame.width * 0.45, y: 0, width: frame.width * 0.1, height: frame.height))
    }

    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor.black
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.4)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == lblNumber1 {
            lblNumber1.resignFirstResponder()
            lblNumber2.becomeFirstResponder()
        }
        
        if textField.returnKeyType == .done {
            if let deleg = delegate {
                if let size = Int(lblNumber1.text!) {
                    if let bracket = Int(lblNumber2.text!) {
                        if lblNumber2.text == ""{
                            lblNumber2.text = "0"
                        }
                        deleg.addRepetition(size: size, bracket: bracket)
                        self.endEditing(true)
                        return false
                    }
                }
            }
        }
        return true
    }
}
