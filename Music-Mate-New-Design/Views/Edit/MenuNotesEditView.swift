//
//  MenuNotesEditView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuNotesEditViewDelegate: class {
    func changeOption(_ option: OptionNotesSelected)
}

class MenuNotesEditView: UIView {

    weak var delegate: MenuNotesEditViewDelegate?
    
    let items = [ #imageLiteral(resourceName: "btn_menu_edit_notes_whole"), #imageLiteral(resourceName: "btn_menu_edit_notes_half"), #imageLiteral(resourceName: "btn_menu_edit_notes_quarter"), #imageLiteral(resourceName: "btn_menu_edit_notes_quaver"), #imageLiteral(resourceName: "btn_menu_edit_notes_semiquaver"), #imageLiteral(resourceName: "btn_menu_edit_notes_flat"), #imageLiteral(resourceName: "btn_menu_edit_notes_sharp"), #imageLiteral(resourceName: "btn_menu_edit_notes_natural"), #imageLiteral(resourceName: "btn_menu_edit_notes_pause_whole"), #imageLiteral(resourceName: "btn_menu_edit_notes_pause_half"), #imageLiteral(resourceName: "btn_menu_edit_notes_pause_quarter"), #imageLiteral(resourceName: "btn_menu_edit_notes_pause_quarver"), #imageLiteral(resourceName: "btn_menu_edit_notes_pause_semiquarver") ]
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(frame.height * 0.2, 0.0, 0.0, 0.0)
        flow.scrollDirection = .horizontal
        return flow
    }()
    
    lazy var collectionView:UICollectionView = {
        var cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(NotesTypeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(collectionView)
    }
    
}

extension MenuNotesEditView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width:CGFloat = frame.height
        
        let height:CGFloat = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! NotesTypeCollectionViewCell
        cell.setCardImage(image: items[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! NotesTypeCollectionViewCell
//
////        cell.onSelected(true)
        
        let cell = collectionView.cellForItem(at: indexPath) as? NotesTypeCollectionViewCell
        if (cell?.cellSelected)! {
            cell?.onSelected(false)
            delegate?.changeOption(.cancel)
            
            return
        } else {
            cell?.onSelected(true)
        }
        
        switch index {
        case 0:
            delegate?.changeOption(.whole)
        case 1:
            delegate?.changeOption(.half)
        case 2:
            delegate?.changeOption(.quarter)
        case 3:
            delegate?.changeOption(.quaver)
        case 4:
            delegate?.changeOption(.semiquaver)
        case 5:
            delegate?.changeOption(.flat)
        case 6:
            delegate?.changeOption(.sharp)
        case 7:
            delegate?.changeOption(.natural)
        case 8:
            delegate?.changeOption(.pausewhole)
        case 9:
            delegate?.changeOption(.pausehalf)
        case 10:
            delegate?.changeOption(.pausequarter)
        case 11:
            delegate?.changeOption(.pausequaver)
        case 12:
            delegate?.changeOption(.pausesemiquaver)
        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? NotesTypeCollectionViewCell
        cell?.onSelected(false)
    }
    
}

enum OptionNotesSelected {
    case whole
    case half
    case quarter
    case quaver
    case semiquaver
    case flat
    case sharp
    case natural
    case pausewhole
    case pausehalf
    case pausequarter
    case pausequaver
    case pausesemiquaver
    case cancel
}
