//
//  MenuTonesNotes.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 11/28/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuTonesNotesViewDelegate: class {
    func changeTone(_ newTone: Int)
}

class MenuTonesNotes: UIView {

    weak var delegate: MenuTonesNotesViewDelegate?
    
    var pickerTone: UIPickerView = UIPickerView()
    
    var tones = [ "C0", "D0", "E0", "F0", "G0", "A0", "B0",
                  "C1", "D1", "E1", "F1", "G1", "A1", "B1",
                  "C2", "D2", "E2", "F2", "G2", "A2", "B2",
                  "C3", "D3", "E3", "F3", "G3", "A3", "B3",
                  "C4", "D4", "E4", "F4", "G4", "A4", "B4",
                  "C5", "D5", "E5", "F5", "G5", "A5", "B5",
                  "C6", "D6", "E6", "F6", "G6", "A6", "B6",
                  "C7", "D7", "E7", "F7", "G7", "A7", "B7",
                  "C8", "D8", "E8", "F8", "G8", "A8", "B8",
                  ]
    
    override func draw(_ rect: CGRect) {
        pickerTone.frame = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
        pickerTone.dataSource      = self
        pickerTone.delegate        = self
        pickerTone.showsSelectionIndicator = false
        addSubview(pickerTone)
    }

}

extension MenuTonesNotes: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return tones[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return frame.width
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return frame.width * 0.95
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.width))
        v.backgroundColor = UIColor.white
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.width))
        lbl.text = tones[row]
        lbl.textAlignment = NSTextAlignment.center
//        lbl.textColor = UIColor(red: 151/255.0, green: 151/255.0, blue: 151/255.0, alpha: 1.0)
        
        v.layer.borderWidth = 1.5
        v.layer.borderColor = UIColor.init(red:151/255.0, green:151/255.0, blue:151/255.0, alpha: 1.0).cgColor
        
        v.addSubview(lbl)
        
        return v
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            delegate?.changeTone(12)
        case 1:
            delegate?.changeTone(14)
        case 2:
            delegate?.changeTone(16)
        case 3:
            delegate?.changeTone(17)
        case 4:
            delegate?.changeTone(19)
        case 5:
            delegate?.changeTone(21)
        case 6:
            delegate?.changeTone(23)
        case 7:
            delegate?.changeTone(24)
        case 8:
            delegate?.changeTone(26)
        case 9:
            delegate?.changeTone(28)
        case 10:
            delegate?.changeTone(29)
        case 11:
            delegate?.changeTone(31)
        case 12:
            delegate?.changeTone(33)
        case 13:
            delegate?.changeTone(35)
        case 14:
            delegate?.changeTone(36)
        case 15:
            delegate?.changeTone(38)
        case 16:
            delegate?.changeTone(40)
        case 17:
            delegate?.changeTone(41)
        case 18:
            delegate?.changeTone(43)
        case 19:
            delegate?.changeTone(45)
        case 20:
            delegate?.changeTone(47)
        case 21:
            delegate?.changeTone(48)
        case 22:
            delegate?.changeTone(50)
        case 23:
            delegate?.changeTone(52)
        case 24:
            delegate?.changeTone(53)
        case 25:
            delegate?.changeTone(55)
        case 26:
            delegate?.changeTone(57)
        case 27:
            delegate?.changeTone(59)
        case 28:
            delegate?.changeTone(60)
        case 29:
            delegate?.changeTone(62)
        case 30:
            delegate?.changeTone(64)
        case 31:
            delegate?.changeTone(65)
        case 32:
            delegate?.changeTone(67)
        case 33:
            delegate?.changeTone(69)
        case 34:
            delegate?.changeTone(71)
        case 35:
            delegate?.changeTone(72)
        case 36:
            delegate?.changeTone(74)
        case 37:
            delegate?.changeTone(76)
        case 38:
            delegate?.changeTone(77)
        case 39:
            delegate?.changeTone(79)
        case 40:
            delegate?.changeTone(81)
        case 41:
            delegate?.changeTone(83)
        case 42:
            delegate?.changeTone(84)
        case 43:
            delegate?.changeTone(86)
        case 44:
            delegate?.changeTone(88)
        case 45:
            delegate?.changeTone(89)
        case 46:
            delegate?.changeTone(91)
        case 47:
            delegate?.changeTone(93)
        case 48:
            delegate?.changeTone(95)
        case 49:
            delegate?.changeTone(96)
        case 50:
            delegate?.changeTone(98)
        case 51:
            delegate?.changeTone(100)
        case 52:
            delegate?.changeTone(101)
        case 53:
            delegate?.changeTone(103)
        case 54:
            delegate?.changeTone(105)
        case 55:
            delegate?.changeTone(107)
        case 56:
            delegate?.changeTone(108)
        case 57:
            delegate?.changeTone(110)
        case 58:
            delegate?.changeTone(112)
        case 59:
            delegate?.changeTone(113)
        case 60:
            delegate?.changeTone(115)
        case 61:
            delegate?.changeTone(117)
        case 62:
            delegate?.changeTone(119)
        default:
            break
        }
    }
}
