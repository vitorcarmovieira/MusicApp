//
//  MenuRepetitionView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import Appsee

protocol MenuRepetitionViewDelegate: class {
    func changeOption(_ option: OptionRepetitionSelected)
}

class MenuRepetitionView: UIView {

    weak var delegate: MenuRepetitionViewDelegate?
    
    let items = [ #imageLiteral(resourceName: "btn_menu_edit_ritornello_8"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_71"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_62"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_16"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_151"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_142"), #imageLiteral(resourceName: "btn_menu_edit_ritornello_manual"), #imageLiteral(resourceName: "delete_repet"), #imageLiteral(resourceName: "delete_bar")]
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(frame.height * 0.2, frame.width * 0.045, 0.0, 0.0)
        flow.scrollDirection = .horizontal
        return flow
    }()
    
    lazy var collectionView:UICollectionView = {
        var cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(RepetitionTypeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(collectionView)
    }
    
}

extension MenuRepetitionView:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width:CGFloat = frame.height * 1.2
        
        let height:CGFloat = width / 1.2
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! RepetitionTypeCollectionViewCell
        cell.setCardImage(image: items[indexPath.row])
        

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! RepetitionTypeCollectionViewCell
        cell.onSelected(true)
        
        switch index {
        case 0:
            delegate?.changeOption(.oito)
            Appsee.addScreenAction("Clicked Repetition 8")
        case 1:
            delegate?.changeOption(.seteum)
            Appsee.addScreenAction("Clicked Repetition 7+1")
        case 2:
            delegate?.changeOption(.seisdois)
            Appsee.addScreenAction("Clicked Repetition 6+2")
        case 3:
            delegate?.changeOption(.desesseis)
            Appsee.addScreenAction("Clicked Repetition 16")
        case 4:
            delegate?.changeOption(.quinzeum)
            Appsee.addScreenAction("Clicked Repetition 15+1")
        case 5:
            delegate?.changeOption(.quatorzedois)
            Appsee.addScreenAction("Clicked Repetition 14+2")
        case 6:
            delegate?.changeOption(.manual)
            Appsee.addScreenAction("Clicked Repetition Manual")
        case 7:
            Appsee.addScreenAction("Clicked Delete Repetition")
            delegate?.changeOption(.delete)
        case 8:
            Appsee.addScreenAction("Clicked Delete Bar")
            delegate?.changeOption(.deleteBar)
        case 9:
            delegate?.changeOption(.endBar)
        default:
            break
        }
    }


    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? RepetitionTypeCollectionViewCell
        cell?.onSelected(false)
    }
    
}

enum OptionRepetitionSelected {
    case oito
    case seteum
    case seisdois
    case desesseis
    case quinzeum
    case quatorzedois
    case manual
    case delete
    case deleteBar
    case endBar
}
