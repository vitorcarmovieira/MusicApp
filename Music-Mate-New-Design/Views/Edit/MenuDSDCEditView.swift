//
//  MenuDSDCEditView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import Appsee

protocol MenuDSDCEditViewDelegate: class {
    func changeOption(_ option: OptionDSDCSelected)
}

class MenuDSDCEditView: UIView {

    weak var delegate: MenuDSDCEditViewDelegate?
    
    let items = [ #imageLiteral(resourceName: "btn_menu_edit_dsdc_segno"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_coda"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_toCoda"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_fine"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_dsAlFine"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_dsAlCoda"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_dcAlFine"), #imageLiteral(resourceName: "btn_menu_edit_dsdc_dcAlCoda"), #imageLiteral(resourceName: "edit_delete") ]
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(frame.height * 0.2, frame.width * 0.045, 0.0, 0.0)
        flow.scrollDirection = .horizontal
        return flow
    }()
    
    lazy var collectionView:UICollectionView = {
        var cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(DSDCTypeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(collectionView)
    }
    
}

extension MenuDSDCEditView:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width:CGFloat = frame.height * 1.2
        
        let height:CGFloat = width / 1.2
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! DSDCTypeCollectionViewCell
        cell.setCardImage(image: items[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! DSDCTypeCollectionViewCell
        cell.onSelected(true)
        
        switch index {
        case 0:
            Appsee.addScreenAction("Clicked Add Segno")
            delegate?.changeOption(.segno)
        case 1:
            Appsee.addScreenAction("Clicked Add Coda")
            delegate?.changeOption(.coda)
        case 2:
            Appsee.addScreenAction("Clicked Add To Coda")
            delegate?.changeOption(.toCoda)
        case 3:
            Appsee.addScreenAction("Clicked Add Fine")
            delegate?.changeOption(.fine)
        case 4:
            Appsee.addScreenAction("Clicked Add D.S. Al Fine")
            delegate?.changeOption(.dsAlFine)
        case 5:
            Appsee.addScreenAction("Clicked Add D.S. Al Coda")
            delegate?.changeOption(.dsAlCoda)
        case 6:
            Appsee.addScreenAction("Clicked Add D.C. Al Fine")
            delegate?.changeOption(.dcAlFine)
        case 7:
            Appsee.addScreenAction("Clicked Add D.C. Al Coda")
            delegate?.changeOption(.dcAlCoda)
        case 8:
            Appsee.addScreenAction("Clicked Delete Ornament")
            delegate?.changeOption(.delete)
        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? DSDCTypeCollectionViewCell
        cell?.onSelected(false)
    }
    
}

enum OptionDSDCSelected {
    case segno
    case coda
    case toCoda
    case fine
    case dsAlFine
    case dsAlCoda
    case dcAlFine
    case dcAlCoda
    case delete
}
