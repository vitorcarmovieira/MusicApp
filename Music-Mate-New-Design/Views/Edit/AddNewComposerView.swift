//
//  AddNewComposerView.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 28/05/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class AddNewComposerView: UIView {
    
    weak var firstNameLbl : UILabel!
    weak var lastNameLbl : UILabel!
    
    weak var firstNameTxtF : UITextField!
    weak var lastNameTxtF : UITextField!
    
    weak var cancelBtn : UIButton!
    weak var addBtn : UIButton!
    
    
    
    override func draw(_ rect: CGRect) {
        self.initializeLabel(label: &self.firstNameLbl, text: "First Name", frame: CGRect(x: rect.width*0.1, y: rect.height*0.2, width: rect.width*0.3, height: rect.height*0.1), prop: 1.0)
        
        self.initializeLabel(label: &self.lastNameLbl, text: "Last Name", frame: CGRect(x: rect.width*0.1, y: rect.height*0.5, width: rect.width*0.3, height: rect.height*0.1), prop: 1.0)
        
        self.initializeButton(button: &cancelBtn, text: "Cancel", frame: CGRect(x: rect.width*0.4, y: rect.height*0.75, width: rect.width*0.2, height: rect.height*0.14))
        
        self.initializeTextField(field: &firstNameTxtF, text: "First Name", frame: CGRect(x: rect.width*0.4, y: rect.height*0.18, width: rect.width*0.5, height: rect.height*0.15) )
        
        self.initializeTextField(field: &lastNameTxtF, text: "Last Name", frame: CGRect(x: rect.width*0.4, y: rect.height*0.48, width: rect.width*0.5, height: rect.height*0.15) )
        
        self.initializeButton(button: &addBtn, text: "Add", frame: CGRect(x: rect.width*0.7, y: rect.height*0.75, width: rect.width*0.2, height: rect.height*0.14))
       
        
        self.firstNameTxtF.delegate = self
        self.lastNameTxtF.delegate = self
        
        self.cancelBtn.addTarget(self, action: #selector(actionBtnCancel(sender:)), for: .touchUpInside)
        self.addBtn.addTarget(self, action: #selector(actionBtnAdd(sender:)), for: .touchUpInside)
     
    }
    
    @objc func actionBtnAdd(sender: UIButton) {
        let user = UserManager.sharedUserManager
        let token = user.token
        user.server = "http://37.208.116.28"
        serviceAddNewAuthor(urlToRequest: user.server + ":3003/composer", method: ServiceRemote.Method.POST.rawValue, token: token)

    }
    
    @objc func actionBtnCancel(sender: UIButton) {
        self.firstNameTxtF.text = ""
        self.lastNameTxtF.text = ""
        self.isHidden = true
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect, prop: CGFloat, align: NSTextAlignment = NSTextAlignment.left) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = align
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * prop)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, text: String, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setTitle(text, for: .normal)
            btn.layer.cornerRadius = 15
            btn.layer.borderWidth = 1
            btn.layer.borderColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0).cgColor
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            btn.contentHorizontalAlignment = .center
            btn.backgroundColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            btn.titleLabel?.numberOfLines = 1
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: self.frame.height*0.05)
            btn.setTitleColor(UIColor.white, for: .normal)
            btn.titleLabel?.textAlignment = .center
            btn.layer.shadowColor = UIColor.black.cgColor
            btn.layer.shadowOffset = CGSize(width: 0, height: 2)
            btn.layer.shadowOpacity = 0.8
            btn.layer.shadowRadius = 3
            
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initializeTextField(field: inout UITextField!, text: String, frame: CGRect) {
        if field == nil {
            let f = UITextField(frame: frame)
            f.backgroundColor = UIColor.white
            f.placeholder = text
            f.keyboardType = .default
            f.returnKeyType = .done
            f.borderStyle = .roundedRect
            f.textColor = UIColor.black
            
            f.isEnabled = true
            f.leftViewMode = .always
            f.delegate = self
            addSubview(f)
            field = f
        } else {
            field.frame = frame
        }
    }
    @objc func txtTitleDidChange(textField: UITextField) {
        if(textField == firstNameTxtF){
            print("first")
        }
        else if(textField == lastNameTxtF){
            print("last")
        }
    }
    
    
}

extension AddNewComposerView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        becomeFirstResponder()
        
    
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
      
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
    
extension AddNewComposerView {
    func serviceAddNewAuthor(urlToRequest: String, method: String, token:String) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let lastEditionDate = formatter.string(from: date)
        
        var dictAuthor = Dictionary<String, Any>()
        dictAuthor["firstName"] = self.firstNameTxtF.text
        dictAuthor["lastName"] = self.lastNameTxtF.text
        dictAuthor["lastEditionDate"] = "2018-05-10"
        
        
        let postString : [String : Any] = ["token":token ,"composer": dictAuthor ]
        
        
        var request = URLRequest(url:URL(string:urlToRequest)!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application-idValue", forHTTPHeaderField: "secret-key")
        request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
        let session = URLSession.shared
                //Post
        session.dataTask(with: request){data, response, err in
                    //Guard: ws there error ?
        guard(err == nil) else {
            print("\(String(describing: err))")
            return
        }
                    //Guard: check was any data returned?
        guard let data = data else{
            print("no data return")
            return
        }
                    
        let parseResult: [String:AnyObject]!
        do{//
            parseResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
            
            print("parse: ",parseResult["status"])
           
            } catch {
                        print("Could not parse data as Json \(data)")
                        return
                    }
                    
                    }.resume()
            }
            
        }
