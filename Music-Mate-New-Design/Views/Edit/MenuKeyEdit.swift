//
//  MenuKeyEdit.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuKeyEditDelegate: class {
    func changeOption(_ option: OptionKeySelected)
}

class MenuKeyEdit: UIView {

    weak var delegate: MenuKeyEditDelegate?
    
    let items = [ #imageLiteral(resourceName: "btn_menu_edit_key_F"), #imageLiteral(resourceName: "btn_menu_edit_key_Bb"), #imageLiteral(resourceName: "btn_menu_edit_key_Eb"), #imageLiteral(resourceName: "btn_menun_edit_key_Ab"), #imageLiteral(resourceName: "btn_menu_edit_key_Db"), #imageLiteral(resourceName: "btn_menu_edit_key_Gb"), #imageLiteral(resourceName: "btn_menu_edit_key_Cb"),#imageLiteral(resourceName: "btn_menu_edit_general_key_C_Am"), #imageLiteral(resourceName: "btn_menu_edit_key_G"), #imageLiteral(resourceName: "btn_menu_edit_key_D"), #imageLiteral(resourceName: "btn_menu_edit_key_A"), #imageLiteral(resourceName: "btn_menu_edit_key_E"), #imageLiteral(resourceName: "btn_menu_edit_key_B"), #imageLiteral(resourceName: "btn_menu_edit_key_Fs"), #imageLiteral(resourceName: " btn_menu_edit_key_Cs") ]
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(frame.height * 0.2, 0.0, 0.0, 0.0)
        flow.scrollDirection = .horizontal
        return flow
    }()
    
    lazy var collectionView:UICollectionView = {
        var cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(KeyTypeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(collectionView)
    }

}

extension MenuKeyEdit:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{

        let width:CGFloat = frame.height

        let height:CGFloat = width

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! KeyTypeCollectionViewCell
        cell.setCardImage(image: items[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! KeyTypeCollectionViewCell
        cell.onSelected(true)
        
        switch index {
        case 0:
            delegate?.changeOption(OptionKeySelected.F)
        case 1:
            delegate?.changeOption(OptionKeySelected.Bb)
        case 2:
            delegate?.changeOption(OptionKeySelected.Eb)
        case 3:
            delegate?.changeOption(OptionKeySelected.Ab)
        case 4:
            delegate?.changeOption(OptionKeySelected.Db)
        case 5:
            delegate?.changeOption(OptionKeySelected.Gb)
        case 6:
            delegate?.changeOption(OptionKeySelected.Cb)
        case 7:
            delegate?.changeOption(OptionKeySelected.C)
        case 8:
            delegate?.changeOption(OptionKeySelected.G)
        case 9:
            delegate?.changeOption(OptionKeySelected.D)
        case 10:
            delegate?.changeOption(OptionKeySelected.A)
        case 11:
            delegate?.changeOption(OptionKeySelected.E)
        case 12:
            delegate?.changeOption(OptionKeySelected.B)
        case 13:
            delegate?.changeOption(OptionKeySelected.Fs)
        case 14:
            delegate?.changeOption(OptionKeySelected.Cs)
        default:
            break
        }

    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? KeyTypeCollectionViewCell
        cell?.onSelected(false)
    }
    
}

enum OptionKeySelected {
    case F
    case Bb
    case Eb
    case Ab
    case Db
    case Gb
    case Cb
    case C
    case G
    case D
    case A
    case E
    case B
    case Fs
    case Cs
}
