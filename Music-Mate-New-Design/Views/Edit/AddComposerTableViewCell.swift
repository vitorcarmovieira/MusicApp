//
//  AddComposerTableViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 18/06/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class AddComposerTableViewCell: UITableViewCell {

  weak var txfComposer = UITextField()
    
    
    func getLeftView(text: String, widthTxtComplete: CGFloat, height: CGFloat) -> UIView {
        let heightTxts = height
        let left = UIView(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        left.backgroundColor = UIColor(red: 8/255, green: 78/255, blue: 148/255, alpha: 1.0)
        left.layer.cornerRadius = heightTxts * 0.1
        
        let widthWhiteView = (heightTxts * 0.1)
        let xWhiteView = (widthTxtComplete * 0.2) - widthWhiteView
        let whiteView = UIView(frame: CGRect(x: xWhiteView, y: 0, width: widthWhiteView, height: heightTxts))
        whiteView.backgroundColor = UIColor.white
        left.addSubview(whiteView)
        
        let lblLeft = UILabel(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        lblLeft.text = text
        lblLeft.textColor = UIColor.white
        lblLeft.textAlignment = NSTextAlignment.center
        lblLeft.font = UIFont(name: "Lato-Regular", size: heightTxts * 0.3)
        left.addSubview(lblLeft)
        
        return left
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let xIni = frame.width * 0.0638
        self.txfComposer?.leftViewMode = .always
        self.txfComposer?.leftView = getLeftView(text: "Author", widthTxtComplete: frame.width - (2.0 * xIni), height: frame.height)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
