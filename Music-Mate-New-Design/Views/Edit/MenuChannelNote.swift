//
//  MenuChannelNote.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 12/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

protocol MenuChannelNoteDelegate: class {
    func changeChannel(newChannel: Int)
}

class MenuChannelNote: UIView {

     weak var delegate: MenuChannelNoteDelegate?
    
    /// Botão canal 1
    weak var btnChannel1    : UIButton!
    /// Botão canal 2
    weak var btnChannel2    : UIButton!
    /// Botão canal 3
    weak var btnChannel3    : UIButton!
    /// Botão canal 4
    weak var btnChannel4    : UIButton!
    
    override func draw(_ rect: CGRect) {
        let width  = rect.width
        let height = rect.height / 4.0
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnChannel1, image: #imageLiteral(resourceName: "channel1_off"), frame: CGRect(x: 0, y: (height * 0), width: width, height: height))
        initializeButton(button: &btnChannel2, image: #imageLiteral(resourceName: "channel2_off"), frame: CGRect(x: 0, y: (height * 1), width: width, height: height))
        initializeButton(button: &btnChannel3, image: #imageLiteral(resourceName: "channel3_off"), frame: CGRect(x: 0, y: (height * 2), width: width, height: height))
        initializeButton(button: &btnChannel4, image: #imageLiteral(resourceName: "channel4_off"), frame: CGRect(x: 0, y: (height * 3), width: width, height: height))
        //-----------------------------------------------------------
        
        btnChannel1.addTarget(self, action: #selector(actionBtnChannel1(sender:)), for: .touchUpInside)
        btnChannel2.addTarget(self, action: #selector(actionBtnChannel2(sender:)), for: .touchUpInside)
        btnChannel3.addTarget(self, action: #selector(actionBtnChannel3(sender:)), for: .touchUpInside)
        btnChannel4.addTarget(self, action: #selector(actionBtnChannel4(sender:)), for: .touchUpInside)
    }

    @objc private func actionBtnChannel1(sender: UIButton) {
        if let deleg = delegate {
            deleg.changeChannel(newChannel: 1)
        }
    }
    
    @objc private func actionBtnChannel2(sender: UIButton) {
        if let deleg = delegate {
            deleg.changeChannel(newChannel: 2)
        }
    }
    
    @objc private func actionBtnChannel3(sender: UIButton) {
        if let deleg = delegate {
            deleg.changeChannel(newChannel: 3)
        }
    }
    
    @objc private func actionBtnChannel4(sender: UIButton) {
        if let deleg = delegate {
            deleg.changeChannel(newChannel: 4)
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
//            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
//            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
}
