//
//  MenuGeneralEditView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import Appsee

/// Protocolo utilizado para acesso ao delegate da classe MenuGeneralEditView
protocol MenuGeneralEditViewDelegate: class {
    /// Função chamada quando o tempo da partitura é alterado
    ///
    /// - Parameter option: opção selecionada
    func changeOptionTime(_ option: OptionTimeSelected)
    /// Função chamada quando o metronomo da partitura é alterado
    ///
    /// - Parameter value: novo valor de metronomo
    func changeMetronome(_ value: Int)
    /// Função chamada quando a armadura da partitura é alterada
    ///
    /// - Parameter key: nova armadura
    func changeKey(_ key: KeyType)
}

/// View responsável pela exibição do menu geral de edição
class MenuGeneralEditView: UIView {
    
    /// Picker da armadura da partitura
    var pickerKey       : AKPickerView = AKPickerView()
    /// Picker do metronomo da partitura

    /// Picker do tempo da partitura
    var pickerTime      : AKPickerView = AKPickerView()
   
    var pickerMetronome     : UIPickerView!
    /// Delegate do MenuGeneralEditView
    weak var delegate: MenuGeneralEditViewDelegate?
    
    var metronomeValue : Int!
    
    /// Botão que ativa função de seleção da primeira nota do compasso
    weak var btnFirstBar             : UIButton!
    /// Botão que move para esquerda a pickerView de armadura
    weak var btnSelectLeftKey        : UIButton!
    /// Botão que move para direita a pickerView de armadura
    weak var btnSelectRightKey       : UIButton!
    /// Botão que move para esquerda a pickerView de metronomo
    weak var btnSelectLeftMetronome  : UIButton!
    /// Botão que move para direita a pickerView de metronomo
    weak var btnSelectRightMetronome : UIButton!
    /// Botão que move para esquerda a pickerView de tempo de compasso
    weak var btnSelectLeftTime       : UIButton!
    /// Botão que move para direita a pickerView de tempo de compasso
    weak var btnSelectRightTime      : UIButton!
    
    /// Label da função de primeira nota
    weak var lblBar            : UILabel!
    /// Label da função de armadura
    weak var lblKey            : UILabel!
    /// Label da função de armadura selecionada
    weak var lblTypeKey        : UILabel!
    /// Label da função de metronomo
    weak var lblMetronome      : UILabel!
    /// Label da função de metronomo selecionado
    weak var lblTypeMetronome  : UILabel!
    /// Label da função de tempo de compasso
    weak var lblTime           : UILabel!
    /// Label da função de +10 no metronomo
    weak var lblAddMetronome   : UILabel!
    /// Label da função de -10 no metronomo
    weak var lblMinusMetronome : UILabel!
    
    /// Botão para informações adicionais
    //    weak var btnInfo      : UIButton!
    /// Label de informações adicionais
    //    weak var lblInfo      : UILabel!
    var metronomeContent :[String] = [String]()
    /// Vetor de itens da pickerView de armadura
    let itemsKey = [ #imageLiteral(resourceName: "btn_menu_edit_general_key_Db_Bbm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_Ab_Fm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_Eb_Cm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_Bb_Gm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_F_Dm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_C_Am"), #imageLiteral(resourceName: "btn_menu_edit_general_key_G_Em"), #imageLiteral(resourceName: "btn_menu_edit_general_key_D_Bm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_A_Fsm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_E_Csm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_B_Gsm"), #imageLiteral(resourceName: "btn_menu_edit_general_key_Fs_Dsm")]
    /// Vetor de itens da pickerView de tempo de compasso
    let itemsTime = [ #imageLiteral(resourceName: "btn_menu_edit_general_time_2_2"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_2"), #imageLiteral(resourceName: "btn_menu_edit_general_time_2_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_4_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_5_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_6_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_8"), #imageLiteral(resourceName: "btn_menu_edit_general_time_9_8-1"), #imageLiteral(resourceName: "btn_menu_edit_general_time_9_8"), #imageLiteral(resourceName: "btn_menu_edit_general_time_12_8") ]
    
    override func draw(_ rect: CGRect) {
        metronomeContent = ["0","1","2","3","4","5","6","7","8","9"]
        // Borda superior dos botões do menu geral
        let borderTop    = rect.height * 0.07
        // Comprimento dos botões do menu geral
        let width  = rect.height * 0.7
        // Largura dos botões do menu geral
        let heigth = rect.height * 0.7
        // Borda lateral dos botões do menu geral
        let border = ( ( rect.width - (width * 8.0) ) / 9.0)
        
        // Comprimento das labels do menu geral
        let widthLbl  = width * 1.2
        // Largura das labels do menu geral
        let heightLbl = rect.height * 0.15
        // Deslocamento das labels para ajuste correto de posicionamento relativo aos seus botões
        let deslocLbl = width * 0.1
        // Borda superior das labels em relação aos botões
        let borderTopLbl = borderTop + width
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnFirstBar, image: #imageLiteral(resourceName: "btn_menu_edit_general_bar"), frame: CGRect(x: border, y: borderTop, width: width, height: heigth))
        //        initializeButton(button: &btnInfo, image: #imageLiteral(resourceName: "btn_menu_edit_general_info"), frame: CGRect(x: rect.width - (2.8 * border), y: borderTop, width: width, height: heigth))
        //-----------------------------------------------------------
        
        //--------------------Posiciona labels-----------------------
        initializeLabel(label: &lblBar, text: "1st Bar", frame: CGRect(x: border - deslocLbl, y: borderTopLbl, width: widthLbl, height: heightLbl))
        //        initializeLabel(label: &lblInfo, text: "Aditional Info", frame: CGRect(x: (rect.width - (2.8 * border)) - deslocLbl, y: borderTopLbl, width: widthLbl, height: heightLbl))
        //-----------------------------------------------------------
        
        // Configura posicionamento da pickerView de armadura
        setupPickerKey()
        // Configura posicionamento da pickerView de metronomo
        setupPickerMetronome()
        // Configura posicionamento da pickerView de tempo de compasso
        setupPickerTime()
    }
    
    /// Função que ajusta posicionamento da pickerView de armadura, botões de mover e labels
    private func setupPickerKey() {
        // Comprimento da pickerView de armadura
        let width  = frame.height * 0.7
        // Borda lateral entre o botão de mover e a pickerView
        let border = ( ( frame.width - (width * 8.0) ) / 9.0)
        
        // Comprimento dos botões de mover pickerView de armadura
        let widthBtn  = frame.height * 0.5
        // Largura dos botões de mover pickerView de armadura
        let heightBtn = frame.height * 0.5
        // Posição x do botão de mover para esquerda
        let xLeftBtn  = width + (1.5 * border)
        // Posição x do botão de mover para direita
        let xRightBtn = width + (3.8 * border) + widthBtn + (frame.width * 0.03)
        // Posição y dos botões de mover
        let yBtn      = (frame.height / 2.0) - (heightBtn / 2.0)
        
        // Borda superior das labels
        let borderTop = frame.height * 0.07
        // Comprimento das labels
        let widthLbl  = frame.width  * 0.13
        // Larguta das labels
        let heightLbl = frame.height * 0.15
        // Posição x das labels
        let xLabel    = width + (1.5 * border) + widthBtn
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnSelectLeftKey, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_left"), frame: CGRect(x: xLeftBtn, y: yBtn, width: widthBtn, height: heightBtn))
        
        initializeButton(button: &btnSelectRightKey, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_right"), frame: CGRect(x: xRightBtn, y: yBtn, width: widthBtn, height: heightBtn))
        //-----------------------------------------------------------
        
        //--------------------Posiciona labels-----------------------
        initializeLabelBold(label: &lblTypeKey, text: "C/Am", frame: CGRect(x: xLabel, y: borderTop, width: widthLbl, height: heightLbl))
        
        initializeLabelBold(label: &lblKey, text: "Key Signature", frame: CGRect(x: xLabel, y: width + borderTop, width: widthLbl, height: heightLbl))
        //-----------------------------------------------------------
        
        //---------------------Posiciona picker----------------------
        pickerKey.frame = CGRect(x: width + (1.5 * border) + widthBtn, y: (heightBtn / 2.0), width: widthLbl, height: heightBtn)
        pickerKey.pickerViewStyle = .wheel
        pickerKey.maskDisabled    = false
        pickerKey.dataSource      = self
        pickerKey.delegate        = self
        pickerKey.textColor       = UIColor.white
        
        addSubview(pickerKey)
        
        pickerKey.selectItem(6)
        //-----------------------------------------------------------
        
        //---------Adiciona actions para os botões de mover----------
        btnSelectLeftKey.addTarget(self, action: #selector(actionBtnSelectLeftKey(sender:)), for: .touchUpInside)
        btnSelectRightKey.addTarget(self, action: #selector(actionBtnSelectRightKey(sender:)), for: .touchUpInside)
        //-----------------------------------------------------------
    }
    
    /// Função que ajusta posicionamento da pickerView de metronomo, botões de mover e labels
    private func setupPickerMetronome() {
        // Comprimento da pickerView de metronomo
        let width  = frame.height * 0.7
        // Borda lateral entre o botão de mover e a pickerView
        let border = ( ( frame.width - (width * 8.0) ) / 9.0)
        // Calcular posição inicial dos elementos da pickerView de metronomo
        let xIni = width + border + ( 2.0 * (frame.height * 0.5) ) + (frame.width * 0.03)
        // Comprimento dos botões de mover pickerView de metronomo
        let widhtBtn  = frame.height * 0.5
        // Largura dos botões de mover pickerView de metronomo
        let heightBtn = frame.height * 0.5
        // Posição x do botão de mover para esquerda
        let xLeftBtn  = width + (2.5 * border)
        // Posição x do botão de mover para direita
        let xRightBtn = width + (2.8 * border) + widhtBtn + (frame.width * 0.13)
        // Posição y dos botões de mover
        let yBtn      = (frame.height / 2.0) - (heightBtn / 2.0)
        // Borda superior das labels
        let borderTop = frame.height * 0.07
        // Comprimento das labels
        let widthLbl  = frame.width  * 0.1
        // Larguta das labels
        let heightLbl = frame.height * 0.15
        // Posição x das labels
        let xLabel    = width + (3.0 * border) + widhtBtn
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnSelectLeftMetronome, image: #imageLiteral(resourceName: "menos"), frame: CGRect(x: xLeftBtn + xIni, y: yBtn, width: widhtBtn, height: heightBtn))
        
        initializeButton(button: &btnSelectRightMetronome, image: #imageLiteral(resourceName: "mais"), frame: CGRect(x: xRightBtn + xIni, y: yBtn, width: widhtBtn, height: heightBtn))
        //-----------------------------------------------------------
        
        //--------------------Posiciona labels-----------------------
        initializeLabelBold(label: &lblTypeMetronome, text: "90", frame: CGRect(x: xLabel + xIni , y: borderTop, width: widthLbl, height: heightLbl))
        
        initializeLabelBold(label: &lblMetronome, text: "Metronome", frame: CGRect(x: xLabel + xIni, y: width + borderTop, width: widthLbl, height: heightLbl))
        

        //-----------------------------------------------------------
        
        //---------------------Posiciona picker----------------------
        
        pickerMetronome = UIPickerView(frame: CGRect(x: frame.width*0.55, y: frame.height*(-0.1), width: frame.height*0.35, height: frame.width*0.13))
      
        pickerMetronome.dataSource = self
        pickerMetronome.delegate = self
        addSubview(pickerMetronome)
        pickerMetronome.backgroundColor = UIColor.white
        pickerMetronome.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerMetronome.layer.borderWidth = 1
        pickerMetronome.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        //-----------------------------------------------------------
        
        //---------Adiciona actions para os botões de mover----------
        btnSelectLeftMetronome.addTarget(self, action: #selector(actionBtnSelectLeftMetronome(sender:)), for: .touchUpInside)
        btnSelectRightMetronome.addTarget(self, action: #selector(actionBtnSelectRightMetronome(sender:)), for: .touchUpInside)
        //-----------------------------------------------------------
    }
    
    /// Função que ajusta posicionamento da pickerView de tempo de compasso, botões de mover e labels
    private func setupPickerTime() {
        // Comprimento da pickerView de metronomo
        let width  = frame.height * 0.7
        // Borda lateral entre o botão de mover e a pickerView
        let border = ( ( frame.width - (width * 8.0) ) / 9.0)
        // Calcular posição inicial dos elementos da pickerView de metronomo
        let xIni = width + border + ( 2.0 * (frame.height * 0.5) ) + (frame.width * 0.015)
        // Comprimento dos botões de mover pickerView de metronomo
        let widhtBtn  = frame.height * 0.5
        // Largura dos botões de mover pickerView de metronomo
        let heightBtn = frame.height * 0.5
        // Posição x do botão de mover para esquerda
        let xLeftBtn  = width + (1.5 * border)
        // Posição x do botão de mover para direita
        let xRightBtn = width + (3.8 * border) + widhtBtn + (frame.width * 0.03)
        // Posição y dos botões de mover
        let yBtn      = (frame.height / 2.0) - (heightBtn / 2.0)
        // Borda superior das labels
        let borderTop = frame.height * 0.07
        // Comprimento das labels
        let widthLbl  = frame.width  * 0.13
        // Larguta das labels
        let heightLbl = frame.height * 0.15
        // Posição x das labels
        let xLabel    = width + (1.5 * border) + widhtBtn
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnSelectLeftTime, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_left"), frame: CGRect(x: xLeftBtn + (2.5 * xIni), y: yBtn, width: widhtBtn, height: heightBtn))
        
        initializeButton(button: &btnSelectRightTime, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_right"), frame: CGRect(x: xRightBtn + (2.5 * xIni), y: yBtn, width: widhtBtn, height: heightBtn))
        //-----------------------------------------------------------
        
        //--------------------Posiciona labels-----------------------
        //        initializeLabelBold(label: &lblTypeTime, text: "4/4", frame: CGRect(x: xLabel + (2.0 * xIni), y: borderTop, width: widthLbl, height: heightLbl))
        
        initializeLabelBold(label: &lblTime, text: "Time Signature", frame: CGRect(x: xLabel + (2.5 * xIni), y: width + borderTop, width: widthLbl, height: heightLbl))
        //-----------------------------------------------------------
        
        //---------------------Posiciona picker----------------------
        pickerTime.frame = CGRect(x: width + (1.5 * border) + widhtBtn + (2.5 * xIni), y: 0, width: widthLbl, height: frame.height * 0.9)
        pickerTime.pickerViewStyle   = .wheel
        pickerTime.maskDisabled      = false
        self.pickerTime.isPickerTime = true
        pickerTime.dataSource        = self
        pickerTime.delegate          = self
        pickerTime.selectItem(0, animated: true)
        pickerTime.textColor = UIColor.white
        
        addSubview(pickerTime)
        //-----------------------------------------------------------
        
        //---------Adiciona actions para os botões de mover----------
        btnSelectLeftTime.addTarget(self, action: #selector(actionBtnSelectLeftTime(sender:)), for: .touchUpInside)
        btnSelectRightTime.addTarget(self, action: #selector(actionBtnSelectRightTime(sender:)), for: .touchUpInside)
        //-----------------------------------------------------------
    }
    
    func setKey(key: Int) {
      
        var newIndex = 0
        switch key {
        case 1:
            newIndex = 5
        case 2:
            newIndex = 12
        case 3:
            newIndex = 0
        case 4:
            newIndex = 7
        case 5:
            newIndex = 2
        case 6:
            newIndex = 9
        case 7:
            newIndex = 4
        case 8:
            newIndex = 11
        case 9:
            newIndex = 0
        case 10:
            newIndex = 6
        case 11:
            newIndex = 1
        case 12:
            newIndex = 8
        case 13:
            newIndex = 3
        case 14:
            newIndex = 10
        default:
            break
        }
        
        pickerKey.selectItem(newIndex, animated: false)
    }
    
    func setMetronome(metronome: Int) {
        lblTypeMetronome.text = "\(metronome)"
        metronomeValue = metronome
        var unidade = 0
        unidade = metronome-Int(metronome/10)*10
        for i in 0..<metronomeContent.count {
            if(Int(metronomeContent[i])==unidade){
                self.pickerMetronome.selectRow(unidade, inComponent: 0, animated: false)
                break
            }
        }
        if let deleg = self.delegate {
            deleg.changeMetronome(metronomeValue)
        }
    }
    
    
    func setTimeSignature(numerator: Int, denominator: Int) {
        if numerator == 2 && denominator == 2 {
            pickerTime.selectItem(0, animated: false)
        }
        if numerator == 3 && denominator == 2 {
            pickerTime.selectItem(1, animated: false)
        }
        if numerator == 2 && denominator == 4 {
            pickerTime.selectItem(2, animated: false)
        }
        if numerator == 3 && denominator == 4 {
            pickerTime.selectItem(3, animated: false)
        }
        if numerator == 4 && denominator == 4 {
            pickerTime.selectItem(4, animated: false)
        }
        if numerator == 5 && denominator == 4 {
            pickerTime.selectItem(5, animated: false)
        }
        if numerator == 6 && denominator == 4 {
            pickerTime.selectItem(6, animated: false)
        }
        if numerator == 3 && denominator == 8 {
            pickerTime.selectItem(7, animated: false)
        }
        if numerator == 6 && denominator == 8 {
            pickerTime.selectItem(8, animated: false)
        }
        if numerator == 9 && denominator == 8 {
            pickerTime.selectItem(9, animated: false)
        }
        if numerator == 12 && denominator == 8 {
            pickerTime.selectItem(10, animated: false)
        }
        
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeLabelBold(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Bold", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    @objc func actionBtnSelectLeftKey(sender: UIButton) {
        if pickerKey.selectedItem - 1 >= 0 {
            pickerKey.selectItem(pickerKey.selectedItem - 1, animated: true)
        }
    }
    
    @objc func actionBtnSelectRightKey(sender: UIButton) {
        if pickerKey.selectedItem + 1 < itemsKey.count {
            pickerKey.selectItem(pickerKey.selectedItem + 1, animated: true)
        }
    }
    
    @objc func actionBtnSelectLeftMetronome(sender: UIButton) {
        if(metronomeValue >= 40){
            self.setMetronome(metronome: metronomeValue-10)
        }
    }
    
    @objc func actionBtnSelectRightMetronome(sender: UIButton) {
        if(metronomeValue <= 290){
            self.setMetronome(metronome: metronomeValue+10)
        }
    }
    
    @objc func actionBtnSelectLeftTime(sender: UIButton) {
        if pickerTime.selectedItem - 1 >= 0 {
            pickerTime.selectItem(pickerTime.selectedItem - 1, animated: true)
        }
    }
    
    @objc func actionBtnSelectRightTime(sender: UIButton) {
        if pickerTime.selectedItem + 1 < itemsTime.count {
            pickerTime.selectItem(pickerTime.selectedItem + 1, animated: true)
        }
    }
}

extension MenuGeneralEditView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
            return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerMetronome{
            return metronomeContent.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerMetronome {
            Appsee.addScreenAction("Clicked Change Metronome")
            lblTypeMetronome.text = "\(Int(metronomeValue/10))\(metronomeContent[row])"
            metronomeValue = Int(lblTypeMetronome.text!)
            setMetronome(metronome: metronomeValue)
        }
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if pickerView == pickerMetronome{
            return frame.height*0.4
        }
        return frame.height*0.1
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        if(pickerView == pickerMetronome){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
            label.textAlignment = .center
            label.textColor = UIColor.black
            label.font = UIFont(name: "Lato-Regular", size: frame.width*0.03)
            label.text = metronomeContent[row]
            view1.addSubview(label)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        return view
    }
    
    
}

extension MenuGeneralEditView: AKPickerViewDataSource, AKPickerViewDelegate {
    
    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        if pickerView == pickerMetronome {
            return 300
        }
        if pickerView == pickerKey {
            return itemsKey.count
        }
        if pickerView == pickerTime {
            return itemsTime.count
        }
        
        return 0
    }
    
    //    func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
    //        if pickerView == pickerMetronome {
    //            return "| "
    //        }
    //
    //        return "xx"
    //    }
    
    func pickerView(_ pickerView: AKPickerView, imageForItem item: Int) -> UIImage {
        if pickerView == pickerMetronome {
            if (item % 10) == 0{
                return #imageLiteral(resourceName: "icone_contador")
            }
            else{
                return #imageLiteral(resourceName: "icone_contador_menor")
            }
        }
        
        if pickerView == pickerKey {
            if item >= 0 && itemsKey.count > item {
                return itemsKey[item]
            } else {
                if item < 0 {
                    return itemsKey[item]
                }
                if item >= itemsKey.count {
                    return itemsKey[itemsKey.count - 1]
                }
            }
        }
        
        if pickerView == pickerTime {
            if item >= 0 && itemsTime.count > item {
                return itemsTime[item]
            } else {
                if item < 0 {
                    return itemsTime[item]
                }
                if item >= itemsTime.count {
                    return itemsTime[itemsTime.count - 1]
                }
            }
        }
        
        return UIImage()
    }
    
    // MARK: - AKPickerViewDelegate
    
    //    func pickerView(_ pickerView: AKPickerView, marginForItem item: Int) -> CGSize {
    //        return CGSize(width: 200.0, height: pickerView.frame.height)
    //    }
    
    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        if pickerView == pickerMetronome {
            lblTypeMetronome.text = "\(item + 1)"
            delegate?.changeMetronome(item + 1)
        }
        
        if pickerView == pickerTime {
            Appsee.addScreenAction("Clicked Change Time Signature")
            switch item {
            case 0:
                delegate?.changeOptionTime(.doisdois)
            case 1:
                delegate?.changeOptionTime(.tresdois)
            case 2:
                delegate?.changeOptionTime(.doisquartos)
            case 3:
                delegate?.changeOptionTime(.tresquartos)
            case 4:
                delegate?.changeOptionTime(.quatroquartos)
            case 5:
                delegate?.changeOptionTime(.cincoquartos)
            case 6:
                delegate?.changeOptionTime(.seisquartos)
            case 7:
                delegate?.changeOptionTime(.tresoitavos)
            case 8:
                delegate?.changeOptionTime(.seisoitavos)
            case 9:
                delegate?.changeOptionTime(.noveoitavos)
            case 10:
                delegate?.changeOptionTime(.dozeoitavos)
            default:
                break
            }
        }
        
        if pickerView == pickerKey {
            Appsee.addScreenAction("Clicked Change Key Signature")
            switch item {
            case 4:
                lblTypeKey.text = "F/Dm"
                delegate?.changeKey(.F_Dm)
            case 3:
                lblTypeKey.text = "Bb/Gm"
                delegate?.changeKey(.Bb_Gm)
            case 2:
                lblTypeKey.text = "Eb/Cm"
                delegate?.changeKey(.Eb_Cm)
            case 1:
                lblTypeKey.text = "Ab/Fm"
                delegate?.changeKey(.Ab_Fm)
            case 0:
                lblTypeKey.text = "Db/Bbm"
                delegate?.changeKey(.Db_BBm)
            case 5:
                lblTypeKey.text = "C/Am"
                delegate?.changeKey(.C_Am)
            case 6:
                lblTypeKey.text = "G/Em"
                delegate?.changeKey(.G_Em)
            case 7:
                lblTypeKey.text = "D/Bm"
                delegate?.changeKey(.D_Bm)
            case 8:
                lblTypeKey.text = "A/F#m"
                delegate?.changeKey(.A_Fsm)
            case 9:
                lblTypeKey.text = "E/C#m"
                delegate?.changeKey(.E_Csm)
            case 10:
                lblTypeKey.text = "B/G#m"
                delegate?.changeKey(.B_Gsm)
            case 11:
                lblTypeKey.text = "F#/D#m"
                delegate?.changeKey(.Fs_Dsm)

            default:
                break
            }
        }
    }
}

enum OptionTimeSelected {
    case doisdois
    case tresdois
    case doisquartos
    case tresquartos
    case quatroquartos
    case cincoquartos
    case seisquartos
    case tresoitavos
    case seisoitavos
    case noveoitavos
    case dozeoitavos
}

