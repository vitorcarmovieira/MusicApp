//
//  MenuCanalyseEdit.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import Appsee

protocol MenuCanalyseEditDelegate: class {
    func changeOption(_ option: OptionCanalyseSelected)
}

class MenuCanalyseEdit: UIView {

    weak var delegate: MenuCanalyseEditDelegate?
    
    let items = [#imageLiteral(resourceName: "btn_menu_edit_canalyze_13"), #imageLiteral(resourceName: "btn_menu_edit_canalyze_12"), #imageLiteral(resourceName: "btn_menu_edit_canalyze_34"), #imageLiteral(resourceName: "btn_menu_edit_canalyze_delete_above"), #imageLiteral(resourceName: "btn_menu_edit_canalyze_delete_center"), #imageLiteral(resourceName: "btn_menu_edit_canalyze_manual")]
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(frame.height * 0.2, frame.width * 0.05, 0.0, 0.0)
        flow.scrollDirection = .horizontal
        return flow
    }()
    
    lazy var collectionView:UICollectionView = {
        var cv = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(CanalyseTypeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    override func draw(_ rect: CGRect) {
        addSubview(collectionView)
    }
    
}

extension MenuCanalyseEdit:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width:CGFloat = frame.height * 1.43
        
        let height:CGFloat = width / 1.43
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CanalyseTypeCollectionViewCell
        cell.setCardImage(image: items[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CanalyseTypeCollectionViewCell
        cell.onSelected(true)
        
        switch index {
        case 0:
            delegate?.changeOption(.umtres)
            Appsee.addScreenAction("Clicked Canalyze Left-Right")
        case 1:
            delegate?.changeOption(.umdois)
            Appsee.addScreenAction("Clicked Canalyze 1/2")
        case 2:
            delegate?.changeOption(.tresquatro)
            Appsee.addScreenAction("Clicked Canalyze 3/4")
        case 3:
            delegate?.changeOption(.above)
            Appsee.addScreenAction("Clicked Canalyze Above")
        case 4:
            delegate?.changeOption(.center)
            Appsee.addScreenAction("Clicked Canalyze Center")
        case 5:
            delegate?.changeOption(.manual)
            Appsee.addScreenAction("Clicked Canalyze Manual")
        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CanalyseTypeCollectionViewCell
        cell?.onSelected(false)
    }

}

enum OptionCanalyseSelected {
    case umtres
    case umdois
    case tresquatro
    case above
    case center
    case manual
}
