//
//  MenuEditView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/13/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

/// View responsável pela exibição do menu de edit
class MenuEditView: UIView {

    /// Botão geral
    weak var btnGeneral    : UIButton!
    /// Botão canalização
    weak var btnCanalyze   : UIButton!
    /// Botão repetição
    weak var btnRepetition : UIButton!
    /// Botão dsdc
    weak var btnDSDC       : UIButton!
    /// Botão notas
    weak var btnNotes      : UIButton!
    /// Botão informações adicionais
    weak var btnInfo       : UIButton!
    /// Botão desfazer
    weak var btnUndo       : UIButton!
    /// Botão feito
    weak var btnDone       : UIButton!
    
    /// Label geral
    weak var lblGeneral    : UILabel!
    /// Label armadura
    weak var lblKey        : UILabel!
    /// Label canalização
    weak var lblCanalyze   : UILabel!
    /// Label repetição
    weak var lblRepetition : UILabel!
    /// Label dsdc
    weak var lblDSDC       : UILabel!
    /// Label notas
    weak var lblNotes      : UILabel!
    /// Label desfazer
    weak var lblUndo       : UILabel!
    /// Label feito
    weak var lblDone       : UILabel!
    
    weak var stripeView : UIView!
    weak var stripeView2 : UIView!
    weak var stripeView3 : UIView!
    weak var stripeView4 : UIView!
    weak var stripeView5 : UIView!
    
    
    var state     : StateMenuEdit  = StateMenuEdit.usable { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        backgroundColor = UIColor(red: 3 / 255, green: 65 / 255, blue: 120 / 255, alpha: 1.0)
        //Calculo do comprimento e largura do botão do menu
        let width  = rect.height * 0.555
        let heigth = rect.height * 0.555
//        let endX   = rect.width
        
        //Calculo das bordas do botão
        let borderTop    = rect.height * 0.11
        
        //Calculo do comprimento e largura dos labels do menu
        let widthLbl  = width * 1.2
        let deslocLbl = width * 0.1
        let heightLbl = rect.height * 0.15
        
        //Calculo da borda de cima do label
        let borderTopLbl = borderTop + width
        
        let border = ( ( rect.width - (width * 8.0) ) / 9.0)
        
        
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnGeneral, image: #imageLiteral(resourceName: "general"), frame: CGRect(x: rect.width * 0.026, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnCanalyze, image: #imageLiteral(resourceName: "btn_menu_edit_canalyze"), frame: CGRect(x: rect.width * 0.156, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnRepetition, image: #imageLiteral(resourceName: "btn_menu_edit_repetition"), frame: CGRect(x: (rect.width * 0.299), y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnDSDC, image: #imageLiteral(resourceName: "btn_menu_edit_DSDC"), frame: CGRect(x: rect.width * 0.403, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnNotes, image: #imageLiteral(resourceName: "btn_menu_edit_notes"), frame: CGRect(x: rect.width * 0.546, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnInfo, image: #imageLiteral(resourceName: "addinfo_on"), frame: CGRect(x: rect.width * 0.69, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnUndo, image: #imageLiteral(resourceName: "btn_menu_edit_undo"), frame: CGRect(x: rect.width * 0.82, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnDone, image: #imageLiteral(resourceName: "btn_menu_edit_done"), frame: CGRect(x: rect.width * 0.911, y: borderTop, width: width, height: heigth))
        //-----------------------------------------------------------
        
        
        //--------------------Posiciona labels-----------------------
        initializeLabel(label: &lblGeneral, text: "General", frame: CGRect(x: rect.width * 0.026 - frame.width * 0.008, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblKey, text: "Canalyse", frame: CGRect(x: rect.width * 0.156 - frame.width * 0.008, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblCanalyze, text: "Repetition", frame: CGRect(x: rect.width * 0.299 - frame.width * 0.006, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblRepetition, text: "Ornaments", frame: CGRect(x: rect.width * 0.403 - frame.width * 0.007, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblDSDC, text: "Edit Notes", frame: CGRect(x: rect.width * 0.546 - frame.width * 0.006, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblNotes, text: "Additional Info", frame: CGRect(x: rect.width * 0.661 , y: borderTopLbl, width: widthLbl + 50, height: heightLbl))
        
        initializeLabel(label: &lblUndo, text: "Undo", frame: CGRect(x: rect.width * 0.82 - frame.width * 0.008, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblDone, text: "Done", frame: CGRect(x: rect.width * 0.911 - frame.width * 0.008, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        //-----------------------------Poiciona as Barras------------------------------
        
        initStripeView (stripe: &stripeView, frame: CGRect(x: rect.width * 0.117, y: frame.height*0.1, width:frame.width*0.002, height: frame.height*0.8))
        initStripeView (stripe: &stripeView2, frame: CGRect(x: rect.width * 0.26, y: frame.height*0.1, width:frame.width*0.002, height: frame.height*0.8))
        initStripeView (stripe: &stripeView3, frame: CGRect(x: rect.width * 0.507, y: frame.height*0.1, width:frame.width*0.002, height: frame.height*0.8))
        initStripeView (stripe: &stripeView4, frame: CGRect(x: rect.width * 0.651, y: frame.height*0.1, width:frame.width*0.002, height: frame.height*0.8))
        initStripeView (stripe: &stripeView5, frame: CGRect(x: rect.width * 0.794, y: frame.height*0.1, width:frame.width*0.002, height: frame.height*0.8))
        
        
        enableButtonsByState()
    }

    func enableButtonsByState() {
        switch state {
        case .usable:
            btnGeneral.isEnabled    = true
            btnInfo.isEnabled       = true
            btnCanalyze.isEnabled   = true
            btnRepetition.isEnabled = true
            btnDSDC.isEnabled       = true
            btnNotes.isEnabled      = true
            btnUndo.isEnabled       = true
            btnDone.isEnabled       = true
        case .notUsable:
            break
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)//UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    deinit {
        btnGeneral    = nil
        btnInfo       = nil
        btnCanalyze   = nil
        btnRepetition = nil
        btnDSDC       = nil
        btnNotes      = nil
        btnUndo       = nil
        btnDone       = nil
        
        lblGeneral    = nil
        lblKey        = nil
        lblCanalyze   = nil
        lblRepetition = nil
        lblDSDC       = nil
        lblNotes      = nil
        lblUndo       = nil
        lblDone       = nil
    }
    
    func enableButtons() {
        btnGeneral.isEnabled    = true
        btnInfo.isEnabled       = true
        btnCanalyze.isEnabled   = true
        btnRepetition.isEnabled = true
        btnDSDC.isEnabled       = true
        btnNotes.isEnabled      = true
        btnUndo.isEnabled       = true
        btnDone.isEnabled       = true
    }
    
    func disableButtons() {
        btnGeneral.isEnabled    = false
        btnInfo.isEnabled       = false
        btnCanalyze.isEnabled   = false
        btnRepetition.isEnabled = false
        btnDSDC.isEnabled       = false
        btnNotes.isEnabled      = false
        btnUndo.isEnabled       = false
        btnDone.isEnabled       = false
    }
}

enum StateMenuEdit {
    case usable
    case notUsable
}
