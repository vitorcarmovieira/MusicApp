//
//  NotesTypeCollectionViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class NotesTypeCollectionViewCell: UICollectionViewCell {
    
    var cardView:NotesTypeView
    var cellSelected = false
    
    override init(frame: CGRect) {
        cardView = NotesTypeView(frame: frame)
        
        super.init(frame: frame)
        self.contentView.addSubview(cardView)
    }
    
    @objc func didSwipeLeft(gesture:UIGestureRecognizer){
        NSLog("Swipe left")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCardImage(image:UIImage){
        self.cardView.image.image = image
    }
    
    override func layoutSubviews() {
        self.cardView.frame = self.bounds
        
    }
    
//    override var isHighlighted: Bool { // make lightgray background show immediately
//        willSet {
//            onSelected(newValue)
//        }
//    }
//    override var isSelected: Bool { // keep lightGray background until unselected
//        willSet {
//            onSelected(newValue)
//        }
//    }
    func onSelected(_ newValue: Bool) {
        if !newValue {
            alpha = 1.0
            cellSelected = false
        } else {
            alpha = 0.4
            cellSelected = true
        }
    }
}
