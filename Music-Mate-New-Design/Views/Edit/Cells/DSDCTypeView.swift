//
//  DSDCTypeView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/16/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class DSDCTypeView: UIView {

    var image:UIImageView
    
    override init(frame: CGRect) {
        self.image = UIImageView()
        self.image.image = #imageLiteral(resourceName: "mini_score")
        
        super.init(frame: frame)
        
        self.addSubview(image)
        self.backgroundColor    = UIColor.clear
        self.layer.cornerRadius = 10.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.image.frame = CGRect(x: 0, y: 0, width: frame.height * 0.8, height: frame.height * 0.8)
    }
    
}
