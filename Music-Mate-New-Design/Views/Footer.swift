//
//  Footer.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/9/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class Footer: UIView {

    weak var btnImgInstrument : UIButton!
    weak var btnLblInstrument : UIButton!
    
    weak var btnImgMIDI       : UIButton!
    weak var btnLblMIDI       : UIButton!
    weak var stripeView       : UIView!
    weak var btnHelp          : UIButton!
    
    weak var btnEnableAmostras : UIButton!
    weak var btnEnableNoteLog  : UIButton!
    
    
    override func draw(_ rect: CGRect) {
        let widthImg  = rect.height * 0.588
        let heightImg = widthImg
        
        let widthLbl  = widthImg * 5.0
        let heightLbl = heightImg
        //
//        let border    = widthImg
        let borderTop = rect.height * 0.2
//        let borderBetween = rect.width * 0.03
        
//        let rectImgInstrument = CGRect(x: border, y: borderTop, width: widthImg, height: heightImg)
        let rectLblInstrument = CGRect(x: rect.width * 0.026, y: 0, width: widthLbl, height: heightLbl + (2.0 * borderTop))
        
        let rectImgMIDI = CGRect(x: (rectLblInstrument.width) + rect.width * 0.074 , y: borderTop, width: widthImg, height: heightImg)
        let rectLblMIDI = CGRect(x: (rectImgMIDI.origin.x + rectImgMIDI.width) + rect.width * 0.007 , y: borderTop, width: widthLbl * 2.0, height: heightLbl)
        
        let rectBtnHelp = CGRect(x: rect.width*0.9375 , y: borderTop, width: (widthLbl / 2.0), height: heightLbl)
        
        initStripeView(stripe: &stripeView, frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height*0.02))
//        initButton(button: &btnImgInstrument, image: #imageLiteral(resourceName: "footer_piano"), frame: rectImgInstrument)
        initButton(button: &btnLblInstrument, text: "", frame: rectLblInstrument, prop: rectLblInstrument.height * 0.65)
        
        initButton(button: &btnImgMIDI, image: #imageLiteral(resourceName: "footer_midi_desconnected"), frame: rectImgMIDI)
        initButton(button: &btnLblMIDI, text: "Midi desconnected", frame: rectLblMIDI, prop: rectLblMIDI.height * 0.8)

        initButton(button: &btnHelp, text: "Help", frame: rectBtnHelp, prop: rectBtnHelp.height * 0.8)
       // initButton(button: &btnInfo, text: "Info", frame: rectBtnInfo, prop: rectBtnInfo.height * 0.8)

    }
    
    func isDebug(){
       
        self.initButton(button: &btnEnableAmostras, text: "Amostras", frame: CGRect(x: self.frame.width*0.6, y: 0, width: self.frame.width*0.2, height: self.frame.height), prop: self.frame.width*0.2)
        self.initButton(button: &btnEnableNoteLog, text: "Log", frame: CGRect(x: self.frame.width*0.6, y: 0, width: self.frame.width*0.2, height: self.frame.height), prop: self.frame.width*0.2)
    }
    
    func setMidiIsConnected(connected: Bool) {
        if connected {
            btnImgMIDI.setImage(#imageLiteral(resourceName: "midi_desconectado"), for: .normal)
            btnLblMIDI.setTitle("Midi connected", for: .normal)
            btnLblMIDI.setTitleColor(UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1.0), for: .normal)
        } else {
            btnImgMIDI.setImage(#imageLiteral(resourceName: "midi_conectado"), for: .normal)
            btnLblMIDI.setTitle("Midi disconnected", for: .normal)
            btnLblMIDI.setTitleColor(UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), for: .normal)
        }
    }
    
    func initButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initButton(button: inout UIButton!, text: String, frame: CGRect, prop: CGFloat) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setTitle(text, for: .normal)
            
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            btn.contentHorizontalAlignment = .left
            btn.titleLabel?.font = UIFont(name: "Lato-Regular", size: 16)
            btn.titleLabel?.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            btn.setTitleColor(UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), for: .normal)
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
}
