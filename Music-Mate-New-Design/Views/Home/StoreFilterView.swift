//
//  StoreFilterView.swift
//  Music-Mate-New-Design
//
//  Created by Nicolas Fernandes on 13/05/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit
var row2 = 0
protocol StoreFilterDelegate: class {
    func changeCountry(newCountry: ModelCountry)
    func changeGenre(newGenre: ModelGenre)
    func changeSubgenre(newSubgenre: ModelSubGenre)
    func changeInstrument(newInstrument: ModelInstrument)
    func changeType(newType: ModelTypeInstrument)
    func changeAuthor(newAuthor: ModelAuthor)
//    func changeArranger(newArranger: String)
    func changeTitle(newTitle: String)
//    func changeSubtitle(newSubtitle: String)
    func changeDificult(newDificult: Int)
    func changeDate(newString: String)
//    func changeTags(newTag: String)
//    func changeScoreType(newType: Int)
//    func changeKeyType(newKey: Int)
    //    func changeMP(newMP: ModelMP)
}

class StoreFilter: UIView {
    weak var delegate: StoreFilterDelegate?
    
    //Opções de países
    weak var tbCountry        : UITableView!
    //Opções de genero 1
    weak var tbGener1         : UITableView!
    //Opções de genero 2
    weak var tbGener2         : UITableView!
    //Opções de instrumentos
    weak var tbInstrument     : UITableView!
    //Opções de tipos de instrumentos
    weak var tbTypeInstrument : UITableView!
    //Opções de dificuldade
    weak var tbDificult       : UITableView!
    weak var tbAuthor         : UITableView!
    //Opcoes de Preset
    weak var tbMP             : UITableView!
    //Opcoes de Preset
    weak var tbKey            : UITableView!
    //Botao cancel
    weak var btnDone          : UIButton!
    //Botao cancel
    weak var btnApply         : UIButton!
    //Botao cancel
    weak var btnClearAll          : UIButton!
    
    
    ///Label sobre o artista
    weak var lblAboutArtist   : UILabel!
    
    ///Label usuario
    weak var lblProfile       : UILabel!
    
    ///Label sobre o artista
    weak var lblComposer      : UILabel!
    ///Label compositor requerido
    weak var lblReqComposer   : UILabel!
    
    ///Label Arrangement
    weak var lblArrangement   : UILabel!
    ///Label titulo requerido
    weak var lblReqArrangement: UILabel!
    
    ///Label pais
    weak var lblCountry       : UILabel!
    ///Label pais requerido
    weak var lblReqCountry    : UILabel!
    
    
    weak var lblIsPoliphonic    : UILabel!
    
    
    ///Label sobre partitura
//    weak var lblAboutScore    : UILabel!
    
    ///Label titulo
    weak var lblTitle         : UILabel!
    ///Label titulo requerido
    weak var lblReqTitle      : UILabel!
//    ///Label titulo
//    weak var lblSubTitle      : UILabel!
//    ///Label titulo requerido
//    weak var lblReqSubTitle   : UILabel!
    ///Label genero
    weak var lblGenero        : UILabel!
    ///Label genero requerido
    weak var lblReqGenero     : UILabel!
    ///Label genero2
    weak var lblGenero2       : UILabel!
    ///Label genero2 requerido
    weak var lblReqGenero2    : UILabel!
    ///Label instrumento
    weak var lblInstrument    : UILabel!
    ///Label tipo de instrumento
    weak var lblTypeInstrument: UILabel!
    ///Label instrumento requerido
    weak var lblReqInstrument : UILabel!
    ///Label tipo requerido
    weak var lblReqType       : UILabel!
    ///Label data
    weak var lblDate          : UILabel!
    ///Label data requerida
    weak var lblReqDate       : UILabel!
    ///Label dificuldade
    weak var lblDifficulty    : UILabel!
    ///Label dificuldade requerido
    weak var lblReqDifficulty : UILabel!
    
    //Label MOnofonico o Polifonico
//    weak var lblMonoPoli    : UILabel!
    ///Label dificuldade requerido
//    weak var lblReqMonoPoli : UILabel!
    
//    weak var swtIsPoliphonic : UISwitch!
    
//    ///Barra horizontal esquerda do sobre artista
//    weak var barLAboutArtist  : UIView!
//    ///Barra horizontal direita do sobre artista
//    weak var barRAboutArtist  : UIView!
//
//    ///Barra horizontal esquerda do sobre partitura
//    weak var barLAboutScore   : UIView!
//    ///Barra horizontal direita do sobre partitura
//    weak var barRAboutScore   : UIView!
    
    ///Imagem perfil usuario
    weak var imgProfile       : UIImageView!
    
    /// Campo Arrangement
    weak var txtArrangement      : UITextField!
    /// Campo Composer
    weak var txtComposer      : UITextField!
    /// Campo Pais
    weak var txtCountry       : UITextField!
    /// Campo Title
    weak var txtTitle         : UITextField!
    
//    /// Campo Subtitle
//    weak var txtSubtitle      : UITextField!
    
    /// Campo Gener1
    weak var txtGener1        : UITextField!
    /// Campo Gener2
    weak var txtGener2        : UITextField!
    /// Campo Instrumento
    weak var txtInstrument    : UITextField!
    /// Campo tipo de Instrumento
    weak var txtTypeInstrument: UITextField!
    /// Campo Instrumento
    weak var txtDate          : UITextField!
    /// Campo Instrumento
    weak var txtDificult      : UITextField!
    /// Campo Instrumento
//    weak var txtTags          : UITextField!
//    /// Campo Monofonico Polifonico
//    weak var txtMP          : UITextField!
//    /// Campo Key Type
//    weak var txtKeyType          : UITextField!
    
    
    
    /// Campo Composer
    var vComposer      : UIView!
    /// Campo Arrangement
    var vArrangement     : UIView!
    /// Campo Pais
    var vCountry       : UIView!
    /// Campo Title
    var vTitle         : UIView!
    
    /// Campo Subtitle
    var vSubtitle      : UIView!
    
    /// Campo Gener1
    var vGener1        : UIView!
    /// Campo Gener2
    var vGener2        : UIView!
    /// Campo Instrumento
    var vInstrument    : UIView!
    /// Campo Tipo de Instrumento
    var vTypeInstrument: UIView!
    /// Campo Instrumento
    var vDate          : UIView!
    /// Campo Instrumento
    var vDificult      : UIView!
    /// Campo Instrumento
    var vTags          : UIView!
    /// Campo Monofonico Polifonico
    var vMP            : UIView!
    /// Campo Monofonico Polifonico
    var vKey            : UIView!
    
    
    
    var countries        = [""]
    var geners1          = [""]
    var geners2          = [""]
    var instruments      = ["Flute", "Guitar", "Piano", "Sax", "Violin" ]
    var instrumentsTypes = ["Flute", "Guitar", "Piano", "Sax", "Violin" ]
    var dificulties      = ["Easy", "Medium", "Hard"]
    var mp               = ["Monophonic", "Poliphonic" ]
    var keys             = ["Major", "Minor" ]
    
    
    var currentGender    = ModelGenre()
    var currentSubgender = ModelSubGenre()
    var currentCountry   = ModelCountry()
    var currentInstrument  = ModelInstrument()
    var currentTypeInstrument = ModelTypeInstrument()
    //    var correntMP        = ModelMP()
    
    private var modelCountries       : [ModelCountry]        = []
    private var modelGenders         : [ModelGenre]          = []
    private var modelSubGenders      : [ModelSubGenre]       = []
    private var modelInstruments     : [ModelInstrument]     = []
    private var modelTypeInstruments : [ModelTypeInstrument] = []
    //    private var modelMP              : [ModelMP]             = []
    
    override func draw(_ rect: CGRect) {
        
        modelCountries = DAOCountry.getInstance().getAllCountries()
        
        countries.removeAll()
        
        for country in modelCountries {
            countries.append(country.name)
        }
        
        modelGenders = DAOGenre.getInstance().getAllGenrs()
        
        geners1.removeAll()
        
        for gender in modelGenders {
            geners1.append(gender.name)
        }
        
        modelInstruments = DAOInstrument.getInstance().getAllInstruments()
        
        instruments.removeAll()
        
        for modelInstrument in modelInstruments {
            instruments.append(modelInstrument.name)
        }
        
        instrumentsTypes.removeAll()
        
        let xIni = rect.width * 0.0638
        let widthAbout = rect.width
        let height1 = rect.height * 0.021
        let height2 = rect.height * 0.09
        //        let height3 = rect.height * 0.10
        let height4 = rect.height * 0.0478
        
        //--------------------Posiciona barras----------------------
        //        let yBar          = height1 * 0.08
        //        let widthBar      = rect.width * 0.382
        //        let heigthBar     = height2 * 0.05
        //        let rectLBarArtist = CGRect(x: 0.0, y: yBar, width: rect.width / 3.0, height: heigthBar)
        //        let rectRBarArtist = CGRect(x: rect.width - widthBar, y: yBar, width: widthBar, height: heigthBar)
        
        //        initializeBar(bar: &barLAboutArtist, rect: rectLBarArtist)
        //        initializeBar(bar: &barRAboutArtist, rect: rectRBarArtist)
        //
        //        barLAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //        barRAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        let color = UIColor(red: 14/255, green: 86/255, blue: 160/255, alpha: 1.0)
        //        let rectAboutArtist = CGRect(x: 0, y: 0, width: widthAbout, height: height2)
        //
        //        initializeLabel(label: &lblAboutArtist, text: "About Artist", frame: rectAboutArtist, color: color, prop: 0.25)
        //----------------------------------------------------------
        
        //----------------Posiciona imagem usuario------------------
        //        let yImg      = height2
        //        let widthImg  = height3 * 0.5
        //        let heightImg = height3 * 0.5
        //        let rectImg   = CGRect(x: xIni, y: yImg, width: widthImg, height: heightImg)
        //
        //        initializeImage(imageView: &imgProfile, image: #imageLiteral(resourceName: "peter"), frame: rectImg)
        //----------------------------------------------------------
        
        //-----------------Posiciona label usuario------------------
        //        let xLblUser      = xIni + widthImg + (xIni / 3.0)
        //        let yLblUser      = yImg
        //        let widthLblUser  = rect.width - xLblUser
        //        let heightLblUser = heightImg
        //        let rectLblUser   = CGRect(x: xLblUser, y: yLblUser, width: widthLblUser, height: heightLblUser)
        //
        //        initializeLabel(label: &lblProfile, text: "Peter Tilanus", frame: rectLblUser, color: UIColor.black, prop: 0.4, align: .left)
        //----------------------------------------------------------
        
        //---------------Posiciona txts sobre artista---------------
        let widthTxtComplete = rect.width - (2.0 * xIni)
        let heightTxts       = rect.height * 0.0478
        
                let yTxtComposer     = height2 * 1.3
//                let yTxtCountry      = yTxtComposer + height2
        //
        //        let rectTxtComposer  = CGRect(x: xIni, y: yTxtComposer, width: widthTxtComplete, height: heightTxts)
//                let rectTxtCountry   = CGRect(x: xIni, y: yTxtCountry, width: widthTxtComplete, height: heightTxts)
        //
        //        initializeTextField(field: &txtComposer, text: "Composer", frame: rectTxtComposer, widthComplete: widthTxtComplete)
//                initializeTextField(field: &txtCountry, text: "Country", frame: rectTxtCountry, widthComplete: widthTxtComplete)
        //----------------------------------------------------------
        
        //--------------------Posiciona barras----------------------
//        let yBarA          = height1 * 1.5
//        let widthBar      = rect.width * 0.382
//        let heigthBar     = height2 * 0.05
//        let rectLBarArtist  = CGRect(x: 0.0, y: yBarA, width: widthBar, height: heigthBar)
//        let rectRBarArtist = CGRect(x: rect.width - widthBar, y: yBarA, width: widthBar, height: heigthBar)
        
//        initializeBar(bar: &barLAboutArtist, rect: rectLBarArtist)
//        initializeBar(bar: &barRAboutArtist, rect: rectRBarArtist)
        
        ///switch antigo
//        initializeSwitch(switchE: &swtIsPoliphonic, isOn: false, frame: CGRect(x: rect.width*0.2, y: rect.height*0.9,width: rect.width*0.2, height: rect.width*0.2))
        
//        
//        barLAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
//        barRAboutArtist.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        //        let yLblS: CGFloat          = 0.0//yTxtCountry + (height1 * 0.7)
//        let rectAboutArtist = CGRect(x: 0, y: height1, width: widthAbout, height: height1)
//        initializeLabel(label: &lblAboutArtist, text: "About Artist", frame: rectAboutArtist, color: color, prop: 1.0)
        //----------------------------------------------------------
        
        //-------------Posiciona txts sobre partitura---------------
//        let yTxtComposer       = height1 * 3
//        let yTxtArrangement    = yTxtComposer + height2
        
        //------------- Bar Score _____________________________
//        let yBarB          = yTxtArrangement + height4 + ( height1 * 1.5)
//        let rectLBarScore  = CGRect(x: 0.0, y: yBarB, width: widthBar, height: heigthBar)
//        let rectRBarScore  = CGRect(x: rect.width - widthBar, y: yBarB, width: widthBar, height: heigthBar)
//
//        let rectAboutScore = CGRect(x: 0, y: yTxtArrangement + height4 + height1, width: widthAbout, height: height1)
//        initializeLabel(label: &lblAboutScore, text: "About Partiture", frame: rectAboutScore, color: color, prop: 1.0)
//
//        initializeBar(bar: &barLAboutScore, rect: rectLBarScore)
//        initializeBar(bar: &barRAboutScore, rect: rectRBarScore)
//        barLAboutScore.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
//        barRAboutScore.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        
//        let yBarScore          = yTxtArrangement + (height1 * 1.5)
        
        let yTxtTitle          = height1 + height1 * 0.1
        let yTxtSubtitle       = yTxtTitle + height2
        let yTxtGener1         = yTxtComposer + height2
        let yTxtGener2         = yTxtGener1 + height2
        let yTxtInstrument     = yTxtGener2 + height2
        let yTxtTypeInstrument = yTxtInstrument + height2
        let yTxtDate           = yTxtTypeInstrument + height2
        let yTxtTags           = yTxtDate + height2
        let yTxtMP             = yTxtTags + height2
        let widthMidle = (rect.width - (2.0 * xIni)) / 2.3
        let xMidle     = rect.width / 1.8
        
        let rectTxtComposer       = CGRect(x: xIni, y: yTxtComposer, width: widthTxtComplete, height: heightTxts)
//        let rectTxtArrangement    = CGRect(x: xIni, y: yTxtArrangement, width: widthTxtComplete, height: heightTxts)
        let rectTxtTitle          = CGRect(x: xIni, y: yTxtTitle, width: widthTxtComplete, height: heightTxts)
        let rectTxtSubtitle       = CGRect(x: xIni, y: yTxtSubtitle, width: widthTxtComplete, height: heightTxts)
        let rectTxtGener1         = CGRect(x: xIni, y: yTxtGener1, width: widthTxtComplete, height: heightTxts)
        let rectTxtGener2         = CGRect(x: xIni, y: yTxtGener2, width: widthTxtComplete, height: heightTxts)
        let rectTxtInstrument     = CGRect(x: xIni, y: yTxtInstrument, width: widthTxtComplete, height: heightTxts)
        let rectTxtTypeInstrument = CGRect(x: xIni, y: yTxtTypeInstrument, width: widthTxtComplete, height: heightTxts)
        let rectTxtDate           = CGRect(x: xMidle, y: yTxtDate, width: widthMidle, height: heightTxts)
        let rectTxtDificult       = CGRect(x: xIni, y: yTxtDate, width: widthMidle, height: heightTxts)
        let rectTxtTags           = CGRect(x: xIni, y: yTxtTags, width: widthTxtComplete, height: heightTxts)
        let rectTxtMP             = CGRect(x: xIni, y: yTxtMP, width: widthMidle, height: heightTxts)
        let rectTxtKey            = CGRect(x: xMidle, y: yTxtMP, width: widthMidle, height: heightTxts)
        
        initializeButtonCancelDone(button: &btnDone, image: #imageLiteral(resourceName: "botao"), txt: "Cancel", frame: CGRect(x: frame.width * 0.7, y: frame.height * 0.854, width: frame.width*0.247, height: frame.height*0.049))
        initializeButtonCancelDone(button: &btnApply, image: #imageLiteral(resourceName: "botao"), txt: "Apply", frame: CGRect(x: frame.width * 0.38, y: frame.height * 0.854, width: frame.width*0.247, height: frame.height*0.049))
        initializeButtonCancelDone(button: &btnClearAll, image: #imageLiteral(resourceName: "botao"), txt: "Clear All", frame: CGRect(x: xIni, y: frame.height * 0.854, width: frame.width*0.247, height: frame.height*0.049))
        initializeTextField(field: &txtComposer, text: "Artist", frame: rectTxtComposer, widthComplete: widthTxtComplete)
//        initializeTextField(field: &txtArrangement, text: "Arranger", frame: rectTxtArrangement, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtTitle, text: "Title", frame: rectTxtTitle, widthComplete: widthTxtComplete)
//        initializeTextField(field: &txtSubtitle, text: "Subtitle", frame: rectTxtSubtitle, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtGener1, text: "Genre", frame: rectTxtGener1, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtGener2, text: "Subgenre", frame: rectTxtGener2, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtInstrument, text: "Category", frame: rectTxtInstrument, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtTypeInstrument, text: "Instrument", frame: rectTxtTypeInstrument, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtDate, text: "Date", frame: rectTxtDate, widthComplete: widthTxtComplete)
        initializeTextField(field: &txtDificult, text: "Difficult", frame: rectTxtDificult, widthComplete: widthTxtComplete)
//        initializeTextField(field: &txtMP, text: "Score Type", frame: rectTxtMP, widthComplete: widthTxtComplete)
//        initializeTextField(field: &txtKeyType, text: "Key Type", frame: rectTxtKey, widthComplete: widthTxtComplete)
//        initializeTextField(field: &txtTags, text: "Tags", frame: rectTxtTags, widthComplete: widthTxtComplete)
        //----------------------------------------------------------
        
        txtComposer.rightViewMode        = .always
        txtGener1.rightViewMode         = .always
        txtGener2.rightViewMode         = .always
        txtInstrument.rightViewMode     = .always
        txtTypeInstrument.rightViewMode = .always
        txtDate.rightViewMode           = .always
        txtDificult.rightViewMode       = .always
//        txtTags.rightViewMode           = .always
//        txtMP.rightViewMode             = .always
//        txtKeyType.rightViewMode        = .always
        
        setRightView(field: &txtComposer, type: 1)
        setRightView(field: &txtGener1, type: 1)
        setRightView(field: &txtGener2, type: 1)
        setRightView(field: &txtInstrument, type: 1)
        setRightView(field: &txtTypeInstrument, type: 1)
        setRightView(field: &txtDate, type: 2)
        setRightView(field: &txtDificult, type: 1)
        //        setRightView(field: &txtTags, type: 1)
//        setRightView(field: &txtMP, type: 1)
//        setRightView(field: &txtKeyType, type: 1)
        txtDate.keyboardType = .decimalPad
        
        let boderTxts       = rect.height * 0.005
        let inc             = 2.0 * boderTxts
        let rectVComposer   = CGRect(x: xIni - boderTxts, y: yTxtComposer - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
//        let rectVArrangement   = CGRect(x: xIni - boderTxts, y: yTxtArrangement - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        
        let rectVTitle      = CGRect(x: xIni - boderTxts, y: yTxtTitle - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVSubtitle   = CGRect(x: xIni - boderTxts, y: yTxtSubtitle - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVGener1     = CGRect(x: xIni - boderTxts, y: yTxtGener1 - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVGener2     = CGRect(x: xIni - boderTxts, y: yTxtGener2 - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVInstrument = CGRect(x: xIni - boderTxts, y: yTxtInstrument - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVTypeInstrument = CGRect(x: xIni - boderTxts, y: yTxtTypeInstrument - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVDate       = CGRect(x: xMidle - boderTxts, y: yTxtDate - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        let rectVDificult   = CGRect(x: xIni - boderTxts, y: yTxtDate - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        let rectVCountry   = CGRect(x: xIni - boderTxts, y: yTxtTags - boderTxts, width: widthTxtComplete + inc, height: heightTxts + inc)
        let rectVMP   = CGRect(x: xIni - boderTxts, y: yTxtMP - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        
        let rectVKey   = CGRect(x: xMidle - boderTxts, y: yTxtMP - boderTxts, width: widthMidle + inc, height: heightTxts + inc)
        
        vComposer   = UIView(frame: rectVComposer)
//        vArrangement   = UIView(frame: rectVArrangement)
        
        vCountry    = UIView(frame: rectVCountry)
        vTitle      = UIView(frame: rectVTitle)
        vSubtitle   = UIView(frame: rectVSubtitle)
        vGener1     = UIView(frame: rectVGener1)
        vGener2     = UIView(frame: rectVGener2)
        vInstrument = UIView(frame: rectVInstrument)
        vTypeInstrument = UIView(frame: rectVTypeInstrument)
        vDate       = UIView(frame: rectVDate)
        vDificult   = UIView(frame: rectVDificult)
//        vTags       = UIView(frame: rectVTags)
        vMP         = UIView(frame: rectVMP)
        vKey         = UIView(frame: rectVKey)
        
        let colorBg = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
        
        vComposer.backgroundColor   = colorBg
//        vArrangement.backgroundColor   = colorBg
                vCountry.backgroundColor    = colorBg
        vTitle.backgroundColor      = colorBg
        vSubtitle.backgroundColor   = colorBg
        vGener1.backgroundColor     = colorBg
        vGener2.backgroundColor     = colorBg
        vInstrument.backgroundColor = colorBg
        vTypeInstrument.backgroundColor = colorBg
        vDate.backgroundColor       = colorBg
        vDificult.backgroundColor   = colorBg
//        vTags.backgroundColor       = colorBg
//        vMP.backgroundColor       = colorBg
//        vKey.backgroundColor       = colorBg
        
        vComposer.layer.cornerRadius   = heightTxts * 0.1
//        vArrangement.layer.cornerRadius   = heightTxts * 0.1
                vCountry.layer.cornerRadius    = heightTxts * 0.1
        vTitle.layer.cornerRadius      = heightTxts * 0.1
        vSubtitle.layer.cornerRadius   = heightTxts * 0.1
        vGener1.layer.cornerRadius     = heightTxts * 0.1
        vGener2.layer.cornerRadius     = heightTxts * 0.1
        vInstrument.layer.cornerRadius = heightTxts * 0.1
        vTypeInstrument.layer.cornerRadius = heightTxts * 0.1
        vDate.layer.cornerRadius       = heightTxts * 0.1
        vDificult.layer.cornerRadius   = heightTxts * 0.1
//        vTags.layer.cornerRadius       = heightTxts * 0.1
//        vMP.layer.cornerRadius       = heightTxts * 0.1
//        vKey.layer.cornerRadius       = heightTxts * 0.1
        
        //        addSubview(vComposer)
        //        addSubview(vCountry)
        //        addSubview(vTitle)
        //        addSubview(vSubtitle)
        //        addSubview(vGener1)
        //        addSubview(vGener2)
        //        addSubview(vInstrument)
        //        addSubview(vDate)
        //        addSubview(vDificult)
        //        addSubview(vTags)
        
        sendSubview(toBack: vComposer)
//        sendSubview(toBack: vArrangement)
        
                sendSubview(toBack: vCountry)
        sendSubview(toBack: vTitle)
        sendSubview(toBack: vSubtitle)
        sendSubview(toBack: vGener1)
        sendSubview(toBack: vGener2)
        sendSubview(toBack: vInstrument)
        sendSubview(toBack: vTypeInstrument)
        sendSubview(toBack: vDate)
        sendSubview(toBack: vDificult)
//        sendSubview(toBack: vTags)
//        sendSubview(toBack: vMP)
//        sendSubview(toBack: vKey)
        
        txtDate.placeholder = "___/___/______"
        txtDate.textColor = UIColor.black
        txtDate.addTarget(self, action: #selector(applyFilterDateOnTextField(_:)), for: UIControlEvents.editingChanged)
        
        let desloc = widthTxtComplete * 0.2
        
        //        let rectTbCountry    = CGRect(x: xIni + desloc, y: yTxtCountry + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 3.0)
        
        
        txtComposer.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtGener1.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtGener2.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtInstrument.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtTypeInstrument.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        txtDificult.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        btnClearAll.addTarget(self, action: #selector(textFieldClean(textField:)), for: .touchUpInside)
//        txtTags.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
//        txtMP.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
//        txtKeyType.addTarget(self, action: #selector(textFieldActive(_:)), for: UIControlEvents.touchDown)
        
        
        txtGener1.text         = currentGender.name
        txtGener2.text         = currentSubgender.name
        txtInstrument.text     = DAOTypeInstrument.getInstance().getTypeInstrumentActive().instrumentCategory.name
        txtTypeInstrument.text = DAOTypeInstrument.getInstance().getTypeInstrumentActive().name
        //  txtTags.text            = "autor"
//                txtCountry.text   = currentCountry.name
        
//                txtCountry.placeholder = "Choose a country"
        txtGener1.placeholder  = "Choose a genre"
        txtGener2.placeholder  = "Choose a subgenre"
        txtInstrument.placeholder = "Choose an instrument"
        txtTypeInstrument.placeholder = "Choose a type of instrument"
//        txtTags.placeholder         = "Choose an Author"
//        txtTags.placeholder         = "Monophonic or Poliphonic"
        
        
//                addBorderRed(field: &txtCountry)
        addBorderRed(field: &txtComposer)
        //        addBorderRed(field: &txtArrangement)
        addBorderRed(field: &txtTitle)
        //        addBorderRed(field: &txtSubtitle)
        addBorderRed(field: &txtGener1)
        addBorderRed(field: &txtGener2)
        addBorderRed(field: &txtInstrument)
        addBorderRed(field: &txtTypeInstrument)
        addBorderRed(field: &txtDate)
        addBorderRed(field: &txtDificult)
        //        addBorderRed(field: &txtTags)
//        addBorderRed(field: &txtMP)
//        addBorderRed(field: &txtKeyType)
        
        //        let deslocVertical    = boderTxts
        //        let rectLblComposer   = CGRect(x: xIni, y: yTxtComposer + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblCountry    = CGRect(x: xIni, y: yTxtComposer + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        
        let rectLblComposer      = CGRect(x: xIni, y: yTxtComposer + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
//        let rectLblArrangement      = CGRect(x: xIni, y: yTxtArrangement + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        
        let rectLblTitle      = CGRect(x: xIni, y: yTxtTitle + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblSubtitle   = CGRect(x: xIni, y: yTxtSubtitle + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblGener1     = CGRect(x: xIni, y: yTxtGener1 + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblGener2     = CGRect(x: xIni, y: yTxtGener2 + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblInstrument = CGRect(x: xIni, y: yTxtInstrument + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblTypeInstrument = CGRect(x: xIni, y: yTxtTypeInstrument + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblDate       = CGRect(x: xMidle, y: yTxtDate + heightTxts , width: widthMidle, height: heightTxts/2.0)
        let rectLblDificult   = CGRect(x: xIni, y: yTxtDate + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        let rectLblMP   = CGRect(x: xIni, y: yTxtTags + (heightTxts * 0.9), width: widthTxtComplete, height: heightTxts/2.0)
        
        initializeLabel(label: &lblComposer, text: "", frame: rectLblComposer, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
//        initializeLabel(label: &lblArrangement, text: "", frame: rectLblArrangement, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblTitle, text: "", frame: rectLblTitle, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
//        initializeLabel(label: &lblSubTitle, text: "", frame: rectLblSubtitle, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblGenero, text: "", frame: rectLblGener1, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblGenero2, text: "", frame: rectLblGener2, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblInstrument, text: "", frame: rectLblInstrument, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblTypeInstrument, text: "", frame: rectLblTypeInstrument, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblDifficulty, text: "", frame: rectLblDificult, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        initializeLabel(label: &lblDate, text: "", frame: rectLblDate, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
//        initializeLabel(label: &lblMonoPoli, text: "", frame: rectLblMP, color: UIColor.red, prop: 0.5, align: NSTextAlignment.left)
        
        
        let rectTbGener1     = CGRect(x: xIni + desloc, y: yTxtGener1 + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 2.0)
        let rectTbGener2     = CGRect(x: xIni + desloc, y: yTxtGener2 + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 2.0)
        let rectTbInstrument = CGRect(x: xIni + desloc, y: yTxtInstrument + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 2.0)
        let rectTbTypeInstrument = CGRect(x: xIni + desloc, y: yTxtTypeInstrument + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 2.0)
        let rectTbDificult   = CGRect(x: xIni + desloc, y: yTxtDate + heightTxts, width: widthTxtComplete/4.1, height: heightTxts * 2.0)
        let rectTbAuthor   = CGRect(x: xIni + desloc, y: yTxtComposer + heightTxts, width: widthTxtComplete - desloc, height: heightTxts * 2.0)
        let rectTbMP   = CGRect(x: xIni + desloc, y: yTxtTags - (heightTxts/2.3), width: (widthTxtComplete/2) - desloc, height: heightTxts * 2)
        let rectTbCountry   = CGRect(x: xIni + desloc, y: yTxtTags - (heightTxts/2.3), width: (widthTxtComplete/2) - desloc, height: heightTxts * 2)
        
        
        initializeTableView(table: &tbCountry, frame: rectTbCountry)
        initializeTableView(table: &tbGener1, frame: rectTbGener1)
        initializeTableView(table: &tbGener2, frame: rectTbGener2)
        initializeTableView(table: &tbInstrument, frame: rectTbInstrument)
        initializeTableView(table: &tbTypeInstrument, frame: rectTbTypeInstrument)
        initializeTableView(table: &tbDificult, frame: rectTbDificult)
        initializeTableView(table: &tbAuthor, frame: rectTbAuthor)
        initializeTableView(table: &tbMP, frame: rectTbMP)
//        initializeTableView(table: &tbKey, frame: rectTbKey)
        
                tbCountry.register(UITableViewCell.self, forCellReuseIdentifier: "cellCountry")
        tbGener1.register(UITableViewCell.self, forCellReuseIdentifier: "cellGener1")
        tbGener2.register(UITableViewCell.self, forCellReuseIdentifier: "cellGener2")
        tbInstrument.register(UITableViewCell.self, forCellReuseIdentifier: "cellInstrument")
        tbTypeInstrument.register(UITableViewCell.self, forCellReuseIdentifier: "cellTypeInstrument")
        tbDificult.register(UITableViewCell.self, forCellReuseIdentifier: "cellDificult")
        tbAuthor.register(UITableViewCell.self, forCellReuseIdentifier: "cellAuthor")
        tbMP.register(UITableViewCell.self, forCellReuseIdentifier: "cellMP")
//        tbKey.register(UITableViewCell.self, forCellReuseIdentifier: "cellKey")
        
        
        tbGener1.backgroundColor = UIColor.clear
        tbGener1.layer.shadowColor = UIColor.darkGray.cgColor
        tbGener1.layer.shadowOffset = CGSize(width: 4.0, height: 4.0)
        tbGener1.layer.shadowOpacity = 1.0
        tbGener1.layer.shadowRadius = 2
        //        tbGener1.layer.masksToBounds = false
        
        tbGener2.backgroundColor = UIColor.clear
        tbGener2.layer.shadowColor = UIColor.darkGray.cgColor
        tbGener2.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tbGener2.layer.shadowOpacity = 1.0
        tbGener2.layer.shadowRadius = 2
        //        tbGener2.layer.masksToBounds = false
        
        tbInstrument.backgroundColor = UIColor.clear
        tbInstrument.layer.shadowColor = UIColor.darkGray.cgColor
        tbInstrument.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tbInstrument.layer.shadowOpacity = 1.0
        tbInstrument.layer.shadowRadius = 2
        //        tbInstrument.layer.masksToBounds = false
        
        tbTypeInstrument.backgroundColor = UIColor.clear
        tbTypeInstrument.layer.shadowColor = UIColor.darkGray.cgColor
        tbTypeInstrument.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tbTypeInstrument.layer.shadowOpacity = 1.0
        tbTypeInstrument.layer.shadowRadius = 2
        //        tbTypeInstrument.layer.masksToBounds = false
        
        tbDificult.backgroundColor = UIColor.clear
        tbDificult.layer.shadowColor = UIColor.darkGray.cgColor
        tbDificult.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tbDificult.layer.shadowOpacity = 1.0
        tbDificult.layer.shadowRadius = 2
        //        tbDificult.layer.masksToBounds = false
        
        
        /// aqui deve ser do switch mp
        tbMP.backgroundColor = UIColor.clear
        tbMP.layer.shadowColor = UIColor.darkGray.cgColor
        tbMP.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        tbMP.layer.shadowOpacity = 1.0
        tbMP.layer.shadowRadius = 2
        //        tbMP.layer.masksToBounds = false
        
//        tbKey.backgroundColor = UIColor.clear
//        tbKey.layer.shadowColor = UIColor.darkGray.cgColor
//        tbKey.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
//        tbKey.layer.shadowOpacity = 1.0
//        tbKey.layer.shadowRadius = 2
        
        tbGener1.separatorStyle = .none
        tbGener2.separatorStyle = .none
        tbInstrument.separatorStyle = .none
        tbTypeInstrument.separatorStyle = .none
        tbDificult.separatorStyle = .none
        tbAuthor.separatorStyle = .none
        tbMP.separatorStyle = .none
//        tbKey.separatorStyle = .none
        
        
        txtComposer.delegate    = self
//        txtArrangement.delegate = self
//                txtCountry.delegate    = self
        txtTitle.delegate      = self
//        txtSubtitle.delegate   = self
        txtGener1.delegate     = self
        txtGener2.delegate     = self
        txtInstrument.delegate = self
        txtTypeInstrument.delegate = self
        txtDificult.delegate   = self
        txtDate.delegate       = self
//        txtTags.delegate       = self
//
//        txtMP.delegate         = self
//        txtKeyType.delegate    = self
        
        
//        swtIsPoliphonic.isHidden = true
        
//        txtArrangement.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
        txtTitle.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
//        txtSubtitle.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
//        txtTags.addTarget(self, action: #selector(txtTitleDidChange(textField:)), for: .editingChanged)
    }
    
    
    @objc func textFieldClean(textField: UITextField) {
        
        txtTitle.text = " "
        txtDate.text = " "
        txtGener1.text = " "
        txtGener2.text = " "
//        txtCountry.text = " "
        txtComposer.text = " "
        txtInstrument.text = " "
        txtDificult.text = " "
//        txtArrangement.text = " "
        txtTypeInstrument.text = " "
    }
    @objc func txtTitleDidChange(textField: UITextField) {
//        if textField == txtArrangement {
//            if let deleg = delegate {
//                deleg.changeArranger(newArranger: textField.text!)
//            }
//        }
        if textField == txtTitle {
            if let deleg = delegate {
                deleg.changeTitle(newTitle: textField.text!)
            }
        }
//        if textField == txtSubtitle {
//            if let deleg = delegate {
//                deleg.changeSubtitle(newSubtitle: textField.text!)
//            }
//        }
        
//        if textField == txtTags {
//            if let deleg = delegate {
//                deleg.changeTags(newTag: textField.text!)
//            }
//        }
    }
    
    func addBorderRed(field: inout UITextField!) {
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.red.cgColor
    }
    
    func removeBorderRed(field: inout UITextField!) {
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.clear.cgColor
    }
    
    private var indexText = 0
    
    @objc private func applyFilterDateOnTextField(_ field: UITextField) {
        let text = field.text!
        var textWithMask = ""
        
        if text.count - 1 >= 0{
            if isNumber(textToValidate: text[text.count - 1]) {
                for i in 0..<text.count {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i < 10 {
                        textWithMask += text[i]
                    }
                    
                }
            } else {
                for i in 0..<(text.count - 1) {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    if i < 10 {
                        textWithMask += text[i]
                    }
                }
            }
        }
        
        
        
        let validator = Validator()
        
        if validator.validateDate(date: txtDate.text!){
            txtDate.layer.borderColor = UIColor.clear.cgColor
            if let deleg = delegate {
                deleg.changeDate(newString: txtDate.text!)
            }
        }else{
            txtDate.layer.borderColor = UIColor.red.cgColor
        }
        
        field.text = textWithMask
    }
    
    public func isNumber(textToValidate:String) -> Bool{
        
        let num = Int(textToValidate)
        
        if num != nil {
            return true
        }
        
        return false
    }
    
    func setRightView(field: inout UITextField!, type: Int) {
        let border         = field.frame.height * 0.2
        var widthImgRight  = field.frame.height * 0.3
        var heightImgRight = widthImgRight * 1.6
        if type == 2 {
            widthImgRight  = field.frame.height * 0.6
            heightImgRight = widthImgRight * 0.9
        }
        
        let x = field.frame.width - widthImgRight - (border * 9.0)
        let right = UIImageView(frame: CGRect(x: x, y: border, width: widthImgRight, height: heightImgRight))
        
        if type == 1 {
            right.image = #imageLiteral(resourceName: "up_down")
        } else {
            right.image = #imageLiteral(resourceName: "mini_calendar")
        }
        field.rightView = right
    }
    
    func getLeftView(text: String, widthTxtComplete: CGFloat, height: CGFloat) -> UIView {
        let heightTxts = height
        let left = UIView(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        left.backgroundColor = UIColor(red: 8/255, green: 78/255, blue: 148/255, alpha: 1.0)
        left.layer.cornerRadius = heightTxts * 0.1
        
        let widthWhiteView = (heightTxts * 0.1)
        let xWhiteView = (widthTxtComplete * 0.2) - widthWhiteView
        let whiteView = UIView(frame: CGRect(x: xWhiteView, y: 0, width: widthWhiteView, height: heightTxts))
        whiteView.backgroundColor = UIColor.white
        left.addSubview(whiteView)
        
        let lblLeft = UILabel(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        lblLeft.text = text
        lblLeft.textColor = UIColor.white
        lblLeft.textAlignment = NSTextAlignment.center
        lblLeft.font = UIFont(name: "Lato-Regular", size: heightTxts * 0.3)
        left.addSubview(lblLeft)
        
        return left
    }
    
    func initializeTextField(field: inout UITextField!, text: String, frame: CGRect, widthComplete: CGFloat) {
        if field == nil {
            let f = UITextField(frame: frame)
            f.backgroundColor = UIColor.white
            f.layer.cornerRadius = frame.height * 0.1
            f.leftViewMode = .always
            f.delegate = self
            f.leftView = getLeftView(text: text, widthTxtComplete: widthComplete, height: frame.height)
            addSubview(f)
            field = f
        } else {
            field.frame = frame
        }
    }
    
    func initializeButtonCancelDone(button: inout UIButton!, image: UIImage, txt : String, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            //  btn.setImage(image, for: .normal)
            btn.setBackgroundImage(image, for: .normal)
            btn.setTitle(txt, for: .normal)
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: frame.height * 0.5)
            btn.titleLabel?.textColor = UIColor.white
            btn.titleLabel?.textAlignment = .center
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.center
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initializeImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            addSubview(img)
            imageView = img
        } else {
            imageView.frame = frame
            imageView.image = image
        }
    }
    
    func initializeSwitch(switchE: inout UISwitch!, isOn: Bool, frame: CGRect) {
        if switchE == nil {
            let swt = UISwitch(frame: frame)
            swt.isOn = isOn
            addSubview(swt)
            switchE = swt
        } else {
            switchE.frame = frame
        }
    }
    
    func initializeBar(bar: inout UIView!, rect: CGRect) {
        if bar == nil {
            let b = UIView(frame: rect)
            b.backgroundColor = UIColor(red: 201/255, green: 201/255, blue: 201/255, alpha: 1.0)
            
            addSubview(b)
            bar = b
        } else {
            bar.frame = frame
        }
    }
    
    func initializeTableView(table: inout UITableView!, frame: CGRect) {
        if table == nil {
            let tb = UITableView(frame: frame)
            tb.delegate = self
            tb.dataSource = self
            tb.isHidden = true
            addSubview(tb)
            
            table = tb
        } else {
            table.frame = frame
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect, color: UIColor, prop: CGFloat, align: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = color
            lbl.textAlignment = align
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * prop)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    @IBAction func btnDone(_ sender: Any) {
        
        self.isHidden = true
    }
}

extension StoreFilter: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbCountry {
            return countries.count
        }
        if tableView == tbGener1 {
            return geners1.count
        }
        if tableView == tbGener2 {
            return geners2.count
        }
        if tableView == tbInstrument {
            return instruments.count
        }
        if tableView == tbTypeInstrument {
            return instrumentsTypes.count
        }
        if tableView == tbDificult {
            return dificulties.count
        }
        if tableView == tbAuthor {
            return authors.count
        }
        if tableView == tbMP {
            return mp.count
        }
        if tableView == tbKey {
            return keys.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellName = ""
        
        if tableView == tbAuthor {
            cellName = "cellAuthor"
        }
        if tableView == tbCountry {
            cellName = "cellCountry"
        }
        if tableView == tbGener1 {
            cellName = "cellGener1"
        }
        if tableView == tbGener2 {
            cellName = "cellGener2"
        }
        if tableView == tbInstrument {
            cellName = "cellInstrument"
        }
        if tableView == tbTypeInstrument {
            cellName = "cellTypeInstrument"
        }
        if tableView == tbDificult {
            cellName = "cellDificult"
        }
        if tableView == tbMP {
            cellName = "cellMP"
        }
        if tableView == tbKey {
            cellName = "cellKey"
        }
        
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellName) as UITableViewCell?)!
        
        if tableView == tbAuthor {
            cell.textLabel?.text = "\(authors[indexPath.row].firstName) \(authors[indexPath.row].lastName)"
            cell.textLabel?.font = txtComposer.font
        }
//        if tableView == tbCountry {
//            cell.textLabel?.text = countries[indexPath.row]
//            cell.textLabel?.font = txtCountry.font
//        }
        if tableView == tbGener1 {
            cell.textLabel?.text = geners1[indexPath.row]
            cell.textLabel?.font = txtGener1.font
        }
        if tableView == tbGener2 {
            cell.textLabel?.text = geners2[indexPath.row]
            cell.textLabel?.font = txtGener2.font
        }
        if tableView == tbInstrument {
            cell.textLabel?.text = instruments[indexPath.row]
            cell.textLabel?.font = txtInstrument.font
        }
        if tableView == tbTypeInstrument {
            cell.textLabel?.text = instrumentsTypes[indexPath.row]
            cell.textLabel?.font = txtTypeInstrument.font
        }
        if tableView == tbDificult {
            cell.textLabel?.text = dificulties[indexPath.row]
            cell.textLabel?.font = txtDificult.font
        }
//        if tableView == tbMP {
//            cell.textLabel?.text = mp[indexPath.row]
//            cell.textLabel?.font = txtMP.font
//        }
//        if tableView == tbKey {
//            cell.textLabel?.text = keys[indexPath.row]
//            cell.textLabel?.font = txtKeyType.font
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Row selected, so set textField to relevant value, hide tableView
        // endEditing can trigger some other action according to requirements
        if tableView == tbCountry {
            currentCountry = modelCountries[indexPath.row]
            
            txtCountry.text = countries[indexPath.row]
            tbCountry.isHidden = true
            txtCountry.endEditing(true)
            txtCountry.layer.borderColor = UIColor.clear.cgColor
            txtCountry.isEnabled = true
            
            if let deleg = delegate {
                deleg.changeCountry(newCountry: currentCountry)
            }
        }
        
        if tableView == tbAuthor {
            txtComposer.text = "\(authors[indexPath.row].firstName) \(authors[indexPath.row].lastName)"
            
            if let deleg = delegate {
                deleg.changeAuthor(newAuthor: authors[indexPath.row])
            }
            tbAuthor.isHidden = true
            txtComposer.endEditing(true)
            txtComposer.layer.borderColor = UIColor.clear.cgColor
            txtComposer.isEnabled = true
            row2 = indexPath.row
        }
        
        
        if tableView == tbGener1 {
            currentGender = modelGenders[indexPath.row]
            
            geners2.removeAll()
            
            modelSubGenders = DAOSubGenre.getInstance().getSubGenrs(with: currentGender.id)
            
            for sub in modelSubGenders {
                geners2.append(sub.name)
            }
            
            tbGener2.reloadData()
            txtGener2.text = ""
            txtGener2.placeholder = "Choose a subgenre"
            txtGener1.text = geners1[indexPath.row]
            tbGener1.isHidden = true
            txtGener1.endEditing(true)
            txtGener1.layer.borderColor = UIColor.clear.cgColor
            txtGener1.isEnabled = true
            
            if let deleg = delegate {
                deleg.changeGenre(newGenre: currentGender)
            }
        }
        
        if tableView == tbGener2 {
            currentSubgender = modelSubGenders[indexPath.row]
            
            txtGener2.text = geners2[indexPath.row]
            tbGener2.isHidden = true
            txtGener2.endEditing(true)
            txtGener2.layer.borderColor = UIColor.clear.cgColor
            txtGener2.isEnabled = true
            
            if let deleg = delegate {
                deleg.changeSubgenre(newSubgenre: currentSubgender)
            }
        }
        
        if tableView == tbInstrument {
            currentInstrument = modelInstruments[indexPath.row]
            
            instrumentsTypes.removeAll()
            
            modelTypeInstruments = DAOTypeInstrument.getInstance().getTypesInstrumentsById(currentInstrument.id)
            
            for sub in modelTypeInstruments {
                instrumentsTypes.append(sub.name)
            }
            
            tbTypeInstrument.reloadData()
            txtInstrument.text = instruments[indexPath.row]
            tbInstrument.isHidden = true
            txtInstrument.endEditing(true)
            txtInstrument.layer.borderColor = UIColor.clear.cgColor
            txtInstrument.isEnabled = true
            
            if let deleg = delegate {
                deleg.changeInstrument(newInstrument: currentInstrument)
            }
            txtTypeInstrument.text = ""
        }
        
        if tableView == tbTypeInstrument {
            currentTypeInstrument = modelTypeInstruments[indexPath.row]
            txtTypeInstrument.text = instrumentsTypes[indexPath.row]
            tbTypeInstrument.isHidden = true
            txtTypeInstrument.endEditing(true)
            txtTypeInstrument.layer.borderColor = UIColor.clear.cgColor
            txtTypeInstrument.isEnabled = true
            
            if let deleg = delegate {
                deleg.changeType(newType: currentTypeInstrument)
            }
        }
        
        if tableView == tbDificult {
            txtDificult.text = dificulties[indexPath.row]
            tbDificult.isHidden = true
            txtDificult.endEditing(true)
            txtDificult.layer.borderColor = UIColor.clear.cgColor
            txtDificult.isEnabled = true
            if let deleg = delegate {
                deleg.changeDificult(newDificult: indexPath.row + 1)
            }
        }
        
//        if tableView == tbMP {
//            txtMP.text = mp[indexPath.row]
//            tbMP.isHidden = true
//            txtMP.endEditing(true)
//            txtMP.layer.borderColor = UIColor.clear.cgColor
//            txtMP.isEnabled = true
//            if let deleg = delegate {
//                deleg.changeScoreType(newType: indexPath.row)
//            }
//        }
        
//        if tableView == tbKey {
//            txtKeyType.text = keys[indexPath.row]
//            tbKey.isHidden = true
//            txtKeyType.endEditing(true)
//            txtKeyType.layer.borderColor = UIColor.clear.cgColor
//            txtKeyType.isEnabled = true
//            if let deleg = delegate {
//                deleg.changeKeyType(newKey: indexPath.row)
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func setComposer(_ new: String) {
//        if txtComposer != nil {
//            txtComposer.text = new
//            
//            let bnStoreFilter = StoreFilter()
//            
//            let results = bnStoreFilter.validateValue(text: new)
//            
//            if let result = results.first {
//                if result.key {
//                    removeBorderRed(field: &txtComposer)
//                    lblComposer.isHidden = true
//                }
//            }
//        }
    }
    
//    func setArranger(_ new: String) {
//        if txtArrangement != nil {
//            txtArrangement.text = new
//
//        }
//    }
    
    func setTitle(_ new: String) {
        if txtTitle != nil {
            txtTitle.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateTitle(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtTitle)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
//    func setSubtitle(_ new: String) {
//        if txtSubtitle != nil {
//            txtSubtitle.text = new
//        }
//    }
    
    func setGenre(_ new: String) {
        if txtGener1 != nil {
            txtGener1.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateGener(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtGener1)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
    func setSubgenre(_ new: String) {
        if txtGener2 != nil {
            txtGener2.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateSubGener(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtGener2)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
    func setCategoryInstrument(_ new: String) {
        if txtInstrument != nil {
            txtInstrument.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateInstrument(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtInstrument)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
    func setInstrument(_ new: String) {
        if txtTypeInstrument != nil {
            txtTypeInstrument.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateInstrument(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtTypeInstrument)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
    func setDifficult(_ new: String) {
        if txtDificult != nil {
            txtDificult.text = new
            
            let bnStoreFilter = BRAditionalInfo()
            
            let results = bnStoreFilter.validateDificult(text: new)
            
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtDificult)
                    lblComposer.isHidden = true
                }
            }
        }
    }
    
//    func setTags(_ new: String) {
//        if txtTags != nil {
//            txtTags.text = new
//
//            let bnStoreFilter = BRAditionalInfo()
//
//            let results = bnStoreFilter.validateInstrument(text: new)
//
//            if let result = results.first {
//                if result.key {
//                    removeBorderRed(field: &txtTags)
//                    lblComposer.isHidden = true
//                }
//            }
//        }
//    }
    
//    func setScoreType(_ new: String) {
//        if txtMP != nil {
//            txtMP.text = new
//
//            let bnStoreFilter = BRAditionalInfo()
//
//            let results = bnStoreFilter.validateInstrument(text: new)
//
//            if let result = results.first {
//                if result.key {
//                    removeBorderRed(field: &txtMP)
//                    lblComposer.isHidden = true
//                }
//            }
//        }
//    }
    
//    func setKeyType(_ new: String) {
//        if txtKeyType != nil {
//            txtKeyType.text = new
//
//            let bnStoreFilter = BRAditionalInfo()
//
//            let results = bnStoreFilter.validateInstrument(text: new)
//
//            if let result = results.first {
//                if result.key {
//                    removeBorderRed(field: &txtKeyType)
//                    lblComposer.isHidden = true
//                }
//            }
//        }
//    }
}

extension StoreFilter: UITextFieldDelegate {
    @objc func textFieldActive(_ field: UITextField) {
        if field == txtComposer {
            txtComposer.endEditing(true)
            tbAuthor.isHidden         = !tbAuthor.isHidden
            tbGener1.isHidden         = true
            tbGener2.isHidden         = true
            tbInstrument.isHidden     = true
            tbDificult.isHidden       = true
            tbTypeInstrument.isHidden = true
            tbMP.isHidden             = true
//            tbKey.isHidden            = true
        }
        if field == txtCountry {
            txtCountry.endEditing(true)
                        tbCountry.isHidden = !tbCountry.isHidden
            
            tbAuthor.isHidden   = true
            tbGener1.isHidden     = true
            tbGener2.isHidden     = true
            tbInstrument.isHidden = true
            tbDificult.isHidden   = true
            tbTypeInstrument.isHidden = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
        }
        else if field == txtGener1 {
            txtGener1.endEditing(true)
            tbGener1.isHidden = !tbGener1.isHidden
            tbTypeInstrument.isHidden = true
            tbAuthor.isHidden   = true
            tbGener2.isHidden     = true
            tbInstrument.isHidden = true
            tbDificult.isHidden   = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
        }
        else if field == txtGener1 {
            tbGener1.isHidden = !tbGener1.isHidden
            
            tbAuthor.isHidden   = true
            tbTypeInstrument.isHidden = true
            tbGener2.isHidden     = true
            tbInstrument.isHidden = true
            tbDificult.isHidden   = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
        } else if field == txtGener2 {
            txtGener2.endEditing(true)
            tbGener2.isHidden = !tbGener2.isHidden
            
            tbAuthor.isHidden   = true
            tbGener1.isHidden     = true
            tbTypeInstrument.isHidden = true
            tbInstrument.isHidden = true
            tbDificult.isHidden   = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
            
        } else if field == txtInstrument {
            txtInstrument.endEditing(true)
            tbInstrument.isHidden = !tbInstrument.isHidden
            
            tbAuthor.isHidden   = true
            tbGener1.isHidden     = true
            tbTypeInstrument.isHidden = true
            tbGener2.isHidden     = true
            tbDificult.isHidden   = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
            
        } else if field == txtTypeInstrument {
            txtTypeInstrument.endEditing(true)
            tbTypeInstrument.isHidden = !tbTypeInstrument.isHidden
            
            tbAuthor.isHidden   = true
            tbInstrument.isHidden = true
            tbGener1.isHidden     = true
            tbGener2.isHidden     = true
            tbDificult.isHidden   = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
            
        } else if field == txtDificult {
            txtDificult.endEditing(true)
            tbDificult.isHidden = !tbDificult.isHidden
            
            tbAuthor.isHidden   = true
            tbGener1.isHidden     = true
            tbTypeInstrument.isHidden = true
            tbInstrument.isHidden = true
            tbGener2.isHidden     = true
            tbMP.isHidden   = true
//            tbKey.isHidden            = true
            
        }
//        else if field == txtTags {
//            tbAuthor.isHidden = true
//            tbDificult.isHidden   = true
//            tbGener1.isHidden     = true
//            tbTypeInstrument.isHidden = true
//            tbInstrument.isHidden = true
//            tbGener2.isHidden     = true
//            tbMP.isHidden   = true
//            tbKey.isHidden            = true
//
//        } else if field == txtMP {
//            tbMP.isHidden = !tbMP.isHidden
//            tbDificult.isHidden   = true
//            tbGener1.isHidden     = true
//            tbTypeInstrument.isHidden = true
//            tbInstrument.isHidden = true
//            tbGener2.isHidden     = true
//            tbKey.isHidden            = true
//
//        } else if field == txtKeyType {
//            tbKey.isHidden = !tbKey.isHidden
//            tbDificult.isHidden   = true
//            tbGener1.isHidden     = true
//            tbTypeInstrument.isHidden = true
//            tbInstrument.isHidden = true
//            tbGener2.isHidden     = true
//            tbMP.isHidden            = true
//
//        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        let bnStoreFilter = BRAditionalInfo()
        
        //        if textField == txtComposer {
        //            let results = bnStoreFilter.validateComposer(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtComposer)
        //                    lblComposer.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtComposer)
        //                    lblComposer.isHidden = false
        //                    lblComposer.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtArrangement {
        //            let results = bnStoreFilter.validateArrangement(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtComposer)
        //                    lblComposer.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtComposer)
        //                    lblComposer.isHidden = false
        //                    lblComposer.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtCountry {
        //            let results = bnStoreFilter.validateCountry(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtCountry)
        //                    lblCountry.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtCountry)
        //                    lblCountry.isHidden = false
        //                    lblCountry.text     = result.value
        //                }
        //            }
        //        }
        
        if textField == txtTitle {
            let results = bnStoreFilter.validateTitle(text: textField.text!)
            if let result = results.first {
                if result.key {
                    removeBorderRed(field: &txtTitle)
                    lblTitle.isHidden = true
                    
                }
                //                else {
                //                    addBorderRed(field: &txtTitle)
                //                    lblTitle.isHidden = false
                //                    lblTitle.text     = result.value
                //                }
            }
        }
        
        //        if textField == txtSubtitle {
        //            let results = bnStoreFilter.validateSubTitle(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtSubtitle)
        //                    lblSubTitle.isHidden = true
        //
        //                } else {
        ////                    addBorderRed(field: &txtSubtitle)
        //                    lblSubTitle.isHidden = false
        //                    lblSubTitle.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtGener1 {
        //            let results = bnStoreFilter.validateGener(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtGener1)
        //                    lblGenero.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtGener1)
        //                    lblGenero.isHidden = false
        //                    lblGenero.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtGener2 {
        //            let results = bnStoreFilter.validateSubGener(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtGener2)
        //                    lblGenero2.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtGener2)
        //                    lblGenero2.isHidden = false
        //                    lblGenero2.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtInstrument {
        //            let results = bnStoreFilter.validateInstrument(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtInstrument)
        //                    lblInstrument.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtInstrument)
        //                    lblInstrument.isHidden = false
        //                    lblInstrument.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtDificult {
        //            let results = bnStoreFilter.validateDificult(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtDificult)
        //                    lblDifficulty.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtDificult)
        //                    lblDifficulty.isHidden = false
        //                    lblDifficulty.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtDate {
        //            let results = bnStoreFilter.validateDate(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtDificult)
        //                    lblDate.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtDificult)
        //                    lblDate.isHidden = false
        //                    lblDate.text     = result.value
        //                }
        //            }
        //        }
        
        //        if textField == txtMP {
        //            let results = bnStoreFilter.validateDificult(text: textField.text!)
        //            if let result = results.first {
        //                if result.key {
        //                    removeBorderRed(field: &txtMP)
        //                    lblMonoPoli.isHidden = true
        //
        //                } else {
        //                    addBorderRed(field: &txtMP)
        //                    lblMonoPoli.isHidden = false
        //                    lblMonoPoli.text     = result.value
        //                }
        //            }
        //        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // TODO: Your app can do something when textField finishes editing
        print("The textField ended editing. Do something based on app requirements.")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
