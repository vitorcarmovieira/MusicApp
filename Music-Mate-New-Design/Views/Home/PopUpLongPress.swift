//
//  PopUpLongPress.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 25/01/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

protocol PopUpLongPressDelegate: class {
    func sendToMyLibrary()
}

class PopUpLongPress: UIView {
    
    weak var delegate: PopUpLongPressDelegate?
    
    //
    
    
    weak var lblTitleMusic : UILabel!
    weak var lblSubTitleMusic : UILabel!
    weak var lblNameComposer : UILabel!
    weak var lblComposedBy : UILabel!
    weak var lblTitleTonality : UILabel!
    weak var lblTitleTimeSignature : UILabel!
    weak var lblTonality : UILabel!
    weak var lblTimeSignature : UILabel!
    weak var lblTypePartiture : UILabel!
    weak var lblStatusPatiture : UILabel!
    weak var lblNumbersDownloads : UILabel!
    
    weak var imgDownloadsPatiture : UIImageView!
    weak var imgStatusPatiture : UIImageView!
   
    
    
    weak var btnDownload : UIButton!
    weak var btnDelete : UIButton!
    weak var btnSendToMyLibrary : UIButton!
    weak var btnNewArrangement : UIButton!
    weak var btnOpen : UIButton!
    
    
    weak var stripeView : UIView!
    weak var circleView : UIView!
    weak var circleView2 : UIView!
    weak var partituraView : UIView!
    weak var viewAux : UIView!
    weak var viewAux2 : UIView!
    
 
    var store : Bool!
 
    
    override func draw(_ rect: CGRect) {
     
        self.initPartituraView(stripe: &partituraView, frame: CGRect(x: 0, y: 0, width: rect.width*0.6, height: rect.height))
        
        self.initStripeView(stripe: &stripeView, frame: CGRect(x: rect.width*0.679, y: rect.height*0.145, width: rect.width*0.218, height: rect.height*0.002))
        
        self.initCircleView(stripe: &circleView, frame: CGRect(x: rect.width*0.63, y: rect.height*0.203, width: rect.width*0.02, height: rect.width*0.02))
 
        self.initCircleView(stripe: &circleView2, frame: CGRect(x: rect.width*0.63, y: rect.height*0.603, width: rect.width*0.02, height: rect.width*0.02))
        
        self.initTonalityTime(view: &viewAux, labelMaior: &lblTonality, labelMenor: &lblTitleTonality, radius: rect.width*0.017, border: rect.width*0.0015, text: "Tonality", frameView: CGRect(x: rect.width*0.696, y: rect.height*0.3, width: rect.width*0.09, height: rect.width*0.09), frameLabelMaior: CGRect(x: rect.width*0.691, y: rect.height*0.275, width: rect.width*0.1, height: rect.width*0.1), frameLabelMenor: CGRect(x: rect.width*0.691, y: rect.height*0.325, width: rect.width*0.1, height: rect.width*0.1), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), sizeMaior: self.frame.height*0.03, sizeMenor: self.frame.height*0.022)
        self.initTonalityTime(view: &viewAux2, labelMaior: &lblTimeSignature, labelMenor: &lblTitleTimeSignature, radius: rect.width*0.017, border: rect.width*0.0015, text: "Time", frameView: CGRect(x: rect.width*0.811, y: rect.height*0.3, width: rect.width*0.09, height: rect.width*0.09), frameLabelMaior: CGRect(x: rect.width*0.806, y: rect.height*0.275, width: rect.width*0.1, height: rect.width*0.1), frameLabelMenor: CGRect(x: rect.width*0.806, y: rect.height*0.325, width: rect.width*0.1, height: rect.width*0.1), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), sizeMaior: self.frame.height*0.03, sizeMenor: self.frame.height*0.022)
        
         self.initLabel(label: &lblTonality, text: "C/Am", frame: CGRect(x: rect.width*0.1, y: rect.height*0.35, width: rect.width*0.38, height: rect.height*0.2), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.0536, numberOfLines: 1, title: false)
        self.initLabel(label: &lblTitleTonality, text: "Tonality", frame: CGRect(x: rect.width*0.1, y: rect.height*0.4, width: rect.width*0.124, height: rect.height*0.2), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.6), size: self.frame.height*0.0321, numberOfLines: 1, title: true)
        
        self.initLabel(label: &lblTimeSignature, text: "4/4", frame: CGRect(x: rect.width*0.4, y: rect.height*0.35, width: rect.width*0.38, height: rect.height*0.2), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.0536, numberOfLines: 1, title: false)
        self.initLabel(label: &lblTitleTimeSignature, text: "Time", frame: CGRect(x: rect.width*0.38, y: rect.height*0.4, width: rect.width*0.12, height: rect.height*0.2), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.6), size: self.frame.height*0.0321, numberOfLines: 1, title: true)
        
//        self.initImageProfile(imageView: &imgStatusPatiture, frame: CGRect(x: rect.width*0.08, y: rect.height*0.86, width: rect.width*0.03, height: rect.width*0.03), image: #imageLiteral(resourceName: "Private"))
//        self.initImageProfile(imageView: &imgDownloadsPatiture, frame: CGRect(x: rect.width*0.08, y: rect.height*0.8, width: rect.width*0.03, height: rect.width*0.03), image: #imageLiteral(resourceName: "download"))
    
        self.initLabel(label: &lblStatusPatiture, text: "Public", frame: CGRect(x:rect.width*0.675, y: rect.height*0.588, width: rect.width*0.3, height: rect.height*0.05), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.025, numberOfLines: 1, title: false)
        self.initLabel(label: &lblNumbersDownloads, text: "10.000 downloads", frame: CGRect(x: rect.width*0.675, y: rect.height*0.79, width: rect.width*0.3, height: rect.height*0.05), textColor:  UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.025, numberOfLines: 1, title: false)
        self.initLabel(label: &lblTypePartiture, text: "Free Score", frame: CGRect(x: rect.width*0.675, y: rect.height*0.62, width: rect.width*0.3, height: rect.height*0.05), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.025, numberOfLines: 1, title: false)
        
        self.initLabel(label: &lblNameComposer, text: "Nome do compositor", frame:  CGRect(x: rect.width*0.675, y: rect.height*0.23, width: rect.width*0.325, height: rect.height*0.05), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.6), size: self.frame.height*0.025, numberOfLines: 1, title: false)
        
         self.initLabel(label: &lblSubTitleMusic, text: "Hava Nagila Hava", frame: CGRect(x: rect.width*0.6, y: rect.height*0.07, width: rect.width*0.4, height: rect.height*0.06), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.028, numberOfLines: 1, title: true)
        
        self.initLabel(label: &lblComposedBy, text: "Composed by", frame: CGRect(x: rect.width*0.675, y: rect.height*0.188, width: rect.width*0.325, height: rect.height*0.05), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.03, numberOfLines: 1, title: false)
        
        self.initLabel(label: &lblTitleMusic, text: "Nome da Musica \n Subtitulo da Musica", frame: CGRect(x: rect.width*0.59, y: 0, width: rect.width*0.4, height: rect.height*0.1), textColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), size: self.frame.height*0.035, numberOfLines: 1, title: true)
        
       

        
        
       
        self.initButton(button: &btnDelete, text: "Delete", frame: CGRect(x: rect.width*0.69, y: rect.height*0.9, width: rect.width*0.218, height: rect.height*0.07), backgroundColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), textColor: UIColor.white, borderColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), shadow: false)
       
        self.initButton(button: &btnOpen, text: "Open", frame: CGRect(x: rect.width*0.69, y: rect.height*0.45, width: rect.width*0.218, height: rect.height*0.07), backgroundColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), textColor: UIColor.white, borderColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), shadow: false)
        
        self.initButton(button: &btnDownload, text: "Download To MyMusic", frame: CGRect(x: rect.width*0.69, y: rect.height*0.9, width: rect.width*0.218, height: rect.height*0.07), backgroundColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), textColor: UIColor.white, borderColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), shadow: false)
        
        self.initButton(button: &btnSendToMyLibrary, text: "Send To My Music", frame:CGRect(x: rect.width*0.69, y: rect.height*0.8, width: rect.width*0.218, height: rect.height*0.07), backgroundColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), textColor: UIColor.white, borderColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), shadow: false)
        
        self.initButton(button: &btnNewArrangement, text: "New Arrangement", frame:CGRect(x: rect.width*0.69, y: rect.height*0.8, width: rect.width*0.218, height: rect.height*0.07), backgroundColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), textColor: UIColor.white, borderColor: UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0), shadow: false)
        self.isMyMusic()
        
        btnSendToMyLibrary.addTarget(self, action: #selector(actionBtnSendToMyLibrary(sender:)), for: UIControlEvents.touchUpInside)
    }
    
    @objc private func actionBtnSendToMyLibrary(sender: UIButton) {
        if let deleg = self.delegate {
            deleg.sendToMyLibrary()
        }
    }
    
    func initPopup(){
            backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 245/255, alpha: 0.95)
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowOpacity = 0.8
            layer.shadowRadius = 6
            layer.cornerRadius = 10
            clipsToBounds = true
            layer.borderColor = UIColor.black.cgColor
    }
    
    func isStore(){
        btnDelete.isHidden = true
        btnDelete.isEnabled = false
        btnSendToMyLibrary.isHidden = true
        btnSendToMyLibrary.isEnabled = false
     //   imgDownloadsPatiture.isHidden = false
        lblNumbersDownloads.isHidden = false
        btnDownload.isHidden = false
        btnDownload.isEnabled = true
        btnNewArrangement.isEnabled = false
        btnNewArrangement.isHidden = true
        btnOpen.isEnabled = false
    }
    func isMyCommpositions(){
        btnDelete.isHidden = false
        btnDelete.isEnabled = true
        btnSendToMyLibrary.isHidden = false
        btnSendToMyLibrary.isEnabled = true
        btnDownload.isHidden = true
        btnDownload.isEnabled = false
      //  imgDownloadsPatiture.isHidden = true
        lblNumbersDownloads.isHidden = true
        btnNewArrangement.isEnabled = false
        btnNewArrangement.isHidden = true
        btnOpen.isEnabled = true
    }
    func isMyMusic(){
        btnDelete.isHidden = false
        btnDelete.isEnabled = true
        btnSendToMyLibrary.isHidden = true
        btnSendToMyLibrary.isEnabled = false
        btnDownload.isHidden = true
        btnDownload.isEnabled = false
       // imgDownloadsPatiture.isHidden = true
        lblNumbersDownloads.isHidden = true
        btnNewArrangement.isEnabled = true
        btnNewArrangement.isHidden = false
        btnOpen.isEnabled = true
    }
    
    func initImageProfile(imageView: inout UIImageView!, frame: CGRect, image: UIImage){
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            self.addSubview(img)
            imageView = img
        }
        
    }
    
    func initTonalityTime(view: inout UIView!, labelMaior: inout UILabel!, labelMenor: inout UILabel!, radius : CGFloat, border : CGFloat, text: String, frameView: CGRect,frameLabelMaior: CGRect,frameLabelMenor: CGRect, textColor: UIColor, sizeMaior: CGFloat, sizeMenor: CGFloat){
        if view == nil {
            let vw = UIView(frame: frameView)
            vw.backgroundColor = UIColor.clear
            vw.layer.cornerRadius = radius
            vw.layer.borderColor = textColor.cgColor
            vw.layer.borderWidth = border
            self.addSubview(vw)
            view = vw
        }
        
        if labelMaior == nil {
            let lbl = UILabel(frame: frameLabelMaior)
            lbl.text = text
            lbl.textAlignment = .center
            lbl.backgroundColor = UIColor.clear
            lbl.font = UIFont(name: "Lato-Bold", size: sizeMaior)
            lbl.numberOfLines = 1
            lbl.lineBreakMode = .byWordWrapping
            lbl.textColor = textColor
            self.addSubview(lbl)
            labelMaior = lbl
        }
        if labelMenor == nil {
            let lbl = UILabel(frame: frameLabelMenor)
            lbl.text = text
            lbl.textAlignment = .center
            lbl.backgroundColor = UIColor.clear
            lbl.font = UIFont(name: "Lato-Bold", size: sizeMenor)
            lbl.numberOfLines = 1
            lbl.lineBreakMode = .byWordWrapping
            lbl.textColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.6)
            self.addSubview(lbl)
            labelMenor = lbl
        }
    }
    
    
    func initLabel(label: inout UILabel!, text: String, frame: CGRect, textColor: UIColor, size: CGFloat, numberOfLines: Int, title: Bool){
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            if(title){
                lbl.textAlignment = .center
            }else{
                lbl.textAlignment = .left
            }
            lbl.backgroundColor = UIColor.clear
            lbl.font = UIFont(name: "Lato-Bold", size: size)
            lbl.numberOfLines = numberOfLines
            lbl.lineBreakMode = .byWordWrapping
            lbl.textColor = textColor
            self.addSubview(lbl)
            label = lbl
        }
    }
    
    func initPopupVIew(){
        self.layer.cornerRadius = 3
        self.layer.shadowRadius = 3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.backgroundColor = UIColor.white
    }
    
    func initPartituraView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initCircleView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 28/255, green: 82/255, blue: 135/255, alpha: 0.6)
            stp.layer.cornerRadius = stp.frame.width/2
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initButton(button: inout UIButton!, text: String, frame: CGRect, backgroundColor : UIColor, textColor : UIColor, borderColor : UIColor, shadow : Bool) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setTitle(text, for: .normal)
            btn.layer.cornerRadius = 15
            btn.layer.borderWidth = 1
            btn.layer.borderColor = borderColor.cgColor
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            btn.contentHorizontalAlignment = .center
            btn.backgroundColor = backgroundColor
            btn.titleLabel?.numberOfLines = 2
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: self.frame.height*0.025)
            btn.setTitleColor(textColor, for: .normal)
            btn.titleLabel?.textAlignment = .center
            if (shadow){
                btn.layer.shadowColor = UIColor.black.cgColor
                btn.layer.shadowOffset = CGSize(width: 0, height: 2)
                btn.layer.shadowOpacity = 0.8
                btn.layer.shadowRadius = 3
                
            }
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
}
