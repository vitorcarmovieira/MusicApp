//
//  StoreView.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 01/04/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class StoreFilteredView: UIView {
    weak var stripeLeftNews     : UIView!
    weak var stripeRightNews     : UIView!
    weak var gambView : UIView!
    weak var homeController : HomeViewController!
    
    
    
    
    weak var lblNews: UILabel!
    weak var lblAllLibrary: UILabel!
    
    override func draw(_ rect: CGRect) {
        initStripeGambView(stripe: &gambView, frame:  CGRect(x: 0, y: 0, width: frame.width, height: frame.height*0.01))
        
        initStripeView(stripe: &stripeLeftNews, frame: CGRect(x: 0, y: frame.height*0.03, width: frame.width*0.4, height: frame.height*0.006))
        initStripeView(stripe: &stripeRightNews, frame: CGRect(x: frame.width*0.6, y: frame.height*0.03, width: frame.width*0.4, height: frame.height*0.006))
        
        initLabel(label: &lblNews, text: "News", frame: CGRect(x: frame.width*0.4, y: frame.height*0.019, width: frame.width*0.2, height: frame.height*0.025))
        
        initLabel(label: &lblAllLibrary, text: "All Library", frame: CGRect(x: frame.width*0.4, y: frame.height*0.34, width: frame.width*0.2, height: frame.height*0.025))
        
    }
    
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    func initStripeGambView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 208/255, green: 220/255, blue: 229/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    func initLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    func initHome(home: inout HomeViewController!){
        if homeController == nil {
            let hv = home
            homeController = hv
        }
    }
    
}

