//
//  InstrumentSelectedHomeView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class InstrumentSelectedHomeView: UIView {

    weak var imgInstrument : UIImageView!
    weak var lblInstrument : UILabel!
    
    var instrumentoName = "Piano" { didSet { setNeedsDisplay() } }
    var instrumentImage = UIImage() { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        let widthImg  = rect.width * 0.28
        let heightImg = widthImg
        
        let widthLbl  = rect.width
        let heightLbl = widthImg
        
        initializeImage(imageView: &imgInstrument, image: instrumentImage, frame: CGRect(x: 0, y: 0, width: widthImg, height: heightImg))
        initializeLabel(label: &lblInstrument, text: instrumentoName, frame: CGRect(x: widthImg * 1.2, y: 0, width: widthLbl, height: heightLbl))
    }

    func initializeImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            addSubview(img)
            imageView = img
        } else {
            imageView.frame = frame
            imageView.image = image
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor.white
            lbl.textAlignment = NSTextAlignment.left
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.5)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
            label.font = UIFont(name: "Lato-Regular", size: frame.height * 0.5)
        }
    }
}
