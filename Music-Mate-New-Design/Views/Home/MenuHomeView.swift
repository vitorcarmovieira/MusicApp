//
//  MenuHomeView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

//
///View responsável pela exibição do menu da tela de home
class MenuHomeView: UIView {
    
    ///Botão recent do menu
    weak var btnMyMusic  : UIButton!
    ///Botão artist do menu
    weak var btnShop  : UIButton!
    ///Botão title do menu
    weak var btnPerfil   : UIButton!
    ///Botão compose do menu
    weak var btnCompose : UIButton!
    ///Botão genre do menu
    weak var btnNewComposition : UIButton!
    weak var btnFilter : UIButton!
    ///Linha inferior dos botões do menu
    var band       : UIView   = UIView()
    
    ///Camada do botão recent do menu
    var layerRecent  = CAGradientLayer()
    ///Camada do artist recent do menu
    var layerArtist  = CAGradientLayer()
    ///Camada do title recent do menu
    var layerTitle   = CAGradientLayer()
    ///Camada do compose recent do menu
    var layerCompose = CAGradientLayer()
    ///Camada do genre recent do menu
    var layerGenre   = CAGradientLayer()
    ///Camada do country recent do menu
    var layerCountry = CAGradientLayer()
    
    ///Direção do menu (horizontal, vertical)
    var direction : DirectionMenu = DirectionMenu.horizontal { didSet { setNeedsDisplay() } }
    ///Função atualmente selecionada do menu
    var function  : FunctionMenu  = FunctionMenu.none { didSet { setNeedsDisplay() } }
    
    ///Fonte utilizada nos botões
    private var font = UIFont()
    
    override func draw(_ rect: CGRect) {
        //Calcula comprimento dos botões do menu
        let width = rect.width / 4
        //Calcula posição do primeiro botão
        let initialX = CGFloat(0.0)
        
        //---------------------Posiciona botões----------------------
        
        self.initButton(button: &btnFilter, image: #imageLiteral(resourceName: "filter"), frame: CGRect(x: rect.width*0.91, y: rect.width*0.16, width: rect.height*0.027, height: rect.width * 0.029))
        self.initButton(button: &btnMyMusic, image: #imageLiteral(resourceName: "mymusic_select"), frame: CGRect(x: (width * 0) + initialX, y: 0, width: width, height: rect.height * 0.1))
        self.initButton(button: &btnShop, image: #imageLiteral(resourceName: "store"), frame: CGRect(x: (width * 2) + initialX, y: 0, width: width, height: rect.height * 0.1))
        self.initButton(button: &btnPerfil, image: #imageLiteral(resourceName: "perfil"), frame: CGRect(x: (width * 3) + initialX, y: 0, width: width, height: rect.height * 0.1))
        self.initButton(button: &btnCompose, image: #imageLiteral(resourceName: "impost"), frame: CGRect(x: (width * 1) + initialX, y: 0, width: width, height: rect.height * 0.1))
        //self.initButtonNewComposer(button: &btnNewComposition, image: #imageLiteral(resourceName: "newCompose"), frame: CGRect(x:  rect.width*0.88, y: rect.height*0.88, width: rect.width*0.1, height: rect.width*0.1))
        //-----------------------------------------------------------
     //   self.btnShop.isEnabled = false
        //-----------------Posiciona faixa inferios------------------
        band.frame             = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
        band.backgroundColor    = UIColor(red: 55/255, green: 96/255, blue: 137/255, alpha: 1.0)
        band.layer.cornerRadius = 1
        //-----------------------------------------------------------

    }
   
    func initButtonNewComposer(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)

            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
            
        } else {
            button.frame = frame
        }
        
    }
    
    func initButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
            
        } else {
            button.frame = frame
        }
        
    }
    
    override func setNeedsDisplay() {
        super.setNeedsDisplay()
    }
    
    func initializeButton(button: inout UIButton!, text: String, frame: CGRect, layer: inout CAGradientLayer) {
        font = UIFont(name: "Lato-Regular", size: frame.height * 0.4)!
        
        if button == nil {
            let color1 = UIColor(red: 10/255, green: 78/255, blue: 148/255, alpha: 1.0).cgColor
            let color2 = UIColor(red: 40/255, green: 101/255, blue: 164/255, alpha: 1.0).cgColor
            
            let btn = UIButton(frame: frame)
            btn.addBorder()
            btn.addRadial()
            btn.createGradientLayer(layer: layer, colors: [color1, color2])
            btn.titleLabel!.font = font
            btn.setTitle(text, for: .normal)
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
            button.titleLabel?.font = font
        }
    }
    

}

extension UIView {
    func createGradientLayer(layer: CAGradientLayer, colors: [CGColor]) {
        layer.frame = self.bounds
        
        layer.colors = colors
        layer.locations = [0.5, 1.0]
        
        layer.startPoint = CGPoint(x: 0.75, y: 0.0)
        layer.endPoint = CGPoint(x: 0.75, y: 1.0)
        
        self.layer.addSublayer(layer)
    }
    
    func setFrameLayer(layer: CAGradientLayer, actived: Bool = false) {
        layer.frame = self.bounds
        
        if actived {
            let color1 = UIColor(red: 10/255, green: 78/255, blue: 175/255, alpha: 1.0).cgColor
            let color2 = UIColor(red: 40/255, green: 101/255, blue: 175/255, alpha: 1.0).cgColor
            layer.colors = [color1, color2]
        } else {
            let color1 = UIColor(red: 10/255, green: 78/255, blue: 148/255, alpha: 1.0).cgColor
            let color2 = UIColor(red: 40/255, green: 101/255, blue: 164/255, alpha: 1.0).cgColor
            layer.colors = [color1, color2]
        }
    }
    
    func addBorder() {
        layer.borderWidth = 2.3
        layer.borderColor = UIColor(red: 8/255, green: 46/255, blue: 86/255, alpha: 1.0).cgColor
    }
    
    func addRadial() {
        layer.cornerRadius = frame.width * 0.02
    }
}

enum FunctionMenu {
    case myMusic
    case myComposition
    case shop
    case profile
    case none
}
