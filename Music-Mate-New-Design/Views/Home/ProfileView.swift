//
//  ProfileView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 3/27/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

protocol ProfileViewDelegate: class {
    func changeCountry(_ newCountry: ModelCountry)
}

class ProfileView: UIView {
    @IBOutlet weak var imgProfile    : UIImageView!
    @IBOutlet weak var txtName       : UITextField!
    @IBOutlet weak var txtLasName    : UITextField!
    @IBOutlet weak var txtGender     : UITextField!
    @IBOutlet weak var txtBirthday   : UITextField!
    @IBOutlet weak var txtCountry    : UITextField!
    @IBOutlet weak var txtInterests  : UITextField!
    @IBOutlet weak var txtInstrument : UITextField!
    @IBOutlet weak var txtEmail      : UITextField!
    @IBOutlet weak var tbGender      : UITableView!
    @IBOutlet weak var tbCountry     : UITableView!
    @IBOutlet weak var btnChange     : UIButton!
    @IBOutlet weak var btnLogout     : UIButton!
    @IBOutlet weak var btnSave       : UIButton!
    @IBOutlet weak var btnCancel     : UIButton!
    
    weak var delegate: ProfileViewDelegate?
    
    var genders   = ["Male", "Female" ]
    var countries: [String] = []
    
    var modelsContries: [ModelCountry] = []
    
    override func draw(_ rect: CGRect) {
        
        modelsContries.removeAll()
        modelsContries = DAOCountry.getInstance().getAllCountries()
        
        countries.removeAll()
        
        for model in modelsContries {
            countries.append(model.name)
        }
        
        let width  = txtGender.bounds.width * 2.5
        let height = txtGender.frame.height
        
        txtName.leftViewMode       = .always
        txtLasName.leftViewMode    = .always
        txtGender.leftViewMode     = .always
        txtBirthday.leftViewMode   = .always
        txtCountry.leftViewMode    = .always
        txtInterests.leftViewMode  = .always
        txtInstrument.leftViewMode = .always
        txtEmail.leftViewMode      = .always
        
        txtName.leftView     = getLeftView(text: "First Name", widthTxtComplete: width, height: height)
        txtLasName.leftView    = getLeftView(text: "Last Name", widthTxtComplete: width, height: height)
        txtGender.leftView     = getLeftView(text: "Gender", widthTxtComplete: width, height: height)
        txtBirthday.leftView   = getLeftView(text: "Birthday", widthTxtComplete: width, height: height)
        txtCountry.leftView    = getLeftView(text: "Country", widthTxtComplete: width, height: height)
        txtInterests.leftView  = getLeftView(text: "Interests", widthTxtComplete: width, height: height)
        txtInstrument.leftView = getLeftView(text: "Change Instrument", widthTxtComplete: width * 1.2, height: height)
        txtEmail.leftView      = getLeftView(text: "Email", widthTxtComplete: width, height: height)
        
        tbCountry.dataSource = self
        tbGender.dataSource  = self
        
        tbCountry.delegate = self
        tbGender.delegate  = self
        
        
        tbCountry.register(UITableViewCell.self, forCellReuseIdentifier: "cellCountry")
        tbGender.register(UITableViewCell.self, forCellReuseIdentifier: "cellGender")
        
        tbGender.isHidden  = true
        tbCountry.isHidden = true
        
        txtBirthday.addTarget(self, action: #selector(applyFilterDateOnTextField(_:)), for: UIControlEvents.editingChanged)
    }

    @objc private func applyFilterDateOnTextField(_ field: UITextField) {
        let text = field.text!
        var textWithMask = ""
        
        if text.count - 1 >= 0{
            if isNumber(textToValidate: text[text.count - 1]) {
                for i in 0..<text.count {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i < 10 {
                        textWithMask += text[i]
                    }
                    
                }
            } else {
                for i in 0..<(text.count - 1) {
                    if i == 2 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    
                    if i == 5 && text[i] != "/" {
                        textWithMask += "/"
                    }
                    if i < 10 {
                        textWithMask += text[i]
                    }
                }
            }
        }
        
        
        
//        let validator = Validator()
        
//        if validator.validateDate(date: txtDate.text!){
//            txtDate.layer.borderColor = UIColor.clear.cgColor
//        }else{
//            txtDate.layer.borderColor = UIColor.red.cgColor
//        }
        field.text = textWithMask
    }
    
    func isNumber(textToValidate:String) -> Bool{
        
        let num = Int(textToValidate)
        
        if num != nil {
            return true
        }
        
        return false
    }
    
    func getLeftView(text: String, widthTxtComplete: CGFloat, height: CGFloat) -> UIView {
        let heightTxts = height
        let left = UIView(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        left.backgroundColor = UIColor(red: 8/255, green: 78/255, blue: 148/255, alpha: 1.0)
        left.layer.cornerRadius = heightTxts * 0.1
        
        let widthWhiteView = (heightTxts * 0.1)
        let xWhiteView = (widthTxtComplete * 0.2) - widthWhiteView
        let whiteView = UIView(frame: CGRect(x: xWhiteView, y: 0, width: widthWhiteView, height: heightTxts))
        whiteView.backgroundColor = UIColor.white
        left.addSubview(whiteView)
        
        let lblLeft = UILabel(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        lblLeft.text = text
        lblLeft.textColor = UIColor.white
        lblLeft.textAlignment = NSTextAlignment.center
        lblLeft.font = UIFont(name: "Lato-Bold", size: heightTxts * 0.35)
        left.addSubview(lblLeft)
        
        return left
    }

}

extension ProfileView: UITextFieldDelegate {
    
}

extension ProfileView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbGender {
            return genders.count
        }
        if tableView == tbCountry {
            return countries.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbGender {
            let cellGender = tableView.dequeueReusableCell(withIdentifier: "cellGender", for: indexPath)
            cellGender.textLabel?.text = genders[indexPath.row]
            
            return cellGender
        }
        if tableView == tbCountry {
            
            let cellCountry = tableView.dequeueReusableCell(withIdentifier: "cellCountry", for: indexPath)
            cellCountry.textLabel?.text = countries[indexPath.row]
            
            return cellCountry
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbGender {
            txtGender.text = genders[indexPath.row]
        }
        if tableView == tbCountry {
            txtCountry.text = countries[indexPath.row]
            if let deleg = delegate {
                deleg.changeCountry(modelsContries[indexPath.row])
            }
        }
        tableView.isHidden = true
    }
}
