//
//  InstrumentsCollectionViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/7/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class InstrumentsCollectionViewCell: UICollectionViewCell {
    
    var cardView:InstrumentView

    override init(frame: CGRect) {
        cardView = InstrumentView(frame: frame)

        super.init(frame: frame)
        self.contentView.addSubview(cardView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCardText(text:String){
        self.cardView.label.text = text
    }
    
    override func layoutSubviews() {
        self.cardView.frame = self.bounds
    }
    
    deinit {
        print("Entrou deinit InstrumentsCollectionViewCell")
    }
}
