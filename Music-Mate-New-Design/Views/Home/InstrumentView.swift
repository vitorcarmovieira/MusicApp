//
//  InstrumentView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/7/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class InstrumentView: UIView {
    
    var label:UILabel
    var image:UIImageView
    
    override init(frame: CGRect) {
        self.label               = UILabel()
        self.label.textAlignment = NSTextAlignment.center
        self.label.textColor     = UIColor.white
        
        self.image = UIImageView()
        self.image.image = #imageLiteral(resourceName: "instrument_piano")
        
        super.init(frame: frame)
        
        self.addSubview(self.label)
        self.addSubview(image)
        self.backgroundColor    = UIColor.clear
        self.layer.cornerRadius = 10.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.label.frame = CGRect(x: 0, y: frame.height * 0.75, width: frame.width, height: frame.height - (frame.height * 0.8) )
        self.image.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height - (frame.height * 0.2) )
        self.label.font = UIFont(name: "Lato-Regular", size: self.label.frame.height * 0.4)
    }
    
    deinit {
        print("Entrou deinit InstrumentView")
    }
}
