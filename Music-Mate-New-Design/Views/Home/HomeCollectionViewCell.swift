//
//  HomeCollectionViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/7/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var cardView       :HomeScoreView
    
    override init(frame: CGRect) {
        cardView = HomeScoreView(frame: frame)
        self.cardView.label.textColor = UIColor.black
       
        super.init(frame: frame)
        
        self.contentView.addSubview(cardView)
       // self.contentView.addSubview(deleteCellView)
        //
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCardText(text:String){
        self.cardView.label.text = text
    }
    func setCardPolyImage(image : UIImage){
        self.cardView.polyMonoImage.image = image
    }
    
    func setCardTypeImage(image : UIImage){
        self.cardView.typeImage.image = image
    }
    
    override func layoutSubviews() {
        self.cardView.frame = self.bounds

    }
}
