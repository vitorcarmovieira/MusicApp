//
//  MenuProfile.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 1/24/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class MenuProfile: UIView {

    ///Label sobre mim
    weak var lblAboutMe     : UILabel!
    ///Label sobre conta
    weak var lblMyAccount   : UILabel!
    
    ///Imagem perfil usuario
    weak var imgProfile       : UIImageView!
    ///Botao edit
    weak var btnEdit         : UIButton!
    
    /// Campo Instrument
    weak var txtInstrument      : UITextField!
    /// Campo Composer
    weak var txtName      : UITextField!
    /// Campo Country
    weak var txtCountry      : UITextField!
    /// Campo Gender
    weak var txtGender     : UITextField!
    /// Campo Birthday
    weak var txtBirthday      : UITextField!
    /// Campo Email
    weak var txtEmail      : UITextField!
    /// Campo Password1
    weak var txtPassword1      : UITextField!
    /// Campo Password2
    weak var txtPassword2      : UITextField!
    /// Campo Interested
    weak var txtInterested      : UITextField!
    
    ///Barra horizontal esquerda do sobre mim
    weak var barLAboutMe  : UIView!
    ///Barra horizontal direita do sobre mim
    weak var barRAboutMe  : UIView!
    
    ///Barra horizontal esquerda da conta
    weak var barLAccount  : UIView!
    ///Barra horizontal direita da conta
    weak var barRAccount  : UIView!
    
    override func draw(_ rect: CGRect) {
        let xIni = rect.width * 0.08
        let widthAbout = rect.width
        let height1 = rect.height * 0.095
        let height2 = rect.height * 0.082
        let height3 = rect.height * 0.10
        
        //--------------------Posiciona barras----------------------
        let yBar          = height2 * 0.475
        let widthBar      = rect.width / 3.0
        let heigthBar     = height2 * 0.05
        let rectLBarArtist = CGRect(x: 0.0, y: yBar, width: rect.width / 3.0, height: heigthBar)
        let rectRBarArtist = CGRect(x: rect.width - widthBar, y: yBar, width: widthBar, height: heigthBar)
        
        initBar(bar: &barLAboutMe, rect: rectLBarArtist)
        initBar(bar: &barRAboutMe, rect: rectRBarArtist)
    
        barLAboutMe.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        barRAboutMe.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        let color = UIColor(red: 14/255, green: 86/255, blue: 160/255, alpha: 1.0)
        let rectAboutArtist = CGRect(x: 0, y: 0, width: widthAbout, height: height2)
        
        initLabel(label: &lblAboutMe, text: "About Me", frame: rectAboutArtist, color: color, prop: 0.25)
        //----------------------------------------------------------
        
        //----------------Posiciona imagem usuario------------------
        let yImg      = height2
        let widthImg  = (height2 * 0.6) * 3.0
        let heightImg = (height2 * 0.6) * 3.0
        let rectImg   = CGRect(x: xIni, y: yImg, width: widthImg, height: heightImg)
        
        initImage(imageView: &imgProfile, image: #imageLiteral(resourceName: "profile_bg_profile"), frame: rectImg)
        //----------------------------------------------------------
        
        //------------------Posiciona botao edit--------------------
        let yBtn      = height2
        let widthBtn  = height2 * 2.5
        let xBtn      = rect.width - xIni - widthBtn
        let heightBtn = height2 * 0.6
        let rectBtn   = CGRect(x: xBtn, y: yBtn, width: widthBtn, height: heightBtn)
        
        initButton(button: &btnEdit, text: "Edit", frame: rectBtn, prop: 0.8)
        addBorderBlue(field: &btnEdit)
        //----------------------------------------------------------
        
        //---------------Posiciona botao instrument-----------------
        let widthBtn1  = rect.width - widthImg - (3.0 * xIni)
        let xBtn1     = widthImg + (2.0 * xIni)
        let heightBtn1 = height2 * 0.6
        let yBtn1      = yImg + heightImg - heightBtn1
        let rectBtn1   = CGRect(x: xBtn1, y: yBtn1, width: widthBtn1, height: heightBtn1)
        
        initTextField(field: &txtInstrument, text: "Change Instrument", frame: rectBtn1, widthComplete: (rect.width - (2.0 * xIni)) * 1.5)
        //----------------------------------------------------------
        
        //-----------------Posiciona txts sobre mim-----------------
        let widthTxtComplete = rect.width - (2.0 * xIni)
        let heightTxts       = height2 * 0.6
        let widthMidle       = (rect.width - (2.0 * xIni)) / 2.3
        let xMidle           = rect.width / 1.8
        
        let yTxtName        = height2 + height3 + (xIni * 1.3)
        let yTxtCountry     = yTxtName + height2 + (xIni * 0.1)
        let yTxtGender      = yTxtCountry + height2 + (xIni * 0.1)

        let rectTxtName      = CGRect(x: xIni, y: yTxtName, width: widthTxtComplete, height: heightTxts)
        let rectTxtCountry   = CGRect(x: xIni, y: yTxtCountry, width: widthTxtComplete, height: heightTxts)
        let rectTxtGender    = CGRect(x: xIni, y: yTxtGender, width: widthMidle, height: heightTxts)
        let rectTxtBirthday  = CGRect(x: xMidle, y: yTxtGender, width: widthMidle, height: heightTxts)

        initTextField(field: &txtName, text: "Name", frame: rectTxtName, widthComplete: widthTxtComplete)
        
        initTextField(field: &txtCountry, text: "Country", frame: rectTxtCountry, widthComplete: widthTxtComplete)
        
        initTextField(field: &txtGender, text: "Gender", frame: rectTxtGender, widthComplete: widthTxtComplete)
        
        initTextField(field: &txtBirthday, text: "Birthday", frame: rectTxtBirthday, widthComplete: widthTxtComplete)

        //----------------------------------------------------------
        
        //--------------------Posiciona barras----------------------
        let yBarA            = yTxtGender + (height1 * 0.7) + (height2 * 0.475)
        let rectLBarAccount  = CGRect(x: 0.0, y: yBarA, width: rect.width / 3.0, height: heigthBar)
        let rectRBarAccount  = CGRect(x: rect.width - widthBar, y: yBarA, width: widthBar, height: heigthBar)
        
        initBar(bar: &barLAccount, rect: rectLBarAccount)
        initBar(bar: &barRAccount, rect: rectRBarAccount)
        
        barLAccount.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        barRAccount.backgroundColor = UIColor(red: 176 / 255, green: 202 / 255, blue: 228 / 255, alpha: 1.0)
        //----------------------------------------------------------
        
        //--------------------Label About Score---------------------
        let yLblS          = yTxtGender + (height1 * 0.8)
        let rectAccount    = CGRect(x: 0, y: yLblS, width: widthAbout, height: height2)
        
        initLabel(label: &lblMyAccount, text: "My Account", frame: rectAccount, color: color, prop: 0.25)
        //----------------------------------------------------------
        
        //---------------Posiciona txts sobre conta-----------------
        let yTxtEmail      = yLblS + height2 + (xIni * 0.1)
        let yTxtPassword1  = yTxtEmail + height2 + (xIni * 0.1)
        let yTxtPassword2  = yTxtPassword1 + height2 + (xIni * 0.1)
        let yTxtInterested = yTxtPassword2 + height2 + (xIni * 0.1)
        
        let rectTxtEmail      = CGRect(x: xIni, y: yTxtEmail, width: widthTxtComplete, height: heightTxts)
        let rectTxtPassord1   = CGRect(x: xIni, y: yTxtPassword1, width: widthTxtComplete, height: heightTxts)
        let rectTxtPassord2   = CGRect(x: xIni, y: yTxtPassword2, width: widthTxtComplete, height: heightTxts)
        let rectTxtInterested = CGRect(x: xIni, y: yTxtInterested, width: widthTxtComplete, height: heightTxts)
        
        initTextField(field: &txtEmail, text: "E-mail", frame: rectTxtEmail, widthComplete: widthTxtComplete)
        initTextField(field: &txtPassword1, text: "Password", frame: rectTxtPassord1, widthComplete: widthTxtComplete)
        initTextField(field: &txtPassword2, text: "Password", frame: rectTxtPassord2, widthComplete: widthTxtComplete)
        initTextField(field: &txtInterested, text: "Interested in", frame: rectTxtInterested, widthComplete: widthTxtComplete)
        //----------------------------------------------------------
    }

    func addBorderBlue(field: inout UIButton!) {
        field.layer.cornerRadius = 5
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
    }
    
    func initTextField(field: inout UITextField!, text: String, frame: CGRect, widthComplete: CGFloat) {
        if field == nil {
            let f = UITextField(frame: frame)
            f.backgroundColor = UIColor.white
            f.layer.cornerRadius = frame.height * 0.1
            f.leftViewMode = .always
//            f.delegate = self
            f.leftView = getLeftView(text: text, widthTxtComplete: widthComplete, height: frame.height)
            addSubview(f)
            field = f
        } else {
            field.frame = frame
        }
    }
    //
    func getLeftView(text: String, widthTxtComplete: CGFloat, height: CGFloat) -> UIView {
        let heightTxts = height
        let left = UIView(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        left.backgroundColor = UIColor(red: 8/255, green: 78/255, blue: 148/255, alpha: 1.0)
        left.layer.cornerRadius = heightTxts * 0.1
        
        let widthWhiteView = (heightTxts * 0.1)
        let xWhiteView = (widthTxtComplete * 0.2) - widthWhiteView
        let whiteView = UIView(frame: CGRect(x: xWhiteView, y: 0, width: widthWhiteView, height: heightTxts))
        whiteView.backgroundColor = UIColor.white
        left.addSubview(whiteView)
        
        let lblLeft = UILabel(frame: CGRect(x: 0, y: 0, width: widthTxtComplete * 0.2, height: heightTxts))
        lblLeft.text = text
        lblLeft.textColor = UIColor.white
        lblLeft.textAlignment = NSTextAlignment.center
        lblLeft.font = UIFont(name: "Lato-Bold", size: heightTxts * 0.35)
        left.addSubview(lblLeft)
        
        return left
    }
    
    func initButton(button: inout UIButton!, text: String, frame: CGRect, prop: CGFloat) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setTitle(text, for: .normal)

            btn.contentHorizontalAlignment = .center
            btn.titleLabel?.font = UIFont(name: "Lato-Regular", size: prop)
            btn.titleLabel?.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            btn.setTitleColor(UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0), for: .normal)
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let img = UIImageView(frame: frame)
            img.image = image
            addSubview(img)
            imageView = img
        } else {
            imageView.frame = frame
            imageView.image = image
        }
    }
    
    func initLabel(label: inout UILabel!, text: String, frame: CGRect, color: UIColor, prop: CGFloat, align: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = color
            lbl.textAlignment = align
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * prop)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initBar(bar: inout UIView!, rect: CGRect) {
        if bar == nil {
            let b = UIView(frame: rect)
            b.backgroundColor = UIColor(red: 201/255, green: 201/255, blue: 201/255, alpha: 1.0)
            
            addSubview(b)
            bar = b
        } else {
            bar.frame = frame
        }
    }
    
}
