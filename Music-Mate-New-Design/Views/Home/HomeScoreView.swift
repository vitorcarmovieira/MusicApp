//
//  HomeScoreView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/7/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class HomeScoreView: UIView {
    
    var label:UILabel
    var backgroundImage:UIImageView
    var typeImage:UIImageView
    var polyMonoImage:UIImageView
    
    //
    override init(frame: CGRect) {
        self.label               = UILabel()
        self.label.textAlignment = NSTextAlignment.center
        self.label.textColor     = UIColor.white
        
        self.backgroundImage = UIImageView()
        self.backgroundImage.image = #imageLiteral(resourceName: "partitura")
       // self.backgroundImage.contentMode = .scaleAspectFit
        
        self.typeImage = UIImageView()
        self.typeImage.contentMode = .scaleAspectFit
        self.typeImage.image = #imageLiteral(resourceName: "arrangement")
        
        self.polyMonoImage = UIImageView()
        self.polyMonoImage.image = #imageLiteral(resourceName: "poli_icon")
        self.polyMonoImage.contentMode = .scaleAspectFit
        
        super.init(frame: frame)
        self.addSubview(self.backgroundImage)
        self.addSubview(self.label)
        self.addSubview(self.typeImage)
        self.addSubview(self.polyMonoImage)
      
        self.backgroundColor    = UIColor.clear
        self.layer.cornerRadius = 10.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundImage.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        self.typeImage.frame = CGRect(x: frame.width*0.14, y: frame.height*0.665, width: frame.width*0.2666, height: frame.width*0.2666)
        self.polyMonoImage.frame = CGRect(x: frame.width*0.54, y: frame.height*0.665, width: frame.width*0.2666, height: frame.width*0.2666)
        
        self.label.frame = CGRect(x: frame.width*0.06, y: frame.height * 0.03, width: frame.width*0.85, height: frame.height*0.6 )
        self.label.font = UIFont(name: "Lato-Regular", size: self.label.frame.height * 0.15)
        self.label.numberOfLines = 5
        self.label.lineBreakMode = .byTruncatingTail
    }
    
}
