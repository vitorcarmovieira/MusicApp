//
//  SearchView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/6/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class SearchView: UIView {

    weak var searchBar: UISearchBar!
    
    var direction : DirectionMenu = DirectionMenu.horizontal { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        initializeImage (search: &searchBar, image: #imageLiteral(resourceName: "barra_buscar"), frame: CGRect(x: rect.width*0.1, y: rect.height*0.11, width: rect.width*0.8, height: rect.height*0.03))
      self.backgroundColor = UIColor.clear
    }

    func initializeImage(search: inout UISearchBar!, image: UIImage, frame: CGRect) {
        if search == nil {
            let searchBarr = UISearchBar(frame: frame)
            searchBarr.placeholder = "Search here"
            searchBarr.tintColor = UIColor(red: 154/255, green: 191/255, blue: 228/255, alpha: 1.0)
            searchBarr.barStyle = .default
            searchBarr.isTranslucent = false
            searchBarr.searchBarStyle = .prominent
            searchBarr.barTintColor =  UIColor.white
            addSubview(searchBarr)
            search = searchBarr
 
        } else {
            search.frame = frame
        }
    }
}


