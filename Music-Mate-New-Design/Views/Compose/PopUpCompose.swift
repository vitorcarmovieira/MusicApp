//
//  PopUpCompose.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/31/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import MediaPlayer

protocol PopUpComposeDelegate: class {
    func changeMetronome(_ value: Int)
    func changeTimeSignature(_ option: TimeSelected)
    func changeTitle(_ title: String)
    func changeKey(_ key: KeyType)
    func transpose(_ option: OptionKeySelected)
    func changeQuantization(_ quantization : Int)
}

class PopUpCompose: UIView {
    
    weak var delegate: PopUpComposeDelegate?
    
    var metronomeContent :[String] = [String]()
    var transposeContent : [String] = [String]()
    var quantizationContent : [UIImage] = [UIImage]()
    var lblTransposeContent  :[String] = [String]()
    var lblTimeSignatureContent  :[String] = [String]()
    var timeSignatureContent :[UIImage] = [UIImage]()
    
    weak var lblSoundMetronome : UILabel!
    weak var lblMinimizeCursor : UILabel!
    weak var lblQuantization   : UILabel!
    weak var lblTranspose      : UILabel!
    weak var lblTransposeValue : UILabel!
    weak var lblTimeSignature  : UILabel!
    weak var lblMetronome      : UILabel!
    weak var lblMetronomeTxt   : UILabel!
    
    weak var btnplusOctaveTranspose    : UIButton!
    weak var btnMinusOctaveTranspose : UIButton!
    weak var btnNextTimeSignature     : UIButton!
    weak var btnPreviousTimeSignature : UIButton!
    weak var btnNextQuantization     : UIButton!
    weak var btnPreviousQuantization : UIButton!
    weak var btnPlusTranspose     : UIButton!
    weak var btnMinusTranspose      : UIButton!
    weak var btnPlusTenMetronome      : UIButton!
    weak var btnMinusTenMetronome      : UIButton!
    weak var btnDone                  : UIButton!
    
    var pickerMetronome     : UIPickerView!
    var pickerTranspose     : UIPickerView!
    var pickerTimeSignature     : UIPickerView!
    var pickerQuantization    : UIPickerView!
    
    weak var stripeView1 : UIView!
    weak var stripeView2 : UIView!
    weak var stripeView3 : UIView!
    weak var stripeView4 : UIView!
    
    weak var swtSoundCursor    : UISwitch!
    weak var swtMinimizeCursor : UISwitch!
    
    var widPickr: CGFloat  = 0.0
    var hegtPickr: CGFloat = 0.0
    var widPickrr: CGFloat  = 0.0
    var hegtPickrr: CGFloat = 0.0
    
    var metronomeValue : Int!
    
    override func draw(_ rect: CGRect) {
        metronomeContent = ["0","1","2","3","4","5","6","7","8","9"]
        lblTimeSignatureContent = ["2/2","3/2","2/4","3/4","4/4","5/4","6/4","3/8","6/8","9/8","12/8"]
        quantizationContent = [#imageLiteral(resourceName: "quantiz14"), #imageLiteral(resourceName: "quantiz18"), #imageLiteral(resourceName: "quantz116")]
        lblTransposeContent = ["C/Am","Db/Bbm","D/Bm","Eb/Cm","E/C#m","F/Dm","F#/D#m","G/Em","Ab/Fm","A/F#m","Bb/Gm","B/G#m"]
        timeSignatureContent = [#imageLiteral(resourceName: "btn_menu_edit_general_time_2_2"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_2"), #imageLiteral(resourceName: "btn_menu_edit_general_time_2_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_4_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_5_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_6_4"), #imageLiteral(resourceName: "btn_menu_edit_general_time_3_8"), #imageLiteral(resourceName: "btn_menu_edit_general_time_9_8-1"), #imageLiteral(resourceName: "btn_menu_edit_general_time_9_8"), #imageLiteral(resourceName: "btn_menu_edit_general_time_12_8")]
        transposeContent = ["C/Am","Db/Bbm","D/Bm","Eb/Cm","E/C#m","F/Dm","F#/D#m","G/Em","Ab/Fm","A/F#m","Bb/Gm","B/G#m"]
        
        let widthSwitch  = frame.width * 0.1
        let heightSwitch = frame.height * 0.4
        let border = frame.width * 0.03
        
        initializeSwitch(switchE: &swtSoundCursor, isOn: true, frame: CGRect(x: frame.width*0.77, y: frame.height * 0.029, width: widthSwitch, height: heightSwitch))
        initializeSwitch(switchE: &swtMinimizeCursor, isOn: false, frame: CGRect(x: frame.width*0.77, y: frame.height * 0.075, width: widthSwitch, height: heightSwitch))
        
        initStripeView(stripe: &stripeView1, frame: CGRect(x: 0, y: frame.height * 0.125, width: frame.width, height: frame.height*0.001))
        initStripeView(stripe: &stripeView2, frame: CGRect(x: 0, y: frame.height * 0.268, width: frame.width, height: frame.height*0.001))
        initStripeView(stripe: &stripeView3, frame: CGRect(x: 0, y: frame.height * 0.41, width: frame.width, height: frame.height*0.001))
        initStripeView(stripe: &stripeView4, frame: CGRect(x: 0, y: frame.height * 0.557, width: frame.width, height: frame.height*0.001))
        
        initializeLabelBold(label: &lblSoundMetronome, text: "Sound Metronome", frame: CGRect(x: (border*3.0), y: frame.height * 0.021, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblMinimizeCursor, text: "Minimize Cursor", frame: CGRect(x: (border*3.0), y: frame.height * 0.068, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblTranspose, text: "Transpose", frame: CGRect(x: (border*3.0), y: frame.height * 0.138, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblTimeSignature, text: "Time Signature", frame: CGRect(x: (border*3.0), y: frame.height * 0.284, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblMetronomeTxt, text: "Metronome", frame: CGRect(x: (border*3.0), y: frame.height * 0.428, width: frame.width, height: frame.height * 0.035))
        initializeLabelBold(label: &lblQuantization, text: "Quantization", frame: CGRect(x: (border*3.0), y: frame.height * 0.5735, width: frame.width, height: frame.height * 0.035))
        initializeLabelSmall(label: &lblMetronome, text: "90", frame: CGRect(x:frame.width*0.45, y: frame.height * 0.461, width: frame.height*0.04, height: frame.height*0.03
        ))
        
        initializeButton(button: &btnplusOctaveTranspose, image: #imageLiteral(resourceName: "8va"),frame: CGRect(x: frame.width*0.79, y: frame.height * 0.1955, width: frame.height*0.028, height: frame.height*0.028
        ))
        initializeButton(button: &btnMinusOctaveTranspose, image: #imageLiteral(resourceName: "8vb"),frame: CGRect(x: (border*3.0), y: frame.height * 0.1955, width: frame.height*0.028, height: frame.height*0.028
        ))
        initializeButton(button: &btnNextTimeSignature, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_right"), frame: CGRect(x: frame.width*0.79, y: frame.height * 0.339, width: frame.height*0.028, height: frame.height*0.028
        ))
        initializeButton(button: &btnPreviousTimeSignature, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_left"), frame: CGRect(x: (border*3.0), y: frame.height * 0.339, width: frame.height*0.028, height: frame.height*0.028
        ))
        
        initializeButton(button: &btnNextQuantization, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_right"), frame: CGRect(x: frame.width*0.79, y: frame.height * 0.635, width: frame.height*0.028, height: frame.height*0.028
        ))
        initializeButton(button: &btnPreviousQuantization, image: #imageLiteral(resourceName: "btn_menu_edit_general_select_left"), frame: CGRect(x: (border*3.0), y: frame.height * 0.635, width: frame.height*0.028, height: frame.height*0.028
        ))
        
        initializeButton(button: &btnMinusTenMetronome, image: #imageLiteral(resourceName: "menos"), frame: CGRect(x: (border*3.0), y: frame.height * 0.4952, width: frame.height*0.025, height: frame.height*0.025
        ))
        initializeButton(button: &btnPlusTenMetronome, image: #imageLiteral(resourceName: "mais"), frame: CGRect(x: frame.width*0.79, y: frame.height * 0.4952, width: frame.height*0.025, height: frame.height*0.025
        ))
        initializeButtonCancelDone(button: &btnDone, image: #imageLiteral(resourceName: "botao"), txt: "Done", frame: CGRect(x: frame.width * 0.2, y: frame.height * 0.898, width: frame.width*0.6, height: frame.height*0.0438))
        
        pickerTranspose = UIPickerView(frame: CGRect(x: frame.width*0.4, y: frame.height * 0.141, width: frame.height*0.05, height: frame.width*0.5))
        widPickr  = pickerTranspose.frame.width
        hegtPickr = pickerTranspose.frame.height
        pickerTranspose.dataSource = self
        pickerTranspose.delegate = self
        addSubview(pickerTranspose)
        pickerTranspose.backgroundColor = UIColor.white
        pickerTranspose.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerTranspose.layer.borderWidth = 1
        pickerTranspose.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        pickerTranspose.selectRow(5, inComponent: 0, animated: false)
        
        
        
        pickerTimeSignature = UIPickerView(frame: CGRect(x: frame.width*0.4, y: frame.height * 0.286, width: frame.height*0.05, height: frame.width*0.5))
        widPickrr  = pickerTimeSignature.frame.width
        hegtPickrr = pickerTimeSignature.frame.height
        pickerTimeSignature.dataSource = self
        pickerTimeSignature.delegate = self
        addSubview(pickerTimeSignature)
        pickerTimeSignature.backgroundColor = UIColor.white
        pickerTimeSignature.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerTimeSignature.layer.borderWidth = 1
        pickerTimeSignature.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        pickerTimeSignature.selectRow(4, inComponent: 0, animated: false)
        
        pickerMetronome = UIPickerView(frame: CGRect(x: frame.width*0.44, y: frame.height * 0.44, width: frame.height*0.03, height: frame.width*0.5))
        pickerMetronome.dataSource = self
        pickerMetronome.delegate = self
        addSubview(pickerMetronome)
        pickerMetronome.backgroundColor = UIColor.white
        pickerMetronome.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerMetronome.layer.borderWidth = 1
        pickerMetronome.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        
        pickerQuantization = UIPickerView(frame: CGRect(x: frame.width*0.4, y: frame.height * 0.58, width: frame.height*0.05, height: frame.width*0.5))
        pickerQuantization.dataSource = self
        pickerQuantization.delegate = self
        addSubview(pickerQuantization)
        pickerQuantization.backgroundColor = UIColor.white
        pickerQuantization.layer.borderColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0).cgColor
        pickerQuantization.layer.borderWidth = 1
        pickerQuantization.transform = CGAffineTransform(rotationAngle: -90*(.pi/180))
        
        btnDone.addTarget(self, action: #selector(actionBtnClose(sender:)), for: .touchUpInside)
        btnPreviousQuantization.addTarget(self, action: #selector(actionBtnPreviousQuantization(sender:)), for: .touchUpInside)
        btnNextQuantization.addTarget(self, action: #selector(actionBtnNextQuantization(sender:)), for: .touchUpInside)
    }
    
    func previousKeySignature() {
        if(pickerTranspose.selectedRow(inComponent: 0).hashValue-1 > 0){
            pickerTranspose.selectRow(pickerTranspose.selectedRow(inComponent: 0).hashValue-1, inComponent: 0, animated: true)
        }else {
            pickerTranspose.selectRow(transposeContent.count-1, inComponent: 0, animated: true)
        }
        
        switch pickerTranspose.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeKey(KeyType.Db_BBm)
        case 1:
            delegate?.changeKey(KeyType.Ab_Fm)
        case 2:
            delegate?.changeKey(KeyType.Eb_Cm)
        case 3:
            delegate?.changeKey(KeyType.Bb_Gm)
        case 4:
            delegate?.changeKey(KeyType.F_Dm)
        case 5:
            delegate?.changeKey(KeyType.C_Am)
        case 6:
            delegate?.changeKey(KeyType.G_Em)
        case 7:
            delegate?.changeKey(KeyType.D_Bm)
        case 8:
            delegate?.changeKey(KeyType.A_Fsm)
        case 9:
            delegate?.changeKey(KeyType.E_Csm)
        case 10:
            delegate?.changeKey(KeyType.B_Gsm)
        case 11:
            delegate?.changeKey(KeyType.Fs_Dsm)
        default:
            break
        }
        
        lblTransposeValue.text = lblTransposeContent[pickerTranspose.selectedRow(inComponent: 0).hashValue]
    }
    func nextKeySignature() {
        if(pickerTranspose.selectedRow(inComponent: 0).hashValue+1 < transposeContent.count){
            pickerTranspose.selectRow(pickerTranspose.selectedRow(inComponent: 0).hashValue+1, inComponent: 0, animated: true)
        }else {
            pickerTranspose.selectRow(0, inComponent: 0, animated: true)
        }
        
        switch pickerTranspose.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeKey(KeyType.Db_BBm)
        case 1:
            delegate?.changeKey(KeyType.Ab_Fm)
        case 2:
            delegate?.changeKey(KeyType.Eb_Cm)
        case 3:
            delegate?.changeKey(KeyType.Bb_Gm)
        case 4:
            delegate?.changeKey(KeyType.F_Dm)
        case 5:
            delegate?.changeKey(KeyType.C_Am)
        case 6:
            delegate?.changeKey(KeyType.G_Em)
        case 7:
            delegate?.changeKey(KeyType.D_Bm)
        case 8:
            delegate?.changeKey(KeyType.A_Fsm)
        case 9:
            delegate?.changeKey(KeyType.E_Csm)
        case 10:
            delegate?.changeKey(KeyType.B_Gsm)
        case 11:
            delegate?.changeKey(KeyType.Fs_Dsm)
        default:
            break
        }
        
        lblTransposeValue.text = lblTransposeContent[pickerTranspose.selectedRow(inComponent: 0).hashValue]
    }
    func setInitialTonality(tom : Int){
        switch tom {
        case 1:
            pickerTranspose.selectRow(0, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.C)
        case 2:
            pickerTranspose.selectRow(1, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Db)
        case 3:
            pickerTranspose.selectRow(1, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Db)
        case 4:
            pickerTranspose.selectRow(2, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.D)
        case 5:
            pickerTranspose.selectRow(3, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Eb)
        case 6:
            pickerTranspose.selectRow(4, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.E)
        case 7:
            pickerTranspose.selectRow(5, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.F)
        case 8:
            pickerTranspose.selectRow(6, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Fs)
        case 9:
            pickerTranspose.selectRow(6, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Fs)
        case 10:
            pickerTranspose.selectRow(7, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.G)
        case 11:
            pickerTranspose.selectRow(8, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Ab)
        case 12:
            pickerTranspose.selectRow(9, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.A)
        case 13:
            pickerTranspose.selectRow(10, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.Bb)
        case 14:
            pickerTranspose.selectRow(11, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.B)
        default:
            pickerTranspose.selectRow(0, inComponent: 0, animated: false)
            delegate?.transpose(OptionKeySelected.C)
        }
    }
    
    func setInitialQuantization(quantization : Int){
        switch quantization {
        case 4:
            pickerQuantization.selectRow(0, inComponent: 0, animated: false)
        case 8:
            pickerQuantization.selectRow(1, inComponent: 0, animated: false)
        case 16:
            pickerQuantization.selectRow(2, inComponent: 0, animated: false)
        default:
            pickerQuantization.selectRow(1, inComponent: 0, animated: false)
        }
    }
    
    @objc func actionBtnPreviousQuantization(sender: UIButton) {
        if(pickerQuantization.selectedRow(inComponent: 0).hashValue-1 >= 0){
            pickerQuantization.selectRow(pickerQuantization.selectedRow(inComponent: 0).hashValue-1, inComponent: 0, animated: true)
        }else {
            pickerQuantization.selectRow(quantizationContent.count-1, inComponent: 0, animated: true)
        }
        switch pickerQuantization.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeQuantization(4)
        case 1:
            delegate?.changeQuantization(8)
        case 2:
            delegate?.changeQuantization(16)
        default:
            delegate?.changeQuantization(4)
        }
    }
    
    @objc func actionBtnNextQuantization(sender: UIButton) {
        if(pickerQuantization.selectedRow(inComponent: 0).hashValue+1 < quantizationContent.count){
            pickerQuantization.selectRow(pickerQuantization.selectedRow(inComponent: 0).hashValue+1, inComponent: 0, animated: true)
        }else {
            pickerQuantization.selectRow(0, inComponent: 0, animated: true)
        }
        switch pickerQuantization.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeQuantization(4)
        case 1:
            delegate?.changeQuantization(8)
        case 2:
            delegate?.changeQuantization(16)
        default:
            delegate?.changeQuantization(4)
        }
    }
    
    func nextTimeSignature() {
        if(pickerTimeSignature.selectedRow(inComponent: 0).hashValue+1 < timeSignatureContent.count){
            pickerTimeSignature.selectRow(pickerTimeSignature.selectedRow(inComponent: 0).hashValue+1, inComponent: 0, animated: true)
        }else {
            pickerTimeSignature.selectRow(0, inComponent: 0, animated: true)
        }
        switch pickerTimeSignature.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeTimeSignature(TimeSelected.doisdois)
        case 1:
            delegate?.changeTimeSignature(TimeSelected.tresdois)
        case 2:
            delegate?.changeTimeSignature(TimeSelected.doisQuartos)
        case 3:
            delegate?.changeTimeSignature(TimeSelected.tresQuartos)
        case 4:
            delegate?.changeTimeSignature(TimeSelected.quatroQuartos)
        case 5:
            delegate?.changeTimeSignature(TimeSelected.cincoQuartos)
        case 6:
            delegate?.changeTimeSignature(TimeSelected.seisQuartos)
        case 7:
            delegate?.changeTimeSignature(TimeSelected.tresOitavos)
        case 8:
            delegate?.changeTimeSignature(TimeSelected.seisOitavos)
        case 9:
            delegate?.changeTimeSignature(TimeSelected.noveOitavos)
        case 10:
            delegate?.changeTimeSignature(TimeSelected.dozeOitavos)
        default:
            break
        }
        
        
    }
    
    func previousTimeSignature() {
        if(pickerTimeSignature.selectedRow(inComponent: 0).hashValue-1 >= 0){
            pickerTimeSignature.selectRow(pickerTimeSignature.selectedRow(inComponent: 0).hashValue-1, inComponent: 0, animated: true)
        }else {
            pickerTimeSignature.selectRow(timeSignatureContent.count-1, inComponent: 0, animated: true)
        }
        switch pickerTimeSignature.selectedRow(inComponent: 0) {
        case 0:
            delegate?.changeTimeSignature(TimeSelected.doisdois)
        case 1:
            delegate?.changeTimeSignature(TimeSelected.tresdois)
        case 2:
            delegate?.changeTimeSignature(TimeSelected.doisQuartos)
        case 3:
            delegate?.changeTimeSignature(TimeSelected.tresQuartos)
        case 4:
            delegate?.changeTimeSignature(TimeSelected.quatroQuartos)
        case 5:
            delegate?.changeTimeSignature(TimeSelected.cincoQuartos)
        case 6:
            delegate?.changeTimeSignature(TimeSelected.seisQuartos)
        case 7:
            delegate?.changeTimeSignature(TimeSelected.tresOitavos)
        case 8:
            delegate?.changeTimeSignature(TimeSelected.seisOitavos)
        case 9:
            delegate?.changeTimeSignature(TimeSelected.noveOitavos)
        case 10:
            delegate?.changeTimeSignature(TimeSelected.dozeOitavos)
        default:
            break
        }
    }
    
    func getTimeSignature(_ numerador: Int,_ denominador : Int){
        if (numerador == 2 && denominador == 2){
            pickerTimeSignature.selectRow(0, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.doisdois)
        }
        else if (numerador == 3 && denominador == 2){
            pickerTimeSignature.selectRow(1, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.tresdois)
        }
        else if (numerador == 2 && denominador == 4){
            pickerTimeSignature.selectRow(2, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.doisQuartos)
        }
        else if (numerador == 3 && denominador == 4){
            pickerTimeSignature.selectRow(3, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.tresQuartos)
        }
        else if (numerador == 4 && denominador == 4){
            pickerTimeSignature.selectRow(4, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.quatroQuartos)
        }
        else if (numerador == 5 && denominador == 4){
            pickerTimeSignature.selectRow(5, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.cincoQuartos)
        }
        else if (numerador == 6 && denominador == 4){
            pickerTimeSignature.selectRow(6, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.seisQuartos)
        }
        else if (numerador == 3 && denominador == 8){
            pickerTimeSignature.selectRow(7, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.tresOitavos)
        }
        else if (numerador == 6 && denominador == 8){
            pickerTimeSignature.selectRow(8, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.seisOitavos)
        }
        else if (numerador == 9 && denominador == 8){
            pickerTimeSignature.selectRow(9, inComponent: 0, animated: false)
        }
        else if (numerador == 12 && denominador == 8){
            pickerTimeSignature.selectRow(10, inComponent: 0, animated: false)
            delegate?.changeTimeSignature(TimeSelected.dozeOitavos)
        }
    }
    
    func setMetronome(metronome: Int) {
        lblMetronome.text = "\(metronome)"
        metronomeValue = metronome
        var unidade = 0
        unidade = metronome-Int(metronome/10)*10
        for i in 0..<metronomeContent.count {
            if(Int(metronomeContent[i])==unidade){
                self.pickerMetronome.selectRow(unidade, inComponent: 0, animated: false)
                break
            }
        }
        if let deleg = self.delegate {
            deleg.changeMetronome(metronomeValue)
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        if let deleg = delegate {
            deleg.changeTitle(textField.text!)
        }
    }
    
    private var lastValue: CGFloat = 30.0
    
    
    @objc func actionBtnClose(sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }, completion: { (finished: Bool) in
            self.isHidden = true
        })
    }
    
    func initializeImage(imageView: inout UIImageView!, image: UIImage, frame: CGRect) {
        if imageView == nil {
            let btn = UIImageView(frame: frame)
            btn.image = image
            addSubview(btn)
            
            imageView = btn
        } else {
            imageView.frame = frame
        }
    }
    
    func initializeSwitch(switchE: inout UISwitch!, isOn: Bool, frame: CGRect) {
        if switchE == nil {
            let swt = UISwitch(frame: frame)
            swt.isOn = isOn
            addSubview(swt)
            switchE = swt
        } else {
            switchE.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initializeButtonCancelDone(button: inout UIButton!, image: UIImage, txt : String, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            //  btn.setImage(image, for: .normal)
            btn.setBackgroundImage(image, for: .normal)
            btn.setTitle(txt, for: .normal)
            btn.titleLabel?.font = UIFont(name: "Lato-Bold", size: frame.height * 0.55)
            btn.titleLabel?.textColor = UIColor.white
            btn.titleLabel?.textAlignment = .center
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.center
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    
    func initializeLabelSmall(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.left) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Bold", size: frame.height * 0.5)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.center) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Bold", size: frame.height * 0.4)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeLabelBold(label: inout UILabel!, text: String, frame: CGRect, alignment: NSTextAlignment = NSTextAlignment.left) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = alignment
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.5)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    deinit {
        print("Entrou deinit PopUpPlay")
    }
    
}

extension PopUpCompose: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerMetronome{
            return metronomeContent.count
        }
        if pickerView == pickerTranspose {
            return transposeContent.count
        }
        if pickerView == pickerTimeSignature {
            return timeSignatureContent.count
        }
        if pickerView == pickerQuantization {
            return quantizationContent.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if pickerView == pickerMetronome || pickerView == pickerQuantization{
            return frame.height*0.06
        }
        if pickerView == pickerTimeSignature {
            return frame.height*0.06
        }
        return frame.height*0.1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        if(pickerView == pickerTranspose){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: hegtPickr, height: widPickr)
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: hegtPickr, height: widPickr)
            label.textAlignment = .center
            label.textColor = UIColor(red: 71/255, green: 74/255, blue: 81/255, alpha: 1.0)
            label.font = UIFont(name: "Lato-Regular", size: frame.width*0.08)
            label.text = transposeContent[row]
            view1.addSubview(label)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        if(pickerView == pickerTimeSignature){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: hegtPickrr, height: widPickrr)
            let imagem = UIImageView()
            imagem.frame = CGRect(x: 0, y: 0, width: hegtPickrr, height: widPickrr)
            imagem.contentMode = .scaleAspectFit
            view1.backgroundColor = UIColor.clear
            imagem.image = timeSignatureContent[row]
            view1.addSubview(imagem)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        if(pickerView == pickerMetronome){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: pickerMetronome.frame.height, height: pickerMetronome.frame.width)
            label.textAlignment = .center
            label.textColor = UIColor(red: 71/255, green: 74/255, blue: 81/255, alpha: 1.0)
            label.font = UIFont(name: "Lato-Regular", size: frame.width*0.08)
            label.text = metronomeContent[row]
            view1.addSubview(label)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        if(pickerView == pickerQuantization){
            let view1 = UIView()
            view1.frame = CGRect(x: 0, y: 0, width: hegtPickrr, height: widPickrr)
            let imagem = UIImageView()
            imagem.frame = CGRect(x: 0, y: 0, width: hegtPickrr, height: widPickrr)
            imagem.contentMode = .scaleAspectFit
            view1.backgroundColor = UIColor.clear
            imagem.image = quantizationContent[row]
            view1.addSubview(imagem)
            view1.transform = CGAffineTransform(rotationAngle: 90*(.pi/180))
            return view1
        }
        return view
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     
        if pickerView == pickerQuantization {
            switch row {
            case 0:
                delegate?.changeQuantization(4)
            case 1:
                delegate?.changeQuantization(8)
            case 2:
                delegate?.changeQuantization(16)
            default:
                delegate?.changeQuantization(4)
            }
        }
        if pickerView == pickerTranspose {
            switch row {
            case 0:
                delegate?.transpose(OptionKeySelected.C)
            case 1:
                delegate?.transpose(OptionKeySelected.Db)
            case 2:
                delegate?.transpose(OptionKeySelected.D)
            case 3:
                delegate?.transpose(OptionKeySelected.Eb)
            case 4:
                delegate?.transpose(OptionKeySelected.E)
            case 5:
                delegate?.transpose(OptionKeySelected.F)
            case 6:
                delegate?.transpose(OptionKeySelected.Fs)
            case 7:
                delegate?.transpose(OptionKeySelected.G)
            case 8:
                delegate?.transpose(OptionKeySelected.Ab)
            case 9:
                delegate?.transpose(OptionKeySelected.A)
            case 10:
                delegate?.transpose(OptionKeySelected.Bb)
            case 11:
                delegate?.transpose(OptionKeySelected.B)
            default:
                break
            }
        }
        if pickerView == pickerTimeSignature {
            switch row {
            case 0:
                delegate?.changeTimeSignature(TimeSelected.doisdois)
            case 1:
                delegate?.changeTimeSignature(TimeSelected.tresdois)
            case 2:
                delegate?.changeTimeSignature(TimeSelected.doisQuartos)
            case 3:
                delegate?.changeTimeSignature(TimeSelected.tresQuartos)
            case 4:
                delegate?.changeTimeSignature(TimeSelected.quatroQuartos)
            case 5:
                delegate?.changeTimeSignature(TimeSelected.cincoQuartos)
            case 6:
                delegate?.changeTimeSignature(TimeSelected.seisQuartos)
            case 7:
                delegate?.changeTimeSignature(TimeSelected.tresOitavos)
            case 8:
                delegate?.changeTimeSignature(TimeSelected.seisOitavos)
            case 9:
                delegate?.changeTimeSignature(TimeSelected.noveOitavos)
            case 10:
                delegate?.changeTimeSignature(TimeSelected.dozeOitavos)
            default:
                break
            }
        }
        if pickerView == pickerMetronome {
            lblMetronome.text = "\(Int(metronomeValue/10))\(metronomeContent[row])"
            metronomeValue = Int(lblMetronome.text!)
            setMetronome(metronome: metronomeValue)
        }
    }
}
