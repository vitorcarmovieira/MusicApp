//
//  MenuViewCompose.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 10/03/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit
@IBDesignable
class MenuViewCompose: UIView {
    
    ///Botão start do menu
    weak var btnSettings     : UIButton!
    ///Botão restart do menu
    weak var btnCursorBegin   : UIButton!
    ///Botão resume do menu
    weak var btnResume    : UIButton!
    ///Botão edit do menu
    weak var btnEdit      : UIButton!
    ///Botão save do menu
    weak var btnSave      : UIButton!

    weak var btnSend      : UIButton!
    
    weak var btnSendLater : UIButton!
    

    ///Botão exit do menu
    weak var btnExit      : UIButton!
    weak var btnRec        :UIButton!
    ///Label start do menu
    weak var lblSettings     : UILabel!
    ///Label restart do menu
    weak var lblCursorBegin   : UILabel!
    ///Label resume do menu
    weak var lblResume    : UILabel!
    ///Label edit do menu
    weak var lblEdit      : UILabel!
    weak var lblRec       : UILabel!
    
    ///Label save do menu
    weak var lblSave      : UILabel!
    weak var lblSendLater      : UILabel!
    ///Label exit do menu
    weak var lblExit      : UILabel!
     weak var lblSend      : UILabel!
    
    weak var stripeView : UIView!
    weak var stripeView2 : UIView!
    weak var stripeView3 : UIView!
    weak var stripeView4 : UIView!
    
    
    ///Estado atual do menu (popUp, save)
    var state     : StateMenu  = StateMenu.popUpOpened { didSet { setNeedsDisplay() } }
    ///Direção atual do menu (horizontal, vertical)
    var direction : DirectionMenu = DirectionMenu.horizontal { didSet { setNeedsDisplay() } }
    
    override func draw(_ rect: CGRect) {
        
        
        initStripeViewBigger(stripe: &stripeView2, frame: CGRect(x:0, y: frame.height*0.996, width: frame.width, height: frame.height*0.004))
        initStripeView(stripe: &stripeView, frame: CGRect(x: frame.width * 0.1, y: frame.height*0.1, width: frame.width*0.002, 
            height: frame.height*0.8))
        initStripeView(stripe: &stripeView3, frame: CGRect(x: frame.width * 0.39, y: frame.height*0.1, width: frame.width*0.002, height: frame.height*0.8))
        initStripeView(stripe: &stripeView4, frame: CGRect(x: frame.width * 0.906, y: frame.height*0.1, width: frame.width*0.002, height: frame.height*0.8))
        
        //Calculo do comprimento e largura do botão do menu
        let width  = rect.height * 0.6
        let heigth = rect.height * 0.6
        let endX   = rect.width
        
        //Calculo das bordas do botão
        let borderTop    = rect.height * 0.1
        let border       = rect.height * 0.1
        
        //Calculo do comprimento e largura dos labels do menu
        let widthLbl  = width
        let heightLbl = rect.height * 0.15
        
        //Calculo da borda de cima do label
        let borderTopLbl = borderTop + width
        
        //---------------------Posiciona botões----------------------
        initializeButton(button: &btnSettings, image: #imageLiteral(resourceName: "settings"), frame: CGRect(x: (0 * width) + border, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnRec, image: #imageLiteral(resourceName: "RecON"), frame: CGRect(x: frame.width*0.125, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnResume, image: #imageLiteral(resourceName: "start_play"), frame: CGRect(x: frame.width*0.21, y: borderTop, width: width, height: heigth))
        
         initializeButton(button: &btnCursorBegin, image: #imageLiteral(resourceName: "BackBegginer"), frame: CGRect(x: frame.width*0.29, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnEdit, image: #imageLiteral(resourceName: "btn_menu_edit"), frame: CGRect(x:frame.width*0.42, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnSave, image: #imageLiteral(resourceName: "btn_menu_save"), frame: CGRect(x: frame.width*0.825, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnSendLater, image: #imageLiteral(resourceName: "sendLater"), frame: CGRect(x:  frame.width*0.625, y: borderTop*1.7, width: width * 0.8, height: heigth * 0.8))
        
        initializeButton(button: &btnSend, image: #imageLiteral(resourceName: "btn_menu_send"), frame: CGRect(x:  frame.width*0.725, y: borderTop, width: width, height: heigth))
        
        initializeButton(button: &btnExit, image: #imageLiteral(resourceName: "exit"), frame: CGRect(x: endX - (1 * width) - border, y: borderTop, width: width, height: heigth))
        //-----------------------------------------------------------
        
        
        //--------------------Posiciona labels-----------------------
        initializeLabel(label: &lblSettings, text: "Settings", frame: CGRect(x: (0 * width) + border, y: borderTopLbl, width: widthLbl, height: heightLbl))
       
        initializeLabel(label: &lblRec, text: "Rec", frame: CGRect(x: frame.width*0.125, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblResume, text: "Play", frame: CGRect(x: frame.width*0.208, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblCursorBegin, text: "Restart", frame: CGRect(x: frame.width*0.289, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblEdit, text: "Edit", frame: CGRect(x: frame.width*0.4157, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblSave, text: "Save", frame: CGRect(x: frame.width*0.825, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        initializeLabel(label: &lblSendLater, text: "Send To Server", frame: CGRect(x:  frame.width*0.612, y: borderTopLbl, width: widthLbl*1.3, height: heightLbl))
        
         initializeLabel(label: &lblSend, text: "Send To Store", frame: CGRect(x:  frame.width*0.716, y: borderTopLbl, width: widthLbl*1.3, height: heightLbl))
        
        initializeLabel(label: &lblExit, text: "Home", frame: CGRect(x: endX - (1 * width) - border, y: borderTopLbl, width: widthLbl, height: heightLbl))
        
        //-----------------------------------------------------------
        
        enableButtonsByState()
    }
    
    func isPlayPerforme(){
        self.btnEdit.isHidden = true
        self.btnSave.isHidden = true
        self.lblEdit.isHidden = true
        self.lblSave.isHidden = true
    }
    /// Habilita os botões do menu de acordo com o estado do menu
    func enableButtonsByState() {
        switch state {
        case .popUpOpened:
            btnSettings.isEnabled   = true
            btnResume.isEnabled  = true
            btnEdit.isEnabled    = true
            btnSave.isEnabled    = true
            btnExit.isEnabled    = true
        case .save:
            btnSettings.isEnabled   = true
            btnResume.isEnabled  = true
            btnEdit.isEnabled    = false
            btnSave.isEnabled    = false
            btnExit.isEnabled    = true
        }
    }
    
    func initializeLabel(label: inout UILabel!, text: String, frame: CGRect) {
        if label == nil {
            let lbl = UILabel(frame: frame)
            lbl.text = text
            lbl.textColor = UIColor(red: 7/255, green: 82/255, blue: 158/255, alpha: 1.0)
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont(name: "Lato-Regular", size: frame.height * 0.8)
            addSubview(lbl)
            label = lbl
        } else {
            label.frame = frame
        }
    }
    
    func initializeButton(button: inout UIButton!, image: UIImage, frame: CGRect) {
        if button == nil {
            let btn = UIButton(frame: frame)
            btn.setImage(image, for: .normal)
            btn.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
            addSubview(btn)
            button = btn
        } else {
            button.frame = frame
        }
    }
    
    func initStripeView(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 176/255, green: 202/255, blue: 228/255, alpha: 1.0)//UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    func initStripeViewBigger(stripe: inout UIView!, frame: CGRect){
        if stripe == nil {
            let stp = UIView(frame: frame)
            stp.backgroundColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 1.0)
            self.addSubview(stp)
            stripe = stp
        }
    }
    
    deinit {
        btnSettings   = nil
        btnResume  = nil
        btnEdit    = nil
        btnSave    = nil
        btnExit    = nil
        lblResume  = nil
        lblEdit    = nil
        lblSave    = nil
        lblExit    = nil
    }
    
    func enableButtons() {
        btnSettings.isEnabled   = true
        btnResume.isEnabled  = true
        btnEdit.isEnabled    = true
        btnSave.isEnabled    = true
        btnCursorBegin.isEnabled = true
        btnRec.isEnabled = true
    }
    
    func disableButtons() {
        btnSettings.isEnabled   = false
        btnResume.isEnabled  = false
        btnEdit.isEnabled    = false
        btnSave.isEnabled    = false
        btnCursorBegin.isEnabled = false
        btnRec.isEnabled = false
    }
}




