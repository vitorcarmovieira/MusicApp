//
//  LoadingView.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 2/7/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class LoadingView: UIView {

//    var myActivityIndicator: UIActivityIndicatorView!
    
    var activityIndicator : CustomActivityIndicatorView!
    
    var imgDone: UIImageView!
    
    override func draw(_ rect: CGRect) {
        if activityIndicator == nil {
            let img = #imageLiteral(resourceName: "loading")
            let width  = img.size.width
            let height = img.size.height
            
            let x = (rect.midX/2.0) - (width / 4.0)
            let y = (rect.midY/2.0) - (height / 4.0)
            
            let act = CustomActivityIndicatorView(frame: CGRect(x: x, y: y, width: width, height: height), image: img)
            addSubview(act)
            
            activityIndicator = act
            
            activityIndicator.startAnimating()
        }
//        if (myActivityIndicator ==  nil) {
//            myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//            //myActivityIndicator.backgroundColor = UIColor.blue
//
//
//            myActivityIndicator.center = self.center
//            self.addSubview(myActivityIndicator)
//
//            self.myActivityIndicator.isHidden = false
//            // Start Activity Indicator
//            self.myActivityIndicator.startAnimating()
//
//        } else {
//            self.myActivityIndicator.isHidden = false
//            // Start Activity Indicator
//            self.myActivityIndicator.startAnimating()
//
//        }
    }

    func start() {
        if activityIndicator != nil {
            activityIndicator.startAnimating()
            if imgDone != nil {
                imgDone.isHidden = true
            }
        }
    }
    
    func done() {
        if activityIndicator != nil {
            activityIndicator.stopAnimating()
            if imgDone == nil {
                let img = #imageLiteral(resourceName: "loading_done")
                let width  = img.size.width
                let height = img.size.height
                
                let x = frame.midX - (width / 2.0)
                let y = frame.midY - (height / 2.0)
                
                let imgView = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
                imgView.image = img
                addSubview(imgView)
                
                imgDone = imgView
            } else {
                imgDone.isHidden = false
            }
        }
    }
}
