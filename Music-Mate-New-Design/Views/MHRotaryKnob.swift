//
//  MHRotaryKnob.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 10/27/17.
//  Copyright © 2017 LT Music Developer. All rights reserved.
//

import UIKit

class MHRotaryKnob: UIControl {
    var interactionStyle = MHRotaryKnobInteractionStyle(rawValue: 0)!
    var backgroundImage: UIImage?
    var foregroundImage: UIImage?
    private var currentKnobImage: UIImage?
    var knobImageCenter = CGPoint.zero
    var maximumValue: CGFloat = 0.0
    var minimumValue: CGFloat = 0.0
    var value: CGFloat = 0.0
    var defaultValue: CGFloat = 0.0
    var isResetsToDefault = false
    var isContinuous = false
    var scalingFactor: CGFloat = 0.0
    var maxAngle: CGFloat = 0.0
    var minRequiredDistanceFromKnobCenter: CGFloat = 0.0
    
    private var backgroundImageView: UIImageView?
    // shows the background image
    private var foregroundImageView: UIImageView?
    // shows the foreground image
    private var knobImageView: UIImageView?
    // shows the knob image
    private var knobImageNormal: UIImage?
    // knob image for normal state
    private var knobImageHighlighted: UIImage?
    // knob image for highlighted state
    private var knobImageDisabled: UIImage?
    // knob image for disabled state
    private var angle: CGFloat = 0.0
    // for tracking touches
    private var touchOrigin = CGPoint.zero
    // for horizontal/vertical tracking
    private var canReset = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        
    }
    
    func commonInit() {
        interactionStyle = .rotating
        minimumValue = 0.0
        maximumValue = 1.0
        defaultValue = 0.5
        value = defaultValue
        angle = 30.0
        isContinuous = true
        isResetsToDefault = true
        scalingFactor = 0.5
        maxAngle = 90.0
        minRequiredDistanceFromKnobCenter = 4.0
        knobImageView = UIImageView(frame: CGRect(x: bounds.width * 0.07, y: bounds.height * 0.09, width: bounds.width * 0.86, height: bounds.height * 0.83))
        addSubview(knobImageView!)
        valueDidChange(from: value, to: value, animated: true)
    }
    
    // MARK: - Data Model
    func clampAngle(_ angle: CGFloat) -> CGFloat {
        if angle < -maxAngle {
            return -maxAngle
        }
        else if angle > maxAngle {
            return maxAngle
        }
        else {
            return angle
        }
        
    }
    
    func angle(forValue value: CGFloat) -> CGFloat {
        return ((value - minimumValue) / (maximumValue - minimumValue) - 0.5) * (maxAngle * 2.0)
    }
    
    func value(forAngle angle: CGFloat) -> CGFloat {
        return (angle / (maxAngle * 2.0) + 0.5) * (maximumValue - minimumValue) + minimumValue
    }
    
    func angleBetweenCenterAndPoint(_ point: CGPoint) -> CGFloat {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        // Yes, the arguments to atan2() are in the wrong order. That's because
        // our coordinate system is turned upside down and rotated 90 degrees.
        let angle: CGFloat = atan2(point.x - center.x, center.y - point.y) * 100.0 / .pi
        return clampAngle(angle)
    }
    
    func squaredDistance(toCenter point: CGPoint) -> CGFloat {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let dx: CGFloat = point.x - center.x
        let dy: CGFloat = point.y - center.y
        return dx * dx + dy * dy
    }
    
    func shouldIgnoreTouch(at point: CGPoint) -> Bool {
        let minDistanceSquared: CGFloat = minRequiredDistanceFromKnobCenter * minRequiredDistanceFromKnobCenter
        return squaredDistance(toCenter: point) < minDistanceSquared
    }
    
    func value(forPosition point: CGPoint) -> CGFloat {
        var delta: CGFloat
        if interactionStyle == .sliderVertical {
            delta = touchOrigin.y - point.y
        }
        else {
            delta = point.x - touchOrigin.x
        }
        var newAngle: CGFloat = delta * scalingFactor + angle
        newAngle = clampAngle(newAngle)
        return value(forAngle: newAngle)
    }
    
    func setValue(_ newValue: CGFloat) {
        setValue(newValue, animated: false)
    }
    
    func setValue(_ newValue: CGFloat, animated: Bool) {
        let oldValue: CGFloat = newValue
        if newValue < minimumValue {
            value = minimumValue
        }
        else if newValue > maximumValue {
            value = maximumValue
        }
        else {
            value = newValue
        }
        
        valueDidChange(from: oldValue, to: value, animated: animated)
    }
    
    func setEnabled(_ isEnabled: Bool) {
        super.isEnabled = isEnabled
        if !self.isEnabled {
            showDisabledKnobImage()
        }
        else if self.isHighlighted {
            showHighlighedKnobImage()
        }
        else {
            showNormalKnobImage()
        }
        
    }
    
    // MARK: - Touch Handling
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let point: CGPoint = touch.location(in: self)
        if interactionStyle == .rotating {
            // If the touch is too close to the center, we can't calculate a decent
            // angle and the knob becomes too jumpy.
            if shouldIgnoreTouch(at: point) {
                return false
            }
            // Calculate starting angle between touch and center of control.
            angle = angleBetweenCenterAndPoint(point)
        }
        else {
            touchOrigin = point
            angle = angle(forValue: value)
        }
        self.isHighlighted = true
        showHighlighedKnobImage()
        canReset = false
        return true
    }
    
    func handle(_ touch: UITouch) -> Bool {
        if touch.tapCount > 1 && isResetsToDefault && canReset {
            setValue(defaultValue, animated: true)
            return false
        }
        let point: CGPoint = touch.location(in: self)
        if interactionStyle == .rotating {
            if shouldIgnoreTouch(at: point) {
                return false
            }
            // Calculate how much the angle has changed since the last event.
            let newAngle: CGFloat = angleBetweenCenterAndPoint(point)
            let delta: CGFloat = newAngle - angle
            angle = newAngle
            // We don't want the knob to jump from minimum to maximum or vice versa
            // so disallow huge changes.
            if fabs(delta) > 45.0 {
                return false
            }
            value += (maximumValue - minimumValue) * delta / (maxAngle * 5.0)
            // Note that the above is equivalent to:
            //self.value += [self valueForAngle:newAngle] - [self valueForAngle:angle];
        }
        else {
            value = value(forPosition: point)
        }
        return true
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        if handle(touch) && isContinuous {
            sendActions(for: .valueChanged)
        }
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        self.isHighlighted = false
        showNormalKnobImage()
        // You can only reset the knob's position if you immediately stop dragging
        // the knob after double-tapping it, i.e. when tracking ends.
        canReset = true
        _ = handle(touch!)
        sendActions(for: .valueChanged)
    }
    
    override func cancelTracking(with event: UIEvent?) {
        self.isHighlighted = false
        showNormalKnobImage()
    }
    
    // MARK: - Visuals
    func valueDidChange(from oldValue: CGFloat, to newValue: CGFloat, animated: Bool) {
        // (If you want to do custom drawing, then this is the place to do so.)
        let newAngle: CGFloat = angle(forValue: newValue)
        if animated {
            // We cannot simply use UIView's animations because they will take the
            // shortest path, but we always want to go the long way around. So we
            // set up a keyframe animation with three keyframes: the old angle, the
            // midpoint between the old and new angles, and the new angle.
            let oldAngle: CGFloat = angle(forValue: oldValue)
            let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
            animation.duration = 0.2
            animation.values = [(oldAngle * .pi / 180.0), ((newAngle + oldAngle) / 2.0 * .pi / 180.0), (newAngle * .pi / 180.0)]
            animation.keyTimes = [0.0, 0.5, 1.0]
            animation.timingFunctions = [CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn), CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)]
            knobImageView?.layer.add(animation, forKey: nil)
        }
        if knobImageView == nil {
            print("E nil")
        }
        knobImageView?.transform = CGAffineTransform(rotationAngle: newAngle * .pi / 180.0)
    }
    
    func setBackgroundImage(_ image: UIImage?) {
        if backgroundImageView == nil {
            backgroundImageView = UIImageView(frame: bounds)
            addSubview(backgroundImageView!)
            sendSubview(toBack: backgroundImageView!)
        }
        backgroundImageView?.image = image
    }

    
    func setForegroundImage(_ image: UIImage) {
        if foregroundImageView == nil {
            foregroundImageView = UIImageView(frame: bounds)
            addSubview(foregroundImageView!)
            bringSubview(toFront: foregroundImageView!)
        }
        foregroundImageView?.image = image
    }
    
    func setKnobImage(_ image: UIImage, for theState: UIControlState) {
        if theState == .normal {
            if image != knobImageNormal {
                knobImageNormal = image
                if state == .normal {
                    knobImageView?.image = image
//                    knobImageView?.backgroundColor = UIColor.blue
//                    knobImageView?.adjustsImageSizeForAccessibilityContentSizeCategory = true
                }
            }
        }
        if theState == .highlighted {
            if image != knobImageHighlighted {
                knobImageHighlighted = image
                if self.state == .highlighted {
                    knobImageView?.image = image
                }
            }
        }
        if theState == .disabled {
            if image != knobImageDisabled {
                knobImageDisabled = image
                if state == .disabled  {
                    knobImageView?.image = image
                }
            }
        }
    }
    
    func knobImage(for theState: UIControlState) -> UIImage {
        if theState == .normal {
            return knobImageNormal!
        }
        else if theState == .highlighted {
            return knobImageHighlighted!
        }
        else if theState == .disabled {
            return knobImageDisabled!
        }
        else {
            return UIImage()
        }
        
    }
    
    func setKnobImageCenter(_ theCenter: CGPoint) {
        knobImageView?.center = theCenter
    }

    func showNormalKnobImage() {
        knobImageView?.image = knobImageNormal
    }
    
    func showHighlighedKnobImage() {
        if knobImageHighlighted != nil {
            knobImageView?.image = knobImageHighlighted
        }
        else {
            knobImageView?.image = knobImageNormal
        }
    }
    
    func showDisabledKnobImage() {
        if knobImageDisabled != nil {
            knobImageView?.image = knobImageDisabled
        }
        else {
            knobImageView?.image = knobImageNormal
        }
    }

}

enum MHRotaryKnobInteractionStyle : Int {
    case rotating
    case sliderHorizontal
    // left -, right +
    case sliderVertical
}
