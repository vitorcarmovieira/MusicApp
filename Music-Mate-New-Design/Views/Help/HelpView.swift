//
//  HelpView.swift
//  Music-Mate-New-Design
//
//  Created by Nicolas Fernandes de Lima on 13/04/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class HelpView: UIView {
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var ctrlPage: UIPageControl!
    
    @IBOutlet weak var helpCollectionView: UICollectionView!
    
    
    @IBAction func bntActionBack(_ sender: Any) {
        
        self.nivel = 1
        collectionContentDescriptions.removeAll()
        collectionContentImages.removeAll()
        collectionContentTitles.removeAll()
        
        collectionContentDescriptions = DescriptionLevel1
        collectionContentImages = images
        collectionContentTitles = titles
        helpCollectionView.reloadData()
        self.btnBack.isHidden = true
        
    }
    //images
    var images              = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "mycom_off"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "ComposeV"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "midi"), #imageLiteral(resourceName: "text")]
    var imagesCompose       = [#imageLiteral(resourceName: "settings"),#imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "RecON"), #imageLiteral(resourceName: "Group 2"), #imageLiteral(resourceName: "edit_partiture"), #imageLiteral(resourceName: "general"), #imageLiteral(resourceName: "firstnote")]
    var imagesFooter        = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    var imagesProfile       = [#imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "tags")]
    var imagesPlayPerform   = [#imageLiteral(resourceName: "settings"),#imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "switch"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "text"), #imageLiteral(resourceName: "default"), #imageLiteral(resourceName: "Group 2"), #imageLiteral(resourceName: "PerformOFF"), #imageLiteral(resourceName: "compare"), #imageLiteral(resourceName: "statistic"), #imageLiteral(resourceName: "1_4"), #imageLiteral(resourceName: "text")]
    
    //titles
    var titles               = ["My Music", "My Compositions", "Store", "Profile", "Into Field", "Compose Button", "Instruments", "MIDI connected and disconnected", "Help"]
    var composeTitles        = ["Settings", "Sound Metronome", "Minimaize Cursor", "Sound Right/Left Hand", "Transpose", "Metronome", "Key Signature", "Rec", "Play and Begin", "Edit", "General", "First Bar"]
    var footerTitles         = ["My Music", "Compose", "Store", "Profile", "Into Field"]
    var profileTitles        = ["Profile", "Instrument", "Interest"]
    var playPerformTitles    = ["Settings", "Sound Metronome", "Minimaize Cursor", "Sound Right/Left Hand", "Transpose", "Metronome", "Default Button", "Play and Begin", "Perform", "Compare", "Statistic", "Quantization", "Pause"]
    
    
    //drescriptions
    var DescriptionLevel1       = ["Here you can save all your scores, your downloaded scores and arrageaments.", "This takes you to the compose environment where you can create your own scores.", "This takes you to the store, where you can download any music you like.", "This is your personal profile.", "Long press the score to see more info and delete the score.", "Press this to create a new coposition.", "Press this to change your active instrument (remember: changing your instrument will change accordingly in your library).", "It will appears as ON when you are using digital instrument and OFF if you aren't.", "You can review the tips to help you using our application."]
    var descriptionsCompose     = ["Here you can change fundamental settings of your score like metronome and cursor movement", "It will enable and disable the metronome", "It minimize cursor to a little triangle", "It will be anable the right or the left hand for you", "Here you can change the key of the score.", "Here you can select your metronome speed.", "Inform the tone that the sheet music was written.", "You can start recording your performing clicking rec button.", "Press Play to start playing the song and Begin to restart it. ", "Edit the score manually (only possible after recording).", "Here you can change things like: First Bar, Key Signature, Time Signature and Metronome. ", "To define an upbeat, touch on the first not of the first bar. The notes before this note will be the upbeat. Remember that MusicApp always uses an upbeat, even an empty, to do the count-in."]
    var descriptionsFooter      = ["asd", "dsa", "asd", "dsa", "asd"]
    var descriptionsProfile     = ["Here you can change your personal info, like your keyward and other things.", "Here you can change the instrument you are using at the moment.", "Here you can add your music interest. Can be more than one."]
    var descriptionsPlayPerform = ["Here you can change fundamental settings of your score like metronome and cursor movement", "It will enable and disable the metronome", "It minimize cursor to a little triangle", "It will be anable the right or the left hand for you", "Here you can change the key of the score.", "Here you can select your metronome speed.", "It will restore all the settings to its original state. ", "Press Play to start playing the song and Begin to restart it.", "You can perform the song and after we will give you a feedback.", "You can perform the song and after we will give you a feedback.", "Here you can change the key of the score.","Here you can change the key of the score.", "You can tap in the screen to pause  your performing. "]
    
    //level 2 descriptions
    let descriptionRepetitionCell1  = ["asd", "dsa", "asd", "dsa", "asd"]
    let descriptionSegnoCell1       = ["asd", "dsa", "asd", "dsa", "asd"]
    let descriptionCanalyzeCell1    = ["asd", "dsa", "asd", "dsa", "asd"]
    let descriptionNotesCell1       = ["asd", "dsa", "asd", "dsa", "asd"]
    
    //level 2 images
    
    let imagesRepetitionCell1   = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    let imagesSegnoCell1        = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    let imagesCanalyzeCell1     = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    let imagesNotesCell1        = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]

    
    //collection vars
    var collectionContentDescriptions   = ["asd", "dsa", "asd", "dsa", "asd"]
    var collectionContentImages         = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    var collectionContentTitles         = [" ", " ", " ", " ", " "]
    let descriptionCell1                = ["asd", "dsa", "asd", "dsa", "asd"]
    let imagesCell1                     = [#imageLiteral(resourceName: "mymusic"), #imageLiteral(resourceName: "composition"), #imageLiteral(resourceName: "storeHelp"), #imageLiteral(resourceName: "prof_off"), #imageLiteral(resourceName: "text")]
    
    var flowLayout: UICollectionViewFlowLayout!
    var aux = false
    var nivel = 1
    var qualTela = ""

    
    
    override func draw(_ rect: CGRect) {
        
        
        self.helpCollectionView.bounces                     = true
        self.helpCollectionView.alwaysBounceHorizontal      = true
        self.helpCollectionView.alwaysBounceVertical        = false
        self.helpCollectionView.backgroundColor             = UIColor.clear
        
        flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: helpCollectionView.frame.width, height: helpCollectionView.frame.height * 0.19)
        helpCollectionView.collectionViewLayout     = flowLayout
        helpCollectionView.isPagingEnabled          = true
        flowLayout.scrollDirection                  = .horizontal
        
        self.helpCollectionView.delegate        = self
        self.helpCollectionView.dataSource      = self
        self.helpCollectionView.isPagingEnabled = true
        helpCollectionView.register(UINib(nibName: "HelpCell", bundle: nil), forCellWithReuseIdentifier: "ReusableCell")
//        ctrlPage.numberOfPages                  = helpCollectionView.numberOfSections
//        ctrlPage.currentPage                    = 0
        
    }
    
    func helpHome(){
        
        self.nivel = 1
        self.qualTela = "home"
        collectionContentDescriptions.removeAll()
        collectionContentImages.removeAll()
        collectionContentTitles.removeAll()
        collectionContentDescriptions           = DescriptionLevel1
        collectionContentImages                 = images
        collectionContentTitles                 = titles
        helpCollectionView.reloadData()
        self.btnBack.isHidden                   = true
        self.ctrlPage.numberOfPages             = 2
//        ctrlPage.currentPage = 0
        
    }
    
    func helpCompose(){
        
        self.nivel = 1
        self.qualTela = "compose"
        collectionContentImages.removeAll()
        collectionContentDescriptions.removeAll()
        collectionContentTitles.removeAll()
        collectionContentDescriptions           = descriptionsCompose
        collectionContentImages                 = imagesCompose
        collectionContentTitles                 = composeTitles
        helpCollectionView.reloadData()
        self.btnBack.isHidden                   = true
        self.ctrlPage.numberOfPages             = 3
        
    }
    
//    func helpEdit(){
//
//        self.nivel = 1
//        self.qualTela = "edit"
//        collectionContentImages.removeAll()
//        collectionContentDescriptions.removeAll()
//        collectionContentTitles.removeAll()
//        collectionContentDescriptions           = descriptionsFooter
//        collectionContentImages                 = imagesFooter
//        collectionContentTitles                 = footerTitles
//        helpCollectionView.reloadData()
//        self.btnBack.isHidden                   = true
//
//    }
    
    func helpProfile(){
        
        self.nivel = 1
        self.qualTela = "profile"
        collectionContentImages.removeAll()
        collectionContentDescriptions.removeAll()
        collectionContentTitles.removeAll()
        collectionContentDescriptions           = descriptionsProfile
        collectionContentImages                 = imagesProfile
        collectionContentTitles                 = profileTitles
        helpCollectionView.reloadData()
        self.btnBack.isHidden                   = true
        self.ctrlPage.numberOfPages             = 1
        
    }

    func helpPlayPerform(){
        
        self.nivel = 1
        self.qualTela = "playPerform"
        collectionContentImages.removeAll()
        collectionContentDescriptions.removeAll()
        collectionContentTitles.removeAll()
        collectionContentDescriptions           = descriptionsPlayPerform
        collectionContentImages                 = imagesPlayPerform
        collectionContentTitles                 = playPerformTitles
        helpCollectionView.reloadData()
        self.btnBack.isHidden                   = true
        self.ctrlPage.numberOfPages             = 3
        
    }
    
    @IBAction func actionBtnClose(_ sender: Any) {
        
        self.isHidden = true
    }
    
    
}

extension HelpView: UICollectionViewDelegate, UICollectionViewDataSource {

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if (qualTela == "edit"){
            
            if(nivel == 1){
            
                if (indexPath.row == 0){
                    collectionContentDescriptions.removeAll()
                    collectionContentImages.removeAll()
                    collectionContentTitles.removeAll()
                    helpCollectionView.reloadData()
                    collectionContentDescriptions       = descriptionCanalyzeCell1
                    collectionContentImages             = imagesCanalyzeCell1
                    self.btnBack.isHidden               = false
                    self.nivel                          = 2
                    
                     }
                
                if (indexPath.row == 1){
                    collectionContentDescriptions.removeAll()
                    collectionContentImages.removeAll()
                    collectionContentTitles.removeAll()
                    collectionContentDescriptions       = descriptionRepetitionCell1
                    collectionContentImages             = imagesRepetitionCell1
                    helpCollectionView.reloadData()
                    self.btnBack.isHidden               = false
                    nivel                               = 2
                }
                
                if (indexPath.row == 2){
                    collectionContentDescriptions.removeAll()
                    collectionContentImages.removeAll()
                    collectionContentTitles.removeAll()
                    collectionContentDescriptions       = descriptionSegnoCell1
                    collectionContentImages             = imagesSegnoCell1
                    helpCollectionView.reloadData()
                    self.btnBack.isHidden               = false
                    nivel                               = 2
                }
                
                if (indexPath.row == 3){
                    collectionContentDescriptions.removeAll()
                    collectionContentImages.removeAll()
                    collectionContentTitles.removeAll()
                    collectionContentDescriptions       = descriptionNotesCell1
                    collectionContentImages             = imagesNotesCell1
                    helpCollectionView.reloadData()
                    self.btnBack.isHidden               = false
                    nivel                               = 2
                }
//            switch (qualTela){
//
//            case "home":
//                if (indexPath.row == 0){
//                    collectionContentDescriptions.removeAll()
//                    collectionContentImages.removeAll()
//                    collectionContentTitles.removeAll()
//                    collectionContentDescriptions       = descriptionCell1
//                    collectionContentImages             = imagesCell1
//                    helpCollectionView.reloadData()
//                    self.btnBack.isHidden               = false
//                    nivel                               = 2
//                }
//
//            case "compose":
//                if (indexPath.row == 0){
//                    collectionContentDescriptions.removeAll()
//                    collectionContentImages.removeAll()
//                    collectionContentTitles.removeAll()
//                    collectionContentDescriptions       = descriptionsCompose
//                    collectionContentImages             = imagesCompose
//                    helpCollectionView.reloadData()
//                    self.btnBack.isHidden               = false
//                    nivel                               = 2
//                }
//
//            default: "home"
//            }
               
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.collectionContentDescriptions.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
//        switch qualTela {
//        case "home":
//            return 1
//
//        case "profile":
//            return 1
//
//        case "compose":
//            return 1
//
//        case "playPerform":
//            return 1
//
//        default:
//            return 3
//        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReusableCell", for: indexPath) as!helpCollectionViewCell
        let myColor : UIColor = UIColor( red: 0.86, green: 0.86, blue:0.86, alpha: 1.0 )
        cell.layer.borderColor = myColor.cgColor
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.backgroundColor = UIColor.white

        
        
        if(nivel == 1){
            cell.lblTitle.text              = self.collectionContentTitles[indexPath.item]
//            cell.lblTitle.isHidden          = false
            cell.imgHelp.image              = self.collectionContentImages[indexPath.item]
            cell.lblDescription.text        = self.collectionContentDescriptions[indexPath.item]
            
            return cell
        }
        else{
            cell.isLevelTwo()
            cell.lblTitle.isHidden           = true
            cell.imgHelp.image               = self.collectionContentImages[indexPath.item]
            cell.lblDescription.text         = self.collectionContentDescriptions[indexPath.item]
            
            return cell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.ctrlPage.currentPage           = indexPath.section
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        let pageNumber                      = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        ctrlPage.currentPage                = Int(pageNumber)
//        ctrlPage.numberOfPages              = self.helpCollectionView.numberOfSections
    }
}


