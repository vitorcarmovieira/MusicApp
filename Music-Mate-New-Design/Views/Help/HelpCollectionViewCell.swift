//
//  HelpCollectionViewCell.swift
//  Music-Mate-New-Design
//
//  Created by Nicolas Fernandes de Lima on 05/04/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import UIKit

class helpCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHelp  : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    var initialFrame : CGRect!
    
    
    override func draw(_ rect: CGRect) {
        self.initialFrame = lblDescription.frame
        layer.cornerRadius = 10
    }
    
//    func hidelblTitle () {
//
//        lblTitle.isHidden = true
//
//    }
//
    func isLevelOne(){
        layer.cornerRadius = 10
//        lblTitle.isHidden = false
        if (self.initialFrame == nil){
            self.initialFrame = lblDescription.frame
        }
        self.lblDescription.frame = CGRect(x: self.lblDescription.frame.minX, y: self.initialFrame.minY, width: self.lblDescription.frame.width, height: self.initialFrame.height)
        
    }
    
    func isLevelTwo(){
        layer.cornerRadius = 10
//        lblTitle.isHidden = true
        self.lblDescription.frame = CGRect(x: self.lblDescription.frame.minX, y: 0, width: self.lblDescription.frame.width, height: self.frame.height*0.9)
        
    }
    
}
