//
//  Vector.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit

class Vector {
    
    func divideVectorIntoPieces(_ source: [Float], pieceLenght: Int, stepLenght: Int) -> [[Float]] {
        var numberOfSteps = Int(ceil(Double(source.count / stepLenght)))
        //
        while (numberOfSteps - 1) * stepLenght + pieceLenght >= source.count {
            numberOfSteps -= 1
        }
        numberOfSteps += 1
        
        let numberOfPieces = numberOfSteps
        
        var pieces = [[Float]](repeating: [Float](repeating: 0.0, count: pieceLenght), count: numberOfPieces)
        
        for pi in 0..<numberOfPieces {
            var pj = 0
            var si = pi * stepLenght
            
            while pj < pieceLenght && si < source.count {
                pieces[pi][pj] = source[si]
                
                pj += 1
                si += 1
            }
        }
        
        return pieces
    }
    
    func divideVector(vector : [Float], size : Int) -> [[Float]] {
        let q = Int(ceil(Float(vector.count) / Float(size)))
        var resp = [[Float]](repeating: [Float](repeating: 0, count: size), count: q)
        
        for i in 0 ..< q {
            for j in 0 ..< size {
                if (size * i + j < vector.count) {
                    resp[i][j] = vector[size * i + j]
                } else {
                    break
                }
            }
        }
        
        return resp
    }
    
    func multiplyRealVectors(vectorA : [Float], vectorB : [Float], vectorC : inout [Float]) {
        let final = min(vectorA.count, min(vectorB.count, vectorC.count))
        for i in 0 ..< final {
            vectorC[i] = vectorA[i] * vectorB[i]
        }
    }
    
    func normalize(source: [Float], output: inout [Float]) {
        var range: [Int] = []
        
        let highestValue = highestAbsValue(vector: source, range: [])
        
        scale(source: source, scale: 1/highestValue, output: &output, range: &range)
    }
    
    func scale(source: [Float], scale: Float, output: inout [Float], range: inout [Int]) {
        let smallestLength = min(source.count, output.count)
        
        if range.count == 0 {
            range = [0, smallestLength - 1]
        }
        
        if range[0] > range[1] || range[1] >= smallestLength {
            return
        }
        
        for i in range[0]...range[1] {
            output[i] = source[i] * scale
        }
    }
    
    func sum(vector: [Float], range: [Int]) -> Float {
        let validRange = valideRange(vector: vector, range: range)
        
        var sum: Float = 0.0
        
        for i in validRange[0]...validRange[1] {
            var vlAbs = vector[i]
            
            if vlAbs < 0 {
                vlAbs *= -1
            }
            
            sum += vlAbs
        }
        
        return sum
    }
    
    func sumVectors(srcA: [Float], srcB: [Float], output: inout [Float], range: inout [Int]) {
        let smallestLength = min(min(srcA.count, srcB.count), output.count)
        
        if range.count == 0 {
            range = [0, smallestLength - 1]
        }
        
        if range[0] > range[1] || range[1] >= smallestLength {
            return
        }
        
        for i in range[0]...range[1] {
            output[i] = srcA[i] + srcB[i]
        }
    }
    
    func multiplyComplexVectors(srcA: [Float], srcB: [Float], output: inout [Float], range: inout [Int]) {
        let smallestLength = min(min(srcA.count, srcB.count), output.count) / 2
        
        if range.count == 0 {
            range = [0, smallestLength - 1]
        }
        
        if range[0] > range[1] || range[1] >= smallestLength {
            return
        }
        
        var real      = range[0] * 2
        var imaginary = range[0] * 2 + 1
        
        var valor: [Float] = []
        
        while real <= range[1] * 2 {
            
            valor = Complex().multiplicar(realNum1: srcA[real], imagNum1: srcA[imaginary], realNum2: srcB[real], imagNum2: srcB[imaginary])
            
            output[real]      = valor[0]
            output[imaginary] = valor[1]
            
            real      += 2
            imaginary += 2
        }
    }
    
    func complex(source: [Float], output: inout [Float]) {
        var si = 0
        var oi = 0
        
        while si < source.count && oi < output.count {
            output[oi] = source[si]
            
            si += 1
            oi += 2
        }
    }
    
    func magnitude(source: [Float], output: inout [Float]) {
        var si = 0
        var oi = 0
        
        while si < source.count && oi < output.count {
            
            output[oi] = Complex().magnitude(real: source[si], imag: source[si + 1])
            
            si += 2
            oi += 1
        }
    }
    
    func abs(source: [Float], output: inout [Float]) {
        var si = 0
        var oi = 0
        
        while si < source.count && oi < output.count {
            
            if source[si] < 0 {
                output[oi] = -source[si]
            } else {
                output[oi] = source[si]
            }

            si += 1
            oi += 1
        }
    }

    func energy(vector: [Float], range: [Int]) -> Float {
        let validRange = valideRange(vector: vector, range: range)
        
        var energy: Float = 0.0
        
        for i in validRange[0]...validRange[1] {
            energy += powf(vector[i], 2.0)
        }
        
        return energy / Float(range[1] - range[0])
    }
    
    func highestAbsValue(vector: [Float], range: [Int]) -> Float {
        let validRange = valideRange(vector: vector, range: range)
        
        var max: Float = 0.0
        
        for i in validRange[0]...validRange[1] {
            var vlAbs = vector[i]
            
            if vlAbs < 0 {
                vlAbs *= -1
            }
            
            if vlAbs > max {
                max = vlAbs
            }
        }
        
        return max
    }
    
    func valideRange(vector: [Float], range: [Int]) -> [Int] {
        if range.count == 0 {
            return [0, vector.count - 1]
        } else if range[0] > range[1] {
            return [range[1], range[0]]
        } else if range[0] < 0 || range[0] >= vector.count {
            return [0, 0]
        } else if range[1] >= vector.count {
            return [range[0], vector.count - 1]
        }
        
        return range
    }
    
    func shift(source: [Float], amount: Int) -> [Float] {
        var output = [Float](repeating: 0.0, count: source.count)
        
        var si = amount
        var io = 0
        
        while si < source.count && io < output.count {
            output[io] = source[si]
            
            si += 1
            io += 1
        }
        
        return output
    }
}
