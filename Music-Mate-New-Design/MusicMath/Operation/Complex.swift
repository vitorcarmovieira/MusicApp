//
//  Complex.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit

class Complex {
    func multiplicar(realNum1: Float, imagNum1: Float, realNum2: Float, imagNum2: Float) -> [Float] {
        let real = realNum1 * realNum2 - imagNum1 * imagNum2
        let imag = realNum1 * imagNum2 + imagNum1 * realNum2
        
        return [real, imag]
    }
    //
    func magnitude(real: Float, imag: Float) -> Float {
        return sqrtf(powf(real, 2.0) + powf(imag, 2.0))
    }
}
