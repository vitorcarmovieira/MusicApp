//
//  NoteAnalyser1.swift
//  Music-Mate-New-Design
//
//  Created by Diego Lopes on 5/28/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation
import Accelerate

class NoteAnalyser1 {
    private var fRange     : FRange
    private var sampleRate : Int
    private var fourierLenght   : Int
    private var intWindowLenght : Int
    
    init(_ sampleRate: Int, fourierLenght: Int, intWindowLenght: Int) {
        self.sampleRate      = sampleRate
        self.intWindowLenght = intWindowLenght
        self.fourierLenght   = 1
        self.fRange          = FRange(N: self.fourierLenght, Fs: sampleRate)
        
        self.setFourierLenght(fourierLenght)
        self.fRange          = FRange(N: self.fourierLenght, Fs: sampleRate)

    }

    func setFourierLenght(_ fourierLenght: Int) {
        let power = round(log2(Double(fourierLenght)))
        self.fourierLenght = Int(pow(2, power))
    }
    
    func frequencies(in inputArray: [Float], extWindowLenght: Int) -> [Int] {
        
        if extWindowLenght < self.intWindowLenght {
            return []
        }
        
        let extWindows = Vector().divideVectorIntoPieces(inputArray, pieceLenght: extWindowLenght, stepLenght: extWindowLenght)
        
        var mPeaks: [[Peak]] = []
        
        //Divide em janelas do tamanho da quantização
        for window in extWindows {
            var peaks: [Peak] = []
            
            let intWindows = Vector().divideVectorIntoPieces(window, pieceLenght: intWindowLenght, stepLenght: intWindowLenght)
            
            //Divide em janelas de 30 ms (256 amostras)
            for intWindow in intWindows {
                //Encontra os picos de frequencia em cada janela interna
                peaks.append(contentsOf: getFrequencyPeaks(intWindow))

            }
            
            //Soma picos por frequencia por quantização
            sumAmplitude(peaks: &peaks)
            
//            print("-------Picos somados em todas as janelas da quantização-------")
//            for peak in peaks {
//                print(peak.sample, peak.amplitude, peak.qtdeJanelas)
//            }
//            print("--------------------------------------------------------------")
//
            
            mPeaks.append(peaks)
        }

        removeLowNotes(mPeaks: &mPeaks)

        let filters = getFilters(mPeaks: &mPeaks)


        mPeaks.removeAll()
        
        return filters
    }
    
    private func getFilters(mPeaks: inout [[Peak]]) -> [Int] {
        var peaks: [Int] = []
        
        for peak in 0..<mPeaks.count {
            mPeaks[peak].sort(by: { $0.sample < $1.sample })
            
            if mPeaks[peak].count > 0 {
                for i in 0..<mPeaks[peak].count {
                    if !peaks.contains(mPeaks[peak][i].sample) {
                        peaks.append(mPeaks[peak][i].sample)
                    }
                }
            }
        }
        
        peaks.sort(by: { $0 < $1 })
        
        return peaks
    }
    
    private func removeLowNotes(mPeaks: inout [[Peak]]) {

        let threshold: [Float] = [ 15.0, 15.0, 15.0, 3.0, 3.0, 1.0, 1.0 ]
        
        for peak in 0..<mPeaks.count {
            var i = 0
            
            while i < mPeaks[peak].count {
                if mPeaks[peak][i].amplitude < threshold[mPeaks[peak][i].sample/12] {
                    mPeaks[peak].remove(at: i)
                } else {
                    i += 1
                }
            }
        }

        var sum  : [Int: Float] = [:]
        var cont : [Int: Int]   = [:]
        
//        print("----Lista por quantização de picos após limiar----")
        for i in 0..<mPeaks.count {
            mPeaks[i].sort(by: { $0.amplitude > $1.amplitude })
            
//            print("----Quantização \(i + 1)----")
            for j in 0..<mPeaks[i].count {
                let db = 10 * log(mPeaks[i][j].amplitude / mPeaks[i][0].amplitude)
                if let _ = sum[mPeaks[i][j].sample] {
                    sum[mPeaks[i][j].sample]!  = sum[mPeaks[i][j].sample]! + mPeaks[i][j].amplitude
                    cont[mPeaks[i][j].sample]! = cont[mPeaks[i][j].sample]! + 1
                } else {
                    sum.updateValue(mPeaks[i][j].amplitude, forKey: mPeaks[i][j].sample)
                    cont.updateValue(1, forKey: mPeaks[i][j].sample)
                }
//                print(mPeaks[i][j].sample, mPeaks[i][j].amplitude, mPeaks[i][j].qtdeJanelas, db)
            }
//            print("____________________________")
        }
//        print("--------------------------------------------------")
        
//        print("----------------------Media-----------------------")
//        for (key, value) in sum {
//            let media = value / Float(cont[key]!)
//            print(key, media)
//        }
//        print("--------------------------------------------------")
        
        let dbPorOitava: [Float] = [ -5.0, -5.0, -5.0, -5.0, -5.0, -5.0, -5.0 ]
        
        for peak in 0..<mPeaks.count {
            mPeaks[peak].sort(by: { $0.amplitude > $1.amplitude })
            
            var i = 1
            
            while i < mPeaks[peak].count {
                let cond = (10 * log(mPeaks[peak][i].amplitude / mPeaks[peak][0].amplitude)) < dbPorOitava[Int(mPeaks[peak][i].sample/12)]
                
                if cond {
                    mPeaks[peak].remove(at: i)
                } else {
                    i += 1
                }
            }
        }
        
//        print("-------Lista por quantização de picos após db-----")
        for i in 0..<mPeaks.count {
            mPeaks[i].sort(by: { $0.amplitude > $1.amplitude })
            
//            print("----Quantização \(i + 1)----")
            for j in 0..<mPeaks[i].count {
//                let db = 10 * log(mPeaks[i][j].amplitude / mPeaks[i][0].amplitude)
//                print(mPeaks[i][j].sample, mPeaks[i][j].amplitude, mPeaks[i][j].qtdeJanelas, db)
            }
//            print("____________________________")
        }
//        print("--------------------------------------------------")
    }
    
    func getFrequencyPeaks(_ inputArray: [Float]) -> [Peak] {
        let aux = self.transform(signal: inputArray)

//        for i in 0..<aux.count {
//            aux[i] *= aux[i]
//        }
        
        return locatePeaks(aux)
    }
    
    func locatePeaks(_ inputArray: [Float]) -> [Peak] {
        let minimumPeak: [Float] = [ 5.0, 1.0, 1.0, 0.7, 1.0, 0.5, 0.5 ]
        
        
        var peaks: [Peak] = []
        
        if (inputArray.count/2) > 1 {
            for i in 1..<inputArray.count/2 {
                if inputArray[i-1] < inputArray[i] && inputArray[i+1] < inputArray[i] {
                    let filter = fRange.filterForSample(sample: i)
                    let pos = filter/12

                    if inputArray[i] > minimumPeak[pos] {
                        peaks.append(Peak(sample: filter, amplitude: inputArray[i], qtdeJanelas: 0))
                    }
                }
            }
        }
        
        maximarAmplitude(peaks: &peaks)
        
//        print("-----Pico por sample em cada janela de 30 ms-----")
//        for peak in peaks {
//            print(peak.sample, peak.amplitude)
//        }
//        print("-------------------------------------------------")
        
        return peaks
    }
    
    private func maximarAmplitude(peaks: inout [Peak]) {
        peaks.sort(by: { $0.sample < $1.sample })
        
        var newList: [Peak] = []
        
        var peakAux = Peak(sample: 0, amplitude: 0, qtdeJanelas: 0)
        
        for peak in peaks {
            if peakAux.sample != peak.sample {
                if peakAux.sample != 0 || peakAux.amplitude != 0 {
                    newList.append(Peak(sample: peakAux.sample, amplitude: peakAux.amplitude, qtdeJanelas: 0))
                }
                peakAux = peak
            } else {
                peakAux.amplitude = max(peak.amplitude, peakAux.amplitude)
            }
        }
        
        if peakAux.sample != 0 || peakAux.amplitude != 0 {
            newList.append(peakAux)
        }
        
        peaks.removeAll()
        peaks.append(contentsOf: newList)
    }
    
    private func sumAmplitude(peaks: inout [Peak]) {
        peaks.sort(by: { $0.sample < $1.sample })
        
        var newList: [Peak] = []
        
        var peakAux = Peak(sample: 0, amplitude: 0, qtdeJanelas: 0)
        
        for peak in peaks {
            if peakAux.sample != peak.sample {
                if peakAux.sample != 0 || peakAux.amplitude != 0 {
                    newList.append(Peak(sample: peakAux.sample, amplitude: peakAux.amplitude, qtdeJanelas: peakAux.qtdeJanelas))
                }
                peakAux = peak
                
                peakAux.qtdeJanelas = 1
            } else {
                peakAux.qtdeJanelas += 1
                peakAux.amplitude += peak.amplitude
            }
        }
        
        if peakAux.sample != 0 || peakAux.amplitude != 0 {
            newList.append(peakAux)
        }
        newList.sort(by: { $0.amplitude > $1.amplitude })
        
        var i = 0
        
        while i < newList.count {
            if newList[i].qtdeJanelas <= 3 {
                newList.remove(at: i)
            } else {
                i += 1
            }
        }
        
        peaks.removeAll()
        peaks.append(contentsOf: newList)
    }
    
    private func transform(signal : [Float]) -> [Float] {
        //Real and imaginary window vector instance
        var signalReal = signal

        signalReal.append(contentsOf: [Float](repeating: 0, count: self.fourierLenght - signal.count))

        var signalImaginary = [Float](repeating: 0, count: self.fourierLenght)
        
        //Real and imaginary window representative
        var splitComplexSignal = DSPSplitComplex(realp: &signalReal, imagp: &signalImaginary)
        
        //Settings for FFT
        let length = vDSP_Length(floor(log2(Float(self.fourierLenght))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetup(length, radix)
        
        //Signal from time domain to frequency domain
        vDSP_fft_zip(weights!, &splitComplexSignal, 1, length, FFTDirection(FFT_FORWARD))
        
        var magnitude = [Float](repeating: 0, count: self.fourierLenght)
        vDSP_zvmags(&splitComplexSignal, 1, &magnitude, 1, vDSP_Length(self.fourierLenght))
        magnitude = magnitude.map(){sqrt($0)}
        
//        for mag in magnitude {
//            print(mag)
//        }

        vDSP_destroy_fftsetup(weights)
//        free(&signalReal);
//        free(&signalImaginary);
        //        free(hammingWindow);
        
        return magnitude
    }
    
    private func sine(amplitude: Float, frequency: Float, fi: Float, size: Int) -> [Float]{
        var sign = [Float](repeating: 0, count: size)
        var t: Float = 0
        
        for i in 0..<size {
            t = Float(i) / Float(sampleRate)
            sign[i] = sin(2 * Float.pi * frequency * t + fi)
        }
        
        return sign
    }
}
