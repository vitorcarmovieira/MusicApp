//
//  NoteAnalyzer.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 18/05/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation
import Accelerate

private let FOURIER_LENGHT = 8192
private var DEFAULT_SAMPLE_RATE = 8820
private let WINDOW_SIZE = 8192

class NoteAnalyser {
    
    init(Fs: Int) {
        DEFAULT_SAMPLE_RATE = Fs
        fRange = FRange(N: FOURIER_LENGHT, Fs: DEFAULT_SAMPLE_RATE)
    }
    
    private let kaiserWindow = WindowGenerator().kaiser(l: WINDOW_SIZE, beta: Constants.Filtering.BETA)
    
    private var _sampleRate = DEFAULT_SAMPLE_RATE
  //
    var sampleRate: Int {
        set {
            _sampleRate = newValue
            self.fRange = FRange(N: FOURIER_LENGHT, Fs: self._sampleRate)
        }
        get {
            return _sampleRate
        }
    }
    //
    private var fRange: FRange
    
    func getFilters(inputArray: [Float]) -> [Peak] {
        let windows = Vector().divideVectorIntoPieces(inputArray, pieceLenght: WINDOW_SIZE, stepLenght: WINDOW_SIZE / 2)
        var peaks: [Peak] = []
        
        for window in windows {
            peaks.append(contentsOf: getFrequencyPeaks(inputArray: window))
        }
        
        removeSameFrequencies(peaks: &peaks)
        validPeaks(peaks: &peaks)
        removeHarmonics(peaks: &peaks)
        
        return peaks
    }
    
    private func getFrequencyPeaks(inputArray: [Float]) -> [Peak] {
        var array = inputArray
        
        Vector().multiplyRealVectors(vectorA: array, vectorB: kaiserWindow, vectorC: &array)
        array = transform(signal: array)
        
        
        return locatePeaks(inputArray:array)
    }
    
    private func transform(signal : [Float]) -> [Float] {
        //Real and imaginary window vector instance
        var signalReal = signal
        signalReal.append(contentsOf: [Float](repeating: 0, count: 8192 - signal.count))
        
        var signalImaginary = [Float](repeating: 0, count: 8192)
        
        //Real and imaginary window representative
        var splitComplexSignal = DSPSplitComplex(realp: &signalReal, imagp: &signalImaginary)
        
        //Settings for FFT
        let length = vDSP_Length(floor(log2(Float(Constants.Filtering.FOURIER_LENGTH))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetup(length, radix)
        
        //Signal from time domain to frequency domain
        vDSP_fft_zip(weights!, &splitComplexSignal, 1, length, FFTDirection(FFT_FORWARD))
        
        var magnitude = [Float](repeating: 0, count: Constants.Filtering.FOURIER_LENGTH)
        vDSP_zvmags(&splitComplexSignal, 1, &magnitude, 1, vDSP_Length(Constants.Filtering.FOURIER_LENGTH))
        magnitude = magnitude.map(){sqrt($0)}
        
        return magnitude
    }
    
    func locatePeaks(inputArray: [Float]) -> [Peak] {
        var peaks: [Peak] = []
        
        if (inputArray.count/2) > 1 {
            for i in 1..<inputArray.count/2 {
                if inputArray[i-1] < inputArray[i] && inputArray[i+1] < inputArray[i] {
                    if inputArray[i] > 3 {
                        peaks.append(Peak(sample: fRange.filterForSample(sample: i), amplitude: inputArray[i], qtdeJanelas: 0))
                    }
                }
            }
        }
        
        return peaks
    }
    
    func removeSameFrequencies(peaks: inout [Peak]) {
        peaks.sort { $0.sample < $1.sample }
        
        var newList: [Peak] = []
        
        var peakAux = Peak(sample: 0, amplitude: 0.0, qtdeJanelas: 0)
        
        for peak in peaks {
            if peakAux.sample != peak.sample {
                if peakAux.sample != 0 && peakAux.amplitude != 0 {
                    newList.append(Peak(sample: peakAux.sample, amplitude: peakAux.amplitude, qtdeJanelas: 0))
                }
                peakAux = peak
            } else {
                peakAux.amplitude = max(peak.amplitude, peakAux.amplitude)
            }
        }
        
        if peakAux.sample != 0 && peakAux.amplitude != 0 {
            newList.append(peakAux)
        }
        
        peaks.removeAll()
        peaks.append(contentsOf: newList)
    }
    
    func validPeaks(peaks: inout [Peak]) {
        var validPeaks: [Peak] = []
        let thresholds = Thresholds(proportion: 1.0)
        
        for peak in peaks {
            if peak.amplitude >= thresholds.at(filter: peak.sample) {
                validPeaks.append(peak)
            }
        }
        
        peaks.removeAll()
        peaks.append(contentsOf: validPeaks)
    }
    
    func removeHarmonics(peaks: inout [Peak]) {
        var validPeaks: [Peak] = []
        
        peaks.sort { $0.sample > $1.sample }
        
        jumpHarmonic: for i in 0..<peaks.count {
            verification: for j in i+1..<peaks.count {
                switch peaks[i].sample {
                case peaks[j].sample + 12, peaks[j].sample + 19, peaks[j].sample + 24, peaks[j].sample + 28, peaks[j].sample + 31, peaks[j].sample + 34:
                    continue jumpHarmonic
                default:
                    continue verification
                }
            }
            validPeaks.append(peaks[i])
        }
        
        peaks.removeAll()
        peaks.append(contentsOf: validPeaks)
    }
}
