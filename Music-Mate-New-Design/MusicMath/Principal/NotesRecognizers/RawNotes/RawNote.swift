//
//  RawNote.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class RawNote {
    var filter    : Int   = 0
    var start     : Int   = 0
    var duration  : Int   = 0
    var amplitude : Float = 0.0
    var isPossibleHarmonic = false
    
    init(filter: Int, start: Int) {
        self.filter = filter
        self.start  = start
    }
    
    var midi : Int {
        get {
            return filter + 24
        }
    }
    
    func addDuration(duration: Int) {
        self.duration += duration
    }
    
    func newAmplitude(amplitude: Float) {
        self.amplitude = max(amplitude, amplitude)
    }
    
    func Filter() -> Int {
        return self.filter
    }
    
    func setFilter(filter: Int) {
        self.filter = filter
    }
    
    func Start() -> Int {
        return start
    }
    
    func setStart(start: Int) {
        self.start = start
    }
    
    func Duration() -> Int {
        return duration
    }
    
    func end() -> Int {
        return start + duration
    }
    
    func isInSameTimeAs(rawNote: RawNote) -> Bool {
        let isInSameTime = start > rawNote.start && start < rawNote.end() || end() > rawNote.start && end() < rawNote.end() || rawNote.Start() >= start && rawNote.end() < end()
        
        return isInSameTime
    }
    
    func Amplitude() -> Float {
        return amplitude
    }
    
    func isHarmonic(rawNote: RawNote) -> Bool {
        let isPossibleHarmonic = filter == rawNote.filter - 12 || filter == rawNote.filter - 19 || filter == rawNote.filter - 24
        
        if isPossibleHarmonic {
            return !(end() <= rawNote.start || rawNote.end() <= start)
        }
        
        return false
    }
    
    func isSameNote(rawNote: RawNote) -> Bool {
        let isPossibleSameNote = filter == rawNote.filter
        
        if isPossibleSameNote {
            return rawNote.amplitude < amplitude * 0.5
        }
        
        return false
    }
    
    func setPossibleHarmonic(isPossibleHarmonic: Bool) {
        self.isPossibleHarmonic = isPossibleHarmonic
    }
    
    func IsPossibleHarmonic() -> Bool {
        return isPossibleHarmonic
    }
    
    private func ComparatorFilter(rawNotes: inout [RawNote], sortOrder: SortOrder) {
        if sortOrder == .ascending {
            rawNotes.sort(by: {$0.Filter() < $1.Filter()})
        } else {
            rawNotes.sort(by: {$0.Filter() > $1.Filter()})
        }
    }
    
    func ComparatorFilterAsc(rawNotes: inout [RawNote]) {
        ComparatorFilter(rawNotes: &rawNotes, sortOrder: .ascending)
    }
    
    func ComparatorFilterDes(rawNotes: inout [RawNote]) {
        ComparatorFilter(rawNotes: &rawNotes, sortOrder: .descendant)
    }
    
    private func ComparatorStart(rawNotes: inout [RawNote], sortOrder: SortOrder) {
        if sortOrder == .ascending {
            rawNotes.sort(by: {$0.Start() < $1.Start()})
        } else {
            rawNotes.sort(by: {$0.Start() > $1.Start()})
        }
    }
    
    func ComparatorStartAsc(rawNotes: inout [RawNote]) {
        return ComparatorStart(rawNotes: &rawNotes, sortOrder: SortOrder.ascending);
    }
    
    func ComparatorStartDes(rawNotes: inout [RawNote]) {
        return ComparatorStart(rawNotes: &rawNotes, sortOrder: SortOrder.descendant);
    }
    
}

enum SortOrder {
    case ascending
    case descendant
}
