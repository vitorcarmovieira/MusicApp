//
//  MatrixBinarizer.swift
//  Engine
//
//  Created by Diego Lopes on 4/6/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class MatrixBinarizer {
    func peakThresholdForFilter(filter: Int) -> Float {
        return 0.5
    }
    
    func PfmThresholdForFilter(filter: Int) -> Float {
        let PercentageForPFM: Float = 0.1
        return peakThresholdForFilter(filter: filter) * PercentageForPFM
    }
    
    func binarizeEnergyMatrix(signal: inout [Float]) {
        PeakIdentifier().binarizeVector(vector: &signal, notePeakThreshold: peakThresholdForFilter(filter: 0))
    }
}
