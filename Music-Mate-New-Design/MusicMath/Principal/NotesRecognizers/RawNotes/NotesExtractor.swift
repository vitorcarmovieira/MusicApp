//
//  NotesExtractor.swift
//  Engine
//
//  Created by Diego Lopes on 4/6/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class NotesExtractor {
    var sampleRate: Float = 0.0
    
    init(sampleRate: Float) {
        self.sampleRate = sampleRate
    }
    
    func extractNotes(signal: inout [Float], filter: Int) -> [RawNote] {
        var normalize = [Float](repeating: 0.0, count: signal.count)
        Vector().normalize(source: signal, output: &normalize)
        
        let matrixBinarizer = MatrixBinarizer()
        matrixBinarizer.binarizeEnergyMatrix(signal: &normalize)
        
        let rawNotes = identifyNotes(vector: &normalize, filterId: filter)
        
        for raw in rawNotes {
            raw.newAmplitude(amplitude: Vector().highestAbsValue(vector: signal, range: [raw.Start(), raw.end()]))
        }
        
        return rawNotes
    }
    
    func identifyNotes(vector: inout [Float], filterId: Int) -> [RawNote] {
        var rawNotes : [RawNote] = []
        
        connect2(vector: &vector, minimumConnectivitySize: connectionSize())
        removeNoise(vector: &vector, maximumNoiseSize: noiseSize())
        
        let notes = identifyGroups(vector: vector, commonValue: 1)
        
        for note in notes {
            let raw = RawNote(filter: filterId, start: note[0])
            raw.addDuration(duration: note[1] - note[0])
            rawNotes.append(raw)
            
        }
        
        return rawNotes
    }
    
    func connect(vector: inout [Float], minimumConnectivitySize: Int) {
        replaceCommonGroupOfValuesByNewValue(vector: &vector, commonValue: 0, newValue: 1, sizeOfGroup: minimumConnectivitySize)
    }
    
    func removeNoise(vector: inout [Float], maximumNoiseSize: Int) {
        replaceCommonGroupOfValuesByNewValue(vector: &vector, commonValue: 1, newValue: 0, sizeOfGroup: maximumNoiseSize)
    }
    
    func replaceCommonGroupOfValuesByNewValue(vector: inout [Float], commonValue: Int, newValue: Float, sizeOfGroup: Int) {
        var groups = identifyGroups(vector: vector, commonValue: Float(commonValue))
        
        for groupId in 0..<groups.count {
            if groups[groupId][1] - groups[groupId][0] < sizeOfGroup {
                fillsIntervalWithValue(vector: &vector, start: groups[groupId][0], end: groups[groupId][1], value: newValue)
            }
        }
    }
    
    func identifyGroups(vector: [Float], commonValue: Float) -> [[Int]] {
        var vetAux = vector
        vetAux.append(-1.0)
        
        var groups: [[Int]] = []
        
        var groupStart = 0
        var groupEnd   = 0
        var i          = 0
        
        while i < vetAux.count {
            if vetAux[i] == commonValue {
                groupStart = i
                for j in (i+1)..<vetAux.count {
                    if vetAux[j] != commonValue {
                        groupEnd = j - 1
                        groups.append([groupStart, groupEnd])
                        i = j
                        
                        break
                    }
                }
            }
            i += 1
        }
        
        return groups
    }
    
    func fillsIntervalWithValue(vector: inout [Float], start: Int, end: Int, value: Float) {
        for index in start..<end {
            vector[index] = value
        }
    }
    
    func connectionSize() -> Int {
        return 800
    }
    
    func noiseSize() -> Int {
        return 513
    }
    
    func connect2(vector: inout [Float], minimumConnectivitySize: Int) {
        var counter = 0
        var hasStarted = false
        
        for i in 0..<vector.count {
            if !hasStarted {
                if vector[i] == 1 {
                    hasStarted = true
                }
            } else {
                if vector[i] == 0 {
                    vector[i] = 1
                    counter+=1
                    if counter >= minimumConnectivitySize {
                        hasStarted = false
                    }
                } else {
                    counter = 0
                }
            }
        }
        
    }
}
