//
//  PeakIdentifier.swift
//  Engine
//
//  Created by Diego Lopes on 4/6/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class PeakIdentifier {
    let minimumSpaceBetweenNotes = 320
    let spaceBetweenNotesSpikes  = 640
    var pfmThreshold: Float = 0.0
    
    func setPfmThreshold(pfmThreshold: Float) {
        self.pfmThreshold = pfmThreshold
    }
    
    func zeroPreviousValuesAt(pivot: Int, vector: inout [Float]) {
        var start = pivot - minimumSpaceBetweenNotes
        
        if start < 0 {
            start = 0
        }
        
        for index in start..<pivot {
            vector[index] = 0.0
        }
    }
    
    func binarizeVector(vector: inout [Float], notePeakThreshold: Float) {
        for index in 0..<vector.count {
            if vector[index] >= notePeakThreshold {
                vector[index] = 1
            } else {
                vector[index] = 0
            }
        }
    }
}
