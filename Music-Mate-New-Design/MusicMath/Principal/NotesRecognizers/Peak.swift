//
//  Peak.swift
//  Engine
//
//  Created by Diego Lopes on 4/6/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class Peak {
    var sample    : Int   = 0
    var amplitude : Float = 0.0
    var qtdeJanelas = 0
    
    init(sample: Int, amplitude: Float, qtdeJanelas: Int) {
        self.sample    = sample
        self.amplitude = amplitude
        self.qtdeJanelas = qtdeJanelas
    }
    
    func create(sample: Int, amplitude: Float) -> Peak {
        return Peak(sample: sample, amplitude: amplitude, qtdeJanelas: 0)
    }
    
    func getSample() -> Int {
        return self.sample
    }
    
    func getAmplitude() -> Float {
        return self.amplitude
    }
    
    func amplitudeInDb() -> Float {
        return 20.0 * Float(log10(amplitude))
    }
}
