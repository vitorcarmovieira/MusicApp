//
//  HarmonicHandler.swift
//  Music-Mate-New-Design
//
//  Created by Gabriel on 04/01/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//
//
import Foundation

public class HarmonicHandler {
    
    func handle(rawNotes : inout [RawNote]) {
        let thresholds = Thresholds(proportion: 1)
        
        rawNotes.sort(by: {$0.filter < $1.filter})
        
        for i in 0 ..< rawNotes.count {
            var j = i + 1
            var level = 0
            while j < rawNotes.count {
                if (HarmonicAnalyzer().isHarmonic(mainNote: rawNotes[i], possibleHarmonic: rawNotes[j], levels: &level)) {
                    if (Double(rawNotes[j].amplitude) < Double(thresholds.energyFor(filter: rawNotes[j].filter))) {
                        rawNotes.remove(at: j);
                        j -= 1
                    }
                }
                j += 1
            }
        }
        
        rawNotes.sort(by: {$0.start < $1.start})
    }
    
    func handle1(rawNotes : inout [RawNote]) {
        rawNotes.sort(by: {$0.filter < $1.filter})
        
        for i in 0 ..< rawNotes.count {
            var j = i + 1
            var level = 0
            while j < rawNotes.count {
                if (HarmonicAnalyzer().isHarmonic(mainNote: rawNotes[i], possibleHarmonic: rawNotes[j], levels: &level) ||
                    rawNotes[i].isSameNote(rawNote: rawNotes[j])) {
                    rawNotes.remove(at: j)
                } else {
                    j += 1
                }
            }
        }
        
        rawNotes.sort(by: {$0.start < $1.start})
    }
}
