//
//  HarmonicAnalyzer.swift
//  Engine
//
//  Created by Diego Lopes on 4/6/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//
//
import UIKit

class HarmonicAnalyzer {
    let HARMONICS = [12, 19, 24, 28, 31, 34, 36, 38, 40, 42, 43, 45, 46, 47, 48]
    
    let DIFFERENCE_BETWEEN_NOTE_AND_HARMONIC = 4410
    
    func isHarmonic(mainNote: RawNote, possibleHarmonic: RawNote, levels: inout Int) -> Bool {
        if isHarmonicByFilter(mainNote: mainNote, possibleHarmonic: possibleHarmonic, levels: &levels) {
            var vlAbs = mainNote.Start() - possibleHarmonic.Start()
            
            if vlAbs < 0 {
                vlAbs *= -1
            }
            
            return vlAbs <= DIFFERENCE_BETWEEN_NOTE_AND_HARMONIC
        }
        
        return false
    }
    
    func isHarmonicByFilter(mainNote: RawNote, possibleHarmonic: RawNote, levels: inout Int) -> Bool {
        if levels == 0 {
            levels = HARMONICS.count
        }
        
        var isHarmonicByFilter = false
        
        for h in 0..<levels {
            if h < HARMONICS.count {
                isHarmonicByFilter = isHarmonicByFilter || mainNote.Filter() == possibleHarmonic.Filter() - HARMONICS[h]
            }
        }
        
        return isHarmonicByFilter
    }
}
