//
//  SpectrumAnalyzer.swift
//  Engine
//
//  Created by Diego Lopes on 4/9/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
import Accelerate

class SpectrumAnalyser {
    var sampleRate: Int
    var fRange: FRange
    private var matrizDeBancosDeFiltro = [[Float]]()
    
    init(sampleRate: Int) {
        self.sampleRate = sampleRate
        fRange = FRange(N: Constants.Filtering.FOURIER_LENGTH, Fs: sampleRate)
        
        matrizDeBancos()
    }

    private func transform(signal : [Float]) -> [Float] {
        //Real and imaginary window vector instance
        var signalReal = signal
        signalReal.append(contentsOf: [Float](repeating: 0, count: 8192 - signal.count))
        
        var signalImaginary = [Float](repeating: 0, count: 8192)
        
        //Real and imaginary window representative
        var splitComplexSignal = DSPSplitComplex(realp: &signalReal, imagp: &signalImaginary)
        
        //Settings for FFT
        let length = vDSP_Length(floor(log2(Float(Constants.Filtering.FOURIER_LENGTH))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetup(length, radix)
        
        //Signal from time domain to frequency domain
        vDSP_fft_zip(weights!, &splitComplexSignal, 1, length, FFTDirection(FFT_FORWARD))
        
        var magnitude = [Float](repeating: 0, count: Constants.Filtering.FOURIER_LENGTH)
        vDSP_zvmags(&splitComplexSignal, 1, &magnitude, 1, vDSP_Length(Constants.Filtering.FOURIER_LENGTH))
        magnitude = magnitude.map(){sqrt($0)}
        
        return magnitude
    }
    
    private func getFrequencyPeaksIn(signal : [Float], windowSize : Int) -> [[Peak]]{
        var peaks = [[Peak]]()
        var windowedSignal = Vector().divideVector(vector: signal, size: windowSize/2)

        let kaiserWindow = WindowGenerator().kaiser(l: windowSize, beta: Constants.Filtering.BETA)
        
        for wind in 0 ..< windowedSignal.count {
//            var range: [Int] = []
            Vector().multiplyRealVectors(vectorA: windowedSignal[wind], vectorB: kaiserWindow, vectorC: &windowedSignal[wind])
//            Vector().multiplyRealVectors(srcA: windowedSignal[wind], srcB: kaiserWindow, output: &windowedSignal[wind], range: &range)
            
            peaks.append(locatePeaks(frequencyDomain: transform(signal: windowedSignal[wind])))
        }
        
        return peaks
    }
    
    private func locatePeaks(frequencyDomain : [Float]) -> [Peak] {
        var peaks = [Peak]()
        
        for i in 1 ..< (frequencyDomain.count / 2) - 1 {
            if (frequencyDomain[i-1] < frequencyDomain[i] && frequencyDomain[i+1] < frequencyDomain[i]) {
                if (!(i < 500 && frequencyDomain[i] < 3)) {
                    peaks.append(Peak(sample: i, amplitude: frequencyDomain[i], qtdeJanelas: 0))
                }
            }
        }
        return peaks
    }
    
    public func getNotes(signal : [Float], poli : Bool) -> [RawNote] {
        
        var peaks = getFrequencyPeaksIn(signal: signal, windowSize: Constants.Filtering.WINDOW_LENGTH)
        var rawNotesPerQuantization = [[RawNote]]()
        
        for wind in 0 ..< peaks.count {
            rawNotesPerQuantization.append(getNotesFromPeaks(peaks: peaks[wind], window: wind))
        }
        
        var noteList: [RawNote] = []
        
        for rawNotes in rawNotesPerQuantization {
            for rawNote in rawNotes {
                noteList.append(rawNote)
            }
        }
        //
        joinSameFrequency(rawNotes: &noteList)
        noteList.sort(by: {$0.Filter() < $1.Filter()})
        
        let filtros = filtersInvolved(rawNotes: &noteList)
        let noteList2 = getNotesForFilters(source: signal, filters: filtros)
        
//        removeSameNote(rawNotes: &noteList2)
        
        return noteList2
    }
    //
    func getNotesForFilters(source: [Float], filters: [Int]) -> [RawNote] {
        var rawNotes: [RawNote] = []
        
      //  let notesExtractor = NotesExtractor(sampleRate: Float(Constants.Audio.SAMPLE_RATE))
        
        var filteredSignal = [Float](repeating: 0, count: source.count)
        
        for filter in filters {
            vDSP_conv(source, 1, matrizDeBancosDeFiltro[filter], -1, &filteredSignal, 1, vDSP_Length(filteredSignal.count), vDSP_Length(matrizDeBancosDeFiltro[filter].count))
            
          //  rawNotes.append(contentsOf: notesExtractor.extractNotes(signal: &filteredSignal, filter: filter))
        }

        rawNotes.sort(by: {$0.start < $1.start})
        
        return rawNotes
    }
    
    func filtersInvolved(rawNotes: inout [RawNote]) -> [Int] {
        var filters: [Int] = []
        let thresholds = Thresholds(proportion: 1.0)
        var i = 0
        
        for rawNote in rawNotes {
//            if rawNote.filter == 36 {
                print("-->",rawNote.filter, rawNote.amplitude)
//            }
        }
        while i < rawNotes.count {
            if (rawNotes[i].Amplitude() < thresholds.at(filter: rawNotes[i].Filter())){
                rawNotes.remove(at: i)
            } else {
                i+=1
            }
        }
        
        for pivor in 0..<rawNotes.count {
            var andante = pivor + 1
            while andante < rawNotes.count {
                if (rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 12 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 19 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 24 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 28 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 31 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 34 ||
                    rawNotes[pivor].Filter() == rawNotes[andante].Filter() - 36) {
                    rawNotes.remove(at: andante)
                } else {
                    andante+=1
                }
            }
        }
        
        for rawNote in rawNotes {
            filters.append(rawNote.Filter())
        }
        
        return filters;
    }
    
    func joinSameFrequency(rawNotes: inout [RawNote]) {
        for pivor in 0..<rawNotes.count {
            var andante = pivor+1
            while andante < rawNotes.count {
                if (rawNotes[pivor].Filter() == rawNotes[andante].Filter()) {
                    if (rawNotes[pivor].Amplitude() < rawNotes[andante].Amplitude()) {
                        rawNotes[pivor].newAmplitude(amplitude: rawNotes[andante].Amplitude())
                    }
                    rawNotes.remove(at: andante)
                } else {
                    andante += 1
                }
            }
        }
    }
    
    private func getNotesFromPeaks(peaks: [Peak], window: Int) -> [RawNote]{
        
        var rawNotes = [RawNote]()
        
        for peak in peaks {
            let filter = fRange.filterForSample(sample: peak.sample)
            let amplitude = peak.getAmplitude()
            
            let rawNote = RawNote(filter: filter, start: window)
            rawNote.newAmplitude(amplitude: amplitude)
            
            rawNotes.append(rawNote)
        }
        
        removeSameNote(rawNotes: &rawNotes)

        rawNotes.sort(by: {$0.Amplitude() < $1.Amplitude()})
        
        let numeroDeNotas = 10
        
        var list: [RawNote] = []
        
        for i in 0..<rawNotes.count {
            if rawNotes[i].Filter() > 16 && list.count < numeroDeNotas {
                list.append(rawNotes[i])
            }
        }
        
        return rawNotes
    }
    
    func removeSameNote(rawNotes: inout [RawNote]) {
        if rawNotes.count > 0 {
            for i in 0..<rawNotes.count - 1 {
                var j = i + 1
                while j < rawNotes.count {
                    if (rawNotes[i].Filter() == rawNotes[j].Filter()){
                        if(rawNotes[i].Amplitude() <= rawNotes[j].Amplitude()) {
                            rawNotes[i].newAmplitude(amplitude: rawNotes[j].Amplitude())
                        }
                        rawNotes.remove(at: j)
                    } else {
                        j+=1
                    }
                }
            }
        }
    }
    
    private func matrizDeBancos(){
        matrizDeBancosDeFiltro.append(banco1)
        matrizDeBancosDeFiltro.append(banco2)
        matrizDeBancosDeFiltro.append(banco3)
        matrizDeBancosDeFiltro.append(banco4)
        matrizDeBancosDeFiltro.append(banco5)
        matrizDeBancosDeFiltro.append(banco6)
        matrizDeBancosDeFiltro.append(banco7)
        matrizDeBancosDeFiltro.append(banco8)
        matrizDeBancosDeFiltro.append(banco9)
    //    matrizDeBancosDeFiltro.append(banco10)
        matrizDeBancosDeFiltro.append(banco11)
        matrizDeBancosDeFiltro.append(banco12)
        matrizDeBancosDeFiltro.append(banco13)
        matrizDeBancosDeFiltro.append(banco14)
        matrizDeBancosDeFiltro.append(banco15)
        matrizDeBancosDeFiltro.append(banco16)
        matrizDeBancosDeFiltro.append(banco17)
        matrizDeBancosDeFiltro.append(banco18)
        matrizDeBancosDeFiltro.append(banco19)
        matrizDeBancosDeFiltro.append(banco20)
        matrizDeBancosDeFiltro.append(banco21)
        matrizDeBancosDeFiltro.append(banco22)
        matrizDeBancosDeFiltro.append(banco23)
        matrizDeBancosDeFiltro.append(banco24)
        matrizDeBancosDeFiltro.append(banco25)
        matrizDeBancosDeFiltro.append(banco26)
        matrizDeBancosDeFiltro.append(banco27)
        matrizDeBancosDeFiltro.append(banco28)
        matrizDeBancosDeFiltro.append(banco29)
        matrizDeBancosDeFiltro.append(banco30)
        matrizDeBancosDeFiltro.append(banco31)
        matrizDeBancosDeFiltro.append(banco32)
        matrizDeBancosDeFiltro.append(banco33)
        matrizDeBancosDeFiltro.append(banco34)
        matrizDeBancosDeFiltro.append(banco35)
        matrizDeBancosDeFiltro.append(banco36)
        matrizDeBancosDeFiltro.append(banco37)
        matrizDeBancosDeFiltro.append(banco38)
        matrizDeBancosDeFiltro.append(banco39)
        matrizDeBancosDeFiltro.append(banco40)
        matrizDeBancosDeFiltro.append(banco41)
        matrizDeBancosDeFiltro.append(banco42)
        matrizDeBancosDeFiltro.append(banco43)
        matrizDeBancosDeFiltro.append(banco44)
        matrizDeBancosDeFiltro.append(banco45)
        matrizDeBancosDeFiltro.append(banco46)
        matrizDeBancosDeFiltro.append(banco47)
        matrizDeBancosDeFiltro.append(banco48)
        matrizDeBancosDeFiltro.append(banco49)
        matrizDeBancosDeFiltro.append(banco50)
        matrizDeBancosDeFiltro.append(banco51)
        matrizDeBancosDeFiltro.append(banco52)
        matrizDeBancosDeFiltro.append(banco53)
        matrizDeBancosDeFiltro.append(banco54)
        matrizDeBancosDeFiltro.append(banco55)
        matrizDeBancosDeFiltro.append(banco56)
        matrizDeBancosDeFiltro.append(banco57)
        matrizDeBancosDeFiltro.append(banco58)
        matrizDeBancosDeFiltro.append(banco59)
        matrizDeBancosDeFiltro.append(banco60)
        matrizDeBancosDeFiltro.append(banco61)
        matrizDeBancosDeFiltro.append(banco62)
        matrizDeBancosDeFiltro.append(banco63)
        matrizDeBancosDeFiltro.append(banco64)
        matrizDeBancosDeFiltro.append(banco65)
        matrizDeBancosDeFiltro.append(banco66)
        matrizDeBancosDeFiltro.append(banco67)
        matrizDeBancosDeFiltro.append(banco68)
        matrizDeBancosDeFiltro.append(banco69)
        matrizDeBancosDeFiltro.append(banco70)
        matrizDeBancosDeFiltro.append(banco71)
        matrizDeBancosDeFiltro.append(banco72)
        matrizDeBancosDeFiltro.append(banco73)
        matrizDeBancosDeFiltro.append(banco74)
        matrizDeBancosDeFiltro.append(banco75)
        matrizDeBancosDeFiltro.append(banco76)
        matrizDeBancosDeFiltro.append(banco77)
        matrizDeBancosDeFiltro.append(banco78)
        matrizDeBancosDeFiltro.append(banco79)
        matrizDeBancosDeFiltro.append(banco80)
        matrizDeBancosDeFiltro.append(banco81)
        matrizDeBancosDeFiltro.append(banco82)
        matrizDeBancosDeFiltro.append(banco83)
        matrizDeBancosDeFiltro.append(banco84)
    }
}
