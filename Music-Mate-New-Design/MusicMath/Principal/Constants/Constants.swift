//
//  Constants.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//
//
import UIKit

class Constants {
    class Audio {
        static let SAMPLE_RATE       : Int   = 8820
    }
    
    class Filtering {
        static let NUMBER_OF_FILTERS : Int   = 84
        static let COEFFICIENT       : Float = 0.1102
        static let BETA              : Float = (73 - 8.7) * COEFFICIENT
        static let FOURIER_LENGTH    : Int   = 8192
        static let WINDOW_LENGTH     : Int   = 8192
    }
}
