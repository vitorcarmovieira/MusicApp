//
//  FRange.swift
//  Music-Mate-New-Design
//
//  Created by Gabriel on 04/01/18.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation
//
public class FRange {
    
    //Twelfth Root of 2.
    private let r = pow(2, 1/12.0)
    
    //Lower frequency of C1.
    private let StartFrequency = 31.7308794
    
    //Matrix where the ranges will be stored. Where the row represents the filter,
    //column 0 is the lower value and column 1 is the upper value.
    private var ranges = Array(repeating: Array(repeating: 0, count: 2), count: 84)
    
    /*
     * Creates an object of the class with the appropriate parameters.
     *
     * @param N     The length of the fourier window.
     * @param Fs    The sampling rate.
     */
    init(N : Int, Fs : Int) {
        calculateRanges(N: N, Fs: Fs)
    }
    
    /*
     * Instantiate the ranges.
     *
     * @param N     The length of the fourier window.
     * @param Fs    The sampling rate.
     */
    private func calculateRanges( N : Int, Fs : Int){
        let proportionNFs = Double(N)/Double(Fs)
        
        for filter in 0..<84 {
            ranges[filter][0] = Int(round(StartFrequency * pow(r, Double(filter)) * proportionNFs))
            ranges[filter][1] = Int(round(StartFrequency * pow(r, Double(filter + 1)) * proportionNFs))
        }
    }
    
    /*
     * Get the range for the given filter.
     *
     * @param filter    The filter used to recover the range.
     * @return The range for the given filter.
     */
    public func rangeFor(filter : Int) -> Array<Int> {
        return ranges[filter];
    }
    
    /*
     * Calculates the filter for a sample.
     *
     * @param sample    The sample to be calculated.
     * @return The filter represented by the sample.
     */
    public func filterForSample(sample : Int) -> Int {
        for filter in 0..<84 {
            if (ranges[filter][0] <= sample && ranges[filter][1] >= sample) {
                return filter;
            }
        }
        
        return 0;
    }
    
}
