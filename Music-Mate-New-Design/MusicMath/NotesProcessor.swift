//
//  NotesProcessor.swift
//  Music-Mate-New-Design
//
//  Created by Ultimo Alves on 18/05/2018.
//  Copyright © 2018 LT Music Developer. All rights reserved.
//

import Foundation

class NotesProcessor {
    
    var metronome = 90
    var baseNote = 960
    var filters: [[Float]] = []
    
    init() {
        matrizDeBancos()
    }
    
    func filter(input: [Float], filtro: Int) -> [Float] {
        var output = [Float](repeating: 0, count: input.count)
        
        DSP().filter1(source: input, filter: filters[filtro], output: &output)
        Vector().abs(source: output, output: &output)
        Vector().normalize(source: output, output: &output)

        let output2 = Vector().shift(source: output, amount: 1100)
        
        return output2
    }
    
    func extractNotes(input: inout [Float], filtro: Int) -> [[Int]] {
        binarizeVector(vector: &input)
        var rawNotes = identifyNotes(vector: &input, filterId: filtro)
        
        for i in 0..<rawNotes.count {
            print(rawNotes[i][0], rawNotes[i][1])
            rawNotes[i][1] = samplesToTicks(sample: rawNotes[i][1])
            rawNotes[i][2] = samplesToTicks(sample: rawNotes[i][2])
        }
        
        return rawNotes
    }
    
    func fillsIntervalWithValue(vector: inout [Float], start: Int, end: Int, value: Float) {
        for index in start..<end {
            vector[index] = value
        }
    }
    
    private func samplesToTicks(sample: Int) -> Int {
        return Int(round(Double(metronome*sample*baseNote)/529200.0))
    }
    
    func connect2(vector: inout [Float], minimumConnectivitySize: Int) {
        var counter = 0
        var hasStarted = false
        
        for i in 0..<vector.count {
            if !hasStarted {
                if vector[i] == 1 {
                    hasStarted = true
                }
            } else {
                if vector[i] == 0 {
                    vector[i] = 1
                    counter+=1
                    if counter >= minimumConnectivitySize {
                        hasStarted = false
                    }
                } else {
                    counter = 0
                }
            }
        }
    }
    
    func removeNoise(vector: inout [Float], maximumNoiseSize: Int) {
        replaceCommonGroupOfValuesByNewValue(vector: &vector, commonValue: 1, newValue: 0, sizeOfGroup: maximumNoiseSize)
    }
  //
    func replaceCommonGroupOfValuesByNewValue(vector: inout [Float], commonValue: Int, newValue: Float, sizeOfGroup: Int) {
        var groups = identifyGroups(vector: vector, commonValue: Float(commonValue))
        
        for groupId in 0..<groups.count {
            if groups[groupId][1] - groups[groupId][0] < sizeOfGroup {
                fillsIntervalWithValue(vector: &vector, start: groups[groupId][0], end: groups[groupId][1], value: newValue)
            }
        }
    }
    //
    func identifyNotes(vector: inout [Float], filterId: Int) -> [[Int]] {
        var rawNotes : [[Int]] = []
        
        connect2(vector: &vector, minimumConnectivitySize: 800)
        removeNoise(vector: &vector, maximumNoiseSize: 513)
        
        let notes = identifyGroups(vector: vector, commonValue: 1)
        
        for note in notes {
            rawNotes.append([filterId, note[0], note[1] - note[0]])
        }
        
        return rawNotes
    }
    
    func identifyGroups(vector: [Float], commonValue: Float) -> [[Int]] {
        var vetAux = vector
        vetAux.append(-1.0)
        
        var groups: [[Int]] = []
        
        var groupStart = 0
        var groupEnd   = 0
        var i          = 0
        
        while i < vetAux.count {
            if vetAux[i] == commonValue {
                groupStart = i
                for j in (i+1)..<vetAux.count {
                    if vetAux[j] != commonValue {
                        groupEnd = j - 1
                        groups.append([groupStart, groupEnd])
                        i = j
                        
                        break
                    }
                }
            }
            i += 1
        }
        
        return groups
    }
    
    func binarizeVector(vector: inout [Float]) {
        for index in 0..<vector.count {
            if vector[index] >= 0.5 {
                vector[index] = 1
            } else {
                vector[index] = 0
            }
        }
    }
    
    private func matrizDeBancos(){
        filters.append(banco1)
        filters.append(banco2)
        filters.append(banco3)
        filters.append(banco4)
        filters.append(banco5)
        filters.append(banco6)
        filters.append(banco7)
        filters.append(banco8)
        filters.append(banco9)
        filters.append(banco10)
        filters.append(banco11)
        filters.append(banco12)
        filters.append(banco13)
        filters.append(banco14)
        filters.append(banco15)
        filters.append(banco16)
        filters.append(banco17)
        filters.append(banco18)
        filters.append(banco19)
        filters.append(banco20)
        filters.append(banco21)
        filters.append(banco22)
        filters.append(banco23)
        filters.append(banco24)
        filters.append(banco25)
        filters.append(banco26)
        filters.append(banco27)
        filters.append(banco28)
        filters.append(banco29)
        filters.append(banco30)
        filters.append(banco31)
        filters.append(banco32)
        filters.append(banco33)
        filters.append(banco34)
        filters.append(banco35)
        filters.append(banco36)
        filters.append(banco37)
        filters.append(banco38)
        filters.append(banco39)
        filters.append(banco40)
        filters.append(banco41)
        filters.append(banco42)
        filters.append(banco43)
        filters.append(banco44)
        filters.append(banco45)
        filters.append(banco46)
        filters.append(banco47)
        filters.append(banco48)
        filters.append(banco49)
        filters.append(banco50)
        filters.append(banco51)
        filters.append(banco52)
        filters.append(banco53)
        filters.append(banco54)
        filters.append(banco55)
        filters.append(banco56)
        filters.append(banco57)
        filters.append(banco58)
        filters.append(banco59)
        filters.append(banco60)
        filters.append(banco61)
        filters.append(banco62)
        filters.append(banco63)
        filters.append(banco64)
        filters.append(banco65)
        filters.append(banco66)
        filters.append(banco67)
        filters.append(banco68)
        filters.append(banco69)
        filters.append(banco70)
        filters.append(banco71)
        filters.append(banco72)
        filters.append(banco73)
        filters.append(banco74)
        filters.append(banco75)
        filters.append(banco76)
        filters.append(banco77)
        filters.append(banco78)
        filters.append(banco79)
        filters.append(banco80)
        filters.append(banco81)
        filters.append(banco82)
        filters.append(banco83)
        filters.append(banco84)
    }
}
