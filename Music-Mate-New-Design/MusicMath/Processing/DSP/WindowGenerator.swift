//
//  WindowGenerator.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit
//
class WindowGenerator {
    
    func kaiser(l: Int, beta: Float) -> [Float] {
        var window = [Float](repeating: 0.0, count: l)
        let besselBeta = bessel(x: beta)
        
        var arg: Float = 0.0
        let N = l - 1
        
        for n in 0..<l {
            arg = beta * sqrtf(1.0 - powf(Float(2 * n - N) / Float(N), 2.0))
            window[n] = bessel(x: arg) / besselBeta
        }
        
        return window
    }
    
    func bessel(x: Float) -> Float {
        var sum: Float        = 0.0
        var xForPowerI: Float = 0.0
        
        for i in 1..<10 {
            xForPowerI += powf(x / 2.0, Float(i))
            var factorial = 1
            
            for j in 1..<i {
                factorial *= j
            }
            
            sum += powf(xForPowerI / Float(factorial), 2.0)
        }
        
        return 1.0 + sum
    }
    
    func blackman(l: Int) -> [Float] {
        var window = [Float](repeating: 0.0, count: l)
        let M = l / 2
        let N = l - 1
        
        for n in 0..<M {
            window[n] = 0.42 - 0.5 * cos(2 * Float.pi * Float(n) / Float(N)) + 0.8 * cos(4 * Float.pi * Float(n) / Float(N))
        }
        
        for n in M..<l {
            window[n] = window[l - n - 1]
        }
        
        return window
    }
    
    func sinc(x: Float) -> Float {
        if x > -0.00001 && x < 0.00001 {
            return 1
        }
        return sin(x) / x
    }
}
