//
//  DSP.swift
//  Engine
//
//  Created by Diego Lopes on 4/4/18.
//  Copyright © 2018 Diego Developer. All rights reserved.
//

import UIKit

class DSP {
    func convcirc(srcA: [Float], srcB: [Float], output: inout [Float]) {
        let N = srcA.count
        var aux = [Float](repeating: 0.0, count: 2 * N)
        
        for n in 0..<aux.count - 1 {
            var sum: Float = 0.0
            
            for l in 0..<N - 1 {
                sum += srcA[l] * srcB[abs(n - 1) % N + 1]
            }
            aux[n] = sum
        }
        
        var cont = 0
        for k in N..<aux.count {
            output[cont] = aux[k]
            cont += 1
        }
    }
    //
    func convcirc2(srcA: [Float], srcB: [Float], output: inout [Float]) {
        let N = srcA.count
        
        var k = 0
        
        for i in 0..<N {
            for j in 0..<N {
                k = (i - j) % N
                output[i] += srcA[j] * srcB[k]
            }
        }
    }
    
    func filter1(source: [Float], filter: [Float], output: inout [Float]) {
        let N = min(source.count, output.count)
        var sum: Float = 0.0
        
        for n in 0..<N {
            sum = 0.0
            for m in 0..<filter.count where n - m >= 0 {
                sum += filter[m] * source[n - m]
            }
            output[n] = sum
        }
    }
}
